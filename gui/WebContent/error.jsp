<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
  ~
  ~ Available under MIT license (https://opensource.org/licenses/MIT)
  ~
  --%>
<!DOCTYPE html>
<%@page	import="fr.inria.cedar.connectionlens.gui.util.SessionAttributes"%>
<%@page	import="fr.inria.cedar.connectionlens.gui.util.RequestParameters"%>
<%@page import="fr.inria.cedar.connectionlens.sql.ConnectionManager"%>
<%@page import="fr.inria.cedar.connectionlens.gui.util.SessionAttributes"%>
<%@page import="com.google.common.base.Strings"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.List"%>

<html>

<head>
    <title style="font-variant: small-caps">Abstra</title>

    <meta charset="utf-8" />

    <link href="style/default.css" rel="stylesheet" type="text/css" media="all" />
    <link href="style/exportsvg.css" rel="stylesheet" type="text/css" media="all" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        /* * { box-sizing: border-box; }*/
        /*.column { float: left; width: auto; padding: 10px }*/
        /*.row:after { content: ""; display: table; clear: both; }*/
        .hiddenList {
            display: none;
        }

        .shownList {
            visibility: visible;
        }

        body {
            padding-top: 65px;
        }

        /* so that navbar does not hide the content */

    </style>
</head>

<body class="no-db">
<jsp:include page="header.jsp" />

<div class="container-fluid">
    <h2>Oops!</h2>

    <!--parameters-->
    <div class="row">
        <div class="col-sm-12">
            There was an error. Please check the Tomcat logs.
        </div>
    </div>
</div>

</body>
</html>
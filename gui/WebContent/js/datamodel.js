 /*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

class DataSource {
    constructor(sourceFile, sourceType) {
        this.file = sourceFile;
        this.typed = !!sourceType;
        if (this.typed)
            this.type = sourceType;
    }

    hasType() {
        return this.typed;
    }

    setType(type) {
        this.typed = !!type;
        this.type = type;
    }
}

class GraphNode {
    constructor(node) {
        this.id = node.global_id;
        this.label = node.label;
        this.type = node.type;
        this.context = node.context;
        this.queries = new Set();
        this.zone = new Set([this]);
        let dataSource = ctx.sources.find(s => s.file === node.source);
        if (dataSource) {
            this.dataSource = dataSource;
            if (!dataSource.hasType())
                this.dataSource.setType(this.source_type);
        }
        else {
            this.dataSource = new DataSource(node.source, node.source_type);
            ctx.sources.push(this.dataSource);
        }
    }

    static from(node) {
        // Copy a node
        return new GraphNode({
            global_id: node.id,
            label: node.label,
            type: node.type,
            source: node.dataSource.file,
            context: node.context
        });
    }

    getId() {
        return this.id;
    }

    getLabel(graph) {
        if (graph && !this.label.length) {
            let neighbors = graph.getIncoming(this);
            if (neighbors.length === 1)
                return neighbors[0].link.getLabel();
        }
        return this.label;
    }

    getDataSource() {
        return this.dataSource;
    }

    getContext() {
        return this.context;
    }

    getSubstructureContext() {
        let el = d3.create("div").classed("element", true).classed("open", false);
        let topbar = el.append("div").classed("topbar", true);
        let neighbors = ctx.graph.getOutgoing(this);
        if (neighbors.length)
            topbar.append("div").classed("expand-substructure", true)
                .on("click", () => el.classed("open", !el.classed("open")));
        topbar.append("div").text(this.getLabel(ctx.graph));
        if (neighbors.length) {
            let table = el.append("table");
            neighbors.forEach(function (neighbor) {
                let row = table.append("tr");
                row.append("td").text(neighbor.link.getLabel());
                row.append("td").append(() => neighbor.node.getSubstructureContext().node());
            });
        }
        return el;
    }

    getLabelHtml(graph) {
        // Set the visible chars and the invisible chars that only appear when hovering over a node

        let label = this.getLabel(graph);
        var visible_chars = new Array(label.length).fill(true);

        switch (this.getType().typeName) {
            case "ENTITY_PERSON":
                let words = label.split(" ");
                if (words.length > 1) {
                    visible_chars = [true].concat(
                        new Array(words[0].length - 1).fill(false),
                        new Array(this.label.length - words[0].length).fill(true)
                    );
                }
                break;
            case "EMAIL":
                let adresslength = label.split("@")[0].length;
                visible_chars = new Array(label.length).fill(true).fill(false, 0, adresslength);
                break;
            default:
                if (label.length > 30)
                    visible_chars.fill(false, 15, label.length - 15);
                break;
        }

        let currentlyhiding = false;
        let final_label = "";
        for (var i = 0; i < label.length; i++) {
            if (currentlyhiding) {
                if (visible_chars[i]) {
                    currentlyhiding = false;
                    final_label += "</tspan>";
                    final_label += "<tspan class=\"ellipsis\">[...]</tspan>";
                }
                final_label += label[i];
            }
            else {
                if (!visible_chars[i]) {
                    currentlyhiding = true;
                    final_label += "<tspan>";
                }
                final_label += label[i];
            }
        }
        if (currentlyhiding) {
            final_label += "</tspan>";
            final_label += "<tspan class=\"ellipsis\">[...]</tspan>";
        }
        return final_label;
    }

    getType() {
        console.log("this.type=" + this.type);
        //console.log(ctx.types);
        return ctx.types.find(type => type.names.has(this.type)) || ErrorQueue.enqueue("No such type: " + this.type);
    }

    hasPosition() {
        if (this.x || this.y)
            return true;
        return false;
    }

    save() {
        return { id: this.id, x: this.x, y: this.y };
    }
};

class GraphEdge {
    constructor(edge) {
        this.id = edge.global_id;
        this.label = edge.label;
        this.sameAs = edge.same_as;
        this.score = edge.score;
        this.source = edge.source;
        this.target = edge.target;
        this.sourceId = edge.source;
        this.targetId = edge.target;
        let dataSource = ctx.sources.find(s => s.file === edge.origin);
        if (dataSource) {
            this.dataSource = dataSource;
        }
        else {
            this.dataSource = new DataSource(edge.origin);
            ctx.sources.push(this.dataSource);
        }
    }

    static from(edge) {
        // Copy an edge
        return new GraphEdge({
            global_id: edge.id,
            label: edge.label,
            same_a: edge.sameAs,
            score: edge.score,
            source: edge.sourceId,
            target: edge.targetId,
            origin: edge.dataSource.file
        });
    }

    getId() {
        return this.id;
    }

    getDataSource() {
        return this.dataSource;
    }

    getLabel() {
        return this.label;
    }

    getScore() {
        return this.score;
    }

    isSameAs() {
         this.sameAs; //IM 22/4/21
    }

    getSource(graph) {
        if (graph && typeof this.source === "string")
            this.source = graph.getNode(this.source);
        return this.source;
    }

    getTarget(graph) {
        if (graph && typeof this.target === "string")
            this.target = graph.getNode(this.target);
        return this.target;
    }

    getSourceId() {
        return this.sourceId;
    }

    getTargetId() {
        return this.targetId;
    }

    isAdjacent(node) {
        let nodeId = node.getId();
        return nodeId === this.sourceId || nodeId === this.targetId;
    }
}

class Graph {
    constructor(graphData) {
        this.nodes = {};
        this.links = {};
        this.nodeIds = new Set();
        this.linkIds = new Set();
        console.log(graphData);
        if (graphData) {
            this.addNodes(graphData.nodes);
            console.log(graphData.links);
            this.addLinks(graphData.links);
            console.log(this.links);
        }
    }

    static from(graph) {
        // Copy a graph
        let g = new Graph();
        g.addNodes(graph.getNodes().map(GraphNode.from));
        g.addLinks(graph.getLinks().map(GraphEdge.from));
        return g;
    }

    get length() {
        return this.nodeIds.size;
    }

    getNodes() {
        return Object.values(this.nodes);
    }

    getLinks() {
        return Object.values(this.links);
    }

    getNodeIds() {
        return this.nodeIds;
    }

    getLinkIds() {
        return this.linkIds;
    }

    getSources() {
        let sources = this.getNodes().map(node => node.getDataSource()).concat(
            this.getLinks().map(link => link.getDataSource())
        );
        sources = [...new Set(sources)];
        return sources;
    }

    addNode(node) {
        let nodeId = node.getId();
        if (this.nodeIds.has(nodeId))
            return;
        this.nodes[nodeId] = node;
        this.nodeIds.add(nodeId);
        return node;
    }

    addLink(link) {
        let linkId = link.getId();
        if (this.linkIds.has(linkId))
            return;
        this.links[linkId] = link;
        this.linkIds.add(linkId);
        return link;
    }

    addNodes(nodes) {
        let newNodes = nodes.map(node => this.addNode(node));
        return newNodes.filter(node => node);
    }

    addLinks(links) {
        let newLinks = links.map(link => this.addLink(link));
        return newLinks.filter(link => link);
    }

    addGraph(graph) {
        let newNodes = this.addNodes(graph.getNodes());
        let newLinks = this.addLinks(graph.getLinks());

        return { "nodes": newNodes, "links": newLinks };
    }

    addGraphResults(graph) {
        let newGraph = this.addGraph(graph);

        this.resolveEdges(newGraph.links);
        this.resolveZones(newGraph.nodes);
    }

    resolveEdge(edge) {
        edge.source = this.getNode(edge.sourceId);
        edge.target = this.getNode(edge.targetId);
    }

    resolveEdges(edges) {
        // Resolve the source and target node of an edge
        let graph = this;
        edges.forEach(edge => graph.resolveEdge(edge));

        // Remove multi-edges
        edges.forEach(function (edge) {
            if (!graph.getLink(edge.getId())){
            	console.log("edge not found");
                return;
            }
	        let debug1=graph.getNeighbors(edge.getSource())
                .filter(neighbor => neighbor.node.getId() === edge.getTargetId());
	        let debug2=debug1
                .filter(neighbor => neighbor.link.getId() !== edge.getId());
            let sameEdges = debug2
                .map(neighbor => neighbor.link);
            console.log("removed edges");
            //  graph.removeLinks(sameEdges); 
        })
    }


    resolveZones(nodes) {
        // Resolve the zones of the new top-level records
        let tlrecords = nodes.filter(node => node.getType().typeName === "TLRECORD");
        let graph = this;
        tlrecords.forEach(function (record) {
            function addToZone(neighbor) {
                if (record.zone.has(neighbor.node))
                    return;
                record.zone.add(neighbor.node);
                if (neighbor.node.getType().typeName !== "TLRECORD" && neighbor.node.getType().typeName !== "COLLECTION")
                    graph.getOutgoing(neighbor.node).forEach(addToZone);
            }
            graph.getOutgoing(record).forEach(addToZone);
            ParentFilter.add(record);
        });

        // Find what zones added nodes are in
        nodes.forEach(function (node) {
            let explored = new Set();
            function addToZone(neighbor) {
                if (explored.has(neighbor.node))
                    return;
                explored.add(neighbor.node);
                if (neighbor.node.getType().typeName === "TLRECORD") {
                    neighbor.node.zone.add(node);
                    return;
                }
                if (neighbor.node.getType().typeName === "COLLECTION")
                    return;
                graph.getIncoming(neighbor.node).forEach(addToZone);
            }
            graph.getIncoming(node).forEach(addToZone);
        })
    }

    removeNode(node) {
        let nodeId = node.getId();
        this.nodeIds.delete(nodeId);
        delete this.nodes[nodeId];
        this.getNodes().forEach(n => n.zone.delete(node));
        this.removeLinks(this.getLinks().filter(link => link.isAdjacent(node)));
    }

    removeLink(link) {
        let linkId = link.getId();
        this.linkIds.delete(linkId);
        delete this.links[linkId];
    }

    removeNodes(nodes) {
        nodes.forEach(node => this.removeNode(node));
    }

    removeLinks(links) {
        links.forEach(link => this.removeLink(link));
    }

    getNode(nodeId) {
        return this.nodes[nodeId];
    }

    getLink(linkId) {
        return this.links[linkId];
    }

    isConnected(node1, node2) {
        if (node1.getId() === node2.getId())
            return false;
        return this.getLinks().some(link => link.isAdjacent(node1) && link.isAdjacent(node2));
    }

    getOutgoing(node) {
		// IM, 23/4/21: Don't filter away sameAs edges
        //let links = this.getLinks().filter(link => !link.isSameAs() && link.getSourceId() === node.getId());
        let links = this.getLinks().filter(link => link.getSourceId() === node.getId()); 
        let neighbors = links.map(function (link) {
            return { "link": link, "node": link.getTarget(), "parent": node };
        });
        return neighbors;
    }

    getIncoming(node) {
    	// IM, 23/4/21: Don't filter away sameAs edges
        //let links = this.getLinks().filter(link => !link.isSameAs() && link.getTargetId() === node.getId());
        let links = this.getLinks().filter(link => link.getTargetId() === node.getId());
        
        let neighbors = links.map(function (link) {
            return { "link": link, "node": link.getSource(), "parent": node };
        });
        return neighbors;
    }

    getNeighbors(node) {
        let links = this.getLinks().filter(link => link.isAdjacent(node));
        let nodeId = node.getId();
        let graph = this;
        let neighbors = links.map(function (link) {
            if (link.getSource(graph).getId() === nodeId)
                var n = link.getTarget(graph);
            else
                var n = link.getSource(graph);
            return { "link": link, "node": n, "parent": node };
        });
        return neighbors;
    }

    save() {
        return this.getNodes().map(node => node.save());
    }
}

class AnswerTree extends Graph {
    constructor(tree) {
        super();
        console.log(tree.nodes);
        console.log(tree.links);
        this.addNodes(tree.nodes);
        this.addLinks(tree.links);
        this.matches = tree.match_map;
        this.score = tree.score;
        this.rank = tree.rank;
        console.log(this.links.size); 
    }

    static merge(trees) {
        let graph = new Graph();
        console.log(graph);
        console.log(trees);
        console.log(graph.getLinks().size);
        trees.forEach(tree => graph.addGraph(Graph.from(tree)));
        return graph;
    }
}
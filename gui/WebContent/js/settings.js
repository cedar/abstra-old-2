/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

class Setting {
    constructor(name, action, value, active = null) {
        this.name = name;
        this.value = value;
        this.action = action;
        this.optional = active !== null;
        if (this.optional)
            this.active = active;
        let element = d3.select("#settings-content")
            .append("div")
            .classed("setting", true);
        element.append("div")
            .classed("label", true)
            .text(name);
        this.element = element.append("div")
            .classed("setting-content", true);
        let setting = this;
        if (active !== null)
            this.activeCheckbox = this.element.append("input")
                .classed("active", true)
                .attr("type", "checkbox")
                .attr("checked", active ? "" : null)
                .on("input", () => setting.setActive(d3.event.target.checked));
    }

    getValue() {
        if (this.optional && !this.active)
            return undefined;
        return this.value;
    }

    setValue(value) {
        this.value = value;
        this.action(this.getValue());
    }

    setActive(active) {
        this.active = active;
        this.action(this.getValue());
    }

    load(property) {
        this.setValue(property.value);
        if (this.optional)
            this.activeCheckbox.property("checked", property.active);
        this.setActive(property.active);
    }

    save() {
        let property = { value: this.value };
        if (this.optional)
            property.active = this.active;
        return property;
    }
}

class RangeSetting extends Setting {
    constructor(name, action, value, active, min, max, step = 1) {
        super(name, action, value, active);

        let setting = this;
        this.range = this.element.append("input")
            .classed("main-setting", true)
            .attr("type", "range")
            .attr("min", min)
            .attr("max", max)
            .attr("step", step)
            .attr("value", value)
            .on("input", () => setting.setValue(d3.event.target.value));
        if (this.optional)
            this.range.attr("disabled", this.active ? null : "");
    }

    setValue(value) {
        value = parseFloat(value);
        super.setValue(value);
    }

    setActive(active) {
        if (this.optional)
            this.range.attr("disabled", active ? null : "");
        super.setActive(active);
    }

    load(property) {
        this.range.property("value", property.value);
        super.load(property);
    }
}

class CheckboxSetting extends Setting {
    constructor(name, action, value) {
        super(name, action, value);

        let setting = this;
        this.element.append("input")
            .classed("main-setting", true)
            .attr("type", "checkbox")
            .attr("checked", value ? "" : null)
            .on("input", () => setting.setValue(d3.event.target.checked));
    }

    load(property) {
        this.element.select(".main-setting").property("checked", property.value);
        super.load(property);
    }
}
/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

class Simulation {
    constructor(graph, svg, parameters = {}, isMainSimulation = false) {
        this.nodeSize = getDefault(parameters, "nodeSize", ctx.SETTINGS.nodeSize.getValue());
        this.linkDistance = getDefault(parameters, "linkDistance", ctx.SETTINGS.linkDistance.getValue());
        this.linkStrength = getDefault(parameters, "linkStrength", ctx.SETTINGS.linkStrength.getValue());
        this.repulsionStrength = getDefault(parameters, "repulsionStrength", ctx.SETTINGS.repulsionStrength.getValue());
        this.repulsionMaxDistance = getDefault(parameters, "repulsionMaxDistance", ctx.SETTINGS.repulsionMaxDistance.getValue());
        this.centerForce = getDefault(parameters, "centerForce", ctx.SETTINGS.centerForce.getValue());
        this.boundingBox = getDefault(parameters, "boundingBox", ctx.BOUNDING_BOX.DEFAULT);
        this.alpha = getDefault(parameters, "alpha", ctx.ALPHA);
        this.alphaDrag = getDefault(parameters, "alphaDrag", ctx.ALPHA_DRAG);

        this.graph = graph;
        this.isMainSimulation = isMainSimulation;

        this.svg = svg;
        this.initializeSvg();

        this.simulation = d3.forceSimulation()
            .force("link", d3.forceLink()
                .id(d => d.getId())
                .distance(this.linkDistance)
                .strength(this.linkStrength))
            .force("repulsion", d3.forceManyBody()
                .strength(this.repulsionStrength)
                .distanceMin(2 * this.nodeSize)
                .distanceMax(this.repulsionMaxDistance === undefined ? Infinity : this.repulsionMaxDistance))
            .force("collide", d3.forceCollide(this.nodeSize))
            .alpha(0);
        if (this.centerForce) {
            let size = this.getSize();
            this.simulation.force("center", d3.forceCenter(size.width / 2, size.height / 2))
        }
        this.updateGraphData();
        let sim = this;
        this.simulation.on("tick", () => sim.step());
    }

    getSize() {
        if (!this.svg.attr("width"))
            return this.svg.node().getBoundingClientRect();
        return {
            width: parseInt(this.svg.attr("width")),
            height: parseInt(this.svg.attr("height"))
        };
    }

    initializeSvg() {
        this.svg.html("<defs><marker id=\"arrowhead_" + this.svg.attr("id") + "\" viewBox=\"0 0 10 10\" refX=\"10\" refY=\"5\" markerWidth=\"6\" markerHeight=\"6\" orient=\"auto-start-reverse\"><path class=\"arrowhead\" d=\"M 0 0 L 10 5 L 0 10 z\" /><defs></marker>");

        if (this.isMainSimulation) {
            let viewport = this.svg.append("g")
                .attr("id", "viewport");
            this.zoom = d3.zoom()
                .on("zoom", function () {
                    viewport.attr("transform", d3.event.transform);
                });
            this.svg.call(this.zoom).on("dblclick.zoom", null);
            viewport.append("g").classed("links", true);
            viewport.append("g").classed("nodes", true);
        }
        else {
            this.svg.append("g").classed("links", true);
            this.svg.append("g").classed("nodes", true);
        }
    }

    getBoundingBox() {
        // Get the bounding box of the nodes in the layout

        let nodes = this.graph.getNodes();
        if (!nodes)
            return;
        let padding = 2 * this.nodeSize;
        let boundingBox = {};
        let x = nodes.map(d => d.x);
        let y = nodes.map(d => d.y);
        boundingBox.x = Math.min(...x) - padding;
        boundingBox.y = Math.min(...y) - padding;
        boundingBox.width = Math.max(...x) + 2 * padding - boundingBox.x;
        boundingBox.height = Math.max(...y) + 2 * padding - boundingBox.y;
        boundingBox.cx = boundingBox.x + boundingBox.width / 2;
        boundingBox.cy = boundingBox.y + boundingBox.height / 2;

        return boundingBox;
    }

    resetViewport() {
        // Reset the viewport to fit all the nodes in the view

        let boundingBox = this.getBoundingBox();
        if (!boundingBox)
            return;

        let bcr = this.svg.node().getBoundingClientRect();

        let scale = Math.min(1, bcr.width / boundingBox.width, bcr.height / boundingBox.height);

        let transform = d3.zoomIdentity;
        transform = transform.translate(bcr.width / 2, bcr.height / 2);
        transform = transform.scale(scale);
        transform = transform.translate(-boundingBox.cx, -boundingBox.cy);
        this.zoom.transform(this.svg, transform);
    }

    setNodeSize(nodeSize) {
        this.nodeSize = nodeSize;
        this.simulation.force("repulsion").distanceMin(2 * this.nodeSize);
        this.simulation.force("collide").radius(this.nodeSize);
        this.nodes.select("circle").attr("r", this.nodeSize);
        this.nodes.select("text").attr("dy", 20 + this.nodeSize);
        this.nodes.select(".expand-zone").attr("transform", "translate(" + (Math.sqrt(2) / 2 * this.nodeSize) + ", " + (Math.sqrt(2) / 2 * this.nodeSize) + ")");
        this.restart(this.alphaDrag, 500);
    }

    setLinkDistance(linkDistance) {
        this.linkDistance = linkDistance;
        this.simulation.force("link").distance(this.linkDistance);
        this.restart(this.alphaDrag, 500);
    }

    setLinkStrength(linkStrength) {
        this.linkStrength = linkStrength;
        this.simulation.force("link").strength(this.linkStrength);
        this.restart(this.alphaDrag, 500);
    }

    setRepulsionStrength(repulsionStrength) {
        this.repulsionStrength = repulsionStrength;
        this.simulation.force("repulsion").strength(this.repulsionStrength);
        this.restart(this.alphaDrag, 500);
    }

    setRepulsionMaxDistance(repulsionMaxDistance) {
        this.repulsionMaxDistance = repulsionMaxDistance;
        this.simulation.force("repulsion").distanceMax(this.repulsionMaxDistance === undefined ? Infinity : this.repulsionMaxDistance);
        this.restart(this.alphaDrag, 500);
    }

    setCenterForce(centerForce) {
        this.centerForce = centerForce;
        if (this.centerForce) {
            let bcr = this.svg.node().getBoundingClientRect();
            this.simulation.force("center", d3.forceCenter(bcr.width / 2, bcr.height / 2))
        }
        else
            this.simulation.force("center", null);
        this.restart(this.alphaDrag, 0);
    }

    setBoundingBox(boundingBox) {
        this.boundingBox = boundingBox;
        this.restart(this.alphaDrag, 500);
    }

    step() {
        // One step of the node simulation
        let nodeSize = this.nodeSize;

        // Bounded simulation
        if (this.boundingBox) {
            let clampx = clamp(this.boundingBox.x, this.boundingBox.x + this.boundingBox.width);
            let clampy = clamp(this.boundingBox.y, this.boundingBox.y + this.boundingBox.height);
            this.graph.getNodes().forEach(function (node) {
                if (node.x)
                    node.x = clampx(node.x);
                if (node.y)
                    node.y = clampy(node.y);
                if (node.fx)
                    node.fx = clampx(node.fx);
                if (node.fy)
                    node.fy = clampy(node.fy);
            });
        }

        this.nodes.attr("transform", d => "translate(" + d.x + ", " + d.y + ")");

        // Place links at their correct positions
        this.links
            .each(d => d.angle = Math.atan2(d.getTarget().y - d.getSource().y, d.getTarget().x - d.getSource().x))
            .select("line")
            .attr("x1", d => d.getSource().x + Math.cos(d.angle) * nodeSize)
            .attr("y1", d => d.getSource().y + Math.sin(d.angle) * nodeSize)
            .attr("x2", d => d.getTarget().x - Math.cos(d.angle) * nodeSize)
            .attr("y2", d => d.getTarget().y - Math.sin(d.angle) * nodeSize);

        // Place link labels at their correct positions
        const textOffset = 4;
        this.links
            .each(function (d) {
                const offset = d.getSource().x < d.getTarget().x ? textOffset : -textOffset
                const angle = d.getSource().x < d.getTarget().x ? d.angle : (d.angle + Math.PI)
                d.dx = (d.getSource().x + d.getTarget().x) / 2 + Math.sin(d.angle) * offset;
                d.dy = (d.getSource().y + d.getTarget().y) / 2 - Math.cos(d.angle) * offset;
                d.rotation = angle * 180 / Math.PI;
            })
            .select("text")
            .attr("transform", d => "translate(" + d.dx + " " + d.dy + ") rotate(" + d.rotation + ") ");
    }

    startDragging(node) {
        if (!d3.event.active)
            this.simulation.alphaTarget(this.alphaDrag).restart();
        d3.select("body").classed("dragging", true);
    }

    dragging(node) {
        node.fx = d3.event.x;
        node.fy = d3.event.y;
    }

    endDragging(node) {
        if (!d3.event.active)
            this.simulation.alphaTarget(0);
        node.fx = null;
        node.fy = null;
        d3.select("body").classed("dragging", false);
    }

    restart(alpha, decayTime = 2000) {
        // Restart the simulation

        alpha = alpha || this.alpha;
        if (alpha < this.simulation.alpha())
            return;
        this.simulation.alpha(alpha).restart();
        if (this.isMainSimulation) {
            this.alphaAnimation();
            if (decayTime > 0) {
                this.simulation.alphaDecay(0);
                let decay = d3.forceSimulation().alphaDecay();
                setTimeout(() => this.simulation.alphaDecay(decay), decayTime);
            }
        }
    }

    alphaAnimation() {
        // Animate the alpha slider under the "Restart simulation" button
        if (this.alphaAnimationActive)
            return;
        this.alphaAnimationActive = true;

        let sim = this;

        function animationFrame() {
            let alpha = sim.simulation.alpha();
            var percentage = 100 * alpha / ctx.ALPHA;
            d3.select("#layout-alpha div").style("width", percentage + "%");
            if (alpha > sim.simulation.alphaMin()) {
                requestAnimationFrame(animationFrame);
            }
            else
                sim.alphaAnimationActive = false;
        }

        requestAnimationFrame(animationFrame);
    }

    setNodePosition(node) {
        // Initialize node positions
        if (node.hasPosition())
            return;
        let neighbors = this.graph.getNeighbors(node).filter(n => n.node.hasPosition());

        if (neighbors.length == 0) {
            let bcr = this.svg.node().getBoundingClientRect();
            node.x = bcr.width / 2;
            node.y = bcr.height / 2;
        }
        else {
            let x = neighbors.map(d => d.node.x).reduce((a, b) => a + b, 0) / neighbors.length;
            let y = neighbors.map(d => d.node.y).reduce((a, b) => a + b, 0) / neighbors.length;
            node.x = x;
            node.y = y;
        }
        node.x += (Math.random() - .5) * 5 * this.nodeSize;
        node.y += (Math.random() - .5) * 5 * this.nodeSize;
    }

    updateGraphData() {
        // Nodes
        let nodes = this.svg.select(".nodes")
            .selectAll(".node")
            .data(this.graph.getNodes(), d => d.getId());
        console.log(this.graph.getNodes());
        let nodesEnter = nodes.enter()
            .each(d => this.setNodePosition(d))
            .append("g")
            .classed("node", true);
        let circles = nodesEnter.append("circle")
            .attr("r", this.nodeSize)
            .style("fill", d => d.getType().color);
        nodesEnter.append("text")
            .html(d => d.getLabelHtml(this.graph))
            .attr("text-anchor", "middle")
            .attr("dy", 20 + this.nodeSize);
        if (this.isMainSimulation) {
            let sim = this;
            circles.on("click", expandNode)
                .on("contextmenu", removeNode)
                .on("mouseover", highlightNeighbors)
                .on("mouseout", unhighlightAll)
            nodesEnter.call(d3.drag().on("start", node => sim.startDragging(node))
                .on("drag", node => sim.dragging(node))
                .on("end", node => sim.endDragging(node)));
        }
        nodes.exit().remove();
        nodesEnter.merge(nodes)
            .select("text")
            .html(d => d.getLabelHtml(this.graph));

        // Links
        let links = this.svg.select(".links")
            .selectAll(".edge")
            .data(this.graph.getLinks(), d => d.getId());
        let linksEnter = links.enter().append("g")
            .classed("hidden", true)
            .classed("edge", true)
            .classed("same-as", link => link.isSameAs());
        let lines = linksEnter.append("line");
        lines //.filter(link => !link.isSameAs())
            .attr("marker-end", "url(#arrowhead_" + this.svg.attr("id") + ")");
        linksEnter.append("text")
            .text(link => link.getLabel())
            .attr("text-anchor", "middle")
        links.exit().remove();

        this.nodes = this.svg.selectAll(".nodes .node");
        this.links = this.svg.selectAll(".links .edge");

        this.simulation.nodes(this.graph.getNodes());
        this.simulation.force("link").links(this.graph.getLinks());

        if (this.isMainSimulation) {
            // linksEnter.classed("filteredIn", link => Filter.isFilteredIn(link.source) && Filter.isFilteredIn(link.target));
            // nodesEnter.classed("filteredIn", node => Filter.isFilteredIn(node));
            let open = nodesEnter
                .filter(node => node.getType().typeName === "TLRECORD")
                .append("g")
                .classed("expand-zone", true)
                .classed("open", node => node.f.checked)
                .attr("transform", "translate(" + (Math.sqrt(2) * this.nodeSize / 2) + ", " + (Math.sqrt(2) / 2 * this.nodeSize) + ")")
                .on("click", toggleParentFilter)
                .on("mouseover", highlightZone)
                .on("mouseout", unhighlightAll);
            open.append("rect")
                .attr("x", -5)
                .attr("y", -5)
                .attr("width", 10)
                .attr("height", 10)
            open.append("line")
                .classed("h", true)
                .attr("x1", -3)
                .attr("y1", 0)
                .attr("x2", 3)
                .attr("y2", 0);
            open.append("line")
                .classed("v", true)
                .attr("x1", 0)
                .attr("y1", -3)
                .attr("x2", 0)
                .attr("y2", 3);
        }

        // For non-main simulations, do all simulation steps at once
        if (!this.isMainSimulation) {
            this.simulation.alpha(this.alpha);
            this.simulation.alphaDecay(0);
            var nsteps = 120;
            for (var i = 0; i < nsteps; ++i)
                this.simulation.tick();
            this.simulation.alphaDecay(d3.forceSimulation().alphaDecay());
            nsteps = Math.ceil(Math.log(this.simulation.alphaMin()) / Math.log(1 - this.simulation.alphaDecay()));
            for (var i = 0; i < nsteps; ++i)
                this.simulation.tick();
            this.step();
        }
        else if (this.nodes.size())
            this.restart();
    }
}
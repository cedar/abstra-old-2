/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function exportViz() {
    var original = document.getElementById("layout");
    var svg = original.cloneNode(true);
    svg.id = "export-layout";

    var bb = original.getBoundingClientRect();
    svg.width = bb.width;
    svg.height = bb.height;

    // Add legend
    var legendbb = document.getElementById("legend").getBoundingClientRect();
    var legend = d3.select(svg)
        .append("g")
        .attr("id", "export-legend")
        .attr("transform", "translate(" + (bb.width - legendbb.width - 20) + " 20)");
    legend.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("rx", 3)
        .attr("ry", 3)
        .attr("width", legendbb.width)
        .attr("height", legendbb.height)
    var legenditems = legend.selectAll("g")
        .data(document.querySelectorAll("#legend .legend-item:not(.hidden)"))
        .enter()
        .append("g")
        .attr("transform", (_, i) => "translate(6 " + (32 * i + 6) + ")");
    legenditems.append("text")
        .text(d => d.querySelector(".type-name").textContent)
        .attr("dx", 32)
        .attr("dy", 22);
    legenditems.append("rect")
        .attr("x", 6).attr("y", 6)
        .attr("width", 20).attr("height", 20)
        .style("fill", d => d.querySelector(".type-color").style.backgroundColor);

    // parse the styles
    parseStyles(svg);

    var docType = document.implementation.createDocumentType("svg", "-//W3C//DTD SVG 1.1//EN", "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");
    var doc = document.implementation.createDocument("http://www.w3.org/2000/svg", "svg", docType);
    doc.replaceChild(svg, doc.documentElement);
    var data = new XMLSerializer().serializeToString(doc);

    var blob = new Blob([data], { type: "image/svg+xml;charset=utf-8" });
    var url = URL.createObjectURL(blob);

    var a = document.createElement("a");
    a.href = url;
    a.download = "ConnectionLens.svg";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    URL.revokeObjectURL(url);
};

function filterStyle(selectorText, style) {
    console.log(selectorText, style);
    if (selectorText === "#layout .links text")
        style["fill"] = "black";
    else if (selectorText === "#layout")
        return {};
    else if (selectorText === "#layout .nodes tspan")
        return {};
    else if (selectorText === "#layout .nodes circle")
        delete style["cursor"];
    return style;
}

var parseStyles = function (svg) {
    var styleSheets = [];
    var i;
    var docStyles = svg.ownerDocument.styleSheets;

    for (i = 0; i < docStyles.length; i++)
        styleSheets.push(docStyles[i]);

    if (!styleSheets.length)
        return;

    var defs = svg.querySelector('defs') || document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    if (!defs.parentNode) {
        svg.insertBefore(defs, svg.firstElementChild);
    }
    svg.matches = svg.matches || svg.webkitMatchesSelector || svg.mozMatchesSelector || svg.msMatchesSelector || svg.oMatchesSelector;

    for (i = 0; i < styleSheets.length; i++) {
        var currentStyle = styleSheets[i]

        var rules;
        try {
            rules = currentStyle.cssRules;
        } catch (e) {
            continue;
        }
        var style = document.createElement('style');
        var l = rules && rules.length;
        for (var j = 0; j < l; j++) {
            var selector = rules[j].selectorText;
            if (!selector) {
                continue;
            }
            if ((svg.matches && svg.matches(selector)) || svg.querySelector(selector)) {
                var cssText = rules[j].cssText;
                style.innerHTML += cssText + '\n';
            }
        }
        if (style.innerHTML)
            defs.appendChild(style);
    }

};


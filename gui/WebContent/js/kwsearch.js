/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function clickAnswerTree() {
    // Select or deselect an answer tree in the side panel

    let target = d3.select(this.parentNode);
    if (target.classed("loading"))
        return;
    target.classed("selected", !target.classed("selected"));
}

function dblClickAnswerTree() {
    // Handle double clicking on an answer tree in the side panel

    addAnswerTrees(d3.select(this.parentNode));
}

function mouseOverAnswerTree() {
    // Show the graph layout of an answer tree

    let offsetx = this.offsetWidth;
    let offsety = this.getBoundingClientRect().y - document.getElementById("kwsearch").getBoundingClientRect().y;
    d3.select(this).select(".overflow-wrapper").style("left", offsetx + "px").style("top", offsety + "px");
}

function createAnswerTreeLinks(trees) {
    // Create answer tree elements in the side panel

    const size = { width: 500, height: 300 };
    const simParameters = {
        boundingBox: { x: 30, y: 30, width: size.width - 60, height: size.height - 60 },
        linkDistance: 15,
        repulsionMaxDistance: Infinity,
        nodeSize: 5,
        alpha: 15,
        centerForce: true
    };

    let treeItems = d3.select("#kwsearch-content")
        .selectAll(".tree")
        .data(trees, tree => tree.rank)
        .enter()
        .append("div")
        .classed("tree", true)
        .on("mouseover", mouseOverAnswerTree);
    let treeLinks = treeItems.append("a")
        .attr("href", "javascript:void(0);")
        .on("click", clickAnswerTree)
        .on("dblclick", dblClickAnswerTree)
    let treeInfo = treeLinks.append("div")
        .classed("tree-info", true);
    treeInfo.append("div")
        .classed("tree-rank", true)
        .text(tree => "Rank: #" + tree.rank);
    treeInfo.append("div")
        .classed("tree-size", true)
        .text(tree => "Size: " + tree.getNodes().length);
    treeInfo.append("div")
        .classed("tree-score", true)
        .text(tree => "Score: " + (Math.round(tree.score * 100) / 100));
    let sources = treeInfo.append("div")
        .classed("sources", true)
        .text(tree => tree.getSources().length + " source" + (tree.getSources().length > 1 ? "s" : ""));
    treeItems.append("div")
        .classed("overflow-wrapper", true)
        .append("svg")
        .attr("id", tree => tree.rank)
        .attr("width", size.width)
        .attr("height", size.height)
        .each((tree, i, svgs) => new Simulation(tree, d3.select(svgs[i]), simParameters));

    treeLinks.append("div")
        .classed("loader", true);
}

function keywordSearch() {
    // Execute a keyword search

    let query = d3.select("#query").property("value");
    if (!query)
        return;

    closeSidePanels();

    d3.select("#kwsearch").classed("hidden", false);
    d3.select("#layout-background").classed("hidden", false);

    // If same query, use cached results
    if (ctx.lastKwsearch === query)
        return;

    // Delete cache
    delete ctx.lastKwsearch;

    // Cancel requests in progress
    if (ctx.kwsearchPromise)
        ctx.kwsearchPromise.cancel();

    ctx.kwsearchScrollable = false;

    d3.select("#kwsearch").classed("loading", true);
    d3.select("#kwsearch-header h2").text(query);
    d3.select("#kwsearch-content").html("").classed("loading", false);

    ctx.kwsearchPromise = cancelablePromise(getKeywordSearchResults(query, 0));
    ctx.kwsearchPromise
        .then(function (trees) {
            delete ctx.kwsearchPromise;

            // Cache results
            ctx.lastKwsearch = query;
            ctx.lastKwsearchPage = 0;
            ctx.kwsearchScrollable = true;

            d3.select("#kwsearch").classed("loading", false);

            createAnswerTreeLinks(trees);

            kwsearchScroll(d3.select("#kwsearch-content").node());
        })
        .catch(function (err) {
            if (err && err.isCanceled)
                return;
            closeSidePanels();
            ctx.kwsearchScrollable = false;
            d3.select("#kwsearch").classed("loading", false);
            ErrorQueue.enqueue(err);
        });
}

function kwsearchScroll(target) {
    if (!ctx.kwsearchScrollable)
        return;
    if (target.scrollHeight - target.offsetHeight - target.scrollTop < 400) {
        // Get another page of keyword search results
        ctx.kwsearchScrollable = false;
        ctx.lastKwsearchPage++;

        d3.select("#kwsearch-content").classed("loading", true);

        ctx.kwsearchPromise = cancelablePromise(getKeywordSearchResults(ctx.lastKwsearch, ctx.lastKwsearchPage));
        ctx.kwsearchPromise
            .then(function (trees) {
                delete ctx.kwsearchPromise;

                ctx.kwsearchScrollable = trees.length > 0;

                d3.select("#kwsearch-content").classed("loading", false);

                createAnswerTreeLinks(trees);

                kwsearchScroll(target);
            })
            .catch(function (err) {
                if (err && err.isCanceled)
                    return;
                ctx.kwsearchScrollable = true;
                d3.select("#kwsearch-content").classed("loading", false);
                ErrorQueue.enqueue(err);
            });
    }
}

function selectAllAnswerTrees() {
    // Select all answer tree elements in the side panel

    let nodes = d3.selectAll("#kwsearch-content .tree:not(.loading)");
    let allSelected = nodes.filter(":not(.selected)").empty();
    nodes.classed("selected", !allSelected);
}

function addAllAnswerTrees() {
    // Add all answer trees to the layout

    let nodes = d3.selectAll("#kwsearch-content .tree");
    addAnswerTrees(nodes);
}

function addSelectedAnswerTrees() {
    // Add the selected answer trees to the layout

    let selectedNodes = d3.selectAll("#kwsearch-content .tree.selected");
    addAnswerTrees(selectedNodes);
}

function addAnswerTrees(nodes) {
    // Add answer trees to the layout

    nodes = nodes.filter(":not(.loading)");

    if (nodes.empty())
        return;

    nodes.classed("loading", true).classed("selected", true);

    let query = ctx.lastKwsearch;
    let graph = AnswerTree.merge(nodes.data()); // this adds to the central display panel
    // all the edges adjacent to these nodes - this is why fixing #538 took a day

    let oldNodes = ctx.graph.getNodeIds(); 
    let newNodes = graph.getNodeIds(); 

    ctx.nodeAddQueue
        .enqueue(() => getLinks(oldNodes, newNodes))
        .then(function (response) {
            ctx.graph.addGraphResults(response);

            updateLayout();

            // Add filters to filter nodes in simulation
            KwsearchFilter.add(query);
            graph.getNodeIds().forEach(function (nodeId) {
                ctx.graph.getNode(nodeId).queries.add(query);
            });
            Filter.applyFilters();

            nodes.remove();
            kwsearchScroll(d3.select("#kwsearch-content").node());
        })
        .catch(function (err) {
            nodes.classed("loading", false);
            ErrorQueue.enqueue(err);
        });
}
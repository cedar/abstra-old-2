/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

const TYPES = [
    {
        typeName: "DATA_SOURCE",
        names: ["DATA_SOURCE"],
        displayName: "Data source",
        pluralDisplayName: "Data sources",
        color: "#e15759"
    },
    {
        typeName: "RECORD_PERSON",
        names: ["RECORD_PERSON"],
        displayName: "Person",
        pluralDisplayName: "Persons",
        color: "#008000"
    },
    {
        typeName: "RECORD_LOCATION",
        names: ["RECORD_LOCATION"],
        displayName: "Location",
        pluralDisplayName: "Locations",
        color: "#FFFF00"
    },
    {
        typeName: "RECORD_ORGANIZATION",
        names: ["RECORD_ORGANIZATION"],
        displayName: "Organization",
        pluralDisplayName: "Organizations",
        color: "#800080"
    },
    {
        typeName: "RECORD_EVENT",
        names: ["RECORD_EVENT"],
        displayName: "Event",
        pluralDisplayName: "Events",
        color: "#00FFFF"
    },
    {
        typeName: "RECORD_CREATIVEWORK",
        names: ["RECORD_CREATIVEWORK"],
        displayName: "Creative work",
        pluralDisplayName: "Creative works",
        color: "#A52A2A"
    },
    {
        typeName: "RECORD_CREATIVE_WORK",
        names: ["RECORD_CREATIVE_WORK"],
        displayName: "Creative work",
        pluralDisplayName: "Creative works",
        color: "#A52A2A"
    },
    {
        typeName: "RECORD_PRODUCT",
        names: ["RECORD_PRODUCT"],
        displayName: "Product",
        pluralDisplayName: "Products",
        color: "#f4a460"
    },
    {
        typeName: "RECORD_OTHER",
        names: ["RECORD_OTHER"],
        displayName: "Other",
        pluralDisplayName: "Others",
        color: "#f28e2c"
    },
    {
        typeName: "SUB_RECORD",
        names: ["SUB_RECORD"],
        displayName: "Sub-record",
        pluralDisplayName: "Sub-records",
        color: "#34495e"
    },
    {
        typeName: "COLLECTION",
        names: ["COLLECTION"],
        displayName: "Collection",
        pluralDisplayName: "Collections",
        color: "#4e79a7"
    },
    {
        typeName: "ENTITY_PERSON",
        names: ["ENTITY_PERSON"],
        displayName: "Person",
        pluralDisplayName: "Persons",
        color: "#76b7b2"
    },
    {
        typeName: "ENTITY_LOCATION",
        names: ["ENTITY_LOCATION"],
        displayName: "Location",
        pluralDisplayName: "Locations",
        color: "#59a14f"
    },
    {
        typeName: "ENTITY_ORGANIZATION",
        names: ["ENTITY_ORGANIZATION"],
        displayName: "Organization",
        pluralDisplayName: "Organizations",
        color: "#edc949"
    },
    {
        typeName: "URI",
        names: ["URI", "RDF_URI"],
        displayName: "URI",
        pluralDisplayName: "URIs",
        color: "#af7aa1"
    },
    {
        typeName: "HASHTAG",
        names: ["HASHTAG"],
        displayName: "Hashtag",
        pluralDisplayName: "Hashtags",
        color: "#ff9da7"
    },
    {
        typeName: "EMAIL",
        names: ["EMAIL"],
        displayName: "Email",
        pluralDisplayName: "Emails",
        color: "#9c755f"
    },
    {
        typeName: "DATE",
        names: ["DATE"],
        displayName: "Date",
        pluralDisplayName: "Dates",
        color: "#9932CC"
    },
    {
        typeName: "MENTION",
        names: ["MENTION"],
        displayName: "Mention",
        pluralDisplayName: "Mentions",
        color: "#99821C"
    },
    {
        typeName: "VALUE",
        names: [
            "HTML_NODE", "HTML_VALUE", "XML_NODE", "XML_TAG_NODE", "XML_TAG_VALUE", "XML_ATTRIBUTE_VALUE", "XML_ATTRIBUTE_NODE", "XML_VALUE",
            "RDF_LITERAL", "RELATIONAL_STRUCT", "RELATIONAL_VALUE",
            "TEXT_VALUE", "FIRST_NAME", "JSON_VALUE", "JSON_STRUCT",
            "AMBI_NODE", "DO_NOT_LINK_VALUE", "NEO4J_ENTITY", "NEO4J_VALUE",
            "NEO4J_EDGE", "NORMALIZATION_NODE", "NORMALIZATION_NODE_EXTRACTION"
        ],
        displayName: "Data node",
        pluralDisplayName: "Data nodes",
        color: "#bab0ab"
    }
];

function initializeTypes() {
    ctx.types = TYPES.map(function (type) {
        type.names = new Set(type.names);
        type.checked = true;
        return type;
    });

    initializeToolbarButtons();
    initializeLegend();
}

function initializeToolbarButtons() {
    const circle_arc = ((t1, delta) => {
        const [sX, sY] = [1 * Math.cos(t1), 1 * Math.sin(t1)];
        const [eX, eY] = [1 * Math.cos(t1 + delta), 1 * Math.sin(t1 + delta)];
        const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        path.setAttribute("d", "M " + sX + " " + sY + " A " + [1, 1, 0, ((delta > Math.PI) ? 1 : 0), ((delta > 0) ? 1 : 0), eX, eY].join(" ") + " L 0 0 Z");
        return path;
    });

    const delta = 2 * Math.PI / ctx.types.length;
    d3.select("#neighbors-toolbar a")
        .append("svg")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("viewBox", "-1 -1 2 2")
        .selectAll("path")
        .data(ctx.types)
        .enter()
        .append((_, i) => circle_arc(i * delta, delta))
        .style("fill", type => type.color);

    d3.select("#kwsearch-toolbar a")
        .append("svg")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("viewBox", "-1 -1 2 2")
        .selectAll("path")
        .data(ctx.types)
        .enter()
        .append((_, i) => circle_arc(i * delta, delta))
        .style("fill", type => type.color);

    d3.select("#neighbors-toolbar-types")
        .selectAll("a")
        .data(ctx.types)
        .enter()
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("id", type => "neighbors-toolbar-button-" + type.displayName.replace(" ", "-"))
        .attr("title", type => "Select all " + type.pluralDisplayName.toLowerCase())
        .style("background-color", type => type.color)
        .on("click", type => selectTypeNeighbors(type))
        .on("dblclick", type => addTypeNeighbors(type));
}

function initializeLegend() {
    let legend = d3.select("#legend")
        .selectAll(".legend-item")
        .data(ctx.types)
        .enter()
        .append("a")
        .attr("id", type => "legend-" + type.typeName)
        .attr("href", "javascript:void(0);")
        .classed("legend-item", true)
        .classed("checked", type => type.checked)
        .on("click", toggleTypeFilter);
    legend.append("div")
        .classed("type-color", true)
        .style("background-color", type => type.color);
    legend.append("div")
        .classed("type-name", true)
        .text(type => type.displayName);
    legend.on("mouseover", highlightNodeType)
        .on("mouseout", unhighlightAll);
}

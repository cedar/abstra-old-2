/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function openAbout() {
    // Open the "About" popup
    d3.selectAll(".popup").classed("hidden", true);
    d3.select("#popup-about").classed("hidden", false);
    d3.select("#popup-container").classed("hidden", false);
}

function openChooseFile() {
    // Open the "Choose files" popup

    d3.selectAll(".popup").classed("hidden", true);
    d3.select("#popup-choose-file").classed("hidden", false);
    d3.select("#popup-container").classed("hidden", false);
}

function importFileDrop(event) {
    // Handle dragging a file on the "Choose files" popup

    let dt = event.dataTransfer;

    // Remove directories
    let files = [...dt.files].filter(f => f.type || f.size % 4096 != 0);

    openImport(files);
}

function openImport(files) {
    // Open the "Import" popup

    if (!files.length)
        return;
    d3.selectAll(".popup").classed("hidden", true);
    d3.select("#popup-import").classed("hidden", false);
    d3.select("#popup-container").classed("hidden", false);
    d3.select("#import-db-name").classed("hidden", d3.select("#import-db-select").node().value.length);
    d3.select("#popup-import #db-name-error").html("");
    let list = d3.select("#popup-import .popup-content #file-list")
        .html("");
    let rows = list.selectAll(".row")
        .data(files)
        .enter()
        .append("div")
        .classed("row", true);
    let options = rows.append("div")
        .classed("file-options", true);
    options.append("div")
        .classed("file-name", true)
        .text(file => file.name);
    options.append("input")
        .classed("original-url", true)
        .attr("type", "text")
        .attr("placeholder", "Original URL");
    rows.append("div")
        .classed("upload-error", true);
}

function importFiles() {
    // Upload files

    let createDB = false;
    let database = d3.select("#import-db-select").node().value.toLowerCase();
    if (!database) {
        database = d3.select("#import-db-name").node().value.toLowerCase();

        // Check that the user-created database name is not already in the database list
        createDB = !new Set(ctx.databases).has(database);
    }
    if (!database)
        return d3.select("#popup-container #db-name-error").html("Database name cannot be empty");
    if (!database.match(/^[a-z0-9_]+$/g))
        return d3.select("#popup-container #db-name-error").html("Database name can only have lowercase letters, numbers, and underscores (_)");
    d3.select("#popup-container #db-name-error").html("");

    // Get file list with original URLs
    let files = [];
    d3.selectAll("#popup-import .row").each(function (file, i, els) {
        files.push({
            file: file,
            originalURL: d3.select(els[i]).select("input").node().value,
            node: els[i]
        });
    });
    if (!files.length)
        return;

    d3.select("#import").classed("loading", true);

    // Import files one by one
    let queue = new PromiseQueue();
    let errors = [];
    let createSuccess = false;
    files.forEach(function (file, i) {
        queue.enqueue(() => importFile(file.file, file.originalURL, database))
            .then(function () {
                if (createDB)
                    createSuccess = true;
                d3.select(file.node).remove();
            })
            .catch(function (err) {
                errors.push({ "message": err, "node": d3.select(file.node).select(".upload-error") });
            });
    });

    // Wait until the uploads are all finished
    queue.whenFinished()
        .then(function () {
            d3.select("#import").classed("loading", false);

            if (createSuccess) {
                // If a new database was created, add it to the database list
                ctx.databases.push(database);
                updateDatabaseList();
            }
            if (errors.length) {
                // If there are errors re-open the popup with only the relevant
                // lines and error messages
                d3.select("#popup-import").classed("hidden", false);
                d3.select("#popup-container").classed("hidden", false);
                errors.forEach(error => error.node.text(error.message));
            }
        });

    // Hide the popup during the upload
    closePopup();
}

function closePopup() {
    // Hide popups
    d3.select("#popup-container").classed("hidden", true);
}
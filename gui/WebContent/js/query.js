/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

async function getNeighbors(node, oldNodes, page) {
    // Fetch neighbors of a node in database
    console.log("getNeighbors");
    console.log("getNeighbors("+node+", "+oldNodes+", "+page+")");

    let neighbors = await makeRequest("POST", "neighbors", { "n": node.getId(), "on": oldNodes, "p": page, "d": ctx.database.database, "a": ctx.database.useAbstract });
    neighbors.forEach(function (neighbor) {
        neighbor.node = new GraphNode(neighbor.node);
        neighbor.link = new GraphEdge(neighbor.link);
        neighbor.parent = node;
    });
    return neighbors;
}

async function getLinks(oldNodes, newNodes) {
    // Fetch links with newNodes from database

    let response = await makeRequest("POST", "links", { "on": oldNodes, "nn": newNodes, "d": ctx.database.database, "a": ctx.database.useAbstract });
    response.links = response.links.map(link => new GraphEdge(link));
    response.nodes = response.nodes.map(node => new GraphNode(node));
    return new Graph(response);
}

async function getKeywordSearchResults(query, page) {
    // Fetch keyword search results in database

    let trees = await makeRequest("POST", "kwsearch", { "q": query, "p": page, "d": ctx.database.database, "a": ctx.database.useAbstract });

    trees = trees.map(function (tree) {
        let globalNodeIds = Object.fromEntries(tree.nodes.map(node => [node.local_id, node.global_id]));
        tree.links.forEach(function (link) {
            delete link.local_id;
            link.source = globalNodeIds[link.source];
            link.target = globalNodeIds[link.target];
        });
        tree.nodes.forEach(function (node) {
            delete node.local_id;
        });

        tree.nodes = tree.nodes.map(node => new GraphNode(node));
        tree.links = tree.links.map(edge => new GraphEdge(edge));

        return new AnswerTree(tree);
    });

    return trees;
}

function importFile(file, originalURL, database) {
    // Upload a file and import it in the database

    return new Promise(function (resolve, reject) {
        var formData = new FormData();
        var xhr = new XMLHttpRequest();

        xhr.onload = function () {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve();
            }
            else {
                let doc = new DOMParser().parseFromString(xhr.responseText, "text/html");
                let msg = doc.querySelectorAll("p")[1].childNodes[1].textContent;
                reject("Error " + xhr.status + ": " + msg);
            }
        };

        formData.set("file", file);
        formData.set("u", originalURL);
        formData.set("d", database);
        xhr.open("POST", "import");
        xhr.send(formData);
    });
}
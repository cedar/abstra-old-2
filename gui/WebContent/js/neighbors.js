/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function clickNeighbor() {
    // Select or deselect a neighbor in the side panel

    let target = d3.select(this);
    if (target.classed("loading"))
        return;
    target.classed("selected", !target.classed("selected"));
}

function dblClickNeighbor() {
    // Handle double clicking on a neighbor in the side panel

    addNeighbors(d3.select(this));
}

function createNeighborLinks(neighbors) {
    // Create neighbor elements in the side panel

    let neighborLinks = d3.select("#neighbors-content")
        .selectAll("a")
        .data(removeDuplicates(neighbors, neighbor => neighbor.node.getId()))
        .enter()
        .append("a")
        .classed("neighbor", true)
        .attr("href", "javascript:void(0);")
        .on("dblclick", dblClickNeighbor)
        .on("click", clickNeighbor);
    neighborLinks.append("div")
        .classed("node-type", true)
        .style("background-color", d => d.node.getType().color)
    let description = neighborLinks.append("div")
        .classed("node-description", true);
    description.append("span")
        .classed("node-label", true)
        .text(d => d.node.getLabel());
    description.append("span")
        .classed("space", true)
        .html("&nbsp;·&nbsp;");
    description.append("span")
        .classed("node-link-label", true)
        .text(d => d.link.label);
    neighborLinks.append("div")
        .classed("loader", true);
}

function expandNode(node) {
    // Get neighbors of a node, in order to add them to the layout

    closeSidePanels();

    d3.select("#neighbors").classed("hidden", false);
    d3.select("#context").classed("hidden", false);
    d3.select("#layout-background").classed("hidden", false);

    // If same query, use cached results
    if (ctx.lastNeighbors === node)
        return;

    // Delete cache
    delete ctx.lastNeighbors;

    // Cancel requests in progress
    if (ctx.neighborsPromise)
        ctx.neighborsPromise.cancel();

    ctx.neighborsScrollable = false;

    d3.select("#neighbors").classed("loading", true);
    d3.select("#neighbors-header h2").text(node.getLabel());
    d3.select("#neighbors-content").html("").classed("loading", false);
    d3.select("#context-node").html(node.getContext());
    d3.select("#context-substructure").html("")
    if (node.getType().typeName === "TLRECORD") {
        let substructure = node.getSubstructureContext().classed("open", true);
        d3.select("#context-substructure").append(() => substructure.node());
    }

    ctx.neighborsPromise = cancelablePromise(getNeighbors(node, ctx.graph.getNodeIds(), 0));
    ctx.neighborsPromise
        .then(function (neighbors) {
            delete ctx.neighborsPromise;

            // Cache results
            ctx.lastNeighbors = node;
            ctx.lastNeighborsPage = 0;
            ctx.neighborsScrollable = neighbors.length > 0;

            d3.select("#neighbors").classed("loading", false);

            console.log("start createNeighborLinks");
            console.log("start createNeighborLinks(" + neighbors + ")");
            createNeighborLinks(neighbors);
            console.log("filterToolbarButtons");
            filterToolbarButtons();

            neighborsScroll(d3.select("#neighbors-content").node());
        })
        .catch(function (err) {
            if (err && err.isCanceled)
                return;
            d3.select("#neighbors").classed("loading", false);
            ErrorQueue.enqueue(err);
        });
}

function neighborsScroll(target) {
    if (!ctx.neighborsScrollable)
        return;
    if (target.scrollHeight - target.offsetHeight - target.scrollTop < 400) {
        // Get another page of neighbors
        ctx.neighborsScrollable = false;
        ctx.lastNeighborsPage++;

        d3.select("#neighbors-content").classed("loading", true);

        ctx.neighborsPromise = cancelablePromise(getNeighbors(ctx.lastNeighbors, ctx.graph.getNodeIds(), ctx.lastNeighborsPage));
        ctx.neighborsPromise
            .then(function (neighbors) {
                delete ctx.neighborsPromise;

                ctx.neighborsScrollable = neighbors.length > 0;

                d3.select("#neighbors-content").classed("loading", false);

                createNeighborLinks(neighbors);
                filterToolbarButtons();

                neighborsScroll(target);
            })
            .catch(function (err) {
                if (err && err.isCanceled)
                    return;
                ctx.neighborsScrollable = true;
                d3.select("#neighbors-content").classed("loading", false);
                ErrorQueue.enqueue(err);
            });
    }
}

function selectAllNeighbors() {
    // Select all neighbor elements in the side panel

    let nodes = d3.selectAll("#neighbors-content .neighbor:not(.loading)");
    let allSelected = nodes.filter(":not(.selected)").empty();
    nodes.classed("selected", !allSelected);
}

function selectTypeNeighbors(type) {
    // Select neighbor elements of a certain type in the side panel

    let nodes = d3.selectAll("#neighbors-content .neighbor:not(.loading)")
        .filter(d => d.node.getType() === type);
    let allSelected = nodes.filter(":not(.selected)").empty();
    nodes.classed("selected", !allSelected);
}

function addAllNeighbors() {
    // Add all neighbors to the layout

    let nodes = d3.selectAll("#neighbors-content .neighbor");
    addNeighbors(nodes);
}

function addTypeNeighbors(type) {
    // Add neighbors of a certain type to the layout

    let nodes = d3.selectAll("#neighbors-content .neighbor")
        .filter(d => d.node.getType() === type);
    addNeighbors(nodes);
}

function addSelectedNeighbors() {
    // Add the selected neighbors to the layout

    let selectedNodes = d3.selectAll("#neighbors-content .neighbor.selected");
    if (selectedNodes.empty())
        return;
    addNeighbors(selectedNodes);
}

function addNeighbors(nodes) {
    // Add neighbors to the layout

    nodes = nodes.filter(":not(.loading)");

    if (nodes.empty())
        return;

    nodes.classed("loading", true);
    nodes.classed("selected", true);

    filterToolbarButtons();

    let neighbors = nodes.data();
    let oldNodes = new Set(ctx.graph.getNodeIds());
    let newNodes = new Set(neighbors.map(d => d.node.getId()));
    ctx.nodeAddQueue
        .enqueue(() => getLinks(oldNodes, newNodes))
        .then(function (response) {
            ctx.graph.addGraphResults(response);

            updateLayout();

            // Add filters to filter nodes in
            neighbors.forEach(function (neighbor) {
                SpecificNodeFilter.add(ctx.graph.getNode(neighbor.node.getId()));
            });
            Filter.applyFilters();

            nodes.remove();
            neighborsScroll(d3.select("#neighbors-content").node());
        })
        .catch(function (err) {
            nodes.classed("loading", false);
            filterToolbarButtons();
            ErrorQueue.enqueue(err);
        });
}
/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

var ctx = {
    ALPHA: 1,
    ALPHA_DRAG: .3,
    BOUNDING_BOX: {
        "DEFAULT": null
    },
    SETTINGS: {
        nodeSize: new RangeSetting("Node size", value => ctx.simulation.setNodeSize(value), 10, null, 1, 50),
        linkDistance: new RangeSetting("Link distance", value => ctx.simulation.setLinkDistance(value), 100, null, 5, 300),
        linkStrength: new RangeSetting("Link strength", value => ctx.simulation.setLinkStrength(value), 0.08, null, 0.01, 0.90, 0.01),
        repulsionStrength: new RangeSetting("Repulsion strength", value => ctx.simulation.setRepulsionStrength(value), -10, null, -50, -1),
        repulsionMaxDistance: new RangeSetting("Repulsion max distance", value => ctx.simulation.setRepulsionMaxDistance(value), 200, true, 5, 300),
        centerForce: new CheckboxSetting("Center force", value => ctx.simulation.setCenterForce(value), false),
        showEdgeLabels: new CheckboxSetting("Show all edge labels", value => showEdgeLabels(value), false)
    }
};

function updateLegend() {
    // Update the legend to only show the types of nodes that are in the view

    let types = new Set(ctx.graph.getNodes().map(d => d.getType()));
    d3.selectAll("#legend .legend-item").classed("hidden", d => !types.has(d));

    // Hide legend if empty
    d3.select("#legend").classed("hidden", types.size == 0);
}

function filterToolbarButtons() {
    // Update the toolbar buttons to only show the types that are in the neighbors panel

    let types = new Set(d3.selectAll("#neighbors-content a:not(.loading)")
        .data().map(d => d.node.getType()));
    d3.selectAll("#neighbors-toolbar-types a")
        .classed("hidden", type => !types.has(type));
}

function updateLayout() {
    // Update the layout when nodes are added/removed

    updateLegend();

    // Node-link diagram
    ctx.simulation.updateGraphData();

    ctx.linkElements = d3.selectAll(".links .edge");
    ctx.nodeElements = d3.selectAll(".nodes .node");
}

function closeSidePanels() {
    // Close neighbors
    d3.select("#neighbors").classed("hidden", true);
    delete ctx.selectedNode;

    // Close keyword search results
    d3.select("#kwsearch").classed("hidden", true);

    // Close node context
    d3.select("#context").classed("hidden", true);

    // Hide layout background
    d3.select("#layout-background").classed("hidden", true);
}

function removeNode(node) {
    // Remove node from layout

    d3.event.preventDefault();

    if (ctx.graph.length == 1)
        return;

    ctx.graph.removeNode(node);

    ctx.filters = ctx.filters.filter(filter => filter.node !== node);
    Filter.applyFilters();

    unhighlightAll();

    updateLayout();

    delete ctx.lastNeighbors;
    delete ctx.lastNeighborsPage;
    delete ctx.neighborsScrollable;
    if (ctx.neighborsPromise) {
        ctx.neighborsPromise.cancel();
        delete ctx.neighborsPromise;
    }
    delete ctx.lastKwsearch;
    delete ctx.lastKwsearchPage;
    delete ctx.kwsearchScrollable;
    if (ctx.kwsearchPromise) {
        ctx.kwsearchPromise.cancel();
        delete ctx.kwsearchPromise;
    }
}

function highlight(isHighlighted) {
    // Highlight nodes according to key
    d3.select("#layout").classed("highlighting", true);

    ctx.nodeElements.classed("hidden", node => !isHighlighted(node));
    ctx.linkElements.classed("hidden", link => !isHighlighted(link.getSource()) || !isHighlighted(link.getTarget()));
}

function highlightNeighbors(node) {
    // Highlight the neighbors of a node
    highlight(n => ctx.graph.isConnected(n, node) || n.getId() === node.getId());
}

function highlightZone(node) {
    // Highlight the internal structure of a record
    highlight(n => node.zone.has(n));
}

function highlightNodeType(type) {
    // Highlight a certain type of nodes
    highlight(node => node.getType() === type);
}

function unhighlightAll() {
    // Remove all highlighting
    d3.select("#layout").classed("highlighting", false);
    ctx.nodeElements.classed("hidden", false);
    ctx.linkElements.classed("hidden", true);
}

function showEdgeLabels(visible) {
    d3.select("#layout").classed("labels-visible", visible);
}

function toggleTypeFilter(type) {
    // Toggle the type filter for a certain type
    type.f.toggleChecked();
    d3.select(this).classed("checked", type.f.checked);
}

function toggleParentFilter(record) {
    record.f.toggleChecked();
    d3.select(this).classed("open", record.f.checked);
}

function initializeViz() {
    // Initialize a new viz, when the page loads or when connecting to a new
    // Database
    let svg = d3.select("#layout");

    ctx.nodeAddQueue = new PromiseQueue();
    ctx.graph = new Graph();
    ctx.simulation = new Simulation(ctx.graph, svg, {}, true);
    ctx.sources = [];

    ctx.filters = [];
    ctx.types.forEach(type => TypeFilter.add(type));

    closeSidePanels();

    // Delete results caches
    delete ctx.lastKwsearch;
    delete ctx.lastKwsearchPage;
    delete ctx.kwsearchScrollable;
    delete ctx.lastNeighbors;
    delete ctx.lastNeighborsPage;
    delete ctx.neighborsScrollable;

    // Cancel requests in progress
    if (ctx.neighborsPromise) {
        ctx.neighborsPromise.cancel();
        delete ctx.neighborsPromise;
    }
    if (ctx.kwsearchPromise) {
        ctx.kwsearchPromise.cancel();
        delete ctx.kwsearchPromise;
    }

    updateLegend();
}

function updateDatabaseList() {
    let databasesWithAbstract = [];
    ctx.databases.forEach(function (database) {
        databasesWithAbstract.push({ database, useAbstract: false });
        databasesWithAbstract.push({ database, useAbstract: true });
    });
    d3.select("#db-selection ul")
        .selectAll("li")
        .data(databasesWithAbstract)
        .enter()
        .append("li")
        .append("a")
        .attr("id", d => "database_" + d.database)
        .attr("href", "javascript:void(0);")
        .on("click", d => switchDatabase(d.database, d.useAbstract))
        .text(d => d.database + (d.useAbstract ? " (abstract)" : ""));
    d3.select("#import-db-select")
        .selectAll("option.db")
        .data(ctx.databases)
        .enter()
        .append("option")
        .classed("db", true)
        .attr("value", d => d)
        .text(d => d);
}

function switchDatabase(database, useAbstract) {
    console.log(database);
    ctx.database = { database, useAbstract };
    d3.select("body").classed("no-db", false);
    d3.select("#selected-db span").text(database + (useAbstract ? " (abstract)" : ""));
    d3.selectAll("#db-selection a").classed("active", false);
    d3.select("#db-selection #" + (useAbstract ? "abstract_" : "") + "database" + database).classed("active", true);
    d3.select("#db-selection").classed("loading", false);
    d3.select("#popup-import select").node().value = ctx.database.database;
    initializeViz();
}

function initializeView() {
    ctx.databases = databases.map(database => database.toLowerCase());
    updateDatabaseList();

    initializeTypes();

    // Import drag and drop
    let dropArea = d3.select("#popup-choose-file .popup-content").node();
    ["dragenter", "dragover", "dragleave", "drop"].forEach(function (eventName) {
        dropArea.addEventListener(eventName, preventDefaults, false);
    });
    ["dragenter", "dragover"].forEach(function (eventName) {
        dropArea.addEventListener(eventName, () => dropArea.classList.add("dragover"), false);
    });
    ["dragleave", "drop"].forEach(function (eventName) {
        dropArea.addEventListener(eventName, () => dropArea.classList.remove("dragover"), false);
    });
    dropArea.addEventListener("drop", importFileDrop, false);

    ctx.importQueue = new PromiseQueue();

    if (window.database) {
        ctx.database = database;
        switchDatabase(ctx.database.database, ctx.database.useAbstract);
    }
}

initializeView();
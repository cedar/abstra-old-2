/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function saveViz() {
    let viz = {
        database: ctx.database,
        nodes: ctx.simulation.graph.save(),
        settings: Object.fromEntries(Object.entries(ctx.SETTINGS).map(e => [e[0], e[1].save()])),
        filters: Filter.save()
    };

    var data = JSON.stringify(viz);
    var blob = new Blob([data], { type: "text/json;charset=utf-8" });
    var url = URL.createObjectURL(blob);

    var a = document.createElement("a");
    a.href = url;
    a.download = "ConnectionLens.cl";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    URL.revokeObjectURL(url);
}

function loadViz() {
    let fileNode = d3.select("body")
        .append("input")
        .attr("type", "file")
        .attr("accept", ".cl")
        .style("display", "none")
        .on("input", function () {
            let files = fileNode.node().files;
            if (!files.length)
                return;
            var fr = new FileReader();
            fr.onload = function () {
                let viz = JSON.parse(fr.result);
                switchDatabase(viz.database.database, viz.database.useAbstract);
                Object.entries(viz.settings).forEach(([name, property]) => ctx.SETTINGS[name].load(property));
                ctx.nodeAddQueue
                    .enqueue(() => getLinks([], viz.nodes.map(node => node.id)))
                    .then(function (response) {

                        ctx.graph.addGraphResults(response);
                        viz.nodes.forEach(function (node) {
                            ctx.graph.getNode(node.id).x = node.x;
                            ctx.graph.getNode(node.id).y = node.y;
                        });
                        updateLayout();

                        Filter.load(viz.filters);
                    })
                    .catch(err => ErrorQueue.enqueue(err));
            }
            fr.readAsText(files[0]);
        });
    fileNode.node().click();
}
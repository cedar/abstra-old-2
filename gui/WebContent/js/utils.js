/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

function cancelablePromise(promise) {
    // Make a promise cancelable

    let hasCanceled_ = false;

    const wrappedPromise = new Promise((resolve, reject) => {
        promise.then((val) =>
            hasCanceled_ ? reject({ isCanceled: true }) : resolve(val)
        );
        promise.catch((error) =>
            hasCanceled_ ? reject({ isCanceled: true }) : reject(error)
        );
    });

    wrappedPromise.cancel = () => hasCanceled_ = true;

    return wrappedPromise;
}

class PromiseQueue {
    queue = [];
    workingOnPromise = false;

    constructor() {
        this.afterFinish = [];
    }

    enqueue(promise) {
        return new Promise((resolve, reject) => {
            this.queue.push({
                promise,
                resolve,
                reject,
            });
            this.dequeue();
        });
    }

    dequeue() {
        if (this.workingOnPromise)
            return false;
        const item = this.queue.shift();
        if (!item) {
            if (!this.workingOnPromise) {
                this.afterFinish.forEach(resolve => resolve());
                this.afterFinish = [];
            }
            return false;
        }
        try {
            this.workingOnPromise = true;
            item.promise()
                .then((value) => {
                    this.workingOnPromise = false;
                    item.resolve(value);
                    this.dequeue();
                })
                .catch(err => {
                    this.workingOnPromise = false;
                    item.reject(err);
                    this.dequeue();
                })
        }
        catch (err) {
            this.workingOnPromise = false;
            item.reject(err);
            this.dequeue();
        }
        return true;
    }

    async whenFinished() {
        let promise = new Promise((resolve, reject) => this.afterFinish.push(resolve));
        this.dequeue();
        await promise;
    }
}

class ErrorQueue {
    static SHOW_TIME = 30000;
    static queue = [];
    static activeError = false;

    static enqueue(error) {
        if (typeof error === "string")
            error = new Error(error);
        console.error(error);
        this.queue.push(error);
        this.dequeue();
    }

    static dequeue() {
        if (this.activeError)
            return;
        const error = this.queue.shift();
        if (!error)
            return;
        this.activeError = true;

        let errorbox = d3.select("#errors-container").append("div")
            .classed("errorbox", true)
            .text(error.message)
            .style("opacity", "0")
            .style("bottom", "150px");
        let errorqueue = this;
        let removed = false;

        function removeError() {
            if (removed)
                return;
            removed = true;
            errorbox.transition()
                .duration(200)
                .style("opacity", "0")
                .on("end", () => {
                    errorbox.remove();
                    errorqueue.activeError = false;
                    errorqueue.dequeue();
                });
        }

        errorbox.append("a")
            .attr("href", "javascript:void(0);")
            .classed("closebutton", true)
            .on("click", removeError);

        errorbox.transition()
            .duration(500)
            .style("opacity", "1")
            .style("bottom", "0px");

        setTimeout(removeError, this.SHOW_TIME);
    }
}

const ResponseType = {
    JSON: "json",
    TEXT: "text",
    XML: "xml"
}

function parseParameters(parameters) {
    // Parse request parameters to a query string

    function parseContent(content) {
        if (content instanceof Set)
            content = Array.from(content);
        if (typeof content === "object")
            content = JSON.stringify(content);
        return content;
    }
    parameters = Object.fromEntries(Object.entries(parameters).map(e => [e[0], parseContent(e[1])]));
    let queryString = new URLSearchParams(parameters).toString();
    return queryString;
}

function makeRequest(method, url, parameters = {}, responseType = ResponseType.JSON) {
    // Asynchronous XHRs

    return new Promise(async function (resolve, reject) {
        // Wait for imports to finish, because the server can only connect to one database at a time
        await ctx.importQueue.whenFinished();

        let xhr = new XMLHttpRequest();
        serialize = function (obj) {
            var str = [];
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        }
        if (method === "POST") {
            xhr.open(method, url, true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        }
        else
            xhr.open(method, url + "?" + parseParameters(parameters), true);
        xhr.onload = function () {
            if (xhr.status >= 200 && xhr.status < 300) {
                switch (responseType) {
                    case ResponseType.JSON:
                        resolve(JSON.parse(xhr.responseText));
                        break;
                    case ResponseType.TEXT:
                        resolve(xhr.responseText);
                        break;
                    case ResponseType.XML:
                        resolve(xhr.responseXML);
                        break;
                    default:
                        reject(new Error("Not a valid response type: " + responseType));
                }
            }
            else {
                let doc = new DOMParser().parseFromString(xhr.responseText, "text/html");
                let msg = doc.querySelectorAll("p")[1].childNodes[1].textContent;
                reject("Error " + xhr.status + ": " + msg);
            }
        };
        xhr.onerror = function () {
            let doc = new DOMParser().parseFromString(xhr.responseText, "text/html");
            let msg = doc.querySelectorAll("p")[1].childNodes[1].textContent;
            reject("Error " + xhr.status + ": " + msg);
        };
        if (method === "POST")
            xhr.send(parseParameters(parameters));
        else
            xhr.send();
    });
}

function clamp(a, b) {
    return function (x) {
        if (x === null || x === undefined || isNaN(x))
            return x;
        return Math.min(b, Math.max(a, x));
    }
}

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

function getDefault(obj, prop, def) {
    return obj.hasOwnProperty(prop) ? obj[prop] : def;
}

function removeDuplicates(array, key) {
    return array.filter((obj, pos, arr) => {
        return arr.map(mapObj => key(mapObj)).indexOf(key(obj)) === pos;
    });
}
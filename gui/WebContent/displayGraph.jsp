<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
  ~
  ~ Available under MIT license (https://opensource.org/licenses/MIT)
  ~
  --%>
<!DOCTYPE html>

<html>

<head>
    <title style="font-variant: small-caps">Abstra</title>

    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- D3.js -->
    <script src="https://d3js.org/d3.v4.min.js" crossorigin="anonymous"></script>

    <style>
        /* * { box-sizing: border-box; }*/
        /*.column { float: left; width: auto; padding: 10px }*/
        /*.row:after { content: ""; display: table; clear: both; }*/
        .hiddenList {
            display: none;
        }

        .shownList {
            visibility: visible;
        }

        body {
            padding-top: 65px;
        }

        label {
            display: contents;
        }

        g:hover text {
            display: inline;
        }

        /* so that navbar does not hide the content */

    </style>
</head>

<body class="no-db">
<jsp:include page="header.jsp" />

<div class="container-fluid">
    <!--parameters-->
    <div class="row">
        <div class="col-sm-12">
            <form id="formParamsExplore" action="/gui/display" method="post">
                <fieldset>
                    <div class="col-sm-12">
                        <div class="col-sm-2">
                            <i class="fa-sharp fa-solid fa-circle-question" title="The database name"></i>
                            <label for="databaseName">Database name*:</label>
                        </div>
                        <div class="col-sm-8" style="text-align: right;">
                            <input id="databaseName" name="databaseName" class="col-sm-8 col-sm-offset-2 offset-sm-2">
                        </div>
                        <div class="col-sm-2" style="text-align: left;">
                            <!-- <i class="fa-sharp fa-solid fa-circle-question" title="Check to read the abstract graph, else it will read the original graph."></i> -->
                            <!-- <label for="readAbstract">Read abstract:</label> -->
                            <input id="readAbstract" name="readAbstract" type="checkbox" value="abstract" title="Check to read the abstract graph, else it will read the original graph.">
                        </div>
                    </div>
                </fieldset>
                <br>

                <br>
                <button class="col-sm-8 col-sm-offset-2 offset-sm-2" type="submit" form="formParamsExplore" value="Submit">Submit</button>
            </form>
        </div>
    </div>

    <!--abstraction result-->
    <div class="row" style="padding-top: 2rem;">
        <div class="col-sm-12">
            <p id="jsonDataParagraph" style="display: none">
                ${ jsonData }
            </p>

            <div>
                <svg id="legendSvg" width="100%" height="5vh"></svg>
            </div>
            <div style="padding-bottom: 70px;">
                <svg id="networkSvg" width="100%" height="70vh">
                    <defs><marker id="arrowhead" viewBox="0 0 10 10" refX="20" refY="5" markerWidth="10" markerHeight="10" orient="auto"><path d="M 0 0 L 10 5 L 0 10 z" fill="#000"/></marker></defs> <!-- define arrow head marker for edges -->
                </svg>
            </div>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp" />



<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="helpModal">Help
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </h5>
            </div>
            <div class="modal-body">
                <h4>How to visualize a data graph?</h4>
                <ol>
                    <li>Fill the "database name" input with the name of the Abstra database that you have used to create your abstraction. If you don't know, you can check your Postgres databases and look for it. By default, Abstra databases are named as follows: abstra_&#60;datasetName&#62;.</li>
                    <li>Click on the "Submit" button to display the data graph. This shows the dataset modelled as a graph.</li>
                </ol>
                <br/>

                <h4>How to visualize an abstract graph?</h4>
                <ol>
                    <li>Fill the "database name" input with the name of the Abstra database that you have used to create your abstraction. If you don't know, you can check your Postgres databases and look for it. By default, Abstra databases are named as follows: abstra_&#60;datasetName&#62;.</li>
                    <li>Click on the "Submit" button to display the abstract graph. This shows the abstract graph obtained from the dataset.</li>
                </ol>
                <br/>

                <h4>Interact with a graph</h4>
                <p>Each <b>node</b> has a name (that you can display by hovering it), a set of incoming and outgoing edges, and a color which corresponds to its type (check the legend above the graph). </p>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type='text/javascript'>
    var jsonDataText = $("#jsonDataParagraph").text();
    console.log(jsonDataText);
    var jsonData = JSON.parse(jsonDataText);
    console.log(jsonData);
    var canvas = d3.select("#networkSvg");
    var width = $("#networkSvg").width(); // attr("width");
    var height = $("#networkSvg").height(); // attr("height");
    var r = 10; // radius of nodes
    var repulsionMaxDistance = 10*r;

    var simulation = d3
        .forceSimulation(jsonData.nodes)
        .force("charge", d3.forceManyBody().strength(500).distanceMin(1*r).distanceMax(3*r)) // each node tries to repel the others
        .force("center", d3.forceCenter(width / 2, height / 2)) // try to center all the nodes and the center is exactly (width/2, height/2)
        .force("collide", d3.forceCollide(30).strength(1)) // forceCollide avoids nodes to collide and let some space around each node (5)
        .force("link", d3.forceLink(jsonData.links).id(function(d) { return d.id; })) // to map source and targets with nodes, we say the source and target corresponds to the node id (it could correspond to the name also)
        .alpha(0.5) // TODO NELLY: check what is alpha
        .on("tick", update);

    // this.simulation = d3.forceSimulation()
    // .force("link", d3.forceLink()
    //     .id(d => d.getId())
    //     .distance(this.linkDistance)
    //     .strength(this.linkStrength))
    //     .force("repulsion", d3.forceManyBody()
    //         .strength(this.repulsionStrength)
    //         .distanceMin(2 * this.nodeSize)
    //         .distanceMax(this.repulsionMaxDistance === undefined ? Infinity : this.repulsionMaxDistance))
    //     .force("collide", d3.forceCollide(this.nodeSize))
    //     .alpha(0);
    // if (this.centerForce) {
    //     let size = this.getSize();
    //     this.simulation.force("center", d3.forceCenter(size.width / 2, size.height / 2))

    /* <g>
        <g>
          <text>
          <circle>
        </g>
        <g></g>...
      </g>
    */
    var color = d3.scaleOrdinal(d3.schemeCategory20); // to get colors from number
    var links = canvas.append("g").selectAll("line").data(jsonData.links).enter().append("line").attr("stroke-width", 1).style("stroke", "black").attr("marker-end", "url(#arrowhead)");
    var drag = d3.drag().on("start", dragStarted).on("drag", dragged).on("end", dragEnded);
    var textsAndNodes = canvas.append("g").selectAll("g").data(jsonData.nodes).enter().append("g").attr("class", "node").call(drag); // make the node draggable
    var textsAndEdges = canvas.append("g").selectAll("g").data(jsonData.links).enter().append("g").attr("class", "edge");
    var nodes = textsAndNodes.append("circle")
        .attr("r", r)
        .attr("fill", function(d) { return color(d.type); }) // give one color per type of nodes
        .attr("title", function(d) { return escape(d.label); }) // node label as title
        .attr("text-anchor", "middle")
        .on("mouseover", function(d) { d3.select(this.parentNode).select("text").style("visibility", "visible"); })
        .on("mouseout", function(d) { d3.select(this.parentNode).select("text").style("visibility", "hidden"); });
    var texts = textsAndNodes.append("text").text(function(d) { return escape(d.label); }).style("visibility", "hidden"); // add nodes labels hidden - make them visible on hover
    var edgeLabels = textsAndEdges.append("text").text(function(d) { return escape(d.label); }).attr("text-anchor", "middle"); // add edges labels

    var legend = d3.select("#legendSvg");
    // if($("#"))
    jsonData.types.push({ "id": "-1", "label": "non classified"});
    legend.selectAll("legendDots") // Add one dot in the legend for each name.
        .data(jsonData.types)
        .enter()
        .append("circle")
        .attr("cx", function(d,i){ return 10 + i*230; }) // 100 is where the first dot appears. 25 is the distance between dots
        .attr("cy", 10)
        .attr("r", 7)
        .style("fill", function(d){ return color(d.id); });

    legend.selectAll("legendLabels") // Add one text in the legend for each name.
        .data(jsonData.types)
        .enter()
        .append("text")
        .attr("x", function(d,i){ return 25 + i*230; })
        .attr("y", 13)
        .style("fill", function(d){ return color(d.id); })
        .text(function(d){ return d.label; })
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle");


    function update() {
        textsAndNodes.attr("transform", function(d) { return "translate(" + d.x + ", " + d.y + ")"; });
        textsAndEdges.attr("transform", function(d) { return "translate(" + ((d.source.x + d.target.x)/2) + ", " + ((d.source.y + d.target.y)/2) + ")"; });
        links
            .attr("x1", function(d) { return d.source.x; }).attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; }).attr("y2", function(d) { return d.target.y; });
        console.log(simulation.alpha());
    }

    function dragStarted(d) {
        //your alpha hit 0 it stops! make it run again
        simulation.alphaTarget(0.3).restart();
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }
    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragEnded(d) {
        // alpha min is 0, head there
        simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    function escape(htmlStr) {
        if(htmlStr.includes("\"")) {
            htmlStr = htmlStr.replace("\"", "");
            htmlStr = htmlStr.substring(0, htmlStr.indexOf("\""));
        }
        htmlStr = htmlStr.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;");
        return htmlStr;
    }

</script>
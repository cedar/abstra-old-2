<div class="col-sm-12 navbar-inverse navbar-fixed-bottom" style="color: white;">
  <p>Authors: Nelly Barret, Ioana Manolescu, Prajna Upadhyay @ Inria</p>
  <p>Main contact: <a href="mailto:nelly.barret@inria.fr">nelly.barret@inria.fr</a></p>
</div>
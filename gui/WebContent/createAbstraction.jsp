<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
  ~
  ~ Available under MIT license (https://opensource.org/licenses/MIT)
  ~
  --%>
<!DOCTYPE html>

<html>

<head>
    <title style="font-variant: small-caps">Abstra</title>

    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        /* * { box-sizing: border-box; }*/
        /*.column { float: left; width: auto; padding: 10px }*/
        /*.row:after { content: ""; display: table; clear: both; }*/
        .hiddenList {
            display: none;
        }

        .shownList {
            visibility: visible;
        }

        body {
            padding-top: 65px;
        }

        label {
            display: contents;
        }

        /* so that navbar does not hide the content */

    </style>
</head>

<body class="no-db">
<jsp:include page="header.jsp" />

<div class="container-fluid">
    <!--parameters-->
    <div class="row">
        <div class="col-sm-12">
            <form id="formParams">
                <fieldset>
                    <div class="col-sm-12">
                        <div class="col-sm-2" style="text-align: right;">
                            <i class="fa-sharp fa-solid fa-circle-question" title="Absolute path of your file"></i>
                            <label for="filePath">File*:</label>
                        </div>
                        <div class="col-sm-8">
                            <input id="filePath" name="filePath" class="col-sm-12">
                        </div>
                    </div>
                </fieldset>
                <br>

                <fieldset class="col-sm-3">
                    <legend id="reportingParamsTitle">Reporting parameters</legend>

                    <div id="reportingParams">
                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="Maximum number of main collections"></i>
                            <label for="emax">Emax</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" id="emax" name="emax" value="5" min="1"><br>
                        </div>

                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="Minimal data coverage to reach before stopping the abstraction"></i>
                            <label for="covmin">cov min</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" id="covmin" name="covmin" value="0.8" min="0" max="1" step="0.01"><br>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="col-sm-5">
                    <legend id="selectionParamsTitle">Main collection selection parameters</legend>

                    <div id="selectionParams">
                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="The method to assign scores to collections"></i>
                            <label for="scoring">Scoring method*</label>
                        </div>
                        <div class="col-sm-8">
                            <select name="scoring" id="scoring">
                                <option value="--" selected>--</option>
                                <option value="DESC">desc-k</option>
                                <option value="LEAF">leaf-k</option>
                                <option value="wDAG">DAG weight propagation</option>
                                <option value="wPR">PageRank weight propagation</option>
                            </select>
                        </div>
                        <div class="hiddenList">
                            <div class="col-sm-4">
                                <label for="k" id="labelK">k</label>
                            </div>
                            <div class="col-sm-8">
                                <input id="k" name="k" type="text" value="3">
                            </div>
                        </div>
                        <br>

                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="The method to compute the boundary of the selected collection"></i>
                            <label for="boundary">Boundary method*</label>
                        </div>
                        <div class="col-sm-8">
                            <select name="boundary" id="boundary">
                                <option value="--" selected>--</option>
                                <option value="DESC">natural descendant boundary</option>
                                <option value="LEAF">natural leaf boundary</option>
                                <option value="DAG">DAG boundary</option>
                                <option value="FL">Flood boundary</option>
                                <option value="FL_AC">Acyclic flood boundary</option>
                            </select>
                        </div>

                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="The method to update the graph and reflect the selection"></i>
                            <label for="update">Graph update method*</label>
                        </div>
                        <div class="col-sm-8">
                            <select name="update" id="update">
                                <option value="--" selected>--</option>
                                <option value="BOOLEAN">boolean</option>
                                <option value="EXACT">exact</option>
                            </select>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="col-sm-4">
                    <legend id="classifParamsTitle">Classification parameters</legend>

                    <div id="classifParams">
                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="Minimum similarity for a semantic property and a data property"></i>
                            <label for="simmin">Sim min</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" id="simmin" name="simmin" value="0.8" min="0.0" max="1.0" step="0.01"><br>
                        </div>

                        <div class="col-sm-4">
                            <i class="fa-sharp fa-solid fa-circle-question" title="Minimal fitness of the assigned category and the collection properties"></i>
                            <label for="scorecatmin">Score cat min</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" id="scorecatmin" name="scorecatmin" value="0.3" min="0.0" max="1.0" step="0.01">
                        </div>
                    </div>
                </fieldset>
                <br><br><br><br>
                <button class="col-sm-7 col-sm-offset-2 offset-sm-2" style="margin-top: 3rem; margin-bottom: 4rem;" type="submit" form="formParams" value="Submit" onclick="return checkParams();">Submit</button>
                <button class="col-sm-1" style="margin-top: 3rem; margin-bottom: 4rem;" value="Reset" id="reset">Reset</button>
            </form>
        </div>
    </div>

    <!--abstraction result-->
    <div class="row" style="padding-bottom: 70px;">
        <div id="descriptionDiv" class="col-sm-6">
            <h3>Description</h3>

        </div>
        <div id="ERdiv" class="col-sm-6">
            <h3>Entity/Relationship schema</h3>

        </div>
    </div>
</div>
<jsp:include page="footer.jsp" />


<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="helpModal">Help
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </h3>
            </div>
            <div class="modal-body">
                <h4>How to create an abstraction?</h4>
                <ol>
                    <li>Enter the absolute path of your dataset in the "file" input</li>
                    <li>Tune the reporting, main collections selection and classification parameters</li>
                    <li>Run the abstraction by clicking on the Submit button</li>
                    <li>Wait for the result to appear in the GUI (this might take few minutes depending on the dataset size). <i>Please consider using the command-line version to load large datasets in Abstra.</i></li>
                </ol>
                <br/>

                <h4>How to read an abstraction?</h4>
                <p>On the <b>left side</b>, you have the HTML description, showing for each main collection its attributes (or properties), possibly nested.
                    The <b>percentage</b> of a property corresponds to the frequency of the property in the parent collection.
                    The key icon (<img src="/gui/img/key-icon.png" height="15">) next to a property indicates that this property is identified as an ID (like a primary key in the classical RDBMS meaning).
                    You can fold/unfold properties by clicking on the arrows near each property.
                    Next to it, there is the <b>list of relationships</b> found between the reported collections.
                </p>
                <p>On the <b>right side</b>, you have the Entity-Relationship schema (E-R schema in short). It shows the main collections together with their top-level properties (no nesting). The relationships are represented by the arrows linking to entities.</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type='text/javascript'>
    $('.fa-angles-down, .fa-angles-up').click(function () {
        if ($(this).attr('class') === 'fa-solid fa-angles-down') {
            $(this).removeClass('fa-solid fa-angles-down').addClass('fa-solid fa-angles-up');
            $(this).parent().next().removeClass('hiddenList').addClass('shownList');
        } else if ($(this).attr('class') === 'fa-solid fa-angles-up') {
            $(this).removeClass('fa-solid fa-angles-up').addClass('fa-solid fa-angles-down');
            $(this).parent().next().removeClass('shownList').addClass('hiddenList');
        }
    });

    $("#scoring").change(function() {
        console.log($(this).val());
        console.log($(this).val);
        console.log($(this).text());
        if($(this).val() === "DESC" || $(this).val() === "LEAF") {
            $(this).parent().next().removeClass('hiddenList').addClass('shownList');
        } else {
            $(this).parent().next().removeClass('shownList').addClass('hiddenList');
        }
    });

    $("#classifParamsTitle, #selectionParamsTitle, #reportingParamsTitle").click(function() {
        $(this).next().toggle();
    });

    $("#formParams").on("submit", function(e) {
        e.preventDefault(); // do not reload
        var form = $(this);
        document.body.style.cursor='wait';
        $.ajax({
            method: "POST",
            url: "/gui/run",
            data: form.serialize(),
            success: function(textResponse) {
                console.log(textResponse);
                var JSONresponse = JSON.parse(textResponse);
                console.log(JSONresponse);
                $("#descriptionDiv").append(JSONresponse["entitiesText"])
                $("#descriptionDiv").append(JSONresponse["relationshipText"])
                $("#ERdiv").append("<img src=" + JSONresponse["filenamePngDrawing"] + " style='margin-left: 20px; max-height:700px; max-width:700px;' alt='Entity Relationship schema'>");
                document.body.style.cursor='default';
            },
            error: function(error) {
                console.log(error);
                document.body.style.cursor='default';
            }
        });
    });

    $("#resetButton").on("click", function() {
        $('#formParams')[0].reset();

    });
    // declare the reset() function (does not exist in JQuery)
    jQuery.fn.reset = function () {
        $(this).each (function() { this.reset(); });
    }

    function checkParams() {
        if($("#emax").val() === "--") {
            alert("please choose a valid emax value.");
            return false;
        }

        if($("#covmin").val() === "--") {
            alert("please choose a valid covmin value.");
            return false;
        }

        if($("#scoring").val() === "--") {
            alert("please choose a valid scoring value.");
            return false;
        }

        if($("#boundary").val() === "--") {
            alert("please choose a valid boundary value.");
            return false;
        }

        if($("#update").val() === "--") {
            alert("please choose a valid update value.");
            return false;
        }

        if($("#simmin").val() === "--") {
            alert("please choose a valid simmin value.");
            return false;
        }

        if($("#scorecatmin").val() === "--") {
            alert("please choose a valid scorecatmin value.");
            return false;
        }

        return true
    }

    function checkCombination() {
        if($("#scoring").val() === "desck") {
            if($("#boundary").val() !== "desck") {
                return false; // descK scores should be combined with natural descK boundary only
            }
            if($("#update").val() !== "boolean") {
                return false; // boolean update makes more sense
            }
        }
        if($("#scoring").val() === "leafk") {
            if($("#boundary").val() !== "leafk") {
                return false; // leafK scores should be combined with natural leafK boundary only
            }
            if($("#update").val() !== "boolean") {
                return false; // boolean update makes more sense
            }
        }
        if($("#scoring").val() === "csize") {
            if($("#boundary").val() === "desck" || $("#boundary").val() === "leafk") {
                return false; // all other techniques make sense
            }
            // all update make sense
        }
        if($("#scoring").val() === "propBP") {
            if($("#boundary").val() === "desck" || $("#boundary").val() === "leafk") {
                return false; // all other techniques make sense
            }
            // all update make sense
        }
        if($("#scoring").val() === "propPR") {
            if($("#boundary").val() === "desck" || $("#boundary").val() === "leafk") {
                return false; // all other techniques make sense
            }
            // all update make sense
        }

        return true;
    }

    $("#scoring, #boundary, #update").change(function() {
        if(!checkCombination()) {
            // the combination is not natural
            alert("This combination is not intuitive. Be sure that this is what you want.")
        }
    });
</script>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
  ~
  ~ Available under MIT license (https://opensource.org/licenses/MIT)
  ~
  --%>
<!DOCTYPE html>

<html>

<head>
    <title style="font-variant: small-caps">Abstra</title>

    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        /* * { box-sizing: border-box; }*/
        /*.column { float: left; width: auto; padding: 10px }*/
        /*.row:after { content: ""; display: table; clear: both; }*/
        .hiddenList {
            display: none;
        }

        .shownList {
            visibility: visible;
        }

        body {
            padding-top: 65px;
        }

        label {
            display: contents;
        }

        /* so that navbar does not hide the content */

    </style>
</head>

<body class="no-db" style="padding-top: 0;">
<jsp:include page="header.jsp" />

<div class="container-fluid" style="background: url('/gui/img/background4.png') no-repeat center top; background-size: cover; height: 100vh; padding-bottom: 65px;">
    <div style="text-align: center;" >
        <div class="row" style="background: white; border: black 1px solid; padding: 5rem 1rem; margin: 10rem 1rem 5em;">
            <img src="/gui/img/algo.png" width="100">
            <p>Load your dataset, tune the parameters and create your own abstraction</p>
            <button id="createAbstractionButton" class="btn btn-primary"><a href="createAbstraction.jsp" style="color: white;">Create an abstraction</a></button><br/>
        </div>

        <div class="row" style="background: white; border: black 1px solid; padding: 5rem 1rem; margin: 1rem;">
            <img src="/gui/img/user-experience.png" width="100">
            <p>Interact with the graph</p>
            <button id="readGraphButton" class="btn btn-primary"><a href="displayGraph.jsp" style="color: white;">Visualize an abstract graph</a></button>
        </div>
    </div>
</div>

<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="helpModal">Help
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </h3>
            </div>
            <div class="modal-body">
                <h4>What is Abstra?</h4>

                <p><b>Abstra</b> is a <b>data abstraction tool</b> to help and simplify users' acquaintance with tabular, tree- or graph-structured data.
                    Abstra computes a <b>description meant for humans</b>, based on the idea that, regardless of the syntax or the data model, any dataset holds some collections of entities/records, that are possibly linked with relationships.
                    Abstra seeks to help users needing help to select the dataset they need and data producers needing tools to generate automatically documentation for the data they produce.
                </p>
                <br/>

                <h4>How does Abstra work?</h4>

                <ul>
                    <li>Abstra relies on a common graph representation of any incoming dataset.</li>
                    <li>It relies on an original algorithm for selecting the core entity collections and their relations. </li>
                    <li>It also leverages Information Extraction to detect what the dataset is about.</li>
                    <li>Abstractions are shown both as <b>HTML text</b> and a <b>lightweight E/R diagram</b>. </li>
                    <li>A GUI also allows to explore the dataset.</li>
                </ul>
                <br/>

                <h4>Getting started</h4>

                <ul>
                    <li>Click on "Create an abstraction" if you want to create the data abstraction of your dataset. You will be able to tune Abstra's parameters and obtain the HTML description and E-R schema.</li>
                    <li>Click on "Visualize an abstract graph" if you want to visualize the graph produced out of the dataset or visualize it after abstraction.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp" />
</body>

</html>

<script type="text/javascript">

</script>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <img src="/gui/img/abstra-icon.png" height="40" class="d-inline-block align-top" style="padding-top: 1rem; padding-right: 1rem;" alt="Abstra icon">
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li style="font-size: large; color: white; padding-top: 1rem;">Abstra</li>
                <li><a href="index.jsp">Home</a></li>
                <li><a href="https://team.inria.fr/cedar/projects/abstra/" target="_blank">About</a></li>
                <li><a data-toggle="modal" data-target="#helpModal">Help</a></li>
            </ul>
        </div>
    </div>
</nav>


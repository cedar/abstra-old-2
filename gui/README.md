# Introduction
This package contains the `endpoint` project from which users can interact with the engine from a 
Web interface. In particular, the endpoint features the demo of Inria Cedar, AIST-AIRC, INESC-ID 
and École Polytechnique's keyword search on hetereogenous data.

# Building
Pre-requisites: `maven2`, `java8`

This step is only necessary if you have not already built from the root project.

	cd /path/to/endpoint
	mvn install

Executing the above command will produce a WAR under the `target/` directory.

# Running
Drop the target/demo.war into your favorite J2EE web application container, 
then fire up the Web container. The application will be accessible from:

	http://hostname[:8080]/demo/

The top menu has three items:
 - `Scenarios` lets you choose a set of pre-registered data to issue queries on.
 - `Import` lets you upload a new dataset in the scenario currently selected. 
 - `About` show static information about the project.

# Configuration
The configuration file is located in WebContent/WEB-INF/local.settings and follows the same syntax as 
the file from the main project. Changes to the file will not take effect at runtime, so do not
forget to restart the server after any changes.

*Note: If you have loaded data directly through the main project, be sure to use the same location
for caches (property `cache_location`), to ensure that behaviors are consistent between the main
project and the demo. * 

# Another (simpler) way to run ConnectionLens
To integrate all the aforementionned steps, the script `runConnectionLens.sh` has been developed. To use it, you just have to follow these steps:
1. Change thee values of main variables, defined in capital letters at the top of the file, in order to comply with your architecture (`CONNECTIONLENS_PATH`, `TOMCAT_PATH`, `BROWSER_PATH` and optionnaly `APP_NAME` and `PORT_TOMCAT`).
2. To avoid tedious tuning of parameters, the script will replace default parameters with yours. To allow this, you have to add the name of the parameter in the variable `settings_to_change` (line 47) and the desired values for these parameters in the variable `values` (line 48). For example, you can add `'default_locale'` to `settings_to_change` and `"english"` to `values`.
3. Run the script with one of the following two modes:
    - Command-line style: `sh runConnectionLens.sh cmd`. This will start ConnectionLens in the interactive command-line mode.
    - GUI style: `sh runConnectionLens.sh`. This will start ConnectionLens in your favorite browser.

Make sure you have Tomcat and Maven installed.

# Known issues
To avoid encoding issues using Tomcat on Windows, it may be necessary to add this line to `catalina.bat`:

	set CATALINA_OPTS=-Dfile.encoding="UTF-8"

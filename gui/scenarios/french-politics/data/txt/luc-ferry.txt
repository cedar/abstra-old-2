Luc Ferry, né le 3 janvier 1951 à La Garenne-Colombes (Hauts-de-Seine), est un homme politique, philosophe et politologue français.
Ancien professeur de philosophie et de science politique, il est ministre de la Jeunesse, de l’Éducation nationale et de la Recherche dans les gouvernements I et II de Jean-Pierre Raffarin.


== Biographie ==


=== Enseignant de philosophie et de sciences politiques ===
Fils d'un préparateur indépendant et constructeur de voitures automobiles sportives, Pierre Ferry, inventeur de voitures de compétition et d'une mère au foyer, Luc Ferry a trois frères (dont le philosophe Jean-Marc Ferry).
Il suit ses études secondaires au lycée Saint-Exupéry de Mantes, puis à la maison avec le CNED. Il suit ensuite des études supérieures à l'université de Paris et à l'université de Heidelberg.
Il devient professeur agrégé de philosophie en 1975 et commence sa carrière d'enseignant au lycée des Mureaux (Yvelines). Il est notamment affecté à l’École normale d'Arras de 1977 à 1979 puis en détachement au CNRS comme attaché de recherche de 1980 à 1982 et enfin chargé de cours à l'université de Reims, puis à l'École normale supérieure, aux universités de Paris X et de Paris I[réf. nécessaire].
En 1980, il obtient un doctorat d'État en science politique à l'université de Reims. Il obtient l'agrégation de science politique en 1982 et devient ainsi professeur des universités. Il est successivement nommé à l'Institut d'études politiques de Lyon de 1982 à 1988 puis professeur de philosophie à l'université de Caen Basse-Normandie de 1989 à 1996 et à l'université Paris VII-Denis-Diderot depuis 1996[réf. nécessaire].
Il accède à la notoriété en publiant avec Alain Renaut La Pensée 68 (1985) dans lequel il critique des penseurs de l'après Mai 68 : Pierre Bourdieu, Jacques Lacan, Jacques Derrida et Michel Foucault,.
En 1987, il devient chroniqueur à L'Express . En 1992, il publie Le Nouvel Ordre écologique.
En 1994, François Bayrou, alors ministre de l'Éducation nationale, le nomme président du Conseil national des programmes au ministère de l'Éducation nationale,. Il occupe le poste jusqu'en 2002.
En janvier 1997, il est nommé à la Commission de réforme de la justice.


=== Ministre de la Jeunesse, de l'Éducation nationale et de la Recherche ===
Du 7 mai 2002 au 30 mars 2004, Luc Ferry est ministre de la Jeunesse, de l'Éducation nationale et de la Recherche dans les deux premiers gouvernements de Jean-Pierre Raffarin.
À son arrivée au gouvernement, Luc Ferry annonce des mesures centrées autour de la lutte contre l’illettrisme. Entre autres mesures : le dédoublement de certaines classes de CP,[Quoi ?] la rédaction de nouveaux programmes en primaire centrés autour de la maîtrise de la langue, au collège la nouveauté est la mise en place des itinéraires de découverte (IDD) et l’introduction de l’enseignement en alternance dès la classe de 4e, au lycée, l’apparition du travail personnel encadré (TPE) et la rénovation du CAP[réf. nécessaire].
Il annonce également un projet de décentralisation de 100 000 personnels non enseignants de l’Éducation nationale aux collectivités territoriales (conseillers d’orientation, psychologues, assistantes sociales, médecins et personnels « techniciens et ouvriers de service »). L’annonce est mal accueillie car elle provoque de nombreuses craintes quant à l’accroissement des inégalités au sein du service public. En mai 2003, face au mouvement d'opposition à cette réforme, Luc Ferry renonce aux délocalisations des médecins scolaires, psychologues et assistantes sociales (seul le transfert des techniciens et ouvriers de service est maintenu)[réf. nécessaire].
En février 2004, Luc Ferry, suivant les propositions de la commission Stasi, propose un texte sur la laïcité à l’école et l’interdiction de signes religieux ostensibles à l’école, adopté à l'Assemblée nationale avec une large majorité.


=== Une présence médiatique et éthique ===
Luc Ferry est président délégué du Conseil d'analyse de la société, créé en juillet 2004.
En juin 2006, il est chargé par le président de l'UMP d'une « mission de réflexion » sur le mariage homosexuel et l'homoparentalité, mission qu'il décide d'interrompre quelques mois plus tard.
Depuis juillet 2007, il est membre du Comité de réflexion sur la modernisation et le rééquilibrage des institutions, mis en place par le président de la République Nicolas Sarkozy[réf. nécessaire]. En 2009, il est nommé membre du Comité consultatif national d'éthique par Nicolas Sarkozy[réf. nécessaire].
Ancien éditorialiste à L'Événement du jeudi, L'Express, Le Point puis au bimensuel économique Challenges[réf. nécessaire], il est membre du comité prospectif de Vivendi Universal[réf. nécessaire] et ancien membre du Conseil économique et social en tant que membre du groupe des personnalités qualifiées et membre de la section des relations extérieures.
En 2011, l'école du village de Ricarville dans la Seine-Maritime est baptisée du nom de Luc Ferry.
En 2015, il assure une chronique sur Radio Classique, qui a pour objet de raconter, en quelques minutes, les mythes fondateurs relatifs aux expressions usuelles que nous utilisons dans la langue française.
Il est membre du Club du Siècle. Il intervient également dans des conventions ou séminaires d'entreprises.


=== Vie privée ===
Luc Ferry épouse le 22 avril 1989 en premières noces Dominique Meunier, avec qui il a adopté une fille, Gabrielle (née en 1991). Après son divorce (vers 1997), il se remarie le 7 mai 1999 avec Marie-Caroline Becq de Fouquières (née en 1975), dont la famille est alliée à Jean-Jacques Servan-Schreiber. De ce second mariage naissent deux autres filles, Louise (née en 1999) et Clara (née en 2001).
Selon Le Figaro, Jules Ferry serait un « aïeul » de Luc Ferry, mais ce dernier n'en est pas un descendant direct. Luc Ferry a lui-même précisé sur France 5 qu'il avait bien un lien de parenté avec Jules Ferry, mais qu'il ne s'agissait que d'un cousinage éloigné.
En 2002, une controverse née de la mise en école privée de deux de ses enfants, amène des critiques contre le ministre de l'Éducation nationale. Celui-ci indique qu'il souhaitait que ses enfants aient une éducation religieuse. 


== Prises de position ==


=== La philosophie ===
Lors de la conférence du 9 avril 2005 à la Sorbonne ayant pour thème « Qu'est ce que la philosophie ? », Luc Ferry définit celle-ci comme une sotériologie, c'est-à-dire une « doctrine du salut ». Il entend par doctrine du salut l'ensemble des réponses proposées aux grandes questions existentielles : quel est le sens de la vie puisque nous sommes mortels ? Comment (selon l'expression de Luc Ferry sur une chaîne de télévision)[réf. nécessaire] « sauver sa peau », non en évitant la mort puisqu'elle est inéluctable, mais en vivant, pour le temps qui nous est donné, de manière satisfaisante ? Et dans ce cas, quelle est cette manière satisfaisante et comment la trouver ?
La philosophie est en cela concurrente, voire adversaire des grandes religions, puisqu'elle nous invite à trouver par nous-mêmes la réponse à cette question existentielle au lieu d'accepter l'enseignement d'autorité des religions. Selon Luc Ferry, une philosophie commence à être pleine et entière lorsqu’elle s’éloigne de Dieu. Plus une philosophie est athée, plus elle correspond à la définition de la philosophie. Mais dans ce cas amputée volontairement d'une direction de pensée. La philosophie n’est donc pas seulement une réflexion critique, car la démarche scientifique par exemple requiert aussi ce type de réflexion, la philosophie n'est pas non plus seulement une rhétorique séduisante, mais bien une recherche de la sagesse. Ce discours est développé et argumenté dans Apprendre à vivre.
La qualité et le caractère philosophique du travail de Luc Ferry ont été remis en cause notamment par les philosophes Jacques Bouveresse, qui voit en lui un « obligé du pouvoir », ou Dominique Lecourt, et dans un pamphlet de Jean-François Raguet.


=== Questions de société ===
Luc Ferry critique certaines tendances de l'écologisme dans son ouvrage Le Nouvel Ordre écologique.
Dans L'homme-Dieu ou le sens de la vie, il décrit l'évolution des pensées, générée par la sécularisation de la société. Selon lui, les valeurs morales se substituent de plus en plus à la religion, et l'homme est de plus en plus guidé par l'éthique, fondée en grande partie sur les droits de l'homme. Or, celle-ci ne couvre pas autant de questions que la religion. Elle se résume davantage à définir des règles de vie en société, laissant ainsi un vide au sujet des questions existentielles, telles que le deuil, ou la question du sens de la vie.
Les générations sécularisées trouvent souvent une réponse à ce vide dans les spiritualités d'Orient. Luc Ferry note ici la contradiction que recèle ce choix, car le bouddhisme donne à la vie une dimension dans laquelle la notion de sens disparaît. Ainsi, il explique le besoin d'adopter une spiritualité laïque qui propose de donner un sens à sa vie, tout en laissant le champ libre à la réflexion au lieu des arguments d'autorité caractérisant les religions.


=== L'échec scolaire ===
Luc Ferry déplore que le système éducatif français fabrique de l'échec sans parvenir à y porter remède. Suivant l'exemple finlandais, Luc Ferry pense qu’il faut intervenir dès l'identification des difficultés scolaires. Sa proposition consiste à dédoubler la classe de cours préparatoire avec deux enseignants par classe en cas de besoin.
Le 2 décembre 2013, il déclare à la télévision : « J’ai visité des centaines d’établissements scolaires et moi je peux vous assurer aujourd’hui que dans 15 % des établissements scolaires, l’enseignement ça ressemble à du domptage et pas à de l’enseignement. Un spécialiste de l’évaluation, quelqu’un qui travaillait, je ne vais pas citer son nom, un chercheur qui travaillait à la direction de l’évaluation et de la prospective me disait que si on faisait abstraction, alors je vais dire un truc pas politiquement correct, c’est pas facile, c’est compliqué à dire mais c’est vrai, si on faisait abstraction des 15 % d’établissements qui sont en perdition dans des quartiers en perdition, la France serait numéro 1 dans l’enquête Pisa » (Programme PISA : Programme international pour le suivi des acquis des élèves).


=== L'enseignement des maths ===
Le 15 février 2018, il déclare sur LCI n'avoir jamais utilisé les maths. S'opposant au mathématicien Cédric Villani, il affirme que dans la vie quotidienne, on n'utilise jamais de raisonnement mathématique.


=== Controverses ===


==== Accusations de pédophilie proférées à l'encontre d'un ministre (2011-2012) ====
Invité du Grand Journal sur Canal+ le 30 mai 2011, Luc Ferry accuse, sans le nommer, un ancien ministre d'avoir eu des relations pédophiles à Marrakech, affirmant détenir ses témoignages « des autorités de l'État au plus haut niveau ». Il a été entendu par la Brigade de protection des mineurs le 3 juin 2011, de même que son prédécesseur au ministère de l’Éducation, Jack Lang, entendu en novembre 2012 comme simple témoin et qui a affirmé n'être au courant de rien. Il s'avère qu'il ne s'agissait que d'une rumeur, et l'affaire est classée sans suite fin 2012.
Cependant, le 5 octobre 2014, dans l'émission Médias, le magazine, sur France 5, Luc Ferry affirme que ce qu'il a dit en 2011 était vrai, et qu'il n'en « retire pas une virgule ». Il cite par ailleurs un article du Figaro Magazine : « À Marrakech, un ex-ministre “s'amuse” », relatant les faits et paru la veille de son passage à l'antenne, le 30 mai 2011.


==== Affaires des salaires versés par l'université Paris-Diderot (1997-2011) ====
En juin 2011, Le Canard enchaîné et d'autres médias, affirment que Luc Ferry, professeur à l'université Paris-Diderot, n'y assure aucun enseignement depuis quatorze ans et qu'il n'y est quasiment jamais présent : outre les périodes de fonctions ministérielles, où il était détaché et payé comme ministre, il a longtemps été dispensé, à sa demande, d'enseignement et mis à disposition afin d'accomplir diverses fonctions officielles. En 2010, avec l'autonomie financière, son université lui demande d'accomplir son service d'enseignement statutaire pour lequel elle le paye, ce qu'il ne fait pas ; l'université lui réclame donc le remboursement de ses rémunérations (environ 4500 euros mensuels selon la même source) ou d'assurer ces enseignements.
Pour sa part, Luc Ferry, qui y voit les conséquences de ses propos tenus au Grand Journal, déclare qu'il est en détachement de l'enseignement supérieur et qu'en l'absence de convention entre Matignon et l'université Paris-VII, cette dernière prend en charge son traitement de président du Conseil d'analyse de la société, comité rattaché aux services du Premier ministre. In fine, Matignon sera tenu de rembourser l'Université, conformément à la loi . Il décide alors de prendre sa retraite d'enseignant à la fin de l'année scolaire 2011.


== Publications ==


=== Ouvrages ===


=== Livres audio ===
Luc Ferry est l'auteur et le narrateur des livres audio suivants, tous édités par Frémeaux & Associés à Vincennes.

Apprendre à vivre : traité de philosophie en 4 CD audios, 1er mai 2006 (EAN 356-1-302-51612-7, notice BnF no FRBNF40930441)
Kant : l'œuvre philosophique expliquée, 28 octobre 2008 (EAN 356-1-302-52192-3, notice BnF no FRBNF41283626)
Nietzsche : l'œuvre philosophique expliquée, 28 octobre 2008 (EAN 356-1-302-52332-3, notice BnF no FRBNF41349093)
Le Christianisme : la pensée philosophique expliquée, 25 mai 2009 (EAN 356-1-302-52552-5, notice BnF no FRBNF42026039)
Philosophie du temps présent, 23 septembre 2009 (EAN 356-1-302-52762-8, notice BnF no FRBNF42058982)
Heidegger : l'œuvre philosophique expliquée, 1er octobre 2009 (EAN 356-1-302-52722-2, notice BnF no FRBNF42057923)
Mythologie : l'héritage philosophique expliqué, 11 novembre 2010 (EAN 356-1-302-53002-4, notice BnF no FRBNF42282880)
Karl Marx : la pensée philosophique expliquée, 1er janvier 2011 (EAN 356-1-302-53442-8, notice BnF no FRBNF42461927)
Sigmund Freud : la pensée philosophique expliquée, 1er novembre 2011 (EAN 356-1-302-53472-5, notice BnF no FRBNF42529162)
Heidegger, les illusions de la technique, 31 octobre 2013 (EAN 356-1-302-53472-5, notice BnF no FRBNF42529162)
Sartre et l'existentialisme : Penser la liberté, 18 novembre 2013 (EAN 356-1-302-53472-5, notice BnF no FRBNF42529162)
La Pensée 68 et l'ère du soupçon, 18 novembre 2013 (EAN 356-1-302-53472-5, notice BnF no FRBNF42529162)
Philosophie de la bohème : l'invention des utopies, 10 avril 2014 (EAN 356-1-302-53472-5, notice BnF no FRBNF42529162)


=== Conférences ===
« Les mythes grecs et la question du sens de la vie », Parenthèse Culture, IFG, 13 juin 2014
« Trois sagesses anciennes : Gilgamesh, Bouddha, Epictète », Parenthèse Culture, IFG, 13 juin 2014
« Une brève histoire de l'éthique : de l'antiquité à nos jours », Parenthèse Culture, IFG, 13 juin 2014
« La révolution de l'humanisme moderne I : Descartes », Parenthèse Culture, IFG, 13 juin 2014
« La révolution de l'humanisme moderne II : Kant et les Lumières », Parenthèse Culture, IFG, 13 juin 2014
« La naissance de la pensée contemporaine », Parenthèse Culture, IFG, 13 juin 2014
« Nietzsche », Parenthèse Culture, IFG, 13 juin 2014
« Penser le temps présent », Parenthèse Culture, IFG, 13 juin 2014
Coffret 20 CD Mythologie & Philosophie - Les grands mythes grecs, éditions de l'Opportun pour Le Figaro, conférences Sara Yalda/théâtre des Mathurins, 2015.


== Notes et références ==


== Voir aussi ==


=== Bibliographie ===
Philippe Lançon, « Le philosophe du Président », Libération,‎ 3 mars 1997 (lire en ligne)
1999, Les Piètres Penseurs, Dominique Lecourt, Flammarion, Paris.
2001, The Mediocracy. French Philosophy since 1968, Dominique Lecourt, Trans. Gregory Elliott, new ed. Verso, London, 2002.
2003, Luc Ferry ou le Rétablissement de l'ordre, Élisabeth Hardouin-Fugier, David Olivier, Estiva Reus, éd. tahin party
2004, La Philosophie française en questions. Entretiens avec Comte-Sponville, Conche, Ferry, Lipovetsky, Onfray et Rosset, Sébastien Charles, Le Livre de poche.
Eric Aeschimann, « Ferry, exister à tout prix », Libération,‎ 2 juin 2011 (lire en ligne)
Thibaud Croisy, « Luc Ferry et “l'art comptant pour rien” », Le Monde, 3 juin 2013.


=== Liens externes ===
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation • Bibliothèque du Congrès • Gemeinsame Normdatei • Service bibliothécaire national • Bibliothèque nationale de la Diète • Bibliothèque nationale d’Espagne • Bibliothèque royale des Pays-Bas • Bibliothèque universitaire de Pologne • Base de bibliothèque norvégienne • WorldCat
Ressources relatives à la recherche : Fichier central des thèses • Persée
[PDF] CV sur le site du Conseil d'analyse économique
 Portail de la philosophie   Portail de la politique   Portail de la politique française
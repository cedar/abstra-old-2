Gilles Bon-Maury est un militant associatif et politique français, né le 16 avril 1978 à Blois. Il est diplômé de l'IEP de Lille et du CELSA. Il a été président d'HES (Homosexualités et Socialisme) de juin 2007 à septembre 2012, et conseiller au cabinet de Najat Vallaud-Belkacem, ministre des Droits des femmes, d'août 2012 à août 2014.


== Parcours ==
Gilles Bon-Maury est adhérent du Parti socialiste depuis 1997. Il est désigné membre de la commission nationale des conflits du PS lors du congrès de Reims (novembre 2008).


=== Président d'HES ===
Gilles Bon-Maury a rejoint le bureau national d'HES (Homosexualités et Socialisme) en 2003, en tant que vice-président chargé des relations européennes. En 2005, il a participé à ce titre à la fondation du réseau Rainbow Rose, qui réunit les groupes socialistes LGBT d'Europe.
Gilles Bon-Maury a été élu président d'HES en juin 2007. Il milite en particulier pour l'ouverture du mariage aux couples de même sexe et pour la reconnaissance de l'homoparentalité,,,. En 2008, HES se consacre à la présentation d'une contribution thématique au congrès de Reims du PS. HES participe par ailleurs aux campagnes des candidats socialistes pour les élections municipales et cantonales en 2008, européennes en 2009 et régionales en 2010.
En avril 2009, HES dépose une contribution aux états généraux de la bioéthique, et prend position en faveur de la dépénalisation et de l'encadrement de la gestation pour autrui. En décembre 2010, Gilles Bon-Maury signe avec soixante personnalités, dont Élisabeth Badinter, Antoinette Fouque, Caroline Fourest, Geneviève Fraisse, une tribune en faveur de l'encadrement de la gestation pour autrui.
Gilles Bon-Maury est responsable du groupe « Nouvelles familles » constitué au sein du Laboratoire des idées du PS en 2009. Le groupe a publié ses conclusions en décembre 2010. Elles proposent de réformer le droit de la famille pour le fonder, non plus sur la référence biologique, mais sur une éthique de la responsabilité.
En avril 2011, Gilles Bon-Maury défend, sur son blog et dans la presse, l'affiche de l'Inter-LGBT pour la Marche des fiertés 2011 représentant un coq avec un boa. Cette affiche a été mise en cause par plusieurs associations (Le Refuge, Lesbiennes of Colour (LOC)...) qui demandaient le retrait d'une affiche véhiculant selon elles des stéréotypes « racistes et pétainistes » ou « réducteurs et contre-productifs ».
À l'occasion des primaires citoyennes organisées par le PS en 2011, HES a invité chacun des six candidats à prendre position en répondant à 17 questions (lutte contre les discriminations, droits des couples et des familles LGBT, droits des personnes trans, lutte contre le VIH/sida, etc.).
Gilles-Bon Maury n'est pas candidat pour un nouveau mandat à la présidence d'HES lors de l'assemblée générale annuelle du 8 septembre 2012. Son successeur Denis Quinqueton, précédemment membre du bureau national chargé du projet, a été élu président de l'association.


=== Équipe de campagne de François Hollande ===
En décembre 2011, Gilles Bon-Maury rejoint l'équipe de campagne de François Hollande, candidat socialiste à l'élection présidentielle, pour y suivre les questions LGBT.
Il fait valoir l'engagement du candidat socialiste en faveur de l'ouverture du mariage et de la reconnaissance de l'homoparentalité (partage de l'autorité parentale, ouverture de l'adoption et de la procréation assistée). Réagissant à la Une erronée du quotidien Libération affichant Mariage gay : Sarkozy tenté par le oui, le 13 janvier 2012, il a dénoncé le projet inégalitaire d'union civile, « sous-mariage » déjà proposé par Nicolas Sarkozy en 2007, susceptible de réapparaitre dans le programme du candidat sortant en 2012,.


=== Ministère des Droits des femmes ===
Le 20 août 2012, Gilles Bon-Maury est nommé conseiller chargé de l'accès aux droits et de la lutte contre les violences faites aux femmes auprès de Najat Vallaud-Belkacem, porte-parole du gouvernement, ministre Des droits des femmes dans le gouvernement de Jean-Marc Ayrault.
Le 7 septembre 2012, Najat Vallaud-Belkacem présente aux associations LGBT la mission interministérielle que lui a confiée le Premier ministre, de lutte contre les violences et les discriminations commises à raison de l'orientation sexuelle ou de l'identité de genre. Elle confie ce dossier à Gilles Bon-Maury. Un programme d'action gouvernemental est présenté au conseil des ministres le 31 octobre 2012.
Dans le premier gouvernement de Manuel Valls (31 mars - 25 août 2014), Najat Vallaud-Belkacem est ministre des Droits des femmes, de la Ville, de la Jeunesse et des Sports. Gilles Bon-Maury est alors son conseiller chargé des Droits des femmes.


=== France Stratégie ===
Gilles Bon-Maury a rejoint France Stratégie, le commissariat général à la stratégie et à la prospective, en 2015. Il est notamment coauteur de deux rapports publiés par l'institution en 2016 : Le coût économique des discriminations et Lignes de faille. Une société à réunifier,.
Depuis 2016, Gilles Bon-Maury est le secrétaire permanent de la Plateforme RSE, instance de concertation installée à France Stratégie, consacrée à la responsabilité sociétale des entreprises. Il enseigne également à Sciences Po.


== Voir aussi ==
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation • Bibliothèque du Congrès • WorldCat


=== Bibliographie ===
Gilles Bon-Maury, Lesbiennes, gays, bis, trans. Aimable clientèle, éd. Bruno Leprince, coll. « Café République », octobre 2008 (ISBN 978-2-916333-48-9) ;
Gilles Bon-Maury, Familles en miettes, éd. Bruno Leprince, coll. « Café République », janvier 2010 (ISBN 978-2-916333-59-5) ;
Gilles Bon-Maury, Liberté des mœurs, égalité des droits, Fondation Jean Jaurès, coll. « Les essais », juin 2011 (ISBN 978-2-36244-021-2).
Gilles Bon-Maury, Catherine Bruneau, Clément Dherbécourt, Adama Diallo, Jean Flamand, Christel Gilles et Alain Trannoy, Le coût économique des discriminations, France Stratégie, septembre 2016 (lire en ligne).
Jean Pisani-Ferry, Fabrice Lenglart, Daniel Agacinski et Gilles Bon-Maury, Lignes de faille. Une société à réunifier, France Stratégie, octobre 2016 (lire en ligne).
Jean-Pierre Chanteau, Kathia Martin-Chenut et Michel Capron (préf. Gilles Bon-Maury), Entreprise et responsabilité sociale en questions : savoirs et controverses, éd. Classiques Garnier, coll. « Rencontres », avril 2017 (ISBN 978-2-406-06933-1).


=== Liens internes ===


== Références ==

 Portail LGBT   Portail de la politique française
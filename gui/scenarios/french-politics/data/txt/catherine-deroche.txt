Catherine Deroche, née le 24 février 1953, est une femme politique française, membre du parti Les Républicains.


== Biographie ==
Elle devient maire de Bouchemaine en 1999, puis s'est fait réélire en 2001 pour un second mandat. Elle devient vice-présidente de la Communauté d'agglomération d'Angers Loire Métropole.
Élue conseillère régionale des Pays de la Loire lors des élections régionales de 2010, elle devient sénatrice de Maine-et-Loire le 12 octobre 2010, en remplacement de Christian Gaudin, nommé préfet. Elle est élue sénatrice en septembre 2011.
Elle soutient François Fillon pour la primaire présidentielle des Républicains de 2016.


== Détail des mandats et fonctions ==
Mandat parlementairedepuis le 12 octobre 2010 : sénatrice de Maine-et-LoireMandats locaux1999 - 2001 : maire de Bouchemaine
2001 - 2008 : maire de Bouchemaine
vice-présidente de la Communauté d'agglomération d'Angers Loire Métropole
depuis 2010 : conseillère régionale des Pays de la LoireFonctions politiquesSecrétaire départementale de l'UMP en Maine-et-Loire


== Notes et références ==


== Voir aussi ==


=== Articles connexes ===
Bouchemaine
Communauté d'agglomération d'Angers Loire Métropole
Conseil régional des Pays de la Loire
Liste des sénateurs de Maine-et-Loire


=== Lien externe ===
Sa fiche sur le site du Sénat
 Portail de la politique française   Portail de l’Anjou et de Maine-et-Loire   Portail du gaullisme
Dominique Plancke, né à Lille en 1957, est un homme politique et militant écologiste français, membre fondateur des Verts.


== Parcours politique et professionnel ==
Dominique Plancke participe, à partir de 1976, à la lutte contre l'extension du camp militaire du Larzac[réf. nécessaire]. Il est resté un proche de José Bové qu'il était venu soutenir dans son occupation d'une ferme du hameau de Montredon achetée par l'armée. 
Il est animateur de la Maison de la nature et de l'environnement de Lille de 1979 à 1983, puis directeur de 1983 à 1989.
Il participe à la fondation des Verts en 1984, et en est le secrétaire national de 1993 à 1995. Il est ensuite directeur administratif du secrétariat national de ce même parti de 1997 à 2001.
Il est membre de la Commission nationale du débat public de novembre 2004 à décembre 2015, président de l'association de Montredon (Larzac) depuis juillet 2005 et directeur du CEDIS, organisme agréé de formation pour élus locaux, depuis novembre 2011.
Il coordonne l'intercollectif de soutien aux Roms du Nord-Pas-de-Calais depuis janvier 2016.


== Parcours judiciaire ==
Poursuivi devant le tribunal d'Avesnes (Nord) pour l'arrêt d'un train de déchets nucléaires, Dominique Plancke bénéficie d'une relaxe en novembre 2003. 
Il participe, avec José Bové, à l'action des Faucheurs volontaires (contre les OGM). Il est condamné à deux mois de prison avec sursis par le tribunal de grande instance de Lille dans le procès des 11 d'Avelin en novembre 2005. Relaxé en décembre 2005 par le tribunal d'Orléans pour des faits similaires commis le 14 août 2004, il est condamné à deux mois de prison avec sursis par la cour d'appel d'Orléans en juin 2006, le parquet ayant fait appel. Cette condamnation est confirmée par la Cour de cassation le 1er juin 2007.
Il est convoqué à nouveau le 2 octobre 2007 avec 38 autres faucheurs devant le tribunal de Villefranche-de-Lauragais (Haute-Garonne) pour avoir, en état de récidive légale, participé à la neutralisation d'une culture de maïs OGM (Mon 810) à Ox (Haute-Garonne) le 30 juillet 2006. Ce procès est à nouveau reporté à la demande du gouvernement pour ne pas interférer avec le « Grenelle de l'environnement ». L'audience a finalement lieu les 5 et 6 juin 2008 devant le tribunal de Toulouse. Dominique Plancke est condamné le 4 septembre 2008 à 120 jours-amende à 100 euros.
Jugé avec 86 autres faucheurs volontaires devant le tribunal de Marmande le 11 octobre 2010 pour une autre action contre les OGM en novembre 2006, il est condamné le 16 novembre 2010 à 120 jours amende à 50 euros. Le procureur avait demandé au tribunal de requalifier les faits en délit de fauchage d'OGM, renonçant ainsi à prendre en compte la récidive.
Pour avoir participé avec le Field Liberation Movement à une action contre un essai de pommes de terre OGM à Wetteren (Belgique) en mai 2011, Dominique Plancke fait l'objet d'un mandat d'amener de la police belge, aujourd'hui levé.


== Mandats politiques ==
Adjoint au maire de Lille de 1989 à 1993.
Conseiller municipal de Lille, délégué au patrimoine, de mars 2001 à avril 2014. Président du Conseil de quartier de Saint-Maurice Pellevoisin de 2008 à 2014. Conseiller communautaire de Lille Métropole Communauté urbaine de mars 2001 à janvier 2012.
Adjoint honoraire de Lille par arrêté préfectoral du 6 février 2015.
Membre de la Commission régionale du Patrimoine et des Sites de 2002 à 2015, ainsi que du bureau de l'association nationale des Villes et Pays d'Art et d'Histoire de 2009 à 2014.
Conseiller régional du Nord-Pas-de-Calais de mars 1998, réélu en mars 2010, et président de la commission transports, jusqu'en décembre 2015.
Vice-président du Groupement des autorités responsables de transport (GART) de 2005 à 2010.
Membre de la Commission nationale du débat public (CNDP) de 2004 à 2010 puis de 2012 à 2015


== Références ==


== Liens externes ==
Europe Écologie-Les Verts de Lille 
Le blog des élu(e)s Verts de Lille 
Association de Montredon (Larzac)La Roque-Sainte-Marguerite
Intercollectifs Roms 59/62 Portail du Nord-Pas-de-Calais   Portail de la politique française
These are Wikipedia pages extracted by Camille, given in input the list of "user names" in the 2016 politician dataset appearing in
data/rdf/rdf_05/data/rdf/rdf_05

There are 
* 797 pages which have matched exactly the name we were looking for
* 1566 pages which matched approximately but not exactly 

For many names there were no Wikipedia pages, but that is normal given that not all the French political players are famous enough to have a Wikipedia page.

Tweets of these people are archived in the CEDAR cluster by Duc, as JSON documents; we have them from 2016 to 2018, it's quite a lot of JSON.


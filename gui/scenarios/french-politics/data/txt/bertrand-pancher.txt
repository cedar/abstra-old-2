Bertrand Pancher, né le 5 juin 1958 à Saint-Mihiel (Meuse), est un homme politique français.


== Biographie ==
Particulièrement sensible aux questions environnementales et spécialisé dans le domaine de la gouvernance, Bertrand Pancher, député du sud-meusien, est secrétaire de la Commission du développement durable et de l’aménagement du territoire de l’Assemblée nationale.
Bertrand Pancher est titulaire d'une licence en droit et maîtrise en sciences économiques.
Membre de l'UDF puis de l'UMP, il rejoint le Parti radical dirigé par Jean-Louis Borloo en mai 2011.


=== Carrière politique ===
À la suite de la nomination de Gérard Longuet comme Ministre Délégué à La Poste et à France Telecom en mars 1986, il devient membre de son cabinet jusqu'à la démission du gouvernement le 10 mai 1988.
Il se présente aux élections municipales de 1989 à Bar-le-Duc, mais il perd face au maire sortant Jean Bernard (PS). Après 6 ans dans l'opposition municipale, il est élu maire de Bar-le-Duc aux élections municipales de juin 1995.
Il devient conseiller général du canton de Bar-le-Duc-Nord en mars 1992 et Vice-Président du Conseil général de la Meuse en mars 1994. En mars 2001, alors juste réélu à la mairie de Bar-le-Duc, il cède son siège à son adjointe Martine Huraut (UMP) pour pouvoir devenir Président du Conseil général de la Meuse.
De 2003 à 2005, il est conseiller chargé de la démocratie participative et du débat public auprès du Ministre de l'Écologie.
Il ne retrouve pas son poste de conseiller général aux élections cantonales de 2004, les Barisiens n'ayant pas apprécié son choix en 2001 d'abandonner son poste de maire pour celui de président du conseil général.
Il est élu député le 17 juin 2007, pour la XIIIe législature (2007-2012), dans la 1re circonscription de la Meuse en battant, au deuxième tour, Thibaut Villemin (PS) avec 53,97 % des suffrages. Il succède ainsi à François Dosé (PS) qui ne se représentait pas. Il est réélu le 17 juin 2012. À l'Assemblée nationale, il est membre de la Commission du développement durable et de l'aménagement du territoire.
Dès son élection en 2007, il s’est particulièrement distingué par sa participation aux travaux du Grenelle de l’environnement, en tant que membre du Groupe V « Construire une démocratie écologique », chargé de réfléchir à la gouvernance environnementale dans le cadre de la phase préparatoire du Grenelle. La mise en œuvre des conclusions du groupe de travail a ensuite favorisé sa nomination par le Premier ministre comme parlementaire en mission auprès de Jean-Louis Borloo, chargé de travailler à l’élaboration de critères de représentativité des acteurs environnementaux. 
Le 26 janvier 2011, fort de son remarquable travail en tant que co-rapporteur sur le projet de loi Grenelle II, il fut désigné par la Commission Développement Durable et de l’aménagement du territoire, rapporteur pour le suivi de la mise en application du texte. Il a rendu un rapport d’information en février 2012 sur ce sujet.
Après avoir représenté l’Assemblée nationale lors de la Conférence sur le climat à Durban en décembre 2011, il a représenté le groupe Union des Démocrates et Indépendants lors de la conférence environnementale qui s’est tenue à Paris en septembre 2012.
En mars 2012, il a présenté au Président de la République, Nicolas Sarkozy, un rapport sur la démocratie écologique afin d’améliorer le dialogue environnementale en France que celui-ci lui avait confié. Bertrand Pancher lui a présenté ses propositions afin d’améliorer les règles de concertation dans notre pays.
Réélu en 2012, il continue son travail au sein de la Commission du développement durable, identifié comme l’un des parlementaires les plus actifs sur la thématique du développement durable.
Depuis le 15 juin 2013, il fait partie de l'équipe resserrée du Contre-Gouvernement de l'UDI, chargé des Territoires et du développement durable. Il est responsable du pôle écologie.
Il se présente aux élections municipales de 2014 et devient le nouveau maire de Bar-le-Duc le 4 avril 2014. Le 14 avril 2014, il prend la présidence de la Communauté d'agglomération Bar-le-Duc Sud Meuse.
Le 18 décembre 2014, il organise une convention du pôle écologie au siège de l'UDI, en présence d'universitaires, de 200 participants et du nouveau président de l'UDI, Jean-Christophe Lagarde, pour préparer la conférence sur le climat COP 21 de Paris, qui se tiendra en décembre 2015 .
Le 9 septembre 2014, il est nommé Vice-président de la Commission spéciale pour l'examen du projet de loi relatif à la transition énergétique pour la croissance verte.
Souhaitant renforcer la concertation avec l’ensemble des acteurs concernés par les dossiers sensibles de son département (Laboratoire de Bure sur le stockage des déchets nucléaires, création de zones d’épandage de crues…) il créa l’Institut local du débat public dont il confia l’animation à Dominique Bourg, sociologue proche de Nicolas Hulot, préfigurant ainsi « Décider Ensemble » qu’il préside sur le plan national.
Réélu aux élections législatives de juin 2017, Bertrand Pancher démissionne de ses mandats de maire de Bar-le-Duc et de président de la communauté d'agglomération Bar-le-Duc Sud Meuse conformément aux lois du 14 février 2014 sur le non-cumul des mandats. Sa 2e adjointe, Martine Joly, le remplace à ces postes.
Le 20 septembre 2017, François de Rugy, Président de l'Assemblée nationale, annonce sa nomination dans l'un des groupes de travail visant à réformer l'Assemblée nationale. Son groupe de travail est "l'ouverture de l’assemblée nationale à la société et son rayonnement scientifique et culturel".
Fin octobre 2017, il relaie à l'Assemblée nationale, avec des députés LR, un amendement portant sur la fiscalité des entrepôts et fourni par le Medef et la Confédération des petites et moyennes entreprises,,.


=== Autres activités ===
Parallèlement à ses activités politiques, de 1990 à 2003, il crée et dirige la société Archimest, spécialisée dans le stockage et la gestion d'archives. Il crée aussi l'association Décider Ensemble dont il est le Président du conseil d'administration. L'association a pour objet de promouvoir et de développer une culture commune du dialogue et de la concertation en matière de préparation et de prise des décisions.


== Détails des fonctions et mandats ==


=== Mandats parlementaires ===
Assemblée Nationale20 juin 2007 - 20 juin 2012 : Député de la 1re circonscription de la Meuse (XIIIe législature)
20 juin 2012 - 21 juin 2017 : Député de la 1re circonscription de la Meuse (XIVe législature)
21 juin 2017 : Député de la 1re circonscription de la Meuse (XVe législature)


=== Mandats locaux ===
Conseil général29 mars 1992 - 22 mars 1998 : Conseiller général du canton de Bar-le-Duc-Nord
22 mars 1998 - 28 mars 2004 : Conseiller général du canton de Bar-le-Duc-Nord
Vice-président du Conseil général de la Meuse de mars 1994 à mars 2001
Président du Conseil général de la Meuse de mars 2001 au 28 mars 2004Intercommunalitéjanvier 2002 - mars 2008 : Président de la Communauté de communes de Bar-le-Duc
14 avril 2014 - 6 juillet 2017 : Président de la Communauté d'agglomération Bar-le-Duc Sud MeuseMairie12 mars 1989 - 10 juin 1995 : Conseiller municipal de Bar-le-Duc
11 juin 1995 - 18 mars 2001 : Maire de Bar-le-Duc
19 mars 2001 - 16 mars 2008 : Adjoint au maire de Bar-le-Duc
4 avril 2014 - 29 juin 2017 : Maire de Bar-le-Duc


=== Fonctions politiques ===
20 mars 1986 – 10 mai 1988 : Membre du Cabinet du Ministre Délégué à La Poste et à France Telecom
2003 - 2005 : conseiller chargé de la démocratie participative et du débat public auprès du Ministre de l'Écologie


== Bibliographie ==
2010 : Démocratie apaisée : ces hirondelles qui annoncent le printemps, de Bertrand Pancher


== Notes et références ==


== Voir aussi ==


=== Bibliographie ===
Le Monde des 12 et 19 juin 2007


=== Articles connexes ===
Liste des députés de la Meuse
Liste des maires de Bar-le-Duc


=== Liens externes ===
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Le site personnel de Bertrand Pancher
Fiche d'identité sur le site de l'Assemblée nationale
Fiche d'identité sur le site du Parti Radical
 Portail de la politique française   Portail de la Meuse
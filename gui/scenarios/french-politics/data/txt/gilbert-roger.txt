Gilbert Roger, né le 9 novembre 1953, est une personnalité politique du Parti socialiste, actuel sénateur de la Seine-Saint-Denis et ancien maire de Bondy (Seine-Saint-Denis).


== Biographie ==
Gilbert Roger est élu conseiller général du canton de Bondy-Sud-Est de 1988, prenant la succession de Michel Beaufort, jusqu'en 2015. Il est premier vice-président du Conseil général de la Seine-Saint-Denis de 2004 à 2011. Il est également maire de Bondy de 1995 jusqu'au 13 octobre 2011.
Lors du Congrès du Mans, il rejoint le courant NPS. Puis il est l'un des premiers soutiens de Ségolène Royal dans sa campagne interne pour la candidature du PS à l'élection présidentielle de 2007. Au Congrès de Reims, il est membre de groupes des Reconstructeurs qui soutient la motion et la candidature de Martine Aubry au poste de première secrétaire nationale.
Réélu au Conseil général de la Seine-Saint-Denis comme premier vice-président en 2008, il est responsable des délégations de l'économie et de l'emploi, des affaires européennes, du tourisme et des affaires internationales ; en décembre 2008, il est désigné comme membre de la chambre des pouvoirs locaux au Conseil de l'Europe et siège à la commission de l'environnement durable.
En mars 2008, il est réélu maire au premier tour et premier vice-président du conseil général.
Après la réélection de Claude Bartolone à la présidence du Conseil général de la Seine-Saint-Denis en 2008, il quitte l'exécutif du conseil général pour prendre la présidence du Groupe socialiste, fonction qu'il conserve jusqu'en 2012. Candidat aux élections départementales de 2015 en duo avec Sylvine Thomassin, il n'obtient que 46,15 % des voix au second tour face au duo UMP de Katia Coppi et Stephen Hervé.
Il est l'un des promoteurs de la communauté d'agglomération Est ensemble fondée en 2010, qui regroupe 400 000 habitants de neuf communes dont Bondy.
Lors des élections sénatoriales de septembre 2011, il mène la liste PS-EELV-PRG-MRC-MGC devant Aline Archimbaud et Claude Dilain et souligne son engagement à ne pas cumuler sa fonction parlementaire avec celle de maire. Élu sénateur, il démissionne de son fauteuil de maire, en faveur de sa première adjointe, Sylvine Thomassin.
Président du Groupe d'amitié France-Palestine au Sénat, il est un des artisans de l'adoption d'une résolution invitant le gouvernement à la reconnaissance de la Palestine en décembre 2014, bien que la gauche y soit devenue minoritaire quelques semaines plus tôt et malgré l'appel du président de l'UMP à ne pas joindre leurs voix à cette résolution.
Il parraine la candidature de Benoît Hamon pour l'élection présidentielle de 2017.


== Liens externes ==
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
(fr) Blog officiel
(fr) Sa fiche sur le site du Sénat


== Notes et références ==

 Portail de la politique française   Portail de la Seine-Saint-Denis
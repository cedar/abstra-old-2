Jean-Yves Caullet est un homme politique français, né le 8 février 1957 à Roubaix, dans le Nord.


== Biographie ==
Il fait ses études secondaires et ses classes préparatoires au lycée Corneille de Rouen. Il intègre l’Institut National Agronomique de Paris-Grignon en 1976, puis l’École du Génie Rural des Eaux et des Forêts.
Il commence sa carrière en 1980 à la Direction Départementale de l’Agriculture du Bas-Rhin à Strasbourg. En 1983, il rejoint le Secrétariat d’État aux DOM-TOM et entre l’année suivante au cabinet du Ministre, Georges Lemoine, jusqu’en 1986.
En 1986, il entre dans la préfectorale, d’abord en tant que directeur de cabinet du Préfet d’Eure-et-Loir, puis sous-préfet d’arrondissement, à Saint Claude (Jura), puis à Avallon (Yonne).
Il est nommé préfet en février 1994.
En 1991, il rejoint le cabinet de François Mitterrand en tant que conseiller technique chargé des questions intérieures et de l’Andorre.
C’est pendant les cinq ans passés à la Présidence de la République, qu’il assiste à la création d’un nouvel État souverain et indépendant en participant activement aux négociations et à la rédaction de la constitution de l’Andorre, adoptée en 1993. Il sera le représentant personnel du coprince d’Andorre, Président de la République française, François Mitterrand puis Jacques Chirac, jusqu’en 1996.
De 1996 à 1997, il est chargé à la RATP des questions de sécurité.
En 2009, il crée l'ACSY (Ambition Citoyenne et Solidaire pour l'Yonne) pour fédérer les élus du département sensibles aux idées républicaines de gauche et pour avoir un lieu d’échanges, de débats et de partage d’expériences.
Suite au décès de Gilles Ménage, il devient secrétaire général de l'Institut François Mitterrand en 2017.


== Parcours politique ==
Le 15 mars 1999, il devient député en remplacement d'Henri Nallet, dont il était le suppléant. Il est néanmoins battu aux élections législatives de 2002 et de 2007 par Jean-Marie Rolland.  
En 2001, il est élu maire d'Avallon, et est réélu en 2008 et en 2014, au premier tour de chaque élection.  
En 2004, il est élu conseiller régional de Bourgogne sur la liste de François Patriat dont il devient le vice-président en 2006, chargé de la Culture puis du Développement économique. Il est réélu en 2010 avant de démissionner à la suite de son élection comme conseiller général l'année suivante. 
En 2011, il est élu conseiller général du canton d'Avallon et prend la tête du groupe d'opposition du Conseil Général de l'Yonne. Il démissionne en 2012, après avoir été élu député. 
Le 20 juin 2012, il est élu député de la 2e circonscription de l'Yonne, avec 50,25 % des voix. Il fait partie du groupe socialiste. 
En 2015, la candidature de sa femme sur la liste socialiste aux élections régionales crée la controverse d'autant que cette dernière est également son assistante parlementaire.
Le 11 janvier 2017, il annonce sur France 3 Bourgogne son ralliement à Emmanuel Macron pour la présidentielle 2017. 
Il est officiellement investi par En Marche, le 11 mai 2017, dans la 2e circonscription de l'Yonne pour les élections législatives de juin 2017. Il est battu par André Villiers, candidat investi par l'UDI-LR. 


== Travail législatif ==

De 2012 à 2017, il fut membre de la Commission permanente Développement Durable et Aménagement du territoire, Président du Groupe d'étude Aménagement du territoire et vice-président du groupe d'amitié France-Andorre. 
A l'Assemblée, il a également été membre de la Mission d'information sur l'offre automobile française dans une approche industrielle, énergétique et fiscale ainsi que de la Mission d'information commune sur l'application de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques.
Le 4 septembre 2012, il est désigné membre titulaire du Conseil d'administration de l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME) par Claude Bartolone, Président de l'Assemblée Nationale.
Le 10 décembre 2012, le Premier ministre lui confie une mission sur la filière bois et forêt. 
Le 20 mars 2013, par décision du conseil des ministres, il est nommé Président du conseil d'administration de l'Office national des forêts, succédant ainsi à Hervé Gaymard.
Le 6 avril 2016, il est désigné rapporteur de la Commission d'enquête sur les conditions d'abattage des animaux de boucherie dans les abattoirs français.


== Mandats ==
1999 - 2002 2012 - 2017: Député de l'Yonne
depuis 2001 : Maire d'Avallon
2004 - 2011 : Conseiller régional de Bourgogne
2011 - 2012 : Conseiller général du canton d'Avallon


== Notes et références ==


== Voir aussi ==


=== Articles connexes ===
Avallon
Canton d'Avallon
Communauté de communes de l'Avallonnais
Conseil général de l'Yonne
Conseil régional de Bourgogne
Deuxième circonscription de l'Yonne
Liste des députés de l'Yonne


=== Liens externes ===
Sa fiche sur le site de l'Assemblée nationale
Site officiel Portail de la politique française   Portail de la Bourgogne   Portail de l’Yonne
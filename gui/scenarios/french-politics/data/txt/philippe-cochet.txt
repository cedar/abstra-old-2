Philippe Cochet, né le 23 mai 1961 à Lyon, est un homme politique français, membre du parti Les Républicains.
Il est élu pour la première fois député le 16 juin 2002, pour la XIIe législature (2002-2007), dans la cinquième circonscription du Rhône. Il fait partie du groupe UMP.
Il est réélu pour un deuxième mandat le 10 juin 2007 dès le premier tour avec 55,92 % des voix, soit le meilleur score du département.
Il est élu maire de Caluire-et-Cuire en mars 2008 et réélu en mars 2014.
Lors des élections législatives de 2012, il rate de peu une réélection au premier tour, le 10 juin, en obtenant 47,78 % des voix. Le 17 juin suivant, il est réélu pour son troisième mandat consécutif en obtenant 61,29 % de voix, face au candidat socialiste Jacky Darne.
Lors des élections législatives de 2017, il est devancé au premier tour par la candidate LREM, Blandine Brocard, (43,33 % contre 28,38 %) et battu par elle au second tour (58,19 % contre 41,81 %).


== Carrière politique ==


=== De 2002 à 2008 ===
Gérant de société et militant de l'UDF, Philippe Cochet est élu pour la première fois conseiller municipal de Caluire-et-Cuire en 1989. Il sera ensuite réélu conseiller municipal en 1995 puis adjoint en 2001. 
C'est en 2002 que Philippe Cochet se lance dans la bataille politique. La retraite du député sortant Jean Rigaud (député du Rhône de 1981 à 2002) attise les convoitises à droite. Alors que plusieurs prétendants se montrent intéressés, Cochet, alors membre de DL, parvient à obtenir l'investiture de la toute jeune Union pour la Majorité Présidentielle. Faisant cependant face à quatre candidatures dissidentes à droite, il rassemble sur son nom 32 % des suffrages exprimés au premier tour avant de s'imposer très largement au second tour face à son adversaire socialiste (65 % des voix).
C'est sans aucune discussion que Philippe Cochet, cette fois sortant, reçoit une nouvelle fois l'investiture de l'UMP pour les législatives de juin 2007. Il s'impose très largement dès le premier tour avec 55,92 % des suffrages exprimés devenant ainsi le député le mieux élu du département du Rhône.
En accord avec le président de la République Nicolas Sarkozy et sur proposition de Jean-Claude Gaudin, Philippe Cochet est nommé le 5 juillet 2007, secrétaire général adjoint de l'UMP (no 3 national du parti) dans la direction collégiale du mouvement.
Il doit néanmoins céder sa place pour des questions de parité hommes-femmes à Nathalie Kosciusko-Morizet, le 28 mars 2008. Cochet continue cependant à siéger dans le comité directeur du l'UMP (composé de 21 membres) en devenant Délégué général à la réforme.


=== Élections municipales de 2008 ===
Après avoir contraint le maire sortant, Alain Jeannot (RPR), à ne pas se représenter,, Philippe Cochet est tête d'une liste d'union de la majorité (UMP-Nouveau Centre-Parti radical) à Caluire-et-Cuire dont près du quart des membres est issu de l'équipe municipale RPR-UDF sortante. Cochet reçoit dès le début de la campagne le soutien officiel d'Alain Jeannot (RPR), maire de 1997 à 2008, et de Bernard Roger-Dalbert (UDF), maire de 1983 à 1997 et Vice-Président du Conseil Général du Rhône. 
Annoncé comme le favori, Philippe Cochet remporte le scrutin dès le premier tour avec 56,23 % des voix ; loin devant la liste PS conduite par Pierre Ferraro (30,76 % des suffrages).
En refusant de collaborer au Grand Lyon avec Gérard Collomb et en critiquant les maires de droite des communes de l'Ouest Lyonnais (Monts d'Or et Val-de-Saône) qui avaient voté pour ce dernier à la présidence du Grand Lyon, il est présenté par la presse lyonnaise comme le principal opposant au maire socialiste de Lyon dans la communauté urbaine. En tant que député-maire de la quatrième ville la plus peuplée du Rhône (les trois premières villes du département étant dirigées par le PS et le PCF) et à la suite de la débâcle de Dominique Perben à Lyon, son influence au sein de la droite lyonnaise s'en trouve renforcée.
Lors des élections législatives de 2017, sept maires de droite du Val-de-Saône et des Monts d’Or soutiennent son adversaire, Blandine Brocard, adjointe au maire de Saint-Germain-au-Mont-d'Or.


=== La Fédération UMP du Rhône ===
Fin mai 2008, Philippe Cochet, fort de son statut de député-maire de la première ville de droite du département, s'est déclaré dans plusieurs médias candidat à la succession de Dominique Perben à la présidence de la fédération départementale de l'UMP du Rhône.
Le 25 novembre 2008, Philippe Cochet est élu délégué départemental UMP de la 5e circonscription du département. Il a officialisé le 26 novembre 2008 sa candidature au poste de président de la fédération de l'UMP du Rhône.
Le 6 décembre 2008, Philippe Cochet est élu au poste de président de la fédération de l'UMP du Rhône dans une élection sans suspense puisqu'il en était le seul candidat (Dominique Perben le président sortant n'ayant pas souhaité se représenter). Malgré l'absence d'enjeux les grands électeurs du Rhône se sont fortement mobilisés puisque 264 des 323 délégués du conseil départemental ont participé au vote et 248 ont apporté leur soutien au nouveau président.
Ses premières déclarations vont dans le sens de l'union de la droite lyonnaise, avec un appel au rassemblement adressé aux millonistes.


=== 2009 à 2015 ===


==== Politique nationale ====
À la suite de la réunion du conseil national du 24 janvier 2009, le poste de Délégué général à la réforme est supprimé de l'organigramme de l'UMP. Philippe Cochet devient membre de la commission nationale d'investitures mais perd sa place au sein du bureau politique du parti.
À la suite de l'élection de Xavier Bertrand comme secrétaire général de l'UMP, Philippe Cochet est nommé en mars 2009 secrétaire national chargé des grandes métropoles et de la politique de la ville.
Le 15 novembre 2010 il est réélu au poste de président de la fédération de l'UMP du Rhône.
En janvier 2011, Philippe Cochet est nommé président par intérim de la fédération de l'UMP de l'Isère à la suite de la démission de son président. Il est chargé, avec Hervé Novelli, d'organiser les élections internes de la fédération iséroise au printemps 2011.
En février 2015, le journal L'Express lui consacre un dossier, dans lequel il explique ses propos controversés de 2013 contre le mariage homosexuel et déclare qu'il n'aura pas à choisir en 2017 entre son mandat de maire et celui de député, car la loi interdisant le cumul sera d'ici là abrogée, selon lui,.
Philippe Cochet cumule les indemnités de trois mandats et emploie sa femme en tant que collaboratrice parlementaire, dont la réalité de l'emploi est mise en cause .
Il soutient Nicolas Sarkozy pour la primaire présidentielle des Républicains de 2016. Dans le cadre de sa campagne, il est nommé orateur national chargé des collectivités territoriales.
Lors des élections législatives de 2017, il est battu par la candidate LREM (ex membre du MoDem), Blandine Brocard, (58,19 % contre 41,81 %). Le 4 juillet 2017, il déclare au journal Le Progrès : « Je suis victime des élections, je suis mort certes, mais avec le drapeau Les Républicains, et ça, j'en suis fier. ». 


==== Politique municipale ====
Le 10 février 2009, il retire ses délégations à son adjointe Michèle Vianès, après qu'il a appris son appartenance au parti Debout la République,,,.
La dette de la commune a augmenté de 183 % entre 2000 et 2012 et a doublé entre 2010 et 2015.


== Synthèse des fonctions politiques ==


=== Fonctions au sein de partis politiques ===
En janvier 2013, à la suite de l’accord entre Jean-François Copé et François Fillon qui a suivi la crise politique du congrès de novembre 2012, il est nommé avec le filloniste Jérôme Chartier délégué général à l'animation du parti.

Délégué national chargé des fédérations du sud de Les républicains (8 juin 2015)
Délégué général de l'UMP chargé de l'animation (14 janvier 2013-juin 2015)
Délégué général adjoint de l'UMP chargé des fédérations (12 janvier 2011 au 14 janvier 2013)
Secrétaire National du Mouvement Populaire (UMP) Chargé des Grandes Métropoles et Politique de la Ville (3 mars 2009 au 12 janvier 2011)
Délégué général de l'UMP chargé de la réforme (28 mars 2008-24 janvier 2009)
Membre du comité directeur de l'UMP (depuis juillet 2007)
Secrétaire général adjoint de l'UMP (juillet 2007-Mars 2008)
Membres de la commission nationale d'investitures (depuis le 24 janvier 2009)
Président de la fédération UMP du Rhône (depuis le 25 novembre 2008)
Président par intérim de la fédération UMP de l'Isère (janvier 2011 à juillet 2011)
Délégué départemental de la 5e circonscription du Rhône (depuis le 6 décembre 2008)


=== Fonctions électives ===
Maire de Caluire-et-Cuire (Rhône) (depuis mars 2008)
Conseiller communautaire du Grand Lyon, Rhône (depuis mars 2008)
Député de la 5e circonscription du Rhône (depuis juin 2002)


==== Mandats ====
20/03/1989 - 18/06/1995 : membre du conseil municipal de Caluire-et-Cuire (Rhône)
19/06/1995 - 18/03/2001 : membre du conseil municipal de Caluire-et-Cuire (Rhône)
16/06/2002 - 14/03/2008 : adjoint au Maire de Caluire-et-Cuire, (Rhône)
19/06/2002 - 19/06/2007 : député de la 5e circonscription du Rhône
20/06/2007 - 25/06/2012 : député de la 5e circonscription du Rhône
26/06/2012 - 18/06/2017 : député de la 5e circonscription du Rhône
16/03/2008 - 10/03/2014 : maire de Caluire-et-Cuire, (Rhône)


===== Mandat local au 14/03/2008 =====
Maire de Caluire-et-Cuire, Rhône depuis le 14 mars 2008
Conseiller communautaire du Grand Lyon, Rhône depuis mars 2008


===== Mandat national au 20/06/2012 =====
Député de la 5e circonscription du Rhône


== Notes et références ==


== Liens externes ==
Site officiel de Philippe Cochet
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Ressources relatives à la vie publique : Assemblée nationale • Base Sycomore
 Portail de la politique française   Portail de la métropole de Lyon
Nicolas Perruchot, né le 9 juillet 1966 dans le 13e arrondissement de Paris, est un homme politique français. Membre de l'UDF, du Nouveau Centre puis de l'UMP, il a été maire de la ville de Blois de 2001 à 2008.


== Biographie ==
Il commence par travailler comme consultant en entreprise, chez France Télécom puis dans l'entreprise de son beau-père. Il est adhérent du RPR puis rejoint l'UDF. Il remporte son premier mandat à 34 ans quand il est élu conseiller général en 2000 (canton de Blois-3) avec 3 voix d'avance, lors d'une élection partielle. Un an plus tard, lors des municipales il bat avec 37 voix d'avance le maire PS sortant Jack Lang. Cette victoire sur le fil face à un adversaire très connu au plan national lui vaut une notoriété médiatique, la presse le surnommant « le tombeur de Jack Lang ». L'année suivante, en juin 2002, il est élu député de la 1re circonscription de Loir-et-Cher avec 600 voix d'avance.
En 2005, il devient également président de la communauté d'agglomération de Blois (Agglopolys).
Parallèlement, Nicolas Perruchot mène d'importants travaux dans le centre-ville dans le but de rendre le centre historique piétonnier ou semi-piétonnier, dynamiser le commerce de proximité et remettre sur le marché les logements vacants (lancement d'une OPAH) pour qu'à long terme le centre-ville soit le troisième centre commercial de l'agglomération.
Son mandat a vu l'apparition de deux nouveaux services municipaux : le « SIR » (Service d'intervention rapide) et « Blois Solidarité Canicule », et des opérations de communication : « Nicolas Perruchot à votre écoute » (un numéro vert pour un dialogue direct avec le maire), « la Mairie à domicile » ou « l'espace Parents » sont autant d'initiatives qui veulent placer la proximité au cœur de l'action municipale.
En tant que maire et président de la communauté d'agglomération de Blois, Nicolas Perruchot a entrepris d'importants travaux d'urbanisme, notamment dans la ZUP des quartiers nord de la ville à travers un plan de rénovation urbaine d'un montant de 257 454 946 €. Il mène également une politique visant à favoriser l'implantation de PME en zone franche.
En 2003, il crée le festival Tous sur le pont, dont le directeur artistique sera Philippe Manœuvre. Après son élection, Marc Gricourt, nouveau maire de Blois, décide de le supprimer après sa dernière édition en 2008 et de le remplacer par le festival Mix'Terres.
En 2007, après avoir soutenu François Bayrou au premier tour de l'élection présidentielle, il appelle à voter pour Nicolas Sarkozy au second tour. Il participe à la création du Nouveau Centre, qui constitue le pôle centriste de la majorité présidentielle. Candidat aux élections législatives de juin 2007, il conserve son siège de député avec une avance de 321 voix sur la circonscription (Nicolas Perruchot - NC : 50,3 %, Geneviève Baraban - PS : 49,7 %).
Il a été désigné porte-parole du Nouveau Centre au sein de son organisation provisoire.
Nicolas Perruchot a dénoncé lors d'un point-presse (début novembre 2007) « un calendrier parlementaire trop chargé » et « un manque de cohérence et de lisibilité dans l'action du gouvernement ». Des propos qui ont créé la surprise au sein de l'UMP comme du NC. Il remettra ainsi sa démission du poste de porte-parole à l'Assemblée nationale du Nouveau Centre, tout en restant membre du parti, le mardi 13 novembre 2007 à François Sauvadet, président du groupe Nouveau Centre à l'Assemblée nationale.
Nicolas Perruchot est battu à Blois aux élections municipales de 2008. Il avait obtenu 29,09 % des suffrages lors du premier tour et n'a recueilli que 39,31 % des voix contre 60,69 % pour son adversaire socialiste, Marc Gricourt, lors du second tour des élections. Il sera remplacé à la tête de l'agglomération de Blois (Agglopolys) par Christophe Degruelle (PS).
Il est membre du groupe d'études sur le problème du Tibet de Assemblée nationale.
Il est candidat à la présidence de son groupe parlementaire, le 6 juillet 2011, en remplacement de François Sauvadet, nommé au gouvernement. Yvan Lachaud remporte l'élection dès le premier tour avec treize voix, Nicolas Perruchot obtient quant à lui deux voix.
Nicolas Perruchot est responsable à l'Assemblée nationale d'un rapport d'enquête sur Les Mécanismes de financement des organisations syndicales d'employeurs et de salariés,. Pour la première fois de l'histoire de la Ve République, un rapport parlementaire n'est pas publié. Le 30 novembre 2011, les 3 élus PS de la commission ayant voté contre, les élus Francis Vercamer et Nicolas Perruchot ainsi que Arnaud Richard ayant voté pour, les autres élus UMP s'abstenant ou n'étant pas présents, faute de majorité le rapport est rejeté. Néanmoins le rapport est mis en ligne par Le Point en février 2012, auquel il manque les comptes-rendus d'audition et les annexes. Nicolas Perruchot évoque un financement de l'ordre de quatre milliards d'euros par an.
Il soutient publiquement le chef Raoni dans son combat contre le Barrage de Belo Monte, il lui a notamment offert sa médaille de l'Assemblée nationale lors de sa venue en France en 2011.
En juin 2012, il perd son mandat parlementaire lors des élections législatives au profit du socialiste Denys Robiliard, adjoint au maire de Blois.
Il rejoint l'UMP en octobre 2012, après la création de la confédération centriste, l'UDI.
Il retrouve le Conseil départemental de Loir-et-Cher à l'occasion des élections départementales de 2015. En effet, il est élu conseiller départemental du nouveau Canton d'Onzain. Cette élection a lieu a au second tour, dans le cadre d'une triangulaire. Nicolas Perruchot et son binome Catherine Lheritier	sont élus avec 38,69 % des voix. Le 2 avril, Nicolas Perruchot est élu premier vice-président du Conseil départemental, aux côtés du président Maurice Leroy. 
Quelques mois avant le renouvellement du Conseil régional du Centre, Hervé Novelli, alors président du groupe d'opposition, démissionne de cette fonction. C'est Nicolas Perruchot qui est élu par ses pairs pour lui succéder jusqu'aux élections régionales de décembre 2015.  
Dans le cadre de l'élection régionale de 2015 en Centre-Val de Loire, Nicolas Perruchot est en 5e position sur la liste départementale UDI-les Républicains menée par Guillaume Peltier. Le fait qu'il passe de la 1re (en 2010) à la 5e position trouve son explication dans le fait que Guillaume Peltier et Marc Fesneau, respectivement en 1re et 3e position sont les chefs de file de leurs partis politiques respectifs au niveau régional (les Républicains et MoDem).


== Mandats actuels ==
Conseiller départemental du canton d'Onzain depuis 2015.
Président du Conseil départemental de Loir-et-Cher depuis 2017.


== Mandats échus ==
19/6/2002 - 17/6/2012 : député de Loir-et-Cher
19/3/2001 - 16/3/2008 : maire de Blois
01/1/2005 - 16/3/2008 : président de la communauté d'agglomération de Blois
10/7/2000 - 12/7/2002 : conseiller général de Loir-et-Cher (canton de Blois-3)
01/10/1998 - 19/3/2001 et 16/3/2008 - 29/4/2010 : conseiller municipal de Blois (opposition municipale)
Conseiller régional de la région Centre-Val de Loire


== Résultats des élections ==


=== Résultats des élections régionales de mars 2010 en Loir-et-Cher ===
François Bonneau (PS - EÉLV - FdG) 48,00 %
Hervé Novelli (UMP - NC)	 37,04 %
Nicolas Perruchot, quant à lui, est la tête de liste « majorité présidentielle » en Loir-et-Cher.
Philippe Loiseau (FN)	 14,96 %Sur la Région Centre : 

François Bonneau (PS - EÉLV - FdG) 50,00 %
Hervé Novelli (UMP - NC)	 36,46 %
Philippe Loiseau (FN)	 13,54 %


=== Résultats des élections municipales de mars 2008 ===
Nicolas Perruchot (UMP) pour la liste « J'aime Blois » : 39,31 %
Marc Gricourt (PS) pour la liste « Tous différents, tous blésois » : 60,69 %


=== Résultats des législatives de juin 2007 ===
Résultats sur la première circonscription :

Nicolas Perruchot (MAJ) : 50,3 %
Geneviève Baraban (PS) : 49,7 %
Participation : 60,4 %
Abstention : 39,6 %Sur la ville de Blois : 

Nicolas Perruchot (MAJ) : 45,7 %
Geneviève Baraban (PS) : 54,3 %Sur la communauté d'agglomération de Blois (Agglopolys):

Nicolas Perruchot (MAJ) : 48,0 %
Geneviève Baraban (PS) : 52,0 %


=== Résultats des législatives de juin 2002 ===
Résultats sur la première circonscription :

Nicolas Perruchot (UDF) : 50,6 %
Michel Fromet (PS) : 49,4 %
Participation : 62,2 %
Abstention : 37,8 %


=== Résultats du second tour des élections municipales de 2001 ===
Nicolas Perruchot (UDF, FU) : 45,31 % (1er tour : 26,41 %)
Jack Lang (PS) : 45,09 % (1er tour : 34,63 %)
Miguel de Peyrecave (FN) : 9,61 % (1er tour : 17,47 %)
Abstention : 39,31 % (1er tour : 46,60 %)


== Notes et références ==


== Liens externes ==
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Site officiel
Fiche sur le site de l'Assemblée nationale Portail de la politique française   Portail de Loir-et-Cher   Portail du gaullisme
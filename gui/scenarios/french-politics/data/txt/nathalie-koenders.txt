Nathalie Koenders, née le 1er mars 1977 à Rennes, est une sportive française de haut niveau en canoë-kayak. Elle a remporté le championnat de France en K1 5 000 m en 1998.
Première adjointe au maire de Dijon, conseillère départementale de la Côte-d'Or, elle est membre du Parti socialiste. Elle est cadre au ministère de la Jeunesse et des Sports en détachement.


== Biographie ==


=== Études et formation ===
Après avoir suivi l'ensemble de sa scolarité à Dijon (école maternelle et élémentaire Montchapet puis collège Marcelle Pardé en section musicale), elle obtient le baccalauréat scientifique en classe internationale allemand au lycée international Charles-de-Gaulle de Dijon. Elle étudie à l'université de Bourgogne en filière STAPS où elle obtient en 1999 une maîtrise en management et droit du sport. 
En 2001, après avoir réussi le concours à l'INSEP, elle devient cadre au Ministère de la Jeunesse et des Sports à la Direction régionale de la jeunesse, des sports et de la cohésion sociale de Lille puis au Creps Dijon-Bourgogne. 
Après sa carrière de sportive, elle reprend ses études, obtient une licence en droit public et réussit le concours du cycle préparatoire au concours d'entrée de l'ENA en 2006. Elle est alors détachée auprès de son école et valide ce cycle préparatoire à l'Institut d'études politiques de Lille et à l'Institut de la gestion publique et du développement économique (IGPDE).


=== Carrière professionnelle ===
De 2010 à 2014, elle est chargée d'enseignement en Master 2 à la Faculté des sports de l'Université de Bourgogne (Droit et fiscalité du sport).
Depuis 2001, elle occupe le statut de cadre au Centre de ressources, d'expertise et de performance sportives de Dijon Bourgogne (Ministère des Sports). Elle occupe des fonctions liées à l'ingénierie de formation des cadres sportifs et notamment des entraîneurs. Elle intervient en particulier dans les domaines juridiques liés au sport et en culture générale sportive : sport et femme, sport et violence, sport et santé, sport et handicap, etc.). Par ailleurs, elle met en place une pré-qualification aux métiers du sport et de l’animation socio-culturelle en lien avec la missions locales pour l'insertion professionnelle et sociale des jeunes de Dijon et les centres d'entraînement aux méthodes d'éducation active (CEMEA). En outre, elle accompagne des candidats à la validation des acquis (VAE) pour les diplômes sportifs.
De septembre 2000 à septembre 2001, en qualité de cadre au sein de la Direction régionale et départementale de la Jeunesse et des Sports de Lille (DRDJS 59) elle participe à l'accompagnement et à la professionnalisation des emplois-jeunes dans les clubs sportifs. Elle participe à ce titre à l'élaboration d'un guide d’aide à la pérennisation des emplois aidés.


=== Engagement politique ===
En 2008, lors des élections municipales, elle est élue sur la liste de François Rebsamen, qui lui attribue une délégation au commerce, à l'artisanat et au projet Cœur de ville. Elle participe à la mise en place de ce projet visant à soutenir et dynamiser l’activité commerciale du centre-ville ainsi qu'à la création d'une fédération des commerçants et artisans. Elle accompagne la mise en place du Tramway de Dijon et la poursuite de la piétonisation du centre-ville. Sa délégation est élargie en 2012 à la démocratie locale : elle participe dans ce cadre à la mise en place des budgets participatifs au sein des commissions de quartier et à la mise en œuvre de consultations citoyennes.
Depuis 2008, elle préside la commission de quartier Centre-ville de Dijon. 
Réélue au scrutin de 2014, elle est nommée première adjointe au nouveau maire de Dijon, Alain Millot. Elle reçoit délégation pour les questions relatives à l'administration générale, à la démocratie locale et au personnel ainsi que la tranquillité publique. Elle est membre de la commission finances, administration générale et personnel et de la commission citoyenneté et démocratie locale. Elle est élue en outre vice-présidente de la communauté urbaine du Grand Dijon.
Le 29 mars 2015, au second tour, elle est élue conseillère départementale du canton de Dijon-2 en tandem avec le maire Alain Millot. 
Elle est membre de la 4e commission  (Actions sociales et intergénérationnelles). 
Du 27 juillet au 10 août 2015, elle assure en qualité de première adjointe, l'intérim des fonctions de maire à la suite du décès d'Alain Millot.
Depuis 2015,  elle préside le groupe des élus socialistes, radicaux de gauche, citoyens et apparentés, du conseil municipal de Dijon.
Elle est membre du conseil national du Parti socialiste depuis le Congrès de Poitiers en 2015.
En décembre 2015, elle est faite chevalier de l'Ordre national du Mérite.
Engagée au service du développement des droits et du rôle des femmes dans la société, qu'elle a longtemps défendus lors de sa carrière sportive notamment pour développer la pratique féminine et valoriser le sport féminin, elle demeure très attachée à ces principes et valeurs.
Le 8 juillet 2017, elle intègre la direction collégiale du PS.


=== Carrière sportive ===
Parallèlement, Nathalie Koenders pratique le canoë-kayak à haut niveau au club de l'ASPTT Dijon,. 

1992 à 2001 : Sélectionnée en équipe de France jeune et senior.
1993 : Participation aux championnats du monde (junior).[réf. nécessaire]
1994 : Championne de France (junior) en K1 500 mètres.[réf. nécessaire]
1998 : Championne de France en K1 5 000 mètres.[réf. nécessaire]
1999 : Participation aux championnats du monde de course en ligne de canoë-kayak de 1999 en K4 200 mètres,.


== Notes et références ==


== Liens externes ==
Site de la ville de Dijon
Site de la communauté urbaine du Grand Dijon Portail du canoë-kayak   Portail de la politique française   Portail de Dijon
/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui.viz;

/**
 * Visualization types currently allows in the web interface.
 * @author Julien Leblay
 */
@SuppressWarnings("javadoc")
public enum VisualizationTypes {
	GRAPH, 
	TREE, 
	STACKED_GRAPH;
}

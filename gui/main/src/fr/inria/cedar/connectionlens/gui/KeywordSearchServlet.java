/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import static java.util.Comparator.comparing;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.InMemoryGraph;
import fr.inria.cedar.connectionlens.gui.util.RequestParameters;
import fr.inria.cedar.connectionlens.gui.util.SessionAttributes;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.search.ConditionReachedException;
import fr.inria.cedar.connectionlens.search.GAMSearch;
import fr.inria.cedar.connectionlens.search.Query;
import fr.inria.cedar.connectionlens.search.QuerySearch;
import fr.inria.cedar.connectionlens.search.QuerySearch.GlobalSearchAlgorithms;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Process keyword search over graph
 *
 * @author Julien Leblay
 */
public class KeywordSearchServlet extends MasterServlet {

	/* Generated */
	private static final long serialVersionUID = 1603934997182444288L;

	/** Class logger. */
	private static final Logger log = LoggerFactory.getLogger(KeywordSearchServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("Cache-Control", "no-cache");
		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// Parse parameters
		final String queryString = request.getParameter(RequestParameters.QUERY);
		final String pageString = request.getParameter(RequestParameters.PAGE);
		log.info("QUERY: " + queryString);
		if (Strings.isNullOrEmpty(queryString) || Strings.isNullOrEmpty(pageString)) {
			handleError(response, SC_BAD_REQUEST, "Keywords must be specified");
			//log.info("EXIT 1"); 
			return response.isCommitted();
		}
		final int page = Integer.parseInt(pageString);
		
		// Parse database name
		final String database = request.getParameter(RequestParameters.DATABASE);
		final String useAbstractString = request.getParameter(RequestParameters.ABSTRACT);
		if (Strings.isNullOrEmpty(database) || Strings.isNullOrEmpty(useAbstractString)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			//log.info("EXIT 2"); 
			return response.isCommitted();
		}
		boolean useAbstract = Boolean.parseBoolean(useAbstractString);

		// Get engine and graph
		ConnectionLens cl;
		try {
			cl = getEngine(database, useAbstract, request, false); 
		} catch (Exception e) {
			//log.info("EXIT 3"); 
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}
		final ScoringFunction sf = cl.resolveScoringFunction("default", Query.parse(queryString));
		Graph graph = cl.graph();
		if (useAbstract) {
			graph = graph.getClassifiedGraph();
		}

		// Execute the search
		List<AnswerTree> results = executeSearch(graph, sf, queryString);
		StringBuffer sResponse = new StringBuffer();
		sResponse.append("["); 
		// Serialize results
		// IM, 11/11/11: trying to build a JSON string without a JSON library
		// JSONArray output = new JSONArray();
		for (int i = page * PAGE_SIZE; i < (page + 1) * PAGE_SIZE && i < results.size(); i++) {
			//JSONObject tree = results.get(i).serialize(sf);
			String thisTreeJSON = results.get(i).toJSONString(sf); 
			int thisTreeJSONLen = thisTreeJSON.length();
			sResponse.append(thisTreeJSON.substring(0, thisTreeJSONLen-1) + 
						", \"rank\": " + (i+1) + "}, "); 
			//tree.put("rank", i + 1);
			//output.put(tree);
		}
		if (sResponse.length() > 2) { // removing the last comma
			sResponse.setLength(sResponse.length() - 2);
		}
		sResponse.append("]");
		log.info(String.valueOf(sResponse));
		try {
			response.setContentType("application/json");
			//	response.getWriter().print(output);
			response.getWriter().print(sResponse); 
		} catch (IOException e) {
			System.out.println(e.getMessage()); 
			log.error(e.getMessage(), e);
		}
		return response.isCommitted();
	}

	/**
	 * Build the search algorithm corresponding to the "global_search_algorithm"
	 * property
	 * 
	 * @param graph
	 * @param f
	 * @return
	 */
	private QuerySearch buildGlobalSearch(Graph graph, ScoringFunction f) {
		GlobalSearchAlgorithms algo = GlobalSearchAlgorithms
				.valueOf(Config.getInstance().getProperty("global_search_algorithm"));
		StatisticsCollector stats = StatisticsCollector.mute();

		switch (algo) {
		case GAM:
			boolean queryStrategy = Config.getInstance().getBooleanProperty("query_only_specific_edge");
			boolean prefetchGraph = Config.getInstance().getBooleanProperty("prefetch_graph_in_memory");
			log.info("Creating a GAMSearch on " + 
					((graph.isAbstract())? "abstract ":"non-abstract ")  + 
					(prefetchGraph?" in-memory":"") + "graph"); 
			if (prefetchGraph && !graph.isAbstract()) {
				// don't prefetch an abstract graph, things may not work fully on them
				return new GAMSearch(graph.index(), new InMemoryGraph(graph), f, stats, queryStrategy);
			}
			return new GAMSearch(graph.index(), graph, f, stats, queryStrategy);
		default:
			throw new IllegalStateException("No such global search algorithm: " + algo);
		}

	}

	/**
	 * Execute the search and save it to cache
	 * 
	 * @param graph
	 * @param sf
	 * @param queryString
	 * @return
	 */
	private List<AnswerTree> executeSearch(Graph graph, ScoringFunction sf, String queryString) {
		// Use cached results if possible
		if (queryString.equalsIgnoreCase((String) session.getAttribute(SessionAttributes.LAST_KWSEARCH))) {
			Object lastQueryResult = session.getAttribute(SessionAttributes.LAST_KWSEARCH_RESULT);
			if (lastQueryResult != null) {
				// System.out.println("executeSearch returning cached results, exit");
				// in this case, the trees are not scored in this method.
				return (List<AnswerTree>) lastQueryResult;
			}
		}
		final Query q = Query.parse(queryString);
		final QuerySearch querySearch = buildGlobalSearch(graph, sf);
		// IM, 21/3/21: results gathered in a sorted list with ad-hoc comparator based on score
		final SortedSet<AnswerTree> sortedResults = 
				new ConcurrentSkipListSet<AnswerTree>(new AnswerTree.Comparator(sf));
				//new ConcurrentSkipListSet<>(
				// comparing(at -> ((AnswerTree) at).score(sf)).reversed().thenComparing(Object::hashCode)); 
				// IM 22/02/2022 The comparator was Object::toString, this caused NaN scores in the GUI (#623)
		        // Object.hashCode seems to solve it
		session.setAttribute(SessionAttributes.QUERY_LATCH, querySearch);
		//System.out.println("executeSearch launches querySearch"); 
		try {
			querySearch.run(q, at -> { //the processor just adds the result to the array
				synchronized (sortedResults) {
					if (sortedResults.contains(at)) {
						return;
					}
					sortedResults.add(at);
					//System.out.println("executeSearch[" + (sortedResults.size() -1) + "] score: " + at.score(sf) + "\n"); 
					if (session.getAttribute(SessionAttributes.QUERY_LATCH) != querySearch) {
						throw new ConditionReachedException("Another query has been issued. Aborting");
					}
				}
			}); 
		} catch (ConditionReachedException e) {
			System.out.println(e.toString()); 
			log.info("Stop condition '" + e.getMessage() + "' reached for " + q);
		}
		session.removeAttribute(SessionAttributes.QUERY_LATCH);
		
		ArrayList<AnswerTree> results = new ArrayList<>(sortedResults);
		System.out.println("Obtained " + results.size() + " query results"); 
		// Cache the results
		session.setAttribute(SessionAttributes.LAST_KWSEARCH, queryString);
		session.setAttribute(SessionAttributes.LAST_KWSEARCH_RESULT, results);

		return results;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}

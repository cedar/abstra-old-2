/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import com.google.common.base.Strings;
import com.google.common.io.Files;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.gui.util.RequestParameters;
import fr.inria.cedar.connectionlens.gui.util.SessionAttributes;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static fr.inria.cedar.connectionlens.gui.util.RequestParameters.FILE;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

/**
 * Import files into the graph
 * 
 * @author Jérémie Feitz
 */
public class AbstraHomeServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	private static final Logger log = LoggerFactory.getLogger(AbstraHomeServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(this.getClass().getName() + " doGet method called with path " + request.getRequestURI() + " and parameters " + request.getQueryString());
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import edu.stanford.nlp.util.Sets;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.gui.util.RequestParameters;

/**
 * This servlet is called every time nodes are added to the main visualization.
 * From the list of the nodes to be added and the list of the nodes already in
 * the graph, it find the edges that should be added, and provides a more
 * thorough serialization of the new nodes than GetNeighborsServlet and
 * KeywordSearchServlet.
 *
 * @author Jérémie Feitz
 */
public class GetLinksBetweenSetsServlet extends MasterServlet {

	/* Generated */
	private static final long serialVersionUID = 1603934997182444288L;

	/** Class logger. */
	private static final Logger log = LoggerFactory.getLogger(GetLinksBetweenSetsServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("Cache-Control", "no-cache");
		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// Parse parameters
		final String oldNodesString = request.getParameter(RequestParameters.OLD_NODES);
		final String newNodesString = request.getParameter(RequestParameters.NEW_NODES);
		log.info("LINKS BETWEEN SETS: " + oldNodesString + " and " + newNodesString);
		if (Strings.isNullOrEmpty(oldNodesString) || Strings.isNullOrEmpty(newNodesString)) {
			handleError(response, SC_BAD_REQUEST, "Old nodes and new nodes must be specified");
			return response.isCommitted();
		}

		// Parse database name
		final String database = request.getParameter(RequestParameters.DATABASE);
		final String useAbstractString = request.getParameter(RequestParameters.ABSTRACT);
		if (Strings.isNullOrEmpty(database) || Strings.isNullOrEmpty(useAbstractString)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			return response.isCommitted();
		}
		boolean useAbstract = Boolean.parseBoolean(useAbstractString);

		// Get engine and graph
		ConnectionLens cl;
		try {
			cl = getEngine(database, useAbstract, request, false); 
		} catch (Exception e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}
		Graph graph = cl.graph();
		if (useAbstract)
			graph = graph.getClassifiedGraph();

		// Resolve old nodes and new nodes from their IDs
		JSONArray oldNodesArray = new JSONArray(oldNodesString);
		Set<Node> oldNodes = new HashSet<Node>();
		for (int i = 0; i < oldNodesArray.length(); i++) {
			String nodeId = oldNodesArray.getString(i);
			try {
				Node node = graph.resolveNode(graph.getIDFactory().parseNodeID(new Integer(nodeId))); 
				oldNodes.add(node);
			} catch (IllegalStateException e) {
				handleError(response, SC_BAD_REQUEST, "There is no such node: " + nodeId);
				return response.isCommitted();
			}
		}
		JSONArray newNodesArray = new JSONArray(newNodesString);
		Set<Node> newNodes = new HashSet<Node>();
		for (int i = 0; i < newNodesArray.length(); i++) {
			String nodeId = newNodesArray.getString(i);
			try {
				Node node = graph.resolveNode(graph.getIDFactory().parseNodeID(new Integer(nodeId)));
				newNodes.add(node);
			} catch (IllegalStateException e) {
				handleError(response, SC_BAD_REQUEST, "There is no such node: " + nodeId);
				return response.isCommitted();
			}
		}

		// Add the substructure of top-level records
		if (useAbstract)
			newNodes = findSubstructures(graph, newNodes);

		// Find the relevant edges
		log.info(graph.getEdgesBetweenSets(oldNodes, newNodes).toString());
		log.info(graph.getSameAsBetweenSets(oldNodes, newNodes, Config.getInstance().getDoubleProperty("same_as_threshold", .8)).toString());
		//log.info("At " + System.currentTimeMillis() + " calling getEdges on " + oldNodes.size() +
		//		" old nodes and " + newNodes.size() + " new nodes");
		Set<Edge> links =  Sets.union(graph.getEdgesBetweenSets(oldNodes, newNodes), 
				graph.getSameAsBetweenSets(oldNodes,
				newNodes, Config.getInstance().getDoubleProperty("same_as_threshold", .8)));
		log.info("SERIALIZING " + links.size() + " LINKS");
		
		// Serialize edges
		JSONArray jsonEdges = new JSONArray();
		log.info("** links ** ");
		if(links != null) {
			log.info(String.valueOf(links));
		}
		for (Edge edge : links) {
			log.info("dealing with edge " + edge);
			JSONObject jsonEdge = edge.serialize();

			jsonEdge.put("global_id", edge.getId().value().toString());
			jsonEdge.put("same_as", edge.isSameAs());
			jsonEdge.put("score", edge.getSortingScore());

			NodeID sourceID = edge.getSourceNode().getId();
			if (sourceID == null) {
				throw new IllegalStateException("Null source ID"); 
			}
			NodeID targetID = edge.getTargetNode().getId();
			if (targetID == null) {
				throw new IllegalStateException("Null target ID"); 
			}
			jsonEdge.put("source", sourceID.value().toString());
			jsonEdge.put("target", targetID.value().toString());
			log.info("  " + jsonEdge);
			jsonEdges.put(jsonEdge);
		}
		log.info("jsonEdges = " + String.valueOf(jsonEdges));
		
		// Serialize nodes
		JSONArray jsonNodes = new JSONArray();
		for (Node node : newNodes) {
			JSONObject jsonNode = node.serialize();
			jsonNode.put("global_id", node.getId().value().toString());
			jsonNode.put("context", cl.graph().getContext(node));

			jsonNodes.put(jsonNode);
		}
		log.info("jsonNodes = " + String.valueOf(jsonNodes));

		JSONObject output = new JSONObject();
		output.put("links", jsonEdges);
		output.put("nodes", jsonNodes);

		log.info("output = " + output);
		
		response.setContentType("application/json");
		response.getWriter().print(output);

		return response.isCommitted();
	}

	/**
	 * Find the substructures of the top-level records
	 * 
	 * @param graph
	 * @param nodes
	 * @return
	 */
	private Set<Node> findSubstructures(Graph graph, Set<Node> nodes) {
		Set<Node> allNodes = new HashSet<Node>(nodes);
//		for (Node node : nodes) {
//			log.info(node.toString());
//			if (Node.getRecordTypesAsList().contains(node.getNodeType()) || node.getNodeType() == Types.COLLECTION)
//				addSubstructure(graph, node, allNodes);
//		}
		return allNodes;
	}

	/**
	 * Add the substructure of a node to the set allNodes
	 * 
	 * @param graph
	 * @param node
	 * @param allNodes
	 */
	private void addSubstructure(Graph graph, Node node, Set<Node> allNodes) {
		Set<Edge> adjacentEdges = graph.getOutgoingEdges(node);
		for (Edge edge : adjacentEdges) {
			NodeID sourceID = edge.getSourceNode().getId();
			Node neighborNode;
			if (sourceID.compareTo(node.getId()) == 0)
				neighborNode = edge.getTargetNode();
			else
				neighborNode = edge.getSourceNode();

			if (allNodes.contains(neighborNode))
				continue; // Avoid getting stuck in loops
			allNodes.add(neighborNode);
//			if (neighborNode.getNodeType() != Node.Types.COLLECTION)
//				addSubstructure(graph, neighborNode, allNodes);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}

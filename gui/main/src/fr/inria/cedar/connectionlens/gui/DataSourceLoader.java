/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import static fr.inria.cedar.connectionlens.gui.util.ServletParameters.SYSTEM;
import fr.inria.cedar.connectionlens.Config;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DataSourceLoader implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Config.getInstance(sce.getServletContext().getRealPath(".")  + "/WEB-INF/local.settings");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		sce.getServletContext().setAttribute(SYSTEM, null);
	}
}

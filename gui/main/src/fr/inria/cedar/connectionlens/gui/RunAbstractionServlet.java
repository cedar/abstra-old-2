/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

import static java.util.Locale.ENGLISH;
import java.util.HashMap;

/**
 * Import files into the graph
 * 
 * @author Jérémie Feitz
 */
public class RunAbstractionServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	private static final Logger log = LoggerFactory.getLogger(RunAbstractionServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("doPost called with parameter " + request.getQueryString());

//		request.setAttribute("filePath", request.getParameter("filePath"));

		// 1. we register the file (this hould also create the Config)
		String filePath = request.getParameter("filePath");
		System.out.println(filePath);

		EntityExtractor extractor = new StanfordNERExtractor(ENGLISH);
		MorphoSyntacticAnalyser analyzer = new TreeTagger(ENGLISH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(filePath);
		System.out.println("Registered file " + filePath);

		// 2. we get the form parameters
		String eMax = request.getParameter("emax");
		System.out.println(eMax);
		String covMin = request.getParameter("covmin");
		System.out.println(covMin);
		String scoring = request.getParameter("scoring");
		System.out.println(scoring);
		String k = request.getParameter("k");
		System.out.println(k);
		String boundary = request.getParameter("boundary");
		System.out.println(boundary);
		String update = request.getParameter("update");
		System.out.println(update);
		String simMin = request.getParameter("simmin");
		System.out.println(simMin);
		String scoreCatMin = request.getParameter("scorecatmin");
		System.out.println(scoreCatMin);

		// 3. we run the abstraction
		try {
			cl.runAbstraction(eMax, covMin, simMin, scoreCatMin, k, scoring, boundary);
		} catch (Exception e) {
			e.printStackTrace();
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}
		System.out.println("Ran abstraction of " + filePath);

		// 4. update the createAbstraction.jsp page with the description
		JSONObject jsonObject = new JSONObject();
		HashMap<String, String> responseJson =  new HashMap<>();
		System.out.println(cl);
		System.out.println(cl.getReportingToUser());
		System.out.println(cl.getReportingToUser().getHtmlEntities());
		System.out.println(cl.getReportingToUser().getHtmlRelationships());
		String entitiesHtml = cl.getReportingToUser().getHtmlEntities();
		entitiesHtml = entitiesHtml.replaceAll(cl.getReportingToUser().getPathToReportingFolder(), "/gui/img");
		responseJson.put("entitiesText", entitiesHtml);
		responseJson.put("relationshipText", cl.getReportingToUser().getHtmlRelationships());
		String pathToERdrawing = cl.getReportingToUser().getFilenamePngDrawingTomcat();
		String[] split = pathToERdrawing.split("/");
		String nameOfTheWebApp = split[split.length-3]; // -3 to get the name of the web app
		String imgFolder = split[split.length-2]; // -2 to get the folder 'img'
		String filename = split[split.length-1]; // -1 to get the filename
		responseJson.put("filenamePngDrawing", Paths.get("/" + nameOfTheWebApp, imgFolder, filename).toString());
		jsonObject.putAll(responseJson);
		System.out.println(jsonObject.toJSONString());
		response.setContentType("text/plain");
		response.getWriter().write(jsonObject.toJSONString());

//		request.setAttribute("entitiesText", cl.getReportingToUser().getHtmlEntities());
//		request.setAttribute("relationshipText", cl.getReportingToUser().getHtmlRelationships());
//		request.setAttribute("filenamePngDrawing", cl.getReportingToUser().getPngDrawing());
//		request.getRequestDispatcher("createAbstraction.jsp").forward(request, response);
	}
}

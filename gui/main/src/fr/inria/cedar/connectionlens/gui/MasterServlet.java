/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import static fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor.buildPairFinder;
import static fr.inria.cedar.connectionlens.gui.util.SessionAttributes.MESSAGE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;
import static java.util.Locale.FRENCH;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.ParameterException;
import com.google.common.base.Strings;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.Experiment.Extractors;
import fr.inria.cedar.connectionlens.extraction.BatchCacheExtractor;
import fr.inria.cedar.connectionlens.extraction.BatchExtractor;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.FlairNERExtractor;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.gui.util.SessionAttributes;
import fr.inria.cedar.connectionlens.util.PythonUtils;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Factorizes processings common to all requests.
 * 
 * @author Julien Leblay
 */
public abstract class MasterServlet extends HttpServlet {

	/* Generated */
	private static final long serialVersionUID = 8188116188728079286L;

	/** Class logger. */
	private static final Logger log = LoggerFactory.getLogger(MasterServlet.class);

	/** The session. */
	protected HttpSession session;

	/** Page size for paginating API results */
	protected static final int PAGE_SIZE = 30;

	private StatisticsCollector extractStats;

	/**
	 * Factorizes processings common to all requests.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return true, if successful
	 * @throws ServletException
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		session = request.getSession();
		session.removeAttribute(MESSAGE);
		return true;
	}

	/**
	 * Create a ConnectionLens object for a database, and store the engine in a
	 * request's session
	 * 
	 * @param database
	 * @param useAbstract
	 * @param request
	 * @return
	 */
	protected ConnectionLens getEngine(String database, boolean useAbstract, HttpServletRequest request,
			boolean forImport) {
		// log.info("MasterServlet get engine for " + database + ", abstract=" +
		// useAbstract);
		// If the last action was an import and the current action is not, we want to
		// reload the engine
		Object hasImportedMaybe = session.getAttribute(SessionAttributes.HAS_IMPORTED);
		boolean hasImported = (hasImportedMaybe == null) ? false : (boolean) hasImportedMaybe;

		// Use cached engine if possible
		if (forImport || !hasImported) {
			if (database.equalsIgnoreCase((String) session.getAttribute(SessionAttributes.DATABASE))) {
				Object cacheUsesAbstract = session.getAttribute(SessionAttributes.ABSTRACT);
				if (cacheUsesAbstract != null) {
					// log.info("MasterServlet checkpoint 1.1");
					boolean b = (boolean) cacheUsesAbstract; // IM, 21/08/20 this sometimes causes ClassCastException:
					// Cannot convert from Stream to Boolean. TODO try to figure out when is the
					// parameter saved wrongly
					// log.info("MasterServlet checkpoint 1.2");
					if (b == useAbstract) {
						Object cl = session.getAttribute(SessionAttributes.ENGINE);
						if (cl != null)
							return (ConnectionLens) cl;
					}
				}
			}
		}
		database = database.toLowerCase();
		// As per https://www.postgresql.org/docs/9.6/sql-syntax-lexical.html
		// the database name can comprise also digits, and underscores.
		// Further, it should not have more than 63 characters, but we can live with
		// this for now.
		if (!database.matches("[a-z][a-z0-9_]*")) {
			throw new IllegalStateException("Not a valid database name");
		}

		// Invalidate the session to remove all cached query results
		session.invalidate();
		session = request.getSession();

		if (!forImport)
			session.setAttribute(SessionAttributes.HAS_IMPORTED, false);

		Config config = Config.getInstance();
		config.setProperty("RDBMS_DBName", CONNECTION_LENS_DB_PREFIX + database);

		// Create the engine
		final SimilarPairProcessor pairFinder = buildPairFinder(config, StatisticsCollector.mute());
		
		// reusing functionality from Experiment to ensure consistency
		EntityExtractor	extractor = Experiment.buildExtractor(extractStats);
		final EntityExtractor wrappedExtractor = Experiment.possiblyWrapExtractor(extractor, extractStats);
		
		final TreeTagger tagger = new TreeTagger(requireNonNull(Locale.forLanguageTag(config.getProperty("default_locale"))));
		ConnectionLens cl = new ConnectionLens(wrappedExtractor, tagger, pairFinder, false);
		// Cache the results
		session.setAttribute(SessionAttributes.DATABASE, database);
		session.setAttribute(SessionAttributes.ABSTRACT, useAbstract);
		session.setAttribute(SessionAttributes.ENGINE, cl);

		return cl;
	}

	/**
	 * Handles exceptions, logging messages on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param t        the t
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleError(HttpServletResponse response, int status, Throwable t) throws IOException {
		// t.printStackTrace();
		String msg = t == null ? "" : Strings.nullToEmpty(t.getMessage());
		log.error(msg);
		session.setAttribute(MESSAGE, msg);
		response.sendError(status, msg);
	}

	/**
	 * Handles error messages, logging them on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleError(HttpServletResponse response, int status, String message) throws IOException {
		log.error(message);
		session.setAttribute(MESSAGE, message);
		response.sendError(status, message);
	}

	/**
	 * Handles warning messages, logging them on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleWarning(HttpServletResponse response, int status, String message) throws IOException {
		log.warn(message);
		session.setAttribute(MESSAGE, message);
		response.sendError(status, message);
	}

	/**
	 * @param array the array
	 * @return an array in which all of the strings in the input are URL encoded
	 * @throws UnsupportedEncodingException
	 */
	protected String[] urlDecode(String[] array) throws UnsupportedEncodingException {
		if (array == null) {
			return null;
		}
		String[] result = new String[array.length];
		for (int i = 0, l = array.length; i < l; i++) {
			result[i] = URLDecoder.decode(array[i], "UTF-8");
		}
		return result;
	}

}

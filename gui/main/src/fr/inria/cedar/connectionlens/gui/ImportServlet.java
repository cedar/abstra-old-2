/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import static fr.inria.cedar.connectionlens.gui.util.RequestParameters.FILE;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.io.Files;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.gui.util.RequestParameters;
import fr.inria.cedar.connectionlens.gui.util.SessionAttributes;
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * Import files into the graph
 * 
 * @author Jérémie Feitz
 */
public class ImportServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	private static final Logger log = LoggerFactory.getLogger(ImportServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		Map<String, Object> params;

		try {
			params = parseParameters(request);
		} catch (FileUploadException e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		// Parse parameters
		FileItem item = (FileItem) params.get(FILE);
		String original_url = (String) params.get(RequestParameters.URL);
		String database = (String) params.get(RequestParameters.DATABASE);
		database = database.toLowerCase();
		
		if (Strings.isNullOrEmpty(database)) { // IM, 17/08/20 || Strings.isNullOrEmpty(useAbstractString)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			return response.isCommitted();
		}	
		Config  config = Config.getInstance();
		boolean useAbstract = config.getBooleanProperty("create_abstraction");
		// Check parameters
		if (item.isFormField()) {
			handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Not a valid file");
			return response.isCommitted();
		}

		// Parse global URI
		URI originalURI = null;
		if (original_url.length() > 0) {
			try {
				originalURI = URI.create(original_url);
			} catch (Exception e) {
				handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
				e.printStackTrace();
				return response.isCommitted();
			}
		}

		// Copy file locally
		URI localURI;
		try {
			localURI = saveLocally(item);
		} catch (IOException e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		// Get the engine
		ConnectionLens cl;
		try {
			cl = getEngine(database, useAbstract, request, true);
			session.setAttribute(SessionAttributes.HAS_IMPORTED, true);
		} catch (Exception e) {
			handleError (response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted ();
		}

		// Register
		List<DataSource> dataSources = new LinkedList<DataSource>();
		try {
			if (localURI.toString().toLowerCase().endsWith(".pdf")) {
				System.setProperty("user.dir", "");
				cl.registerPDF(localURI.toString(), originalURI).forEach(dataSources::add);
			}
			else {
				cl.register (localURI, originalURI);
			}
			// NB May 10th 2021: added the specificity computation after the registration of the inputs
			cl.graph().countEdgesForSpecificity();
			cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());

			// IM 13/8/20, bougé ce bloc ici: 
			if (useAbstract) {
				// NB Dec 6th 2021: should use the abstraction coded in the branch abstraction
				// RandCConverter rc = new RandCConverter(cl);
				// rc.createAbstraction();
				// NB May 10th 2021: added the specificity computation after the abstraction
				// cl.graph().getAbstractGraph().countEdgesForSpecificity();
				// cl.graph().getAbstractGraph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());

			}
		} catch (Exception e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		return response.isCommitted();
	}

	private URI saveLocally(FileItem item) throws IOException {
		final String defaultImportDirectory = session.getServletContext().getRealPath(".") + "/WEB-INF/uploads";
		File directory = new File(defaultImportDirectory);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		if (!directory.isDirectory()) {
			throw new IOException("Import save location should be a directory: " + directory);
		}
		String result = Paths.get(directory.getAbsolutePath(), item.getName()).toString();
		File localFile = new File(result);
		Files.write(item.get(), localFile);
		log.info("Save import locally: " + Paths.get(directory.getAbsolutePath(), item.getName()).toString());
		return localFile.toURI();
	}

	/**
	 * @param request the request
	 * @return the map from the parsed parameters' names to their values
	 * @throws FileUploadException
	 */
	private static Map<String, Object> parseParameters(HttpServletRequest request) throws FileUploadException {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(100000000);// in bytes
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(100000000);
		List<FileItem> items = upload.parseRequest(request);
		Map<String, Object> result = new LinkedHashMap<>();
		for (FileItem item : items) {
			String name = item.getFieldName();
			if (item.isFormField()) {
				result.put(name, (result.containsKey(name) ? result.get(name) + "," : "") + item.getString());
			} else {
				result.put(name, item);
			}
		}
		return result;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}

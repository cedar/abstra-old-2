/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.gui;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;
import static java.util.Locale.ENGLISH;


public class DisplayGraphServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	private static final Logger log = LoggerFactory.getLogger(DisplayGraphServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("doPost called with parameter " + request.getQueryString());

		String databaseName = request.getParameter("databaseName");
		System.out.println(request.getParameter("readAbstract"));
		boolean readAbstract = false;
		// checkbox parameters are not sent if they are not checked. Therefore, if it is sent, the input is checked.
		if(request.getParameterMap().containsKey("readAbstract") && request.getParameter("readAbstract").equals("abstract")) {
			readAbstract = true;
		}
		System.out.println(readAbstract);

		Config.getInstance().setProperty("RDBMS_DBName", databaseName);

		EntityExtractor extractor = new StanfordNERExtractor(ENGLISH);
		MorphoSyntacticAnalyser analyzer = new TreeTagger(ENGLISH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, false);
		RelationalGraph g = (RelationalGraph) cl.graph();

		ArrayList<HashMap<String, String>> nodes = new ArrayList<>(); // list of nodes - for each node we have its id, label and type
		ArrayList<HashMap<String, String>> edges = new ArrayList<>(); // list of edges - for each edge, we have its source, target and label
		ArrayList<HashMap<String, String>> nodeTypes = new ArrayList<>(); // list of types - for each type, we have its id and label

		try {
			String nodesTable = (readAbstract ? SchemaTableNames.CLASSIFIED_NODES_TABLE_NAME : SchemaTableNames.ORIGINAL_NODES_TABLE_NAME);
			String edgesTable = (readAbstract ? SchemaTableNames.CLASSIFIED_EDGES_TABLE_NAME : SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME);
			PreparedStatement stmt = g.getPreparedStatement("SELECT id, label, type FROM " + nodesTable + ";");
			System.out.println(stmt.toString());
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				HashMap<String, String> node = new HashMap<>();
				node.put("id", Integer.toString(rs.getInt(1)));
				node.put("label", rs.getString(2));
				node.put("type", Integer.toString(rs.getInt(3)));
				nodes.add(node);
			}

			stmt = g.getPreparedStatement("SELECT source, target, label FROM " + edgesTable + ";");
			System.out.println(stmt.toString());
			rs = stmt.executeQuery();

			while(rs.next()) {
				HashMap<String, String> edge = new HashMap<>();
				edge.put("source", Integer.toString(rs.getInt(1)));
				edge.put("target", Integer.toString(rs.getInt(2)));
				edge.put("label", rs.getString(3));
				edges.add(edge);
			}

			// get all nodes types (that appear in the dataset)
			stmt = g.getPreparedStatement("SELECT id, label FROM " + SchemaTableNames.TYPES_TABLE_NAME + " WHERE id IN (SELECT type FROM " + nodesTable + ");");
			rs = stmt.executeQuery();

			while(rs.next()) {
				HashMap<String, String> type = new HashMap<>();
				type.put("id", Integer.toString(rs.getInt(1)));
				type.put("label", rs.getString(2));
				nodeTypes.add(type);
			}

			System.out.println(nodes.toString());
			System.out.println(edges.toString());

			HashMap<String,ArrayList<HashMap<String, String>>> jsonData = new HashMap<>();
			jsonData.put("nodes", nodes);
			jsonData.put("links", edges);
			jsonData.put("types", nodeTypes);
			// convert it as a JSON object
			JSONObject jsonObject = new JSONObject();
			jsonObject.putAll( jsonData );
			System.out.println(jsonObject.toJSONString());

			// update the createAbstraction.jsp page with the description
			request.setAttribute("jsonData", jsonObject.toJSONString());
			request.getRequestDispatcher("displayGraph.jsp").forward(request, response);
		} catch(Exception e) {
			throw new IllegalStateException();
		}
	}
}

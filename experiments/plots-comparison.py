import matplotlib.pyplot as plt
import numpy as np


def draw_chart(scores, format):
    # create th chart
    # plot the data
    fig, ax = plt.subplots()
    font_size = 12
    width = 0.2
    labels = ["desc-1/desc", "desc-2/desc", "desc-3/desc", "leaf-1/leaf", "leaf-2/leaf", "leaf-3/leaf", "wDAG/DAG", "wDAG/fl", "wDAG/fl_ac", "wPR/DAG", "wPR/fl", "wPR/fl_ac"]
    F1_scores_entities = []
    F1_scores_relationships = []
    F1_scores_boundaries = []
    for i in range(0, len(scores)):
        key=str(i+1)
        F1_scores_entities.append(scores[key][0])
        F1_scores_boundaries.append(scores[key][1])
        F1_scores_relationships.append(scores[key][2])
    x = np.arange(len(labels))
    plt.bar(x-0.2, F1_scores_entities, width)
    plt.bar(x, F1_scores_boundaries, width)
    plt.bar(x+0.2, F1_scores_relationships, width)
    # configure the chart
    font = {'family': 'normal', 'size': font_size}
    plt.rc('font', **font)
    plt.ylim(ymin=0, ymax=1)
    plt.xticks(x, labels, fontsize=font_size, rotation=45, ha="right")
    plt.yticks(fontsize=font_size)
    plt.ylabel("F1-score", fontsize=font_size)
    plt.legend(["Entities", "Boundaries", "Relationships"], loc='upper right', bbox_to_anchor=(1,0.95))
    ax.text(.5, .95, "Comparisons with the reference (" + format + ")", horizontalalignment='center', transform=ax.transAxes, fontsize=font_size)
    
    # plt.show()
    plt.tight_layout()
    plt.savefig("comparison_with_reference_" + format+".png")


if __name__ == "__main__":
    # create data
    # arrays are of the form [F1-score entities, F1-score relationships, F1-score boundaries]
    scores_XML = {
        "1": [0.8, 0.5982905982905983, 0.3846153846153847],
        "2": [0.4, 0.35555555555555557, 0.0],
        "3": [0.4, 0.3111111111111111, 0.0],
        "4": [0.0, 0.0, 0.0],
        "5": [0.4, 0.38095238095238093, 0.0],
        "6": [0.4, 0.3905325443786982, 0.0],
        "7": [0.8, 0.78125, 0.3125],
        "8": [0.8, 0.8275862068965516, 0.3125],
        "9": [0.8, 0.78125, 0.3125],
        "10": [1.0, 0.6296296296296295, 0.5],
        "11": [1.0, 0.9090909090909091, 0.5],
        "12": [0.8, 0.7819548872180452, 0.35714285714285715]
    }

    scores_RDF = {
        "1": [0.7499999999999999, 0.9189189189189189, 0.3333333333333333],
        "2": [0.5, 0.6046511627906977, 0.125],
        "3": [0.5714285714285714, 0.5652173913043479, 0.16666666666666666],
        "4": [0.0, 0.0, 0.0],
        "5": [0.5, 0.7027027027027027, 0.125],
        "6": [0.5, 0.456140350877193, 0.0],
        "7": [1.0, 1.0, 0.5],
        "8": [0.5, 0.43243243243243246, 0.0],
        "9": [1.0, 1.0, 0.5],
        "10": [0.5, 0.43243243243243246, 0.0],
        "11": [0.5, 0.43243243243243246, 0.0],
        "12": [1.0, 1.0, 0.5]
    }

    scores_JSON = {
        "1": [0.5, 0.8275862068965516, 0.2857142857142857],
        "2": [0.4, 0.6896551724137931, 0.25],
        "3": [0.4, 0.6896551724137931, 0.25],
        "4": [0.0, 0.0, 0.0],
        "5": [0.4, 0.6896551724137931, 0.25],
        "6": [0.4, 0.6896551724137931, 0.25],
        "7": [0.5, 0.7142857142857143, 0.3333333333333333],
        "8": [0.5, 0.6896551724137931, 0.3333333333333333],
        "9": [0.5, 0.7142857142857143, 0.3333333333333333],
        "10": [0.5, 0.6896551724137931, 0.3333333333333333],
        "11": [0.5, 0.6896551724137931, 0.3333333333333333],
        "12": [0.5, 0.7142857142857143, 0.3333333333333333]
    }
    
    # plot data in grouped manner of bar type
    draw_chart(scores_RDF, "RDF")
    draw_chart(scores_XML, "XML")
    draw_chart(scores_JSON, "JSON")

 
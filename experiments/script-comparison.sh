DATE=2022-10-04
COMMIT=39a035889b6c0ca4d2cbbcca7df6520e644fb71b
CL_JAR=core/target/connection-lens-core-full-1.1-SNAPSHOT-*.jar
FOLDER_DATA=../data-CL
SETTINGS=core/src/main/resources/local.settings
idref=SCORE



# RDF

#sm=DESC_1
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=DESC_2
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=DESC_3
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_1
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_2
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_3
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=DAG
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=FL
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=FL_AC
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=DAG
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=FL
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=FL_AC
#um=EXACT
#echo "comparing abstraction of evaluation-rdf.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_rdf_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/rdf/evaluation-rdf.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-rdf-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;


# JSON

sm=DESC_1
bm=DESC
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=DESC_2
bm=DESC
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=DESC_3
bm=DESC
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=LEAF_1
bm=LEAF
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=LEAF_2
bm=LEAF
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=LEAF_3
bm=LEAF
um=BOOLEAN
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wDAG
bm=DAG
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wDAG
bm=FL
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wDAG
bm=FL_AC
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wPR
bm=DAG
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wPR
bm=FL
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

sm=wPR
bm=FL_AC
um=EXACT
echo "comparing abstraction of evaluation-researchers.nt - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_json_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/json/evaluation-researchers.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-researchers-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;



# XML

#sm=DESC_1
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=DESC_2
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=DESC_3
#bm=DESC
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_1
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_2
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=LEAF_3
#bm=LEAF
#um=BOOLEAN
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=DAG
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=FL
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wDAG
#bm=FL_AC
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=DAG
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=FL
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#sm=wPR
#bm=FL_AC
#um=EXACT
#echo "comparing abstraction of evaluation-Xmark.xml - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_eval_xmark_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xml/evaluation-Xmark.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ${FOLDER_DATA}/logs/${DATE}/evaluation-Xmark-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

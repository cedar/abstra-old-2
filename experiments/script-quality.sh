DATE=2022-10-01
COMMIT=6638638ff601bec137c1865fdd8638e9b31b1790
CL_JAR=core/target/connection-lens-core-full-1.1-SNAPSHOT-*.jar
FOLDER_DATA=/data/datasets/abstraction_data
SETTINGS=core/src/main/resources/local.settings
idref=SCORE

sm=PR
bm=SFLOOD
um=GRAPH

echo "doing abstraction of core dataset - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}"
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_core_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/coreresearch.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/core-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of github events - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_github_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/github.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/github-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of NYtimes - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_nytimes_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/nytimes.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/nytimes-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of JSON prescriptions - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_prescriptions_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/prescriptions.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/prescriptions-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of JSON 40k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers40k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark05.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark05-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of yelp businesses - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_yelpbusiness_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/yelpbusiness.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/yelpbusiness-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of yelp checkin - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_yelpcheckin_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/yelpcheckin.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/yelpcheckin-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of yelp tips - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_yelptip_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/yelptip.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/yelptip-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm4m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm4m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm4m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm4m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm16m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm16m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm16m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm16m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of conferences - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_conferences_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/conferences.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm16m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of enelshops - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_enelshops_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/enelshops.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/enelshops-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of foodista - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_foodista_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/foodista.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/foodista-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of lubm1m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_lubm1m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/lubm1m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/lubm1m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of nasa - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_nasa_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/nasa.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/nasa-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of pubmed data - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_pubmed_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/pubmed.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/pubmed-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark1 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark1_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark1.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark4 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark4_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark4.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark4-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "finished all abstractions";

################

#echo "doing abstraction of watdiv4 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_watdiv4_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/watdiv4.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/watdiv4-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;


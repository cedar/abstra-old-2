import matplotlib.pyplot as plt
import numpy as np

def plot_chart(nb_nodes, time_normalization, time_summarization, time_collectiongraph, time_main_collections, time_classification, dataset_name, nb_nodes_min, nb_nodes_max):
    font_size = 17
    fig, ax = plt.subplots()
    scale = 1000
    ax.plot(nb_nodes, np.divide(np.array(time_normalization), scale), label="normalization", linestyle="-", marker='o')
    ax.plot(nb_nodes, np.divide(np.array(time_summarization), scale), label="summarization", linestyle="--", marker='o')
    ax.plot(nb_nodes, np.divide(np.array(time_collectiongraph), scale), label="collection graph building", linestyle="-.", marker='o')
    ax.plot(nb_nodes, np.divide(np.array(time_main_collections), scale), label="main collections selection", linestyle="-", marker='o')
    ax.plot(nb_nodes, np.divide(np.array(time_classification), scale), label="classification", linestyle="--", marker='o')
    # ax.plot(nb_nodes, np.divide(np.array(time_reporting), scale), label="reporting", linestyle=":", marker='o')
    plt.xlabel("Number of nodes \n (min=" + nb_nodes_min + ", max=" + nb_nodes_max + ")", fontsize=font_size)
    plt.ylabel("Time (in seconds)", fontsize=font_size)
    plt.xscale("log")
    plt.yscale("log")
    plt.legend(loc="lower right", fontsize=0.7*font_size)
    ax.text(.5, .93, "Abstraction time for " + str(dataset_name) + " datasets", horizontalalignment='center', transform=ax.transAxes, fontsize=font_size)  # set the title in the drawing
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    # plt.show()
    plt.tight_layout(rect=(0, 0, 1, 1))
    plt.savefig("chart_" + dataset_name+".png")


if __name__ == "__main__":
    # scalability expes JSON
    nb_nodes = [279757, 560347, 1122325, 2240991, 4505659]
    time_normalization = [3255, 6358, 14481, 28834, 58585]
    time_summarization = [1479, 2636, 5266, 13372, 28611]
    time_collectiongraph = [5251, 12652, 24577, 53458, 113654]
    time_main_collections = [2752, 4573, 9257, 31849, 62210]
    time_classification = [30397, 27611, 39275, 55357, 109729]

    plot_chart(nb_nodes, time_normalization, time_summarization, time_collectiongraph, time_main_collections, time_classification, "Researchers", "279,757", "4,505,659")

    # scalability expes XMark
    nb_nodes = [848990, 1695792, 3392394, 6795211, 13615553]
    time_normalization = [11315, 23649, 50515, 98835, 199801]
    time_summarization = [4570, 9996, 20989, 47665, 105973]
    time_collectiongraph = [18983, 40194, 102378, 224661, 582949]
    time_main_collections = [179303, 411443, 882209, 2630799, 5180670]
    time_classification = [67434, 81037, 115146, 176284, 343451]
    plot_chart(nb_nodes, time_normalization, time_summarization, time_collectiongraph, time_main_collections, time_classification, "XMark", "848,990", "13,615,553")

    # scalability expes BSBM
    nb_nodes = [1199819, 2397584, 4755392, 9461721, 19870744]
    time_normalization = [99830, 205741, 425066, 864107, 1814684]
    time_summarization = [171031, 291991, 691768, 1230672, 3024804]
    time_collectiongraph = [40225, 82713, 196755, 371570, 1489725]
    time_main_collections = [509502, 1095655, 3683561, 5983660, 16303039]
    time_classification = [56337, 85983, 155023, 462491, 836014]
    plot_chart(nb_nodes, time_normalization, time_summarization, time_collectiongraph, time_main_collections, time_classification, "BSBM", "1,199,819", "19,870,744")

    
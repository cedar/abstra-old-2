DATE=2022-10-01
COMMIT=75a51fc44b5874929a76af8c7cab69e050025e95
CL_JAR=core/target/connection-lens-core-full-1.1-SNAPSHOT-*.jar
FOLDER_DATA=/data/datasets/abstraction_data
SETTINGS=core/src/main/resources/local.settings
idref=SCORE

sm=wPR
bm=FL
um=EXACT

# JSON

#echo "doing abstraction of researchers10k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers10k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/researchers10k.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers10k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of researchers20k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers20k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/researchers20k.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers20k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of researchers40k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers40k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/researchers40k.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers40k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of researchers80k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers80k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/researchers80k.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers80k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of researchers160k - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers160k_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/researchers160k.json -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers160k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of researchers10k - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers10k_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers10k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of researchers20k - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers20k_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers20k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of researchers40k - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers40k_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers40k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of researchers80k - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers80k_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers80k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of researchers160k - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_researchers160k_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/researchers160k-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;


# XML

#echo "doing abstraction of xmark025 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark025_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark025.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark025-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark05 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark05_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark05.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark05-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark1 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark1_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark1.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark2 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark2_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark2.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark2-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark4 - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark4_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/xmark4.xml -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark4-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of xmark025 - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark025_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark025-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of xmark05 - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark05_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark05-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of xmark1 - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark1_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of xmark2 - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark2_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark2-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of xmark4 - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_xmark4_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/xmark4-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;


# RDF

#echo "doing abstraction of bsbm1m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm1m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm1m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm1m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

echo "doing abstraction of bsbm1m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm1m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm1m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm2m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm2m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm2m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm2m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm4m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm4m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm4m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm4m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm8m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm8m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm8m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm8m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm16m - with data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm16m_${sm}_${bm}_${um}_${idref} -i ${FOLDER_DATA}/bsbm16m.nt -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm16m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;

#echo "doing abstraction of bsbm1m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm1m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm1m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of bsbm2m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm2m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm2m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of bsbm4m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm4m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm4m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of bsbm8m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm8m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm8m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;
#
#echo "doing abstraction of bsbm16m - without data loading and parameters ${sm} - ${bm} - ${um} - ${idref}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstraction_bsbm16m_${sm}_${bm}_${um}_${idref} -n -abs -sm ${sm} -bm ${bm} -um ${um} -v -c ${SETTINGS} > ../${DATE}/bsbm16m-${COMMIT}-${sm}_${bm}_${um}_${idref}.log;


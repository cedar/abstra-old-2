# Introduction
Abstra is a system capable of creating the E-R schema (Entity-Relationship schema) of any RDF, JSON or XML datasets. Other formats, such as relational and property graphs will be added in a near future. We call this task an _dataset abstraction_, or in short an _abstraction_.

Abstra has been developed upon https://gitlab.inria.fr/cedar/connectionlens. The underlying modules are as follows:
 - core : the core of the code
 - gui : a Web-based interactive interface which allows to see and create abstractions, as well as explore the dataset.

# Building
Pre-requisites: `Postgres` (9.6 or later), `maven2`, `java 11`, `Tomcat`, `Python3.6/Python3.7` `graphviz` (optional, needed to draw collection graphs using DOT)

## Install a PostgreSQL server
   
A PostgreSQL server must be running locally, and you must have access to an account with the ability to create users. By default, the username is **kwsearch**, therefore this user must be created beforehand with **CREATE DATABASE** privileges. On some installations (depending on the PostgreSQL configuration), this user *needed* to have a password (not to be created lacking one). 
         
## Install Tomcat
   
See http://tomcat.apache.org/ and Tomcat > 9.*. We tested Abstra with 9.0.52 and 9.0.54.
          
## Clone the repository
   
`git clone https://gitlab.inria.fr/nbarret/abstraction-work.git`
     
## Run the `configure` script

**Please note that for this step your python3 installation should point to Python3.6 or Python3.7.**

On Linux and MacOS run `configure.sh` from the `abstraction-work` directory as follows:
 
- `./configure.sh -d path/to/abstra/dir` 

On Windows run `configure.bat` as follows:
- `configure.bat path/to/abstra/dir` 

where `path/to/abstra/dir` is an absolute path to a user-specified directory where Abstra will store the resources it needs and also write in some auxiliary files. `configure.sh` will create this directory and sub-directories required for installing and running Abstra.

**This will create the file `local.settings` under `core/src/main/resources`. This is the settings file that you can modify to tune the abstraction process.**

If needed, manually update the `RDBMS_password` property with your postgres password in the `local.settings` file present under `core/src/main/resources/` and `gui/WebContent/WEB-INF/`.
    
	
### Compile the code and generate the jar using the following commands:
 
- `cd /path/to/abstraction-work/`
- `mvn clean install -DskipTests`

Executing the above command will produce, under the `abstraction-work` directory: 
- a file of the form `core/target/connection-lens-core-full-...jar` that you can run (see below)
- a file named `gui/target/gui.war` that you can deploy in Tomcat (see below)

   
# Running the code 

## In command-line

You can ingest a dataset by running **from the `abstraction-work/core` directory** a command such as: 

`java -jar target/connection-lens-core-...full.jar -i path/to/dataset.json -DRDBMS_DBName=abstra_db`

where you replace `...` with the exact name of the jar file obtained from compiling the project. This example produces the abstraction of the given JSON dataset into the database called `abstra_db`. This will also produce an HTML description available at `/path/to/abstra/dir/tmp` named with your dataset name.


## In your favourtie IDE

You will run the `Experiment` class.

Create a run configuration from the IDE and use the parameters: `-i path/to/dataset.json -DRDBMS_DBName=abstra_db` 



## Getting started with the GUI

The details about starting and using the GUI can be found [here](https://gitlab.inria.fr/cedar/connection-lens/-/blob/develop/docs/Using%20the%20GUI.md).
  

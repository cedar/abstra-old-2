set CURRENT_DIR=%cd%
set CACHE_DIR=cache
set TMP_DIR=tmp
set TT_DIR=treetagger
set TT_BIN=""
Set PYTHON_SCRIPTS_DIR=scripts
set HEIDELTIME_DIR=heideltime
set TT_BIN=binWindows
set ABSTRA_DIR=%1
set TOMCAT_DIR=%2



IF NOT EXIST %ABSTRA_DIR% ( mkdir %ABSTRA_DIR% )

ECHO "ABSTRA_DIR is :'%ABSTRA_DIR%'"

IF NOT EXIST %ABSTRA_DIR%\%CACHE_DIR%  ( mkdir  %ABSTRA_DIR%\%CACHE_DIR% )
IF NOT EXIST %ABSTRA_DIR%\%TMP_DIR% ( mkdir  %ABSTRA_DIR%\%TMP_DIR% )



python -m venv %ABSTRA_DIR%\abstra_env
%ABSTRA_DIR%\abstra_env\Scripts\python -m pip install --upgrade pip
%ABSTRA_DIR%\abstra_env\Scripts\pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html



IF NOT EXIST %ABSTRA_DIR%\%TT_DIR%\bin (mkdir %ABSTRA_DIR%\%TT_DIR%\bin)
IF NOT EXIST %ABSTRA_DIR%\%TT_DIR%\cmd (mkdir %ABSTRA_DIR%\%TT_DIR%\cmd)
IF NOT EXIST %ABSTRA_DIR%\%TT_DIR%\lib (mkdir %ABSTRA_DIR%\%TT_DIR%\lib)
IF NOT EXIST %ABSTRA_DIR%\%TT_DIR%\models (mkdir %ABSTRA_DIR%\%TT_DIR%\models)
IF NOT EXIST %ABSTRA_DIR%\%PYTHON_SCRIPTS_DIR% (mkdir %ABSTRA_DIR%\%PYTHON_SCRIPTS_DIR%)
IF NOT EXIST %ABSTRA_DIR%\%HEIDELTIME_DIR% (mkdir %ABSTRA_DIR%\%HEIDELTIME_DIR%)


xcopy core\lib\treetagger\%TT_BIN% %ABSTRA_DIR%\%TT_DIR%\bin
copy core\lib\treetagger\cmd\utf8-tokenize.perl %ABSTRA_DIR%\%TT_DIR%\cmd\
xcopy core\lib\treetagger\lib %ABSTRA_DIR%\%TT_DIR%\lib
xcopy core\lib\treetagger\models %ABSTRA_DIR%\%TT_DIR%\models

xcopy core\scripts %ABSTRA_DIR%\%PYTHON_SCRIPTS_DIR% /S

copy core\lib\heideltime\config-heideltime-no-treetagger.props %ABSTRA_DIR%\%HEIDELTIME_DIR%\config-heideltime.props

copy core\src\main\resources\parameter.settings %ABSTRA_DIR%\local.settings

set ABSOLUTEPATH_MODELS= %ABSTRA_DIR%\%TT_DIR%\models
set ABSOLUTEPATH_TMP=%ABSTRA_DIR%\%TMP_DIR%
set ABSOLUTEPATH_CACHE=%ABSTRA_DIR%\%CACHE_DIR%
set ABSOLUTEPATH_PYTHON=%ABSTRA_DIR%\abstra_env
set ABSOLUTEPATH_PYTHON_SCRIPTS=%ABSTRA_DIR%\%PYTHON_SCRIPTS_DIR%
set ABSOLUTEPATH_TT=%ABSTRA_DIR%\%TT_DIR%
set ABSOLUTEPATH_HEIDELTIME=%ABSTRA_DIR%\%HEIDELTIME_DIR%

rem add to local.settings the  absolute path to models
(ECHO.
ECHO.
ECHO #### GENERATED PARAMETERS
ECHO.) >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute path to tmp directory
ECHO # Temporary directory for Abstra >> %ABSTRA_DIR%\local.settings
ECHO temp_dir=%ABSOLUTEPATH_TMP% >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute path to the deployed GUI in Tomcat
ECHO # Tomcat directory for Abstra GUI >> %ABSTRA_DIR%\local.settings
ECHO tomcat_dir=%TOMCAT_DIR% >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute path to the cache
ECHO # Abstra cache location >> %ABSTRA_DIR%\local.settings
ECHO cache_location=%ABSOLUTEPATH_CACHE% >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute path to python
ECHO # Python location >> %ABSTRA_DIR%\local.settings
ECHO python_path=%ABSOLUTEPATH_PYTHON%\bin\python >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute path to python scripts
ECHO # Path to python scripts (flair and pdf) >> %ABSTRA_DIR%\local.settings
ECHO python_script_location=%ABSOLUTEPATH_PYTHON_SCRIPTS% >> %ABSTRA_DIR%\local.settings

rem add to local.settings the absolute paths to TreeTagger
ECHO # Treetagger location >> %ABSTRA_DIR%\local.settings
ECHO treetagger_home=%ABSOLUTEPATH_TT% >> %ABSTRA_DIR%\local.settings

ECHO # Stanford models location >> %ABSTRA_DIR%\local.settings
ECHO stanford_models=%ABSOLUTEPATH_MODELS% >> %ABSTRA_DIR%\local.settings

rem add to local.settings the path to heideltime's settings
ECHO # Configuration file used by HeidelTime (date extractor) >> %ABSTRA_DIR%\local.settings
ECHO config_heideltime=%ABSOLUTEPATH_HEIDELTIME%\config-heideltime.props >> %ABSTRA_DIR%\local.settings

rem Path to word2vec model (used for classification)
ECHO # Word2Vec model location (used for classification during abstraction) >> %ABSTRA_DIR%\local.settings
ECHO word_embedding_model_path=%ABSOLUTEPATH_CLASSIFICATION%/word2vec.bin >> %ABSTRA_DIR%\local.settings
ECHO stop_words_english=%ABSOLUTEPATH_CLASSIFICATION%/stop_words_english.txt >> %ABSTRA_DIR%\local.settings
ECHO stop_words_french=%ABSOLUTEPATH_CLASSIFICATION%/stop_words_french.txt >> %ABSTRA_DIR%\local.settings

rem set of semantic resources for the classification
ECHO # set of manually-defined semantic properties >> %ABSTRA_DIR%\local.settings
ECHO set_of_semantic_properties_manual=%ABSOLUTEPATH_CLASSIFICATION%/set-of-semantic-properties-manual.json >> %ABSTRA_DIR%\local.settings
ECHO # set of semantic properties extended with Yago and DBPedia triples >> %ABSTRA_DIR%\local.settings
ECHO set_of_semantic_properties_extended=%ABSOLUTEPATH_CLASSIFICATION%/set-of-semantic-properties-extended.json >> %ABSTRA_DIR%\local.settings
ECHO # set of classes (domain/range) extracted from GitTables semantic properties >> %ABSTRA_DIR%\local.settings
ECHO gitTables_classes=%ABSOLUTEPATH_CLASSIFICATION%/GitTables-classes.txt >> %ABSTRA_DIR%\local.settings
ECHO # get of semantic properties provided by GitTables >> %ABSTRA_DIR%\local.settings
ECHO gitTables_semantic_properties=%ABSOLUTEPATH_CLASSIFICATION%/GitTables-semantic-properties.json >> %ABSTRA_DIR%\local.settings
ECHO # class hierarchy of Schema.org classes >> %ABSTRA_DIR%\local.settings
ECHO schemaorg_class_hierarchy=%ABSOLUTEPATH_CLASSIFICATION%/class-hierarchy-schemaorg-cleaned.json >> %ABSTRA_DIR%\local.settings
ECHO # synonyms for name,designation,denomination file >> %ABSTRA_DIR%\local.settings
ECHO synonyms_denomination_file=%ABSOLUTEPATH_CLASSIFICATION%/words-related-to-denomination.txt >> %ABSTRA_DIR%\local.settings
ECHO # mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person) >> %ABSTRA_DIR%\local.settings
ECHO mapping_classes_to_categories=%ABSOLUTEPATH_CLASSIFICATION%/mapping-classes-to-categories.json >> %ABSTRA_DIR%\local.settings

rem rdf quotient (used to summarize rdf inputs)
ECHO # rdf quotient (used to summarize rdf inputs) >> %ABSTRA_DIR%\local.settings
ECHO rdf_quotient=%ABSTRA_DIR%/rdf-quotient.jar >> %ABSTRA_DIR%\local.settings

rem add to config-heideltime.props the absolute path to TreeTagger
(ECHO.
 ECHO.
 ECHO #### GENERATED PARAMETERS 
 ECHO.) >> %ABSTRA_DIR%\%HEIDELTIME_DIR%\config-heideltime.props
ECHO treeTaggerHome=%ABSOLUTEPATH_TT% >> %ABSTRA_DIR%\%HEIDELTIME_DIR%\config-heideltime.props


copy %ABSTRA_DIR%\local.settings core\src\main\resources
copy %ABSTRA_DIR%\local.settings gui\WebContent\WEB-INF

# Introduction
This project contains the core ConnectionLens library for keyword search in heterogeneous data.

# Installation
## Building
Pre-requisites: `maven2`, `java8`, `postgresql 9.6`, `treetagger`

The following step is only necessary if you have not already built from the root project,
and wish to build the `engine` library independently.

	cd /path/to/project/engine
	mvn install
	(you can use mvn install -DskipTests when you don't need to check test correctness)

Executing the above command will produce two JAR under the `target/` directory:
* one with a name of the form **connection-lens-core-<version>.jar** which contains just our classes compiled; 
* one with a name of the form **connection-lens-core-full-<version>.jar** which also includes all the necessary libraries.

## PostgreSQL
A PostgreSQL server must be running locally, and you must have access to an account with the 
ability to create users. By default, the username is `kwsearch`, therefore this user must be
created beforehand. This username can be overriden by modifying the parameter `RDBMSUser`.

## TreeTagger
TreeTagger must be installed, with the parameter `treetagger_home` pointing to the directory,
and a directory called `models` under that same directory.
In `models`, you must have  the **french.par** and **english-utf8.par** parameter files 
available from  https://files.inria.fr/cedar/models.zip .
Treetagger is available from [http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)
There are two models for English; the first (PENN) worked well for us.

## Python installation

**Python 3.6.5** is required for the correct library versions; you can get it from [here](https://www.python.org/downloads/release/python-365/). The next sequence of steps should be followed: 

1. Go into the root `connection-lens` directory that you got by cloning the repository. Create a virtual environement: `python3.6 -m venv cl_env`.
1. Activate the environement: `source cl_env/bin/activate`.
1. Install the libraries: `pip install -r requirements.txt`. If you see any warnings or errors related to pip version, please upgrade pip using `pip install --upgrade pip` and re-install the libraries. 
1. Deactivate the virtual environment using `deactivate`.
1. In the **`local.settings`** file update the `python_path` parameter with the path to your Python virtualenv installation, for example: `python_path=/home/username/connection-lens/cl_env/bin/python`. This will enable connection-lens to use the python version 3.6 and the library versions set up in the virtual environment for all the python scripts.

## Entity extractors

As of March 2020, we have **two** alternative entity extractors, as explained below.

### Stanford Core NER

Currently, the **default** entity extractor used the Stanford Core NLP infrastructure, which needs 
access to the language-specific model files in a location reachable by the system. 

We currently support French and English. The default model files locations are in `/var/connectionLens/models/`, but it is possible to choose another directory and indicate it in `local.settings` as the value of the parameter `stanford.models`:
	
	/var/connectionlens/models/elra.ser.gz (for French)
	/var/connectionlens/models/english.all.3class.distsim.crf.ser.gz (for English)
	
A simple rule-based entity extraction may also performed to extract additional entities with some specific properties (ex. person names not extracted by the entity extractor), for this we used the Stanford constituency parser which is also a part of the Stanford CoreNLP Tools. For this step some others models will be included for French and English:
	
    /var/connectionlens/models/french.tagger (French tagging)
    /var/connectionlens/models/english.tagger (Fnglish tagging)
    /var/connectionlens/models/frenchSR.ser.gz (French Parsing)
    /var/connectionlens/models/englishSR.ser.gz (English Parsing)

### Flair NLP NER

A new entity extractor (https://gitlab.inria.fr/cedar/cl_extraction_simplified) developed by **Catarina Conceição**  is based on the StanfordNLP and Flair NLP tools and seem to give better results.

This new extractor needs a Python installation and some libraries that allow running the Python script used to extract entities from text.

Before using this extractor, ensure that you have installed Python and required libraries correctly by following the Python installation instructions on your local machine.

By default, the **`Stanford NER (SNER)`** is used to extract entities `extractor=SNER`. 
If you want to switch to `Flair NER`, you need to change the `extractor` parameter in the properties file to:

`extractor=FLAIR_NER`

### Language

For both extractor, the language is defined by the `default_locale` in **`local.settings`** file: 

`fr` for French and `en` for English.


# PDF integration

The registration of PDF files into ConnectionLens is different compared to other formats already supported by the tool. For this, we used a Python tool (https://gitlab.inria.fr/cedar/pdf-integration) to translate the original PDF  into `.json` and/or `n-triples`. This tool was integrated into ConnectionLens. The Python libraries required are also in the requirements.txt file that contains the libraries for Flair NER, hence please complete that step in the installation.

# Locations of the Flair and PDF integration scripts
 
The folder [scripts](https://gitlab.inria.fr/cedar/connection-lens/-/tree/develop/core/scripts) contains Python scripts for Flair NER and content extraction from PDF files. By default, ConnectionLens expects these two folders to be under `/var/connectionlens/scripts`; you can chose another directory and indicate it in `core/src/main/resources/properties` as the value of the parameter `python_script_location`. You need to move the directories  `Flair_NER_tool` and `pdf_scripts` under `/var/connectionlens/scripts/` (or the alternative directory you chose for the scripts)

# Optional: drawing graphs and answers using DOT
Dot (or GraphViz) is a practical tool for drawing graphs of moderate size. It can be downloaded from: [https://www.graphviz.org/](https://www.graphviz.org/).

ConnectionLens does not require it, but if available, and if you set **drawing.draw=true** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file, it will use Dot to generate visualizations of small graph or answer trees.

If you install Dot on your machine, specify the path to the dot executable as the value of the parameter **drawing.dot_installation** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file. 

# Optional: plotting the speed at which answers are obtained using Gnuplot 
Gnuplot is a scientific plot generation tool. It can be downloaded from: [http://www.gnuplot.info/](http://www.gnuplot.info/).

ConnectionLens does not require it, but if available, and if you set **drawing.solution-times=true** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file, it will use gnuplot to generate a graph showing how many answers are found for a query, as a function of time. 

If you install Gnuplot on your machine, specify the path to the gnuplot executable as the value of the parameter **drawing.gnuplot_installation** in the [local.settings](https://gitlab.inria.fr/cedar/connection-lens/blob/develop/core/src/main/resources/local.settings) file. 
You will get a graph in Postscript; most PDF readers can convert it automatically to PDF when you try to open it.

# Running
Assuming all required libraries are on the classpath, the following command will provide further 
details about applicable parameters and options:

	java -jar target/connection-lens-core-<version>...jar --help
	
Otherwise, use the package that includes all dependencies:

	java -jar target/connection-lens-core-full-<version>...jar --help

```
Usage: java -jar connection-lens-full-<version>.jar [options]
  Options:
    -c, --config
       Path to configuration file. The file will be used to set all default
       values. If the option is not set, default parameter files will searched in the
       current directory. If no such file is found, build-in default will be used.
    -h, --help
       Displays this help message.
       Default: false
    -ou, --orignal-uri
       Comma-separated list of original uri's for each file (dataset) loaded.
    -i, --input
       Comma-separated list of file or directory paths. If a directory is
       specified, all descendant files are used as inputs.
       Default: []
    -a, --interactive-mode
       If true, read incoming query from STDIN after the registration phase,
       until EOF is reached.
       Default: false
    -n, --noreset-at-start
       Do NOT reset the data structures upon starting.
       Default: false
    -o, --output
       Path to output file. Default: STDOUT
       Default: java.io.PrintStream@548e7350
    -q, --query
       A (single) keyword query to execute.
    -Q, --query-file
       A file that containts a list of queries to execute.
    -t, --thread-for-p2b
       P2B is launched with multi-threading
       Default: false
    -v, --verbose
       Use verbose mode.
       Default: false
    -D
       Force the given parameters, bypassing defaults parameters and those found
       in parameter files, ignoring those that may be specified in each parameter
       file.For instance, '-Dtimeout=10000' force a timeout 10s seconds on a phase.
       Syntax: -Dkey=value
       Default: {}
```

# Examples
The following example shows how to load 2 input files using the default parameter values. Loading is indicated by **-i**.

	java -jar target/connection-lens-full-<version>.jar -i inputFile1,inputFile2

The following command runs the load a single input file using English and an alternative P2B method:

	java -jar target/connection-lens-full-<version>.jar -i inputFile -p2bm DFS -l en

To run queries interactively (assuming data is already loaded):

	java -jar target/connection-lens-full-<version>.jar -n -a

The parameter RDBMSDBName can be used both for loading and querying to designate the database (within Postgres) 
where data should be added, respectively, the database in which to look for query answers. 

## Queries
The query are lists of space-separated keyword. The results return ways to combined all nodes 
matches matching each of the keywords independently. Note that keywords between quotes are 
considered *atomic*, i.e. node and edge matching the whole string between quotes will be returned.

### Example 1:

	John Doe

This will return connections between nodes whose labels match  "John" and those whose labels match "Doe". 

	"John Doe"

This will return nodes whose labels match "John Doe". 

	"John Doe" John Smith

This will return connections between three nodes: one whose labels matches "John Doe", another whose label matches "John" and another whose label matches  "Smith". 




random_seed=0

# DATABASE
# --------
RDBMS_type = POSTGRESQL
RDBMS_host = localhost
RDBMS_port = 5432
RDBMS_user = abstra_user
RDBMS_password =
RDBMS_DBName = abstra_default


# LANGUAGE
# --------
# The default locale used through the execution of the system. Possible values: [en, fr]
default_locale=en


# DRAWING AND PLOTTING
# --------------------
# The path where dot is installed
drawing.dot_installation=/usr/local/bin/dot
drawing.draw=false
drawing.coarse_edge=false

# Parameters for plotting solution numbers as a function of the search time
drawing.solution_times=false

# The same directory as for drawing (parameter above) will be used.
# However, one can plot solutions or not, and draw trees or not, independently.
drawing.gnuplot_installation=/usr/local/bin/gnuplot


# CACHES
# ------
# The number of nodes to keep in cache
node_cache=100000
# The number of edges to keep in cache
edge_cache=100000
# The number of same as edges to keep in cache
same_as_cache=50000
# The number of string-pair and similarity results to keep in cache
similarity_cache=200000
# The number of nodes for which the adjacency list in cache
adjacency_cache=50000
# The number of (similarity) candidates kept in cache
candidate_pairs_cache=50000
# The number of specificities kept in cache
specificity_cache=5000
# Maximum number of extraction entries that will be cached in memory.
# Note that if value_atomicity=PER_GRAPH, a buffer of size node_cache will cache all nodes
# (including, but not only, value nodes) thus no extraction will be repeated, thus no need for caching.
# -1 means unbounded caching; 0 means no caching.
max_extraction_mem_cache=100000
# Maximum number of extraction entries that will be cached on disk.
# The same comment wrt value_atomicity=PER_GRAPH applies (but disk caching will hurt performance even more).
# You may still want some disk caching when using Flair, in order to speed up future executions.
# Memory cached extractor entities are saved to disk only if you use disk caching.
max_extraction_disk_cache=-1

# Number of updates to keep in memory before spilling to the database
# (1 mean sequential updates, <= 0 means all updates are kept in memory until spilling)
update_batch_size=1000000


# INDEXING
# --------
# The indexing model. Possible values: [POSTGRES_FULLTEXT, LUCENE]
indexing_model=POSTGRES_FULLTEXT
lucene_base_dir=.lucene


# EXTRACTION
# ----------
# Named entity extractor. Possible values: SNER (Stanford extractor), FLAIR_NER (Flair extractor), NONE (no extraction)
extractor=SNER
# This parameter is used to start the Flask Web service, then it is used by Flair to determine
# how many sentences to process in parallel, which helps e.g. when running on a GPU. A useful value was 128.
extractor_batch_size_flair=1
# This parameter controls the size of a batch of texts ConnectionLens sends to the extractor.
# Currently this is only useful if you use Flair extraction and the Flair batch size is not 1.
# It should be a multiple of extractor_batch_size_flair (for example 10 times larger).
extractor_batch_size_cl=1
# Whether to split long texts into sentences (thus creating more nodes)
split_sentences=false
# Maximum length for a node label if split=true. The current value is 1/3 of the default Postgres page size.
max_label_length=2000

# Whether to extract from URI nodes (true or false). In some cases we don't want it.
extract_from_uris=false
# Boolean to indicate if date extraction should be run or not. It can be very long on large graphs (more than 100 nodes).
extract_dates=false

# Extraction policies (for now available only for XML). Policies appear on the same line and are comma-separated.
# Each policy is of the form "XMLPath Decision" where Decision can be: Person, Organization, Location, NoExtract, or NoExtractAll
# Fields that are not referred to by any extraction policy will be handled by the respective extractor.
extract_policy=
# PubmedArticleSet.PubmedArticle.AuthorList.Author.Name Person, PubmedArticleSet.PubmedArticle.KeywordList NoExtractAll
# Entity Profiles for smart extract, if none specified, smartExtract is not run. Entity profiles should be specified as
# in the example in a JSON array containing a JSON object for each couple path/profile specified.
# The profile is a JSON object, specifying the number of named entities (org/pers/loc)
smart_extraction_profile=
#Example for CoiStatement:
# smart_extraction_profile= [{"path":"PubmedArticleSet.PubmedArticle.CoiStatement", "profile":{"organization":1}}]

# Whether to call Ambiverse for disambiguation
entity_disambiguation=false
# Entity disambiguation policies follow the same syntax as extraction policies except that only Person, Organization, Location are supported.
# If the disambiguation policy is non empty, disambiguation is attempted only for the entities explicitly designated in the policy.
# If the disambiguation policy is empty, all entities, regardless of the extractor who produced them, will be disambiguated.
entity_disambig_policy=


# PDF
# ---
# Type of tables in PDF. Possible values: lattice (separation lines of cell written), stream (separation lines of cell not written).
flavor=lattice


# NODE COMPARISON AND SIMILARITY MEASURES
# ---------------------------------------
# Whether or not to perform node comparisons (true or false)
compare_nodes=false
# Threshold above which sameAs are considered relevant
similarity_threshold_jaccard=.8
# similarity between short strings
similarity_threshold_levenshtein=.8
# similarity for entities
similarity_threshold_uri=.8
similarity_threshold_entity=.95
similarity_threshold_organization=.95
similarity_threshold_location=.95
similarity_threshold_person=.8
similarity_threshold_fname=.8
similarity_threshold_lname=.9
similarity_threshold_email=1
similarity_threshold_hashtag=1
similarity_threshold_number=-1
similarity_threshold_datetime=1

# whether or not to require a short, common, meaningful prefix between *any two* labels compared
# if true, this will be implemented based on the column labelPrefix
compare_stored_label_prefix=true
# mandatory prefix length
stored_prefix_length=3
# "do not link" labels. Must be double quote-enclosed and comma-separated.
do_not_link_labels=
#"[Données non publiées]","true","false","M.","Mme", "Mme.","Paris","France","Net","CREATION","VUE_PDF_DU_RECEPISSE_DU_DEPOT_XML","SCI","néant"

# How to select pairs of short strings for comparison: having a common prefix (PREFIX); being identical (EQUAL); not at all (NONE)
short_string_comparison=PREFIX
# How to select pairs of long strings for comparison: having a common prefix (PREFIX); being identical (EQUAL); not at all (NONE)
long_string_comparison=PREFIX
# Minimum matching prefix length for prefix-based string matching: needs to be at least as large as stored_prefix_length!
matching_prefix_length=3
# Length above which Jaccard similarity will be used over the other
long_string_threshold=128
# Length over which Jaro/Hamming/Levenshtein similarity will be used over the other
short_string_threshold=32
# Number of keyword allowed to be missing in a match for an answer to be returned.
query_cover_threshold=0
# Whether the Jaccard common-word heuristic should be used.
jaccard_common_word=true
# Used with LENGTH above to determine the length range (must be within [0, 1]
length_difference_ratio=.2
# Possible values:
# - PER_INSTANCE: all value nodes are distinct
# - PER_PATH:     all value nodes that share the same label and type within a given data source, are unified
# - PER_DATASET:    all value nodes that share the same label within a data source, are unified
# - PER_GRAPH: all value nodes sharing the same label, across all data sources composing a graph, are unified
value_atomicity=PER_INSTANCE
# How many entity nodes to create when loading
# PER_OCCURRENCE, PER_DATASET, PER_LOADING, PER_GRAPH
entity_node_creation_policy=PER_GRAPH


# SEARCH
# ------
# Timeout (ms) after which to stop the search
search_stopper_timeout=15000
# Number of results after which to stop the search
search_stopper_topk=-1
# Time (ms) to wait before stopping the search on no new discovery of answers.
search_stopper_stationarity=-1
# Maximum number of edges allowed in an answer.
search_stopper_ATsize=-1

# The default search algorithm to use:
# Possible values:
# - GAM: Grow and Aggressive Merge with SMoESP.
# - GAM_E:Grow and Aggressive Merge as described in BDA 2020, Inf. Sys. 2022.
# - SQL_PATH: supports only 2-keyword queries and only finds results within a single dataset; computes paths up to some fixed length using SQL directly
# - BFS_G: BFS with Grow only.
# - BFS_M: BFS with merge
# - BFS_AM: BFS with aggressive merge
global_search_algorithm=GAM
# Parameters for the GAM algorithm(s):
# Turn on/off MoESP
do_not_use_moesp=false
# Turn on/off SMESP
do_not_use_smesp=false
# Number of matches to consider for a keyword:
max_matches_per_kwd=-1
# Filter for edges; a semicolon(;) separated list of the only edge labels allowed in the answers
edge_filter=
# Metric on a (tree, root-adjacent edge) pair that determines the priority of the pair in the queue:
# - NUMBER_OF_NODES: The number of nodes of the tree.
# - ROOT_ADJACENT_EDGES: The number of edges adjacent to the tree root.
# - NUMBER_OF_NODES_ROOT_ADJACENT_EDGES: The number of nodes in the tree, then the number of edges adjacent to the tree root.
# - NODES_MINUS_MATCHES: The number of tree nodes minus the number of tree matches.
# - MATCHES_MINUS_NODES: The number of tree matches minus the number of tree nodes.
# - NODES_MINUS_MATCHES_THEN_FANOUT: The number of tree nodes minus the number of tree matches, then (if tie) the number of root-adjacent edges.
# - MATCHES_MINUS_NODES_THEN_FANOUT: The number of tree matches minus the number of tree nodes, then (if tie) the number of root-adjacent edges.
# - NODES_MINUS_MATCHES_THEN_SPECIFICITY: The number of tree nodes minus the number of tree matches, then (if tie) the specificity of the edge.
# - MATCHES_MINUS_NODES_THEN_SPECIFICITY: The number of tree matches minus the number of tree nodes, then (if tie) specificity of the edge.
# - MATCHES_THEN_NODES_THEN_SPECIFICITY: (-1)* number of tree matches, then the number of nodes, then (if tie) the specificity of the edge.
# - FIXED_SCORING: A fixed score of number of tree nodes minus the number of tree matches plus the specificity of the edge computes just once per queue item.
# - NUM_EDGES: A simple scoring scheme which counts the number of edges in an answer tree.
gam_heuristics_growth=MATCHES_MINUS_NODES_THEN_SPECIFICITY
# whether or not to limit the number of entity nodes extracted from the same text matching the same kwd, to 1
one_entity_match_per_text=false
# bidirectional_search: target will be source
gam_bidirectional_search=true
# for unidirectional search, what edges to take: true for incoming, false for outgoing
unigam_edges_direction=true
# whether or not to only traverse edge whose specificity is above a minimum threshold
query_only_specific_edge=false
# whether or not to load all the graph in the memory cache before evaluating queries
prefetch_graph_in_memory=false
# threshold for same_as edges
same_as_threshold=.8
# number of threads
gam_number_of_thread=4
# Scoring functions.
# - DEFAULT: The default weighted scoring function.
# - ANSWERSIZE: The number of edges as the scoring function.
scoring_function=DEFAULT
# Weight of the matching score in the scoring function
score_alpha=0.2
# Weight of the connection score in the scoring function
score_beta=0.2
# Weight of the specificity in the scoring function is 1 - alpha - beta
# max length of label to use Needleman-Wunsch in score matching/chunk long label for print and visualization
node_label_matching_length=80
# the priority queue strategy to be used for GAMSearch.
# - SINGLE: A single priority queue.
# - MULTI: Multiple Priority Queues, one for each subset of the set of keywords (inputs).
pqueue_type=SINGLE
# Parameters for the SQL_SEARCH algorithm:
# Maximum path length:
sql_search_max_length=5


# GRAPH STORAGE
# -------------
# The storage model i.e. how the graph is stored. Possible values:
# - COMPACT: The graph is stored in a SQL database, IDs are integer-based
storage_model=COMPACT
# Refresh materialized view in DB.
storage_use_materialized_strong_same_as_edges=false
# Whether or not to remove obsolete tuples from the specificity table
cleanup_specificity=false


# ABSTRA PARAMETERS
# -----------------
# Whether to normalize the original graph or not.
# This can be done after the dataset registration without running the abstraction.
# Possible values = [true, false]
perform_normalization=false

# Reset the abstract graph but don't reset the original graph. Should be combined with -n (no reset at first)
# Should be set to false to query an abstract that has been built in a previous run.
# Possible values = [true, false]
reset_abstraction=true

# This parameter controls where to stop in the abstraction. Abstraction will stop after the specified task.
# Possible values = [normalization, collections, mainCollections, classification, reporting]
end_at=reporting

# Should be set to true when the collection graph has been already stored in Postgres
# DON'T FORGET TO USE THE OPTION -n TO NOT RESET THE DATABASE BEFORE STARTING THE ABSTRACTION
start_from_collection_graph=false

# Main collections selection parameters
# Maximal number of main collections. -1 to report all collections
E_MAX=5
# Minimal data coverage to reach to stop the selection of main collections. 0 < COV_MIN <= 1.0
COV_MIN=1.0
# Minimal edge transfer factor for the FL and FL_AC boundaries.
ETF_MIN=0.8
# the k for k-desc and k-desc scoring methods
k=3
# Whether to rely (or not) on ID/IDREF ref edges in XML dataset. Possible values are:
# DISABLE: IDREF edges do not impact (at all) the selection of main collections
# ENABLE_SCORE: IDREF edges are enabled while computing the score of each collection (i.e. they transfer weight) but not while computing the boundaries ("internal" pointed collections are not inlined)
# ENABLE_BOUNDARY: IDREF edges are enabled for scoring collections and for the boundary computation ("internal" pointed collections are inlined)
use_idrefs_xml=ENABLE_SCORE

# Classification parameters
# Whether to classify all collections or only main entity collections (true to use the GUI, false else)
classify_all_collections=false
# Minimal similarity between a semantic property and a data property.
SIM_MIN=0.8
# Minimal fitness between the collection properties and the assigned category
SCORE_CAT_MIN=0.3

# Reporting parameters
# true to display primary-key symbols in the E/R schema, false else
show_PK=false

# True to compare the abstraction with a reference.
# This will compute the F1 score of nodes, relationships and boundaries of main entity collections
compare_with_reference=false

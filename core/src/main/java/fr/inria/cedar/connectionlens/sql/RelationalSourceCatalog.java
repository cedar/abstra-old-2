/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.inria.cedar.connectionlens.source.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.source.DataSource.Types;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * This class implements a catalog that will list, for each registered
 * datasource, its id, its path and its type
 * 
 * @author redouane
 *
 */
public class RelationalSourceCatalog extends DataSourceCatalog {

	/** The logger. */
	private final static Logger log = LoggerFactory.getLogger(RelationalSourceCatalog.class);

	private Map<String, PreparedStatement> statementMap = new ConcurrentHashMap<>();

	/** The database connection. */
	private Connection conn;

	/** The name of the catalog table is the database */
	private final String CatalogTableName;

	/**
	 * Instantiates a new relational source catalog.
	 *
	 * @param x          the entity extractor
	 * @param indexStats
	 */
	public RelationalSourceCatalog(Factory f, EntityExtractor x, StatisticsCollector graphStats,
			StatisticsCollector extractStats, StatisticsCollector indexStats) {
		super(f, x, graphStats, extractStats, indexStats);
		this.CatalogTableName = SchemaTableNames.ORIGINAL_CATALOG_TABLE_NAME;
		this.conn = null;
	}

	/**
	 * Drops the underlying relational table.
	 */
	public void drop() {
		String query = "DROP TABLE IF EXISTS " + CatalogTableName + " CASCADE";
		try {
			PreparedStatement preparedStatement = createPreparedStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			log.warn(e.getMessage());
		}

	}

	/**
	 * Creates the underlying relational table.
	 */
	protected void create() {
		String query = "CREATE TABLE " + CatalogTableName
				+ "(id serial primary key, type varchar, path varchar unique, original_uri varchar)";
		try {
			PreparedStatement preparedStatement = createPreparedStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			log.warn(e.getMessage());
		}
	}

	/**
	 * Drops then recreates the underlying relational table. {@inheritDoc}
	 * 
	 * @see fr.inria.cedar.connectionlens.source.DataSourceCatalog#reset()
	 */
	@Override
	public void reset() {
		drop();
		create();
		super.reset();
		log.info("CATALOG RESET");
	}

	@Override
	public void close() {
		try {
			for (String key : statementMap.keySet()) {
				PreparedStatement stmt = statementMap.get(key);
				stmt.close();
				statementMap.remove(key);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Loads any pre-existing data source from the database into the catalog.
	 * {@inheritDoc}
	 * 
	 * @see fr.inria.cedar.connectionlens.source.DataSourceCatalog#init()
	 */
	@Override
	public void init(Graph graph, boolean reset) {
		// IM, 4/21
		try {
			RelationalGraph rGraph = (RelationalGraph) graph;
			this.conn = rGraph.getConnection();
		} catch (ClassCastException cce) {
			throw new IllegalStateException("RelationalGraph required for now");
		}
		if (reset) {
			reset();
			return; // nothing more to do
		}
		// if not reset, we need to load the previous entries
		String query = "SELECT id, path, original_uri, type FROM " + CatalogTableName;
		try {
			PreparedStatement preparedStatement = createPreparedStatement(query);
			ResultSet result = null;
			// IM, 4/21: in some cases the table had not been created, patching below
			// try {
			result = preparedStatement.executeQuery();
			// }
			// catch(SQLException e) {
			// create();
			// result = preparedStatement.executeQuery();
			// } // end of patch
			while (result.next()) {
				int id = result.getInt(1);
				URI uri = new URI(result.getString(2));
				URI originalURI = new URI(result.getString(3));
				Types type = Types.valueOf(result.getString(4).toUpperCase());
				// the new constructor assign possible null value to graphStats and extractStats
				if (graphStats != null) {
					graphStats.start(uri);
				}
				if (extractStats != null) {
					extractStats.start(uri);
				}
				// adding the entry to the graph
				addEntry(graph, id, type, uri, originalURI);
			}
		} catch (SQLException | URISyntaxException e) {
			throw new IllegalStateException(e);
		}
		// if identifier out of range of serial ids given so far
	}

	@Override
	public DataSource addEntry(Graph graph, Types type, URI uri, URI origURI) {
		DataSource result = super.addEntry(graph, type, uri, origURI);
		if (result != null) { // if not already added
			String originalURI = "";
			if (origURI != null) {
				originalURI = origURI.toString();
			}
			String query = "INSERT INTO " + CatalogTableName + " (id, type, path,original_uri) VALUES (?, ?, ?,?)";
			try {
				PreparedStatement preparedStatement = createPreparedStatement(query);
				preparedStatement.setInt(1, result.getID());
				preparedStatement.setString(2, type.toString());
				preparedStatement.setString(3, uri.toString());
				preparedStatement.setString(4, originalURI);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				log.warn("This datasource has already been added");
			}
			result.setGraphStats(graphStats);
			result.setExtractStats(extractStats);
		}
		return result;
	}

	/**
	 * Builds a data source instance based on the given parameters.
	 *
	 * @param identifier the data source identifier
	 * @param type       the data source type
	 * @param uri        the data source URI
	 * @return a fresh instance of data source
	 * @throws IllegalStateException if the argument did not suffice to build the
	 *                               data source instance.
	 */
	@Override
	protected DataSource build(int identifier, Types type, URI uri, URI origURI, Graph graph) {
		try {
			switch (type) {
			case HTML:
				return new HTMLDataSource(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case XML:
				return new XMLDataSource(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case XML_V2:
				return new XMLDataSource2(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case RDF:
				return new RDFDataSource(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case JSON:
				return new JSONDataSource(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case JSON_V2:
				return new JSONDataSource2(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case RELATIONAL:
				return new RelationalDataSource(factory, identifier, uri, origURI, graphStats, extractStats, graph);
			case TEXT:
				return new TextDataSource(factory, identifier, uri, origURI, extractor, graphStats, extractStats, graph);
			case NEO4J:
				return new Neo4jDataSource(factory, identifier, uri, origURI, extractor, graphStats, extractStats, graph);
			case CSV:
				return new CSVDataSource(factory, identifier, uri, origURI, extractor, graphStats, extractStats, graph);
			case CSV_V2:
				return new CSVDataSource2(factory, identifier, uri, origURI, extractor, graphStats, extractStats, graph);
			default:
				throw new IllegalStateException("No such data source type: " + type);
			}
		} catch (SQLException e) {
			throw new IllegalStateException("Problem while building DS: " + uri, e);
		}
	}

	@SuppressWarnings("resource")
	protected PreparedStatement createPreparedStatement(String sql) {
		try {
			PreparedStatement result = statementMap.get(sql);
			if (result == null) {
				statementMap.put(sql, (result = conn.prepareStatement(sql)));
			}
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException("Could not create prepared statement " + sql, e);
		}
	}

}

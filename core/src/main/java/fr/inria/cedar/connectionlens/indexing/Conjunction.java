/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.indexing;

import com.google.common.base.Joiner;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

/**
 * A Conjunction of query components.
 */
public class Conjunction implements QueryComponent, Iterable<AtomicKeyword>{

	/** The underlying atomic keywords. */
	protected final Collection<AtomicKeyword> values;

	/**
	 * Instantiates a new conjunction.
	 *
	 * @param values the values
	 */
	public Conjunction(Collection<AtomicKeyword> values) {
		super();
		this.values = values;
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.QueryComponent#value()
	 */
	@Override
	public String value() {
		return Joiner.on("&").join(values);
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + value() + ")";
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(values);
	}

	/**
	 * @return the number of underlying components in the conjunction
	 */
	public int size() {
		return values.size();
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Conjunction other = (Conjunction) obj;
		return values.equals(other.values);
	}
	
	/**
	 * @param words an array words
	 * @return a Conjunction of atomic keywords matching the given array of words.
	 */
	public static Conjunction of(String... words) {
		return new Conjunction(AtomicKeyword.asList(words));
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<AtomicKeyword> iterator() {
		return values.iterator();
	}
}

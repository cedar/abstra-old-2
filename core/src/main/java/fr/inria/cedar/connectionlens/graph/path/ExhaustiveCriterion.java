/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph.path;

/**
 * The special criterion used for exhaustive exploration: it always return <code>false</code> 
 * because it does basically nothing to stop the exploration.
 * 
 * @author Mihu
 */
public class ExhaustiveCriterion extends StopCriterion {

	/**
	 * Instantiates a new exhaustive criterion.
	 */
	public ExhaustiveCriterion() {}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#canStop()
	 */
	@Override
	public boolean canStop() {
		return false; // always return false
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#reset()
	 */
	@Override
	public void reset() {
		// do nothing
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#update()
	 */
	@Override
	public void update() {
		// also do nothing
	}
}
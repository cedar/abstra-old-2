package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Utils {

    public static final Logger log = Logger.getLogger(Utils.class);

    public static Set<String> intersectionSetsOfStrings(Set<String> s1, Set<String> s2) {
        Set<String> intersection = new HashSet<>(s1); // use the copy constructor
        intersection.retainAll(s2);
        return intersection;
    }

    // intersection with duplicates
    public static ArrayList<String> intersectionListsOfStrings(ArrayList<String> l1, ArrayList<String> l2) {
        return l1.stream().distinct().filter(l2::contains).collect(Collectors.toCollection(ArrayList::new));
    }

    // intersection with duplicates
    public static ArrayList<Integer> intersectionListsOfInts(ArrayList<Integer> l1, ArrayList<Integer> l2) {
        return l1.stream().distinct().filter(l2::contains).collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<Integer> buildArrayListFromResultSetInteger(ResultSet rs) throws SQLException {
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (rs.next()) {
            arrayList.add(rs.getInt(1));
        }
        return arrayList;
    }

    public static HashMap<Integer, ArrayList<Integer>> buildHashMapFromResultSetArrayListInteger(ResultSet rs) throws SQLException {
        HashMap<Integer, ArrayList<Integer>> hashmap = new HashMap<>();
        int currentId = -1;
        ArrayList<Integer> arraylist = new ArrayList<>();
        while (rs.next()) {
            if (currentId != rs.getInt(1) && !rs.isFirst()) {
                hashmap.put(currentId, arraylist);
                arraylist = new ArrayList<>();
                arraylist.add(rs.getInt(2));
                currentId = rs.getInt(1);
            } else {
                arraylist.add(rs.getInt(2));
                currentId = rs.getInt(1);
            }
        }
        hashmap.put(currentId, arraylist);
        return hashmap;
    }

    public static HashMap<Integer, ArrayList<String>> buildHashMapFromResultSetArrayListString(ResultSet rs, int sizeLimit) throws SQLException {
        HashMap<Integer, ArrayList<String>> hashmap = new HashMap<>();
        while (rs.next()) {
            int currentId = rs.getInt(1);

            if (!hashmap.containsKey(currentId)) { // if the key does not exist yet, we create it
                hashmap.put(currentId, new ArrayList<>());
            }
            if(sizeLimit == -1 || hashmap.get(currentId).size() < sizeLimit) { // if we limit the size of the arraylist
                hashmap.get(currentId).add(rs.getString(2));
            }
        }
        return hashmap;
    }

    public static HashMap<String, Integer> buildHashMapSetFromResultSetString(ResultSet rs) throws SQLException {
        HashMap<String, Integer> hashmap = new HashMap<>();
        while (rs.next()) {
            hashmap.put(rs.getString(1), rs.getInt(2));
        }
        return hashmap;
    }

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import org.apache.log4j.Logger;

import static com.google.common.base.Preconditions.checkArgument;
import java.util.Arrays;
import java.util.Objects;

/**
 * A Score.
 */
public interface Score extends Comparable<Score> {

	/**
	 * @return the underlying value of the score object 
	 */
	Object value();
	
	/**
	 * A score with double value.
	 */
	public static class DoubleScore implements Score {
		
		/** The score's double value. */
		private final double value;

		/**
		 * Instantiates a new double score.
		 *
		 * @param v the value
		 */
		public DoubleScore(float v) {
			this.value = v;
		}

		/**
		 * Instantiates a new double score.
		 *
		 * @param v the value
		 */
		public DoubleScore(double v) {
			this.value = v;
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof DoubleScore)) {
				return false;
			}
			return value().equals(((DoubleScore) o).value());
		}

		@Override
		public int hashCode() {
			return Objects.hash(value);
		}

		@Override
		public int compareTo(Score o) {
			checkArgument(o instanceof DoubleScore);
			return(int) Math.signum(this.value - (double) o.value());
		}

		@Override
		public Double value() {
			return value;
		}

		@Override
		public String toString() {
			return String.valueOf(value);
		}
	}

	/**
	 * A score with double vector value.
	 */
	public static class DoubleVectorScore implements Score {
		
		/** The logger. */
		private final Logger log = Logger.getLogger(DoubleVectorScore.class);
		
		/** The score vector. */
		private final double[] value;

		/**
		 * Instantiates a new double vector score.
		 *
		 * @param size the size
		 */
		public DoubleVectorScore(int size) {
			this.value = new double[size];
		}
		
		/**
		 * Sets the ith item of the score vector to the given value.
		 *
		 * @param i the index to set
		 * @param v the new value to set
		 */
		public void set(int i, double v) {
			this.value[i] = v;
		}

		/**
		 * {@inheritDoc}
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(Score o) {
			checkArgument(o instanceof DoubleVectorScore);
			if (value.length != ((DoubleVectorScore) o).value.length) {
//				throw new IllegalArgumentException("Attempting to compare vectors of different sizes.");
				// TODO: Comparing vector of different sizes should not be allowed.
				log.debug("Attempting to compare vectors of different sizes.");
				return ((DoubleVectorScore) o).value.length - value.length;
			}
			// if both trees have exactly the same score vector
			double[] values = (double[]) o.value();
			if (Arrays.equals(this.value, values)) {
				return 0;
			}
			
			// compare non-null scores of 2 trees
			int count1 = 0, count2 = 0;
			for (int i = 0, l = this.value.length; i < l; i++) {
				count1 += this.value[i] != 0 ? 1 : 0;
				count2 += values[i] != 0 ? 1 : 0;
			}
			
			if (count1 > count2) {
				return 1; // AT with more non-null scores if better
			}
			if (count1 < count2) {
				return -1;
			}

			// if both trees have the same number of non-null scores
			// compare the quality of matching: alpha*(mean of matching score) + (1-alpha)*connection score
			double[] score1 = this.value;
			double aggr_score1 = 0;
			for (int i = 0; i < score1.length-1; i++) { // mean of |Q| scores (excluding connection score)
				aggr_score1 += (score1[i]/score1.length);
			}
			
			double[] score2 = values;
			double aggr_score2 = 0;
			for (int i = 0; i < score2.length-1; i++) { // mean of |Q| scores (excluding connection score)
				aggr_score2 += (score2[i]/score2.length);
			}
			
			double alpha = 0.25;
			double quality1 = alpha*aggr_score1 + (1-alpha)*score1[score1.length-1];
			double quality2 = alpha*aggr_score2 + (1-alpha)*score2[score2.length-1];
			
			if (quality1 > quality2) {
				return 1;
			}
			if (quality1 == quality2) {
				return 0;
			}
			return -1;
		}
		
		public DoubleScore mean() {
			double result = 0.0;
			for (double v: value) {
				result += v;
			}
			return new DoubleScore(result/value.length);
		}
		
		/**
		 * {@inheritDoc}
		 * @see fr.inria.cedar.connectionlens.score.Score#value()
		 */
		@Override
		public double[] value() {
			return value;
		}

		/**
		 * {@inheritDoc}
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof DoubleVectorScore)) {
				return false;
			}
			return Arrays.equals(value, ((DoubleVectorScore) o).value);
		}

		/**
		 * {@inheritDoc}
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hash(value);
		}

		/**
		 * {@inheritDoc}
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return Arrays.toString(value);
		}
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_LITERAL;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.NODE_LOOKUP_T;
import static org.openrdf.rio.RDFFormat.NTRIPLES;
import static org.openrdf.rio.RDFFormat.TURTLE;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openrdf.model.BNode;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.RDFParserRegistry;
import org.openrdf.rio.Rio;
import org.openrdf.rio.turtle.TurtleParserFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Implementation of an RDF DataSource.
 */
public class RDFDataSource extends DataSource implements Serializable {
	
	/**
	 * Common terms used to declare schema that would be no use when added to Index
	 */
	public final static Set<String> schemaTerms = ImmutableSet.of("http", "https", "ftp", "mailto", "file", "data",
			"irc", "org", "com", "news", "telnet", "gopher", "schemas", "schema", "edu", "gov", "net", "fr", "en", "uk",
			"ca", "us", "www");

	public static final String propertyChildOf = "https://connectionlens.fr/entity/childOf";

	/** Property EntityValue. */
	public static final String propertyEntityValue = "https://connectionlens.fr/entity/value";

	/** URI Entity Prefix. */
	public static final String uriEntityPrefix = "https://connectionlens.fr/entity/";

	/** this appears to double up with respect to TopLevelDataSource.nodePerDSAndLabel 
	 *  There could however be an interest to have it use Values (which are specialized RDF things)
	 *  instead of Strings, as keys. 
	 */
	private HashMap<Value, Node> labelToNode = new HashMap<>();
	/**
	 * Create a new RDF data source
	 * 
	 * @param id           an identifier
	 * @param uri          path to the data source
	 * @param extractStats
	 */
	public RDFDataSource(Factory f, int id, URI uri, URI origURI, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, id, uri, origURI, Types.RDF, graphStats, extractStats, graph);
		log = Logger.getLogger(RDFDataSource.class);
		//log.info("Graph stats: " + graphStats);
	}

	@Override
	public void init() {
	}

	@Override
	public void postprocess(Graph graph) {
	}

	/**
	 * Transforms and entity to a URI.
	 *
	 * @param entity some entity
	 * @return the entity's URI representation
	 */
	public static String entityToURI(EntityOccurrenceNode entity) {
		String uriEntity = uriEntityPrefix + entity.getDataSource().getLocalURI() + "/"
				+ entity.getLabel().replaceAll("\\s+", "_") + "?offset=" + entity.getOffset() + "&length="
				+ entity.getLength() + "&type=" + entity.getType();
		return uriEntity;
	}


	/**
	 * Check whether a word is a schema term to be ignored when indexing
	 * 
	 * @param word word to check
	 * @return true if it is a schema term, false otherwise
	 */
	public static boolean isSchemaTerm(String word) {
		return schemaTerms.contains(word);
	}

	/**
	 * Checks if a string contains digits [0-9]+
	 * 
	 * @param word
	 * @return <code>true</code> if the string contains digits, <code>false</code>
	 *         otherwise
	 */
	public static boolean hasDigits(String word) {
		return Pattern.compile("[A-Za-z]*[0-9]+[A-Za-z]*").matcher(word).find();
	}

	/**
	 * Check if a string is actually a number
	 * 
	 * @param word
	 * @return <code>true</code> if the string is a number, <code>false</code>
	 *         otherwise
	 */
	public static boolean isDigit(String word) {
		return Pattern.compile("[0-9]+([\\.,][0-9]+)?").matcher(word).find();
	}

	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		// IM 8/2/20: an RDF graph does not have a well-defined root. No sense in a
		// cyclic edge.
		processor.accept(buildLabelledEdge(root, root, Edge.ROOT_LABEL));
		try (InputStream in = this.localURI.toURL().openStream()) {
			TripleHandler handler = new TripleHandler(processor, this);
			
			// 27/04/22 Madhu: The TurtleParserFactory registry below has been done explicitly here.
			// This is a temporary fix for Issue#708. Keeping it until we find the right way to register
			// that a new parser is available.
			// If registered correctly, the following lines can be used to pick the parser automatically.
			// RDFFormat format = Rio.getParserFormatForFileName(localURI.toString());
			// RDFParser p = Rio.createParser(format);
			RDFParserRegistry.getInstance().add(new TurtleParserFactory());
			RDFParser p;
			if(this.localURI.toString().endsWith(".nt"))
				p = Rio.createParser(NTRIPLES);
			else
				p = Rio.createParser(TURTLE);
			p.setRDFHandler(handler);
			p.parse(in, "");
			// this.stats.tick(this.localURI, TRIPLE_PARSE_T);
			// this.graphStats.put(this.localURI, INPUT_SIZE, handler.count());
			processEdgesToLocalAndOriginalURIs(processor);
			// handler.mapValueNode.close();
		} catch (RDFParseException | RDFHandlerException | IOException e) {
			throw new IllegalStateException(e);
		}
	}


	/**
	 * Returns a preprocessed URI i.e. schema terms and numbers being removed
	 * 
	 * @param uri the original URI
	 * @return the URI with its schema term removed
	 */
	public static String preprocessURI(String uri) {
		List<String> result = new LinkedList<>(); // special removal in case of a URI;
		for (String chunk : uri.split("[\\!\\@\\#\\$\\%\\^\\&\\*\\-\\_\\/\\:\\.\\+\\=]")) {
			// remove schema term
			if (!schemaTerms.contains(chunk) && !chunk.matches("[0-9]")) {
				result.add(chunk.trim());
			}
		}
		return Joiner.on(" ").join(result);
	}

	@Override
	public String getContext(Graph g, Node n) {
		int N = 5;
		int i = 1;
		// returns N triples having this URI within
		String triples = "";
		Set<Edge> edges = g.getAdjacentEdges(n);
		for (Edge e : edges) {
			if (i++ > N) {
				break; // limit the number of triples to be returned
			}
			Node other = e.getTargetNode().equals(n) ? e.getSourceNode() : e.getTargetNode(); // get the other node
			String triple;

			// if the n is a literal, it can only be an object
			// ignore edges with no label, because that is an edge connecting this literal
			// to its entity child
			if (n.getType() == Node.Types.RDF_LITERAL && !e.getLabel().equals("")) {
				triple = other.getLabel() + " -----" + e.getLabel() + "-----> " + "<b>" + n.getLabel() + "</b>";
				triples += triple + "<br/>";
			} else if (n.getType() == Node.Types.RDF_URI) {
				// if it is a URI, it can be either a subject, or an object
				if (n.equals(e.getSourceNode())) { // if n is the subject
					triple = "<b>" + n.getLabel() + "</b>" + " -----" + e.getLabel() + "-----> " + other.getLabel();
				} else { // else, if it is an object
					triple = other.getLabel() + " -----" + e.getLabel() + "-----> " + "<b>" + n.getLabel() + "</b>";
				}
				triples += triple + "<br/>";
			}
		}
		return triples;
	}
	
	public class TripleHandler implements RDFHandler {

		/** Whether to report progress. */
		private final Consumer<Edge> processor;

		/** The line counter. */
		private final AtomicLong counter = new AtomicLong(0);

		private DataSource ds;

		ValueAtomicities atomicity = ValueAtomicities.valueOf(Config.getInstance().getProperty("value_atomicity"));
		/**
		 * Instantiates a new triple table handler.
		 *
		 * @param p the edge processor
		 */
		public TripleHandler(Consumer<Edge> p, DataSource ds) {
			this.processor = p;
			this.ds = ds;
		}

		@Override
		public void startRDF() throws RDFHandlerException {
			// Noop
		}

		String sLabel = null, oLabel = null;
		Node subjectNode = null, objectNode = null;
		Value object = null;
		boolean storedGraphIsEmpty = false;
		boolean askedIfGraphIsEmpty = false;

		@Override
		public void handleStatement(Statement st) throws RDFHandlerException{
			if (atomicity == ValueAtomicities.PER_GRAPH) {
				handleStatementPerGraph(st);
			}
			else {
				// RDF does not tolerate PER_INSTANCE because it goes against the RDF principles.
				// PER_PATH doesn't make sense as there is not always a unique path leading to a node;
				// moreover, it would also go against the RDF principles.
				//
				// Instead of throwing errors, the class tacitly interprets anything but PER_GRAPH, as: PER_DATASET.
				handleStatementOtherPolicy(st); 
			}
		}

		/** This and the next method handle a local cache for nodes by their labels */
		/*  This de facto implements PER_DATASET */
		private Node getNodeFromLocalCache(Value value) {
			return labelToNode.get(value);
		}
		private void addNodeToLocalCache(Value value, Node node) {
			labelToNode.put(value, node);
		}


		private void handleStatementOtherPolicy(Statement st) throws RDFHandlerException {
			// create a node from the Subject (always a RDFURINode)
			try {
				sLabel = st.getSubject().toString();
				subjectNode = getNodeFromLocalCache(st.getSubject());
				if (subjectNode == null) {
					subjectNode = createNode(st.getSubject());
					log.info(subjectNode);
					addNodeToLocalCache(st.getSubject(), subjectNode);
				}
				// we segment the object only if it is a literal:
				object = st.getObject();
				oLabel = object.toString();
				if (object instanceof org.openrdf.model.URI || object instanceof org.openrdf.model.BNode) {
					// Look in the cache first:
					objectNode = getNodeFromLocalCache(object);
					if (objectNode == null) {
						objectNode = createNode(st.getObject());
						log.info(objectNode);
						addNodeToLocalCache(st.getObject(), objectNode);
					}
					Edge e = buildLabelledEdge(subjectNode, objectNode, st.getPredicate().toString());
					processor.accept(e); //buildLabelledEdge(subjectNode, objectNode, st.getPredicate().toString()));
					counter.incrementAndGet();
				} else { // literals
					String originalLiteral = st.getObject().toString();
					ArrayList<String> literals = new ArrayList<>();
					if (Config.getInstance().getBooleanProperty("split_sentences")) {
						literals.addAll(getSentences(originalLiteral));
					} else {
						literals.add(originalLiteral);
					}
					for (String sentence : literals) {
						objectNode = getNodeFromLocalCache(new LiteralImpl(sentence));
						if (objectNode == null) {
							objectNode = createNode(st.getObject());
							log.info(objectNode);
							addNodeToLocalCache(st.getObject(), objectNode);
						}
						processor.accept(buildLabelledEdge(subjectNode, objectNode, st.getPredicate().toString()));
						counter.incrementAndGet();
					}
				}
			} catch (Exception e) {
				e.printStackTrace(); 
				//log.info("After loading " + counter.get() + " triples got: " + e.getMessage());
				throw new IllegalStateException(e.toString());
			}
		}
		
		/** This goes through the Graph to create new nodes, given that they
		 *  neeed to be graph-wise unique. */
		private void handleStatementPerGraph(Statement st) throws RDFHandlerException {
			//log.info("\nHandle statementPerGraph: " + st);
			// create a node from the Subject (always a RDFURINode)
			try {
				sLabel = st.getSubject().toString(); //the results is different from StringValue(), and we want toString!! #318;
				long t1 = System.currentTimeMillis();
				subjectNode = graph.getNodeFromCacheOrStorage(sLabel);
				long t2 = System.currentTimeMillis();
				//log.info("t2-t1:  " + (t2-t1));
				graphStats.increment(ds.getLocalURI(), NODE_LOOKUP_T, (new Long(t2-t1)).intValue());
				graphStats.increment(StatisticsCollector.total(), NODE_LOOKUP_T, (new Long(t2-t1)).intValue());
			    
				//log.info("subjectNode "+subjectNode.getLabel());
				if (subjectNode == null) {
					subjectNode = createNode(st.getSubject());
					graph.addToNodeCache(sLabel, subjectNode);
					//log.info("Created subject " + subjectNode + " on " + sLabel + " and cached it");
				}
				// we segment the object only if it is a literal:
				object = st.getObject();
				oLabel = object.toString();

				if (object instanceof org.openrdf.model.URI || object instanceof org.openrdf.model.BNode) {
					// Look in the cache first:
					long t3 = System.currentTimeMillis();
					objectNode = graph.getNodeFromCacheOrStorage(oLabel);
					long t4 = System.currentTimeMillis();
					graphStats.increment(ds.getLocalURI(), NODE_LOOKUP_T, (new Long(t4-t3)).intValue());
					graphStats.increment(StatisticsCollector.total(), NODE_LOOKUP_T, (new Long(t4-t3)).intValue());
					//log.info("t4-t3:  " + (t4-t3));
					if (objectNode == null) {
						objectNode = createNode(st.getObject());
						graph.addToNodeCache(oLabel, objectNode);
						// log.info("Created subject " + subjectNode + " on " + sLabel + " and
						// cached it");
					}

					Edge edge = buildLabelledEdge(subjectNode, objectNode, st.getPredicate().toString());
					edge.setDataSource(this.ds);
					processor.accept(edge);
					counter.incrementAndGet();
				} else { // literals
					String originalLiteral = st.getObject().toString();
					ArrayList<String> literals = new ArrayList<String>();
					if (Config.getInstance().getBooleanProperty("split_sentences")) {
						literals.addAll(getSentences(originalLiteral));
					} else {
						literals.add(originalLiteral);
					}
					for (String sentence : literals) {
						objectNode = graph.getNodeFromCacheOrStorage(sentence);
						//log.info("objectNode "+objectNode);
						if (objectNode == null) {
							objectNode = createNode(st.getObject());
							graph.addToNodeCache(sentence, objectNode);
							//log.info("Created object " + objectNode + " on " + oLabel + " and cached it");
						}
						Edge edge = buildLabelledEdge(subjectNode, objectNode, st.getPredicate().toString());
						edge.setDataSource(this.ds);
						processor.accept(edge);
						counter.incrementAndGet();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.info("After loading " + counter.get() + " triples got: " + e.getMessage());
				log.info("Offending triple is:" + st);
				throw new IllegalStateException(e.toString());
			}
			if (counter.get() % 50000 == 0) {
				log.info("Handled " + counter + " triples "); 
			}
		}

		@Override
		public void handleNamespace(String prefix, String uri) throws RDFHandlerException {
			// TODO
		}

		@Override
		public void handleComment(String comment) throws RDFHandlerException {
			// Ignore
		}

		@Override
		public void endRDF() throws RDFHandlerException {
			// noop
		}

		public long count() {
			return this.counter.get();
		}

		/**
		 * Creates a node out of the given RDF value. All checks on whether we should really create this node have
		 * been done before.
		 * 
		 * @param value the URI/Literal to check in the list of values it must be
		 *              surrounded in "..." // * @param isURI to distinguish URI(true)
		 *              or Literal(false)
		 * @return the node ID either newly created for unseen node (RDFURINode or
		 *         RDFLiteralNode)
		 */
		private Node createNode(Value value) {
			String val = value.toString();
			boolean isURI = false;
			if (value instanceof org.openrdf.model.URI) {
				isURI = true;
			} else if (value instanceof BNode) {
				val = localURI + "#" + ((BNode) value).getID();
				isURI = true;
			}

			Node.Types nodeType = (isURI ? RDF_URI : RDF_LITERAL);
			return buildLabelNodeOfType(val, nodeType);
		}
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Package containing classes and sub-packages related to indexing.
 */
package fr.inria.cedar.connectionlens.indexing;
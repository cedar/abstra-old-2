package fr.inria.cedar.connectionlens.abstraction.classification;

import java.util.ArrayList;

public class ClassificationCategory {
    int id;
    String label;
    String informativeLabel;
    boolean isDefaultCategory;
    ArrayList<Integer> subCategories;

    public ClassificationCategory(int id, String label, boolean isDefaultCategory) {
        this.id = id;
        this.label = label;
        this.informativeLabel = Classification.camelToSnake(this.label.replaceAll("CATEGORY", "").replaceAll("_", " "));
        this.isDefaultCategory = isDefaultCategory;
    }

    public ClassificationCategory(int id, String label, boolean isDefaultCategory, ArrayList<Integer> subClasses) {
        this.id = id;
        this.label = label;
        this.informativeLabel = Classification.camelToSnake(this.label.replaceAll("CATEGORY", "").replaceAll("_", " "));
        this.isDefaultCategory = isDefaultCategory;
        this.subCategories = subClasses;
    }

    public int getId() {
        return this.id;
    }

    public String getLabel() {
        return this.label;
    }

    public String getInformativeLabel() {
        return this.informativeLabel;
    }

    public boolean isDefaultCategory() {
        return this.isDefaultCategory;
    }

    public ArrayList<Integer> getSubCategories() {
        return this.subCategories;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setInformativeLabel(String informativeLabel) {
        this.informativeLabel = informativeLabel;
    }

    public void setDefaultCategory(boolean defaultCategory) {
        this.isDefaultCategory = defaultCategory;
    }

    public void setSubCategories(ArrayList<Integer> subCategories) {
        this.subCategories = subCategories;
    }

//    public String print() {
//        return "ClassificationCategory{" +
//                "id=" + this.id +
//                ", label='" + this.label + '\'' +
//                ", informativeLabel='" + this.informativeLabel + '\'' +
//                ", isDefaultCategory=" + this.isDefaultCategory +
//                '}';
//    }

    @Override
    public String toString() {
        return this.informativeLabel;
    }
}

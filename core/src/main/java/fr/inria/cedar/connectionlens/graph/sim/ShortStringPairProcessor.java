/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static com.google.common.collect.Range.lessThan;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isString;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofRelativeLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.withTypes;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameNormaLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.SOURCE;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.TARGET;
import static java.lang.Math.max;

import com.wcohen.ss.Levenstein;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class ShortStringPairProcessor extends CachedSimilarPairProcessor {

	/** The maximum string length for which to apply this similarity */
	private final int lengthThreshold;

	/** The length to between candidate pairs */
	private final double lengthRatio;

	private final Levenstein sim = new Levenstein();
	
	/** If true, we need to find identical-label strings */
	private final boolean requireEquality; 
	
	public ShortStringPairProcessor(StatisticsCollector stats, double th) {
		this(stats, th,
			Config.getInstance().getIntProperty("long_string_threshold"),
			Config.getInstance().getDoubleProperty("length_difference_ratio"),
			Config.getInstance().getIntProperty("matching_prefix_length"), 
			false);
	}
	public ShortStringPairProcessor(StatisticsCollector stats, double th, boolean requireEquality) {
		this(stats, th,
			Config.getInstance().getIntProperty("long_string_threshold"),
			Config.getInstance().getDoubleProperty("length_difference_ratio"),
			Config.getInstance().getIntProperty("matching_prefix_length"), 
			requireEquality);
	}
	
	public ShortStringPairProcessor(StatisticsCollector stats,
			double th, int lengthThreshold, double lengthRatio, int prefixLength, boolean requireEquality) {
		super(stats, th);
		this.lengthThreshold = lengthThreshold;
		this.lengthRatio = lengthRatio;
		this.requireEquality = requireEquality;
	}

	@Override
	public Double apply(String a, String b) {
		if(requireEquality) { // in this case the selector already did the work
			return 1.0;
		}
		else{
			return 1-Math.abs(sim.score(a, b))/max(a.length(), b.length());
		}
	}

	/**
	 * This method returns the selector for short strings, that is:
	 * the logical condition that determines who is a potential partner to be compared with 
	 * short strings. 
	 */
	@Override
	public NodePairSelector selector() {
		NodePairSelector result =
			withTypes(SOURCE, RDF_LITERAL, JSON_VALUE, RELATIONAL_VALUE, HTML_VALUE, XML_TAG_VALUE, XML_ATTRIBUTE_VALUE, TEXT_VALUE)
			.and(withTypes(TARGET, RDF_LITERAL, JSON_VALUE, RELATIONAL_VALUE, HTML_VALUE, XML_TAG_VALUE, XML_ATTRIBUTE_VALUE, TEXT_VALUE))
			.and(isString(BOTH))
			.and(ofRelativeLength(lengthRatio))
			.and(ofAbsoluteLength(BOTH, lessThan(lengthThreshold)));
		if(requireEquality) {
			result = sameNormaLabel().and(result); 
		}
		//else {
			result = sameStoredPrefix().and(result); 
		//}
		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	public boolean requireEqual() {
		return requireEquality; 
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.google.common.collect.Multimap;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.FirstNameDictionary;
import fr.inria.cedar.connectionlens.extraction.PartOfSpeech;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON; 
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * Separated these from the other EntityOccurrenceNodes because they require
 * special handling - of their label components (first name, last name etc.) -
 * of their normalized label - of their label prefix - of their comparison
 * function
 * 
 * @author ioanamanolescu
 *
 */
public class PersonOccurrenceNode extends EntityOccurrenceNode {
	String entityString;
	private Multimap<String, PartOfSpeech> labelPOS;

	/** The logger. */
	protected static final Logger log = Logger.getLogger(PersonOccurrenceNode.class);

	public static final Pattern englishFirstNameInitialsPattern = Pattern.compile("^[A-Z](\\.| )?(-)?([A-Z](\\.)?)?$");
	public static final Pattern frenchFirstNameInitialsPattern = Pattern
			.compile("^([A-Z]|Ph)(\\.| )?(-)?(([A-Z]|Ph)(\\.)?)?$");

	private static final long serialVersionUID = 8009426805657456679L;

	/**
	 * We need two constructors. The first is used when the node is first created
	 * from a dataset. At this point, it needs to create its normalized label; it
	 * does so by first, filling in labelComponents1 and labelComponents2.
	 * 
	 * @param id
	 * @param label
	 * @param d
	 * @param rep
	 * @param offset
	 * @param length
	 */
	public PersonOccurrenceNode(NodeID id, String label, DataSource d, Supplier<Node> rep, int offset, int length,
			String... normaLabel) {
		super(id, label, d, rep, offset, length);
		extractFirstAndLastNames();
		if (normaLabel.length > 0) {
			this.normalizedLabel = normaLabel[0];
		} else {
			//this.normalizedLabel = getNormalizedLabel();
			specificNormalization(); 
		}
	}
	/** Self-represented. To be used only when reading from a loaded database */
	public PersonOccurrenceNode(NodeID id, String label, DataSource d, int offset, int length,
			String normaLabel) {
		super(id, label, d, offset, length, ENTITY_PERSON);
		extractFirstAndLastNames(); // TODO: maybe not needed?
		this.normalizedLabel = normaLabel; 
	}

	public static boolean isInitials(String label) {
		Matcher matchInitials = null;
		if (Config.getInstance().getProperty("default_locale").equals("fr")) {
			// log.info("Matching " + label + " as a first name");
			matchInitials = frenchFirstNameInitialsPattern.matcher(label);
		} else {
			matchInitials = englishFirstNameInitialsPattern.matcher(label);
		}
		return matchInitials.find();
	}

	public static boolean isCivility(String label) {
		Matcher matchCivility = null;
		if (Config.getInstance().getProperty("default_locale").equals("fr")) {
			// log.info("Matching " + label + " as a first name");
			matchCivility = (Pattern.compile("^" + Node.frenchCivilityPattern + "$")).matcher(label.toLowerCase());
		} else {
			matchCivility = (Pattern.compile("^" + Node.englishCivilityPattern + "$")).matcher(label.toLowerCase());

		}
		return matchCivility.find();
	}

	/**
	 * Extracts first and last names. It uses the first name dictionary to detect
	 * first names. It does not need to detect a civility particle because those
	 * should have been removed previously.
	 */
	private void extractFirstAndLastNames() {
		//log.info("\nExtracting first and last name from: " + label);
		int firstCapitalPosition = -1;
		int secondCapitalPosition = -1;
		int capitalCount = 0;
		String labelToUse = label;
		if (this.labelComponents1 == null) {
			this.labelComponents1 = new ArrayList<String>(); 
		}
		if (this.labelComponents2 == null) {
			this.labelComponents2 = new TreeSet<String>();
		}
		// the next two loops try to identify cases such as RichardFerrand or
		// Richard_Ferrand:
		ArrayList<Integer> capitalPositions = new ArrayList<Integer>();
		for (int i = 0; i < labelToUse.length(); i++) {
			if (('A' <= labelToUse.charAt(i)) && (labelToUse.charAt(i) <= 'Z')) {
				capitalPositions.add(i);
				if (firstCapitalPosition < 0) {
					firstCapitalPosition = i;
				} else { // set secondCapitalPosition iff this second capital is not too close to the
							// first
					if (secondCapitalPosition < 0) {
						if ((firstCapitalPosition != (i - 1)) && // OConnor
								(firstCapitalPosition != (i - 2))
								|| isLetter(labelToUse.charAt(firstCapitalPosition + 1))) // O'Connor
							secondCapitalPosition = i;
						capitalCount++;
					}
				}
			}
		}

		// .. and insert a whitespace (or replace the non-letter separator such as _
		// etc. with a whitespace)
		if (capitalCount >= 2 && capitalCount < labelToUse.length()) { // && capitalGroupCounts >= 2) {
			String first = labelToUse.substring(0, secondCapitalPosition);
			String second = labelToUse.substring(secondCapitalPosition, labelToUse.length());
			if (first.charAt(first.length() - 1) != ' ') {
				// log.info( "Found 2 capitals, just before the second we have: " +
				// first.charAt(first.length() - 1));
				if (('a' <= first.charAt(first.length() - 1)) && (first.charAt(first.length() - 1) <= 'z')) {
					labelToUse = first + " " + second;
				} else { // there is an intermediary character before the second capital
					first = first.substring(0, first.length() - 1);
					labelToUse = first + " " + second;
				}
			}
		}
		//log.info("Label to use 1: " + labelToUse);
		// Normalization preprocessing should take out M., Mr., Dr., Pr. etc.
		labelToUse = entityLabelPreprocessing(labelToUse);
		//log.info("Label to use 2: " + labelToUse);
		String lastEncounteredFirstName = null;
		//for (PartOfSpeech token : labelPOS.values()) {
		for (String tValue: labelToUse.split(" ")) {
			//String tValue = token.value();
			if (FirstNameDictionary.isFirstName(tValue) || isInitials(tValue)) {
				//log.info(tValue + " in components 1 (first name)");
				this.labelComponents1.add(tValue);
				lastEncounteredFirstName = tValue;
			} else {
				//log.info(tValue + " in components 2 (last name)");
				this.labelComponents2.add(tValue); //token.value());
			}
		}

		if (labelComponents2.size() == 0) { // no family name has been recognized
			if (labelComponents1.size() > 1) { // but two or more first names are recognized, e.g., "Edouard Philippe"
				labelComponents2.add(lastEncounteredFirstName);
				labelComponents1.remove(lastEncounteredFirstName);
				//log.info("Last name rescued: " + lastEncounteredFirstName);
			}
			// in some texts, e.g., family stories, we may have just first names
		} else {
			if (labelComponents1.size() == 0) {
				// no first name has been recognized, e.g., language not in dictionary
				if (labelComponents2.size() > 1) {
					// but there are at least 2 last names, then take the first encountered to be a
					// first name
					String rescuedFirstName = findFirstOccurrenceOf(labelComponents2, label);
					if (rescuedFirstName != null) {
						labelComponents1.add(rescuedFirstName);
						labelComponents2.remove(rescuedFirstName);
						// in some cases a "D. " is turned into "d" (no dot) in labelPOS, then "d" is
						// considered a last name AND "d." a first name,
						// and the removal of "d" from the last name does not work. Fix below:
						if (isInitials(rescuedFirstName.toUpperCase())) {
							// log.info("Removing " + rescuedFirstName.subSequence(0,
							// rescuedFirstName.length() - 1) +
							// " from last name");
							labelComponents2.remove(rescuedFirstName.subSequence(0, rescuedFirstName.length() - 1));
							// log.info("Removing " + rescuedFirstName.replaceAll("\\.", "") +
							// " from last name");
							labelComponents2.remove(rescuedFirstName.replaceAll("\\.", ""));
						}
					}
				}
			}
		}
		//log.info("UNDERSTOOD FROM " + label + " first: " + labelComponents1 + " last: " + labelComponents2);
	}

	/**
	 * Return the first component of the label (from left to right) that appears in
	 * components. The goal is to rescue as first name, the piece of the name that
	 * appeared first (and was considered last name).
	 */
	private String findFirstOccurrenceOf(TreeSet<String> components, String label) {
		String[] labelComponents = label.split(" ");
		for (String labelComp : labelComponents) {
			if (components.contains(labelComp.toLowerCase())) {
				return labelComp.toLowerCase();
			}
		}
		return null;
	}

	private boolean isLetter(char c) {
		if ('a' <= c && c <= 'z') {
			return true;
		}
		if ('A' <= c && c <= 'Z') {
			return true;
		}
		return false;
	}

	public Multimap<String, PartOfSpeech> getLabelPOSEntity() {
		return labelPOS;
	}

	public ArrayList<String> getlabelComponents1() {
		return labelComponents1;
	}

	public ArrayList<String> getFirstNames() {
		return labelComponents1;
	}

	public Collection<String> getlabelComponents2() {
		return (Collection<String>) labelComponents2;
	}

	public Collection<String> getLastNames() {
		return (Collection<String>) labelComponents2;
	}

	public String getLastName() {
		// log.info(this.getClass().getSimpleName() + " getLastName, there are " +
		// labelComponents2.size() + " last name components");
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (String ln : labelComponents2) {
			sb.append(ln);
			if (i < labelComponents2.size() - 1) {
				sb.append(" ");
			}
			i++;
		}
		return new String(sb);
	}

	@Override
	void specificNormalization() {
		if (labelComponents1.size() + labelComponents2.size() > 0) {
			StringBuffer sb = new StringBuffer();
			for (String fn : labelComponents1) {
				sb.append(fn + " ");
			}
			for (String ln : labelComponents2) {
				sb.append(ln + " ");
			}
			this.normalizedLabel = (new String(sb)).trim();
			//log.info("PersonOccurrence: Normalized " + label + " into: " +
			// normalizedLabel);
		}
	}

	@Override
	/** This method is one of the main interests for having PersonOccurrenceNodes:
	 * it allows a personalized way of carving the stored prefix */
	public String getLabelPrefix() {
		String toCutFrom = "";
		if (labelComponents2.size() > 0) {
			toCutFrom = labelComponents2.first();
		} else {
			if (labelComponents1.size() > 0) {
				toCutFrom = labelComponents1.get(0);
			}
		}
		if (requiredPrefixLength < 0) {
			return toCutFrom;
		}
		return (toCutFrom.length() >= requiredPrefixLength ? toCutFrom.substring(0, requiredPrefixLength) : toCutFrom).toLowerCase();
	}

}

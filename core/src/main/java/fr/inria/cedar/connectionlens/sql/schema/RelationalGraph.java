/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql.schema;

import static com.google.common.collect.BoundType.OPEN;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Range.greaterThan;
import static fr.inria.cedar.connectionlens.extraction.EntityType.ENTITY_TYPES;
import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.Edge.SAME_AS_LABEL;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.*;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.base.Joiner;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.ExtractionPolicy;
import fr.inria.cedar.connectionlens.extraction.FirstNameDictionary;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Item.Types;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.NodePairSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.AbsoluteLengthSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.CommonPrefixSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.CommonWordsSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.ConjunctiveSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.DisjunctiveSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.IsBooleanSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.IsDateTimeSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.IsNumericSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.IsStringSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.IsURISelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.LabelSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.MinHashLSHSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.NotSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.RelativeLengthSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.SameNormaLabelSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.SameStoredPrefixSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.TypeSelector;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.indexing.IndexingModels;
import fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndex;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSource.ValueAtomicities;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.source.RDFDataSource;
import fr.inria.cedar.connectionlens.sql.RelationalStructure;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraphSession.BatchSession;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraphSession.BufferedSession;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraphSession.CopyBatchSession;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraphSession.SequentialSession;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.StatisticsLogger;

/**
 * Relational implementation of the virtual Graph interface.
 *
 * @author Julien Leblay, Ioana Manolescu
 */
public class RelationalGraph extends RelationalStructure implements Graph {

	/**
	 * This stores in memory the nodes about which we already know they are stored
	 * on disk. They could have been stored there: - by a spill in the current run
	 * (RelationalGraphSession.[Copy]BatchSession.spillNodes, prepareNode) - in a
	 * previous loading (and we looked them up during this loading)
	 * (getNodeFromCacheOrStorage)
	 */
	HashSet<NodeID> nodesAlreadyOnDisk = new HashSet<>();
	/**
	 * This is a cache of the nodes that have been created so far during this
	 * loading, whether they have already been spilled to disk, or not.
	 * 
	 * This is NOT the node ID collection which, when full, triggers spilling. That
	 * is RelationalGraphSession.nodeBuffer.
	 */
	HashMap<String, Node> nodesUsedInThisLoading = new HashMap<>();
	/**
	 * This parameter controls how many nodes we try to keep in memory. This could
	 * have been an independent parameter, or could have been exactly the batch
	 * size. The reason for setting it to 4 * batch size is: - the same batch size
	 * is used for nodes and (separately) for edges; - each edge has 2 nodes,
	 * therefore one edge counts up to three times, once for each node and once for
	 * the edge; - (if there is also extraction, many other nodes can appear...)
	 * Things are not so clear cut of course; when different spills occur also
	 * depends on the graph density etc.
	 * 
	 * So, this setting is a pragmatic choice, which seems to work OK for now.
	 */
	static final int MAX_NODES_IN_GRAPH_MEMORY = 4 * Config.getInstance().getIntProperty("update_batch_size");
	// used to inform the user only once that the node cache size has been reached.
	boolean cacheFullNotified = false;

	HashMap<NodeID, String> disambiguatedEntitiesNode = new HashMap<>();

	/** The logger. */
	private static final Logger log = Logger.getLogger(RelationalGraph.class);

	/** index of the graph */
	private IndexAndProcessNodes index;

	/** extractor **/
	private EntityExtractor extractor;

	private MorphoSyntacticAnalyser mas;

	private StatisticsLogger statsLog;

	private Boolean indexLater;

	/** Whether to require a common prefix or not */
	static final boolean COMPARE_STORED_LABEL_PREFIX = Config.getInstance().getBooleanProperty("compare_stored_label_prefix");

	/** The node cache size. */
	static final int NODE_CACHE_SIZE = Config.getInstance().getIntProperty("node_cache");

	/** The node cache. */
	final Cache<NodeID, Node> nodeCache = CacheBuilder.newBuilder().recordStats().maximumSize(NODE_CACHE_SIZE)
			.initialCapacity(NODE_CACHE_SIZE / 5).build();

	/** The node cache by label **/
	final Cache<String, Node> nodeCacheByLabel = CacheBuilder.newBuilder().recordStats().maximumSize(NODE_CACHE_SIZE)
			.initialCapacity(NODE_CACHE_SIZE / 2).build();

	@Override
	public Cache<NodeID, Node> nodeCache() {
		return this.nodeCache;
	}

	@Override
	public int nodeCacheSize() {
		return NODE_CACHE_SIZE;
	}


	/** The same as edges cache size. */
	static final int SAME_AS_CACHE_SIZE = Config.getInstance().getIntProperty("same_as_cache");

	/** The same as edges cache. */
	final Cache<NodeID, Set<Edge>> sameAsCache = CacheBuilder.newBuilder().recordStats().maximumSize(SAME_AS_CACHE_SIZE)
			.initialCapacity(SAME_AS_CACHE_SIZE / 5).build();

	@Override
	public Cache<NodeID, Set<Edge>> sameAsCache() {
		return this.sameAsCache;
	}

	@Override
	public int sameAsCacheSize() {
		return SAME_AS_CACHE_SIZE;
	}

	/** The edge cache size. */
	static final int EDGE_CACHE_SIZE = Config.getInstance().getIntProperty("edge_cache");

	/** The edge cache. */
	final Cache<EdgeID, Edge> edgeCache = CacheBuilder.newBuilder().recordStats().maximumSize(EDGE_CACHE_SIZE)
			.initialCapacity(EDGE_CACHE_SIZE / 5).build();

	@Override
	public Cache<EdgeID, Edge> edgeCache() {
		return this.edgeCache;
	}

	@Override
	public int edgeCacheSize() {
		return EDGE_CACHE_SIZE;
	}

	/** The specificity cache size. */
	static final int SPEC_CACHE_SIZE = Config.getInstance().getIntProperty("specificity_cache");

	/** The in-degree cache. */
	public final Cache<Pair<Node, String>, Specificity> inDegreeCache = CacheBuilder.newBuilder().recordStats()
			.maximumSize(SPEC_CACHE_SIZE).initialCapacity(SPEC_CACHE_SIZE / 5).build();

	@Override
	public Cache<Pair<Node, String>, Specificity> inDegreeCache() {
		return inDegreeCache;
	}

	/** The out-degree cache. */
	public final Cache<Pair<Node, String>, Specificity> outDegreeCache = CacheBuilder.newBuilder().recordStats()
			.maximumSize(SPEC_CACHE_SIZE).initialCapacity(SPEC_CACHE_SIZE / 5).build();

	@Override
	public Cache<Pair<Node, String>, Specificity> outDegreeCache() {
		return outDegreeCache;
	}

	/** The adjacency cache size. */
	private int ADJACENCY_CACHE_SIZE = Config.getInstance().getIntProperty("adjacency_cache");

	/** The incoming edge cache. */
	public Cache<Pair<Node, Set<Item.Types>>, Set<Edge>> incomingCache = CacheBuilder.newBuilder().recordStats()
			.maximumSize(ADJACENCY_CACHE_SIZE).initialCapacity(ADJACENCY_CACHE_SIZE / 5).build();

	/** The outgoing edge cache. */
	public Cache<Pair<Node, Set<Item.Types>>, Set<Edge>> outgoingCache = CacheBuilder.newBuilder().recordStats()
			.maximumSize(ADJACENCY_CACHE_SIZE).initialCapacity(ADJACENCY_CACHE_SIZE / 5).build();


	@Override
	public Connection getConnection() {
		return this.conn; 
	}

	protected String labelConstraint(String[] labels) {
		if (labels.length > 0) {
			return "AND label IN ('"
					+ Arrays.stream(labels).map(l -> String.valueOf(l)).collect(Collectors.joining("','"))
					+ "')";
		}
		return "";

	}

	protected String typeConstraint(Item.Types... types) {
		int actualTypeVectorLength = 0;
		Set<Types> alreadySeen = new HashSet<Types>();
		for (int i = 0; i < types.length; i++) {
			if (!alreadySeen.contains(types[i])) {
				alreadySeen.add(types[i]);
				if (types[i] == EXTRACTION_EDGE) {
					actualTypeVectorLength += 4; // IM 23/2/22, to get rid of generics
				} else {
					actualTypeVectorLength++;
				}
			}
		}
		if (actualTypeVectorLength != types.length) {
			Types[] realTypes = new Types[actualTypeVectorLength];
			int j = 0;
			for (int i = 0; i < types.length; i++) {
				if (types[i] == EXTRACTION_EDGE) {
					realTypes[j] = EXTRACTED_PERSON;
					j++;
					realTypes[j] = EXTRACTED_ORGANIZATION;
					j++;
					realTypes[j] = EXTRACTED_LOCATION;
					j++;
					realTypes[j] = EXTRACTED_FIRSTNAME;
					j++;
				} else {
					realTypes[j] = types[i];
					j++;
				}
			}
			return typeConstraint(realTypes);
		} else {
			if (types.length > 0) {
				return "AND type IN ("
						+ Arrays.stream(types).map(t -> String.valueOf(t.ordinal())).collect(Collectors.joining(","))
						+ ")";
			}
			return "";

		}
	}

	public GraphSession openSession(int batchSize) {
		StatisticsCollector graphStats = new StatisticsCollector();
		StatisticsCollector extractStats = new StatisticsCollector();
		StatisticsCollector simStats = new StatisticsCollector();
		if (batchSize <= 0) {
			return new BatchSession(this, graphStats, extractStats, simStats);
		}
		if (batchSize == 1) {
			return new SequentialSession(this, graphStats, extractStats, simStats);
		}
		return new BufferedSession(this, batchSize, graphStats, extractStats, simStats);
	}

	@Override
	public GraphSession openSession(int batchSize, StatisticsCollector graphStats, StatisticsCollector extractStats,
			StatisticsCollector simStats) {
		if (batchSize <= 0) {
			return new BatchSession(this, graphStats, extractStats, simStats);
		}
		if (batchSize == 1) {
			return new SequentialSession(this, graphStats, extractStats, simStats);
		}
		return new BufferedSession(this, batchSize, graphStats, extractStats, simStats);
	}

	@Override
	public GraphSession openSession(int batchSize, Function<String, int[]> f, StatisticsCollector graphStats,
			StatisticsCollector extractStats, StatisticsCollector simStats) {
		if (batchSize <= 0) {
			return new BatchSession(this, f, graphStats, extractStats, simStats);
		}
		if (batchSize == 1) {
			return new SequentialSession(this, f, graphStats, extractStats, simStats);
		}
		if (ValueAtomicities
				.valueOf(Config.getInstance().getProperty("value_atomicity")) == ValueAtomicities.PER_GRAPH) {
			return new CopyBatchSession(this, f, batchSize, graphStats, extractStats, simStats);
		}
		return new BufferedSession(this, f, batchSize, graphStats, extractStats, simStats);
	}

	/** The data source catalog. */
	private DataSourceCatalog catalog;

	/** The item ID factory */
	private final Factory factory;

	/**
	 * An integer specifying the type of graph we are dealing with:
	 *   - 0: original/normal graph
	 *   - 1: abstract graph
	 *   - 2: normalized graph
	 *   - 3: summarised graph
	 *   - 4: classified graph
	 */
	private final GraphType thisGraphType;

	/**
	 * The name of the tables where information is stored in the database
	 */
	private final String nodesTableName;
	private final String edgesTableName;
	private final String biDirEdgesTableName;
	private final String sameAsEdgesTableName;
	private final String specificityTableName;
	private final String computedSpecificityTableName;
	private final String weakSameAsEdgesTableName;
	private final String disambiguatedTableName;
	private final String specStatTableName;

	PreparedStatement getInOutFromSpecificityStmt = null;
	PreparedStatement resolveNodeStmt = null;
	PreparedStatement resolveSameAsEdgeStmt = null;
	PreparedStatement resolveEdgeStmt = null;

	private RelationalGraph abstractGraph;
	private RelationalGraph normalizedGraph;
	private RelationalGraph summaryGraph;
	private RelationalGraph classifiedGraph;

	int nodeIdCounter = 0;

	/** for extraction policy */
	private ExtractionPolicy extractPolicy;

	/**
	 * Instantiates a new relational graph.
	 *
	 * @param c the data source catalog
	 */

	public RelationalGraph(Factory f, DataSourceCatalog c, GraphType typeOfGraph) {
		this.catalog = c;
		this.factory = f;
		this.thisGraphType = typeOfGraph;
		this.abstractGraph = null;
		if (this.isOriginal()) {
			// original graph
			this.nodesTableName = SchemaTableNames.ORIGINAL_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME;
			this.biDirEdgesTableName = SchemaTableNames.ORIGINAL_BIDIREDGES_TABLE_NAME;
			this.sameAsEdgesTableName = SchemaTableNames.ORIGINAL_SAME_AS_EDGES_TABLE_NAME;
			this.weakSameAsEdgesTableName = SchemaTableNames.ORIGINAL_WEAK_SAME_AS_TABLE_NAME;
			this.specificityTableName = SchemaTableNames.ORIGINAL_SPECIFICITY_TABLE_NAME;
			this.computedSpecificityTableName = SchemaTableNames.ORIGINAL_SPECIFICITY_EDGE_TABLE_NAME;
			this.specStatTableName = SchemaTableNames.ORIGINAL_SPEC_STAT_TABLE_NAME;
			this.disambiguatedTableName = SchemaTableNames.ORIGINAL_DISAMBIGUATED_TABLE_NAME;
		} else if(this.isNormalized()) {
			// normalized graph
			this.nodesTableName = SchemaTableNames.NORMALIZED_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME;
			this.biDirEdgesTableName = null;
			this.sameAsEdgesTableName = null;
			this.weakSameAsEdgesTableName = null;
			this.specificityTableName = null;
			this.computedSpecificityTableName = null;
			this.specStatTableName = SchemaTableNames.NORMALIZED_SPEC_STAT_TABLE_NAME;
			this.disambiguatedTableName = null;
		} else {
			// abstract graph for GUI -- TODO NELLY: only define originalGraph and abstractGraph (and change table names accordingly)
			this.nodesTableName = SchemaTableNames.CLASSIFIED_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.CLASSIFIED_EDGES_TABLE_NAME;
			this.biDirEdgesTableName = SchemaTableNames.CLASSIFIED_BIDIREDGES_TABLE_NAME;
			this.sameAsEdgesTableName = SchemaTableNames.CLASSIFIED_SAME_AS_EDGES_TABLE_NAME;
			this.weakSameAsEdgesTableName = SchemaTableNames.CLASSIFIED_WEAK_SAME_AS_TABLE_NAME;
			this.specificityTableName = SchemaTableNames.CLASSIFIED_SPECIFICITY_TABLE_NAME;
			this.computedSpecificityTableName = SchemaTableNames.CLASSIFIED_SPECIFICITY_EDGE_TABLE_NAME;
			this.specStatTableName = SchemaTableNames.CLASSIFIED_SPEC_STAT_TABLE_NAME;
			this.disambiguatedTableName = SchemaTableNames.CLASSIFIED_DISAMBIGUATED_TABLE_NAME;
		}

		resolveNodeStmt = getPreparedStatement(
				"SELECT n.id, n.representative, n.ds, n.label, n.normalabel, n.type FROM " + nodesTableName + " n WHERE n.id = ?;");
		resolveEdgeStmt = getPreparedStatement(" SELECT l.source, l.target, l.label, l.confidence, l.type FROM "
				+ edgesTableName + "  l WHERE l.id IN (?) ");

		if(this.isOriginal() || this.isAbstract()) {
			getInOutFromSpecificityStmt = getPreparedStatement("SELECT s.nin, s.nout FROM " + specificityTableName
					+ " s WHERE s.representative = ? AND s.edge_label = ?");
			resolveSameAsEdgeStmt = getPreparedStatement(" SELECT l.node1, l.node2, '" + SAME_AS_LABEL
					+ "', w.confidence FROM " + sameAsEdgesTableName + " w " + " WHERE w.id IN (?)");
		}
		this.extractPolicy = createExtractionPolicy();
	}

	/**
	 * @param f the factory of the graph.
	 * @param c the catalog of the inputs of the graph.
	 * @param thisGraphType the type of graph (ORIGINAL, NORMALIZED, SUMMARY, ....)
	 * @param extractor the entity extractor
	 * @param mas the morphosyntactic analyser
	 * @param stats the statistics logger
	 * @param indexLater true if to index nodes and edges later (in case of multiple loading)
	 */
	public RelationalGraph(Factory f, DataSourceCatalog c, GraphType thisGraphType, EntityExtractor extractor,
			MorphoSyntacticAnalyser mas, StatisticsLogger stats, Boolean indexLater) {

		this(f, c, thisGraphType);
		this.extractor = extractor;
		this.mas = mas;
		this.statsLog = stats;
		this.indexLater = indexLater;
		this.index = buildIndex(getFactory(), catalog);
	}

	/**
	 * @param f the factory of the graph.
	 * @param c the catalog of the inputs of the graph.
	 * @param thisGraphType the type of graph (ORIGINAL, NORMALIZED, SUMMARY, ....)
	 * @param extractor the entity extractor
	 * @param mas the morphosyntactic analyser
	 * @param stats the statistics logger
	 */
	public RelationalGraph(Factory f, DataSourceCatalog c, GraphType thisGraphType, EntityExtractor extractor,
			MorphoSyntacticAnalyser mas, StatisticsLogger stats) {
		this(f, c, thisGraphType);
		this.extractor = extractor;
		this.mas = mas;
		this.statsLog = stats;
		this.indexLater = false;
		this.index = buildIndex(getFactory(), catalog);
	}

	/**
	 * @param f the factory of the graph.
	 * @param c the catalog of the graph.
	 * @param extractor the entity extractor of the graph.
	 * @param mas the morphosyntactic analyser.
	 * @param stats the statistics logger.
	 * @param indexLater true to index nodes and edges later (in case of multiple loading), false else.
	 */

	public RelationalGraph(Factory f, DataSourceCatalog c, EntityExtractor extractor, MorphoSyntacticAnalyser mas,
			StatisticsLogger stats, Boolean indexLater) {
		this(f, c, GraphType.ORIGINAL_GRAPH);
		this.extractor = extractor;
		this.mas = mas;
		this.statsLog = stats;
		this.indexLater = indexLater;
		this.index = buildIndex(getFactory(), catalog);
	}

	/**
	 * builds the graph's index
	 * @param factory the graph's factory.
	 * @param catalog the graph's catalog.
	 * @return
	 */
	private IndexAndProcessNodes buildIndex(Factory factory, DataSourceCatalog catalog) {
		final StatisticsCollector extractStats = statsLog.getStatisticsCollector(EXTRACTION);
		final StatisticsCollector indexStats = statsLog.getStatisticsCollector(INDEX);
		final StatisticsCollector abstractStats = statsLog.getStatisticsCollector(ABSTRACTION_WORK);
		IndexingModels im = IndexingModels.valueOf(Config.getInstance().getProperty("indexing_model"));
		Locale l = Locale.forLanguageTag(Config.getInstance().getProperty("default_locale"));
		switch (im) {
		case LUCENE:
			return new LuceneBasedIndex(l, catalog, factory, this::resolveNode, this::resolveEdge,
					this::getAdjacentEdges, extractor, mas, indexStats, extractStats, abstractStats, this.thisGraphType);
		default: // POSTGRES_FULLTEXT:
			return new PostgresFullTextIndex(l, factory, catalog, extractor, mas, this::resolveNode, this::resolveEdge,
					this::getAdjacentEdges, indexStats, extractStats, abstractStats, indexLater, this.thisGraphType);
		}

	}

	/**
	 * Instantiates a new relational graph.
	 *
	 * @param c the data source catalog
	 */

	public RelationalGraph(Factory f, DataSourceCatalog c) {
		this(f, c, GraphType.ORIGINAL_GRAPH);
	}

	private Node resolveNode(int globalId) {
		return resolveNode(getFactory().parseNodeID(globalId));
	}

	/**
	 * Search for a node in the node cache (or in the database if the node is not in cache,
	 * in which case the node is also placed in the cache
	 * @param id the id (NodeID) of the searched node
	 * @return the node if it has been found.
	 */
	@Override
	public Node resolveNode(NodeID id) {
//		log.info("Looking up for node " + id);
		return maybeUseCache(id, nodeCache, NODE_CACHE_SIZE, () -> {
			try {
				resolveNodeStmt.setBigDecimal(1, new BigDecimal((Integer) id.value()));
				ResultSet rs = executeQuery(resolveNodeStmt);
				if (rs.next()) {
					final NodeID result = getFactory().parseNodeID(rs.getInt(1));
					final NodeID rep = getFactory().parseNodeID(rs.getInt(2));
					DataSource ds = catalog.getEntry(rs.getInt(3));
					String reconstitutedLabel = rs.getString(4).replaceAll("''", "'");
					String reconstitutedNormaLabel = rs.getString(5).replaceAll("''", "'");
					Node.Types nodeType = Node.Types.values()[Integer.parseInt(rs.getString(6))];
					Node n = ds.rebuildNodeFromStore(result, reconstitutedLabel, reconstitutedNormaLabel, () -> resolveNode(rep), nodeType);
					if (n == null) {
						throw new IllegalStateException("Unable to create node for " + id);
					}
					n.setNodeType(nodeType);
					return n;
				}
			} catch (SQLException | NullPointerException e) {
				log.warn("Node not found in database, attempting to fetch from memory.", e);
			}

			throw new IllegalStateException("Something went wrong when resolving the node with ID: " + id.value());
		});
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Edge resolveEdge(EdgeID id) {
//		log.info("Looking up for edge " + id);
		return maybeUseCache(id, edgeCache, EDGE_CACHE_SIZE, () -> {
			try {
				ResultSet rs = null;
				resolveEdgeStmt.setBigDecimal(1, new BigDecimal((Integer) id.value()));
				rs = executeQuery(resolveEdgeStmt);
				if (rs.next()) {
					Node n1 = resolveNode(rs.getInt(1));
					Node n2 = resolveNode(rs.getInt(2));
					String label = rs.getString(3);
					String conf = rs.getString(4);
					DataSource ds1 = catalog.getEntry(n1.getDataSource().getID());
					Edge resolvedEdge = ds1.buildEdge(id, n1, n2, Edge.Types.values()[Integer.valueOf(rs.getString(5))],
							label, (conf == null ? 1 : Double.valueOf(conf)), () -> resolveSpecificity(n1, n2, label));
					//log.info("Resolved edge with ID " + id + " into edge with ID: " + resolvedEdge.getId());
					return resolvedEdge;
				}
			} catch (SQLException e) {
				log.warn("Edge not found in database, attempting to fetch from memory.", e);
			}
			throw new IllegalStateException("No such edge: " + id);
		});
	}

	/**
	 * Get the DataSource node corresponding to the given id.
	 * @param id id is the number of the data source (1 for ds1, 2 for ds2) and NOT the id the node representing the data source.
	 * @return the node (data source) corresponding to the id.
	 */
	@Override
	public Node resolveDataSource(int id) {
		PreparedStatement stmt = getPreparedStatement("SELECT l.id FROM " + nodesTableName + " l  WHERE l.ds = ? AND l.type = 0;");
		try {
			stmt.setInt(1, id);
			ResultSet rs = executeQuery(stmt);
			while(rs.next()) {
				return resolveNode(factory.parseNodeID(rs.getInt(1)));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return null;
	}

	@Override
	public int nextID() {
		nodeIdCounter = nodeIdCounter + 1;
		return nodeIdCounter;
	}

	@Override
	public void setIDCounter(Boolean reset) {
		if (reset) {
			nodeIdCounter = 0;
		} else {
			int maxNodeIdCounter = getMaxNodeID();
			int maxEdgeIdCounter = getMaxEdgeID();
			nodeIdCounter = Math.max(maxNodeIdCounter, maxEdgeIdCounter);
		}
	}

	/**
	 *
	 * @return the largest node ID assigned so far.
	 */
	private int getMaxNodeID() {
		String query = "SELECT max(id) FROM " + nodesTableName;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				return Integer.valueOf(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return 0;

	}

	/**
	 *
	 * @return the largest edge ID assigned so far
	 */
	private int getMaxEdgeID() {
		String query = "SELECT max(id) FROM " + edgesTableName;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				return Integer.valueOf(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return 0;

	}

	/**
	 * Puts in the specificity table the (rep, label, nin) and (rep, label, nout)  values 
	 * that result from the edges table. 
	 * It is called upon Session.commit() whenever there are new nodes or edges! l'horreur... 
	 *
	 * @return true, if the database was changed as a result of this update.
	 */
	public void countEdgesForSpecificity() {
		//log.info("COUNTING EDGES FOR SPECIFICITY IN " + specificityTableName);
		this.createTable(SchemaTableNames.CLASSIFIED_SPECIFICITY_TABLE_NAME, "(representative NUMERIC, edge_label varchar, nIn float, nOut float, primary key (representative, edge_label))", false, false); // TODO NELLY: check why this table has not been created before
		executeUpdate("INSERT INTO " + specificityTableName + " (representative, edge_label, nin) "
				+ " SELECT n.representative as node, e.label, COUNT(DISTINCT e.source) AS in " + " FROM "
				+ edgesTableName + " e, " + nodesTableName + " n WHERE e.target = n.id "
				+ " GROUP BY e.label, node HAVING COUNT(DISTINCT e.source) > 1 "  
				+ " ON CONFLICT ON CONSTRAINT "  
				+ specificityTableName + "_pkey DO UPDATE SET nin = excluded.nin");
		executeUpdate("INSERT INTO " + specificityTableName + " (representative, edge_label, nout) "
				+ " SELECT n.representative as node, e.label, COUNT(DISTINCT e.target) AS in " + " FROM "
				+ edgesTableName + " e, " + nodesTableName + " n WHERE e.source = n.id "
				+ " GROUP BY e.label, node HAVING COUNT(DISTINCT e.target) > 1 "    
				+ " ON CONFLICT ON CONSTRAINT " + specificityTableName  
				+ "_pkey DO UPDATE SET nout = excluded.nout");
	}

	@Override
	public Specificity resolveSpecificity(Node node1, Node node2, String label) {
		Specificity sIn = maybeUseCache(Pair.of(node2, label), inDegreeCache, SPEC_CACHE_SIZE,
				() -> _resolveSpecificity(node2, label, true));
		Specificity sOut = maybeUseCache(Pair.of(node1, label), outDegreeCache, SPEC_CACHE_SIZE,
				() -> _resolveSpecificity(node1, label, false));
		return new Specificity(node1, label, sIn.in(), sOut.out());
	}

	/**
	 * Resolves the in-degree for the given node and label.
	 *
	 * @param node  the node
	 * @param label the label
	 * @return the in-degree
	 */
	protected Specificity resolveInDegree(Node node, String label) {
		return maybeUseCache(Pair.of(node, label), inDegreeCache, SPEC_CACHE_SIZE,
				() -> _resolveSpecificity(node, label, true));
	}

	/**
	 * Resolves the in-degree for the given node and label.
	 *
	 * @param node  the node
	 * @param label the label
	 * @return the out-degree specificity
	 */
	protected Specificity resolveOutDegree(Node node, String label) {
		return maybeUseCache(Pair.of(node, label), outDegreeCache, SPEC_CACHE_SIZE,
				() -> _resolveSpecificity(node, label, false));
	}

	/**
	 * Resolve in- or out-degree for the given node and label.
	 *
	 * @param node  the node
	 * @param label the label
	 * @param in    true, if retrieving the in-degree, false for out-degree.
	 * @return the in- or out-degree
	 */
	private Specificity _resolveSpecificity(Node node, String label, boolean in) {
		try {
			getInOutFromSpecificityStmt.setBigDecimal(1, new BigDecimal((Integer) node.getId().value()));
			// IM, 6/6/20: Here we really need to count the edges adjacent to n,
			// not to its representative. See execution trace and explanation:
			// https://gitlab.inria.fr/cedar/connection-lens/-/issues/302#note_347703
			getInOutFromSpecificityStmt.setString(2, label);
			ResultSet rs = executeQuery(getInOutFromSpecificityStmt);
			//try {
			//	throw new Exception("Really counting input and output edges!");
			//}
			//catch(Exception e) {
			//	e.printStackTrace(); 
			//}
			if (rs.next()) {
				return new Specificity(node, label, rs.getInt(1), rs.getInt(2));
			}
		} catch (SQLException e) {
			log.warn("Specificity not found in database, attempting to fetch from memory.", e);
		}
		DataSource ds = catalog.getEntry(node.getDataSource().getLocalURI());
		if (ds != null) {
			return in ? new Specificity(node, label, 1, 0) : new Specificity(node, label, 0, 1);
		}
		throw new IllegalStateException("No such specificity: " + node + " / " + label);
	}

	@Override
	public Factory getIDFactory() {
		return this.getFactory();
	}

	@Override
	/** Node-level caching in resolveNode, 1 or 2 queries max per node */
	public Set<Node> getNodes(DataSource ds, Item.Types... types) {
		PreparedStatement stmt = getPreparedStatement(
				"SELECT l.id FROM " + nodesTableName + " l  WHERE l.ds = ? " + typeConstraint(types));
		Set<Node> result = new LinkedHashSet<>();
		try {
			stmt.setInt(1, ds.getID());
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveNode(getFactory().parseNodeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	public Set<Node> getAllNodesPostLoading(){
		int violationCounter = 0;
		PreparedStatement stmt = getPreparedStatement("SELECT id, ds, type, label, normalabel, representative, odw, dw FROM nodes");
		Set<Node> result = new LinkedHashSet<>();
		HashMap<NodeID, Node> localHash = new HashMap<NodeID, Node>();
		try {
			ResultSet rs = executeQuery(stmt);
			while(rs.next()) {
				int thisNID = rs.getInt(1);
				NodeID nID = getFactory().parseNodeID(thisNID);
				DataSource ds = catalog.getEntry(rs.getInt(2));
				Node.Types nodeType = Node.Types.values()[Integer.valueOf(rs.getString(3))];
				String reconstitutedLabel = rs.getString(4).replaceAll("''", "'");
				String reconstitutedNormaLabel = rs.getString(5).replaceAll("''", "'");
				int repNID = rs.getInt(6);
				NodeID repID = getFactory().parseNodeID(repNID);
				int odw = rs.getInt(7);
				int dw = rs.getInt(8);

				Node n = null;
				if (thisNID != repNID) { // n is not represented by itself
					final Node theRep = (Node)localHash.get(repID);
					if (theRep == null) {// TODO use the actual labels and types of representative, not those of n!!!
						violationCounter++;
						final Node newRep = ds.rebuildNodeFromStore(repID, reconstitutedLabel, reconstitutedNormaLabel, null, nodeType);
						localHash.put(repID, newRep);
						n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, ()->newRep, nodeType);
					}
					else { // we have retrieved from the database n's representative, before n
						n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, ()->theRep, nodeType);
					}
				}
				else { // n is self-represented, use a self-representing constructor
					n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, null, nodeType);
				}
				// add this to the local hash anyway, for future use
				localHash.put(nID, n);

			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		log.info("Number of representatives seen after distinct nodes they represent: " + violationCounter);
		return result;
	}
	
	@Override
	/** Node-level caching in resolveNode, 1 or 2 point queries max per node */
	public Set<Node> getNodes(fr.inria.cedar.connectionlens.graph.Node.Types nodeType) {
		PreparedStatement stmt = getPreparedStatement(
				"SELECT l.id FROM " + nodesTableName + " l  WHERE " + typeConstraint(nodeType).substring(4));
		// the type constraint starts with an " AND" that has to be eliminated here
		Set<Node> result = new LinkedHashSet<>();
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveNode(getFactory().parseNodeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result; 
	}

	@Override
	/** Global query wrt edges.
	 *  Up to 4 node-level queries still possible, through resolveNode().
	 *  Node caching through resolveNode()
	 *  No edge caching. */
	public Set<Edge> getEdges(DataSource ds, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();

		String query = "SELECT l.id, l.source, l.target, l.label, l.confidence, l.type FROM "
				+ edgesTableName + " l WHERE l.ds = ?" + typeConstraint(types);

		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, ds.getID());
			ResultSet rs = executeQuery(stmt);

			while (rs.next()) {
				EdgeID edgeID = getFactory().parseEdgeID(rs.getInt(1));
				Node n1 = resolveNode(rs.getInt(2));
				Node n2 = resolveNode(rs.getInt(3));
				String label = rs.getString(4);
				String conf = rs.getString(5);
				DataSource ds1 = catalog.getEntry(n1.getDataSource().getID());
				Edge edge = ds1.buildEdge(edgeID, n1, n2, Edge.Types.values()[Integer.valueOf(rs.getString(6))],
						label, (conf == null ? 1 : Double.valueOf(conf)), () -> resolveSpecificity(n1, n2, label));
				result.add(edge);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result; 
	}

	@Override
	/** Global, narrow query for node IDs.
	 * Then, up to 4 node-level queries for each node through resolveNode().
	 * Node caching through resolve-node.
	 */
	public Set<Node> getNodes(DataSource ds, String label, Item.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT l.id FROM " + nodesTableName + " l WHERE l.ds = ? AND l.label LIKE ? "
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, ds.getID());
			stmt.setString(2, label);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveNode(getFactory().parseNodeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	/** Global, narrow query for node IDs.
	 * Then, up to 4 node-level queries for each node through resolveNode().
	 * Node caching through resolve-node.
	 */
	public Set<Node> getNodes(String label, Item.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT l.id FROM " + nodesTableName + " l WHERE l.label LIKE ?" + typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setString(1, label);
			ResultSet rs = executeQuery(stmt);
			// log.info(stmt);
			while (rs.next()) {
				result.add(resolveNode(factory.parseNodeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}


	@Override
	/** Global, narrow query for edge IDs.
	 * Then, possibly 1 edge-level query and up to 4 node-level queries, through resolveEdge().
	 * Edge caching
	 * Node caching
	 */
	public Set<Edge> getEdges(DataSource ds, String label, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id FROM " + edgesTableName + " l WHERE l.ds = ? AND l.label LIKE ? "
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, ds.getID());
			stmt.setString(2, label);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(factory.parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}


	@Override
	/** Global, narrow query for edge IDs.
	 * Then, possibly 1 edge-level query and up to 4 node-level queries, through resolveEdge().
	 * Edge caching
	 * Node caching
	 */
	public Set<Edge> getEdges(String label, Item.Types... types) {
		if (Edge.SAME_AS_LABEL.equals(label)) {
			return getSameAs();
		}
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id FROM " + edgesTableName + " l WHERE l.label LIKE ?" + typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setString(1, label);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	/** Global, narrow query for edge IDs.
	 * Then, possibly 1 edge-level query and up to 4 node-level queries, through resolveEdge().
	 * Edge caching
	 * Node caching
	 */
	public Set<Edge> getEdges(Node from, Node to, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id,source, target FROM " + edgesTableName + " l WHERE l.source = ? AND l.target = ?"
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {

			stmt.setBigDecimal(1, new BigDecimal((Integer) from.getId().value()));
			stmt.setBigDecimal(2, new BigDecimal((Integer) to.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 *
	 * @param oldNodes (list of nodes).
	 * @param newNodes (list of nodes).
	 * @return (1) all edges from any old node to any new node; (2) all edges
	 *         from any new node to any old node; (3) all edges from any new node to
	 *         any new node.
	 * IM, 23/4/21: THIS IS UNLIMITED IN THE NUMBER OF EDGES IT RETURNS, TO AVOID DISCONNECTED GRAPHS
	 * WE HOPE THINGS REMAIN UNDER CONTROL THANKS TO SMALL NUMBER OF NODES!
	 *
	 * 1 global, narrow query then point queries for edges (and thus nodes).
	 * Cache-awareness through resolveNodes.
	 */
	@Override
	public Set<Edge> getEdgesBetweenSets(Collection<Node> oldNodes, Collection<Node> newNodes) {
		//log.info("REL GRAPH GET EDGES BETWEEN SETS " + oldNodes + ", " + newNodes);

		Set<Edge> result = new HashSet<Edge>();
		String query = ""; 
		PreparedStatement stmt; 

		// from the old nodes to the new nodes, or viceversa, only if both not empty
		if (oldNodes.size() > 0 && newNodes.size() > 0) {// LIMITED IM 23/3/21
			query = "SELECT id FROM " + edgesTableName + remainderQuery(oldNodes, newNodes);
			stmt = getPreparedStatement(query);
			try {
				ResultSet rs = executeQuery(stmt);
				while (rs.next()) {
					result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}

			query = "SELECT id FROM " + edgesTableName + remainderQuery(newNodes, oldNodes);
			stmt = getPreparedStatement(query);
			try {
				ResultSet rs = executeQuery(stmt);
				while (rs.next()) {
					result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		if (newNodes.size() > 0) {
			// from the new nodes to the new nodes
			query = "SELECT id FROM " + edgesTableName + remainderQuery(newNodes, newNodes); 
			stmt = getPreparedStatement(query);
			try {
				ResultSet rs = executeQuery(stmt);
				while (rs.next()) {
					result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		//log.info("RELGRAPH SQL-BASED returned " + result.size() + " edges"); 

		return result; 

	}

	private String remainderQuery(Collection<Node> sourceNodes, Collection<Node> targetNodes) {
		String query = " WHERE source IN " + makeStringRepresentation(sourceNodes)  
		+ " AND "
		+ " target IN " +  makeStringRepresentation(targetNodes) ; 
		//log.info("Source nodes: " + sourceNodes.size() + " target nodes: " + targetNodes.size() + " query: " + query);
		return query; 

	}
	private String makeStringRepresentation(Collection<Node>  nodeSet) {
		StringBuffer sb = new StringBuffer();
		sb.append("("); 
		int i = 0; 
		for (Node n: nodeSet) {
			sb.append(n.getId().value()); 
			if (i < nodeSet.size() - 1) {
				sb.append(", ");
			}
			i ++;
		}
		sb.append(")");
		return new String(sb); 
	}

	/**
	 * @param n
	 * @param k
	 * @return k Outgoing Specific Edges
	 * 1 query over the computed specificity table
	 * Then, edge-level querying, cache-aware, through resolveEdge.
	 */
	public Set<Edge> getOutgoingSpecificEdges(Node n, int k) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id,l.source,l.target FROM " + computedSpecificityTableName
				+ " l WHERE l.source = ? ORDER by l.specificity DESC " + " LIMIT " + k;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param n
	 * @param k
	 * @return Incoming Specific edges
	 * 1 query over the computed specificity table
	 * Then, edge-level querying, cache-aware, through resolveEdge.
	 */
	public Set<Edge> getIncomingSpecificEdges(Node n, int k) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id,l.source,l.target FROM " + computedSpecificityTableName
				+ " l WHERE l.target = ? ORDER by l.specificity DESC " + " LIMIT " + k;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	/** Cache-aware point lookup. Feeds the adjacency cache directly,
	 * and the node and edge caches indirectly, through resolveEdge.
	 */
	public Set<Edge> getOutgoingEdges(Node n, Item.Types... types) {
		return maybeUseCache(Pair.of(n, ImmutableSet.copyOf(types)), outgoingCache, ADJACENCY_CACHE_SIZE, () -> {
			Set<Edge> result = new LinkedHashSet<>();
			String query = "SELECT l.id, l.source, l.target,l.label, 1.0 FROM " + edgesTableName
					+ " l WHERE l.source = ? " + typeConstraint(types);
			PreparedStatement stmt = getPreparedStatement(query);
			try {
				stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
				ResultSet rs = executeQuery(stmt);
				while (rs.next()) {
					result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
			return result; 
		});
	}

	@Override
	public Set<Edge> getOutgoingEdges(Node n, String[] labels, Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.source, l.target,l.label, 1.0 FROM " + edgesTableName
				+ " l WHERE l.source = ? " + labelConstraint(labels) + typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	public Set<Edge> getIncomingEdges(Node n, String[] labels, Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.source, l.target, l.label, 1.0 FROM " + edgesTableName
				+ " l WHERE l.target = ? " + labelConstraint(labels) + typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	/** Cache-aware point lookup. Feeds the adjacency cache directly,
	 * and the node and edge caches indirectly, through resolveEdge.
	 */
	public Set<Edge> getIncomingEdges(Node n, Item.Types... types) {
		return maybeUseCache(Pair.of(n, ImmutableSet.copyOf(types)), incomingCache, ADJACENCY_CACHE_SIZE, () -> {
			Set<Edge> result = new LinkedHashSet<>();
			String query = "SELECT l.id, l.source, l.target, l.label, 1.0 FROM " + edgesTableName
					+ " l WHERE l.target = ? " + typeConstraint(types);
			PreparedStatement stmt = getPreparedStatement(query);
			try {
				stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
				ResultSet rs = executeQuery(stmt);
				while (rs.next()) {
					result.add(resolveEdge(getFactory().parseEdgeID(rs.getInt(1))));
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
			return result; 
		});
	}

	@Override
	public Set<Edge> getSameAs(Node n, double threshold) {
		return getSameAs(n, null, threshold);
	}

	@Override
	public Set<Edge> getSameAs(Node from, DataSource to, double threshold) {
		//return maybeUseCache(from.getId(), sameAsCache, sameAsCacheSize, () -> {
		Set<Edge> result = getStrongSameAs(from, to);
		if (threshold >= 0. && threshold < 1.0) {
			result = Sets.union(result, getWeakSameAs(from, to, threshold));
		}
		return result;
		//});
	}

	@Override
	public Set<Edge> getSameAs(double threshold) {
		Set<Edge> result = getStrongSameAs(null, null);
		if (0 <= threshold && threshold < 1.0) {
			result = Sets.union(result, getWeakSameAs(null, null, threshold));
		}
		return result;
	}

	@Override 
	/** point query, then calls resolveNode. Contributes to the node cache indirectly.
	 * TODO why not use the sameAsCache here? 
	 */
	public Set<Edge> getWeakSameAs(@Nullable Node from, double threshold) {
		//return maybeUseCache(from.getId(), sameAsCache, sameAsCacheSize, () -> {
		Set<Edge> weakSameAs = new LinkedHashSet<>();
		String query = "SELECT id FROM " + weakSameAsEdgesTableName;
		if (from == null) {
			query = "SELECT id, node1, node2, confidence FROM " + weakSameAsEdgesTableName + " WHERE confidence >= ? ";
		} else {
			query = "SELECT node2, confidence FROM " + weakSameAsEdgesTableName
					+ " WHERE ds1 = ? AND node1 = ? AND confidence >= ?";
		}
		PreparedStatement preparedStatement = getPreparedStatement(query);

		try {
			if (from == null) {
				preparedStatement.setDouble(1, threshold);
			} else {
				preparedStatement.setInt(1, from.getDataSource().getID());
				preparedStatement.setBigDecimal(2, new BigDecimal((Integer) from.getId().value()));
				preparedStatement.setDouble(3, threshold);
			}
			// log.info(preparedStatement);
			ResultSet rs = executeQuery(preparedStatement);
			while (rs.next()) {
				if (from == null) {
					Node n1 = resolveNode(rs.getInt(2));
					Node n2 = resolveNode(rs.getInt(3));
					DataSource ds1 = catalog.getEntry(n1.getDataSource().getID());
					Edge edge = ds1.buildSameAsEdge(n1, n2, rs.getDouble(4));
					weakSameAs.add(edge);

				} else {
					Edge e = from.getDataSource().buildSameAsEdge(from, resolveNode(rs.getInt(1)), rs.getDouble(2));
					weakSameAs.add(e);
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}

		return weakSameAs;
		//});
	}

	@Override 
	/** point query, then calls resolveNode. Contributes to the node cache indirectly.
	 * TODO why not use the sameAsCache here? 
	 */
	public Set<Edge> getWeakSameAs(@Nullable Node from, @Nullable DataSource to, double threshold) {
		Set<Edge> weakSameAs = new LinkedHashSet<>();
		String query = "SELECT id FROM " + weakSameAsEdgesTableName;
		if (from == null && to == null) {
			query = "SELECT id, node1, node2, confidence FROM " + weakSameAsEdgesTableName + " WHERE confidence >= ? ";
		} else if (from != null) {
			query = "SELECT node2, confidence FROM " + weakSameAsEdgesTableName
					+ " WHERE ds1 = ? AND node1 = ? AND confidence >= ?";
			if (to != null)
				query += " AND ds2 = ? ";
		}
		PreparedStatement preparedStatement = getPreparedStatement(query);
		try {
			if (from == null && to == null) {
				preparedStatement.setDouble(1, threshold);
			} else if (from != null) {
				preparedStatement.setInt(1, from.getDataSource().getID());
				preparedStatement.setBigDecimal(2, new BigDecimal((Integer) from.getId().value()));
				preparedStatement.setDouble(3, threshold);
				if (to != null) {
					preparedStatement.setInt(4, to.getID());
				}
			}
			ResultSet rs = executeQuery(preparedStatement);
			while (rs.next()) {
				if (from == null) {
					// log.info(rs.getString(1));
					Node n1 = resolveNode(rs.getInt(2));
					Node n2 = resolveNode(rs.getInt(3));
					DataSource ds1 = catalog.getEntry(n1.getDataSource().getID());
					Edge edge = ds1.buildSameAsEdge(n1, n2, rs.getDouble(4));
					weakSameAs.add(edge);
				} else {
					Edge e = from.getDataSource().buildSameAsEdge(from, resolveNode(rs.getInt(1)), rs.getDouble(2));
					weakSameAs.add(e);
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}

		return weakSameAs;
	}

	/**
	 * @param from the node from which the same-as edge shall start
	 * @param to   the data source to which the same-as edge shall end
	 * @return the set of strong same-as edges between the given node and data
	 *         source.
	 * point query, then feeds the node cache indirectly through resolveNode
	 */
	private Set<Edge> getStrongSameAs(@Nullable Node from, @Nullable DataSource to) {
		Set<Edge> result = new LinkedHashSet<>();
		String query;
		query = "SELECT l1.id AS node1, l2.id AS node2 FROM " + nodesTableName + " l1, " + nodesTableName + " l2 "
				+ " WHERE l1.label <> '' AND l1.representative = l2.representative AND l1.id <> l2.id";
		if (from != null) {
			query += " AND l1.ds = ? AND l1.id = ? ";
			if (to != null) {
				query += " AND l2.ds = ?";
			}
		}
		PreparedStatement preparedStatement = getPreparedStatement(query);

		try {
			if (from != null) {
				preparedStatement.setInt(1, from.getDataSource().getID());
				preparedStatement.setBigDecimal(2, new BigDecimal((Integer) from.getId().value()));
				if (to != null) {
					preparedStatement.setInt(3, to.getID());
				}
			}
			ResultSet rs = executeQuery(preparedStatement);
			while (rs.next()) {
				Node f = resolveNode(rs.getInt(1));
				Node o = resolveNode(rs.getInt(2));
				Edge e = f.getDataSource().buildSameAsEdge(f, o, 1.);
				result.add(e);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	
	public Pair<Double, Double> getSpecificityStat() {
		String query = "SELECT * FROM " + SchemaTableNames.ORIGINAL_SPEC_STAT_TABLE_NAME + " LIMIT 1";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			if (rs.next()) {
				return new ImmutablePair<>(rs.getDouble(1), rs.getDouble(2));
			} else {
				return new ImmutablePair<>(null, null);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	/** Point query, no caching. **/
	public NodeID getOriginalNodeID(Node n) {
		if (!this.isAbstract()) {
			return null;
		} else {
			String query = "SELECT clid FROM " + nodesTableName + " WHERE id = ?";
			PreparedStatement stmt = getPreparedStatement(query);
			try {
				stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
				ResultSet rs = executeQuery(stmt);
				if (rs.next() && rs.getString(1) != null) {
					return getFactory().parseNodeID(rs.getInt(1));
				} else {
					return null;
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	@Override
	/** Point update */
	public void setType(NodeID id, Node.Types t) {
		String query = "UPDATE " + nodesTableName + " SET type = ?  WHERE id = ? ";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, t.ordinal());
			stmt.setBigDecimal(2, new BigDecimal((Integer) id.value()));
			executeUpdate(stmt, false);
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	/** Indirect node caching */
	public Set<Node> getDisambiguatedNodes(DataSource ds) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT ambi.entityid FROM " + disambiguatedTableName + " ambi";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(resolveNode(getFactory().parseNodeID(rs.getInt(1))));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	/** No caching */
	public String getDisambiguatedID(String label) {
		Set<Node> getNodes = getNodes(label);
		if (getNodes != null) {
			for (Node node : getNodes) {
				String ambiID = getDisambiguatedForTheNode(node);
				if (ambiID != null) {
					return ambiID;
				}
			}
			return null;
		}
		return null;
	}

	@Override
	/** No caching */
	public String getDisambiguatedForTheNode(Node node) {
		String ambiID = null;
		String query = "SELECT ambi.ambiuri FROM " + disambiguatedTableName + " ambi WHERE ambi.entityid = ?";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, Integer.valueOf(node.getId().value().toString()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				ambiID = rs.getString(1);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return ambiID;
	}

	@Override
	/** Used only in tests */
	public int countSameAs(DataSource from) {
		String query = "SELECT count(*) FROM " + sameAsEdgesTableName + " WHERE ds1 = ?";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setInt(1, from.getID());
			ResultSet rs = executeQuery(stmt);
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return 0;
	}

	@Override
	/** Used only for tests and statistics reporting. No caching.*/
	public int countEdges(DataSource ds, Types... typeFilter) {
		Set<Types> types = ImmutableSet.copyOf(typeFilter);
		AtomicInteger result = new AtomicInteger(0); 
		String query = "SELECT count(*) FROM " + edgesTableName + " WHERE ds = ? ";
		if (!types.isEmpty()) {
			query += " AND type = ANY (?) ";
		}
		PreparedStatement preparedStatement = getPreparedStatement(query);
		try {
			preparedStatement.setInt(1, ds.getID());
			if (!types.isEmpty()) {
				Array t = createArray("INTEGER", transform(types, Types::ordinal));
				preparedStatement.setArray(2, t);
			}
			//log.info("Running: " + preparedStatement); 
			ResultSet rs = executeQuery(preparedStatement);
			if (rs.next()) {
				result.addAndGet(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result.get();
	}

	@Override
	/** Used only for drawîng, reporting, and tests. No caching.*/
	public int countNodes(DataSource ds, Types... typeFilter) {
		Set<Types> types = ImmutableSet.copyOf(typeFilter);
		AtomicInteger result = new AtomicInteger(0); 
		String query = "SELECT count(*) FROM " + nodesTableName + " WHERE ds = ? ";
		if (!types.isEmpty()) {
			query += " AND type = ANY (?) ";
		}
		PreparedStatement preparedStatement = getPreparedStatement(query);
		try {
			preparedStatement.setInt(1, ds.getID());
			if (!types.isEmpty()) {
				Array t = createArray("INTEGER", transform(types, Types::ordinal));
				preparedStatement.setArray(2, t);
			}
			ResultSet rs = executeQuery(preparedStatement);
			if (rs.next()) {
				result.addAndGet(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result.get();
	}

	@Override
	/** Indirect node caching through resolveNode() */
	public void enumeratePairs(Collection<DataSource> from, Collection<DataSource> to, NodePairSelector selector,
			Consumer<Edge> processor) {
		if (from.size() == 1 && to.size() == 1
				&& Config.getInstance().getProperty("entity_node_creation_policy").equals("PER_GRAPH")) {
			// if the entity comparison mode is not PER_GRAPH, not comparing the nodes of a
			// graph among themselves may introduce errors
			for (DataSource ds1 : from) {
				for (DataSource ds2 : to) {
					if (ds1 == ds2) {
						if (ds1 instanceof RDFDataSource) {
							// log.info("Same RDF data sourcee " + ds1 + ", nothing to compare");
							return;
						}
					}
				}
			}
		}
		Set<Integer> sourceDss = new LinkedHashSet<>();
		Set<Integer> targetDss = new LinkedHashSet<>();
		from.forEach(s -> sourceDss.add(s.getID()));
		to.forEach(s -> sourceDss.add(s.getID()));
		to.forEach(s -> targetDss.add(s.getID()));
		String query = "SELECT l1.id AS n1, l2.id AS n2 FROM " + nodesTableName + " l1, " + nodesTableName
				+ " l2  WHERE ";
		// if(compareStoredLabelPrefix) { query = query +
		// "l1.labelprefix=l2.labelprefix AND "; } // IM, 4/6/20: this is added within
		// some selectors
		query = query
				+ " l1.id <> l2.id AND l1.label <> '' AND l2.label <> '' AND l1.representative <> l2.representative "
				+ " AND l1.ds IN (" + Joiner.on(',').join(sourceDss) + ") AND l2.ds IN ("
				+ Joiner.on(',').join(targetDss) + ")";
		// IM, 19/2/2020: adding here that no labels of 1 or 2 characters should be
		// compared
		// This is an easy way out of linking on "1" or "M." but reality is more tricky;
		// in the US, 2 letters are meaningful
		// because they denote a state (so is "US" :) )
		// In fact, we should check that the _meaningful part of the label_ has at least
		// 2 characters.
		// For that, the most logical plan is to store(also) a "normalized" label, and
		// even index that one, not the original label.
		NodePairSelector fullSelector = selector.and(ofAbsoluteLength(Axis.BOTH, greaterThan(1)));
		// for (String exclusionWord: ConnectionLens.initDoNotLinkLabels()) {
		// fullSelector =
		// fullSelector.and(not(withLabel(SOURCE,exclusionWord))).and(not(withLabel(TARGET,
		// exclusionWord)));
		// }
		String cond = toSQL(fullSelector, "l1", "l2");
		if (!cond.isEmpty()) {
			query += " AND " + cond;
		}
		// log.info("\nRunning selector query: " + query);
		// long qStart = System.currentTimeMillis();
		// int nResults = 0, nCompared = 0;
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
			// log.info("query->"+query);
			while (rs.next()) {
				Node n1 = resolveNode(rs.getInt(1));
				Node n2 = resolveNode(rs.getInt(2));
				//				log.info(n1.getType() + " " + n1.getLabel() + " " +
				//						n1.getId().value() + " and " + n2.getType() + " " + n2.getLabel() + " " +
				//						n2.getId().value());
				//				
				// log.info(n1+"--"+n1.getType()+"--"+n2+"--"+n2.getType());
				if (decideIfWeCompare(n1, n2)) {
					// log.info("RelationalGraph: Comparing " + n1.getType() + " " +
					// n1.getLabel()+" "+n1.getId()
					// +" "+n1.representative().getId()
					// + " with " + n2.getType() + " " + n2.getLabel()
					// +" " +n2.getId()
					// +" "+n2.representative().getId());
					processor.accept(n1.getDataSource().buildSameAsEdge(n1, n2));
					//nCompared++;
				}
				/*
				 * else { log.info("Not comparing"); }
				 */
				//nResults++;
				// if ((nResults % 10000)==0) {
				// log.info("Got " + nResults + " results and compared " + nCompared +
				// " in " + (System.currentTimeMillis() - qStart) + " ms");
				// }

			}
		} catch (SQLException e) {
			// log.info("Query: " + query);
			throw new IllegalStateException(e);
		}
		// long qStop = System.currentTimeMillis();
		// log.info((qStop-qStart) + " ms, " + nResults + " results");
	}
	
	@Override
	public boolean generateEdgeSpecificityComputeMeanStdDev(StatisticsCollector stats) {
		// log.info("Storing edge specificities");
		stats.resume(StatisticsCollector.total());
//		if(true) {
//			throw new IllegalStateException("Stop here.");
//		}
//		log.info("CREATE TABLE "  + this.computedSpecificityTableName);
		String query = "CREATE TABLE " + this.computedSpecificityTableName
				+ " AS (SELECT e.id, e.source, e.target, e.label, "
				+ " 2/(CASE WHEN s1.nin IS NULL then 1 ELSE s1.nin END + CASE WHEN s2.nout IS NULL then 1 ELSE s2.nout END) AS specificity,  "
				+ " e.confidence FROM "
				+ this.edgesTableName + " e LEFT OUTER JOIN " + this.specificityTableName
				+ " s1 ON e.target = s1.representative AND e.label = s1.edge_label LEFT OUTER JOIN "
				+ this.specificityTableName + " s2 ON e.source=s2.representative AND e.label = s2.edge_label)";
		PreparedStatement stmt = getPreparedStatement(query);
		boolean rt = executeUpdate(stmt, false);

		executeUpdate("CREATE INDEX " + (isAbstract()?"abs_":isNormalized()?"norm_":isSummary()?"summ_":isClassified()?"class_":"") + "edge_specificity_source ON " + this.computedSpecificityTableName + " (source, specificity)");
		executeUpdate("CREATE INDEX "+ (isAbstract()?"abs_":isNormalized()?"norm_":isSummary()?"summ_":isClassified()?"class_":"") + "edge_specificity_target ON " + this.computedSpecificityTableName + " (target, specificity)");
//		log.info("CREATE TABLE " + this.specStatTableName);
		String meanstddev = "CREATE TABLE " + this.specStatTableName + " as SELECT avg(specificity), stddev(specificity) FROM " + this.computedSpecificityTableName;
		stmt = getPreparedStatement(meanstddev);
		executeUpdate(stmt, false);
		stats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.SPEC_T);
		return rt;
	}

	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching through resolveNode()
	 * No edge caching.
	 */
	@Override
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, Double threshold, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label, l.source, l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName 
				+ " l WHERE specificity > ? AND  l.source = ? "
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setDouble(1, threshold.doubleValue());
			stmt.setBigDecimal(2, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						n,
						resolveNode(getFactory().parseNodeID(rs.getInt(4))),
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6))); 
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}
	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching
	 * No edge caching
	 */
	@Override
	public Set<Edge> getKOutgoingEdgesPostLoading(Node n, Integer edgeNo, Item.Types... types) {

		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label, l.source, l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName + " l WHERE l.source = ? "
				+ typeConstraint(types) + " LIMIT ?" ;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			stmt.setInt(2, edgeNo);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						n,
						resolveNode(getFactory().parseNodeID(rs.getInt(4))),
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6))); 
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}
	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching
	 * No edge caching
	 */
	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, Double threshold, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label,l.source,l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName + " l WHERE specificity > ? AND l.target = ? "
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setDouble(1, threshold.doubleValue());
			stmt.setBigDecimal(2, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						resolveNode(getFactory().parseNodeID(rs.getInt(3))),
						n, 
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6))); 
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching
	 * No edge caching
	 */
	@Override
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, String[] labels, Double threshold, Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label, l.source, l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName
				+ " l WHERE specificity > ? AND  l.source = ? " + labelConstraint(labels)
				+ typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setDouble(1, threshold.doubleValue());
			stmt.setBigDecimal(2, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						n,
						resolveNode(getFactory().parseNodeID(rs.getInt(4))),
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6)));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching
	 * No edge caching
	 */
	@Override
	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, String[] labels, Double threshold, Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label,l.source,l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName + " l WHERE specificity > ? AND l.target = ? "
				+ labelConstraint(labels) + typeConstraint(types);
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setDouble(1, threshold.doubleValue());
			stmt.setBigDecimal(2, new BigDecimal((Integer) n.getId().value()));
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						resolveNode(getFactory().parseNodeID(rs.getInt(3))),
						n,
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6)));
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}


	/**
	 * This method should be used only after the graph has been completely
	 * loaded, and in particular, the edge_specificity table has been generated.
	 * Indirect node caching
	 * No edge caching
	 */
	public Set<Edge> getKIncomingEdgesPostLoading(Node n, Integer edgeNo, Item.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT l.id, l.label, l.source, l.target, l.confidence, l.specificity FROM " + this.computedSpecificityTableName + " l WHERE l.target = ? "
				+ typeConstraint(types) + " LIMIT ?" ;
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			stmt.setBigDecimal(1, new BigDecimal((Integer) n.getId().value()));
			stmt.setInt(2, edgeNo);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				result.add(new Edge(getFactory().parseEdgeID(rs.getInt(1)),
						resolveNode(getFactory().parseNodeID(rs.getInt(3))),
						n, 
						rs.getString(2),
						rs.getDouble(5),
						rs.getDouble(6))); 

			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * We identify four possible cases. case 1: One of the node is a text and the
	 * other is an entity extracted from it. In this case, we must compare, in order
	 * to find them equivalent. case 2: One of the nodes is a text while the other
	 * is an entity extracted from somewhere else. If the entity is a person and if
	 * one of them is a given name, don't compare. Otherwise, compare. case 3: Both
	 * nodes are texts. In this case, if one of them is a given name, don't compare.
	 * Otherwise, compare. case 4: Both nodes are entities. In this case, if they
	 * are person entities and one of them is a given name, we don't compare.
	 * Otherwise, compare. And in the default case we compare them.
	 * 
	 * In the above, we allowed ourselves to say "if one is given name, don't
	 * compare", because if the other is not a given name, we bet they will be too
	 * far - and we don't want the similarity, anyway.
	 * 
	 * @param n1
	 * @param n2
	 * @return
	 */
	// TODO 24/03/20: with an entity creation policy that is not PER_OCCURRENCE, the
	// property 'extractedFrom' of an entity node
	// will not be accurate.
	private boolean decideIfWeCompare(Node n1, Node n2) {
		boolean n1IsEntity = isEntityType(n1.getType());
		boolean n1IsPerson = n1.getType().equals(ENTITY_PERSON);
		boolean n2IsEntity = isEntityType(n2.getType());
		boolean n2IsPerson = n2.getType().equals(ENTITY_PERSON);
		boolean n1extractedFromN2 = (n1.getExtractedFrom() != null && n1.getExtractedFrom().equals(n1));
		// if(n1extractedFromN2) {
		// log.info("n1 extracted from n2");
		// }
		boolean n2extractedFromN1 = (n2.getExtractedFrom() != null && n2.getExtractedFrom().equals(n1));
		// if(n2extractedFromN1) {
		// log.info("n2 extracted from n1");
		// }
		if (!n1IsEntity) { // n1 text
			if (n2IsEntity) { // n1 text, n2 entity
				if (n2extractedFromN1) { // n1 text, n2 entity, n2 extracted from n1: case 1
					// log.info("Case 1.1");
					return true;
				} else { // n1 text, n2 entity, n2 not extracted from n1: case 2
					// log.info("Case 2.1");
					if (n2IsPerson) {
						if (FirstNameDictionary.isFirstName(n2.getLabel())
								|| FirstNameDictionary.isFirstName(n1.getLabel())) {
							// log.info("n2 person, one of them is a name, we reject");
							return false;
						} else {
							// log.info("n2 person, none of them is a name, we accept");
						}
					} else {
						// log.info("n2 is an entity but not a person, we accept");
					}
				}
			} else { // n1 text, n2 text: case 3
				// log.info("Case 3");
				if (FirstNameDictionary.isFirstName(n2.getLabel()) || FirstNameDictionary.isFirstName(n1.getLabel())) {
					// log.info("two strings, one of them is a name, we reject");
					return false;
				}
			}
		} else { // n1 entity
			// log.info("n1 entity");
			if (!n2IsEntity) { // n1 entity, n2 text
				// log.info("n2 text");
				if (n1extractedFromN2) {// n1 entity, n2 text, n1 extracted from n2: case 1
					// log.info("Case 1.2");
					return true;
				} else {
					// log.info("Case 2.2");
					if (FirstNameDictionary.isFirstName(n2.getLabel())
							|| FirstNameDictionary.isFirstName(n1.getLabel())) {
						// log.info("one of them is a name, we reject");
						return false;
					} else {
						// log.info("None of them is a name, we accept");
					}
				}
			} else { // n1 entity, n2 entity
				// log.info("Case 4");
				// case 4: do not compare if one is a person and the other a given name
				if (n1IsPerson && n2IsPerson
						&& (FirstNameDictionary.isFirstName(n2.getLabel())
								|| FirstNameDictionary.isFirstName(n1.getLabel()))) {
					return false;
				}
			}
		}
		// log.info("Default: we accept");
		return true;
	}


	private String toSQL(NodePairSelector s, String source, String target) {
		if (s instanceof ConjunctiveSelector) {
			ConjunctiveSelector c = (ConjunctiveSelector) s;
			return "(" + toSQL(c.left(), source, target) + ") AND (" + toSQL(c.right(), source, target) + ")";
		}
		if (s instanceof DisjunctiveSelector) {
			DisjunctiveSelector d = (DisjunctiveSelector) s;
			return "(" + toSQL(d.left(), source, target) + ") OR (" + toSQL(d.right(), source, target) + ")";
		}
		if (s instanceof MinHashLSHSelector) {
			MinHashLSHSelector lsh = (MinHashLSHSelector) s;
			String result = "";
			String sep = "(";
			Range<Integer> rng = lsh.range();
			for (int i = rng.lowerEndpoint() + 1, l = rng.upperEndpoint() + 1; i < l; i++) {
				result += sep + source + ".signature[" + i + "] = " + target + ".signature[" + i + "]";
				sep = " OR ";
			}
			return result + ")";
			// result += "lsh_compare(l1.signature, l2.signature, " + rng.lowerEndpoint() +
			// "," +
			// (rng.upperEndpoint() - rng.lowerEndpoint()) + "," + lsh.threshold() + ")";
			// return result;
		}
		if (s instanceof AbsoluteLengthSelector) {
			AbsoluteLengthSelector asl = (AbsoluteLengthSelector) s;
			Range<Integer> rng = asl.range();
			return splitAxis(asl.axis(), toSQL(rng, source, target), toSQL(rng, target, source));
		}
		if (s instanceof RelativeLengthSelector) {
			RelativeLengthSelector rsl = (RelativeLengthSelector) s;
			Double ratio = rsl.ratio();
			String result = "";
			result += "(" + source + ".signature[1] >  " + (1 - ratio) + "*" + target + ".signature[1] ";
			result += " AND " + source + ".signature[1] <= " + (1 + ratio) + "*" + target + ".signature[1]) ";
			result += " OR (" + target + ".signature[1] >  " + (1 - ratio) + "*" + source + ".signature[1] ";
			result += " AND " + target + ".signature[1] <= " + (1 + ratio) + "*" + source + ".signature[1]) ";
			return result;
		}
		if (s instanceof NotSelector) {
			NotSelector n = (NotSelector) s;
			return "NOT (" + toSQL(n.child(), source, target) + ") ";
		}
		if (s instanceof LabelSelector) {
			LabelSelector l = (LabelSelector) s;
			String result = "";
			String selectValue = stripQuotes(l.value()); 
			if (l.value() != null) {
				String axisName = (l.axis() == Axis.TARGET ? target : source); 
				result = axisName + ".label = '" + selectValue + 
						"' and md5(" + axisName + ".label)=md5('" + selectValue + "') ";
			}
			if (l.axis() == Axis.EITHER || l.axis() == Axis.BOTH) {
				result += !result.isEmpty() ? " AND " : ""; //TODO why use AND for both EITHER and BOTH?
				result += source + ".label = " + target + ".label";
			}
			return result;
		}
		if (s instanceof SameNormaLabelSelector) {
			String result = source + ".normalabel = " + target + ".normalabel";
			return result;
		}
		if (s instanceof SameStoredPrefixSelector) {
			String result = source + ".labelprefix = " + target + ".labelprefix";
			return result;
		}
		if (s instanceof TypeSelector) {
			TypeSelector t = (TypeSelector) s;
			String result = "";
			if (t.type() != null) {
				result = (t.axis() == Axis.TARGET ? target : source) + ".type IN ('" + Joiner.on("','").join(t.type())
						+ "')";
			}
			if (t.axis() == Axis.EITHER || t.axis() == Axis.BOTH) {
				result += !result.isEmpty() ? " AND " : "";
				result += source + ".type = " + target + ".type";
			}
			return result;
		}
		if (s instanceof IsStringSelector) {
			IsStringSelector n = (IsStringSelector) s;
			return splitAxis(n.axis(), source + ".signature[2] = 0", target + ".signature[2] = 0");
		}
		if (s instanceof IsBooleanSelector) {
			IsBooleanSelector n = (IsBooleanSelector) s;
			return splitAxis(n.axis(), source + ".signature[2] = 1", target + ".signature[2] = 1");
		}
		if (s instanceof IsDateTimeSelector) {
			IsDateTimeSelector n = (IsDateTimeSelector) s;
			return splitAxis(n.axis(), source + ".signature[2] = 3", target + ".signature[2] = 3");
		}
		if (s instanceof IsNumericSelector) {
			IsNumericSelector n = (IsNumericSelector) s;
			return splitAxis(n.axis(), source + ".signature[2] = 2", target + ".signature[2] = 2");
		}
		if (s instanceof IsURISelector) {
			IsURISelector n = (IsURISelector) s;
			return splitAxis(n.axis(), source + ".type = " + RDF_URI.ordinal(),
					target + ".type = " + RDF_URI.ordinal());
		}
		if (s instanceof CommonWordsSelector) {
			if (((CommonWordsSelector) s).useIndex()) {
				return source + ".id IN (SELECT k1.id FROM keyword_index k1, keyword_index k2 " + " WHERE k2.id = "
						+ target + ".id AND k1.word = k2.word)";
			}
			return "regexp_split_to_array(lower(" + source + ".label), E'\\\\W') " + "&& regexp_split_to_array(lower("
			+ target + ".label), E'\\\\W')";
		}
		if (s instanceof CommonPrefixSelector) {
			return "substring(" + source + ".label from 0 for " + (((CommonPrefixSelector) s).length() + 1) + ") "
					+ "= substring(" + target + ".label from 0 for " + (((CommonPrefixSelector) s).length() + 1) + ")";
		}
		throw new IllegalStateException("Unsupported selector " + s.getClass().getSimpleName());
	}

	private String toSQL(Range rng, String source, String target) {
		String result = "(";
		String sep = "";
		if (rng.hasLowerBound()) {
			result += source + ".signature[1] " + (rng.lowerBoundType() == OPEN ? ">=" : ">") + rng.lowerEndpoint();
			sep = " AND ";
		}
		if (rng.hasUpperBound()) {
			result += sep + source + ".signature[1] " + (rng.upperBoundType() == OPEN ? "<=" : "<")
					+ rng.upperEndpoint();
		}
		result += ")";
		return result;
	}

	private String splitAxis(Axis axis, String left, String right) {
		String result = "";
		if (axis != Axis.TARGET) {
			result += left;
		}
		if (axis == Axis.BOTH) {
			result += " AND ";
		} else if (axis == Axis.EITHER) {
			result += " OR ";
		}
		if (axis != Axis.SOURCE) {
			result += right;
		}
		return result;
	}

	/** For testing purpose only */
	public void checkClustersConsistency() {
		String query = " SELECT COUNT(DISTINCT label), COUNT(DISTINCT representative) FROM " + nodesTableName
				+ " WHERE label <> '' AND type IN (" + ENTITY_TYPES + ")";
		PreparedStatement stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				int labels = rs.getInt(1);
				int reps = rs.getInt(2);
				assert labels == reps : "# distinct labels (" + labels + ") does not match # distinct representatives ("
						+ reps + ")";
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		query = " SELECT COUNT(DISTINCT label), COUNT(DISTINCT representative) FROM " + nodesTableName
				+ " WHERE label <> '' AND type NOT IN (" + ENTITY_TYPES + ")";
		stmt = getPreparedStatement(query);
		try {
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				int labels = rs.getInt(1);
				int reps = rs.getInt(2);
				assert labels == reps : "# distinct labels (" + labels + ") does not match # distinct representatives ("
						+ reps + ")";
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void reset() {
		super.reset();
		nodeCache.invalidateAll();
		nodeCacheByLabel.invalidateAll();
		edgeCache.invalidateAll();
//		inDegreeCache.invalidateAll();
//		outDegreeCache.invalidateAll();
		incomingCache.invalidateAll();
		outgoingCache.invalidateAll();
		sameAsCache.invalidateAll();
		nodeIdCounter = 0;
	}

	private void deleteTablesForOneGraph(GraphType typeOfGraph) {
		// ORIGINAL_GRAPH, ABSTRACT_GRAPH, NORMALIZED_GRAPH, SUMMARY_GRAPH, CLASSIFIED_GRAPH
		// see enum in fr.inria.cedar.connectionlens.graph.Graph.GraphType
		String graphType = typeOfGraph.toString().substring(0, typeOfGraph.toString().length() - 5);
		if(graphType.equals("ORIGINAL_") || graphType.equals("ABSTRACT_") || graphType.equals("NORMALIZED_") || graphType.equals("SUMMARY_") || graphType.equals("CLASSIFIED_")) {
			// log.info("DROP TABLES FOR GRAPH " + graphType);
			Object tmpSchema = new SchemaTableNames();
			Field[] fields = SchemaTableNames.class.getFields();
			for(Field field : fields) {
				try {
					String fieldName = field.getName();
					if(fieldName.startsWith(graphType)) { // get only the tables which correspond to the type of graph
						Field correspondingField = tmpSchema.getClass().getField(fieldName);
						String tableName = correspondingField.get(tmpSchema).toString();
						if (tableName.contains("bidiredges") || tableName.contains("same_as_edges")) {
							executeUpdate("DROP VIEW IF EXISTS " + tableName + " CASCADE");
						}
						// log.info("drop table " + tableName);
						executeUpdate("DROP TABLE IF EXISTS " + tableName + " CASCADE");
					}
				} catch (Exception e) {
					log.info(e);
				}
			}
		}
	}

	@Override
	protected void drop() {
		this.deleteTablesForOneGraph(this.thisGraphType);

		executeUpdate("DROP TABLE IF EXISTS prenoms CASCADE");

		// use the current's graph table names:
		executeUpdate("DROP VIEW IF EXISTS " + biDirEdgesTableName + " CASCADE");
		executeUpdate("DROP VIEW IF EXISTS " + sameAsEdgesTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + edgesTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + specificityTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + weakSameAsEdgesTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + nodesTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + specStatTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + computedSpecificityTableName + " CASCADE");
		executeUpdate("DROP TABLE IF EXISTS " + disambiguatedTableName + " CASCADE");

		this.createdInPreviousRun = false;
		log.info("Database has been dropped, createdInPreviousRun is false");
	}

	@Override
	protected void create() {
		log.info("CREATE TABLE " + nodesTableName);
		executeUpdate("CREATE TABLE " + nodesTableName
				+ "(id NUMERIC, ds integer, type smallint, label varchar, normalabel varchar, labelprefix varchar, representative NUMERIC, signature integer[1], primary key(id))");

//		log.info("CREATE TABLE " + edgesTableName);
		executeUpdate("CREATE TABLE " + edgesTableName
				+ "(id SERIAL, ds integer, type smallint, source NUMERIC, target NUMERIC, label varchar, confidence float, signature integer[1], primary key (id, label))");

		if(this.isOriginal() || this.isAbstract() || this.isClassified() || Config.getInstance().getBooleanProperty("compare_nodes")) {
			// we create such table in abstraction only if we compare nodes
			// we always create this table in the usual case (no abstraction)
//			log.info("CREATE TABLE " + weakSameAsEdgesTableName);
			executeUpdate("CREATE TABLE " + weakSameAsEdgesTableName
					+ "(id NUMERIC, node1 NUMERIC, ds1 integer, node2 NUMERIC, ds2 integer, confidence float, primary key(id))");
		}

		if(this.isOriginal() || this.isAbstract() || this.isClassified()) {
			// we create the specificity table only for the usual case (no abstraction)
			// because we do not need it in the abstraction
			// but we need it for the GUI
//			log.info("CREATE TABLE " + specificityTableName);
			executeUpdate("CREATE TABLE " + specificityTableName
					+ "(representative NUMERIC, edge_label varchar, nIn float, nOut float, primary key (representative, edge_label))");
		}

		// we create indexes on nodes and edges only in the usual case
		if(this.isOriginal()) {
			this.createIndexes();
		}

		if(this.isOriginal() || this.isAbstract()) {
			// as above, we create this table only in the usual case
			// since we do not need it in the abstraction
			executeUpdate("CREATE VIEW " + biDirEdgesTableName + " AS SELECT source, id, target, label FROM "
					+ edgesTableName + " UNION SELECT target, id, source, label FROM " + edgesTableName);

			// such updates are also only needed in the usual case
			StaticStatements ss = new StaticStatements();
			if (Config.getInstance().getBooleanProperty("storage_use_materialized_strong_same_as_edges")) {
				executeUpdate(ss.MATERIALIZED_SAME_AS_EDGES);
			} else {
				executeUpdate(ss.VIRTUAL_SAME_AS_EDGES);
			}
		}

		createdInPreviousRun = false;
	}

	public void createIndexes() {
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_rep ON " + nodesTableName + " (representative)");
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_type ON " + nodesTableName + " (type)");
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_ds_id ON " + nodesTableName + " (ds, id)");
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_label_md5 ON " + nodesTableName + "(md5(label))"); // See issue #464

		if (Config.getInstance().getBooleanProperty("entity_disambiguation")) {
			executeUpdate("CREATE TABLE " + disambiguatedTableName + " (entityID integer , ambiURI varchar)");
		}

		executeUpdate("CREATE INDEX " + weakSameAsEdgesTableName + "_node1 ON " + weakSameAsEdgesTableName + " (node1)");

		executeUpdate("CREATE INDEX " + edgesTableName + "_ds_id ON " + edgesTableName + " (ds, id)");
		executeUpdate("CREATE INDEX s" + edgesTableName + " on " + edgesTableName + " (source)");
		executeUpdate("CREATE INDEX t" + edgesTableName + " on " + edgesTableName + " (target)");
	}

	@Override
	/** For performance, it is better to call this at the end of the graph storage but before comparisons */
	public void createNodeLabelPrefixIndex() {
		//log.info("Creating label prefix index");
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_prefix ON " + nodesTableName + " (labelprefix)");
	}

	@Override
	public void createDisambiguatedIndex() {
		if (Config.getInstance().getBooleanProperty("entity_disambiguation")) {
			executeUpdate("CREATE INDEX IF NOT EXISTS " + disambiguatedTableName + "_entityID ON "
					+ disambiguatedTableName + " (entityID)");
			executeUpdate("CREATE INDEX IF NOT EXISTS " + disambiguatedTableName + "_ambiURI ON "
					+ disambiguatedTableName + " (ambiURI)");
			//lateDisambiguatedIndex = true;
		}
	}

	@Override
	public void close() {
		try {
			for (String key : statementMap.keySet()) {
				PreparedStatement stmt = statementMap.get(key);
				stmt.close();
				statementMap.remove(key);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		if (abstractGraph != null) {
			abstractGraph.close();
		}
		// log.info("Asked " + this.resolveSpecCount + " specificity resolve queries and
		// " + this.resolveNodeCount + " resolve node queries");
	}

	@Override
	/** Point query, indirect node caching through resolveNode */
	public Node resolveEntityNode(String entityNodeLabel) {
		String sqlLabel = stripQuotes(entityNodeLabel); 
		String query = "SELECT l.id, l.representative, l.label, l.normalabel, l.type FROM " + nodesTableName
				+ " l WHERE l.label = ? and md5(l.label)=md5(?) and l.type in ("
				+ ENTITY_PERSON.ordinal() + ", " + ENTITY_ORGANIZATION.ordinal() + ", "
				+ ENTITY_LOCATION.ordinal() + ", " + FIRST_NAME.ordinal() + ", " + EMAIL.ordinal() + ", " +
				+ HASHTAG.ordinal() + ", " + MENTION.ordinal() + ", " + RDF_URI.ordinal() + ", "
				+ DATE.ordinal()+ ") limit 1";
		try {
			PreparedStatement stmt = getPreparedStatement(query);
			stmt.setString(1, sqlLabel);
			stmt.setString(2, sqlLabel); 
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				// this will restore the original node label
				return resolveNode(getFactory().parseNodeID(rs.getInt(1)));
			}
		} catch (Exception e) {
			throw new IllegalStateException("Could not identify entity node: " + e.toString());
		}
		return null;
	}

	@Override
	/** This encapsulates an SQL query so it is the right place to escape the label 
	 * Point query, indirect node caching through resolveNode 
	 */
	public Node resolveNodeByLabel(String label) {
		String sqlLabel = stripQuotes(label); 
		String query = "SELECT l.id, l.representative, l.label, l.normalabel FROM " + nodesTableName
				+ " l WHERE l.label = ? and md5(l.label)=md5(?) limit 1";
		try {
			PreparedStatement stmt = getPreparedStatement(query);
			stmt.setString(1, sqlLabel); 
			stmt.setString(2, sqlLabel); 
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				// this will restore the original node label
				return resolveNode(getFactory().parseNodeID(rs.getInt(1)));
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	@Override
	/** Point query, indirect node caching through resolveNode */
	public Boolean computeSimilarDisambiguatedEntities(Consumer<Edge> processor) {
		String query = "SELECT t1.entityID as e1, t2.entityID as e2"
				+ " FROM disambiguatedinto t1, disambiguatedinto t2 " + " WHERE t1.ambiURI=t2.ambiURI "
				+ " AND t1.entityID <> t2.entityID " + " AND NOT EXISTS " + " (" + "  SELECT * FROM nodes n1, nodes n2 "
				+ "  WHERE n1.id=t1.entityID " + "  AND n2.id=t2.entityID "
				+ "  AND n1.representative=n2.representative" + " )";

		try {
			PreparedStatement stmt = getPreparedStatement(query);
			ResultSet rs = executeQuery(stmt);
			while (rs.next()) {
				Node n1 = resolveNode(rs.getInt(1));
				Node n2 = resolveNode(rs.getInt(2));
				processor.accept(n1.getDataSource().buildSameAsEdge(n1, n2));
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public IndexAndProcessNodes index() {
		return index;
	}

	public boolean isOnDisk(NodeID nodeID) {
		return (nodesAlreadyOnDisk.contains(nodeID));
	}

	public void setOnDisk(NodeID nodeID) {
		nodesAlreadyOnDisk.add(nodeID);
	}

	@Override
	public void addToDisambiguatedEntities(NodeID nodeID, String url) {
		disambiguatedEntitiesNode.put(nodeID, url);
	}

	@Override
	public HashMap<NodeID, String> getDisambiguatedEntitiesNode() {
		return disambiguatedEntitiesNode;
	}

	/**
	 * This method relies on, and adds to, a label-based caching that is used only when
	 * creating only one text node for each label. 
	 * It updates the memory of this loading as well as the knowledge of nodes on disk
	 */
	@Override
	public Node getNodeFromCacheOrStorage(String label) {
		Node node = nodesUsedInThisLoading.get(label);
		if (node == null && createdInPreviousRun) { // If the graph has just been created, no point in looking on disk
			// IM, 11/4/22: double check all the caches, how they empty and how they fill? TODO
			// log.info(label + " not in memory cache");
			node = resolveNodeByLabel(label); // was: label
			if (node != null) {
				// log.info("===> Found the node " + label + " in the graph with ID " +
				// node.getId() + ", cached it!");
				addToNodeCache(label, node); // was: label
				nodesAlreadyOnDisk.add(node.getId());
			}
		}
		//long t2 = System.currentTimeMillis(); 		
		return node;
	}

	public void addToNodeCache(String label, Node node) {
		if (nodesUsedInThisLoading.keySet().size() < MAX_NODES_IN_GRAPH_MEMORY) {
			nodesUsedInThisLoading.put(label, node);
		} else if (!cacheFullNotified) {
			log.info("A maximum number of " + MAX_NODES_IN_GRAPH_MEMORY
					+ " nodes have been stored in the graph in-memory cache;");
			log.info("registration will proceed correctly but probably be a bit slower.");
			log.info("You may consider increasing the value of the update_batch_size parameter.");
			cacheFullNotified = true;
		}
	}

	@Override
	public RelationalGraph getNormalizedGraph() {
		if(this.normalizedGraph == null) {
			this.normalizedGraph = new RelationalGraph(this.factory, this.catalog, GraphType.NORMALIZED_GRAPH, this.extractor, this.mas, this.statsLog);
		}
		if(this.normalizedGraph.index() == null) {
			((RelationalGraph) this.normalizedGraph).buildIndex(this.factory, this.catalog);
		}
		return this.normalizedGraph;
	}

	@Override
	public RelationalGraph getSummaryGraph() {
		if(this.summaryGraph == null) {
			this.summaryGraph = new RelationalGraph(this.factory, this.catalog, GraphType.SUMMARY_GRAPH, this.extractor, this.mas, this.statsLog);
		}
		if(this.summaryGraph.index() == null) {
			((RelationalGraph) this.summaryGraph).buildIndex(this.factory, this.catalog);
		}
		return this.summaryGraph;
	}

	@Override
	public Graph getAbstractGraph() {
		// IM, 21/8/20: the two blocks below make sure the abstract graph exists in memory before returning it
		if (this.abstractGraph == null) {
			this.abstractGraph = new RelationalGraph(this.getFactory(), this.catalog, GraphType.ABSTRACT_GRAPH,
					this.extractor, this.mas, this.statsLog);
		}
		if (this.abstractGraph.index() == null) {
			((RelationalGraph) this.abstractGraph).buildIndex(this.getFactory(), this.catalog);
		}
		return this.abstractGraph;
	}

	@Override
	public RelationalGraph getClassifiedGraph() {
		if(this.classifiedGraph == null) {
			this.classifiedGraph = new RelationalGraph(this.factory, this.catalog, GraphType.CLASSIFIED_GRAPH, this.extractor, this.mas, this.statsLog);
		}
		if(this.classifiedGraph.index() == null) {
			this.classifiedGraph.buildIndex(this.factory, this.catalog);
		}
		this.classifiedGraph.reset(); // to init it and create appropriate tables
		return this.classifiedGraph;
	}

	public void setClassifiedGraph(RelationalGraph classifiedGraph) {
		this.classifiedGraph = classifiedGraph;
	}

	@Override
	public boolean isOriginal() {
		return this.thisGraphType == GraphType.ORIGINAL_GRAPH;
	}

	@Override
	public boolean isNotOriginal() {
		return (this.isAbstract() || this.isNormalized() || this.isSummary() || this.isClassified());
	}

	@Override
	public boolean isAbstract() {
//		log.info(this.thisGraphType);
		return this.thisGraphType == GraphType.ABSTRACT_GRAPH;
	}

	@Override
	public boolean isNormalized() {
		return this.thisGraphType == GraphType.NORMALIZED_GRAPH;
	}

	@Override
	public boolean isSummary() {
		return this.thisGraphType == GraphType.SUMMARY_GRAPH;
	}

	@Override
	public boolean isClassified() {
		return this.thisGraphType == GraphType.CLASSIFIED_GRAPH;
	}

	@Override
	public GraphType getGraphType() { return this.thisGraphType; }

	@Override
	public void updateAbstractGraph(RelationalGraph g) {
		this.abstractGraph = g;
	}

	@Override
	public void updateNormalizedGraph(RelationalGraph g) {
		this.normalizedGraph = g;
	}

	@Override
	public void updateGraph(RelationalGraph g, GraphType graphType) {
		if(graphType == GraphType.ABSTRACT_GRAPH) {
			this.abstractGraph = g;
		} else if (graphType == GraphType.NORMALIZED_GRAPH) {
			this.normalizedGraph = g;
		} else if (graphType == GraphType.SUMMARY_GRAPH) {
			 this.summaryGraph = g;
		} else if(graphType == GraphType.CLASSIFIED_GRAPH) {
			this.classifiedGraph = g;
		}
	}

	@Override
	public void updateSummaryGraph(RelationalGraph g) { this.summaryGraph = g; }

	@Override
	public void updateClassifiedGraph(RelationalGraph g) { this.classifiedGraph = g; }

	private ExtractionPolicy createExtractionPolicy() {
		return new ExtractionPolicy(Config.getInstance().getProperty("extract_policy"));
	}

	@Override
	public ExtractionPolicy extractPolicy() {
		return extractPolicy;
	}

	@Override
	public void setCatalog(DataSourceCatalog catalog) {
		this.index().setCatalog(catalog); 
		this.catalog = catalog; 
	}
	@Override 
	public DataSourceCatalog getCatalog() {
		return catalog;
	}

	public Factory getFactory() {
		return factory;
	}

	@Override
	public String toString() {
		return "RelationalGraph{" +
				"index=" + index +
				", nodesTableName='" + nodesTableName + '\'' +
				", edgesTableName='" + edgesTableName + '\'' +
				", abstractGraph=" + abstractGraph +
				", normalizedGraph=" + normalizedGraph +
				", summaryGraph=" + summaryGraph +
				", classifiedGraph=" + classifiedGraph +
				'}';
	}
}

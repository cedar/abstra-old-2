package fr.inria.cedar.connectionlens.abstraction;

//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.log4j.Logger;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class AbstractionsComparator extends AbstractionTask {
    public static final Logger log = Logger.getLogger(AbstractionsComparator.class);

    public String pathERSchemaReference;
    public String pathERSchemaCompared;
    public ArrayList<CollectionBoundary> referenceEntities; // < collLabel, <prop1, prop2, ..., propN > >
    public ArrayList<CollectionBoundary> comparedEntities;
    public HashMap<String, ArrayList<String>> referenceRelationships; // < collS-collT, relationshipLabel >
    public HashMap<String, ArrayList<String>> comparedRelationships;
    public double precisionMainEntities = 0.0d;
    public double recallMainEntities = 0.0d;
    public double F1scoreMainEntities = 0.0d;
    public double precisionRelationships = 0.0d;
    public double recallRelationships = 0.0d;
    public double F1scoreRelationships = 0.0d;
    public double precisionBoundaries = 0.0d;
    public double recallBoundaries = 0.0d;
    public double F1scoreBoundaries = 0.0d;

    public AbstractionsComparator() {
        String tmpDir = Config.getInstance().getStringProperty("temp_dir");
        this.pathERSchemaReference = Paths.get(tmpDir, "abstraction_" + AbstractionTask.getDatasetName() + "_reference.json").toString();
        log.debug(this.pathERSchemaReference);
        this.referenceEntities = new ArrayList<>();
        this.referenceRelationships = new HashMap<>();
        this.comparedEntities = new ArrayList<>();
        this.comparedRelationships = new HashMap<>();
    }

    // read the current abstraction to add the collection boundaries to this.comparedSchemaEntities
    public void readCurrentAbstraction() {
        // add entities
        this.comparedEntities.addAll(CollectionGraph.getInstance().getCollectionsBoundaries().values());
        log.debug("compared entities: " + this.comparedEntities);

        // add relationships
        for(Map.Entry<String, ArrayList<String>> entry : CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections().entrySet()) {
            log.debug("rel key is: "+entry.getKey());
            String[] keyArray = entry.getKey().split("-"); // "c1-c2" gives ["c1","c2"]
            String collSourceLabel = CollectionGraph.getInstance().getCollectionLabel(Integer.parseInt(keyArray[0]));
            String collTargetLabel = CollectionGraph.getInstance().getCollectionLabel(Integer.parseInt(keyArray[1]));
            this.comparedRelationships.put(collSourceLabel + "-" + collTargetLabel, entry.getValue());
        }
        log.debug("compared relationships: " + this.comparedRelationships);
    }

    // read the reference file of the abstraction.
    // create a CollectionBoundary for each main entity and fill the HashMap with relationships
    public void readAbstractionReference() {
        try {
            // read the JSON file
            JSONParser parser = new JSONParser();
            JSONObject data;
            data = (JSONObject) parser.parse(new FileReader(this.pathERSchemaReference));
            // read relationships
            for (Object entity : (JSONArray) data.get("relationships")) {
                JSONArray relationshipArray = (JSONArray) entity;
                String collectionLabelSource = relationshipArray.get(0).toString();
                String collectionLabelTarget = relationshipArray.get(1).toString();
                String relationshipLabel = relationshipArray.get(2).toString();
                ArrayList<String> relationshipsLabels = new ArrayList<>();
                relationshipsLabels.add(relationshipLabel);
                this.referenceRelationships.put(collectionLabelSource + "-" + collectionLabelTarget, relationshipsLabels);
            }

            // read entities
            for (Object entity : (JSONArray) data.get("entities")) {
                JSONObject boundaryObject = (JSONObject) entity;
                CollectionBoundary collectionBoundary = CollectionBoundary.getCollectionBoundaryFromEntityObjectMain(boundaryObject, this.referenceRelationships);
                log.debug("reference entity is: " + collectionBoundary);
                this.referenceEntities.add(collectionBoundary);
            }
            log.debug("reference entities are: " + this.referenceEntities);

            // read relationships
            for (Object entity : (JSONArray) data.get("relationships")) {
                JSONArray relationshipArray = (JSONArray) entity;
                String collectionLabelSource = relationshipArray.get(0).toString();
                String collectionLabelTarget = relationshipArray.get(1).toString();
                String relationshipLabel = relationshipArray.get(2).toString();
                String strKey = collectionLabelSource + "-" + collectionLabelTarget;
                ArrayList<String> relationships;
                if(this.referenceRelationships.containsKey(strKey)) {
                    relationships = this.referenceRelationships.get(strKey);
                } else {
                    relationships = new ArrayList<>();
                }
                relationships.add(relationshipLabel);
                this.referenceRelationships.put(strKey, relationships);
            }
            log.debug("reference relationships are: " + this.referenceRelationships);
        } catch(IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    // return the list of root label of each entity in the list of given entities
    public ArrayList<String> getRootEntitiesLabels(ArrayList<CollectionBoundary> entities) {
        ArrayList<String> entityLabels = new ArrayList<>();
        for(CollectionBoundary cb: entities) {
            entityLabels.add(cb.getMainCollectionRoot().getLabel());
        }
        log.debug(entityLabels);
        return entityLabels;
    }

    // this will transform the list of collection boundaries to a hashmap
    // storing for each main root label its collection boundary
    public HashMap<String, CollectionBoundary> associateEachBoundaryToItsMainRootLabel(ArrayList<CollectionBoundary> entities) {
        HashMap<String, CollectionBoundary> entitiesWithTheirBoundaries = new HashMap<>();
        for(CollectionBoundary cb: entities) {
            entitiesWithTheirBoundaries.put(cb.getMainCollectionRoot().getLabel(), cb);
        }
        return entitiesWithTheirBoundaries;
    }

    // ***** PRECISION AND RECALL OF MAIN ENTITIES
    // compute precision of entities, i.e. compute how many entities are similar by comparing them by their labels
    public double computeEntityPrecision() {
        log.warn("COMPUTE PRECISION OF MAIN ENTITIES");
        ArrayList<String> labelsOfMainEntitiesReference = this.getRootEntitiesLabels(this.referenceEntities);
        ArrayList<String> labelsOfMainEntitiesCompared = this.getRootEntitiesLabels(this.comparedEntities);
        ArrayList<String> intersectionMainEntitiesLabels = Utils.intersectionListsOfStrings(labelsOfMainEntitiesReference, labelsOfMainEntitiesCompared);
        log.debug("intersection of " + labelsOfMainEntitiesReference + " and " + labelsOfMainEntitiesCompared + " is: " + intersectionMainEntitiesLabels);
        return (double) intersectionMainEntitiesLabels.size() / (double) this.comparedEntities.size();
    }

    // compute recall of entities
    public double computeEntityRecall() {
        log.warn("COMPUTE RECALL OF MAIN ENTITIES");
        ArrayList<String> labelsOfMainEntitiesReference = this.getRootEntitiesLabels(this.referenceEntities);
        ArrayList<String> labelsOfMainEntitiesCompared = this.getRootEntitiesLabels(this.comparedEntities);
        ArrayList<String> intersectionMainEntitiesLabels = Utils.intersectionListsOfStrings(labelsOfMainEntitiesReference, labelsOfMainEntitiesCompared);
        log.debug("intersection of " + labelsOfMainEntitiesReference + " and " + labelsOfMainEntitiesCompared + " is: " + intersectionMainEntitiesLabels);
        return (double)intersectionMainEntitiesLabels.size() / (double)this.referenceEntities.size();
    }

    // ***** PRECISION AND RECALL OF MAIN ENTITIES BOUNDARIES
    // compute precision for main entities boundaries
    public double computeBoundaryPrecision() {
        log.warn("COMPUTE PRECISION OF MAIN ENTITIES BOUNDARIES");
        HashMap<String, CollectionBoundary> collectionBoundaryHashMapReference = this.associateEachBoundaryToItsMainRootLabel(this.referenceEntities);
        HashMap<String, CollectionBoundary> collectionBoundaryHashMapCompared = this.associateEachBoundaryToItsMainRootLabel(this.comparedEntities);

        double avgBoundPrecision = 0.0d;
        double totBoundSize = 0.0d;
        // iterate over the main entities of the compared collections to get their boundaries
        for(String mainEntity: collectionBoundaryHashMapCompared.keySet()) {
            // get the boundary of this main entity
            CollectionBoundary cbCompared = collectionBoundaryHashMapCompared.get(mainEntity);
            ArrayList<String> cbComparedList = cbCompared.getNonLeafCollectionsInBoundaryStrings();
            cbComparedList.removeAll(this.referenceRelationships.values());
            // sum its boundary size
            totBoundSize += cbComparedList.size();
            // if this entity is in the reference
            if(collectionBoundaryHashMapReference.containsKey(mainEntity)) {
                CollectionBoundary cbReference = collectionBoundaryHashMapReference.get(mainEntity);
                ArrayList<String> cbReferenceList = cbReference.getNonLeafCollectionsInBoundaryStrings();

                log.debug("reference boundary: " + cbReferenceList);
                log.debug("reference boundary: " + cbReference.getNonLeafCollectionsInBoundary());
                log.debug("compared boundary: " + cbComparedList);
                log.debug("compared boundary: " + cbCompared.getNonLeafCollectionsInBoundary());

                // compute the common boundary nodes
                ArrayList<String> intersectionBoundaryNodes = Utils.intersectionListsOfStrings(cbReferenceList, cbComparedList);
                log.debug(intersectionBoundaryNodes);
                //double boundaryPrecision = (double) intersectionBoundaryNodes.size() / (double) cbCompared.getNonLeafCollectionsInBoundaryStrings().size();
                // sum the common boundary nodes
                avgBoundPrecision += intersectionBoundaryNodes.size();
            }
        }
        // compute average over the total boundary size of compared boundaries
        avgBoundPrecision /= totBoundSize;
        return avgBoundPrecision;
    }

    // compute recall for entity boundaries
    public double computeBoundaryRecall() {
        log.warn("COMPUTE RECALL OF MAIN ENTITIES BOUNDARIES");
        HashMap<String, CollectionBoundary> collectionBoundaryHashMapReference = this.associateEachBoundaryToItsMainRootLabel(this.referenceEntities);
        HashMap<String, CollectionBoundary> collectionBoundaryHashMapCompared = this.associateEachBoundaryToItsMainRootLabel(this.comparedEntities);

        double avgBoundRec = 0.0d;
        double totBoundSize = 0.0d;
        // iterate over the main entities of the reference collections to get their boundaries
        for(String mainEntity: collectionBoundaryHashMapReference.keySet()) {
            log.debug(mainEntity);
            // get the boundary of this main entity
            CollectionBoundary cbReference = collectionBoundaryHashMapReference.get(mainEntity);
            ArrayList<String> cbReferenceList = cbReference.getNonLeafCollectionsInBoundaryStrings();
            // sum its boundary size
            totBoundSize += cbReference.getNonLeafCollectionsInBoundaryStrings().size();
            log.debug("totBoundSize: "+totBoundSize);
            // if this entity is in compared
            if(collectionBoundaryHashMapCompared.get(mainEntity) != null) {
                CollectionBoundary cbCompared = collectionBoundaryHashMapCompared.get(mainEntity);

                ArrayList<String> cbComparedList = cbCompared.getNonLeafCollectionsInBoundaryStrings();
                cbComparedList.removeAll(this.referenceRelationships.values());
                log.debug("reference boundary: " + cbReferenceList);
                log.debug("compared boundary: " + cbComparedList);
                // compute the common boundary nodes
                Set<String> intersectionBoundaryNodes = Utils.intersectionSetsOfStrings(new HashSet<>(cbReferenceList), new HashSet<>(cbComparedList));
                log.debug(intersectionBoundaryNodes);
                // sum the common boundary nodes
                avgBoundRec += intersectionBoundaryNodes.size();
                log.debug("intersection size: "+intersectionBoundaryNodes.size());
            }
        }
        // compute average over the total boundary size of reference boundaries
        avgBoundRec /= totBoundSize;
        return avgBoundRec;
    }

    private double computeF1score(double precision, double recall) {
        if(precision == 0.0d || recall == 0.0d) {
            return 0.0d;
        } else {
            return 2 / ((1/precision) + (1/recall));
        }
    }

    public void compare() {
        this.readCurrentAbstraction();
        this.readAbstractionReference();

        log.debug(this.referenceEntities);
        log.debug(this.comparedEntities);
        log.debug(this.referenceRelationships);
        log.debug(this.comparedRelationships);
        // ***** ENTITIES *****
        this.precisionMainEntities = this.computeEntityPrecision();
        this.recallMainEntities = this.computeEntityRecall();
        this.F1scoreMainEntities = this.computeF1score(this.precisionMainEntities, this.recallMainEntities);

        // ***** RELATIONSHIPS *****
        // TODO NELLY: does it take into account the label of the relationship?
        // TODO NELLY: moreover, there might be several relationships between two main entities (an author has written some papers and an author has reviewed some papers)
        if(this.comparedRelationships.isEmpty() || this.referenceRelationships.isEmpty()) {
            this.precisionRelationships = 0.0d;
            this.recallRelationships = 0.0d;
            this.F1scoreRelationships = 0.0d;
        } else {
            Set<String> intersectionRelNodes = Utils.intersectionSetsOfStrings(this.referenceRelationships.keySet(), this.comparedRelationships.keySet());
            double relScore = 1.0d * intersectionRelNodes.size();

            for(String n: intersectionRelNodes) {
                if(this.referenceRelationships.get(n).equals(this.comparedRelationships.get(n))) {
                    relScore += 1;
                }
            }
            relScore /= 2;
            this.precisionRelationships = (double) relScore / (double) this.comparedRelationships.size();
            this.recallRelationships = (double) relScore / (double) this.referenceRelationships.size();
            this.F1scoreRelationships = this.computeF1score(this.precisionRelationships, this.recallRelationships);
        }

        // ***** BOUNDARIES *****
        this.precisionBoundaries = this.computeBoundaryPrecision();
        this.recallBoundaries = this.computeBoundaryRecall();
        this.F1scoreBoundaries = this.computeF1score(this.precisionBoundaries, this.recallBoundaries);
        log.info("F1 score of boundaries is: " + this.F1scoreBoundaries);

        // final print
        log.info("Main entities: [" + this.precisionMainEntities + ", " + this.recallMainEntities + ", " + this.F1scoreMainEntities + "]");
        log.info("Relationships: [" + this.precisionRelationships + ", " + this.recallRelationships + ", " + this.F1scoreRelationships + "]");
        log.info("Boundaries: [" + this.precisionBoundaries + ", " + this.recallBoundaries + ", " + this.F1scoreBoundaries + "]");
        log.info("F1-scores: [" + this.F1scoreMainEntities + ", " + this.F1scoreBoundaries + ", " + this.F1scoreRelationships + "]");
    }
}

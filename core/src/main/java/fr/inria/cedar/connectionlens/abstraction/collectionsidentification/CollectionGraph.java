package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.abstraction.Utils;
import fr.inria.cedar.connectionlens.abstraction.classification.Classification;
import fr.inria.cedar.connectionlens.abstraction.classification.ExtractedEntityType;
import fr.inria.cedar.connectionlens.abstraction.classification.ExtractedEntityTypes;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Predicate;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.NO_STATUS;

public class CollectionGraph {
    public static final Logger log = Logger.getLogger(CollectionGraph.class);
    private static CollectionGraph theOriginalCollectionGraph = null;
    private static CollectionGraph theCollectionGraphForMainCollectionsSelection = null;

    private RelationalGraph graph;
    private boolean isRdf;
//    private boolean startFromCollectionGraph = Config.getInstance().getBooleanProperty("start_from_collection_graph");

    private HashMap<Integer, ArrayList<CollectionEdge>> collectionGraph;
    private HashMap<Integer, HashMap<Integer, ArrayList<Path>>> allPathsBetweenCollections;
    private ArrayList<Integer> collectionsIds;
    private ArrayList<Integer> leafCollections; // tmp variable to store leaf collection ids
    private HashMap<Integer, Integer> collectionsSizes;
    private int nbNormalizedNodes;
    private int totalNbCollections;
    private HashMap<Integer, ArrayList<String>> collectionsTypes; // for each collection, the set of (extracted) types it has
    protected HashMap<Integer, Double> collectionsScores;
//    private HashMap<Integer, HashMap<Integer, Double>> edgeTransferFactors; // now only in the CollectionEdge class
    protected HashMap<Integer, HashMap<Integer, Double>> aggregatedPathTransferFactors;
    private ArrayList<Integer> collectionsInvolvedInACycle;
    private ArrayList<CollectionEdge> inCycleCollectionEdges = new ArrayList<>();
    protected HashMap<Integer, ReportStatus> collectionsStatuses = new HashMap<>();
    protected ArrayList<Integer> mainCollectionsList = new ArrayList<>();
    protected HashMap<String, ArrayList<String>> relationshipsBetweenMainEntityCollections = new HashMap<>();
    private  HashMap<Integer, CollectionBoundary> collectionsBoundaries; // < main collection root id, collections that belong to this collection (recursive structure) >
    private  ArrayList<Integer> collectionsInvolvedInABoundary; // < collection id that belong to any collection boundary >
    private HashMap<Integer, ArrayList<String>> collectionsLabels;
    private HashMap<Integer, HashMap<Integer, Double>> collectionsFrequencies;
    private HashMap<Integer, ArrayList<String>> attributeLeafCollectionsValues; // Arraylist contains distinct values
    private ArrayList<Integer> collectionsWithUniqueValues; // collections of XML attributes having unique values only
    protected HashMap<Integer, Integer> collectionsCategories = new HashMap<>(); // < collection id, category id>
    protected HashMap<String, HashMap<ExtractedEntityType, Integer[]>> collectionsEntityProfiles; // < key = collection id + property label, < Entity type, [total size of value nodes for key, total size of extracted entities for key, number of extracted entities for key] >
    private ArrayList<CollectionEdge> idRefEdges = new ArrayList<>(); // need to put the initialization there

    /**
     * This enums represents the status of the collection in the reporting phase.
     * First, all collections are NO_STATUS.
     * Then, the status is set to (REPORTED_TOP or) REPORTED if the collection is reported (in the top-k)
     * Or the status is set to NEVER_REPORTED if the collection is not reportable (e.g. contains one element, leaf collection, ...)
     */
    public enum ReportStatus implements Predicate<ReportStatus> {
        NO_STATUS,
        NON_REPORTABLE, // non-reportable (because null weight or collection of one element or collection of values or elements in collection do not have proper properties)
        REPORTED, // reported
        ;

        @Override
        public boolean test(ReportStatus reportStatus) {
            return this.compareTo(reportStatus) == 0;
        }
    }


    public CollectionGraph() {
    }

    public CollectionGraph(CollectionGraph originalVersion) {
        // we copy only the fields we need for the main collections selection process
        this.graph = originalVersion.graph;
        this.collectionGraph = originalVersion.collectionGraph;
        this.allPathsBetweenCollections = new HashMap<>(originalVersion.allPathsBetweenCollections);
        this.collectionsIds = new ArrayList<>(originalVersion.collectionsIds);
        this.leafCollections = new ArrayList<>(originalVersion.leafCollections);
        this.collectionsSizes = new HashMap<>(originalVersion.collectionsSizes);
        this.collectionsScores = new HashMap<>(originalVersion.collectionsScores);
//        this.edgeTransferFactors = new HashMap<>(originalVersion.edgeTransferFactors);
        this.aggregatedPathTransferFactors = new HashMap<>(originalVersion.aggregatedPathTransferFactors);
        this.collectionsInvolvedInACycle = new ArrayList<>(originalVersion.collectionsInvolvedInACycle);
        this.inCycleCollectionEdges = new ArrayList<>(originalVersion.inCycleCollectionEdges);
        log.debug(this.inCycleCollectionEdges);
        log.debug(originalVersion.inCycleCollectionEdges);
        this.collectionsStatuses = new HashMap<>(originalVersion.collectionsStatuses);
        this.mainCollectionsList = new ArrayList<>(originalVersion.mainCollectionsList);
        this.collectionsBoundaries = new HashMap<>(originalVersion.collectionsBoundaries);
        this.collectionsInvolvedInABoundary = new ArrayList<>(originalVersion.collectionsInvolvedInABoundary);
        this.collectionsLabels = new HashMap<>(originalVersion.collectionsLabels);
        this.collectionsTypes = new HashMap<>(originalVersion.collectionsTypes);
        this.idRefEdges = new ArrayList<>(originalVersion.idRefEdges);
        this.mainCollectionsList = new ArrayList<>(originalVersion.mainCollectionsList);
        this.collectionsInvolvedInABoundary = new ArrayList<>(originalVersion.collectionsInvolvedInABoundary);
        this.nbNormalizedNodes = originalVersion.nbNormalizedNodes;
        this.totalNbCollections = originalVersion.totalNbCollections;
        this.collectionsBoundaries = new HashMap<>(originalVersion.collectionsBoundaries);
        this.collectionsFrequencies = new HashMap<>(originalVersion.collectionsFrequencies);
        this.collectionsCategories = new HashMap<>(originalVersion.collectionsCategories);
        this.collectionsEntityProfiles = new HashMap<>(originalVersion.collectionsEntityProfiles);
        this.relationshipsBetweenMainEntityCollections = new HashMap<>(originalVersion.relationshipsBetweenMainEntityCollections);
        this.isRdf = originalVersion.isRdf;
    }

    public static CollectionGraph getInstance() {
        if(theOriginalCollectionGraph == null) {
            theOriginalCollectionGraph = new CollectionGraph();
        }
        return theOriginalCollectionGraph;
    }

    public static CollectionGraph getWorkingInstance() {
        if(theCollectionGraphForMainCollectionsSelection == null) {
            theCollectionGraphForMainCollectionsSelection = new CollectionGraph(theOriginalCollectionGraph);
        }
        return theCollectionGraphForMainCollectionsSelection;
    }

    private HashMap<Integer, HashMap<Integer, ArrayList<Path>>> init2DHashMapList() {
        HashMap<Integer, HashMap<Integer, ArrayList<Path>>> hashMap2D = new HashMap<>();
        for (int collI : this.collectionsIds) {
            hashMap2D.put(collI, new HashMap<>());
            for (int collJ : this.collectionsIds) {
                hashMap2D.get(collI).put(collJ, new ArrayList<>());
            }
        }
        return hashMap2D;
    }

    private HashMap<Integer, HashMap<Integer, Double>> init2DHashMapdouble() {
        HashMap<Integer, HashMap<Integer, Double>> hashMap2D = new HashMap<>();
        log.debug(this.collectionsIds);
        for (int collI : this.collectionsIds) {
            hashMap2D.put(collI, new HashMap<>());
            for (int collJ : this.collectionsIds) {
                hashMap2D.get(collI).put(collJ, 0.0d);
            }
        }
        return hashMap2D;
    }

    public void initializeAndGetVariablesInMemory(boolean initPathsEnumeration) throws SQLException {
        // init all the variables (except collectionsLabels that is already filled)
        this.leafCollections = new ArrayList<>();
        this.collectionsFrequencies = new HashMap<>();
        this.collectionsTypes = new HashMap<>();
        this.collectionsInvolvedInACycle = new ArrayList<>();
        this.collectionsBoundaries = new HashMap<>();
        this.collectionsInvolvedInABoundary = new ArrayList<>();
        this.collectionsScores = new HashMap<>();
        this.collectionsEntityProfiles = new HashMap<>();

        // get the id and the size of each collection
        // TODO NELLY: recompute the collections sizes?
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT c.collId, c.collSize FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c WHERE c.isActive = true;");
        ResultSet rs = stmt.executeQuery();

        this.collectionsSizes = new HashMap<>();
        while (rs.next()) {
            this.collectionsSizes.put(rs.getInt(1), rs.getInt(2));
        }

        rs = this.graph.getPreparedStatement("SELECT COUNT(*) FROM " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + ";").executeQuery();
        rs.next();
        this.nbNormalizedNodes = rs.getInt(1);

        rs = this.graph.getPreparedStatement("SELECT COUNT(*) FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + ";").executeQuery();
        rs.next();
        this.totalNbCollections = rs.getInt(1);

        // initialize it with empty ArrayLists to avoid NullPointerException
        if(initPathsEnumeration) {
            this.allPathsBetweenCollections = this.init2DHashMapList();
            this.inCycleCollectionEdges = new ArrayList<>();
        }
//        this.edgeTransferFactors = this.init2DHashMapdouble();
        this.aggregatedPathTransferFactors = this.init2DHashMapdouble();

        // set all collections as NO_STATUS
        this.collectionsStatuses = new HashMap<>();
        for(int c : this.collectionsIds) {
            this.collectionsStatuses.put(c, NO_STATUS);
        }
    }

    public void computeLeafCollections() throws SQLException {
        // compute leaf collections in memory and store them on disk
        this.leafCollections = new ArrayList<>();
        log.info("compute leaf collections");
        for(int collI : this.collectionsIds) {
            if(!this.collectionGraph.containsKey(collI)) {
                this.leafCollections.add(collI);
            }
        }
        log.debug(this.leafCollections);
        if(!this.leafCollections.isEmpty()) {
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " SET isLeaf = true WHERE collId IN (" + StringUtils.join(this.leafCollections, ",") + ")").execute();
        }
    }

    // this method get the collection graph in memory based on what is stored on disk.
    // if some collections are "inactive", they will be not retrieved (this is useful for the graph update part when selecting the main collections)
    public void retrieveCollectionGraphFromDisk() throws SQLException {
        PreparedStatement stmt = this.graph.getPreparedStatement("" +
                "SELECT cg.collEdgeId, cg.collIdSource, cg.collIdTarget, cg.isEdgeInCycle, cg.isEdgeComingFromIDREF, cg.isAtMostOne " +
                "FROM " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " cg, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " ci1, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " ci2 " +
                "WHERE cg.collIdSource = ci1.collId AND ci1.isActive = true AND cg.collIdTarget = ci2.collId AND ci2.isActive = true;");
        ResultSet rs = stmt.executeQuery();

        this.collectionGraph = new HashMap<>();
        this.collectionsIds = new ArrayList<>();

        while(rs.next()) {
            int collNodeSource = rs.getInt(2);
            int collNodeTarget = rs.getInt(3);
            CollectionEdge ce = new CollectionEdge(rs.getInt(1), collNodeSource, collNodeTarget, rs.getBoolean(5), rs.getBoolean(6));
            if(this.collectionGraph.containsKey(collNodeSource)) {
                this.collectionGraph.get(collNodeSource).add(ce);
            } else {
                this.collectionGraph.put(collNodeSource, new ArrayList<>());
                this.collectionGraph.get(collNodeSource).add(ce);
            }
            if(rs.getBoolean(4)) {
                this.inCycleCollectionEdges.add(ce);
            }
            if(!this.collectionsIds.contains(collNodeSource)) {
                this.collectionsIds.add(collNodeSource);
            }
            if(!this.collectionsIds.contains(collNodeTarget)) {
                this.collectionsIds.add(collNodeTarget);
            }
        }
        log.debug(this.collectionGraph);
        log.debug(this.collectionsIds);
    }

    public void writeCollectionGraphOnDisk() throws IOException, SQLException {
        String collectionGraphFilename = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "collectionGraph.txt").toString();
        OutputStream os = new FileOutputStream(collectionGraphFilename);

        for(int collectionId : this.collectionGraph.keySet()) {
            for(CollectionEdge ce : this.collectionGraph.get(collectionId)) {
                os.write(Integer.toString(ce.getId()).getBytes()); // collection edge id
                os.write("\t".getBytes());
                os.write(Integer.toString(collectionId).getBytes()); // source collection id
                os.write("\t".getBytes());
                os.write(Integer.toString(ce.getTarget()).getBytes()); // target collection id
                os.write("\t".getBytes());
                if(this.inCycleCollectionEdges.contains(ce)) {
                    os.write(Boolean.toString(true).getBytes()); // the edge is involved in a cycle
                } else {
                    os.write(Boolean.toString(false).getBytes());
                }
                os.write("\t".getBytes());
                os.write(Boolean.toString(ce.getComesFromXMLRelation()).getBytes()); // the edge comes from an ID/IDREF relation
                os.write("\t".getBytes());
                os.write(Boolean.toString(ce.getIsAtMostOne()).getBytes()); // isAtMostOne
                os.write("\n".getBytes());
            }
        }

        os.flush();
        os.close();

        this.graph.getPreparedStatement("TRUNCATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME).execute();
        this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " (collEdgeId, collIdSource, collIdTarget, isEdgeInCycle, isEdgeComingFromIDREF, isAtMostOne) FROM STDIN ", new FileReader(collectionGraphFilename));

        // finally, we need to set collections that have been removed during the ID/IDREF process as inactive in COLLECTIONS_INFORMATIONS_TABLE_NAME
        if(!this.collectionsNodesToDelete.isEmpty()) {
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " " +
                    "SET isActive = false " +
                    "WHERE collId IN (" + StringUtils.join(this.collectionsNodesToDelete, ",") + ")").execute();
        }

        // finally, we don't need to set (again) the leaf collections because COLLECTIONS_INFORMATIONS_TABLE_NAME has not changed
    }

    public void computeAtMostOneOnDisk() throws SQLException {
        this.graph.createTable(SchemaTableNames.AT_MOST_ONE_TABLE_NAME, "(collEdgeId INTEGER, nbNodesCj INTEGER)", true, true);
        PreparedStatement stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.AT_MOST_ONE_TABLE_NAME + " " +
                "SELECT DISTINCT cg.collEdgeId, COUNT(DISTINCT n2.nodeId) " +
                "FROM " +  SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " cg, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " n1, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " n2, " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e " +
                "WHERE cg.isActive = true AND cg.collIdSource = n1.collId AND cg.collIdTarget = n2.collId AND e.source = n1.nodeId and e.target = n2.nodeId " +
                "GROUP BY n1.nodeId, cg.collEdgeId;");
        stmt.execute();

        log.info("SET at-most-one on Postgres");
        stmt = this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " " +
                "SET isAtMostOne = true " +
                "WHERE collEdgeId IN (SELECT collEdgeId FROM " + SchemaTableNames.AT_MOST_ONE_TABLE_NAME + " GROUP BY collEdgeId HAVING MAX(nbNodesCj) <= 1 AND MIN(nbNodesCj) >= 1);");
        stmt.executeUpdate();
    }

    public void getAndSetAtMostOneInMemory() throws SQLException {
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT collEdgeId " +
                "FROM " +  SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " " +
                "WHERE isActive = true AND isAtMostOne = true;");
        ResultSet rs = stmt.executeQuery();

        while(rs.next()) {
            int collEdgeId = rs.getInt(1);
            CollectionEdge ce = this.getCollectionEdge(collEdgeId);
            if(ce != null) {
                ce.setIsAtMostOne(true);
            }
        }
    }

    // returns the first parent of the given collection. Returns -1 if no parent found.
    private int getFirstParent(int childCollectionId) {
        for(Map.Entry<Integer, ArrayList<CollectionEdge>> entry : this.collectionGraph.entrySet()) {
            for(CollectionEdge ce : entry.getValue()) {
                if(ce.getTarget() == childCollectionId) {
                    return ce.getSource();
                }
            }
        }
        return -1;
    }

    private ArrayList<Integer> collectionsNodesToDelete = new ArrayList<>();
    private ArrayList<CollectionEdge> collectionsEdgesToDelete = new ArrayList<>();
    private HashMap<Integer, ArrayList<CollectionEdge>> collectionsEdgesToAdd = new HashMap<>();

    public void buildXMLRefEdgesInCollectionGraph() throws SQLException {
        // 1.1 get leaf values for each leaf collection that comes from an XML attribute (used to build ID/IDREF)
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT DISTINCT c.collId, n.label " + // we use DISTINCT to minimize the number of values to compare
                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n " +
                "WHERE c.nodeId = n.id AND n.type = " + Node.Types.XML_ATTRIBUTE_VALUE.ordinal());
        ResultSet rs = stmt.executeQuery();
        this.attributeLeafCollectionsValues = Utils.buildHashMapFromResultSetArrayListString(rs, -1);

        // 1.2 get collections having unique values - this will help to check whether a collection may be a set of IDs for ID/IDREF edges
        if(!this.attributeLeafCollectionsValues.isEmpty()) {
            stmt = this.graph.getPreparedStatement("SELECT DISTINCT c.collId " +
                    "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n " +
                    "WHERE c.nodeId = n.id AND c.collId IN (" + StringUtils.join(this.attributeLeafCollectionsValues.keySet(), ",") + ") " +
                    "GROUP BY c.collId, n.label " +
                    "HAVING COUNT(n.label) >= 2;"); // there is a duplicated value
            rs = stmt.executeQuery();
            // by default, all leaf attribute collections are considered as containing unique values
            // we remove from that list all the collections having at least one value non unique.
            this.collectionsWithUniqueValues = new ArrayList<>(this.attributeLeafCollectionsValues.keySet());
            while(rs.next()) {
                this.collectionsWithUniqueValues.remove((Integer) rs.getInt(1));
            }

            this.collectionsNodesToDelete = new ArrayList<>(); // this contains collections of XML attributes representing relationships (e.g. the itemref of "), we want to delete them, but after all edges have been added to avoid looking for collections that are no more present in the collection graph
            this.collectionsEdgesToDelete = new ArrayList<>();
            this.collectionsEdgesToAdd = new HashMap<>(); // this contains collections edges representing ID/IDREF in XML. We need to store them and apply them after found them all so that we don't modify the collection graph while looking for such relations
            rs = this.graph.getPreparedStatement("SELECT MAX(collEdgeId) FROM " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME).executeQuery();
            rs.next();
            int collEdgeMaxId = rs.getInt(1);
            int currentId = collEdgeMaxId;
            for (int collI : this.attributeLeafCollectionsValues.keySet()) {
                for (int collJ : this.attributeLeafCollectionsValues.keySet()) {
                    if (collI != collJ) {
                        currentId = this.compareAttributeLeafCollectionsForXMLReferences(collI, collJ, currentId);
                    }
                }
            }

            // Remove collections of XML attribute values and their parent that have been used for ID/IDREF edges
            // and remove their edges
            for(CollectionEdge ce : this.collectionsEdgesToDelete) {
                this.collectionGraph.get(ce.getSource()).remove(ce);
            }
            for(Integer collection : this.collectionsNodesToDelete) {
                this.collectionGraph.remove(collection);
                this.collectionsIds.remove(collection); // we also need to remove the collection from the set of collection ids to only have "available" collections
            }

            // add the new ID/IDREF edges to the graph
            // the collection graph contains the source of each new edge, so no need to check if we need a new entry or not
            for(Map.Entry<Integer, ArrayList<CollectionEdge>> entry : this.collectionsEdgesToAdd.entrySet()) {
                for(CollectionEdge ce : entry.getValue()) {
                    this.collectionGraph.get(entry.getKey()).add(ce);
                }
            }
        }
    }

    private int compareAttributeLeafCollectionsForXMLReferences(int leafCollection1, int leafCollection2, int currentId) {
        if(this.attributeLeafCollectionsValues.containsKey(leafCollection1) && this.attributeLeafCollectionsValues.containsKey(leafCollection2)) {
            ArrayList<String> leafCollection1Values = this.attributeLeafCollectionsValues.get(leafCollection1);
            ArrayList<String> leafCollection2Values = this.attributeLeafCollectionsValues.get(leafCollection2);

            if((leafCollection2Values.containsAll(leafCollection1Values) && leafCollection2Values.size() > leafCollection1Values.size())
                     || (leafCollection2Values.containsAll(leafCollection1Values) && leafCollection2Values.size() == leafCollection1Values.size() && this.collectionsLabels.get(leafCollection2).get(0).endsWith("@id#val"))) {
                // strict inclusion then c1 -> c2
                // OR, non-strict inclusion -> then use id heuristic if possible

                // if the collection containing IDs does not have unique values, it cannot be an ID/IDREF
                if(this.collectionsWithUniqueValues.contains(leafCollection2)) {
                    int fatherOfC1 = this.getFirstParent(leafCollection1);
                    int greatfatherOfC1 = this.getFirstParent(fatherOfC1);
                    int fatherOfC2 = this.getFirstParent(leafCollection2);
                    int greatfatherOfC2 = this.getFirstParent(fatherOfC2);
                    if(greatfatherOfC1 != greatfatherOfC2) { // to avoid returning self looping and then infinitely loop while reporting the properties of a collection
                        CollectionEdge collectionEdgeXMLRef = new CollectionEdge(currentId+1, greatfatherOfC1, greatfatherOfC2, true); // isAtMostOne will be computed later and updated in the collection agraph in memory
                        double transfer = (double) leafCollection1Values.size() / leafCollection2Values.size();
                        // we update the transfer of the newly created edge and in the edgeTransferFactors variables (it was 0 by default)
//                        this.edgeTransferFactors.get(greatfatherOfC1).put(greatfatherOfC2, transfer); // not now - this will be done later because edgeTransferFactors does not exit yet
                        collectionEdgeXMLRef.setEdgeTransferFactor(transfer);
                        // we can't modify the collection graph itself (else some collections will disappear, so we need to store the changes and apply them later)
                        this.collectionsNodesToDelete.add(fatherOfC1);
                        this.collectionsNodesToDelete.add(leafCollection1);
                        this.collectionsEdgesToDelete.add(this.getCollectionEdge(fatherOfC1, leafCollection1));
                        this.collectionsEdgesToDelete.add(this.getCollectionEdge(greatfatherOfC1, fatherOfC1));
                        ArrayList<CollectionEdge> newCollectionEdgesForC1 = new ArrayList<>();
                        if (this.collectionsEdgesToAdd.containsKey(greatfatherOfC1)) {
                            newCollectionEdgesForC1 = this.collectionsEdgesToAdd.get(greatfatherOfC1);
                        }
                        newCollectionEdgesForC1.add(collectionEdgeXMLRef);
                        this.collectionsEdgesToAdd.put(greatfatherOfC1, newCollectionEdgesForC1);
                        this.idRefEdges.add(collectionEdgeXMLRef);
                        return currentId+1;
                    } else {
                        return currentId;
                    }
                }
            } else {
                // the sets are different
                // OR we cannot know the direction of the relation, so we don't add a relation here.
                return currentId;
            }
        }
        return currentId;
    }

    public void retrieveCollectionsTypes() throws SQLException {
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT snt.collId, snt.collType " +
                "FROM " + SchemaTableNames.COLLECTIONS_TYPES + " snt;");

        ResultSet rs = stmt.executeQuery();
        this.collectionsTypes = Utils.buildHashMapFromResultSetArrayListString(rs, -1);
        log.debug(this.collectionsTypes);
    }

    /**
     * Iteratively build paths of length n+1 from paths of length n and detect which edges are part of some cycle in the given graph.
     * Paths that become cyclic because of XML relationships are not considered as cyclic (so that they will not be used in the main collection reporting.
     * This also fills this.loopsInCollectionGraph which contains loops as they are in the collection graph, i.e. a list of successive paths where the last target is equal to the first source.
     */
    public void enumeratePathsAndDetectEdgesInCycles() {
        log.debug("enumeratePathsAndDetectEdgesInCycles on " + this.collectionsIds.size() + " collections");
        // init the allPaths variable with paths of size 1, i.e. collection edges of the collection graph
        for(ArrayList<CollectionEdge> collectionEdges : this.collectionGraph.values()) {
            for(CollectionEdge ce : collectionEdges) {
                int collI = ce.getSource();
                int collJ = ce.getTarget();
                Path pathForCollectionEdge = new Path();
                pathForCollectionEdge.addEdge(ce);
                ArrayList<Path> pathForIJ = new ArrayList<>();
                pathForIJ.add(pathForCollectionEdge);
                this.allPathsBetweenCollections.get(collI).put(collJ, pathForIJ);
                if (collI == collJ) { // the collection is looping on itself, we need to identify this as a cycle. The method enumerate... identifies loops of length >= 2
                    this.inCycleCollectionEdges.add(ce);
                    if(!this.collectionsInvolvedInACycle.contains(collI)) {
                        this.collectionsInvolvedInACycle.add(collI);
                    }
                    if(!this.collectionsInvolvedInACycle.contains(collJ)) {
                        this.collectionsInvolvedInACycle.add(collJ);
                    }
                }
            }
        }

        // iteratively build paths of length n+1 from paths of length n
        boolean stop = false;
        int currentPathLength = 1;
        while (!stop) {
            currentPathLength++; // in each round of the while, we develop paths 1 edge longer
            stop = true; // we start by supposing the search is over, then we try to find new, acyclic paths, to convince us we should keep working
            for (int i : this.collectionsIds) { // i and j are collection ids
                for (int j : this.collectionsIds) {
                    if (i != j && this.allPathsBetweenCollections.get(i).get(j) != null && !this.allPathsBetweenCollections.get(i).get(j).isEmpty()) {
                        ArrayList<Path> allPathsFromItoJ = new ArrayList<>(this.allPathsBetweenCollections.get(i).get(j)); // deep copy to avoid concurrent modification error
                        for (Path path : allPathsFromItoJ) {
                            if (path.getSize() == currentPathLength - 1) {
                                ArrayList<CollectionEdge> edgesFromJtoL = this.collectionGraph.get(j);
                                if (edgesFromJtoL != null && !edgesFromJtoL.isEmpty()) {
                                    // we check the case where there are no outgoing edges for Cj (e.g. Cj is a leaf collection)
                                    for (CollectionEdge e : edgesFromJtoL) {
                                        int l = e.getTarget();
                                        Path p2 = new Path(path);
                                        p2.addEdge(e);

                                        if (i == l) {
                                            // there is a cycle because the target collection of the current edge
                                            // is the same as the source collection of the path
                                            // therefore, we need to add all the edges in that cycle in the set of cyclic edges
                                            for(CollectionEdge edgeInCycle : p2.getPath()) {
                                                if (!this.inCycleCollectionEdges.contains(edgeInCycle)) {
                                                    this.inCycleCollectionEdges.add(edgeInCycle);
                                                    if(!this.collectionsInvolvedInACycle.contains(edgeInCycle.getSource())) {
                                                        this.collectionsInvolvedInACycle.add(edgeInCycle.getSource());
                                                    }
                                                    if(!this.collectionsInvolvedInACycle.contains(edgeInCycle.getTarget())) {
                                                        this.collectionsInvolvedInACycle.add(edgeInCycle.getTarget());
                                                    }
                                                }
                                            }
                                        } else {  // the path p2 is acyclic and there are no paths smaller than p2 that are cyclic
                                            if (!p2.getAllCollectionsExceptLast().contains(e.getTarget()) && this.allPathsBetweenCollections.get(i).get(l) != null && !this.allPathsBetweenCollections.get(i).get(l).contains(p2)) {
//                                                this.allPathsBetweenCollections.get(i).get(l).add(p2);
                                                stop = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
//        this.printAllPathsBetweenCollections();
        log.info(this.inCycleCollectionEdges);
    }

    public boolean getIsRdf() {
        return this.isRdf;
    }

    /**
     * Get (from Postgres) the path for each collection.
     * - if the data has been summarized with the summary by label (e.g. for XML and JSON), this is easy, the table "quotientSummaryPath" already contains the information
     * - else (e.g. for RDF) the path will be the list of the labels of nodes that are in the collection. If the list is too long we can display just the n first elements.
     *
     * @throws SQLException if a SQL query fails
     */
    public void computeCollectionsLabels() throws SQLException {
        // get the list of type for collections having typed nodes
        this.retrieveCollectionsTypes();

        // we get the collections labels as a HashMap
        this.collectionsLabels = new HashMap<>();
        if (this.isRdf) {
            // FOR RDF
            // if there are some types available, we say that the collection label is the type(s)...
            for(int collectionId : this.collectionsIds) {
                if(this.collectionsTypes.containsKey(collectionId)) {
                    ArrayList<String> labels = new ArrayList<>();
                    for(String collectionType : this.collectionsTypes.get(collectionId)) {
                        int indexOfLastSlash = collectionType.lastIndexOf("/");
                        if(indexOfLastSlash == collectionType.length()) {
                            // http://w3c.org/schema/Offer/ gives Offer
                            int indexOfBeforeLastSlash = collectionType.substring(0, indexOfLastSlash).lastIndexOf("/");
                            labels.add(collectionType.substring(indexOfBeforeLastSlash+1, collectionType.length()-1));
                        } else {
                            // http://w3c.org/schema/Offer gives Offer
                            labels.add(collectionType.substring(indexOfLastSlash+1));
                        }
                    }
                    this.collectionsLabels.put(collectionId, labels);
                }
            }

            // ... else, we try to get a meaningful label by getting the labels of the instances in the collection
            // we add paths as a list of the nodes that are in the collection if the nodes inside are not typed
            // and we also add the labels '#val' for leaf collections
            String sqlGetOtherPaths = "" +
                        "SELECT DISTINCT c.collId, CASE WHEN POSITION('#' in nn.label)>0 THEN RIGHT(nn.label, POSITION('#' in REVERSE(nn.label)) - 1) ELSE CASE WHEN POSITION('/' in nn.label)=0 THEN nn.label ELSE CASE WHEN RIGHT(nn.label, POSITION('/' in REVERSE(nn.label)) - 1) = '' IS TRUE THEN nn.label else RIGHT(nn.label, POSITION('/' in REVERSE(nn.label)) - 1) END END END AS label " +
                        "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " nn " +
                        "WHERE c.nodeId = nn.id " + (!this.collectionsLabels.isEmpty() ? "AND c.collId NOT IN (" + StringUtils.join(this.collectionsLabels.keySet(), ",") + ") " : "") + " AND nn.type <> " + Node.Types.RDF_LITERAL.ordinal() + " " +
                        "UNION ALL " +
                        "SELECT DISTINCT c.collId, '#val' " +
                        "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " nn " +
                        "WHERE c.nodeId = nn.id AND nn.type = " + Node.Types.RDF_LITERAL.ordinal();
            PreparedStatement stmt = this.graph.getPreparedStatement(sqlGetOtherPaths);
            ResultSet rs = stmt.executeQuery();

            this.collectionsLabels.putAll(Utils.buildHashMapFromResultSetArrayListString(rs, 3)); // we need to use putAll to merge this hashmap with the previous one built with collections types
        } else {
            PreparedStatement stmt = this.graph.getPreparedStatement("SELECT c.collId, c.collLabel FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c;");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ArrayList<String> path = new ArrayList<>();
                if (rs.getString(2).isEmpty()) {
                    path.add(" "); // for empty paths
                } else {
                    path.add(rs.getString(2));
                }
                this.collectionsLabels.put(rs.getInt(1), path);
            }
        }
        log.debug(this.collectionsLabels);
    }

    /**
     * Compute transfers between collections in Postgres. They are stored in the table "transfers".
     *
     * @throws SQLException if a query fails
     */
    public void computeEdgeTransferFactorsOnDisk() throws SQLException {
        log.info("*** compute edge transfer factors on disk ***");
        // 1. computing transfer between collections in equivalence classes
        // this can be done in SQL only
        // we will store the transfer between collections in a Postgres table of the form: (colli id, collj id, transfer[i,j])
        // transfer[i,j] = |nodes in coll2j having a parent in coll1i| / |nodes in coll2j|
        long startTime = System.currentTimeMillis();
        this.graph.createTable(SchemaTableNames.ALL_INFORMATION_FOR_TRANSFERS_TABLE_NAME, "(collI INTEGER, collJ INTEGER, nbNodesInCollJHavingAParentInCollI INTEGER)", true, true);

        // norm_edges is a data edge, that connects a node with its parent
        // c1 and C2 connect two data nodes, each within a collection
        this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.ALL_INFORMATION_FOR_TRANSFERS_TABLE_NAME + " " + // for each combination of collection, we count the number of nodes in collJ having a parent in collI. this is the upper part of the division to compute the transfer
                "SELECT c1.collId, c2.collId, COUNT(DISTINCT c2.nodeId) " +
                "FROM " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " cg, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c1, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c2, " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e " +
                "WHERE cg.isActive = true AND cg.collIdSource = c1.collId AND cg.collIdTarget = c2.collId AND e.source = c1.nodeId AND e.target = c2.nodeId AND c2.isActive = TRUE " + // don't forget to check that the nodes are still active
                "GROUP BY c1.collId, c2.collId;").execute();
        log.debug("insert into all_information_for_transfers in " + (System.currentTimeMillis() - startTime) + " ms");
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " cg " +
                "SET edgeTransferFactor = sq1.edgeTransferFactor FROM (SELECT ait.collI, ait.collJ, ait.nbNodesInCollJHavingAParentInCollI::numeric/c.collSize AS edgeTransferFactor " +
                "FROM " + SchemaTableNames.ALL_INFORMATION_FOR_TRANSFERS_TABLE_NAME + " ait, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c " +
                "WHERE ait.collJ = c.collId) AS sq1 " +
                "WHERE collIdSource = sq1.collI AND collIdTarget = sq1.collJ;").execute();
        log.debug("insert into transfers in " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public void resetIsActive() throws SQLException {
        log.info("reset isActive");
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " SET isActive = true;").executeUpdate();
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " SET isActive = true;").executeUpdate();
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " SET isActive = true;").executeUpdate();
    }

    /**
     * Get the transfers between collections in memory. We retrieve the table "transfers" from Postgres.
     *
     * @throws SQLException if a query fails
     */
    public void retrieveEdgeTransferFactorsInMemory() throws SQLException {
        log.info("*** retrieving transfers in memory *** ");
        // We retrieve transfers in memory. They will be used to compute the collection graph
        PreparedStatement stmt = this.graph.getPreparedStatement("" +
                "SELECT collIdSource, collIdTarget, edgeTransferFactor " +
                "FROM " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " " +
                "WHERE isActive = true;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int collI = rs.getInt(1);
            int collJ = rs.getInt(2);
//            this.edgeTransferFactors.get(collI).put(collJ, rs.getDouble(3));
            this.getCollectionEdge(collI, collJ).setEdgeTransferFactor(rs.getDouble(3));
        }

        // at this point, XML ID/IDREF edges are already computed and in memory.
        // their edge transfer factor has been set (i.e. ce.getEdgeTransferFactor() works)
        // but the variable this.edgeTransferFactors was not created, thus not updated with such etf.
        // we can do it now because this.edgeTransferFactors now exists and can be updated.
//        for(CollectionEdge ce : this.idRefEdges) {
//            if(this.edgeTransferFactors.containsKey(ce.getSource())) { // if persons point to items, and persons are reported, we can't add the etf between person and item
//                this.edgeTransferFactors.get(ce.getSource()).put(ce.getTarget(), ce.getEdgeTransferFactor());
//            }
//        }
    }

    public void printEdgeTransferFactors() {
        for(ArrayList<CollectionEdge> ces : this.collectionGraph.values()) {
            for(CollectionEdge ce : ces) {
                log.debug("etf[" + ce.getSource() + "][" + ce.getTarget() + "] = " + ce.getEdgeTransferFactor() + " -> " + ce);
            }
        }
    }

    /**
     * Compute the total transfer for each path in between a leaf collection b and another collection a.
     * The total transfer is the sum of the transfers of all paths linking a to b and for each path a -...-> b we do the product of each edge starting for b until a
     */
    public void computeAggregatedPathTransferFactors(boolean workingGraph) {
        // 1. init all the path transfer factor to 1.0 (required for the graph update)
        // the default value for ptf is 1.0 because we multiply it by each etf (thus set it to 0 would lead to a zero ptf).
        for (int c1 : this.aggregatedPathTransferFactors.keySet()) {
            for (int c2 : this.aggregatedPathTransferFactors.get(c1).keySet()) {
                this.aggregatedPathTransferFactors.get(c1).put(c2, 1.0);
            }
        }

        // 2. compute the path transfer factor for each acyclic path
        for (int c1 : this.collectionsIds) {
            for (int c2 : this.collectionsIds) {
                ArrayList<Path> allPathsFromC1toC2 = this.getAllPathsBetweenCollections(c1, c2);
                if (c1 != c2 && allPathsFromC1toC2 != null && !allPathsFromC1toC2.isEmpty()) {
                    ArrayList<Path> noneCyclicPathsFromC1toC2 = new ArrayList<>();
                    for (Path pathFromC2ToC1 : allPathsFromC1toC2) {
                        pathFromC2ToC1.setPathTransferFactor(1.0); // initialise the ptf to 1.0
                        // we check that the path is not cyclic
                        if(pathFromC2ToC1.doesNotContainCyclicEdges(workingGraph)) {
                            // compute the ptf of the current path between C1 and C2... (has been initialised to 1.0)
                            for (CollectionEdge e2 : pathFromC2ToC1.getPath()) {
                                // the path transfer factor is set to 1 by default, then we update it by multiplying each etf
                                // we need to take the collection edge from the CG (and not from the path)
                                // because edges in the paths are the firstly-created edges.
                                // But with the graph update, we want to compute the ptf based on the latest etf
                                CollectionEdge updatedE2 = this.getCollectionEdge(e2.getSource(), e2.getTarget());
                                // we check that the collection edge still exists in the new collection graph because some collections may not exist anymore on some paths
                                if(updatedE2 != null) {
//                                    log.info("new ptf[" + e2.getSource() + "][" + e2.getTarget() + "] = " + pathFromC2ToC1.getPathTransferFactor() + " * " + updatedE2.getEdgeTransferFactor() + " = " + (pathFromC2ToC1.getPathTransferFactor() * updatedE2.getEdgeTransferFactor()));
                                    pathFromC2ToC1.setPathTransferFactor(pathFromC2ToC1.getPathTransferFactor() * updatedE2.getEdgeTransferFactor());
                                }
                            }
                            noneCyclicPathsFromC1toC2.add(pathFromC2ToC1);
                        }
                    }
                    // ... and then aggregate it to obtain the aggregated path transfer factor between C1 and C2
                    PathAggregatorSum pathAggregatorSum = new PathAggregatorSum();
                    double aggregatedPathTransferFactorFromC2ToC1 = pathAggregatorSum.aggregate(noneCyclicPathsFromC1toC2, workingGraph);
                    this.setAggregatedPathTransferFactor(c2, c1, aggregatedPathTransferFactorFromC2ToC1);
                }
            }
        }
    }

    public void printAggregatedPathTransferFactors() {
        for (int i : this.aggregatedPathTransferFactors.keySet()) {
            for (int j : this.aggregatedPathTransferFactors.keySet()) {
                double pathTransferFactor = this.aggregatedPathTransferFactors.get(i).get(j);
                if (pathTransferFactor > 0) {
                    log.debug("pathTransferFactor[" + i + "][" + j + "]=" + pathTransferFactor);
                }
            }
        }
    }

    public void printAllPathsBetweenCollections() {
        for (int i : this.allPathsBetweenCollections.keySet()) {
            for (int j : this.allPathsBetweenCollections.keySet()) {
                ArrayList<Path> allPathsBetweenCiAndCj = this.allPathsBetweenCollections.get(i).get(j);
                if(!allPathsBetweenCiAndCj.isEmpty()) {
                    log.debug("allPathsBetweenCollections[" + i + "][" + j + "]=" + allPathsBetweenCiAndCj);
                }
            }
        }
    }

    public void printCollectionsFrequencies() {
        for (int i : this.collectionsFrequencies.keySet()) {
            log.debug("frequency(" + i + ")=" + this.collectionsFrequencies.get(i));
        }
    }

    public HashMap<Integer, CollectionBoundary> getCollectionsBoundaries() {
        return this.collectionsBoundaries;
    }

    public CollectionBoundary getCollectionBoundary(int collection) {
        return this.collectionsBoundaries.get(collection);
    }

    public String getCollectionLabel(int collId) {
        if (this.collectionsLabels != null && this.collectionsLabels.containsKey(collId)) { // TODO NELLY: fix this != null
            String collectionLabel;
            if(this.collectionsLabels.get(collId).size() == 1) {
                collectionLabel = this.collectionsLabels.get(collId).get(0);
            } else {
                collectionLabel = StringUtils.join(this.collectionsLabels.get(collId));
            }
//            if (collectionLabel.endsWith(".") && collectionLabel.length() > 1) { // if the label is just a dot we keep it to avoid having empty labels
//                collectionLabel = collectionLabel.replace(".", "");
//            }
            return collectionLabel;
        } else {
            return "";
        }
    }

    // returns the FIRST collection edge matching the source and target.
    public CollectionEdge getCollectionEdge(int source, int target) {
        if (this.collectionGraph.containsKey(source)) {
            for (CollectionEdge ce : this.collectionGraph.get(source)) {
                if (ce.getTarget() == target) {
                    return ce;
                }
            }
        }
        return null;
    }

    // return the collection edge having the given id
    public CollectionEdge getCollectionEdge(int id) {
        for(ArrayList<CollectionEdge> ces : this.collectionGraph.values()) {
            for(CollectionEdge ce : ces) {
                if(ce.getId() == id) {
                    return ce;
                }
            }
        }
        return null;
    }

    // compute the frequency of each property for each collection in the arraylist given as an argument
    public void computeCollectionsFrequencies() {
        log.debug(this.collectionsSizes);
        for(ArrayList<CollectionEdge> ces : this.collectionGraph.values()) {
            for(CollectionEdge ce : ces) {
                double frequency = this.collectionsSizes.get(ce.getTarget()) * ce.getEdgeTransferFactor() / (double) this.collectionsSizes.get(ce.getSource());
                if(!this.collectionsFrequencies.containsKey(ce.getSource())) {
                    this.collectionsFrequencies.put(ce.getSource(), new HashMap<>());
                    this.collectionsFrequencies.get(ce.getSource()).put(ce.getTarget(), frequency);
                } else {
                    this.collectionsFrequencies.get(ce.getSource()).put(ce.getTarget(), frequency);
                }
            }
        }

//        this.printCollectionsFrequencies();
    }

    public ArrayList<CollectionEdge> getOutgoingEdges(int collectionId) {
        ArrayList<CollectionEdge> outgoingEdges = new ArrayList<>();
        if(this.collectionGraph.containsKey(collectionId)) {
            outgoingEdges.addAll(this.collectionGraph.get(collectionId));
        }
        return outgoingEdges;
    }

    public ArrayList<CollectionEdge> getIncomingEdges(int collectionId) {
        // return the set of edges incoming the given collection
        ArrayList<CollectionEdge> edgesIncomingCollection = new ArrayList<>();
        for(int collection : this.collectionGraph.keySet()) {
            for(CollectionEdge ce : this.collectionGraph.get(collection)) {
                if(ce.getTarget() == collectionId) {
                    edgesIncomingCollection.add(ce);
                }
            }
        }
        return edgesIncomingCollection;
    }

    /**
     * Get the list of all the descendants of a given collection
     * @param collectionId the collection for which we search descendants.
     * @param depth the maximal depth
     * @return the list of all the descendants of the given collection
     */
    public ArrayList<Integer> getAllDescendants(int collectionId, int depth, boolean takeIntoAccountXMLRelations) {
        ArrayList<Integer> descendants = new ArrayList<>();
        for (int i : this.allPathsBetweenCollections.keySet()) {
            if (i != collectionId && this.allPathsBetweenCollections.get(collectionId).get(i) != null && !this.allPathsBetweenCollections.get(collectionId).get(i).isEmpty()) {
                for(Path p : this.allPathsBetweenCollections.get(collectionId).get(i)) {
                    if(p.getPath().size() <= depth || depth == -1) {
                        descendants.add(i);
                    }
                }
            }
        }
        log.debug("get all descendants of C"+collectionId + " at depth " + depth + ": " + descendants);
        return descendants;
    }

    /**
     * Get the list of all the descendants of a given collection
     * @param collection the collection for which we search descendants
     * @return the list of all the descendants of the given collection
     */
    public ArrayList<Integer> getAllAncestorsOfCollection(int collection) {
        ArrayList<Integer> ancestors = new ArrayList<>();
        for (int i : this.allPathsBetweenCollections.keySet()) {
            if (i != collection && this.allPathsBetweenCollections.get(i).get(collection) != null && !this.allPathsBetweenCollections.get(i).get(collection).isEmpty()) {
                ancestors.add(i);
            }
        }

        return ancestors;
    }


    public RelationalGraph getGraph() {
        return this.graph;
    }

    public HashMap<Integer, ArrayList<String>> getCollectionsLabels() {
        return this.collectionsLabels;
    }

    public HashMap<Integer, ArrayList<CollectionEdge>> getCollectionGraph() {
        return this.collectionGraph;
    }

    public HashMap<Integer, Integer> getCollectionsSizes() {
        return this.collectionsSizes;
    }

//    public HashMap<Integer, HashMap<Integer, Double>> getEdgeTransferFactors() {
//        return this.edgeTransferFactors;
//    }

    public HashMap<Integer, HashMap<Integer, ArrayList<Path>>> getAllPathsBetweenCollections() {
        return this.allPathsBetweenCollections;
    }

    public ArrayList<Path> getAllPathsBetweenCollections(int i, int j) {
        return this.allPathsBetweenCollections.get(i).get(j);
    }

    public ArrayList<Path> getAllCyclicPathsFromCollection(int i) {
        ArrayList<Path> cyclicPathsStartingAndEndingInI = new ArrayList<>();
        // for each collection reachable from i...
        for(Map.Entry<Integer, ArrayList<Path>> entry : this.allPathsBetweenCollections.get(i).entrySet()) {
            for(Path p : entry.getValue()) {
                // ... try to extend it to it by checking if the last collection of the path can come back to i
                Integer lastCollectionInPath = p.getLastCollectionOfPath();
                for(CollectionEdge e : CollectionGraph.getInstance().getOutgoingEdges(lastCollectionInPath)) {
                    if(e.getTarget() == i) {
                        Path cyclicPathOnI = new Path(p);
                        cyclicPathOnI.addEdge(e);
                        cyclicPathsStartingAndEndingInI.add(cyclicPathOnI);
                    }
                }
            }
        }
        log.info(cyclicPathsStartingAndEndingInI);
        return cyclicPathsStartingAndEndingInI;
    }

    public ArrayList<Integer> getCollectionsIds() {
        return this.collectionsIds;
    }

    public ArrayList<CollectionEdge> getInCycleCollectionEdges() {
        return this.inCycleCollectionEdges;
    }

    public boolean checkIfCollectionIsInCycle(int collId) {
        return this.collectionsInvolvedInACycle.contains(collId);
    }

    public ArrayList<Integer> getCollectionsInvolvedInACycle() {
        return this.collectionsInvolvedInACycle;
    }

    public ArrayList<Integer> getLeafCollections() {
        return this.leafCollections;
    }

    public boolean isLeaf(int collection) {
        return this.leafCollections.contains(collection);
    }

    public void setCollectionBoundary(int collectionId, CollectionBoundary collectionBoundary) {
        //we set the boundary of the collection
        this.collectionsBoundaries.put(collectionId, collectionBoundary);
        // and add its collections to the list of collections involved in a boundary
        for(int c : collectionBoundary.getCollectionsInBoundary()) {
            if(!this.collectionsInvolvedInABoundary.contains(c)) {
                this.collectionsInvolvedInABoundary.add(c);
            }
        }
    }

    public ArrayList<Integer> getCollectionsInvolvedInABoundary() {
        return this.collectionsInvolvedInABoundary;
    }

    public void setCollectionsInvolvedInABoundary(ArrayList<Integer> collectionsInvolvedInABoundary) {
        this.collectionsInvolvedInABoundary = collectionsInvolvedInABoundary;
    }

    public void addCollectionToSetOfCollectionsInvolvedInABoundary(int collectionId) {
        if(!this.collectionsInvolvedInABoundary.contains(collectionId)) {
            this.collectionsInvolvedInABoundary.add(collectionId);
        }
    }

    public boolean checkIfCollectionIsInvolvedInBoundary(int collId) {
        for(CollectionBoundary collectionBoundary : this.collectionsBoundaries.values()) {
            for(Integer collectionInvolvedInBoundary : collectionBoundary.getCollectionsInBoundary()) {
                if(collId == collectionInvolvedInBoundary) {
                    return true;
                }
            }
        }
        return false;
    }

    public HashMap<String, ArrayList<String>> getRelationshipsBetweenMainEntityCollections() {
        return this.relationshipsBetweenMainEntityCollections;
    }

    public ArrayList<String> getRelationshipsBetweenMainEntityCollections(String key) {
        return this.relationshipsBetweenMainEntityCollections.getOrDefault(key, new ArrayList<>());
    }

    public void addRelationshipsBetweenMainEntityCollections(int mainEntity1, int mainEntity2, String relationshipLabel) {
        String strKey = ""+mainEntity1+"-"+mainEntity2;
        ArrayList<String> relationships;
        if(this.relationshipsBetweenMainEntityCollections.containsKey(strKey)) {
            relationships = this.relationshipsBetweenMainEntityCollections.get(strKey);
        } else {
            relationships = new ArrayList<>();
        }
        relationships.add(relationshipLabel);
        this.relationshipsBetweenMainEntityCollections.put(strKey, relationships);
    }

    public HashMap<Integer, ReportStatus> getCollectionsStatuses() {
        return this.collectionsStatuses;
    }

    public ReportStatus getCollectionStatus(int collectionId) {
        return this.collectionsStatuses.get(collectionId);
    }

    public boolean checkIfCollectionStatusIsIn(int collectionId, ReportStatus... statuses) {
        for(ReportStatus status : statuses) {
            if(this.collectionsStatuses.get(collectionId) == status) {
                return true;
            }
        }
        return false;
    }

    public int getNumberOfNoStatusCollections() {
        return (int) this.collectionsStatuses.entrySet().stream().filter(entry -> entry.getValue() == NO_STATUS).count();
    }

    public void setStatusOfCollection(int collectionId, ReportStatus status) {
        this.collectionsStatuses.put(collectionId, status);
        if(status == ReportStatus.REPORTED && !this.mainCollectionsList.contains(collectionId)) {
            this.mainCollectionsList.add(collectionId);
        }
    }
    public ArrayList<Integer> getMainCollections() {
        return this.mainCollectionsList;
    }

    public HashMap<Integer, HashMap<Integer, Double>> getCollectionsFrequencies() {
        return this.collectionsFrequencies;
    }

    public double getCollectionFrequency(int collI, int collJ) {
        return this.collectionsFrequencies.get(collI).get(collJ);
    }

    public double getCollectionFrequency(int collI, String collJLabel) {
        return 1.0d; // return this.collectionsFrequencies.get(collI).get(collJ); // TODO NELLY: fix this
    }

    public HashMap<Integer, Integer> getCollectionsCategories() {
        return this.collectionsCategories;
    }

    public Integer getCollectionCategory(int collId) {
        return this.collectionsCategories.getOrDefault(collId, Classification.getCategoryId("CATEGORY_OTHER"));
    }

    public void setCollectionCategory(int collId, int category) {
        this.collectionsCategories.put(collId, category);
    }

    public boolean checkIfCollectionHasCategory(int collId) {
        return this.collectionsCategories.containsKey(collId);
    }

    public HashMap<Integer, ArrayList<String>> getAttributeLeafCollectionsValues() {
        return this.attributeLeafCollectionsValues;
    }

    public HashMap<Integer, Double> getCollectionsScores() {
        return this.collectionsScores;
    }

    public double getCollectionScore(int collectionId) {
        return this.collectionsScores.getOrDefault(collectionId, Double.NEGATIVE_INFINITY);
    }

    public int getNumberOfIncomingEdges(int collection) {
        return this.getIncomingEdges(collection).size();
    }

    public int getNumberOfOutgoingEdges(int collection) {
        return this.collectionGraph.get(collection).size();
    }

    public void setCollectionScore(int collectionId, double newCollectionWeight) {
        this.collectionsScores.put(collectionId, newCollectionWeight);
    }

//    public HashMap<Integer, HashMap<Integer, Double>> getPathTransferFactors() {
//        return this.pathTransferFactors;
//    }

    public double getAggregatedPathTransferFactor(int collI, int collJ) {
        return this.aggregatedPathTransferFactors.get(collI).get(collJ);
    }

    public void setAggregatedPathTransferFactor(int collI, int collJ, double newPathTransferFactor) {
        this.aggregatedPathTransferFactors.get(collI).put(collJ, newPathTransferFactor);
    }

    public ArrayList<String> getCollectionType(int collId) {
        return this.collectionsTypes.getOrDefault(collId, null);
    }

    public HashMap<Integer, ArrayList<String>> getCollectionsTypes() {
        return this.collectionsTypes;
    }

    public void setGraph(RelationalGraph graph) {
        this.graph = graph;
    }

    public void setIsRdf(boolean isRdf) {
        this.isRdf = isRdf;
    }

    public HashMap<String, HashMap<ExtractedEntityType, java.lang.Integer[]>> getEntityProfiles() {
        return this.collectionsEntityProfiles;
    }

    public HashMap<ExtractedEntityType, java.lang.Integer[]> getEntityProfilesOfCollection(String key) {
        return this.collectionsEntityProfiles.getOrDefault(key, null);
    }

    public HashMap<ExtractedEntityType, java.lang.Integer[]> getEntityProfilesOfCollection(String key, HashMap<String, String> entities2classes) {
        return this.collectionsEntityProfiles.getOrDefault(key, null);
    }

//    public double getEdgeTransferFactor(int collI, int collJ) {
//        return this.edgeTransferFactors.get(collI).get(collJ);
//    }

//    public void setEdgeTransfersFactorsInCollectionGraph() {
//        for(ArrayList<CollectionEdge> collectionEdges : this.collectionGraph.values()) {
//            for(CollectionEdge ce : collectionEdges) {
//                ce.setEdgeTransferFactor(this.edgeTransferFactors.get(ce.getSource()).get(ce.getTarget()));
//            }
//        }
//    }

    public int getCollectionSize(int collectionId) {
        return this.collectionsSizes.getOrDefault(collectionId, -1); // TODO NELLY: fix this
    }

    public boolean isIdRefEdge(CollectionEdge ce) {
        return this.idRefEdges.contains(ce);
    }

    // compute the size of each collection
    public void computeCollectionsSizes() throws SQLException {
        // we need to do this in two steps because:
        // extra-boundary collections have their nodes inactivated individually
        // but intra-boundary collections do not have their nodes inactivated individually
        this.graph.getPreparedStatement("TRUNCATE TABLE " + SchemaTableNames.COLLECTIONS_SIZES_TABLE_NAME).execute(); // empty the table from previous counts
        this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.COLLECTIONS_SIZES_TABLE_NAME + " " +
//                "SELECT i.collId, 0 " + // we don't put a size for inactive collections (else we would have divided bby 0 when computing the etf)
//                "FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " i " +
//                "WHERE i.isActive = false " + // encompasses extra and intra-boundary collections
//                "UNION ALL " +
                "SELECT c.collId, COUNT(c.nodeId) " +
                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " i " +
                "WHERE c.collId = i.collId AND i.isActive = true AND c.isActive = true " +
                "GROUP BY c.collId;").execute(); // for collections still having active nodes

        // update the collections sizes
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " SET collSize = cs.collSize " +
                "FROM " + SchemaTableNames.COLLECTIONS_SIZES_TABLE_NAME + " cs " +
                "WHERE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + ".collId = cs.collId;").executeUpdate();
    }

    public void computeEntityProfilesOnDisk() throws SQLException {
        // 1. compute signatures in Postgres
        PreparedStatement stmt;
        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.SIGNATURES_TABLE_NAME + " " +
                "SELECT c.collid, n2.label, n5.type, SUM(LENGTH(n3.label)), SUM(LENGTH(n5.label)), COUNT(n5.label) " +
                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c " +
                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n " + // record ("employee")
                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e " +
                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n2 " + // property label ("name")
                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e2 " +
                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n3 " + // value node ("Bob")
                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e3 " +
                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n4 " + // extract:p/o/l node
                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e4 " +
                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n5 " + // extracted entity ("Bob")
                "WHERE c.nodeid = n.id AND n.id = e.source AND n2.id = e.target AND n2.id = e2.source AND n3.id = e2.target AND n3.id = e3.source AND n4.id = e3.target AND e4.source = n4.id AND e4.target = n5.id " +
                "AND n4.type = " + Node.Types.NORMALIZATION_NODE_EXTRACTION.ordinal() + " " +
                "GROUP BY c.collid, n2.label, n5.type;");
        stmt.execute();
    }

    public void retrieveEntityProfilesInMemory() throws SQLException {
        Classification.setExtractedEntityTypes(new ExtractedEntityTypes()); // moved from classification to here, to know to which entity type the numbers stored in Postgres refer to

        // retrieve signatures from Postgres
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT collId, childLabel, type, totalStrLen, totalOccurTLength, totalNbOccurT FROM " + SchemaTableNames.SIGNATURES_TABLE_NAME + ";");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String key = rs.getInt(1) + rs.getString(2);
            if (this.collectionsEntityProfiles.get(key) == null) {
                this.collectionsEntityProfiles.put(key, new HashMap<>());
            }
            HashMap<ExtractedEntityType, Integer[]> sig = this.collectionsEntityProfiles.get(key);
            Integer[] stats = new Integer[3];
            stats[0] = rs.getInt(4); // total size of value nodes for property in collection
            stats[1] = rs.getInt(5); // total size of extracted entities for property in collection
            stats[2] = rs.getInt(6); // number of extracted entities for property in collection
            sig.put(Classification.getExtractedEntityTypes().getExtractedEntityTypesFromId(rs.getInt(3)), stats);
//            log.debug("extracted " + rs.getInt(3) + " -> " + this.extractedEntityTypes.getExtractedEntityTypesFromId(rs.getInt(3)));
            this.collectionsEntityProfiles.put(key, sig);
        }

        log.debug(this.collectionsEntityProfiles);
    }

    public void setCollectionGraph(HashMap<Integer, ArrayList<CollectionEdge>> collectionGraph) {
        this.collectionGraph = new HashMap<>(collectionGraph);
    }

    public int getNbNormalizedNodes() {
        return this.nbNormalizedNodes;
    }

    public int getTotalNbCollections() {
        return this.totalNbCollections;
    }

    @Override
    public String toString() {
        return "CollectionGraph{" +
                ", isRdf=" + this.isRdf +
                ", collectionGraph=" + this.collectionGraph +
                ", allPathsBetweenCollections=" + this.allPathsBetweenCollections +
                ", collectionsIds=" + this.collectionsIds +
                ", leafCollections=" + this.leafCollections +
                ", collectionsSizes=" + this.collectionsSizes +
                ", collectionsTypes=" + this.collectionsTypes +
                ", collectionsScores=" + this.collectionsScores +
//                ", edgeTransferFactors=" + this.edgeTransferFactors +
//                ", pathTransferFactors=" + this.pathTransferFactors +
                ", collectionsInvolvedInACycle=" + this.collectionsInvolvedInACycle +
                ", inCycleCollectionEdges=" + this.inCycleCollectionEdges +
                ", collectionsStatuses=" + this.collectionsStatuses +
                ", mainCollectionsList=" + this.mainCollectionsList +
                ", relationshipsBetweenMainEntityCollections=" + this.relationshipsBetweenMainEntityCollections +
                ", collectionsBoundaries=" + this.collectionsBoundaries +
                ", collectionsInvolvedInABoundary=" + this.collectionsInvolvedInABoundary +
                ", collectionsLabels=" + this.collectionsLabels +
                ", collectionPropertiesFrequencies=" + this.collectionsFrequencies +
                ", attributeLeafCollectionsValues=" + this.attributeLeafCollectionsValues +
                ", collectionsWithUniqueValues=" + this.collectionsWithUniqueValues +
                ", collectionsCategories=" + this.collectionsCategories +
                ", collectionsEntityProfiles=" + this.collectionsEntityProfiles +
                ", collectionsNodesToDelete=" + this.collectionsNodesToDelete +
                ", collectionsEdgesToDelete=" + this.collectionsEdgesToDelete +
                ", collectionsEdgesToAdd=" + this.collectionsEdgesToAdd +
                '}';
    }
}

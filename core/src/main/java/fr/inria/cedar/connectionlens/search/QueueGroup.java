package fr.inria.cedar.connectionlens.search;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;

/**
 * 
 * Group of PriorityQueues for {@link GAMSearch}.
 * 
 * @author Lucas Maia Morais, Madhulika Mohanty.
 *
 */
public class QueueGroup {
	
	GAMSearch gs;
	
	/** The queue type. */
	public static final QueueType queueType = QueueGroup.QueueType
			.valueOf(Config.getInstance().getProperty("pqueue_type"));
	private Map<Integer, GAMPriorityQueue> QG;
	
	private GAMPriorityQueue gampq;
	
	
	private Comparator<GAMPriorityQueue> comparatorQG
		= Comparator.comparingInt(PQ -> PQ.size());
	
//	private Comparator<PriorityQueue<Pair<GAMAnswerTree, Edge>>> comparatorQG
//		= new Comparator<PriorityQueue<Pair<GAMAnswerTree, Edge>>>() {
//		public int compare(PriorityQueue<Pair<GAMAnswerTree, Edge>> p, PriorityQueue<Pair<GAMAnswerTree, Edge>> q) {
////			if (q.size() == 0){
////				return 1;
////			}
////			if (p.size() == 0) {
////				return -1;
////			}
//			return Comparator.comparingInt(PriorityQueue<Pair<GAMAnswerTree, Edge>>::size).compare(p, q);
//		}
//	};
	
	public QueueGroup(GAMSearch gs) {
		//System.out.print(queueType);
		this.gs = gs;
		if(queueType==QueueGroup.QueueType.MULTI)
			this.QG = new HashMap<Integer, GAMPriorityQueue>();
		else
			this.gampq=new GAMPriorityQueue(gs);
			
	}
	
	public void add(QueueEntry qe) {
		if(queueType==QueueGroup.QueueType.SINGLE) {
			this.gampq.add(qe);
			return;
		}
		GAMAnswerTree t = qe.getPair().getKey();
		Edge e = qe.getPair().getValue();
		Integer matches = t.keywordMask;
		// The lines below are to add keyword from the edge to the set of keywords of the pair
		Node source = e.getSourceNode();
		Node target = e.getTargetNode();
		
		Integer sourceKeys = gs.getKwdsMatchedBy(source);
		matches = (matches | sourceKeys);
		
		Integer targetKeys = gs.getKwdsMatchedBy(target);
		matches = (matches | targetKeys);
		
		// If the priority queue from the keyword set from the tree is still not there, than it needs to be instantiated
		if (!this.QG.containsKey(matches))
			this.QG.put(matches, new GAMPriorityQueue(gs));
		
		this.QG.get(matches).add(qe);
	}
	
	@SuppressWarnings("unused")
	private void printQG() {
		if(queueType==QueueGroup.QueueType.SINGLE) {
			System.out.println(this.gampq);
			return;
		}
		System.out.println("Here is the sets of hash maps of Queue");
		for (Integer key : QG.keySet()) {
			System.out.println(key);
		}
	}

	
	public QueueEntry poll() {
		if(queueType==QueueGroup.QueueType.SINGLE) {
			return this.gampq.poll();
		}
		GAMPriorityQueue optPQ = Collections.min(QG.values(), comparatorQG);

		QueueEntry qe = optPQ.poll();
		
		if(optPQ.isEmpty()) {
			Integer toBeRemoved = null;
			for (Integer k: QG.keySet()) {
				if (optPQ == QG.get(k)) {
					toBeRemoved = k;
					break;
				}
			}
			if(toBeRemoved != null)
				QG.remove(toBeRemoved);
		}
		return qe;
	}
	
	
	public boolean isEmpty() {
		if(queueType==QueueGroup.QueueType.SINGLE) {
			return this.gampq.isEmpty();
		}
		for (GAMPriorityQueue pq: QG.values()) {
			if(!pq.isEmpty())
				return false;
		}
		return true;
	}
	
	/**
	 * This(old) method is used to basically get a tree's root. It seems to do just
	 * that, but TODO it's worth trying to get rid of it and just asking for the
	 * root...
	 * 
	 * @param object
	 * @return
	 */
	Node getLastVisitedNode(Object object) {
		AnswerTree tree;
		if (object instanceof AnswerTree) {
			tree = (AnswerTree) object;
		} else {
			Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
			tree = pair.getLeft();
		}
		return (Node) tree.nodes().toArray()[tree.nodes().size() - 1];
	}
	
	/**
	 * The search heuristics.
	 */
	public enum QueueType {
		/**
		 * A single Priority Queue for all QueueEntries.
		 */
		SINGLE,
		
		/**
		 * One Priority Queue per subset of the set of keywords.
		 */
		MULTI
	}

}
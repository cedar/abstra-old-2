/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isBoolean;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isURI;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.EITHER;
import static java.lang.Math.max;

import com.google.common.collect.Range;
import com.wcohen.ss.Levenstein;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class LevenshteinPairProcessor extends CachedSimilarPairProcessor {

	/** The minimum string length for which to apply this similarity */
	private final int shortStringThreshold;

	/** The maximum string length for which to apply this similarity */
	private final int longStringThreshold;

	private final Levenstein sim = new Levenstein();
	
	public LevenshteinPairProcessor(StatisticsCollector stats, double th) {
		this(stats, th,
				Config.getInstance().getIntProperty("short_string_threshold"),
				Config.getInstance().getIntProperty("long_string_threshold"));
	}
	
	public LevenshteinPairProcessor(StatisticsCollector stats, double th, int shortStringThreshold, 
			int longStringThreshold) {
		super(stats, th);
		this.shortStringThreshold = shortStringThreshold;
		this.longStringThreshold = longStringThreshold;
	}

	@Override
	public Double apply(String a, String b) {
		return 1-Math.abs(sim.score(a, b))/max(a.length(), b.length());
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result =
			not(isBoolean(EITHER).or(isNumeric(EITHER)).or(isURI(EITHER)).or(sameLabel()))
				.and(ofAbsoluteLength(BOTH, Range.lessThan(longStringThreshold)));
		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}
}

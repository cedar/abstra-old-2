/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class ResultSetIterator<T> implements Iterator<T> {
	
	private final ResultSet rs;
	private final Supplier<T> f;
	private T next = null;
	
	public ResultSetIterator(ResultSet rs, Supplier<T> f) {
		this.rs = rs;
		this.f = f;
		prepareNext();
	}
	
	private void prepareNext() {
		try {
			if (rs.next()) {
				next = f.get();
			} else {
				next = null;
				rs.close();
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public T next() {
		if (next == null) {
			throw new NoSuchElementException();
		}
		T result = next;
		prepareNext();
		return result;
	}
}

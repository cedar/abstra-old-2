/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.Stoppers;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.apache.log4j.Logger;

/**
 * Abstract query search class, factorization functionalities for all (global and local) search 
 * algorithms.
 */
public abstract class QuerySearch {
	
	/** The supported global search algorithms */
	public static enum GlobalSearchAlgorithms {
		BFS_G, GAM, SQL_PATH, BFS_M, BFS_AM, GAM_E; 
	}
	protected final boolean drawingEnabled =
			Config.getInstance().getBooleanProperty("drawing.draw");
	protected final boolean drawSolutionTimes =
			Config.getInstance().getBooleanProperty("drawing.solution_times"); 

	/** The logger. */
	private final Logger log = Logger.getLogger(QuerySearch.class);
	
	/** This constructor needed in order for Explore to extend GAMSearch */ 
	public QuerySearch() {
		index = null;
		graph = null;
		scoring = null;
		stats = null; 
	};

	/** The index. */
	protected final IndexAndProcessNodes index;
	
	/** The virtual graph. */
	protected final Graph graph;
	
	/** The answer tree scoring function. */
	protected final ScoringFunction scoring;
	
	/** The statistics collectors. */
	protected final StatisticsCollector stats;

	/**
	 * Instantiates a new query search.
	 *
	 * @param i the index
	 * @param g the graph
	 * @param f the scoring function
	 * @param h the heuristic
	 * @param stats the statistics collector
	 */
	protected QuerySearch(IndexAndProcessNodes i, Graph g, ScoringFunction s, StatisticsCollector stats) {
		this.index = i;
		this.graph = g;
		this.scoring = s;
		this.stats = stats;
	}

	/**
	 * @return the scoring function
	 */
	public ScoringFunction scoring() {
		return this.scoring;
	}
	
	/**
	 * Runs the search with the given query.
	 *
	 * @param q the query
	 * @param processor the processor to use on each answer tree found for the given query 
	 */
	public abstract void run(Query q, Consumer<AnswerTree> processor); 
	
	public abstract void collectStats();	

	/** Counter for the number of calls to the path finder. */
	AtomicInteger nbCalls = new AtomicInteger(0);
	
	/** Counter for the paths founds by the path finder. */
	AtomicInteger card = new AtomicInteger(0);
	
	/**
	 * Checks whether the given answer trees have incompatible matches, i.e. if they both matches 
	 * on some given keywords, but leading to a different item. 
	 *
	 * @param left the left answer tree
	 * @param right the right answer tree
	 * @return true, if the given answer tree have compatible matches as per the above description.
	 */
	protected static boolean hasIncompatibleMatches(AnswerTree left, AnswerTree right) {
		// Case 0: If the answers match on different items for a given keywords, don't attempt to connect
		for (Integer kw : Sets.intersection(left.matches().keySet(), right.matches().keySet())) {
			// TODO: makes sure this does not exclude nodes part of the same cluster.
			if (!left.matches().get(kw).equals(right.matches().get(kw))) {
				return true;
			}
		}
		return false;
	}


}

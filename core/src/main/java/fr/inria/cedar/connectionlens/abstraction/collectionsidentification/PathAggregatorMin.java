package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import java.util.ArrayList;

public class PathAggregatorMin implements PathAggregator {

    @Override
    public double aggregate(ArrayList<Path> pathsToAggregate, boolean workingGraph) {
        double minPathTransferFactor = 1.0d;
        for(Path p : pathsToAggregate) {
            if(p.getPathTransferFactor() < minPathTransferFactor) {
                minPathTransferFactor = p.getPathTransferFactor();
            }
        }
        return minPathTransferFactor;
    }
}

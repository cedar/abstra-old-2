/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Package containing relational database-backed implementations of graph, indexing and data 
 * sources interfaces.
 */
package fr.inria.cedar.connectionlens.sql;
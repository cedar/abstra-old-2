package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection;

import com.sun.star.lang.IllegalArgumentException;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.CollectionGraphPrinting;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration.*;
import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.NON_REPORTABLE;
import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.NO_STATUS;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.*;

/**
 * This class is used to select the main entity collections.
 * Five methods are implemented for this selection, namely:
 * <ul>
 *     <li>descK: selects collections with the highest number of descendants at a depth k,</li>
 *     <li>leafK: selects collections with the highest number of leaf collections at a depth k,</li>
 *     <li>collSize: selects collections containing the highest number of nodes,</li>
 *     <li>dwPropagation: selects collections with the highest <i>dw</i> score,</li>
 *     <li>odwPageRank: selects collections with the highest PageRank score.</li>
 * </ul>
 * The selection may be limited by two parameters: the number of selected collections (E_MAX) and/or the coverage of the collections over the input dataset (COV_MIN).
 */
public class MainCollectionsIdentification extends AbstractionTask {
    public static final Logger log = Logger.getLogger(MainCollectionsIdentification.class);

    public static final int E_MAX = Config.getInstance().getIntProperty("E_MAX"); // 5 by default
    public static final double COV_MIN = Config.getInstance().getDoubleProperty("COV_MIN"); // 1 by default
    public static final double MINIMAL_ETF_FLOODING = (double) Config.getInstance().getDoubleProperty("ETF_MIN"); // 0.8 by default
    public static final IDREF_EDGES_USAGE idRefEdgesUsage = IDREF_EDGES_USAGE.valueOf(Config.getInstance().getStringProperty("use_idrefs_xml")); // disable by default

    public Configuration configuration;
    private int nbSelectedMainEntities = 0;

    public MainCollectionsIdentification(ConnectionLens cl, DataSource ds) {
        super(cl, ds);
        this.initializeVariables();
        log.debug(CollectionGraph.getInstance().getCollectionGraph());
    }

    public MainCollectionsIdentification() {

    }

    /**
     * Initialize the class members.
     * @throws SQLException if one of the query fails
     */
    private void initializeVariables() {
        log.info("MAIN COLLECTIONS IDENTIFICATION STARTS - variables initialization");

        // initialize the data weight of each collection to 0
        for (int collectionId : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            CollectionGraph.getWorkingInstance().setStatusOfCollection(collectionId, NO_STATUS);
        }
    }

    /**
     * Load the summary graph into memory and initialize allPaths and inCycle variables.
     * allPaths is a 2D array of ArrayList where a cell[i][j] contains all paths from collection i to collection j as an Arraylist.
     * allPaths does not contain cyclic edges.
     * @throws SQLException if a query fails
     * @throws IOException if the file for the final description has a problem
     */
    public void run() throws SQLException, IOException, IllegalArgumentException {
        log.info("MAIN COLLECTIONS IDENTIFICATION BEGINS");
        long startTime = System.currentTimeMillis();

        this.localCollector.start(StatisticsCollector.total());

        // We select the "most interesting" collections to report to the user
        // - "most interesting" collections are the ones that are the most weighted in the collection graph
        // - also, if we have to choose between a collection which represents a container and the collection which contains the elements in the container,
        // we prefer to report the collection of elements that the container.
        // - collection contains at least two elements
        // - collection of values are not eligible for reporting
        this.nbSelectedMainEntities = this.selectMainEntityCollections();
        log.info("reported " + CollectionGraph.getInstance().getMainCollections().size() + " collections ");
        log.info("MAIN COLLECTIONS IDENTIFICATION ENDS in " + (System.currentTimeMillis() - startTime));

        // Get their relationships
        this.getRelationshipsBetweenMainEntityCollections();
        this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_GET_RELATIONSHIPS_MEM);
        this.localCollector.stop(StatisticsCollector.total());
    }

    /**
     * Greedily select the "most interesting" collections to report to the user. We report collections in the descending order of their data weight.
     * We do not report collection of one element, nor leaf collections, nor collections of elements that do not have proper properties (i.e. collection of title).
     * When a collection is selected, the data weight of its ancestors and descendants is decreased accordingly to the weight it has in the reported collection.
     */
    private int selectMainEntityCollections() throws SQLException, IOException, IllegalArgumentException {
        this.localCollector.pause(StatisticsCollector.total());
        int nbMainEntityCollections = 0;

        // before starting the selection process, we need to make the deleted collections NOT_REPORTABLE so that they are transparent to the main collections selection process.
        this.makeTheInactiveCollectionsNonReportable();

        log.debug(CollectionGraph.getInstance().getLeafCollections());

        SCORING_METHOD sm;
        BOUNDARY_METHOD bm;
        GRAPH_UPDATE_METHOD gum;
        int k = 0;

        if (Experiment.scoringMethod.startsWith("DESC")) {
            sm = SCORING_METHOD.DESC_K;
            k = Integer.parseInt(Experiment.scoringMethod.split("_")[1]); // take the number after DESC
        } else if (Experiment.scoringMethod.startsWith("LEAF")) {
            sm = SCORING_METHOD.LEAF_K;
            k = Integer.parseInt(Experiment.scoringMethod.split("_")[1]); // take the number after DESC
        } else if (Experiment.scoringMethod.equals("wDAG")) {
            sm = SCORING_METHOD.W_DAG;
        } else if (Experiment.scoringMethod.equals("wPR")) {
            sm = SCORING_METHOD.W_PR;
        } else {
            throw new IllegalArgumentException("Bad value for scoring_method parameter. Check your local.settings.");
        }

        if (Experiment.boundaryMethod.equals("DESC")) {
            bm = BOUNDARY_METHOD.DESC;
        } else if (Experiment.boundaryMethod.equals("LEAF")) {
            bm = BOUNDARY_METHOD.LEAF;
        } else if (Experiment.boundaryMethod.equals("DAG")) {
            bm = BOUNDARY_METHOD.DAG;
        } else if (Experiment.boundaryMethod.equals("FL")) {
            bm = BOUNDARY_METHOD.FL;
        } else if (Experiment.boundaryMethod.equals("FL_AC")) {
            bm = BOUNDARY_METHOD.FLAC;
        } else {
            throw new IllegalArgumentException("Bad value for boundary_method parameter. Check your local.settings.");
        }

        if (sm.equals(SCORING_METHOD.DESC_K) || sm.equals(SCORING_METHOD.LEAF_K)) {
            gum = GRAPH_UPDATE_METHOD.UPDATE_BOOLEAN;
        } else {
            gum = GRAPH_UPDATE_METHOD.UPDATE_EXACT;
        }

        this.localCollector.resume(StatisticsCollector.total());
        this.configuration = new Configuration(sm, k, bm, gum, idRefEdgesUsage, this.cl, this.ds, this);

//        CollectionGraphPrinting collectionGraphPrinting = new CollectionGraphPrinting(this.cl, this.ds, this.configuration.mainCollectionsIdentification);
//        collectionGraphPrinting.printAllSources(false);
//
//        if(true) {
//            throw new RuntimeException("Stop here.");
//        }

        this.configuration.run(this.localCollector);
        nbMainEntityCollections = this.configuration.nbCollectionsReportedCounter;
        double coverageNodes = this.configuration.getNodeCoverage();
        double coverageCollections = this.configuration.getCollectionCoverage();
        log.info("Coverage of nodes is " + coverageNodes);
        log.info("Coverage of collections is " + coverageCollections);
        return nbMainEntityCollections;
    }

    /**
     * Get the relationships that have been found after the RandC. This fills the this.relationshipsCollections variable.
     * @throws SQLException if a SQL query fails
     */
    private void getRelationshipsBetweenMainEntityCollections() throws SQLException {
        MainCollectionsRelationsFinding relationshipsFinding = new MainCollectionsRelationsFinding(this.graph, this.ds);
        relationshipsFinding.run();
    }

    public void makeTheInactiveCollectionsNonReportable() throws SQLException {
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT collId FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " WHERE isActive = false;");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            CollectionGraph.getInstance().setStatusOfCollection(rs.getInt(1), NON_REPORTABLE);
        }
    }

    public int getNbSelectedMainEntities() {
        return this.nbSelectedMainEntities;
    }

    public void reportStatistics() {
        if(this.localCollector != null) {
            this.localCollector.stopAll();
            this.localCollector.reportAll();
        }
    }

    public Configuration getConfiguration() {
        return this.configuration;
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import java.util.HashMap;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.search.GAMSearch;

/**
 * A class that adds caching capability to any scorable object.
 */
public abstract class CachedScorable implements Scorable {

	/** The logger. */
	private final Logger log = Logger.getLogger(CachedScorable.class);
	
	/** The score cache. */
	protected HashMap<ScoringFunction, Score> scoreCache = null;

	/**
	 * Instantiates a new cached scorable.
	 */
	protected CachedScorable() {
		scoreCache = new HashMap<>();
	}

	/**
	 * Instantiates a new cached scorable.
	 *
	 * @param cache the cache
	 */
	protected CachedScorable(@Nullable ScoringFunction sf, @Nullable Score cache) {
		this.scoreCache = new HashMap<>();
		scoreCache.put(sf, cache);
	}
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.score.Scorable#score(fr.inria.cedar.connectionlens.score.ScoringFunction)
	 */
	@Override
	public Score score(ScoringFunction f) {
		if (scoreCache.get(f) == null) {
			scoreCache.put(f, f.compute(this));
		}
		return scoreCache.get(f);
	}



	/**
	 * Invalidate.
	 */
	public void invalidate() {
		this.scoreCache = null;
	}
	
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameNormaLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.withTypes;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.SOURCE;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.TARGET;

import java.util.Locale;

import com.wcohen.ss.Jaro;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;


public class OrganizationPairProcessor extends CachedSimilarPairProcessor {
	private final Node.Types entityType;
	private final double lengthRatio;
	private final Jaro sim = new Jaro();
	private double th;
	private Locale l;

	protected OrganizationPairProcessor(StatisticsCollector stats, double th) {
		this(stats, ENTITY_ORGANIZATION, th, Config.getInstance().getDoubleProperty("length_difference_ratio"));
	}

	protected OrganizationPairProcessor(StatisticsCollector stats, Node.Types t, double th, 
			double lengthRatio) {
		super(stats, th);
		this.th = th;
		this.entityType = t;
		this.lengthRatio = lengthRatio;
		this.l = Locale.forLanguageTag(Config.getInstance().getProperty("default_locale"));
	}

	@Override
	public NodePairSelector selector() {
		return withTypes(SOURCE, entityType) // source must be of this type
					.and(withTypes(TARGET, entityType)) 
					.and(sameNormaLabel()); 
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length()};
	}

	@Override
	public Double apply(String a, String b) {
		return sim.score(a, b);
	}
}

package fr.inria.cedar.connectionlens.abstraction.classification;

import fr.inria.cedar.connectionlens.graph.Node;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

public class SemanticProperty implements Comparable<SemanticProperty> {
    public static Logger log = Logger.getLogger(SemanticProperty.class);

    private String label = null;
    private ArrayList<String> normalizedLabel = null;
    //private ArrayList<Node.Types> domain = new ArrayList<>();
    //private ArrayList<Node.Types> range = new ArrayList<>();
    private ArrayList<Integer> domain = new ArrayList<>(); // integer correspond to the id of the category
    private ArrayList<Integer> range = new ArrayList<>(); // integer correspond to the id of the type

    public String getLabel() { return this.label; }

    public ArrayList<String> getNormalizedLabel() {
        return this.normalizedLabel;
    }

    public void setNormalizedLabel(ArrayList<String> normalizedLabel) {
        this.normalizedLabel = normalizedLabel;
    }

    public ArrayList<Integer> getDomain() {
        return this.domain;
    }

    public ArrayList<Integer> getRange() {
        return this.range;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDomainWithIntTypes(ArrayList<Integer> domain) {
        this.domain = domain;
    }

    public void setDomain(ArrayList<String> _domain) {
        if(_domain != null) {
            for(String s : _domain) {
                // the enum does not contain the type, let's search for it in the GitTables categories
                int keyAssociatedToValue = Classification.getListOfCategories().getCategoryId(s);
                this.domain.add(keyAssociatedToValue);
            }
        }
    }

    public void setRangeWithIntTypes(ArrayList<Integer> range) {
        this.range = range;
    }

    public void setRange(ArrayList<String> _range) {
        if(_range != null) {
            for(String s : _range) {
                try {
                    // this is a node type declared in Node.Types (entities are still declared in Node.Types)
                    Node.Types category = Node.Types.valueOf(s); // if s does not exist in the enum, it will raise an error
                    this.range.add(category.ordinal());
                } catch (IllegalArgumentException ex) {
                    // the enum does not contain the type, let's search for it in the GitTables categories
                    int keyAssociatedToValue = Classification.getCategoryId(s);
                    this.range.add(keyAssociatedToValue);
                }
            }
        }
    }

    public void addToDomain(Integer category) {
        this.domain.add(category);
    }

    public void addToRange(Integer type) {
        this.range.add(type);
    }

    @Override
    public int compareTo(SemanticProperty o) {
        return this.getLabel().compareTo(o.getLabel());
    }      // Compliant

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SemanticProperty) {
            return this.compareTo((SemanticProperty) obj) == 0;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(label);
    }

    @Override
    public String toString() {
        String res =  "SemanticProperty {" +
                "label='" + this.label + "'" +
                ", normalizedLabel='" + this.normalizedLabel + "'" +
                ", domain=" + StringUtils.join(this.domain.stream().map(i -> Classification.getCategoryLabel(i)).collect(Collectors.toList()));
        res += ", range=[";
        for(Integer rangeElement : this.range) { // range can hold entities or categories
            String categoryLabel = Classification.getCategoryLabel(rangeElement);
            if(!categoryLabel.isEmpty()) {
                res += categoryLabel + ", ";
            } else {
                res += Node.Types.values()[rangeElement] + ", ";
            }
        }
        res += "]";
        res += '}';
        return res;
    }
}

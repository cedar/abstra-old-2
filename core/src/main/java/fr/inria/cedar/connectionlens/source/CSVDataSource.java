/*
 * Copyright(C) 2021 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 */

package fr.inria.cedar.connectionlens.source;

import java.io.*;
import java.net.URI;
import java.util.Map;
import java.util.function.Consumer;

//import com.opencsv.CSVParser;
import com.opencsv.CSVReaderHeaderAware;
import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

import static fr.inria.cedar.connectionlens.graph.Node.Types.RELATIONAL_STRUCT;

/**
 * Implementation of a CSV DataSource.
 *
 * @author Prajna Upadhyay
 *
 */
public class CSVDataSource extends DataSource implements Serializable {

    private static final long serialVersionUID = 1L;

    public CSVDataSource(Factory f, int identifier, URI uri, URI origURI, EntityExtractor extractor,
                           StatisticsCollector graphStats, StatisticsCollector extractStats, Graph graph) {
        super(f, identifier, uri, origURI, Types.CSV, graphStats, extractStats, graph);
        log = Logger.getLogger(CSVDataSource.class);
    }


    @Override
    public String getContext(Graph g, Node n) {
        return "<i>Content of the CSV node:</i> <b>" + n.getLabel() + "</b>";
    }

    @Override
    public void traverseEdges(Consumer<Edge> processor) throws IOException, CsvValidationException {
        processEdgesToLocalAndOriginalURIs(processor);
        String file = localURI.toString().replace("file:","");

        CSVReaderHeaderAware reader = null;
        reader = new CSVReaderHeaderAware(new FileReader(file));
        Map<String, String> values;
        boolean containsHeader = true;

        Node topNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
        processor.accept(buildLabelledEdge(this.root, topNode, Edge.ROOT_LABEL));

        while (true) {
            try {
                // this part succeeds if there is a header for the csv file,
                // and associates each value to each header
                if ((values = reader.readMap()) == null) break;
                Node rowNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
                processor.accept(buildLabelledEdge(topNode, rowNode, Edge.TUPLE_EDGE_LABEL));
                for(String columnName:values.keySet()) {
                    Node valNode = createOrFindValueNodeWithAtomicity(values.get(columnName),
                            "", this, getContextForEntityExtraction("."+columnName),
                            Node.Types.RELATIONAL_VALUE);
                    Edge edge = buildLabelledEdge(rowNode, valNode, columnName);
                    edge.setDataSource(this);
                    processor.accept(edge);
                }


            } catch (IOException e) {
                // in case there is no header in the csv file,
                // an exception is thrown which is caught here
                containsHeader = false;
                break;
            }
        }
        if(!containsHeader) {
            // when no header is present, we read it
            //  as an array of strings. We populate
            //  the edges with empty labels
            log.info("CSV file does not contain a header");
            CSVReader csvreader = null;
            csvreader = new CSVReader(new FileReader(file));
            String[] lineInFile;
            while (true) {
                if ((lineInFile = csvreader.readNext()) == null) break;
                Node rowNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
                processor.accept(buildLabelledEdge(topNode, rowNode, Edge.TUPLE_EDGE_LABEL));
                for(int i=0; i<lineInFile.length;i++) {
                    Node valNode = createOrFindValueNodeWithAtomicity(lineInFile[i],
                            "", this, Node.Types.RELATIONAL_VALUE);
                    // since we do not have headers, we build unlabelled edges
                    Edge edge = buildUnlabelledEdge(rowNode, valNode, 1.0);
                    edge.setDataSource(this);
                    processor.accept(edge);
                }
            }
        }
    }

    // if we know the entity extraction "context" -- that some
    // attributes will always extract entities of a type, then
    // use that information before extraction

    public int getContextForEntityExtraction(String path) {
        int thisNodeContext;
        Integer pathNo = contextMap.get(path);
        if (pathNo == null) {
            pathNo = contextMap.size();
            contextMap.put(path, pathNo);
            revContextMap.put(pathNo, path);
        }
        thisNodeContext = pathNo.intValue();

        return thisNodeContext;

    }
    @Override
    public void postprocess(Graph graph) {
        // No-OP.
    }


}

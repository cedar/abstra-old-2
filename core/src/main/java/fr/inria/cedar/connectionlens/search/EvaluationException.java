/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

/**
 * Exception thrown when a certain condition is met.
 */
public class EvaluationException extends RuntimeException {

	/** */
	private static final long serialVersionUID = 7828953335690526825L;

	/**
	 * Instantiates a new condition reached exception.
	 */
	public EvaluationException() {
		super();
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param msg the exception message
	 */
	public EvaluationException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param cause the exception's underlying cause
	 */
	public EvaluationException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param msg the exception message
	 * @param cause the exception's underlying cause
	 */
	public EvaluationException(String msg, Throwable cause) {
		super(msg, cause);
	}
}

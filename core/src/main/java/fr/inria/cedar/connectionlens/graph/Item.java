/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Objects.requireNonNull;

import fr.inria.cedar.connectionlens.source.DataSource;
import javax.annotation.Nullable;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * An item is either a node or an edge
 */
public abstract class Item implements Serializable {

	/** Supported item types. */
	public static interface Types {
		String name();
		int ordinal();
	}
	
	/** The item's label. */
	protected String label;
	
	/** The item's ID. */
	protected ItemID id;

	/** The item's data source. */
	protected DataSource ds;

	/**
	 * Instantiates a new item.
	 * @param id some ID
	 * @param l some label
	 * @param ds some data source
	 */
	public Item(ItemID id, @Nullable String l, DataSource ds) {
		super();
		requireNonNull(id);
		requireNonNull(ds);
		this.id = id;
		this.ds = ds;
		this.label = nullToEmpty(l).trim(); // NELLY 38 spet 2022: trim is required for XML (the label partitioning needs it)
	}

	/**
	 * Serialize the item to JSON.
	 *
	 * @return a JSON serialization of this object.
	 */
	public abstract JSONObject serialize();


	/**
	 * Checks if this item is a node.
	 *
	 * @return true, iff this item is a node
	 */
	public boolean isNode() {
		return false;
	}
	
	/**
	 * @return the item's data source
	 */
	public DataSource getDataSource() {
		return this.ds;
	}
	
	/**
	 * @return the item's type
	 */
	public abstract Types getType();

	/**
	 * @return the item's label
	 */
	public String getLabel() {
		return label;
	}

    public boolean isEntity() {
		return false;
	}

	public boolean isValue() {
		return false;
	}

	public boolean isExtractedLabel() {
		return false;
	}

	/**
	 * @return the item's ID
	 */
	public abstract ItemID getId();
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.score.CachedScorable;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.util.ExceptionTools;

/**
 * Class representing an answer tree, composed of:
 * <ul>
 * <li>a set of edges <code>Edge</code>,</li>
 * <li>a root node <code>Node</code>,</li>
 * <li>a score,</li>
 * <li>a set of leaves that take form of a map (Keyword - Item)</li> (? to
 * review)
 * </ul>
 *
 * @author Mihu
 */
public class AnswerTree extends CachedScorable implements Serializable {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = 5172697113587799248L;

    /**
     * The logger.
     */
    private static final Logger log = Logger.getLogger(AnswerTree.class);

    /**
     * Set of edges
     */
    protected final Set<Edge> edges;

    /**
     * Set of nodes
     */
    protected final Set<Node> nodes;

    /**
     * The local names.
     */
   // private transient final Map<Item, String> localNames;

    /**
     * Root node of the answer tree (node that represents the data source itself)
     * TODO: consider removing the root (there is no obvious use for it).
     */
    protected final Node root;

    /**
     * Mapping between keyword and the nodes/edges in the graph whose label is a match
     */
    private SetMultimap<Integer, Item> keywordMap;
    
    /** An integer with bits set to 1 for each matched kwd */ 
    int keywordMask = 0; 
    /** The same, but just with the matches in the tree root */
    int rootKeywordMask = 0; 
     
    
    private Integer hash;
    
    protected String jsonString = null; // IM, 11/11/11: for efficient JSON serialization

    /**
     * Checks if a given two answer trees can be merged, that is share at least one node.
     *
     * @param left  the left answer tree
     * @param right the right answer tree
     * @return true, iff the left and right answer trees can be merged.
     */
   
    /**
     * Merges the given two answer trees.
     *
     * @param left  the left answer tree
     * @param right the right answer tree
     * @return a new answer tree resulting from the merging of the given left and right answer trees
     */
    public static AnswerTree merge(AnswerTree left, AnswerTree right) {
        return new AnswerTree(
        		left.root,
        		Sets.union(left.nodes(), right.nodes()),
        		Sets.union(left.edges(), right.edges()),
        		left.matches(), right.matches());

    }

    @Override
    public int hashCode() {
    	if (hash == null) {
    		hash = makeHash();
    	}
    	return hash;
    }
    
    protected int makeHash() {
        return Objects.hash(nodes, Sets.union(edges, reversedEdges()), keywordMap);
    }
    
    protected Set<Edge> reversedEdges() {
    	ImmutableSet.Builder<Edge> result = ImmutableSet.builder();
    	edges.forEach(e-> result.add(e.reverse()));
    	return result.build();
    }

    /** This condition must accept trees with different roots
     *  but otherwise identical!!! It is thus important that
     *  the condition tests nothing related to the root.
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if(that instanceof AnswerTree) {
        		AnswerTree aThat = (AnswerTree) that; 
        		if ((keywordMask == aThat.keywordMask) 
        			&& aThat.nodes.equals(nodes)
        			// IM, 16/1/21: the kwd map should be determined by the nodes 
        			&& aThat.keywordMap.equals(keywordMap)
        			&& equalEdgeSets(edges, aThat.edges)) {
        			return true; 
        		}
        }
        return false; 
    }

    
    private static boolean equalEdgeSets(Set<Edge> left, Set<Edge> right) {
    	// Equivalence edges are not taken into account
    	for (Edge e: left) {
    		if ((e.getEdgeType() != Edge.Types.SAME_AS_EDGE) || (e.getConfidence() != 1.0) ) {
    			if (!right.contains(e)){
    				return false; 
    			}
    		}
    	}
    	for (Edge e: right) {
    		if ((e.getEdgeType() != Edge.Types.SAME_AS_EDGE) || (e.getConfidence() != 1.0) ) {
    			if (!left.contains(e)){
    				return false; 
    			}
    		}
    	}
    	return true; 
    }
    
    
   
    
    AnswerTree(Item item, Set<Integer> queryIndices) {
    	 
        ImmutableSet.Builder<Node> nodesBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<Edge> edgesBuilder = ImmutableSet.builder();
        ImmutableSetMultimap.Builder<Integer, Item> matchesBuilder = ImmutableSetMultimap.builder();

        if (item instanceof Node) {
        	Node n = (Node) item; 
            this.root = n; 
            nodesBuilder.add(n); 
        } else {
        	checkArgument(item instanceof Edge);
        	Edge e = (Edge) item; 
            this.root = e.getSourceNode();
            edgesBuilder.add(e); 
        }    
        for (Integer queryIndex: queryIndices) {
        	matchesBuilder.put(queryIndex, item);
        	this.keywordMask = this.keywordMask | (1 << Integer.valueOf(queryIndex)); 
        	if ((item instanceof Node) && (this.root.equals(item))) {
        			this.rootKeywordMask = this.rootKeywordMask |(1 << Integer.valueOf(queryIndex));         					
        	}
        }
        this.keywordMap = matchesBuilder.build();
        this.nodes = nodesBuilder.build();
        this.edges = edgesBuilder.build();
    }

    /** This constructor also computes the keywordMask */
    AnswerTree(Node root, Set<Node> nodes, Set<Edge> edges,  
    		SetMultimap<Integer, Item>... matches) {
    	this.root = root;

    	ImmutableSet.Builder<Node> nodesBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<Edge> edgesBuilder = ImmutableSet.builder();
        ImmutableSetMultimap.Builder<Integer, Item> matchesBuilder = ImmutableSetMultimap.builder();

        for (SetMultimap<Integer, Item> m: matches) {
        	for (Map.Entry<Integer, Item> e: m.entries()) {
        		matchesBuilder.put(e.getKey(), e.getValue());
        		this.keywordMask = this.keywordMask | (1 << Integer.valueOf(e.getKey())); 
        		if ((e.getValue() instanceof Node) && (root.equals(e.getValue()))) {
            			this.rootKeywordMask = this.rootKeywordMask | (1 << Integer.valueOf(e.getKey()));         					
            	}
        	}
        }
        for (Node n: nodes) {
        	nodesBuilder.add(n); 
        }
        for (Edge e: edges) {
        	edgesBuilder.add(e); 
        }
        this.nodes = nodesBuilder.build();
        this.edges = edgesBuilder.build();
        this.keywordMap = matchesBuilder.build();
    }

    AnswerTree(Set<Node> nodes, Set<Edge> edges, //Set<Integer> kwdsMatchedInRoot,  
    		SetMultimap<Integer, Item>... matches) {
    	this(nodes.isEmpty() ? edges.iterator().next().getSourceNode(): nodes.iterator().next(),
    			nodes, edges, 
    			matches);
    }

    /**
     * Verify whether the tree is empty
     *
     * @return true iff the answer tree is empty.
     */
    public boolean isEmpty() {
        return (root == null && this.edges.isEmpty() && this.nodes.isEmpty() && this.keywordMap.isEmpty());
    }

    /**
     * Get the set of edges from the answer tree
     *
     * @return set of edges
     */
    @Override
    public Set<Edge> edges() {
        return edges;
    }
    
    /**
     * @return the root of the answer tree.
     */
    public Node root() {
    	return this.root;
    }

    /**
     * Return the set of nodes in the answer tree
     *
     * @return set of nodes
     */
    @Override
    public Set<Node> nodes() {
        return nodes;
    }

    public Integer countNodes() {
        return nodes.size();
    }

    /**
     * Return the mapping in the leaves: (keyword - node/edge uri there is a match)
     *
     * @return all matches found in the leaves of this tree
     */
    public SetMultimap<Integer, Item> matches() {
        return this.keywordMap;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     * IM, 16/11/2021: this does some JSON pretty-printing. It should not be called when performance is an issue. 
     */
    @Override
    public String toString() {
       return new JSONObject(toJSONString()).toString(2);  
    }
    /**
     * IM 11/11/11: attempt to replace efficiently a JSON serialization
     * missing the matches for now
     * <code>
     * { 																	<br>
     * "score": ..., 		// score of the answer tree	<br>
     * "nodes": ..., 		// list of nodes					<br>
     * "links": ..., 		// list of edges					<br>
     * "matches": ...,	// list of matches				<br>
     * }
     * </code>
     */
    public String toJSONString(ScoringFunction... sf) {
    	if (this.jsonString != null) {
    		return this.jsonString; 
    	}
    	//log.info("toJSONString"); 
    	StringBuffer sb = new StringBuffer();
    	sb.append("{ ");
    	if (sf == null) {
    		//log.info("*** Null scoring function!"); 
    	}
    	else {
    		if (sf.length > 0) {
    			sb.append("\"score\": " +  this.score(sf[0]) + ", "); 
    		}
    		else {
    			//log.info("***  " + this.ownKey() + " empty scoring function array!"); 
    			try {
    				throw new Exception("Empty scoring function array");
    			}
    			catch(Exception e) {
    				ExceptionTools.prettyPrintException(e, 10); 
    			}
    		}
    	}
    	Map<Integer, Integer> global2LocalNodeMap = new HashMap<>();

    	// nodes: 
    	sb.append("\"nodes\": [ ");
    	int iLocal = 0; 
    	for (Node n: nodes) {
    		Integer g = (Integer)(n.getId().value()); 
    		Integer l = global2LocalNodeMap.get(g); 
    		if (l == null) {
    			sb.append("{"); 
    			l = new Integer(iLocal); 
    			iLocal ++; 
    			global2LocalNodeMap.put(g, l);
    			sb.append("\"local_id\": \"N" + l + "\", "); 
    			sb.append("\"global_id\": \"" + g + "\", ");
    			sb.append("\"source_type\": \"" + n.getDataSource().getType() + "\", ");
    			sb.append("\"label\": \"" + n.sanitizeStringLabel() + "\", ");
    			sb.append("\"source\": \"" + n.getDataSource().getLocalURI().toString() + "\", "); 
    			sb.append("\"type\": \"" + n.getType() + "\", ");
    			sb.append("\"is_root\": \"" + this.root.equals(n) + "\""); 
    			sb.append("}");
    			if (iLocal < nodes.size()) {
    				sb.append(", "); 
    			}
    		}
    	}
    	sb.append("], "); // end of nodes
    	
    	// edges: 
    	sb.append("\"links\": [ ");
    	int iEdge = 0; 
    	for (Edge e: edges) {
			sb.append("{");
    		Integer eNo = (Integer)(e.getId().value()); 
    		sb.append("\"local_id\": \"L" + iEdge + "\", "); 
    		iEdge ++; 
    		sb.append("\"confidence\": " + e.confidence() + ", "); 
    		sb.append("\"origin\": \"" + e.getDataSource().getLocalURI().toString() + "\", "); 
    		sb.append("\"global_id\": \"" + eNo + "\","); 
    		sb.append("\"specificity\": " + e.getSpecificityValue() + ", "); 
    		sb.append("\"same_as\": \"" + e.isSameAs() + "\", "); 
    		sb.append("\"label\": \"" + e.getLabel() + "\", ");
    		Integer sourceLocalNo = global2LocalNodeMap.get((Integer)(e.getSourceNode().getId().value())); 
    		sb.append("\"source\": \"N" + sourceLocalNo + "\", "); 
    		Integer targetLocalNo = global2LocalNodeMap.get((Integer)(e.getTargetNode().getId().value())); 
    		sb.append("\"target\": \"N" + targetLocalNo + "\" "); 
        	sb.append("}" + ((iEdge < edges.size())?",":"") + " "); // add a comma only if not last
    	}
    	sb.append("]"); // end of edges
    	sb.append("}"); // end of tree
    	jsonString = new String(sb);
    	return jsonString; 
    }
    
    
    public String shortLabel() {
    	StringBuffer sb = new StringBuffer();
    	for (Edge e: edges) {
    		NumericEdgeID neid = (NumericEdgeID) e.getId();
    		if (neid != null) { sb.append(neid.value() + " "); }
    	}
    	return new String(sb); 
    }

    public String shortLabelString() {
    	StringBuffer sb = new StringBuffer();
    	Node first = nodes.iterator().next(); 
    	sb.append(first.getLabel()+ " ");
    	for (Edge e: edges) {
    		sb.append(e.getLabel() + " ");
    	}
    	if (nodes.size() > 1) {
    		Node last = first; 
    		Iterator<Node> iNodes = nodes.iterator();
    		while (iNodes.hasNext()) {
    			last = iNodes.next();
    		}
    		sb.append(last);
    	}
    	return new String(sb); 
    }
    

	/**
	 * @return true if the root has 1 edge which is labeled sameAs (can be weak or strong)
	 */
	public boolean isGrownThroughSameAs() {
		int rootEdgeCount = 0; 
		int rootSameAsEdgeCount = 0; 
		for (Edge e: edges) {
			if (e.getSourceNode().equals(this.root) ||
					e.getTargetNode().equals(this.root)) {
				rootEdgeCount ++;
				if (e.getLabel().equals(Edge.SAME_AS_LABEL)) {
					rootSameAsEdgeCount ++; 
				}
			}
		}
		if ((rootEdgeCount == 1) && (rootSameAsEdgeCount == 1)) {
			return true; 
		}
		return false;
	}
    

    /**
     * An answer tree comparator, ordering by decreasing scores.
     */
    public static class Comparator implements java.util.Comparator<AnswerTree> {

        /**
         * The scoring function.
         */
        final ScoringFunction scoring;

        /**
         * Instantiates a new comparator.
         *
         * @param s the scoring function
         */
        public Comparator(ScoringFunction s) {
            this.scoring = s;
        }

        /**
         * {@inheritDoc}
         *
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(AnswerTree o1, AnswerTree o2) {
        	//log.info("comparing " + o1.ownKey() + " " + o2.ownKey()); 
            final int r = o2.score(scoring).compareTo(o1.score(scoring));
            if (r == 0 && !o1.equals(o2)) {
                return -1;
            }
            return r;
        }
    }
    
}

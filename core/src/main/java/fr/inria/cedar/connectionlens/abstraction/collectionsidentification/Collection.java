package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

public class Collection implements Comparable<Collection> {
    private int id;
    private double specificity;
    public Collection(){}

    public Collection(int id, double specificity){
        this.id = id;
        this.specificity = specificity;
    }

    @Override
    public int compareTo(Collection c){
        return c.specificity >= this.specificity ? -1 : 1;
    }

    @Override
    public boolean equals(Object c){ return (((Collection)c).id == this.id && ((Collection)c).specificity == this.specificity); }

    public int getId(){ return this.id; }
    public double getSpecificity(){ return this.specificity; }
}

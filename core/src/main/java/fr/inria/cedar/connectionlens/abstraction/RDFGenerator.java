package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

public class RDFGenerator {

    public static final Logger log = Logger.getLogger(RDFGenerator.class);

    private static int totalNumberOfTriples;

    public RDFGenerator() throws IOException {

    }

    public static void main(String[] args) throws IOException {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        int nbAuthor = 20;
        int nbPaper = 10;
        int nbConference = 2;
        int nbLab = 0;


        totalNumberOfTriples = 0;

        String filename = "dataset";
        if(nbAuthor > 0) { filename += "-A"+nbAuthor; }
        if(nbPaper > 0) { filename += "-P"+nbPaper; }
        if(nbConference > 0) { filename += "-C"+nbConference; }
        if(nbLab > 0) { filename += "-L"+nbLab; }
        filename += ".nt";
        File dataset = new File(Paths.get(Config.getInstance().getStringProperty("temp_dir"), filename).toString());
        log.info(dataset.getAbsolutePath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(dataset));

        generatePaperData(nbPaper, chars, numbers, bw);
        generateAuthorData(nbAuthor, chars, numbers, bw);
        generateConferenceData(nbConference, chars, numbers, bw);
//        generateLabData(nbLab, chars, bw);
        bw.flush();
        bw.write("\n\n");
        generateLinksAuthorHasWrittenPaper(nbAuthor, nbPaper, bw);
//        generateLinksPaperWrittenByAuthor(nbPaper, nbAuthor, bw);
//        generateLinksAuthorParticipatesInConference(nbAuthor, nbConference, bw);
        generateLinksConferenceInvitesAuthor(nbConference, nbAuthor, bw);
//        generateLinksConferenceHeldInLab(nbConference, nbLab, bw);
//        generateLinksLabEmploysAuthor(nbLab, nbAuthor, bw);
//        generateLinksConferencePublishesPaper(nbConference, nbPaper, bw);
        generateLinksPaperPublishedInConference(nbPaper, nbConference, bw);
//        bw.flush();
//        bw.write("\n\n");

        // create loops
//        generateLoopsPaperAuthor(nbPaper, nbAuthor, bw);
//        generateLoopsAuthorConference(nbAuthor, nbConference, bw);
//        generateLoopsPaperConference(nbPaper, nbConference, bw);
        bw.flush();
        bw.close();

        log.info("number of triples generated: " + totalNumberOfTriples);
    }

    private static void generateAuthorData(int nbAuthor, String chars, String numbers, BufferedWriter bw) throws IOException {
        log.info("generating data for " + nbAuthor + " Author");
        Random rand = new Random();

        for(int i = 0 ; i < nbAuthor ; i++) {
            boolean hasEmail = rand.nextBoolean();
            boolean hasBirthDate = rand.nextBoolean();
            boolean hasPrefix = rand.nextBoolean();
            bw.write("<http://inria.fr/Author" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/Author> .\n");
            totalNumberOfTriples++;
//            bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/name> \"" + generateRandomName(chars) + "\".\n");
//            totalNumberOfTriples++;
            bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/firstName> \"" + generateRandomWord(chars, 5) + "\".\n");
            totalNumberOfTriples++;
            bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/lastName> \"" + generateRandomWord(chars, 5) + "\".\n");
            totalNumberOfTriples++;
//            if(hasEmail) {
//                bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/email> \"" + generateRandomWord(chars, 10) + "\".\n");
//                totalNumberOfTriples++;
//            }
//            if(hasBirthDate) {
//                bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/birthDate> \"" + generateRandomYear(numbers) + "\".\n");
//                totalNumberOfTriples++;
//            }
//            if(hasPrefix) {
//                bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/honorificPrefix> \"Dr.\".\n");
//                totalNumberOfTriples++;
//            }
            bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/affiliation> \"" + generateRandomWord(chars, 5) + "\".\n");
//            bw.write("<http://inria.fr/Author" + i + "> <http://xmlns.com/foaf/0.1/gender> \"" + generateRandomGender() + "\".\n");
            totalNumberOfTriples+=2;
        }

    }

    private static void generatePaperData(int nbPaper, String chars, String numbers, BufferedWriter bw) throws IOException {
        log.info("generating data for " + nbPaper + " Paper");
        Random rand = new Random();

        for(int i = 0 ; i < nbPaper ; i++) {
            boolean hasYear = rand.nextBoolean();
            boolean hasDoi = rand.nextBoolean();
            boolean hasTitle = rand.nextBoolean();
            bw.write("<http://inria.fr/Paper" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/Paper> .\n");
            totalNumberOfTriples++;
            bw.write("<http://inria.fr/Paper" + i + "> <http://www.w3.org/2000/01/rdf-schema#title> \"" + generateRandomTitle(chars) + "\".\n");
            totalNumberOfTriples++;
            bw.write("<http://inria.fr/Paper" + i + "> <http://inria.fr/year> \"" + generateRandomYear(numbers) + "\".\n");
            totalNumberOfTriples++;
//            if(hasDoi) {
//                bw.write("<http://inria.fr/Paper" + i + "> <http://inria.fr/doi> \"" + generateRandomNumber(chars, 10) + "\".\n");
//                totalNumberOfTriples++;
//            }
        }
    }

    private static void generateConferenceData(int nbConference, String chars, String numbers, BufferedWriter bw) throws IOException {
        log.info("generating data for " + nbConference + " Conference");

        for(int i = 0 ; i < nbConference ; i++) {
            bw.write("<http://inria.fr/Conference" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/Conference> .\n");
//            bw.write("<http://inria.fr/Conference" + i + "> <http://inria.fr/year> \"" + generateRandomYear(numbers) + "\".\n");
            bw.write("<http://inria.fr/Conference" + i + "> <http://inria.fr/place> \"" + generateRandomWord(chars, 5) + "\".\n");
            bw.write("<http://inria.fr/Conference" + i + "> <http://inria.fr/duration> \"" + generateRandomNumber(chars, 1) + " days\".\n");
//            bw.write("<http://inria.fr/Conference" + i + "> <http://inria.fr/organizer> \"" + generateRandomWord(chars, 4) + "\".\n");
            totalNumberOfTriples+= 5;
        }
    }

    private static void generateLabData(int nbLabs, String chars, BufferedWriter bw) throws IOException {
        log.info("generating data for " + nbLabs + " Lab");

        for(int i = 0 ; i < nbLabs ; i++) {
            bw.write("<http://inria.fr/Laboratory" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/Laboratory> .\n");
            bw.write("<http://inria.fr/Laboratory" + i + "> <http://inria.fr/place> \"" + generateRandomWord(chars, 5) + "\".\n");
            bw.write("<http://inria.fr/Laboratory" + i + "> <http://inria.fr/director> \"" + generateRandomName(chars) + "\".\n");
            bw.write("<http://inria.fr/Laboratory" + i + "> <http://inria.fr/numberOfEmployee> \"" + generateRandomNumber(chars, 3) + "\".\n");
            totalNumberOfTriples+= 4;
            for(int j = 0 ; j < 10 ; j++) {
                bw.write("<http://inria.fr/Laboratory" + i + "> <http://inria.fr/employee> \"" + generateRandomName(chars) + "\".\n");
                totalNumberOfTriples++;
            }
        }
    }

    private static void generateLinksConferencePublishesPaper(int nbConference, int nbPaper, BufferedWriter bw) throws IOException {
        log.info("generating links Conference --publishes--> Paper for " + nbConference + " Conference and " + nbPaper + " Paper");

        Random rnd = new Random();
        for(int i = 0 ; i < nbPaper ; i++) {
            int randomConferenceId = rnd.nextInt(nbConference);
            bw.write("<http://inria.fr/Conference" + randomConferenceId + "> <http://inria.fr/publishes> <http://inria.fr/Paper" + i + "> .\n");
            totalNumberOfTriples++;
        }
    }

    private static void generateLinksPaperPublishedInConference(int nbPaper, int nbConference, BufferedWriter bw) throws IOException {
        log.info("generating links Paper --publishedIn--> Conference for " + nbPaper + " Paper and " + nbConference + " Conference");

        Random rnd = new Random();
        for(int i = 0 ; i < nbPaper ; i++) {
            int randomConferenceId = rnd.nextInt(nbConference);
            bw.write("<http://inria.fr/Paper" + i + "> <http://inria.fr/publishedIn> <http://inria.fr/Conference" + randomConferenceId + "> .\n");
            totalNumberOfTriples++;
        }
    }

    private static void generateLinksConferenceInvitesAuthor(int nbConference, int nbAuthor, BufferedWriter bw) throws IOException {
        log.info("generating links Conference --invites--> Author for " + nbConference + " Conference and " + nbAuthor + " Author");

        Random rnd = new Random();
        for(int i = 0 ; i < nbAuthor ; i++) {
            int randomConferenceId = rnd.nextInt(nbConference);
            bw.write("<http://inria.fr/Conference" + randomConferenceId + "> <http://inria.fr/invites> <http://inria.fr/Author" + i + "> .\n");
            totalNumberOfTriples++;
        }
    }

    private static void generateLinksAuthorParticipatesInConference(int nbAuthor, int nbConference, BufferedWriter bw) throws IOException {
        log.info("generating links Author --participatedIn--> Conference for " + nbAuthor + " Author and " + nbConference + " Conference");

        Random rnd = new Random();
        for(int i = 0 ; i < nbAuthor ; i++) {
            int randomConferenceId = rnd.nextInt(nbConference);
            bw.write("<http://inria.fr/Author" + i + "> <http://inria.fr/participatesIn> <http://inria.fr/Conference" + randomConferenceId + "> .\n");
            totalNumberOfTriples++;
        }
    }

    private static void generateLinksPaperWrittenByAuthor(int nbPaper, int nbAuthor, BufferedWriter bw) throws IOException {
        log.info("generating links Paper --writtenBy--> Author for " + nbPaper + " Paper and " + nbAuthor + " Author");

        Random rnd = new Random();
        for(int i = 0 ; i < nbPaper ; i++) {
            int randomNumberOfAuthors = rnd.nextInt(10);
            if(randomNumberOfAuthors == 0) {
                randomNumberOfAuthors = 1;
            }

            for(int j = 0 ; j < randomNumberOfAuthors ; j++) {
                int randomAuthorId = rnd.nextInt(nbAuthor);
                bw.write("<http://inria.fr/Paper" + i + "> <http://inria.fr/writtenBy> <http://inria.fr/Author" + randomAuthorId + "> .\n");
                totalNumberOfTriples++;
            }
        }
    }

    private static void generateLinksAuthorHasWrittenPaper(int nbAuthor, int nbPaper, BufferedWriter bw) throws IOException {
        log.info("generating links Author --hasWritten--> Paper for " + nbAuthor + " Author and " + nbPaper + " Paper");

        Random rnd = new Random();
        for(int i = 0 ; i < nbAuthor ; i++) {
            int randomNumberOfPaper = rnd.nextInt(5); // at most a Author can write 5 papers
            if(randomNumberOfPaper == 0) {
                randomNumberOfPaper = 1;
            }

            for(int j = 0 ; j < randomNumberOfPaper ; j++) {
                int randomPaperId = rnd.nextInt(nbPaper);
                bw.write("<http://inria.fr/Author" + i + "> <http://inria.fr/hasWritten> <http://inria.fr/Paper" + randomPaperId + "> .\n");
                totalNumberOfTriples++;
            }
        }
    }

    private static void generateLinksConferenceHeldInLab(int nbConference, int nbLab, BufferedWriter bw) throws IOException {
        log.info("generating links Conference --heldIn--> Lab for " + nbConference + " Conference and " + nbLab + " Lab");

        Random rnd = new Random();
        for(int i = 0 ; i < nbConference ; i++) {
            int randomLabId = rnd.nextInt(nbLab);
            bw.write("<http://inria.fr/Conference" + i + "> <http://inria.fr/heldIn> <http://inria.fr/Laboratory" + randomLabId + "> .\n");
            totalNumberOfTriples++;
        }
    }

    private static void generateLinksLabEmploysAuthor(int nbLab, int nbAuthor, BufferedWriter bw) throws IOException {
        log.info("generating links Laboratory --employs--> Author for " + nbLab + " Laboratory and " + nbAuthor + " Author");

        Random rnd = new Random();
        for(int i = 0 ; i < nbLab ; i++) {
            int randomNumberOfAuthor = rnd.nextInt(nbLab);

            for(int j = 0 ; j < randomNumberOfAuthor ; j++) {
                int randomAuthorId = rnd.nextInt(nbAuthor);
                bw.write("<http://inria.fr/Laboratory" + i + "> <http://inria.fr/employs> <http://inria.fr/Author" + randomAuthorId + "> .\n");
                totalNumberOfTriples++;
            }
        }
    }

    private static void generateLoopsPaperAuthor(int nbPaper, int nbAuthor, BufferedWriter bw) throws IOException {
        log.info("generating Paper/Author loops for " + nbAuthor + " Author and " + nbPaper + " Paper");
        generateLinksAuthorHasWrittenPaper(nbAuthor, nbPaper, bw);
        generateLinksPaperWrittenByAuthor(nbPaper, nbAuthor, bw);
    }

    private static void generateLoopsPaperConference(int nbPaper, int nbConference, BufferedWriter bw) throws IOException {
        log.info("generating Paper/Conference loops for " + nbPaper + " Paper and " + nbConference + " Conference");
        generateLinksConferencePublishesPaper(nbConference, nbPaper, bw);
        generateLinksPaperPublishedInConference(nbPaper, nbConference, bw);
    }

    private static void generateLoopsAuthorConference(int nbAuthor, int nbConference, BufferedWriter bw) throws IOException {
        log.info("generating Author/Conference loops for " + nbAuthor + " Author and " + nbConference + " Conference");
        generateLinksConferenceInvitesAuthor(nbConference, nbAuthor, bw);
        generateLinksAuthorParticipatesInConference(nbAuthor, nbConference, bw);
    }

    public static String generateRandomYear(String numbers) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder("20");
        for (int i = 0 ; i < 2 ; i++)
            sb.append(numbers.charAt(rnd.nextInt(numbers.length())));
        return sb.toString();
    }

    public static String generateRandomNumber(String numbers, int size) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < size ; i++)
            sb.append(numbers.charAt(rnd.nextInt(numbers.length())));
        return sb.toString();
    }

    public static String generateRandomWord(String chars, int size) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0 ; i < size ; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    public static String generateRandomGender() {
        Random rnd = new Random();
        String chars = "MF";
        return String.valueOf(chars.charAt(rnd.nextInt(chars.length())));
    }

    public static String generateRandomName(String chars) {
        String firstName = generateRandomWord(chars, 5);
        String lastName = generateRandomWord(chars, 7);
        return firstName + " " + lastName;
    }

    public static String generateRandomTitle(String chars) {
        StringBuilder title = new StringBuilder();

        for(int i = 0 ; i < 10 ; i++) {
            title.append(generateRandomWord(chars, 3)).append(" ");
        }

        return title.toString();
    }

    public static String generateRandomConference(String chars) {
        return generateRandomWord(chars, 8);
    }
}

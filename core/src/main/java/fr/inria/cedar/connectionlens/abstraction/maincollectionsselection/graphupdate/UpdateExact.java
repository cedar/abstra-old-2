package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.BoundaryNode;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateExact extends GraphUpdateMethod {
    public static final Logger log = Logger.getLogger(UpdateExact.class);
    ConnectionLens cl;
    DataSource ds;
    MainCollectionsIdentification mainCollectionsIdentification;

    protected ArrayList<Integer> intraBoundaryCollections;
    protected ArrayList<Integer> extraBoundaryCollections;

    public UpdateExact(int cstar, ConnectionLens cl, DataSource ds, MainCollectionsIdentification mainCollectionsIdentification) {
        super(cstar);
        this.cl = cl;
        this.ds = ds;
        this.mainCollectionsIdentification = mainCollectionsIdentification;
    }

    @Override
    public void update() throws SQLException, IOException {
        log.debug("UPDATE GRAPH WITH EXACT METHOD");

        // iteratively update the graph
        long startTime = System.currentTimeMillis();
        this.iterativeUpdate();
//        this.recursiveUpdate();
        log.info("Iterative update in " + (System.currentTimeMillis() - startTime) + " ms ");

        // inactivate collections having all their nodes inactivated
        startTime = System.currentTimeMillis();
        this.inactivateCollections();
        log.info("Deactivate collections in " + (System.currentTimeMillis() - startTime) + " ms ");

        // inactive collection edges whose source OR target is inactivated
        startTime = System.currentTimeMillis();
        this.inactivateCollectionEdges();
        log.info("Deactivate collection edges in " + (System.currentTimeMillis() - startTime) + " ms ");

        // compute the new (remaining) collection graph
        startTime = System.currentTimeMillis();
        this.computeNewCollectionGraph();
        log.info("Compute new collection graph in " + (System.currentTimeMillis() - startTime) + " ms ");

    }

    private void recursiveUpdate() throws SQLException {
        // set nodes that are involved in a boundary to inactive and recompute a new collection graph
        ArrayList<Integer> collectionsInCstarBoundary = CollectionGraph.getInstance().getCollectionBoundary(this.cstar).getCollectionsInBoundary();
        if(!collectionsInCstarBoundary.isEmpty()) {
            // set nodes as inactive
            String sql = "explain analyze WITH RECURSIVE " + SchemaTableNames.INACTIVE_NODES_TABLE_NAME + " AS ( " + // explain analyze does execute the query
                    "SELECT c.nodeId AS inactiveNode, array[c.nodeId] AS visitedNodes " +
                    "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c " +
                    "WHERE c.collId = ? " +
                    "UNION ALL " +
                    "SELECT c2.nodeId AS inactiveNode, sq1.visitedNodes||c2.nodeId " +
                    "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c1, " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c2, " + SchemaTableNames.INACTIVE_NODES_TABLE_NAME + " sq1 " +
                    "WHERE e.source = c1.nodeId AND e.target = c2.nodeId AND sq1.inactiveNode = c1.nodeId AND c2.collId IN (" + StringUtils.join(collectionsInCstarBoundary, ",") + ") AND c2.nodeId <> ALL (sq1.visitedNodes) " +
                    ") " +
                    "UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " SET isActive = false FROM " + SchemaTableNames.INACTIVE_NODES_TABLE_NAME + " inn WHERE nodeId = inn.inactiveNode AND isActive = true; "; // we check that we really NEED to update the value (if this is already set to false, no need to do the update)
            PreparedStatement stmt = CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(sql);
            stmt.setInt(1, this.cstar);
            log.debug(stmt.toString());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
        }
    }

    private void iterativeUpdate() throws SQLException {
        this.computeIntraExtraBoundaryCollections();
        boolean first = true;
        CollectionBoundary collectionBoundary = CollectionGraph.getInstance().getCollectionBoundary(this.cstar);
        log.debug(collectionBoundary);
        log.debug(this.intraBoundaryCollections.size() + " intra-boundary collections out of " + collectionBoundary.getCollectionsInBoundary().size() + ": " + this.intraBoundaryCollections);
        log.debug(this.extraBoundaryCollections.size() + " extra-boundary collections out of " + collectionBoundary.getCollectionsInBoundary().size() + ": " + this.extraBoundaryCollections);
        for (BoundaryNode boundaryNode : CollectionGraph.getInstance().getCollectionBoundary(this.cstar)) {
            int collId = boundaryNode.getId();
            log.info("updating collection C" + collId + ". This is an " + (this.intraBoundaryCollections.contains(collId) ? "intra-boundary" : "extra-boundary") + " collection.");
            if(this.intraBoundaryCollections.contains(collId) || first) { // first collection and intra-boundary collections are set as inactive (but not their nodes)
                // in the case of an intra-boundary collection, we can simplify say that the collection is inactive
                String sql = "UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " SET isActive = false WHERE collId = ? ";
                PreparedStatement stmt = CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(sql);
                stmt.setInt(1, collId);
                log.debug(stmt.toString());
                stmt.executeUpdate();
                first = false;
            } else {
                // extra-boundary collection have their nodes inactivated
                String sql = // "EXPLAIN ANALYZE " + // explain analyze does execute the query
                        // first part is for extra-boundary parents (their nodes are individually set to inactive)
                        // we need to specify that the parent collection (cn2) is also part of the boundary because extra-boundary nodes may have parents not in their boundary
                        //      this is specified with "AND cn2.collId IN (boundary)"
                        // moreover, as detailed in https://gitlab.inria.fr/nbarret/abstraction-work/-/issues/157, we need to deactivate a data node only if all its parents are inactive too
                        //      this is specified with ""
                        "UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " SET isActive = false FROM (" +
                            "SELECT cn1.nodeId AS inactivateNode " +
                            "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " cn1, " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " cn2 " +
                            "WHERE cn1.collId = ? " +
                                "AND cn1.nodeId = e.target " +
                                "AND cn2.nodeId = e.source " +
                                "AND cn2.isActive = false " +
                                "AND cn2.collId IN (" + StringUtils.join(this.extraBoundaryCollections, ",") + ") " +
                            "GROUP BY cn1.nodeId " +
                            "HAVING SUM(CASE WHEN cn2.isActive THEN 1 ELSE 0 END) = 0 " + // if all nodes are inactive then the sum is 0 and we have checked that all cn2 nodes are inactive
                            "UNION ALL " +
                            // second part is for intra-boundary parents (the collection is marked as inactive but not its individual nodes)
                            // we need to specify that the parent collection (cn2) is also part of the boundary because extra-boundary nodes may have parents not in their boundary
                            "SELECT e.target AS inactivateNode " +
                            "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " cn1, " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " cn2, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME +  " i " +
                            "WHERE cn1.collId = ? " +
                                    "AND cn1.nodeId = e.target " +
                                    "AND cn2.nodeId = e.source " +
                                    "AND cn2.collId = i.collId " +
                                    "AND i.isActive = false AND cn2.collId IN (" + StringUtils.join(this.intraBoundaryCollections, ",") + ") " +
                                // TODO NELLY: should we also check that all parents are inactive here?
                        ") AS sq1 WHERE inactivateNode = nodeId;";
                PreparedStatement stmt = CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(sql);
                stmt.setInt(1, collId);
                stmt.setInt(2, collId);
//                log.debug(stmt.toString());
                stmt.executeUpdate();
            }
        }
    }

    private void computeIntraExtraBoundaryCollections() {
        this.intraBoundaryCollections = new ArrayList<>();
        this.extraBoundaryCollections = new ArrayList<>();

        ArrayList<Integer> collectionsInBoundary = CollectionGraph.getInstance().getCollectionBoundary(this.cstar).getCollectionsInBoundary();
        boolean first = true;
        for(BoundaryNode boundaryNode : CollectionGraph.getInstance().getCollectionBoundary(this.cstar)) {
            int collectionId = boundaryNode.getId();
            if(first) {
                this.intraBoundaryCollections.add(collectionId);
                first = false;
            } else {
                ArrayList<CollectionEdge> incomingCollectionEdges = CollectionGraph.getInstance().getIncomingEdges(collectionId);
                if(incomingCollectionEdges.size() == 1) {
                    int sourceCollection = incomingCollectionEdges.get(0).getSource();
                    if(collectionsInBoundary.contains(sourceCollection)) {
                        if(this.extraBoundaryCollections.contains(sourceCollection)) {
                            // the only incoming edge has its source in the boundary but its source is an extra-boundary collection -> this is an extra-boundary collection
                            this.extraBoundaryCollections.add(collectionId);
                        } else {
                            // the only incoming edge has its source in the boundary and the source is not an extra-boundary collection -> this is an intra-boundary collection
                            this.intraBoundaryCollections.add(collectionId);
                        }
                    } else {
                        // the only incoming edge has not its source in the boundary -> this is an extra-boundary collection
                        this.extraBoundaryCollections.add(collectionId);
                    }
                } else {
                    // when a source collection is outside the boundary or the source is an extra-boundary, the collection becomes extra-boundary.
                    // Only collections having all their incoming edges in the boundary are intra-boundary
                    boolean isIntraBoundary = true;
                    for(CollectionEdge ce : incomingCollectionEdges) {
                        // the source is not in the boundary or the source is an extra-boundary collection
                        if(!collectionsInBoundary.contains(ce.getSource()) || this.extraBoundaryCollections.contains(ce.getSource())) {
                            isIntraBoundary = false;
                            this.extraBoundaryCollections.add(collectionId);
                            break;
                        }
                    }
                    if(isIntraBoundary) {
                        this.intraBoundaryCollections.add(collectionId);
                    }
                }
            }
        }
    }

    private void inactivateCollections() throws SQLException {
        // set a collection inactive if all its nodes are inactive too
        CollectionGraph.getWorkingInstance().getGraph().createTable(SchemaTableNames.INACTIVE_COLLECTIONS_TABLE_NAME, "(collId INT)", true, true);
        PreparedStatement stmt = CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement("" +
                "INSERT INTO " + SchemaTableNames.INACTIVE_COLLECTIONS_TABLE_NAME + " SELECT DISTINCT collId FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " GROUP BY collId HAVING SUM(CASE WHEN isActive THEN 1 ELSE 0 END) = 0; " + // 0 means no node is active in the collection
                "UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " SET isActive = false FROM " + SchemaTableNames.INACTIVE_COLLECTIONS_TABLE_NAME + " ic WHERE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + ".collId = ic.collId;");
        log.debug(stmt.toString());
        stmt.execute();
    }

    private void inactivateCollectionEdges() throws SQLException {
        // set a collection edge inactive if its source OR its target are inactive
        String sql = "UPDATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " " +
                "SET isActive = false " +
                "FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " cs, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " ct " +
                "WHERE collIdSource = cs.collId AND collIdTarget = ct.collId AND (cs.isActive = false OR ct.isActive = false);";
        PreparedStatement stmt = CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(sql);// TODO NELLY: how to get rid of this OR condition?
        log.debug(stmt.toString());
        stmt.execute();
    }

    private void computeNewCollectionGraph() throws SQLException {
        CollectionGraph.getWorkingInstance().computeCollectionsSizes(); // we need to update them to obtain accurate etf
        CollectionGraph.getWorkingInstance().retrieveCollectionGraphFromDisk();
        CollectionGraph.getWorkingInstance().computeEdgeTransferFactorsOnDisk();
        CollectionGraph.getWorkingInstance().computeAtMostOneOnDisk();
        CollectionGraph.getWorkingInstance().initializeAndGetVariablesInMemory(false); // the param says we don't init again the path enumeration HashMap (we don't want to do it again)
        CollectionGraph.getWorkingInstance().computeLeafCollections();
        CollectionGraph.getWorkingInstance().getAndSetAtMostOneInMemory(); // set at-most-one on the collection graph edges
        CollectionGraph.getWorkingInstance().retrieveEdgeTransferFactorsInMemory();
        CollectionGraph.getWorkingInstance().computeAggregatedPathTransferFactors(true); // compute path transfer factor between node pairs in a collection graph
    }

}

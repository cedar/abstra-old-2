/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
public class HTMLDataSource extends OrderedTreeDataSource {

	/**
	 * get the HTML policies
	 **/
	private final HTMLPolicies htmlPolicies = HTMLPolicies.getInstance();

	public HTMLDataSource(ItemID.Factory f, int identifier, URI uri, URI origURI, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.HTML, graphStats, extractStats, graph);
	}

	public String getContext(Graph g, Node n) {
		StringBuffer sb = new StringBuffer();
		Collection<Edge> incoming = g.getKIncomingEdgesPostLoading(n, Graph.maxEdgesReadForContext);
		for (Edge e: incoming) {
			Node parent = e.getSourceNode();
			if (n.getType() == Node.Types.HTML_VALUE) { // literal, or attribute value
				if (e.getLabel().length() == 0) { // literal
					sb.append("Text content <b>" + n.getLabel() + "</b>" + 
							" of an HTML element labeled <b>" + parent.getLabel()+ "</b>"); 
				}
				else { // attribute value
					sb.append("Value <b>" + n.getLabel() + "</b> of attribute <b>" + 
							e.getLabel()  + "</b> of XML element labeled <b>" + parent.getLabel()+
							"</b>"); 
				}
			}
			if (n.getType() == Node.Types.HTML_NODE) { // element
				sb.append("HTML element labeled <b>" + n.getLabel() + "</b>"); 
			}
			break; // this value node may come from different
			// contexts. We just show the first.
		}
		sb.append(" from HTML doc: <i>" + n.getDataSource().getLocalURI()+ "</i>");
		return new String(sb); 
	}


	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Scanner scanner = new Scanner(localURI.toURL().openStream())) {
			while (scanner.hasNextLine()) {
				contentBuilder.append(scanner.nextLine()).append('\n');
			}
		} catch (IOException e) {
			log.info("Error while loading " + this.originalURI);
			throw new IllegalStateException(e);
		}
		String htmlString = contentBuilder.toString();
		Set<Edge> edges = new LinkedHashSet<>();
		Node top = null;
		Document doc = policiesIgnoredAndRemoved(Jsoup.parse(htmlString, "UTF-8"));

		org.jsoup.nodes.Node firstNode = doc.childNodes().get(0);

		if (firstNode instanceof DocumentType) {
			firstNode.remove();
			firstNode = doc.childNodes().get(0);
		}
		top = traverseHTMLDoc(firstNode, "1", "", edges, processor);
		Edge edge = buildLabelledEdge(root, top, Edge.ROOT_LABEL); 
		processor.accept(edge);
		processEdgesToLocalAndOriginalURIs(processor);
	}

	/**
	 * traverse an HTML document
	 *
	 * @param htmlJsoupN
	 * @param dew
	 * @param path
	 * @param edges
	 * @param processor
	 * @return HTML node
	 */
	private Node traverseHTMLDoc(org.jsoup.nodes.Node htmlJsoupN, String dew, String path, Set<Edge> edges,
			Consumer<Edge> processor) {
		int currDew = 1;
		List<org.jsoup.nodes.Node> childsOfP = htmlJsoupN.childNodes();
		Node htmlNode =  buildLabelNodeOfType(htmlJsoupN.nodeName(),
				Node.Types.HTML_NODE);
		if (htmlJsoupN.attributes().size() > 0) {
			traverseHTMLAttributes(htmlNode, htmlJsoupN, dew, path, edges, processor);
		}

		for (org.jsoup.nodes.Node jsoupNode : childsOfP) {
			Node child = null;
			String currDews = makeDewElement(dew, currDew);
			// The node is only <>text<>
			if (jsoupNode instanceof TextNode) {
				String text = ((TextNode) jsoupNode).text().trim();
				if (!isNullOrEmpty(text)) {
					for (String sentence : getSentences(text)) {
						String textDews = dew + "#" + currDew;
						String textPath = makePathElement(path, "#" + currDew);
						if (!sentence.isEmpty() && sentence.length() > 3) {
							if (sentence.startsWith("file:")) {
								child = createOrFindValueNodeWithAtomicity(sentence,
										makeLocalKey(textDews, textPath, sentence), this, Node.Types.RDF_URI);
							} else {
								child = createOrFindValueNodeWithAtomicity(sentence,
										makeLocalKey(textDews, textPath, sentence), this, Node.Types.HTML_VALUE);
							}
							Edge edge = buildLabelledEdge(htmlNode, child, "");
							edge.setDataSource(this);
							processor.accept(edge);
							edges.add(edge);
							currDew++;
						}
					}
				}

			} else {
				String currPath = makePathElement(path, jsoupNode.nodeName());
				child = traverseHTMLDoc(jsoupNode, currDews, currPath, edges, processor);
				Edge edge = buildLabelledEdge(htmlNode, child, ""); 
				processor.accept(edge);
				edges.add(edge);
				currDew++;
			}
		}
		return htmlNode;
	}

	/**
	 * create edge attribute.
	 *
	 * @param parent
	 * @param attributesNode
	 * @param dew
	 * @param path
	 * @param edges
	 * @param processor
	 */
	private void traverseHTMLAttributes(Node parent, org.jsoup.nodes.Node attributesNode, String dew, String path,
			Set<Edge> edges, Consumer<Edge> processor) {
		int currDew = 1;

		/**
		 * get all HTML policies containing the node name as condition in the param.
		 */

		List<String> attributesPolicies = htmlPolicies.getAttributesPolicies().stream()
				.filter(n -> n.startsWith(attributesNode.nodeName() + "@")).collect(Collectors.toList());
		if (!attributesPolicies.isEmpty()) {
			for (String attrPolicy : attributesPolicies) {
				String attNodeName = "";
				String valAttr = "";
				if (attributesNode.attributes().size() >= 2) {
					String attr1 = attrPolicy.split("->")[0].split("@")[1];
					String attr2 = attrPolicy.split("->")[1];
					List<Attribute> attributeList1 = filterAttributeName(attributesNode, attr1); // get First attribute
					List<Attribute> attributeList2 = filterAttributeName(attributesNode, attr2); // get Second attribute
					if (!attributeList1.isEmpty() && !attributeList2.isEmpty()) {
						attNodeName = attributeList1.get(0).getValue(); // edge = value(attr1)
						valAttr = attributeList2.get(0).getValue(); // target node = value(attr2)
					}
				} else {
					List<Attribute> attributeList1 = filterAttributeName(attributesNode, attrPolicy.split("@")[1]);
					attNodeName = attrPolicy.split("@")[1];
					if (attributeList1.size() > 0) { // IM, 31/1/2021: added test
						// following failure to load Yamen's HTML
						valAttr = attributeList1.get(0).getValue();
					}
				}
				if (!isNullOrEmpty(attNodeName) && !isNullOrEmpty(valAttr)) {
					String currDews = dew + "@" + currDew;
					String currPath = makePathElement(path, "@" + attNodeName);
					// the attribute node has internal structure. We create it using buildNode.
					currDew++;
					for (String sentence : getSentences(valAttr)) {
						currDews = dew + "@" + currDew;
						currPath = makePathElement(path, "@" + sentence);
						// this value node may or may not exist 
						Node child = createOrFindValueNodeWithAtomicity(sentence,
								makeLocalKey(currDews, currPath, sentence), this, Node.Types.HTML_VALUE);
						Edge edgeAttr = buildLabelledEdge(parent, child, attNodeName); 
						processor.accept(edgeAttr);
						edges.add(edgeAttr);
						currDew++;
					}
					// }
				}

			}
		}
	}

	/**
	 * filter attribute matching attribute key.
	 *
	 * @param attributesNode
	 * @param attributeKey
	 * @return List of attribute
	 */
	private List<Attribute> filterAttributeName(org.jsoup.nodes.Node attributesNode, String attributeKey) {
		return attributesNode.attributes().asList().stream().filter(attr -> attr.getKey().equals(attributeKey))
				.collect(Collectors.<Attribute>toList());
	}

	@Override
	public void postprocess(Graph graph) {
		// noop
	}

	/**
	 * remove all ignored tags without text between Ex:
	 * <p>
	 * blabla
	 * </p>
	 * <sup>text1 text2</sup> blabla> -> blabla text1,text2 blabla
	 *
	 * @param document
	 * @return
	 */
	private Document policiesIgnoredAndRemoved(Document document) {
		for (String ignoredTags : htmlPolicies.getNotTraversedNodeOnly()) { // :no_node.
			for (Element elementToBeIgnored : document.select(ignoredTags)) {
				Element parent = elementToBeIgnored.parent();
				if (elementToBeIgnored.children().size() > 0) {
					elementToBeIgnored.remove();
					parent.insertChildren(0, elementToBeIgnored.children());
				} else if (!elementToBeIgnored.text().isEmpty()) {
					TextNode textNode = new TextNode(elementToBeIgnored.text());
					elementToBeIgnored.replaceWith(textNode);
				}
			}
		}

		for (String notTraversedNodes : htmlPolicies.getIgnoredNodes()) { // ignore:
			for (Element elementToBeRemoved : document.select(notTraversedNodes)) {
				Element parent = elementToBeRemoved.parent();
				elementToBeRemoved.remove();
				// parent.insertChildren(0, elementToBeRemoved.children());
			}
		}
		return document;
	}

}

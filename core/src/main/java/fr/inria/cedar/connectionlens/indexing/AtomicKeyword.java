/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.indexing;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This class models the keywords that are "worth indexing", that is: not stop words,
 * not punctuation, after stemming...
 * 
 * @author ioanamanolescu
 *
 */
public class AtomicKeyword implements Comparable<QueryComponent>, QueryComponent, Serializable {

	/** The keyword's inner value. */
	public final String value;
	
	/**
	 * Instantiates a new atomic keyword.
	 *
	 * @param s the s
	 */
	public AtomicKeyword (String s) {
		requireNonNull(s);
		this.value = s; 
	}
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.QueryComponent#value()
	 */
	@Override
	public String value() {
		return value;
	}
	
	/**
	 * @return this atomic keyword as a singleton collection.
	 */
	public Collection<AtomicKeyword> values() {
		return Collections.singleton(this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value;
	}

	/**
	 * @param kw some keyword
	 * 
	 * @return a collection whose single item is a keyword instance for the given input String.
	 */
	public static Collection<AtomicKeyword> singleton(String kw) {
		return ImmutableList.of(new AtomicKeyword(kw));
	}

	/**
	 * @param kwds an array of keywords
	 * 
	 * @return a set of atomic keywords
	 */
	public static Set<AtomicKeyword> asSet(String... kwds) {
		return Sets.newHashSet(asList(kwds));
	}

	/**
	 * @param kwds an array of keywords
	 * 
	 * @return a list of keywords
	 */
	public static List<AtomicKeyword> asList(String... kwds) {
		List<AtomicKeyword> result = new LinkedList<>();
		for (String v: kwds) {
			result.add(new AtomicKeyword(v));
		}
		return result;
	}
	
	/**
	 * @return true iff this object and o have the same type and share the same value.
	 *  
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		return AtomicKeyword.class.isInstance(o)
				&& value.equals(((AtomicKeyword) o).value);
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(value);
	}
	
	/**
	 * @param l a list of strings
	 * @return a collection of atomic keywords, corresponding to the input list of strings.
	 */
	public static Collection<AtomicKeyword> stringToCollection(List<String> l) {
		Collection<AtomicKeyword> res = new LinkedHashSet<>();
		for(String s: l) {
			res.add(new AtomicKeyword(s));
		}
		return res;
	}

	/**
	 * Compares atomic keywords according to their value's alphanumeric order.
	 * 
	 * {@inheritDoc}
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QueryComponent o) {
		return this.value.compareTo(o.value());
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * TODO: The Class IndexedMap.
 *
 * @param <V> the value type
 */
public class IndexedMap<V> implements BiMap<Integer, V> {

	/** The array (Int -> V). */
	private final BucketList<V> array;
	
	/** The inverse map (V -> Int). */
	// TODO: IndexedMap will only be complete when inverse is a Map rather than bimap
	private final Map<V, Integer> inverse;
	
	/**
	 *
	 * @param <E> the element type
	 * @return the indexed map
	 */
	public static <E> IndexedMap<E> create() {
		return new IndexedMap<>();
	}
	
	/**
	 * @param <E> the element type
	 * @param initialCapacity the initial capacity
	 * @return the indexed map
	 */
	public static <E> IndexedMap<E> create(int initialCapacity) {
		return new IndexedMap<>(initialCapacity);
	}
	
	/**
	 * Instantiates a new indexed map.
	 */
	private IndexedMap() {
		this.array = new BucketList<>();
		this.inverse = new LinkedHashMap<>();
	}
	
	/**
	 * Instantiates a new indexed map.
	 *
	 * @param initialCapacity the initial capacity
	 */
	private IndexedMap(int initialCapacity) {
		this.array = new BucketList<>(initialCapacity);
		this.inverse = new LinkedHashMap<>(initialCapacity);
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.util.Map#size()
	 */
	@Override
	public int size() {
		return array.size();
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		assert array.isEmpty() == inverse.isEmpty();
		return array.isEmpty();
	}

	/**
	 * @param o the o
	 * @return the int value of o, of null it is not an Integer
	 */
	private int intKey(Object o) {
		if (o instanceof Integer) {
			return ((Integer) o).intValue();
		}
		return -1;
	}

	/**
	 * TODO: Within range.
	 *
	 * @param i the i
	 * @return true, if successful
	 */
	private boolean withinRange(int i) {
		return i < array.size() && i >= 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		int k = intKey(key);
		return withinRange(k) && array.get(k) != null;
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		return inverse.containsKey(value);
	}

	/**
	 * TODO: Index of.
	 *
	 * @param key the key
	 * @return the integer
	 */
	public Integer indexOf(Object key) {
		return inverse.get(key);
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@Override
	public V get(Object key) {
		int k = intKey(key);
		return withinRange(k) ? array.get(k) : null;
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.BiMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(Integer key, V value) {
		checkArgument(!containsValue(value), "Value " + value + " already in index");
		checkArgument(!containsValue(key), "Key " + key + " already in assigned");
		return forcePut(key, value);
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.BiMap#forcePut(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V forcePut(Integer key, V value) {
		int k = intKey(key);
		V v;
		if (!withinRange(k)) {
//			array.ensureCapacity(k + 1);
			array.set(k, value);
			v = null;
		} else if ((v = array.get(k)) == null) {
			array.add(value);
		} else throw new IllegalStateException("Inconsistent index: value '" + array.get(k) 
				+ "' already set at index '" + k + "'. Cannot add " + value);
		this.inverse.put(value, key);
		return v;
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public V remove(Object key) {
		if (containsKey(key)) {
			int k = intKey(key);
			V val = array.get(k);
			array.set(k, null);
			inverse.remove(val);
			return val;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.BiMap#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends Integer, ? extends V> m) {
		requireNonNull(m);
		for (Map.Entry<? extends Integer, ? extends V> e: m.entrySet()) {
			put(e.getKey(), e.getValue());
		}
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		array.clear();
		inverse.clear();
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#keySet()
	 */
	@Override
	public Set<Integer> keySet() {
		return ImmutableSet.copyOf(IntStream.range(0, array.size()).iterator());
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.BiMap#values()
	 */
	@Override
	public Set<V> values() {
 		return inverse.keySet();
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<Map.Entry<Integer, V>> entrySet() {
		return ImmutableSet.copyOf(Iterables.transform(inverse.entrySet(), e->new ReversedEntry<>(e)));
	}
	
	private static class ReversedEntry<V, K> implements Map.Entry<V, K> {
		final Map.Entry<K, V> entry;
		public ReversedEntry(Map.Entry<K, V> e) {
			entry = e;
		}

		@Override
		public V getKey() {
			return entry.getValue();
		}

		@Override
		public K getValue() {
			return entry.getKey();
		}

		@Override
		public K setValue(K value) {
			throw new IllegalStateException("Attempted to modifed an immutable entry.");
		}
		
		@Override
		public String toString() {
			return entry.getValue() + "->" + entry.getKey();
		}
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.BiMap#inverse()
	 */
	@Override
	public BiMap<V, Integer> inverse() {
		return new BiMap<V, Integer>() {
			@Override
			public int size() {
				return IndexedMap.this.size();
			}

			@Override
			public boolean isEmpty() {
				return IndexedMap.this.isEmpty();
			}

			@Override
			public boolean containsKey(Object key) {
				int k = intKey(key);
				if (k < 0) {
					return false;
				}
				return IndexedMap.this.array.get(k) != null;
			}

			@Override
			public boolean containsValue(Object value) {
				return IndexedMap.this.inverse.containsKey(value);
			}

			@Override
			public Integer get(Object key) {
				return IndexedMap.this.inverse.get(key);
			}

			@Override
			public Integer remove(Object key) {
				return IndexedMap.this.inverse.remove(key);
			}

			@Override
			public void clear() {
				IndexedMap.this.clear();
			}

			@Override
			public Set<V> keySet() {
				return IndexedMap.this.inverse.keySet();
			}

			@Override
			public Set<java.util.Map.Entry<V, Integer>> entrySet() {
				return IndexedMap.this.inverse.entrySet();
			}

			@Override
			public Integer put(V key, Integer value) {
				Integer result = IndexedMap.this.inverse.get(value);
				IndexedMap.this.put(value, key);
				return result;
			}

			@Override
			public Integer forcePut(V key, Integer value) {
				Integer result = IndexedMap.this.inverse.get(value);
				IndexedMap.this.forcePut(value, key);
				return result;
			}

			@Override
			public void putAll(Map<? extends V, ? extends Integer> m) {
				requireNonNull(m);
				for (Map.Entry<? extends V, ? extends Integer> e: m.entrySet()) {
					put(e.getKey(), e.getValue());
				}
			}

			@Override
			public Set<Integer> values() {
				return IndexedMap.this.keySet();
			}

			@Override
			public BiMap<Integer, V> inverse() {
				return IndexedMap.this;
			}
		};
	}
}

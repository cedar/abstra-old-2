/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;

import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class NumberPairProcessor extends EqualityProcessor {
	
	public NumberPairProcessor(StatisticsCollector stats, double th) {
		super(stats, th);
	}

	@Override
	public Double apply(String a, String b) {
		try {
			return Double.parseDouble(a) == Double.parseDouble(b) ? 1.0 : 0.0;
		} catch (NumberFormatException e) {
			log.warn(e.getMessage());
			return 0.0;
		}
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	@Override
	public NodePairSelector selector() {
		return super.selector().and(isNumeric(BOTH)); 
	}
}

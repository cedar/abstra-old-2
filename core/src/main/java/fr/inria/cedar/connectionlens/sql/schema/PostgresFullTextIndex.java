/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql.schema;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.*;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.sql.RelationalStructure;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.StatisticsKeys;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;

import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;

/**
 * Relational implementation of the Index interface.
 */
public class PostgresFullTextIndex extends RelationalStructure implements IndexAndProcessNodes {

	/** The logger. */
	private static final Logger log = Logger.getLogger(PostgresFullTextIndex.class);

	/** The node resolver. */
	private final Function<NodeID, Node> nodeResolver;

	/** The edge resolver. */
	private final Function<EdgeID, Edge> edgeResolver;

	/** The id ID factory */
	protected final Factory factory;

	/** The id ID factory */
	protected DataSourceCatalog catalog;

	/** The locale */
	protected final String lang;

	protected final EntityExtractor extractor;
	protected final MorphoSyntacticAnalyser mas;
	protected final StatisticsCollector indexingStats;
	protected final StatisticsCollector extractorStats;
	protected final StatisticsCollector abstractStats;

	/** Whether the index is for an original/abstract/normalized graph */
	private final Graph.GraphType typeOfGraph;

	/** The name of the tables of the database where nodes and edges are stored */
	private final String nodesTableName;
	private final String edgesTableName;

	private IndexAndProcessNodes abstractIndex;
	private IndexAndProcessNodes normalizedIndex;

	
	// IM, 18/3/2021: added this to push down some limits on how many nodes we admit
	int maxMatchesPerKwd = -1;  

	/**
	 * Instantiates a new relational index.
	 * 
	 * @param c          the data source catalog
	 * @param indexLater
	 */
	public PostgresFullTextIndex(Locale l, Factory f, DataSourceCatalog c, EntityExtractor ex,
								 MorphoSyntacticAnalyser mas, Function<NodeID, Node> nr, Function<EdgeID, Edge> er,
								 Function<Node, Set<Edge>> nbr, StatisticsCollector indexingStats, StatisticsCollector extractorStats, StatisticsCollector abstractStats,
								 Boolean indexLater, Graph.GraphType typeOfGraph) {
		this.catalog = c;
		this.nodeResolver = nr;
		this.edgeResolver = er;
		this.factory = f;
		this.lang = l.getLanguage().equalsIgnoreCase("fr") ? "french" : "english";
		this.mas = mas;
		this.extractor = ex;
		this.indexingStats = indexingStats;
		this.extractorStats = extractorStats;
		this.abstractStats = abstractStats;
		this.typeOfGraph = typeOfGraph;
		this.abstractIndex = null;
		if (this.isAbstract()) {
			this.nodesTableName = SchemaTableNames.ABSTRACT_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.ABSTRACT_EDGES_TABLE_NAME;
		} else if(this.isNormalized()) {
			this.nodesTableName = SchemaTableNames.NORMALIZED_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME;
		} else {
			this.nodesTableName = SchemaTableNames.ORIGINAL_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME;
		}

		try{
			maxMatchesPerKwd = Config.getInstance().getIntProperty("max_matches_per_kwd");
		}
		catch(Exception e) {
			throw new IllegalStateException("Could not set max_matches_per_kwd! " + e);
		}
	}

	/**
	 * Instantiates a new relational index.
	 *
	 * @param c the data source catalog
	 */
	public PostgresFullTextIndex(Locale l, Factory f, DataSourceCatalog c, EntityExtractor ex,
			MorphoSyntacticAnalyser mas, Function<NodeID, Node> nr, Function<EdgeID, Edge> er,
			Function<Node, Set<Edge>> nbr, StatisticsCollector indexingStats, StatisticsCollector extractorStats, StatisticsCollector abstractStats,
			Boolean indexLater) {
		this(l, f, c, ex, mas, nr, er, nbr, indexingStats, extractorStats, abstractStats, indexLater, Graph.GraphType.ORIGINAL_GRAPH);
	}

	/**
	 * Instantiates a new relational index.
	 *
	 * @param c the data source catalog
	 * 
	 */
	public PostgresFullTextIndex(Locale l, Factory f, DataSourceCatalog c, EntityExtractor ex,
			MorphoSyntacticAnalyser mas, Function<NodeID, Node> nr, Function<EdgeID, Edge> er,
			Function<Node, Set<Edge>> nbr, StatisticsCollector indexingStats, StatisticsCollector extractorStats, StatisticsCollector abstractStats) {
		this(l, f, c, ex, mas, nr, er, nbr, indexingStats, extractorStats, abstractStats, false, Graph.GraphType.ORIGINAL_GRAPH);
	}

	@Override
	/** Does not create the index (the creation is moved to commit) */
	protected void create() {
		createdInPreviousRun = false; // IM 11/4/22 Not sure of the implications for the index, see #703
		// but this may not matter at all, because the field does not seem used for the index
	}

	@Override
	public void commit() {
		// IM moved the index creation at the end to speed up data loading
		indexingStats.resume(StatisticsCollector.total());
//		log.info("Commit: creating index nodes_kw_idx");
		executeUpdate("CREATE INDEX IF NOT EXISTS " + nodesTableName + "_kw_idx ON " + nodesTableName + " USING GIN (to_tsvector('"	+ lang + "', normalabel))");
		// IM 15/6/20 index is mostly not used currently (#315)
		indexingStats.tick(StatisticsCollector.total(), StatisticsKeys.INDEX_T);
	}

	@Override
	protected void drop() {
		boolean result = executeUpdate("DROP INDEX IF EXISTS " + nodesTableName + "_kw_idx", false);
			//	&& executeUpdate("DROP INDEX IF EXISTS " + edgesTableName + "_kw_idx", false);
		if (!result) {
			log.info("Indexes " + nodesTableName + "_kw_idx " // + edgesTableName + "_kw_idx"
					+ " could not be dropped properly.");
		}
		createdInPreviousRun = false;
	}
	
	@Override
	public Set<Node> getNodeMatches(QueryComponent kw, Node.Types... types) {
		//log.info("getNodeMatches for: " + kw.toString());
		if (kw instanceof NGram) {
			return getNGramNodeMatches((NGram) kw, types);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveNodeMatches((Conjunction) kw, types);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveNodeMatches((Disjunction) kw, types);
		}
		return getApproximateNodeMatches((AtomicKeyword) kw, types);
	}
	@Override
	public Set<Node> getExactNodeMatches(QueryComponent kw, Node.Types... types) {
		if (kw instanceof NGram) {
			return getNGramNodeMatches((NGram) kw, types);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveNodeMatches((Conjunction) kw, types);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveNodeMatches((Disjunction) kw, types);
		}
		return getExactNodeMatches1Component((AtomicKeyword) kw, types);
	}

	@Override
	public Set<Edge> getEdgeMatches(QueryComponent kw, Edge.Types... types) {
		if (kw instanceof NGram) {
			return getNGramEdgeMatches((NGram) kw, types);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveEdgeMatches((Conjunction) kw, types);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveEdgeMatches((Disjunction) kw, types);
		}
		return getExactEdgeMatches((AtomicKeyword) kw, types);
	}

	@Override
	public Set<Item> getItemMatches(QueryComponent kw) {
		if (kw instanceof NGram) {
			return getNGramItemMatches((NGram) kw);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveItemMatches((Conjunction) kw);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveItemMatches((Disjunction) kw);
		}
		return getExactItemMatches((AtomicKeyword) kw);
	}

	@Override
	public Set<Item> getItemMatches(DataSource ds, QueryComponent kw) {
		if (kw instanceof NGram) {
			return getNGramItemMatches(ds, (NGram) kw);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveItemMatches(ds, (Conjunction) kw);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveItemMatches(ds, (Disjunction) kw);
		}
		return getExactItemMatches(ds, (AtomicKeyword) kw);
	}

	@Override
	/** Only used in tests as of Jan 2021 */
	public Set<DataSource> getDataSourceMatches(QueryComponent kw) {
		if (kw instanceof NGram) {
			return getNGramDataSourceMatches((NGram) kw);
		}
		if (kw instanceof Conjunction) {
			return getConjunctiveDataSourceMatches((Conjunction) kw);
		}
		if (kw instanceof Disjunction) {
			return getDisjunctiveDataSourceMatches((Disjunction) kw);
		}
		return getExactDataSourceMatches((AtomicKeyword) kw);
	}

	private Set<Node> getApproximateNodeMatches(QueryComponent kw, Node.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		// IM, 3/3/2020: Changed from to_tsvector... to_tsquery to something simpler to
		// avoid index blind spots (#256)
		String query = "SELECT id, type FROM " + this.nodesTableName + " WHERE to_tsvector('" + lang +
				"',	normalabel) @@ " + " to_tsquery('" + lang + "', ?) ";
		//String query = "SELECT id,type FROM " + nodesTableName + " WHERE  normalabel LIKE '%" + normalize(kw.value())
		//		+ "%'"; // IM, 4/4/2021: brought back ts_vector, see #502
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		//log.info("Asking query: " + query);
		final Set<Node.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Node.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value()));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						NodeID nodeId = factory.parseNodeID(rs.getInt(1));
						Node.Types nodeType = Node.Types.values()[Integer.valueOf(rs.getString(2))];

						if (t.contains(nodeType)) {
							result.add(nodeResolver.apply(nodeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not a node, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		//log.info(result.size() + " approx 1 node match results for normalized " + normalize(kw.value()) + " out of " + kw.value());
		
		return result;
	}
	private Set<Node> getExactNodeMatches1Component(QueryComponent kw, Node.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + nodesTableName + " WHERE  md5(label)=md5(?) and label=?";
		
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		
		final Set<Node.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Node.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, kw.value());
			preparedStatement.setString(2, kw.value()); 
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						NodeID nodeId = factory.parseNodeID(rs.getInt(1));
						Node.Types nodeType = Node.Types.values()[Integer.valueOf(rs.getString(2))];

						if (t.contains(nodeType)) {
							result.add(nodeResolver.apply(nodeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not a node, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		//log.info(result.size() + " exact node 1 match results for " + kw.value() + ". Normalized, it would have been: " + normalize(kw.value()));
		return result;
	}

	public String getStemOfString(String kw) {
		String stem = "";
		String query = "SELECT to_tsquery('" + lang + "', ?) ";
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						stem = rs.getString(1);
					} catch (NoSuchElementException e) {
						// if the item is not a node, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return stem;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of node matches the given keyword exactly
	 */
	private Set<Node> getExactNodeMatches(DataSource ds, AtomicKeyword kw) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT id FROM " + nodesTableName + " WHERE to_tsvector('" + lang + "', ,normalabel) @@ "
				+ " to_tsquery('" + lang + "', ?) AND ds = ? ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value));
			preparedStatement.setInt(2, ds.getID());
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					result.add(nodeResolver.apply(factory.parseNodeID(rs.getInt(1))));
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		//log.info(result.size() + " result for exact node " + normalize(kw.value));
		return result;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of node matches the given keyword exactly
	 */
	private Set<Edge> getExactEdgeMatches(DataSource ds, AtomicKeyword kw) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id FROM " + edgesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) AND ds = ? ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value));
			preparedStatement.setInt(2, ds.getID());
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					result.add(edgeResolver.apply(factory.parseEdgeID(rs.getInt(1))));
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of edge matches the given keyword exactly
	 */
	private Set<Edge> getExactEdgeMatches(AtomicKeyword kw, Edge.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + edgesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		final Set<Edge.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Edge.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						EdgeID edgeId = factory.parseEdgeID(rs.getInt(1));
						Edge.Types edgeType = Edge.Types.values()[rs.getInt(2)];
						if (t.contains(edgeType)) {
							result.add(edgeResolver.apply(edgeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not an edge, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of item matches the given keyword exactly
	 */
	private Set<Item> getExactItemMatches(AtomicKeyword kw) {
		return Sets.union(getExactEdgeMatches(kw), getApproximateNodeMatches(kw));
	}

	/**
	 * @param ds some data source
	 * @param kw some atomic keyword
	 * @return the set of node matches the given keyword exactly within the given
	 *         data source
	 */
	private Set<Item> getExactItemMatches(DataSource ds, AtomicKeyword kw) {
		return Sets.union(getExactEdgeMatches(ds, kw), getExactNodeMatches(ds, kw));
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of data sources containing exact matches for the the given
	 *         keyword.
	 */
	private Set<DataSource> getExactNodeDataSourceMatches(AtomicKeyword kw) {
		Set<DataSource> result = new LinkedHashSet<>();
		String query = "SELECT ds FROM " + nodesTableName + " WHERE to_tsvector('" + lang + "', normalabel) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		// this returns data sources, not clear what limiting means
		//log.info("Asking: " + query); 
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					result.add(catalog.getEntry(rs.getInt(1)));
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of data sources containing exact matches for the the given
	 *         keyword.
	 */
	private Set<DataSource> getExactEdgeDataSourceMatches(AtomicKeyword kw) {
		Set<DataSource> result = new LinkedHashSet<>();
		String query = "SELECT ds FROM " + edgesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		// this returns data sources, not clear what limiting means

		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, normalize(kw.value));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					result.add(catalog.getEntry(rs.getInt(1)));
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kw some atomic keyword
	 * @return the set of data sources containing exact matches for the the given
	 *         keyword.
	 */
	private Set<DataSource> getExactDataSourceMatches(AtomicKeyword kw) {
		return Sets.union(getExactEdgeDataSourceMatches(kw), getExactNodeDataSourceMatches(kw));
	}

	/**
	 * @param kws some atomic keyword
	 * @return the set of nodes matching all keywords in the given conjunction.
	 */
	private Set<Node> getConjunctiveNodeMatches(Conjunction kws, Node.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + nodesTableName + " WHERE to_tsvector('" + lang + "', normalabel) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		final Set<Node.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Node.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, Joiner.on(" & ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					NodeID nodeId = factory.parseNodeID(rs.getInt(1));
					Node.Types nodeType = Node.Types.values()[rs.getInt(2)];
					if (t.contains(nodeType)) {
						result.add(nodeResolver.apply(nodeId));
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		log.info(result.size() + " results for approx conjunctive " +
				Joiner.on(" & ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
		return result;
	}

	/**
	 * @param kws some atomic keyword
	 * @return the set of edges matching all keywords in the given conjunction.
	 */
	private Set<Edge> getConjunctiveEdgeMatches(Conjunction kws, Edge.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + edgesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		final Set<Edge.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Edge.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, Joiner.on(" & ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						EdgeID edgeId = factory.parseEdgeID(rs.getInt(1));
						Edge.Types edgeType = Edge.Types.values()[rs.getInt(2)];
						if (t.contains(edgeType)) {
							result.add(edgeResolver.apply(edgeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not an edge, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kws some atomic keyword
	 * @return the set of items matching all keywords in the given conjunction.
	 */
	private Set<Item> getConjunctiveItemMatches(Conjunction kws) {
		return Sets.union(getConjunctiveEdgeMatches(kws), getConjunctiveNodeMatches(kws));
	}

	/**
	 * @param ds  some data source
	 * @param kws some atomic keyword
	 * @return the set of items matching for the given conjunction within the given
	 *         data source.
	 */
	private Set<Item> getConjunctiveItemMatches(DataSource ds, Conjunction kws) {
		return Sets.filter(getConjunctiveItemMatches(kws), i -> i.getDataSource().getID() == ds.getID());
	}

	/**
	 * @param kws some atomic keyword
	 * @return the set of data source whose items matches for the given conjunction.
	 */
	private Set<DataSource> getConjunctiveDataSourceMatches(Conjunction kws) {
		return Sets.newLinkedHashSet(Iterables.transform(getConjunctiveItemMatches(kws), Item::getDataSource));
	}

	/**
	 * Normalizes the input string, by remove punctuation signs and redundant white
	 * spaces.
	 * 
	 * @param s some string to normalized
	 * @return a normalized version of the input string
	 */
	private static String normalize(String s) {
		String result = Strings.nullToEmpty(s).trim().toLowerCase();
//		result = result.replaceAll("[^a-zA-Z0-9_\u00C0-\u017F\\-\\s]", " ");
//		result = result.replaceAll(" +", " ");
		result = result.replace(":", "\\:");
		return result.trim();
	}

	/**
	 * @param kw    some n-gram
	 * @param label some label
	 * @return true, if the given N-gram keyword matches the given label
	 */
	private static boolean match(NGram kw, String label) {
		String l = normalize(label);
		String k = normalize(kw.value());
		return l.contains(k);
	}

	/**
	 * Filters irrelevance component from the n-gram. Currently, irrelevant
	 * component are those of length 1
	 *
	 * @param kw some n-gram
	 * @return the conjunction resulting from the filtering.
	 */
	private static Conjunction filterIrrelevanceComponent(NGram kw) {
		List<AtomicKeyword> result = new LinkedList<>();
		kw.forEach(w -> {
			if (w.value().length() > 1) {
				result.add(w);
			}
		});
		return new Conjunction(result);
	}

	/**
	 * @param kws some n-gram
	 * @return the set of nodes matching the given n-gram
	 */
	private Set<Node> getNGramNodeMatches(NGram kws, Node.Types... types) {
		return Sets.filter(getConjunctiveNodeMatches(filterIrrelevanceComponent(kws), types),
				n -> match(kws, n.getLabel()));
	}

	/**
	 * @param kws some n-gram
	 * @return the set of edges matching the given n-gram
	 */
	private Set<Edge> getNGramEdgeMatches(NGram kws, Edge.Types... types) {
		return Sets.filter(getConjunctiveEdgeMatches(filterIrrelevanceComponent(kws), types),
				n -> match(kws, n.getLabel()));
	}

	/**
	 * @param kws some n-gram
	 * @return the set of items matching the given n-gram
	 */
	private Set<Item> getNGramItemMatches(NGram kws) {
		return Sets.filter(getConjunctiveItemMatches(filterIrrelevanceComponent(kws)), n -> match(kws, n.getLabel()));
	}

	/**
	 * @param ds  some data source
	 * @param kws some n-gram
	 * @return the set of items matching the given n-gram within the given data
	 *         source.
	 */
	private Set<Item> getNGramItemMatches(DataSource ds, NGram kws) {
		return Sets.filter(getConjunctiveItemMatches(ds, filterIrrelevanceComponent(kws)),
				n -> n.getDataSource().getID() == ds.getID() && match(kws, n.getLabel()));
	}

	/**
	 * @param kws some n-gram
	 * @return the set of data source with items matching the given n-gram.
	 */
	private Set<DataSource> getNGramDataSourceMatches(NGram kws) {
		return Sets.newLinkedHashSet(Iterables.transform(getNGramItemMatches(kws), Item::getDataSource));
	}

	/**
	 * @param kws some disjunction
	 * @return the set of nodes matching any keyword in the given disjunction.
	 */
	private Set<Node> getDisjunctiveNodeMatches(Disjunction kws, Node.Types... types) {
		Set<Node> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + nodesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		final Set<Node.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Node.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, Joiner.on(" | ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						NodeID nodeId = factory.parseNodeID(rs.getInt(1));
						Node.Types nodeType = Node.Types.values()[rs.getInt(2)];

						if (t.contains(nodeType)) {
							result.add(nodeResolver.apply(nodeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not a node, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		//log.info(result.size() + " results for approx disjunctive " +
		//		Joiner.on(" & ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
		return result;
	}

	/**
	 * @param kws some disjunction
	 * @return the set of edges matching any keyword in the given disjunction.
	 */
	private Set<Edge> getDisjunctiveEdgeMatches(Disjunction kws, Edge.Types... types) {
		Set<Edge> result = new LinkedHashSet<>();
		String query = "SELECT id,type FROM " + edgesTableName + " WHERE to_tsvector('" + lang + "', label) @@ "
				+ " to_tsquery('" + lang + "', ?) ";
		if (maxMatchesPerKwd > 0) {
			query = query + (" LIMIT " + maxMatchesPerKwd); 
		}
		final Set<Edge.Types> t = ImmutableSet.copyOf(types.length > 0 ? types : Edge.Types.values());
		try {
			PreparedStatement preparedStatement = getPreparedStatement(query);
			preparedStatement.setString(1, Joiner.on(" | ").join(Iterables.transform(kws, kw -> normalize(kw.value))));
			try (ResultSet rs = executeQuery(preparedStatement)) {
				while (rs.next()) {
					try {
						EdgeID edgeId = factory.parseEdgeID(rs.getInt(1));
						Edge.Types edgeType = Edge.Types.values()[rs.getInt(2)];
						if (t.contains(edgeType)) {
							result.add(edgeResolver.apply(edgeId));
						}
					} catch (NoSuchElementException e) {
						// if the item is not an edge, ignore.
					}
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * @param kws some disjunction
	 * @return the set of items matching any keyword in the given disjunction.
	 */
	private Set<Item> getDisjunctiveItemMatches(Disjunction kws) {
		Set<Item> result = null;
		for (AtomicKeyword kw : kws) {
			if (result == null) {
				result = getExactItemMatches(kw);
			} else {
				result = Sets.union(result, getExactItemMatches(kw));
			}
		}
		return result == null ? ImmutableSet.of() : result;
	}

	/**
	 * @param ds  some data source
	 * @param kws some disjunction
	 * @return the set of nodes matching any keyword in the given disjunction within
	 *         the given data source.
	 */
	private Set<Item> getDisjunctiveItemMatches(DataSource ds, Disjunction kws) {
		return Sets.filter(getDisjunctiveItemMatches(kws), i -> i.getDataSource().getID() == ds.getID());
	}

	/**
	 * @param kws some disjunction
	 * @return the set of data source with nodes matching any keyword in the given
	 *         disjunction.
	 */
	private Set<DataSource> getDisjunctiveDataSourceMatches(Disjunction kws) {
		return Sets.newLinkedHashSet(Iterables.transform(getDisjunctiveItemMatches(kws), Item::getDataSource));
	}

	@Override
	public IndexAndProcessNodesSession openSession(int batchSize, Graph g) {
		IndexAndProcessNodesSession iapns = new PostgresFullTextIndexSession(this, this.extractor, this.mas,
				this.extractorStats,  
				this.indexingStats, g);
		if (this.extractorStats == null) {
			throw new Error("Why null stats?");
		}
		if (this.indexingStats == null) {
			throw new Error("Why null stats?");
		}
		return iapns;
	}

	@Override
	public void close() {
		// commit();
		try {
			for (String key : statementMap.keySet()) {
				PreparedStatement stmt = statementMap.get(key);
				stmt.close();
				statementMap.remove(key);
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		if (abstractIndex != null) {
			abstractIndex.close();
		}
	}

	@Override
	public IndexingModels indexingModel() {
		return POSTGRES_FULLTEXT;
	}

	@Override
	public boolean isAbstract() {
		return this.typeOfGraph == Graph.GraphType.ABSTRACT_GRAPH;
	}

	@Override
	public boolean isNormalized() {
		return this.typeOfGraph == Graph.GraphType.NORMALIZED_GRAPH;
	}

	@Override
	public IndexAndProcessNodes abstractIndex() {
		return this.abstractIndex;
	}

	@Override
	public void updateAbstractIndex(IndexAndProcessNodes index) {
		this.abstractIndex = index;
	}

	@Override
	public void setCatalog(DataSourceCatalog catalog) {
		this.catalog = catalog; 
	}

}
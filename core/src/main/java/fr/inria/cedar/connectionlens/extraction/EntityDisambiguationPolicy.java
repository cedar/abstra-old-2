package fr.inria.cedar.connectionlens.extraction;

import java.util.HashSet;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;

public class EntityDisambiguationPolicy {

	private HashSet<String> personContexts;
	private HashSet<String> organizationContexts;
	private HashSet<String> locationContexts;
	static Logger log = Logger.getLogger(EntityDisambiguationPolicy.class);

	boolean empty;

	public EntityDisambiguationPolicy(String s) {
		personContexts = new HashSet<String>();
		organizationContexts = new HashSet<String>();
		locationContexts = new HashSet<String>();

		String[] rules = s.split(",");
		for (String rule : rules) {
			// log.info("RULE: " + rule);
			String[] components = rule.trim().split(" ");
			if ((components.length > 0) && (components.length != 2)) {
				if (components[0].length() > 0) { // complain only if not empty
					log.warn("Ignoring rule of length " + components.length + " (should be 2)"); // ignore if
																									// incomprehensible
				}
				continue;
			}
			boolean understood = false;
			// log.info("0: " + components[0] + " 1: " + components[1]);
			if (components[1].toLowerCase().equals("person")) {
				log.info("Disambiguate Person on: " + components[0]);
				personContexts.add(components[0]);
				understood = true;
			}
			if (components[1].toLowerCase().equals("organization")) {
				log.info("Disambiguate Organization on " + components[0]);
				organizationContexts.add(components[0]);
				understood = true;
			}
			if (components[1].toLowerCase().equals("location")) {
				log.info("Disambiguate Location on " + components[0]);
				locationContexts.add(components[0]);
				understood = true;
			}
			if (!understood) {
				log.warn("Could not understand rule " + components[0] + " " + components[1]);
			}
		}
		int k = (personContexts.size() + organizationContexts.size() + locationContexts.size());
		if (k > 0) {
			log.info(k + " disambiguation rules");
		}
		empty = (k == 0);
	}

	public boolean requestDisambiguation(DataSource ds, Node.Types t, int structuralContext) {
		if (empty) {
			return true;
		}
		// the policy knows some (path, entity type) pairs.
		// the datasource know which paths map to which structural contexts
		String structContextString = ds.getStructuralContext(structuralContext);
		if (structContextString == null) {
			return false; // if this context is not known in this data source, act as if there was no
							// policy
		}
		// log.info("Path " + structContextString + " is: " + structuralContext);
		switch (t) {
		case ENTITY_PERSON:
			return personContexts.contains(structContextString);
		case ENTITY_ORGANIZATION:
			return organizationContexts.contains(structContextString);
		case ENTITY_LOCATION:
			return locationContexts.contains(structContextString);
		default:
			return false;
		}
	}
}

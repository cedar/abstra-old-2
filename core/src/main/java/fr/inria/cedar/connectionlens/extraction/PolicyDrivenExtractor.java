package fr.inria.cedar.connectionlens.extraction;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * This class "extracts information" without looking at the input but
 * only based on the context in which the input appears.
 * The context comes from the text node.
 *
 * @author ioanamanolescu
 *
 */

public class PolicyDrivenExtractor extends TopLevelExtractor {

	ExtractionPolicy ep; 

	static HashMap<ExtractionPolicy, PolicyDrivenExtractor> pdExtractors = new HashMap<>();  

	/** To avoid creating 1 PDE per Node, we keep here 1 instance per policy and reuse it */
	public static PolicyDrivenExtractor getOrCreate(ExtractionPolicy extractPolicy) {
		if (pdExtractors.get(extractPolicy) == null) {
			PolicyDrivenExtractor pde = new PolicyDrivenExtractor(extractPolicy);
			pdExtractors.put(extractPolicy, pde); 
		}
		return pdExtractors.get(extractPolicy);
	}
	
	public PolicyDrivenExtractor(ExtractionPolicy ep) {
		log = Logger.getLogger(PolicyDrivenExtractor.class);
		this.ep = ep; 
		// recording the newly created extractor 
		pdExtractors.put(ep, this);
	}
	/** The no-extraction context means: we *do* have an extraction policy, 
	 *  and it says not to look in these nodes */
	private static int NO_EXTRACT_CONTEXT= Integer.MAX_VALUE; 
	public static int noExtractContext() {
		return NO_EXTRACT_CONTEXT;
	}
	/** No context means: there is no context associated to this node 
	 *  (no policy applies). In this case, an extractor may be called, or not, 
	 *  depending on the parameters controlling the extraction. */
	private static int NO_CONTEXT = -1; 
	public static int noContext() {
		return NO_CONTEXT; 
	}

	@Override
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		//log.info(input + " context: " + structuralContext);
		HashSet<Entity> results = new HashSet<Entity>();
		// We do not test ignorance here. Ignorance should be tested separately, and before calling run().
		//	if (ep.saysNoExtract(ds, nodeContext)) {
		//		hasMatched=true; 
		//	}

		String localInput = input;
		// we are playing "extractor", thus we need to do a minimum clean up and at least remove trailing periods.
		if (input.charAt(input.length() - 1) == '.') {
			localInput= input.substring(0, input.length() - 1); 
		}
		Entity entity = null;
		//@todo prajna
		String dsURI = ds.getLocalURI().toString();
		String sep = ep.getSeparator(ds, structuralContext);
		if (ep.saysPerson(ds, structuralContext)) {
			String[] listOfInputs;
			if (sep.equals("")) {
				listOfInputs = new String[1];
				listOfInputs[0] = localInput;
			} else {
				//log.info("Made " + input + " a person!");
				listOfInputs = localInput.split(sep);
			}
				for (String l : listOfInputs) {
					entity = new Entity(l, l, 1.0, 0, Types.ENTITY_PERSON, dsURI);
					if (entity != null) {
						results.add(entity);
						if (disambiguation) {
							disambiguateIfNeeded(ds, entity, structuralContext, input);
						}
					}
				}

		}
		if (ep.saysOrganization(ds, structuralContext)) {
			String[] listOfInputs;
			if (sep.equals("")) {
				listOfInputs = new String[1];
				listOfInputs[0] = localInput;
			} else {
				//log.info("Made " + input + " an organization!");
				listOfInputs = localInput.split(sep);
			}
				for (String l : listOfInputs) {
					entity = new Entity(l, l, 1.0, 0, Types.ENTITY_ORGANIZATION, dsURI);
					if (entity != null) {
						results.add(entity);
						if (disambiguation) {
							disambiguateIfNeeded(ds, entity, structuralContext, input);
						}
					}
				}

		}
		if (ep.saysLocation(ds, structuralContext)) {
			String[] listOfInputs;
			if (sep.equals("")) {
				listOfInputs = new String[1];
				listOfInputs[0] = localInput;
			} else {
				listOfInputs = localInput.split(sep);
			}
				for (String l : listOfInputs) {
					entity = new Entity(l, l, 1.0, 0, Types.ENTITY_LOCATION, dsURI);
					if (entity != null) {
						results.add(entity);
						if (disambiguation) {
							disambiguateIfNeeded(ds, entity, structuralContext, input);
						}
					}
				}

		}
		return results; 
	}

	@Override
	public String getExtractorName() {
		return "PolicyExtractor";
	}

	@Override
	public Locale getLocale() {
		return locale; 
	}

	

}

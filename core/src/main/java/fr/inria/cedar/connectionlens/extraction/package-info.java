/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Package containg classes and sub-packages related to morpho-syntactic, as well as entity and 
 * relationship extraction.
 */
package fr.inria.cedar.connectionlens.extraction;
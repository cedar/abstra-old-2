/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.withTypes;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.SOURCE;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.TARGET;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.wcohen.ss.Jaro;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.PersonOccurrenceNode;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
public class PersonPairProcessor extends CachedSimilarPairProcessor {

	private final Node.Types entityType;
	private final double lengthRatio;
	private static final Jaro sim = new Jaro();
	private double thresh;
	private static double threshFirst;
	private static double threshLast;
	
	/** The logger. */
	protected static final Logger log = Logger.getLogger(PersonPairProcessor.class);

	protected PersonPairProcessor(StatisticsCollector stats, double th, double th_f, double th_l) {
		this(stats, ENTITY_PERSON, th, th_f, th_l, Config.getInstance().getDoubleProperty("length_difference_ratio"));
	}

	protected PersonPairProcessor(StatisticsCollector stats, Node.Types t, double th, double th_f, double th_l,
			double lengthRatio) {
		super(stats, th);
		this.thresh = th;
		threshFirst = th_f;
		threshLast = th_l;
		this.entityType = t;
		this.lengthRatio = lengthRatio;
	}

	@Override
	public NodePairSelector selector() {
		return (withTypes(SOURCE, entityType) // source must be of this type
				.and(withTypes(TARGET, entityType)) 	
				// target is of this type 
				.and(sameStoredPrefix())); 
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] { str.length() };
	}

	public Double apply(Node n1, Node n2) {
		if (n1 instanceof PersonOccurrenceNode && n2 instanceof PersonOccurrenceNode) {
			PersonOccurrenceNode pn1 = (PersonOccurrenceNode) n1;
			PersonOccurrenceNode pn2 = (PersonOccurrenceNode) n2;
			return getPersonSimilarity(pn1, pn2);
		}
		return 0.0;
	}

	public static Double getPersonSimilarity(PersonOccurrenceNode personA, PersonOccurrenceNode personB) {
		//log.info("\nPersonPairProcessor.getPersonSimilarity");
		Double similarity = 0.0; // global similarity
		Double fn_similarity = 0.0; // firstName similarity
		Double ln_similarity = 0.0; // lastName similarity

		Double alpha = threshFirst / threshLast;
		// lastName
		//log.info("\n");
		//log.info("COMPARING " + personA.getLabel() + " and " + personB.getLabel());
		//log.info("LAST NAMES: " + personA.getLastName() + " and " + personB.getLastName());
		ln_similarity = sim.score(personA.getLastName(), personB.getLastName());
		// Compute first Names similarity
		if (ln_similarity > threshLast) {
			//log.info(ln_similarity + " above the last name threshold " + threshLast);
			if (personA.getFirstNames().size() > 0 && personB.getFirstNames().size() > 0) {
				//log.info("Comparing first names");
				fn_similarity = getFirstNamesSimilarity(personA.getFirstNames(), personB.getFirstNames(), threshFirst);
				if (fn_similarity > threshFirst) {
					//log.info(fn_similarity + " above the first name threshold " + threshFirst);
					similarity = ((1 - alpha) * ln_similarity) + (alpha * fn_similarity);
				}
				else {
					//log.info(fn_similarity + " below the threshold " + threshFirst);
					// IM work in progress, 27/08/20 TODO continue
					boolean aInitial = PersonOccurrenceNode.isInitials(concatFirstNames(personA.getFirstNames()));
					boolean bInitial = PersonOccurrenceNode.isInitials(concatFirstNames(personB.getFirstNames()));
					
					if (aInitial && !bInitial) {
						//log.info("Initials: " + personA.getFirstNames());
						if (couldBeInitialsFor(personA.getFirstNames(), personB.getFirstNames())) {
							fn_similarity = threshFirst; 
						}
					}
					if (bInitial && !aInitial) {
						//log.info("Initials: " + personB.getFirstNames());
						if (couldBeInitialsFor(personB.getFirstNames(), personA.getFirstNames())) {
							fn_similarity = threshFirst; 
						}
					}
					similarity = ((1 - alpha) * ln_similarity) + (alpha * fn_similarity);
				}
			} else if (personA.getFirstNames().size() == 0 && personB.getFirstNames().size() == 0) {
				//log.info("No first names on either side, keeping " + ln_similarity);
				similarity = ln_similarity;
			}
		}
		else {
			//log.info(ln_similarity + " below the last name threshold " + threshLast);
		}
		// IM, 5/9/20: if same last name and one has neither fn nor initials, accept
		if (ln_similarity == 1.0 && (personA.getFirstNames().size() == 0 || personB.getFirstNames().size() == 0)) {
			similarity = 1 - alpha + alpha * threshFirst; 
		}
		//log.info("PPP end with " + similarity);
		return similarity;
	}

	private static boolean couldBeInitialsFor(ArrayList<String> initials, ArrayList<String> firstNames) {
		// TODO: this needs to be refined more, it is too simplistic now.
		if (initials.size() == 1 && firstNames.size() == 1 
				&& (initials.get(0).length() > 0) && (firstNames.get(0).length() > 0)) {
			//log.info("INITIALS[0]: |" + initials.get(0) + "| FIRSTNAMES[0]: |" + firstNames.get(0) + "|"); 
			if (initials.get(0).toLowerCase().charAt(0) == firstNames.get(0).toLowerCase().charAt(0)){
				//log.info(initials + " could be initials for " + firstNames);
				return true; 
			}
		}
		return false;
	}

	private static String concatFirstNames(ArrayList<String> firstNames) {
		StringBuffer sb = new StringBuffer();
		int i = 0; 
		for (String fn: firstNames) {
			sb.append(fn.toUpperCase());
			if (i < firstNames.size() - 1) {
				sb.append(" ");
			}
			i++; 
		}
		return new String(sb);
	}

	/**
	 * This is no longer used: for pairs of People, the getPersonSimilarity method should be called instead.
	 */
	public Double apply(String a, String b) {
//		Double personSim = 0.0;
//		Double simJaro = sim.score(a, b);
//
//		if (simJaro > thresh) {
//			if (a.split(" ").length == 0 && b.split(" ").length == 0) {
//				return simJaro;
//			}
//			Person person_a = new Person(a, locale);
//			Person person_b = new Person(b, locale);
//			personSim = getPersonSimilarity(person_a, person_b);
//		}
//		return personSim;
		return 0.0;
	}


	/**
	 * get firstNames similarity
	 * 
	 * @param fnames1
	 * @param fnames2
	 * @return getFirstName similarity
	 */
	private static Double getFirstNamesSimilarity(ArrayList<String> fnames1, ArrayList<String> fnames2, Double threshold) {
		Double fn_similarity = 0.0; // firstName similarity
		if (fnames1.size() == fnames2.size()) {
			for (int k = 0; k < fnames1.size(); k++) {
				fn_similarity = sim.score(fnames1.get(k), fnames2.get(k));
			}
			return fn_similarity / fnames1.size();
		}

		else if (fnames1.size() < fnames2.size()) {
			for (String fn1 : fnames1) {
				Double maxSimilar = 0.0;
				for (String fn2 : fnames2) {
					Double labelSim = sim.score(fn1, fn2);
					// log.info("labelSim "+labelSim);
					if (labelSim > threshold && labelSim > maxSimilar) {
						maxSimilar = labelSim;
					}
				}
				fn_similarity += maxSimilar;
			}
			return fn_similarity = fn_similarity / fnames1.size();
		} else {
			for (String fn1 : fnames2) {
				Double maxSimilar = 0.0;
				for (String fn2 : fnames1) {
					Double labelSim = sim.score(fn1, fn2);
					if (labelSim > threshold && labelSim > maxSimilar) {
						maxSimilar = labelSim;
					}
				}
				fn_similarity += maxSimilar;
			}
			return fn_similarity = fn_similarity / fnames2.size();
		}
	}

}

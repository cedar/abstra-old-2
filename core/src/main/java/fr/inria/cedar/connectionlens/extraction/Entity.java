/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import static com.google.common.base.Strings.nullToEmpty;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import java.util.Comparator;
import java.util.Objects;
import org.json.JSONObject;

/**
 * An Entity occurrence
 */
public class Entity  implements Token {
	
	/** The exact term as detected from the text */
	private String value; 

	/** The common name (e.g. Macron is commonly known as Emmanuel Macron */
	private String commonName; 

	/** Confidence score (may browse over different knowledge base to compute it) */
	private double confidence;

	/** The start offset of the word */
	private long offset;

	/** The length of the word */
	private int length; 

	/** The ID of the text document in which this occurrence is found */
	private String docID;

	/** Embed the type of Entity directly into an occurrence */
	private Types type;




	/** Ambiverse ID extracted*/
	private String ambiID;


	public Entity (String value, String commonName, double confidence, long offset, Types type, String docID, String ambiID) {
		this.value = value;
		this.commonName = commonName;
		this.confidence = confidence;
		this.offset = offset;
		this.length = value.length();
		this.docID = docID;
		this.type = type;
		this.ambiID = ambiID;

		normalizeValue();
	}

	/**
	 * Create a new entity occurrence (mostly for test purpose because this constructor 
	 * requires the path of the text source, which should NOT be assigned manually)
	 * @param value the entity occurrence found in text
	 * @param commonName its common name (if available & detected by the entity extractor)
	 * @param confidence the confidence value
	 * @param offset the position of the entity in the text
	 * @param type the type of the entity (PERSON|ORGANIZATION|LOCATION)
	 * @param docID the path to the filename
	 */
	public Entity(String value, String commonName, double confidence, long offset, Types type, String docID) {
		this.value = value;
		this.commonName = commonName;
		this.confidence = confidence;
		this.offset = offset;
		this.length = value.length();
		this.docID = docID;
		this.type = type;
		this.ambiID="";
		normalizeValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.extraction.Token#value()
	 */
	@Override
	public String value() { return this.value; }
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.extraction.Token#offset()
	 */
	@Override
	public long offset() { return this.offset; }
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.extraction.Token#length()
	 */
	@Override
	public int length() { return this.length; }
	
	/**
	 * @return the common name of the entity
	 */
	public String commonName() { return this.commonName; }
	
	/**
	 * @return the confidence granted by the extractor to this entity.
	 */
	public double confidence() { return this.confidence; }
	
	/**
	 * @return the ID of the document this entity was extracted from.
	 */
	public String docID() { return this.docID; }
	
	/**
	 * @return the type of the entity.
	 */
	public Types type() { return this.type; }

	public String getAmbiID() {
		return ambiID;
	}

	public void setAmbiID (String ambiID) {
		this.ambiID = ambiID;
	}


	/**
	 * Build a JSON representing an occurrence
	 * @return the JSON representation of this entity
	 */
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		
		jso.put("type", type);
		jso.put("value", value);
		jso.put("commonName", commonName);
		jso.put("confidence", confidence);
		jso.put("offset", offset);
		jso.put("length", length);
		jso.put("docID", docID);
	
		return jso;
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "Entity " + type + ": '" + value 
				+ "', common name = " + commonName + ", confidence = " + confidence 
				+ ", offset = " + offset + ", length = " + length +  ", ambID = " + ambiID +", found in " + docID + "\n";
		return s;
	}
	
	/**
	 * Normalize value.
	 */
	private void normalizeValue() {
		// remove special characters (such as \n)
		value = value.replaceAll("\\n", "");
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		// simple idea: two occurrences are equal if they have the same value AND the same offset
		// same offset not yet implemented, it is hard to manage offset in annotated text
		if (!(o instanceof Entity)) {
			return false;
		} 
		if (o == this) {
			return true;
		}
		Entity occ = (Entity) o;
		return occ.type == this.type && occ.value.equalsIgnoreCase(this.value);
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(nullToEmpty(this.value).toLowerCase());
	}
	
	/**
	 * A comparator used to sort Entity wrt its offset.
	 * 
	 * @author Mihu
	 */
	public static class EntityComparator implements Comparator<Entity> {
		
		/**
		 * {@inheritDoc}
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Entity o1, Entity o2) {
			if (o1.offset() < o2.offset()) {
				return -1;
			}
			if (o1.offset() == o2.offset()) {
				return 0;
			}
			return 1;
		}
		
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing.lucene;

import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.AbstractProcessNodesSession;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;

import java.io.IOException;

import static org.apache.lucene.document.TextField.TYPE_STORED;

public class LuceneBasedIndexSession extends AbstractProcessNodesSession {

	/** The logger */
	private static final Logger logger = Logger.getLogger(LuceneBasedIndexSession.class);

    private final IndexWriter iwriter;
    private final LuceneBasedIndex parent;

	public LuceneBasedIndexSession(LuceneBasedIndex parent, Directory directory, Analyzer analyzer,
								   EntityExtractor ex, MorphoSyntacticAnalyser mas,
								   StatisticsCollector extractorStats, StatisticsCollector indexStats, Graph g) throws IOException {
		super(ex, mas, extractorStats, indexStats, g);
		this.parent = parent;
	    this.iwriter = new IndexWriter(directory, new IndexWriterConfig(analyzer));
	}
	
	@Override
	public void commit() {
		super.commit();
		try {
			this.iwriter.close();
			parent.update();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void addEdge(Edge e) {
		if (!this.edgeBuffer.contains(e) && e.getType() == Edge.Types.EDGE) {
		    try {
			    Document doc = new Document();
			    doc.add(new Field("is_edge", String.valueOf(true), TYPE_STORED));
			    doc.add(new Field("type", String.valueOf(e.getType()), TYPE_STORED));
			    doc.add(new Field("datasource", e.getDataSource().getLocalURI().toString(), TYPE_STORED));
			    doc.add(new Field("label", e.getLabel(), TYPE_STORED));
			    doc.add(new Field("id", String.valueOf(e.getId().value()), TYPE_STORED));
				iwriter.addDocument(doc);
			} catch (IOException ex) {
				throw new IllegalStateException(ex);
			}
		    this.edgeBuffer.add(e);
		}
	}
	
	@Override
	protected void indexNode(Node n) {
	    try {
		    Document doc = new Document();
		    doc.add(new Field("is_edge", String.valueOf(false), TYPE_STORED));
		    doc.add(new Field("type", String.valueOf(n.getType()), TYPE_STORED));
		    doc.add(new Field("datasource", n.getDataSource().getLocalURI().toString(), TYPE_STORED));
		    doc.add(new Field("label", n.getLabel(), TYPE_STORED));
		    doc.add(new Field("id", String.valueOf(n.getId().value()), TYPE_STORED));
		    iwriter.addDocument(doc);

		} catch (IOException e) {
			logger.error(e);
		}
	}
}

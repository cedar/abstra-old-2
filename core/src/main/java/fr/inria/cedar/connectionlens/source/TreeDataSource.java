package fr.inria.cedar.connectionlens.source;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.net.URI;

import fr.inria.cedar.connectionlens.graph.AvoidLinking;
import fr.inria.cedar.connectionlens.graph.DataSourceNode;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * This class represents those data sources where a single root is discernible. 
 * The classes extending it share a notion of root-to-leaf path which is used
 * to implement PER_PATH factorization (1 node per rooted label path and node value).
 * 
 * @author ioanamanolescu
 *
 */
public class TreeDataSource extends DataSource {

	public TreeDataSource(Factory f, int id, URI localURI, URI origURI, Types t, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, id, localURI, origURI, t, graphStats, extractStats, graph);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Makes a path element given some prefix and current path. */
	static String makePathElement(String prefix, String path) {
		return prefix + (path != null ? "." + path : "");
	}
	
	/**
	 * Creates a Dewey element given some prefix and the last node's rank.
	 */
	static String makeDewElement(String prefix, int dew) {
		return prefix + "." + dew;
	}


	/**
	 * Makes a node local ID -- taken from XML
	 *
	 * @param deweyID  the dew prefix
	 * @param rootedLabelPath the path prefix
	 * @param value      the node value
	 * @return a fresh node local ID
	 */
	 String makeLocalKey(String deweyID, String rootedLabelPath, String value) {
		if (isNullOrEmpty(value)
				|| AvoidLinking.getDoNotLinkLabels().contains(Node.genericNormalization(value))) {
			return deweyID;
		}
		switch (atomicity) {
		case PER_INSTANCE:
			return deweyID;
		case PER_PATH:
			return rootedLabelPath + ":" + value;
		case PER_DATASET:
			return value;
		case PER_GRAPH:
			return value;
		default:
			throw new IllegalStateException("No such atomicity " + atomicity);
		}
	}
}

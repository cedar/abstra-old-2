package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ScoringMethod {
    public static final Logger log = Logger.getLogger(ScoringMethod.class);

    protected Configuration configuration;

    public ScoringMethod(Configuration configuration) {
        this.init(configuration);
    }

    public void init(Configuration configuration) {
        this.configuration = configuration;
        for (int collectionId : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            CollectionGraph.getWorkingInstance().setCollectionScore(collectionId, 0.0f);
        }
    }

    public void compute() throws IOException, SQLException {
        throw new IllegalStateException();
    }

    /**
     * Compute the odw/dw of original nodes.
     * The odw/dw of leaf value nodes is the number of incoming edges, other nodes have a odw/dw of 0.
     * @throws SQLException if a SQL query fails.
     */
    protected void computeAndSetOdw() throws SQLException {
        log.info("compute and set odw");
        // a. compute the DW for each leaf value node in a tmp table
        CollectionGraph.getWorkingInstance().getGraph().createTable(SchemaTableNames.ODW_COMPUTATION_TABLE_NAME, "(nodeId INTEGER, odw INTEGER)", true, true);

        String query; // we compute the dw/odw as being the number of incoming edges for leaf nodes only. Other nodes will get their dw/odw with the wDAG/wPR propagation
        if(CollectionGraph.getWorkingInstance().getIsRdf()) {
            query = "INSERT INTO " + SchemaTableNames.ODW_COMPUTATION_TABLE_NAME + " SELECT n.id, COUNT(e.id) " +
                    "FROM " + SchemaTableNames.ORIGINAL_NODES_TABLE_NAME + " n, " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " e " +
                    "WHERE e.target = n.id AND n.type = " + Node.Types.RDF_LITERAL.ordinal() + " " +
                    "GROUP BY n.id;";
        } else {
            query = "INSERT INTO " + SchemaTableNames.ODW_COMPUTATION_TABLE_NAME + " SELECT n.id, COUNT(e.id) " +
                    "FROM " + SchemaTableNames.ORIGINAL_NODES_TABLE_NAME + " n, " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " e " +
                    "WHERE e.target = n.id AND n.type IN (" + Node.getValueTypes() + ", " + Node.Types.RDF_URI.ordinal() + ") " +
                    "GROUP BY n.id;";
        }
        CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(query).execute();

        // b. update the nodes with their dw (computed above in the dw_computation table)
        // non leaf values nodes will have a dw/odw of 0. (value by default)
        query = "UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " n SET odw = odwc.odw FROM " + SchemaTableNames.ODW_COMPUTATION_TABLE_NAME + " odwc WHERE n.nodeId = odwc.nodeId; ";
        CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(query).execute();

        query = "UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " n SET dw = odwc.odw FROM " + SchemaTableNames.ODW_COMPUTATION_TABLE_NAME + " odwc WHERE n.nodeId = odwc.nodeId; ";
        CollectionGraph.getWorkingInstance().getGraph().getPreparedStatement(query).execute();

        // c. then we initialize the collectionWeights variable as follows:
        //   for (collection col corresponding to an EC i which is a leaf, that is: there are no edges i-->j)
        //       compute odw(col) as the sum of dw(n) for every data node n represented by i
        //       dw[col] = odw(col)
        //   endfor
        // for that we need to create one collection for each equivalence class of value nodes only,
        // and one collection with the appropriate nodes for equivalence class containing value nodes and structural nodes
        // get the sum of data weights of all nodes in a collection

        PreparedStatement stmt = CollectionGraph.getInstance().getGraph().getPreparedStatement("SELECT c.collId, SUM(c.dw) " +
                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " ci " +
                "WHERE c.collId = ci.collId AND ci.isLeaf = true AND ci.isActive = true AND c.isActive = true " + // we are careful about not getting leaf collections that do not exist anymore in the collection graph (they have been removed during the ID/IDREF process) + we take into account only nodes that are still present available in the working collection graph
                "GROUP BY c.collId;");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            CollectionGraph.getWorkingInstance().setCollectionScore(rs.getInt(1), rs.getInt(2));
        }
    }
}

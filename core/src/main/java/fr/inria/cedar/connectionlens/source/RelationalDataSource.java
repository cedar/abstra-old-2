/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RELATIONAL_STRUCT;

import java.net.URI;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.base.Joiner;
import com.google.common.collect.TreeMultimap;

import fr.inria.cedar.connectionlens.graph.AvoidLinking;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Implementation of a relation data source.
 */
public class RelationalDataSource extends TreeDataSource implements AutoCloseable {

	/** The source connection. */
	private final Connection sourceConnection;

	/** The statement. */
	private final Statement statement;

	/** Default label to relational value edges. */
	private static final String VALUE_LABEL = "relationalAttributeValue";

	/** Default label to relational structural edges. */
	private static final String STRUCTURAL_LABEL = "relationalStruct";

	// for each table, pk value --> node created for that tuple
	public HashMap<String, HashMap<String, Node>> pkRecord;

	// schema information about the tables: for each, we have the PKs
	// (possibly after adding some PKs ourselves), 
	// the FKs, and all other attributes
	// table name --> set of lists of attributes, each of them is a PK
	public HashMap<String, Set<ArrayList<String>>> tablePks;
	// table name --> set of lists of attributes, each of them is a FK
	public HashMap<String, Set<ArrayList<String>>> tableFks;
	// table name --> set of the other attribute names
	public TreeMultimap<String, String> tableOthers; 
	// FK table --> FK attributes --> PK table --> PK attributes
	public HashMap<Pair<String, ArrayList<String>>, Pair<String, ArrayList<String>>> pkfks;
	// the tables to which we add an attribute for the purpose of registration only
	public HashSet<String> tablesWeIndexed; 

	private String addedIndexAttributeName = "indextuple"; 

	/**
	 * Instantiates a new relational data source.
	 *
	 * @param id  the data source ID
	 * @param uri the data source URI
	 * @throws SQLException
	 */
	public RelationalDataSource(Factory f, int id, URI uri, URI origURI, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) throws SQLException {
		super(f, id, uri, origURI, Types.RELATIONAL, graphStats, extractStats, graph);
		sourceConnection = initConnection(uri);
		Objects.requireNonNull(sourceConnection);
		this.statement = sourceConnection.createStatement();
		log = Logger.getLogger(RelationalDataSource.class);
		pkRecord = new HashMap<String, HashMap<String, Node>>();
		tablePks = new HashMap<String, Set<ArrayList<String>>>(); 
		tableFks = new HashMap<String, Set<ArrayList<String>>>(); 
		tableOthers = TreeMultimap.create();  
		pkfks = new HashMap<Pair<String, ArrayList<String>>, Pair<String, ArrayList<String>>>(); 
		tablesWeIndexed = new HashSet<String>(); 
	}

	/**
	 * Initializes the connection.
	 *
	 * @param uri the data source URI
	 * @return the connection
	 */
	private static Connection initConnection(URI uri) {
		try {
			return ConnectionManager.getConnectionByURL(uri.toString());
		} catch (Exception e) {
			log.debug("Unable to connect to " + uri);
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Gets the primary key attributes if the table has any. Otherwise adds
	 * integer IDs to the table.
	 *
	 * @param schemaName the schema
	 * @param tableName  the table name
	 * @return the indexed tuples
	 * @throws SQLException
	 */
	public Collection<ArrayList<String>> getIndexedTuples(String schemaName, String tableName) throws SQLException {
		DatabaseMetaData metadata = sourceConnection.getMetaData();
		try (ResultSet pkInfo = metadata.getPrimaryKeys(null, schemaName, tableName)) {
			boolean nativeKeys = false; 
			ArrayList<String> currentPK = null; 
			while (pkInfo.next()) {
				String pkAttName = pkInfo.getString(4); 
				Integer pkSeq = pkInfo.getInt(5); 
				if (pkSeq == 1) { // start a new PK
					currentPK = new ArrayList<String>();
					Set<ArrayList<String>> keysOfThisTable = tablePks.get(tableName); 
					if (keysOfThisTable == null) { // if this is the first PK
						keysOfThisTable = new HashSet<ArrayList<String>>();
						tablePks.put(tableName, keysOfThisTable); 
					}
					keysOfThisTable.add(currentPK);
				}
				// in all cases, add this attribute to the current PK
				currentPK.add(pkAttName); 
				//log.info("Table " + tableName + " has PK " + pkAttName + " at position " + pkSeq); 
				nativeKeys = true; 
			}
			if (nativeKeys) {
				log.info("== Native PK of " + tableName + ": " + currentPK);
				//log.info("In tablePKs there are " + tablePks.get(tableName).size() + " keys "); 
				return tablePks.get(tableName);
			}
			// TODO: should be platform/version independent.
			statement.executeUpdate("ALTER TABLE " + (schemaName != null ? (schemaName + ".") : "") + tableName
					+ " ADD " + addedIndexAttributeName + " SERIAL PRIMARY KEY;");
			currentPK = new ArrayList<String>();
			currentPK.add(addedIndexAttributeName); 
			Set<ArrayList<String>> keysOfThisTable = new HashSet<>(); 
			keysOfThisTable.add(currentPK); 
			log.info("== PK we added to " + tableName + ": " + currentPK);
			tablePks.put(tableName, keysOfThisTable); 
			//log.info("WE INDEXED " + tableName); 
			tablesWeIndexed.add(tableName); 
			//log.info("Table " + tableName + " has PK " + primaryKey);
			return tablePks.get(tableName); 
		}
	}

	/**
	 * Extracts the schema name from the data source URI.
	 *
	 * @param URI the data source URI
	 * @return the schema name or null if it could not be extraced.
	 */
	private static String extractSchema(String URI) {
		Pattern p = Pattern.compile("currentSchema=([^&]*)");
		Matcher m = p.matcher(URI);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

	/**
	 * Extracts the database name from the data source URI.
	 *
	 * @param uri the data source URI
	 * @return the database name
	 */
	private static String extractDatabase(String uri) {
		return uri.split("\\/")[3].split("\\?")[0];
	}

	/**
	 * NB: A "datasource" from a relational point of view is a schema, i.e. a
	 * collection of tables(possibly) with foreign key dependencies.
	 */
	@Override
	public void traverseEdges(Consumer<Edge> processor) throws Exception {
		String schema = extractSchema(localURI.toString());
		String dbname = extractDatabase(localURI.toString());

		Node topNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		processor.accept(buildLabelledEdge(this.root, topNode, Edge.ROOT_LABEL));
		// Retrieving table names
		ArrayList<String> tableNames = new ArrayList<>();

		DatabaseMetaData metadata = sourceConnection.getMetaData();
		String[] types = { "TABLE" };
		try (ResultSet tables = metadata.getTables(null, schema, null, types)) {
			while (tables.next()) {
				String table = tables.getString(3); 
				tableNames.add(table);
				log.info(""); log.info("TABLE: " + table); 

				Node tableNode = buildLabelNodeOfType(table, RELATIONAL_STRUCT);
				processor.accept(buildLabelledEdge(topNode, tableNode, Edge.TABLE_EDGE_LABEL));

				// find (or create) the primary keys of this table
				Collection<ArrayList<String>> primaryKeys = getIndexedTuples(schema, table);

				String prevPKTableName = ""; 
				String pkTableName = ""; 
				ArrayList<String> currentFK = null; 
				ArrayList<String> referredPK = null; 
				// learn the FK attributes of this table, and fill in pkfk
				try(ResultSet fks = metadata.getImportedKeys(null, schema, table)){
					while (fks.next()) {
						//log.info("FOREIGN KEY"); 
						String fkAttName = fks.getString(8); 
						Integer fkSeq = fks.getInt(9); 
						prevPKTableName = pkTableName; // save name of the table referenced in prev fk
						pkTableName = fks.getString(3);
						String pkColumnName = fks.getString(4); 
						if (fkSeq == 1) { 
							// record the previous currentFK/referredPK pair in pkfk
							addToFKPK(table, currentFK, prevPKTableName, referredPK); 
							// start a new FK
							currentFK = new ArrayList<String>();
							referredPK = new ArrayList<String>(); 
							Set<ArrayList<String>> fKeysOfThisTable = tableFks.get(table); 
							if (fKeysOfThisTable == null) { // if this is the first FK
								fKeysOfThisTable = new HashSet<ArrayList<String>>();
								tableFks.put(table, fKeysOfThisTable); 
							}
							fKeysOfThisTable.add(currentFK);
						}
						// in all cases, add this attribute to the current PK
						currentFK.add(fkAttName); 
						referredPK.add(pkColumnName); 
						//log.info("XXX Table " + table+ " has FK attribute " + fkAttName + " on " + 
						//		pkTableName + "." + pkColumnName + " at position " + fkSeq); 
					}
				}
				// now record the last PKFK (at schema level)
				addToFKPK(table, currentFK, pkTableName, referredPK); 

				// pass over the table, and create nodes for all the PK attributes and all the
				// non-PK, non-FK attributes
				String fullTableName = schema + "." + table;	
				ArrayList<String> pkAttribAux = new ArrayList<String>(); // all unique
				try (ResultSet columnsInfo = metadata.getColumns(null, schema, table, null)) {
					while (columnsInfo.next()) {
						String name = columnsInfo.getString("COLUMN_NAME");
						if (notAPK(table, name)) { // if not a PK
							if (notAFK(table, name)) { // if neither PK nor FK, then others
								tableOthers.put(table, name); 
							}
							else { // not a PK, but a FK. We chose not to represent it
								// based on the idea that FKs are already represented by 
								// edges and don't need to be also represented as nodes.
							}
						}
						else { // if it's a PK, keep it in any case
							pkAttribAux.add(name); 
						}
						
					}
					// pkAttribAux does not have them in the same order as the one we got before!
					// we do this in order to formulate this query with the PK attribs in the right order
					ArrayList<String> pkAttributes = findPrimaryKeyWithSameAttributes(primaryKeys, pkAttribAux); 
					//log.info("Reordered " + pkAttribAux + " into " + pkAttributes); 
					// important not to change the order of the attributes
					ArrayList<String> queriedAttributes = new ArrayList<String>();
					queriedAttributes.addAll(pkAttributes);
					Collection<String> others = tableOthers.get(table); 
					//log.info("Table " + table + " has key: " + pkAttributes + 	" and other attributes: " + others); 
					queriedAttributes.addAll(others); 
					// now we know all the PKs, FKs (if any), and others (if any)
					// we select the PKs and then the others
					if ((pkAttributes.size() > 0) || (others.size() > 0)){
						String pkeyString = Joiner.on(",").join(pkAttributes); // surely not empty
						//log.info("Joining " + pkAttributes + " gave: " + pkeyString); 
						String cols = pkeyString; 
						if (others != null && others.size() > 0) {
							String otherString = Joiner.on(", ").join(tableOthers.get(table));
							cols = cols + ", " + otherString; 
						}
						//log.info("QUERY: SELECT " + cols + " FROM " + fullTableName);		
						//log.info("Keying " + fullTableName + " tuples by " + pkAttributes); 
						try (ResultSet rs = statement.executeQuery("SELECT " + cols + " FROM " + fullTableName)) {
							while (rs.next()) {
								Node tupleNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
								processor.accept(buildLabelledEdge(tableNode, tupleNode, Edge.TUPLE_EDGE_LABEL));

								ArrayList<String> thisTupleKeyVals = new ArrayList<String>(); 
								for (int i = 1; i <= pkAttributes.size(); i ++) {
									String attName = queriedAttributes.get(i-1);
									String thisIDField = rs.getString(i); 
									//log.info("Key attribute " + attName + " at " + i); 
									if (!isNullOrEmpty(thisIDField)) {
										 if (!attName.equals(addedIndexAttributeName)) {
											 // do not create a node for the key we introduced
											 for (String sentence : getSentences(thisIDField)) {
												 Node valNode = createOrFindValueNodeWithAtomicity(sentence,
														 makeNodeKey(dbname, schema, table, tupleNode.getId().toString(), 
																 attName, sentence), this, Node.Types.RELATIONAL_VALUE);
												 Edge edge = buildLabelledEdge(tupleNode, valNode, attName);
												 //log.info("(1) Adding *" + attName + "=" + sentence + 
												 //	 "* child of tuple node " + tupleNode.getId());
												 edge.setDataSource(this);
												 processor.accept(edge);
											 }
										 }
										 // add to the key in any case
										 thisTupleKeyVals.add(thisIDField); 
									}
								}
								// record the tuple in its table by this key: 
								//log.info("Key values for this tuple: " + thisTupleKeyVals); 
								String thisTupleKey = Joiner.on("|").join(thisTupleKeyVals); 
								recordPKentry(table, thisTupleKey, tupleNode); 
								
								// add nodes reflecting the other attributes
								for (int j = pkAttribAux.size() + 1; j <= pkAttribAux.size() + others.size(); j++) {
									String value = rs.getString(j);
									String attName = queriedAttributes.get(j-1);
									//log.info("Attribute " + attName + " at " + j); 
									if ((!isNullOrEmpty(value)) && 
										(!attName.equals(addedIndexAttributeName))) {
										for (String sentence : getSentences(value)) {
											Node valNode = createOrFindValueNodeWithAtomicity(sentence,
													makeNodeKey(dbname, schema, table, tupleNode.getId().toString(), 
															attName, sentence), this, Node.Types.RELATIONAL_VALUE);
											Edge edge = buildLabelledEdge(tupleNode, valNode, attName);
											//log.info("(2) Adding *" + attName + "=" + sentence + 
											//		"* child of tuple node " + tupleNode.getId()); 
											edge.setDataSource(this);
											processor.accept(edge);
										}
									}
								} 	
							}
						}
					}
				}
			}
		}
		log.info("==== SECOND PASS FOR PK-FK ====");
		Iterator<Pair<String, ArrayList<String>>> pkIt = pkfks.keySet().iterator(); 
		while (pkIt.hasNext()) {
			Pair<String, ArrayList<String>> fkEntry = (Pair<String, ArrayList<String>>) pkIt.next();
			String fkTable = fkEntry.getKey();
			ArrayList<String> fkAttr = fkEntry.getValue();
			Pair<String, ArrayList<String>>pkEntry = (Pair<String, ArrayList<String>>) pkfks.get(fkEntry); 
			String pkTable = pkEntry.getKey();
			ArrayList<String> pkAttr = pkEntry.getValue();
			// reorder the pkAttrs, use this in the SELECT clause
			ArrayList<String> reorderedPKAttr = findPrimaryKeyWithSameAttributes(tablePks.get(pkTable), pkAttr); 
					
			log.info("== FK constraint: " + fkTable + "." + fkAttr + " --> " + pkTable	+ "." + pkAttr); 

			ArrayList<String> fkOwnKey1 = tablePks.get(fkTable).iterator().next(); 
			String query = "SELECT " + toString4Query(fkOwnKey1, "t1") + ", " + 
					toString4Query(reorderedPKAttr, "t2") + // use reorderedPKAttr in the SELECT to ensure
					// correct lookup by tuple key
					" FROM " + schema + "." + fkTable + " AS t1, " + 
					schema + "." + pkTable + " AS t2 WHERE "; 
			for (int i = 0; i < fkAttr.size(); i ++) {
				query += "t1." + fkAttr.get(i) + "=t2." + pkAttr.get(i); // use pkAttr in the WHERE to ensure PKFK join
				if (i < fkAttr.size() - 1) {
					query += " and "; 
				}
			}
			//log.info("Running: " + query); 
			try (ResultSet toLink = statement.executeQuery(query)) {
				while (toLink.next()) {
					String thisTupleFK = getTupleKey(toLink, 1, fkOwnKey1.size());
					String thisTuplePK = getTupleKey(toLink, fkOwnKey1.size() + 1, 
							fkOwnKey1.size() + pkAttr.size()); 
					Node nFK = getNode(fkTable, thisTupleFK);
					if (nFK == null) {
						throw new Error("FK: cannot find node " + thisTupleFK + " in " + fkTable);
					}
					Node nPK = getNode(pkTable, thisTuplePK); 
					if (nPK == null) {
						throw new Error("PK: cannot find node " + thisTuplePK + " in " + pkTable);
					}
					//log.info("Tuple " + thisTupleFK + " of " + fkTable + " refers to tuple " + 
					//		thisTuplePK + " of " + pkTable + " by attribute " + fkAttr); 
					processor.accept(buildLabelledEdge(nFK, nPK, Joiner.on(",").join(fkAttr)));
				}
			}
		}
		processEdgesToLocalAndOriginalURIs(processor);
		//log.info("=== THIRD STEP, DROP PKs WE ADDED"); 
		for (String table: tablesWeIndexed) {
			//log.info("DROP " + table + ".addedIndexAttributeName"); 
			String update = "ALTER TABLE " + schema + "." + table + " DROP COLUMN " + addedIndexAttributeName; 
			try{
				statement.executeUpdate(update); 
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private ArrayList<String> findPrimaryKeyWithSameAttributes(Collection<ArrayList<String>>primaryKeys,
			ArrayList<String> pkAttribAux) {
		Iterator<ArrayList<String>> it = primaryKeys.iterator(); 
		while (it.hasNext()) {
			ArrayList<String> candidateKey = it.next();
			if (candidateKey.size() == pkAttribAux.size()) {
				boolean same = true; 
				for (int i = 0; i < candidateKey.size(); i ++) {
					if (!pkAttribAux.contains(candidateKey.get(i))) {
						same = false; 
					}
				}
				if (same) {
					return candidateKey; 
				}
			}
		}
		throw new Error("No key in " + primaryKeys + " was found to match attributes " + pkAttribAux); 
	}

	String getTupleKey(ResultSet rs, int start, int end) throws SQLException {
		ArrayList<String> results = new ArrayList<String>();
		for (int i = start; i <= end; i ++) {
			results.add(rs.getString(i));
		}
		return Joiner.on("|").join(results); 
	}


	private void addToFKPK(String fkTable, ArrayList<String> fkAttributes, 
			String pkTableName,
			ArrayList<String> pkAttributes) {	
		if (pkTableName.length()>0) {
			// FK table --> FK attributes --> PK table --> PK attributes
			// HashMap<Pair<String, ArrayList<String>>, Pair<String, ArrayList<String>>> pkfks;
			Pair<String, ArrayList<String>> thisFKEntry = Pair.of(fkTable, fkAttributes);
			Pair<String, ArrayList<String>> thisPKEntry = Pair.of(pkTableName, pkAttributes);
			pkfks.put(thisFKEntry, thisPKEntry); 
			// record also into tableFKs
			Set<ArrayList<String>> thisTableFKs = tableFks.get(fkTable); 
			if (thisTableFKs == null) {
				thisTableFKs = new HashSet<ArrayList<String>>();
				tableFks.put(fkTable, thisTableFKs); 
			}
			thisTableFKs.add(fkAttributes); 
			log.info("PKFK: " + fkTable + "." + fkAttributes + " is a FK to " + pkTableName + 
					"." + pkAttributes); 
		}

	}

	/** True if attributeName is not part of a key of table */
	private boolean notAPK(String table, String attributeName) {
		//log.info("Checking to see if " + table + "." + attributeName + " is a PK"); 
		Set<ArrayList<String>> thisTablePKs = tablePks.get(table); 
		if (thisTablePKs == null) {
			//log.info("No PKs for " + table); 
			return true; 
		}
		for (ArrayList<String> thisKey: thisTablePKs) {
			//log.info("Looking for " + attributeName + " in " + thisKey); 
			if (thisKey.contains(attributeName)) {
				return false; 
			}
		}
		return true; 
	}
	/** True if attributeName is not part of a foreign key in table */
	private boolean notAFK(String table, String attributeName) {
		Set<ArrayList<String>> thisTableFKs = tableFks.get(table); 
		if (thisTableFKs == null) {
			return true; 
		}
		for (ArrayList<String> thisKey: thisTableFKs) {
			if (thisKey.contains(attributeName)) {
				return false; 
			}
		}
		return true; 
	}

	private void recordPKentry(String table, String tupleStringID, Node tupleNode) {
		HashMap<String, Node> thisTable = this.pkRecord.get(table);
		if (thisTable == null) {
			thisTable = new HashMap<String, Node>();
			this.pkRecord.put(table, thisTable);
		}
		thisTable.put(tupleStringID, tupleNode);
		//log.info("== In " + table + " put tuple on " + tupleStringID); 
	}

	/**
	 * Makes a node ID.
	 *
	 * @param dbname  the database name
	 * @param schema  the schema name
	 * @param table   the table name
	 * @param tupleID the tuple ID
	 * @param col     the column name
	 * @param value   the value
	 * @return the newly created node ID
	 */
	private String makeNodeKey(String dbname, String schema, String table, String tupleID, String col, String value) {
		String result = dbname;
		if (schema != null) {
			result += "." + schema;
		}
		if (table != null) {
			result += "." + table;
		}
		if (tupleID != null && col == null) {
			result += "." + tupleID;
		}
		if (col != null) {
			result += ".";
			if (AvoidLinking.getDoNotLinkLabels().contains(Node.genericNormalization(col))) {
				return result += tupleID + "." + col;
			}
			switch (atomicity) {
			case PER_INSTANCE:
				result += tupleID + "." + col;
				break;
			case PER_PATH:
				result += col + ":" + value;
				break;
			case PER_DATASET:
				result = value;
				break;
			case PER_GRAPH:
				return value;
			default:
				throw new IllegalStateException("No such atomicity " + atomicity);
			}
		}
		return result;
	}

	
	private Node getNode(String tableName, String tupleID) {
		return this.pkRecord.get(tableName).get(tupleID);
	}

	/**
	 * @param columnList the column list
	 * @param table      the table name
	 * @return a string joining the elements of the given column list, separated by
	 *         a comma and prefixed with the given table name
	 */
	private static String toString4Query(ArrayList<String> columnList, String table) {
		Iterator<String> it = columnList.iterator();
		String cols = "";
		if (it.hasNext()) {
			cols += table + "." + it.next();
		}
		while (it.hasNext()) {
			cols += ", " + table + "." + it.next();
		}
		return cols;
	}

	@Override
	public void close() throws SQLException {
		if (this.sourceConnection != null) {
			this.sourceConnection.close();
		}
	}

	@Override
	public void postprocess(Graph graph) {
		// Do whatever ¯\_(ツ)_/¯
	}

	@Override
	public String getContext(Graph g, Node n) {
		if (n.getType() == Node.Types.RELATIONAL_STRUCT) {
			// returns the name of the table?
			return "<i>This is a structural node of this relational source</i>";
		}
		/*
		 * Display the tuple that contains this node, by: 1) extracting the tuple ID t
		 * 2) find all edges that are connected to t 3) reconstruct the tuple
		 */
		Set<Edge> edges = g.getAdjacentEdges(n); // get the adjacent edges of this relation attribute value, should only
		// have one
		List<String> attributes = new ArrayList<>();
		Map<Node, Map<String, Node>> tuples = new HashMap<>(); // map each tupe to its map <attribute:value>

		int count = 0, limit = 5; // only get 5 tuples

		for (Edge e : edges) {
			// ignore empty edges
			if (!e.getLabel().equals("")) {
				if (count++ > limit)
					break;
				Node tupleNode = (e.getSourceNode().equals(n) ? e.getTargetNode() : e.getSourceNode()); // the source
				// node stands
				// for the ID of
				// the tuple
				Map<String, Node> attrValue = new HashMap<>();
				Set<Edge> edgesTuple = g.getAdjacentEdges(tupleNode);
				for (Edge et : edgesTuple) {
					// for each edge connecting this tuple node, retrieve the node on the other
					// side, together they can reconstruct the tuple
					// the edge label indicates the name of the attribute
					if (et.getType() != EDGE) {
						continue;
					}
					if (!attributes.contains(et.getLabel()))
						attributes.add(et.getLabel());
					attrValue.put(et.getLabel(),
							et.getSourceNode().equals(tupleNode) ? et.getTargetNode() : et.getSourceNode());
				}
				tuples.put(tupleNode, attrValue);
			}
		}
		// convert to a string representing such tuple: on each line, the attribute name
		// and its value
		String tupleString = "";

		// 1. create the header
		String header = "";
		for (String attr : attributes)
			header += "<th>" + attr + "</th>";
		header = "<thead><tr>" + header + "</tr></thead>";

		// 2. create the body
		String body = "";
		String row = "";
		for (Entry<Node, Map<String, Node>> tuple : tuples.entrySet()) {
			// for each tuple, try to extract the row
			row = "";
			for (int i = 0; i < attributes.size(); i++) {
				Node valueNode = tuple.getValue().get(attributes.get(i));
				String value = valueNode != null ? valueNode.getLabel() : "";
				if (valueNode != null && valueNode.equals(n)) {
					row += "<td>" + "<b style='color:red;'>" + value + "</b>" + "</td>";
				} else {
					row += "<td>" + value + "</td>";
				}
			}
			body += "<tr>" + row + "</tr>";
		}

		body = "<tbody>" + body + "</tbody>";

		// 3. finalize the table
		tupleString = "<table class='table' style='width:auto;'>" + header + body + "</table>";
		return tupleString;
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isBoolean;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameType;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.EITHER;

import java.util.function.BiFunction;

import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class ExactPairProcessor extends CachedSimilarPairProcessor {

	private final BiFunction<String, String, Double> sim = (a, b) -> a.equals(b) ?
			1.0:
			0.0;
	
	public ExactPairProcessor(StatisticsCollector stats) {
		super(stats, 1.0);
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result = not(isBoolean(EITHER).or(isNumeric(EITHER)))
				.and(sameLabel()).and(sameType());
		//if(entitiesOnly) {
		//	result = result.and(isEntity(BOTH));
		//}
		return result;
	}


	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	@Override
	public Double apply(String a, String b) {
		return sim.apply(a, b);
	}

	/**
	 * @param val the value to check
	 * @return true, if value is equals to true, false, yes, no, on or off.
	 */
	private static boolean parseBoolean(String val) {
		return val.equalsIgnoreCase("true")  || val.equalsIgnoreCase("yes") || val.equalsIgnoreCase("on")
			|| val.equalsIgnoreCase("false") || val.equalsIgnoreCase("no")  || val.equalsIgnoreCase("off");
	}
}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

import java.sql.SQLException;

public class wDAG extends ScoringMethod {
    public static final Logger log = Logger.getLogger(wDAG.class);

    public wDAG(Configuration configuration) {
        super(configuration);
    }

    @Override
    public void compute() throws SQLException {
        // g. then, we compute the collection data weight.
        // first we assign a weight for each leaf collection, which is the weight of its nodes.
        //     On March 12th 2022, this is the number of nodes (because each node has a weight of 1))
        // then we compute the data weight of each collection by multiplying the data of the leaf collection by the total transfer until the collection
//        log.debug("compute score for greedy by doing the backward propagation and assigning new dw in the list of scores");

        // assign dw/odw for original nodes
        this.computeAndSetOdw();

        BackwardPropagation backwardPropagation = new BackwardPropagation();
        backwardPropagation.propagateWeights(true); // this updates the scores in the collection graph
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
    }
}

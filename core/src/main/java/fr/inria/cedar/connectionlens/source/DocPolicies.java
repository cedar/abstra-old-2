/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.inria.cedar.connectionlens.source.DocPolicies.PolicyType.IGNORED_NODE;
import static fr.inria.cedar.connectionlens.source.DocPolicies.PolicyType.NOT_TRAVERSED_NODE;

/**
 * @author Tayeb Merabti
 */
public abstract class DocPolicies {

    /** The underlying Properties. */
    protected  Properties props;

    /**
     * enum of policies types implemented.
     */
    public static enum PolicyType{

        IGNORED_NODE("ignore"), //totally ignore the node
        NOT_TRAVERSED_NODE("no_node"); // ignore the name node but not the text (if exists).

        private String label;

        private PolicyType (String label) {
            this.label = label;
        }
        /**
         * get the label of the policy
         * @return
         */
        public String getLabel(){
            return this.label;
        }
    }

    /**
     * no_node or not_traversed_node,
     * the node will not be traversed nevertheless the text will not be ignored.
     * @return
     */
    public List<String> getNotTraversedNodeOnly(){
        List<String> notTraversedNodesOnly  = new ArrayList<> ();
        for(String cond:getAllConditions()){
            if(props.getProperty(cond).equals (NOT_TRAVERSED_NODE.getLabel())){
                notTraversedNodesOnly.add(cond);
            }
        }
        return  notTraversedNodesOnly;
    }

    /**
     *
     * @param policyText
     * @return the number of [] in the policy
     */
    private int countOfBrackets(String policyText){
        int count = 0;
        for (int i = 0; i < policyText.length(); i++) {
            if (policyText.charAt(i) == '[') {
                count++;
            }
        }
        return count;
    }


    /**
     * get All conditions
     * @return
     */
    public Set<String> getAllConditions(){
        return props.stringPropertyNames();
    }


    public Set<String> getEdgesProperties(){
        Set<String> allEdgesProps =  new LinkedHashSet<> ();
        Enumeration em = props.keys();
        while(em.hasMoreElements()){
            String str = (String)em.nextElement();
            allEdgesProps.add(str);
        }
        return allEdgesProps;
    }

    /**
     * the whole node will be ignored.
     * @return
     */
    public List<String> getIgnoredNodes(){
        List<String> ignoredNodes  = new ArrayList<> ();
        for(String cond:getAllConditions()){
            if(props.getProperty(cond).equals (IGNORED_NODE.getLabel())){
                if(countOfBrackets(cond) > 1){
                    String reWritingText = multipleAttributesQuery(cond);
                    ignoredNodes.add(reWritingText);
                }
                else {
                    ignoredNodes.add(cond);
                }
            }
        }
        return  ignoredNodes;
    }


    /**
     *
     * @param policyText: div[class="media"][class="media-align-center"][class="media-image"]
     * @return reWriting the policy for Jsoup query -> div[class="media" "media-align-center" "media-image"]
     */
    private String multipleAttributesQuery(String policyText){
        StringBuilder reWritingCond = new StringBuilder (policyText.substring(0,policyText.indexOf('[')));
        HashMap<String,String> attributes = new HashMap<> ();
        Matcher matcher = Pattern.compile("\\[(.*?)\\]").matcher(policyText);
        int i = 0;
        while (matcher.find()) {
            String[] attribute = matcher.group(1).split ("="); // class="media-align-center"
            if(!attributes.containsKey(attribute[0])){
                attributes.put(attribute[0],attribute[1].substring (1,attribute[1].length()-1));
            }
            else{
                attributes.put(attribute[0],attributes.get(attribute[0])+" "+attribute[1].substring (1,attribute[1].length()-1));
            }
            i++;
        }
        for (String attributeName : attributes.keySet()) {
            reWritingCond.append("[")
                    .append(attributeName)
                    .append ("*=\"") //contains
                    .append (attributes.get(attributeName));
        }
        reWritingCond.append("\"]");
        return reWritingCond.toString();
    }



    /**
     * Attributes not ignored.
     * attribute's name -> edge's name.
     * attribute's value -> target node's name.
     * @return
     */
    public List<String> getAttributesPolicies() {
        List<String> nodeAttributes = new ArrayList<> ();
        for (String cond : getAllConditions()) {
            if (!props.getProperty(cond).equals (NOT_TRAVERSED_NODE.getLabel())
                    && !props.getProperty(cond).equals (IGNORED_NODE.getLabel())) {
                if (cond.contains("@")) {
                    if (props.containsKey(cond)) {
                        if (!props.getProperty(cond).isEmpty()) {
                            nodeAttributes.add(cond + "->" + props.getProperty(cond));
                        } else {
                            nodeAttributes.add(cond);
                        }
                    }
                }

            }
        }
        return nodeAttributes;
    }

}

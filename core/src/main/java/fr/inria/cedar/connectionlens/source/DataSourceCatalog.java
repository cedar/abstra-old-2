/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.source.DataSource.Types;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a catalog that will list, for each registered datasource, its id, its path
 * and its type
 * 
 * @author redouane
 *
 */
public abstract class DataSourceCatalog {

	/** The logger. */
	private final static Logger logger = LoggerFactory.getLogger(DataSourceCatalog.class);
	
	/** The item factory */
	protected final Factory factory;

	/** The entity extractor. */
	protected final EntityExtractor extractor;

	/** A map from each data source ID to its URI. */
	protected final SortedMap<Integer, URI> id2uri = Maps.newTreeMap();
	
	/** A map from each data source URI to its instance. */
	protected final Map<URI, DataSource> uri2ds = Maps.newLinkedHashMap();
	
	protected final StatisticsCollector graphStats;
	protected final StatisticsCollector extractStats;
	protected final StatisticsCollector indexStats;


	/**
	 * Instantiates a new data source catalog.
	 * @param f the item factory
	 * @param x the extractor
	 * @param indexStats 
	 */
	public DataSourceCatalog(Factory f, EntityExtractor x, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector indexStats) {
		this.factory = f;
		this.graphStats = graphStats;
		this.extractStats = extractStats; 
		this.indexStats = indexStats; 
		this.extractor = x;
	}

	

	/**
	 * Resets the catalog: this only works in memory, so whatever persistent state
	 * there was, has to be explicitly reset
	 */
	public void reset() {
		this.id2uri.clear();
		this.uri2ds.clear();
	}

	/**
	 * Initialized the catalog.
	 */
	public void init(Graph graph, boolean reset) {
		// Nothing to do
	}
	
	public abstract void close();

	/**
	 * @return the collection of data sources currently in the catalog.
	 */
	public Collection<DataSource> getDataSources() {
		return ImmutableList.copyOf(this.uri2ds.values());
	}

	public ArrayList<DataSource> getDataSourcesAsList() {
		ArrayList<DataSource> dataSources = new ArrayList<>();
		for(Map.Entry<URI, DataSource> entry : this.uri2ds.entrySet()) {
			dataSources.add((entry.getValue()));
		}
		return dataSources;
	}

	/**
	 * Adds the given entry to the catalog, and returns the corresponding data source instance.
	 *
	 * @param id the data source ID
	 * @param type the data source type
	 * @param uri the data source URI
	 * @return the data source instance
	 */
	protected DataSource addEntry(Graph graph, int id, Types type, URI uri, URI origURI) {
		DataSource result = uri2ds.get(uri);
		if (result != null || uri.equals(id2uri.get(id))) {
			logger.error("Attempting to add an already registered data source: " + uri);
			return null;
		}
		this.id2uri.put(id, uri);
		this.uri2ds.put(uri, (result = build(id, type, uri,origURI, graph)));
		result.setGraphStats(graphStats);
		result.setExtractStats(extractStats);
		return result;
	}

	/**
	 * Adds the given entry to the catalog, and returns the corresponding data source instance.
	 *
	 * @param type the data source type
	 * @param uri the data source URI
	 * @return the data source instance
	 */
	public DataSource addEntry(Graph graph, Types type, URI uri, URI origURI) {
		return addEntry(graph, (int) ((id2uri.isEmpty() ? 0 : id2uri.lastKey()) + 1), type, uri,origURI);
	}

	/**
	 * Gets the data source going with the given ID.
	 *
	 * @param identifier some data source identifier
	 * @return the data source in the catalog with the given ID, or null if there is no such data
	 * source in the catalog.
	 */
	public DataSource getEntry(int identifier) {
		URI uri = id2uri.get(identifier);
		if (uri != null) {
			return uri2ds.get(uri);
		}
		return null;
	}

	/**
	 * Gets the data source going with the given URI.
	 *
	 * @param uri some data source URI
	 * @return the data source in the catalog with the given URI, or null if there is no such data
	 * source in the catalog.
	 */
	public DataSource getEntry(URI uri) {
		return uri2ds.get(uri);
	}

	


	/**
	 * Builds a data source instance based on the given parameters.
	 *
	 * @param identifier the data source identifier
	 * @param type the data source type
	 * @param uri the data source URI
	 * @return a fresh instance of data source
	 * @throws IllegalStateException if the argument did not suffice to build the data source 
	 * instance.
	 */
	
	protected abstract DataSource build(int identifier, Types type, URI uri, URI origURI, Graph graph);
}

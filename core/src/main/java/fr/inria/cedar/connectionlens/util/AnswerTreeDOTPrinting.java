/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import com.google.common.collect.TreeMultimap;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.score.Score.DoubleScore;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.search.GAMAnswerTree;
import fr.inria.cedar.connectionlens.search.Query;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnswerTreeDOTPrinting {

	private BufferedWriter bufferedWriter;
	private Set<Node> printedNodes = new HashSet<>();
	private String drawingDir;
	private String dotFileName;
	private String pdfFileName;
	private String drawingDirectories = null; // Changed by Madhu on 13/09/2021
	private boolean isAnswerTree = false;
	private Map<Integer, String> keywordColorMap = new HashMap<>();
	private List<String> colors = new ArrayList<>(Arrays.asList("red", "green", "blue", "orange", "purple"));
	private int colorCount = 0;
	private DecimalFormat decimalFormat = new DecimalFormat("#.00");
	private TreeMultimap<Double, String> solutionPDFsForQuery;
	private ScoringFunction scoreFunction;
	private static final Logger log = Logger.getLogger(AnswerTreeDOTPrinting.class);
	static boolean warnedDotUnavailable = false; 
	private Object obj;

	public AnswerTreeDOTPrinting(Object obj) {
		this.obj = obj;
	}


	private void setUpDirs() {
		drawingDir = Config.getInstance().getStringProperty("temp_dir");
		if(obj instanceof Query) {
			String keywords = ((Query)obj).getComponents().toString().replace("[", "").replace("]", "").replace(",", "_")
					.replace("\"", "_q_").replace(" ", "");
			drawingDirectories = Paths.get(drawingDir, keywords).toString();
		}
		else
			drawingDirectories = Paths.get(drawingDir, ((Integer)obj).toString()).toString();
		//log.info("Creating directory for: "+ (Paths.get(drawingDir, keywords).toString()));
		File queryDrawingDirectory = new File(drawingDirectories);
		if (!queryDrawingDirectory.exists()) {
			queryDrawingDirectory.mkdirs();
		}
	}

	private long getTreeNo(AnswerTree at) {
		try {
			GAMAnswerTree gat = (GAMAnswerTree) at;
			return gat.getNo();
		} catch (ClassCastException cce) {
			//log.info("This is not a GAMAnswerTree");
		}
		return -1;
	}

	private void setup(AnswerTree at, long epoch, ScoringFunction scoreFunction) {
		if (drawingDirectories == null) {
			setUpDirs();
		}
		this.scoreFunction = scoreFunction;
		long treeNo = getTreeNo(at);
		try {

			bufferedWriter = new BufferedWriter(
					new FileWriter(Paths.get(drawingDirectories, dotFileName).toString()));
			bufferedWriter.write("digraph g{" + "\n label=\"Tree " + ((treeNo < 0) ? epoch : treeNo) + ", score: "
					+ this.scoreFunction.compute(at) + ", " + at.edges().size() + " edges\";" + "\n splines=polyline;"
					+ "\n rankdir=\"LR\";" + "\n node[shape=plaintext];\n");
		} catch (IOException e) {
			throw new IllegalStateException("Could not open dot file for printing");
		}

	}

	private String getColorString() {
		if (colorCount == colors.size()) { // cycling back to 0 if necessary
			colorCount = 0;
		}
		String solution = colors.get(colorCount);
		colorCount++;
		return solution;
	}

	public void printMergeFamily(AnswerTree input1, AnswerTree input2, AnswerTree merged, String resultedFrom,
			boolean status, ScoringFunction scoreFunction) {
		long epoch = System.currentTimeMillis();
		printWithTimestamp(input1, resultedFrom + "-in1", status, epoch, scoreFunction);
		printWithTimestamp(input2, resultedFrom + "-in2", status, epoch, scoreFunction);
		printWithTimestamp(merged, resultedFrom, status, epoch, scoreFunction);
	}

	public void print(AnswerTree answerTree, String resultedFrom, boolean status, //Query query,
			ScoringFunction scoreFunction) {
		long epoch = System.currentTimeMillis();
		String pdfFileName = printWithTimestamp(answerTree, resultedFrom, status, epoch, scoreFunction);
		if (status) {
			//TreeMultimap<Double, String> filesForThisQuery = solutionPDFsForQuery.get(query);
			if (solutionPDFsForQuery == null) {
				solutionPDFsForQuery = TreeMultimap.create();
				//solutionPDFsForQuery.put(query, filesForThisQuery);
			}
			DoubleScore ds = (DoubleScore) scoreFunction.compute(answerTree);
			solutionPDFsForQuery.put((-1.0) * ds.value(), pdfFileName);
		}
	}

	public void generateSolutionLatexBook() {
		if (drawingDir == null) {
			setUpDirs();
		}
		String latexBookFileName = "cl_solutions_" + obj.toString() + ".tex";
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(Paths.get(drawingDirectories, latexBookFileName).toString()));
			bufferedWriter.write("\\documentclass{article}\n");
			bufferedWriter.write("\\usepackage[top=2mm,bottom=2mm,left=20mm,right=2mm]{geometry}\n");
			bufferedWriter.write("\\usepackage{graphicx}\n");
			bufferedWriter.write("\\usepackage[yyyymmdd,hhmmss]{datetime}\n");
			bufferedWriter.write("\\begin{document}\n");
			bufferedWriter.write("\\title{Answers to the query: " + obj.toString().replaceAll("_"," ") + "}\n");
			bufferedWriter.write("\\author{\\textsc{ConnectionLens}}\n");
			bufferedWriter.write("\\date{\\today, \\currenttime}\n");
			bufferedWriter.write("\\maketitle\n");
			//TreeMultimap<Double, String> filesForThisQuery = solutionPDFsForQuery.get(query);
			if (solutionPDFsForQuery != null) {
				for (Double score : solutionPDFsForQuery.keySet()) {
					for (String file : solutionPDFsForQuery.get(score)) {
						bufferedWriter.write(
								"\\newpage\\rotatebox{270}{\\includegraphics[width=\\textheight]{" + file + "}}\n");
					}
				}
			}
			bufferedWriter.write("\\end{document}\n");
			bufferedWriter.close();
		} catch (IOException e) {
			throw new IllegalStateException("Unable to generate Latex solution book " + e.toString());
		}
	}

	private String printWithTimestamp(AnswerTree answerTree, String resultedFrom, boolean status, //Query query,
			long epoch, ScoringFunction scoreFunction) {
		isAnswerTree = true;
		long treeNo = getTreeNo(answerTree);
		long numberComponentInFileName = (treeNo < 0) ? epoch : treeNo;
		dotFileName = "cl_graph_" + numberComponentInFileName + "_" + status + "_" + resultedFrom + ".dot";
		setup(answerTree, epoch, scoreFunction);
		pdfFileName = "cl_graph_" + numberComponentInFileName + "_" + status + "_" + resultedFrom + ".pdf";
		Set<Edge> edges = answerTree.edges();
		Set<Node> nodes = answerTree.nodes();
		if (!edges.isEmpty()) {
			for (Edge edge : edges) {
				print(edge, answerTree);
			}
		} else if (!nodes.isEmpty()) {
			for (Node node : nodes) {
				print(node, answerTree);
			}
		}
		closeAndDraw();
		return pdfFileName;
	}

	private void closeAndDraw() {
		String pathToDotInstallation = Config.getInstance().getProperty("drawing.dot_installation");

		try {
			bufferedWriter.write("}\n");
			bufferedWriter.close();
		} catch (IOException ioe) {
			throw new IllegalStateException("Could not close dot file");
		}
		try {
			String dotFilePath = (Paths.get(drawingDirectories, dotFileName).toString());
			String outputDir = (Paths.get(drawingDirectories).toString());
			File directory = new File(outputDir);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			String[] command = { pathToDotInstallation, "-Tpdf", dotFilePath, "-o", outputDir + File.separator + pdfFileName };
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			if (!warnedDotUnavailable) {
				log.info(
						"Could not process .dot files: install graphviz or check the drawing.dot_installation value in local.settings"
								+ e.toString());
				warnedDotUnavailable = true; 
			}
		}
	}

	private void print(Edge edge, AnswerTree answerTree) {//, Query query) {
		Node sourceNode = edge.getSourceNode();
		print(sourceNode, answerTree);
		Node targetNode = edge.getTargetNode();
		print(targetNode, answerTree);
		try {
			bufferedWriter.write(sourceNode.getId().value().toString() + " -> " + targetNode.getId().value().toString()
					+ " [label=\"" + edge.getLabel() + " " + edge.getId().value() + " c: "
					+ decimalFormat.format(edge.confidence()) + " s: "
					+ decimalFormat.format(edge.getSpecificityValue()) + "\"];\n");
		} catch (IOException ioe) {
			throw new IllegalStateException("Could not write edge " + edge.getLabel());
		}
	}

	/**
	 * Only used by the GUI to print an answer tree.
	 * @param node
	 * @param answerTree
	 */
	private void print(Node node, AnswerTree answerTree) {//, Query query) {
		Node root = answerTree.root();
		if (printedNodes.contains(node) && !isAnswerTree) { // to ensure each node is only printed once
			return;
		}

		String nodeColor = "\"black\"";


		// TODO: Changes made by Madhu on 13/09/21. Needs to be checked on GUI, if the color appears correctly or not.
		for (int queryIndex = 0; queryIndex < ((Query)obj).size(); queryIndex++) {
			if (!keywordColorMap.containsKey(queryIndex)) {
				keywordColorMap.put(queryIndex, getColorString());
			}

			if (answerTree.matches().get(queryIndex).contains(node)) {
				nodeColor = "\"" + keywordColorMap.get(queryIndex) + "\"";
			}
		}

		try {
			if (node == root) {
				bufferedWriter.write("\"" + node.getId().value().toString() +
						// "\" [ label=< <TABLE> <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\"
						// COLOR=" + nodeColor + "> " + sanitizeString(node.getLabel()) + " (Root)");
						"\" [ label=< <TABLE> <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\" COLOR=" + nodeColor
						+ "> " + node.sanitizeStringLabel() + " (Root)");

			} else {
				bufferedWriter.write("\"" + node.getId().value().toString()
						+ "\" [ label=< <TABLE> <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\" COLOR="
						+ nodeColor + "> " + node.sanitizeStringLabel());
			}
			bufferedWriter.write(" </FONT> </TD> </TR>");
			bufferedWriter.write(" <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\"> " + node.shortID()
			+ "</FONT></TD></TR>\n");
			bufferedWriter.write(" <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\"> "
					+ node.decodeType(node.getType()) + "</FONT></TD></TR>\n");
			if (!node.getId().equals(node.getRepresentative().getId())){
				bufferedWriter.write(" <TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Bold\"> Rep: "
						+ node.getRepresentative().shortID() + "</FONT></TD></TR>\n");
			}
			bufferedWriter.write("</TABLE>> ];\n");
			printedNodes.add(node);
		} catch (IOException IOE) {
			throw new IllegalStateException("Unable to draw node " + node.getId());
		}
	}

}

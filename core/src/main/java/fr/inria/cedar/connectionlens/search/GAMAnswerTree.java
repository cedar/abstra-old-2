/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.search.ProvenanceSignature.Provenance;

public class GAMAnswerTree extends AnswerTree{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = -1192327172234419963L;
	private static long lastTreeNo = 0;
	private long treeNo = 0; 
	
	//private final Provenance prov;
	private final ProvenanceSignature provSign;

	private DetailedProvenance detailedProvenance;
	private TreeSet<Integer> dataSourceSet; 
	
	Logger log = Logger.getLogger(GAMAnswerTree.class); 
	
	private GAMAnswerTree(AnswerTree at, ProvenanceSignature ps) {
		super(at.root(), at.nodes(), at.edges(), 
				at.matches());
		//this.prov = s;
		this.provSign = ps;
		this.dataSourceSet = new TreeSet<Integer>();
		for (Node n: at.nodes()) {
			this.dataSourceSet.add(n.getDataSource().getID());
		}
		this.treeNo = lastTreeNo;
		lastTreeNo ++; 
		//log.info(this.debugString()); 
	}

	//yamen: public constructor that is only called in mergeAll (it will try to save the two children tree that formed this AT in DetailedProvenance)
	public GAMAnswerTree(AnswerTree at, GAMAnswerTree leftChild, GAMAnswerTree rightChild, ProvenanceSignature ps){
		super(at.root(), at.nodes(), at.edges(), 
				at.matches());
		//this.prov = s;
		this.provSign = ps;
		this.detailedProvenance = new DetailedProvenance(leftChild,rightChild);
		this.dataSourceSet = new TreeSet<Integer>();
		for (Node n: at.nodes()) {
			this.dataSourceSet.add(n.getDataSource().getID());
		}
		this.treeNo = lastTreeNo;
		lastTreeNo ++;
		//log.info(this.debugString()); 
	}
	//yamen: public constructor that is called in case of Grow Operation (Except GrowAcross)
	public GAMAnswerTree(AnswerTree at, GAMAnswerTree originalTree, Edge growEdge, ProvenanceSignature ps){
		super(at.root(), at.nodes(), at.edges(), 
				at.matches());
		//this.prov = s;
		this.provSign = ps;
		this.detailedProvenance = new DetailedProvenance(originalTree,growEdge);
		this.dataSourceSet = new TreeSet<Integer>();
		for (Node n: at.nodes()) {
			this.dataSourceSet.add(n.getDataSource().getID());
		}
		this.treeNo = lastTreeNo;
		lastTreeNo ++;
		//log.info(this.debugString()); 
	}
	//yamen: public constructor that is called in case of GrowAcross
	public GAMAnswerTree(AnswerTree at, GAMAnswerTree originalTree, ProvenanceSignature ps){
		super(at.root(), at.nodes(), at.edges(), 
				at.matches());
		//this.prov = s;
		this.provSign = ps;
		this.detailedProvenance = new DetailedProvenance(originalTree);
		this.dataSourceSet = new TreeSet<Integer>();
		for (Node n: at.nodes()) {
			this.dataSourceSet.add(n.getDataSource().getID());
		}
		this.treeNo = lastTreeNo;
		lastTreeNo ++;
		//log.info(this.debugString()); 
	}
	
	public String encodeMatches() {
		//Madhu: changed the string to integer for addressing keywords as index.
		StringBuffer sb = new StringBuffer();
		// sorting the query components for uniformity
		TreeSet<Integer> sortedKwds = new TreeSet<Integer>();
		for (Integer qi : this.matches().keySet()) {
			sortedKwds.add(qi);
		}
		for (Integer s : sortedKwds) {
			sb.append("-" + s);
		}
		return new String(sb);
	}
	
	//yamen: returns the Merge history to know how did we end up having this tree
	// we will use the DetailedProvenance
	public String getMergeHistory(){
		String history = "Tree: " + this.getNo()+" Merge History\n";
		history+=this.getMergeHistoryUtility();
		return history;
	}
	private String getMergeHistoryUtility(){

		String res = "" + this.getNo()+ " was a result of merging: [";
		String leftChildLog = "",rightChildLog = "";
		if(this.detailedProvenance !=null){
			GAMAnswerTree leftChild = this.detailedProvenance.getLeftChildTree();
			GAMAnswerTree rightChild = this.detailedProvenance.getRightChildTree();
			if(leftChild!= null){
				leftChildLog = leftChild.getMergeHistoryUtility();
				res += " leftTree: " + leftChild.getNo();
			}
			if(rightChild!=null){
				rightChildLog = rightChild.getMergeHistoryUtility();
				res += " rightTree: " + rightChild.getNo();
			}
		}
		res += "]\n";
		res += (leftChildLog.equals(""))? "" : leftChildLog+"\n";
		res += (rightChildLog.equals(""))? "" : rightChildLog+"\n";
		return res;
	}
	//yamen: returns the history of how this AT has been created using the DetailedProvenance
	public String getCreationHistory(){
		String history = "Showing Creation history of Tree: " + this.getNo()+ "\n";
		history += getCreationHistoryUtility();
		return history;
	}
	public String getCreationHistoryUtility(){
		String res = "# Tree: " + this.getNo() + " was a result of ";
		if(this.provSign.finalProv.equals(Provenance.INIT)){
			res+=this.decipherProvenance();
		} else {
			Provenance prov =  this.provSign.finalProv;
			res += prov + " ";
			String leftChildLog = "",rightChildLog = "";
			switch(prov){
				case MERGE:
					res+= "[ ";
					GAMAnswerTree leftChild = this.detailedProvenance.getLeftChildTree();
					GAMAnswerTree rightChild = this.detailedProvenance.getRightChildTree();
					if(leftChild!= null){
						leftChildLog = leftChild.getCreationHistoryUtility();
						res += " leftTree: " + leftChild.getNo();
					}
					if(rightChild!=null){
						rightChildLog = rightChild.getCreationHistoryUtility();
						res += " rightTree: " + rightChild.getNo();
					}
					res += " ]\n";
					res += (leftChildLog.equals(""))? "" : leftChildLog+"\n";
					res += (rightChildLog.equals(""))? "" : rightChildLog+"\n";
					return res;
				case GROW:
					res+= "[ ";
					leftChild = this.detailedProvenance.getLeftChildTree();
					Edge growEdge = this.detailedProvenance.getGrowEdge();
					if(leftChild!= null){
						leftChildLog = leftChild.getCreationHistoryUtility();
						res += " original Tree: " + leftChild.getNo();
					}
					res+= ", GrowEdge: " + growEdge;
					res += " ]\n";
					res += (leftChildLog.equals(""))? "" : leftChildLog+"\n";
					return res;
				case GROW_TO_REP:
					res+= "[ ";
					leftChild = this.detailedProvenance.getLeftChildTree();
					if(leftChild!= null){
						leftChildLog = leftChild.getCreationHistoryUtility();
						res += " original Tree: " + leftChild.getNo();
					}
					res += " ]\n";
					res += (leftChildLog.equals(""))? "" : leftChildLog+"\n";
					return res;
			}
		}
		return res;
	}
	public ProvenanceSignature provSign() {
		return this.provSign;
	}

	public static GAMAnswerTree wrap(AnswerTree d, ProvenanceSignature ps) {
		return new GAMAnswerTree(d, ps);
	}

	public static String decipherProvenance(ProvenanceSignature provSign) {
		switch(provSign.finalProv) {
		case INIT:return("init"); 
		case GROW: return("grow"); 
		case MERGE: return("merge");
		case GROW_TO_REP: return("merge");
		case WEAK_SAME_AS: return ("weak"); 
		case MoESP: return ("moesp"); 
		default: return "";
		}
	}

	public String decipherProvenance() {
		return decipherProvenance(provSign); 
	}

	// Debugging Ioana 
	public String debugString() {
		StringBuffer sb = new StringBuffer();
		sb.append("TreeNo:" + this.treeNo + " ");
		sb.append(decipherProvenance(this.provSign) + " "); 
		sb.append("Root: " + this.root.getId() + "\n"); 
		if (edges.size() > 0) {
			for (Edge e: this.edges) {
				sb.append(e.getSourceNode());
				sb.append("-" + e.getLabel() + "->");
				sb.append(e.getTargetNode());
				sb.append("\n");
			}
		}
		else {
			sb.append(((Node)(this.nodes.toArray()[0])).getId().toString());
		}
		String s =  (new String(sb)).replaceAll("NumericNodeID", "");
		//sb.append("tree of size " + this.nodes().size() + " rooted in " + root.getLabel());
		return s; 
	}


	
	/**
	 * Assumes newEdge is adjacent to the root of the tree.
	 * @param newEdge
	 * @return true if: newEdge has one node equal to this tree root, and the other in another
	 * dataset, that already existed in this tree's datasets
	 */
	public boolean datasetLoop(Edge newEdge) {
		Node otherNode = (newEdge.getSourceNode().equals(this.root))?newEdge.getTargetNode():newEdge.getSourceNode(); 
		Integer otherNodeDataset = otherNode.getDataSource().getID(); 
		if (otherNodeDataset.equals(root.getDataSource().getID())){
			return false; // this is local growth
		}
		else { // the other node is in another dataset than this tree's root
			if (this.dataSourceSet.contains(otherNodeDataset)) {
				return true; 
			}
			else {
				return false; 
			}
		}
	}

	public long getNo() {
		return this.treeNo; 
	}
	
	public static long getLastNo() {
		return lastTreeNo; 
	}

	public String labelsOfMatchingNodes() {
		StringBuffer sb = new StringBuffer();
		HashSet<Item> theseMatches = new HashSet<Item>();
		for (Integer qc: this.matches().keySet()) {
			Set<Item> matchingThis = this.matches().get(qc);
			for (Item i: matchingThis) {
				if (!theseMatches.contains(i)) {
					if (sb.length() > 0) {
						sb.append(" | "); 
					}
					//sb.append(i.getLabel().substring(0, Math.min(50, i.getLabel().length())));
					sb.append(i.getLabel());
					theseMatches.add(i);
				}
			}
		}
		return new String(sb);
	}

	public static GAMAnswerTree copyWithNewRoot(GAMAnswerTree at, Node newRoot, ProvenanceSignature ps) {
		return GAMAnswerTree.wrap(new AnswerTree(newRoot, at.nodes(), at.edges(), at.matches()), ps);
	}
	
	/**
	 * This class will store more details about where this GAMAnswerTree Came From
	 * Intitially, we will store:
	 * 1- the two trees that made this tree (if it was a result of a merge)
	 * 2- the tree and edge (if it was a result of Grow)
	 */
	public class DetailedProvenance{

		//left child will be:
		//1- left child, if the AT is a result of a merge.
		//2- the original AT if the AT isa result of a Grow.
		private GAMAnswerTree leftChildTree;
		private GAMAnswerTree rightChildTree;

		//this will only be used to store the edge in case of Grow (except GROWACROSS)
		private Edge growEdge;
		private Provenance prov;

		public DetailedProvenance(GAMAnswerTree leftChildTree, GAMAnswerTree rightChildTree){
			//this constructor will only be called in case of merge.
			this.prov = Provenance.MERGE;
			this.leftChildTree = leftChildTree;
			this.rightChildTree = rightChildTree;
		}
		//this constructor will be called in case of building current tree using Grow (except GROWACROSS)
		public DetailedProvenance(GAMAnswerTree tree, Edge e){
			this.leftChildTree = tree;
			this.growEdge = e;
		}
		//this constructor will be called in case of building current tree using Grow Across
		public DetailedProvenance(GAMAnswerTree tree){
			this.leftChildTree = tree;
		}
		public GAMAnswerTree getLeftChildTree() {
			return leftChildTree;
		}

		public GAMAnswerTree getRightChildTree() {
			return rightChildTree;
		}
		public Provenance getProvenance() { return prov; }
		public Edge getGrowEdge() { return growEdge; }
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.*;
import static java.lang.Math.max;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.Supplier;

import org.json.JSONException;
import org.json.JSONObject;

import edu.stanford.nlp.util.Pair;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * This class represents the edges in the virtual graph
 *  
 * @author redouane, ioana
 */
public class Edge extends Item implements Serializable, Comparable {

	/** Supported item types. */
	public static enum Types implements Item.Types {
		EDGE, 
		SAME_AS_EDGE, 
		COARSE_EDGE, // this is completely unused. I am leaving it here ONLY so that old dumps can be correctly read...
		
		EXTRACTION_EDGE, 
		EXTRACTED_PERSON, 
		EXTRACTED_LOCATION, 
		EXTRACTED_ORGANIZATION, 
		EXTRACTED_FIRSTNAME, 
		EXTRACTED_EMAIL, 
		EXTRACTED_HASHTAG,
		EXTRACTED_DATE,
		EXTRACTED_MENTION,
		
		ORIGINAL_URL, 
		LOCAL_URL
	}
	

	public static String decodeType(Edge.Types t) {
		switch(t) {
			case EDGE: return "edge";
			case SAME_AS_EDGE: return SAME_AS_LABEL; 
			case COARSE_EDGE: return COARSE_LABEL; 
			
			case EXTRACTION_EDGE: return "extract"; 
			case EXTRACTED_PERSON: return EXTRACTED_PERSON_LABEL; 
			case EXTRACTED_LOCATION: return EXTRACTED_LOCATION_LABEL;
			case EXTRACTED_ORGANIZATION: return EXTRACTED_ORGANIZATION_LABEL; 
			case EXTRACTED_FIRSTNAME: return EXTRACTED_FIRSTNAME_LABEL; 
			case EXTRACTED_EMAIL: return EXTRACTED_EMAIL_LABEL; 
			case EXTRACTED_HASHTAG: return EXTRACTED_HASHTAG_LABEL;
			case EXTRACTED_DATE: return EXTRACTED_DATE_LABEL;
			case EXTRACTED_MENTION: return EXTRACTED_MENTION_LABEL;
			
			case ORIGINAL_URL: return ORIGINAL_URL_LABEL;
			case LOCAL_URL: return LOCAL_URL_LABEL; 
			default: throw new IllegalStateException(t + " not covered by case statement");
		}
	}

	/** The label given to strong or weak sameAs edges. */
	public static final String SAME_AS_LABEL = "cl:sameas";

	/** The label given to coarse edges (those connecting a dataset with all its nodes). */
	public static final String COARSE_LABEL = "cl:coarse";
	
	/** The label given to edges that connect a data node with an entity extracted by Stanford NER from it */
	public static final String EXTRACTED_PERSON_LABEL = "extract:p";
	public static final String EXTRACTED_LOCATION_LABEL = "extract:l";
	public static final String EXTRACTED_ORGANIZATION_LABEL = "extract:o";
	public static final String EXTRACTED_FIRSTNAME_LABEL = "extract:f";
	public static final String EXTRACTED_GENERIC_LABEL = "extract:g";
	public static final String EXTRACTED_EMAIL_LABEL = "extract:e";
	public static final String EXTRACTED_HASHTAG_LABEL = "extract:h";
	public static final String EXTRACTED_DATE_LABEL = "extract:d";
	public static final String EXTRACTED_MENTION_LABEL = "extract:m";
	public static final String EXTRACTED_URI_LABEL = "extract:u";


	/** The label given to edges that connect a data source node to its data child	 */
	public static final String ROOT_LABEL = "cl:root";

	/** The label given to the edge that goes from a data source node to its original URI */
	public static final String ORIGINAL_URL_LABEL = "cl:original_uri";

	/** The label given to edges that goes from a data source node to its local URI	 */
	public static final String LOCAL_URL_LABEL = "cl:local_uri";

	/** The label given to edges that connect a relational database to its table children	 */
	public static final String TABLE_EDGE_LABEL = "cl:table";
	/** The label given to edges that connect a relational table to each of its tuple children	 */
	public static final String TUPLE_EDGE_LABEL = "cl:tuple";
	
	/** source node */
	protected final Node source;
	
	/** target node */
	protected final Node target;
	
	/** The confidence score of each edge, processedQuery-independent */
	protected transient final double confidence;
	
	/** The specificity score of each edge, processedQuery-independent */
	protected transient Supplier<Specificity> specificity;

	protected double specificityValue; 
	
	/** edge's type*/
	private  Types edgeType;


	/**
	 * The cached hash value. 
	 * Caching this means that all values the hash is based on must be immutable.
	 */
	private final int hash;
	
	/**
	 * <code>node1</code> and <code>node2</code> should belong to the same data source.  
	 * To avoid edges being considered equal to some node, a nodeID uri shouldn't have "-> " at 
	 * its beginning (seems unlikely this would happen). 
	 * We would like as well node.where to not contain commas, to retrieve each part easily. 
	 * CHANGES: instead of using commas as separator, put node.where between {..} and use regular 
	 * expression to extract it.
	 * This constructor will be used when parsing the documents and registering.
	 * @param origin source node
	 * @param target target node
	 * @param label 
	 * @param c 
	 * @param s 
	 */

	public Edge(EdgeID id, Node origin, Node target, String label, double c, Supplier<Specificity> s) {
		super(id, label, origin.getDataSource());
		this.source = origin;
		this.target = target;
		this.confidence = c;
		this.specificity = s;
		this.specificityValue = 0; 
		this.hash = Objects.hash(id, label);
	}

	public Edge(EdgeID id, Node origin, Node target, String label, double c, double s) {
		super(id, label, origin.getDataSource());
		this.source = origin;
		this.target = target;
		this.confidence = c;
		this.specificity = null;
		this.specificityValue = s; 
		this.hash = Objects.hash(id, label);
	}
	/**
	 *
	 * @param id
	 * @param origin
	 * @param target
	 * @param label
	 * @param c
	 * @param s
	 * @param source
	 */
	public Edge(EdgeID id, Node origin, Node target, String label, double c, Supplier<Specificity> s, DataSource source) {
		super(id, label, source);
		this.source = origin;
		this.target = target;
		this.confidence = c;
		this.specificity = s;
		this.specificityValue = 0; 
		this.hash = Objects.hash(origin,target, label);
	}

	/**
	 * @param n a node
	 * @return true, iff the input node is equal to the edge's source or target.
	 */
	public boolean hasNode(Node n) {
		return this.source.equals(n) || this.getTargetNode().equals(n);
	}

	/**
	 * @return the edge's ID
	 */
	@Override
	public EdgeID getId() {
		return(EdgeID) this.id;
	}
	public void setDataSource (DataSource ds) {
		this.ds = ds;
	}
	public void setLabel (String label) { this.label = label; }


	@Override
	public Types getType() {
		return getEdgeType();
	}


	public Types getEdgeType() {
		return edgeType;
	}

	public void setEdgeType (Types edgeType) {
		this.edgeType = edgeType;
	}

	public double getConfidence() {
		return this.confidence; 
	}

	/**
	 * @return the source node of the edge.
	 */
	public Node getSourceNode() {
		return this.source;
	}
	
	/**
	 * @return the target node of the edge.
	 */
	public Node getTargetNode() {
		return this.target;
	}

	@Override
	public String toString() {
		return source.shortID() + " rep: " + source.getRepresentative().shortID() + " " + this.source.getLabel() + " --" + this.label + "--> " + this.target.shortID() + " rep: " + target.getRepresentative().shortID() + " " +  target.getLabel();
	}
	

	/**
	 * Converts an Edge to a Pair<Edge, Node>
	 *
	 * @param parent some node id that is the source or target of the edge
	 * @return a Pair<Edge, Node> where the node is NOT the one corresponding to parent
	 */
	public Pair<Edge, Node> toPair(NodeID parent) {
		NodeID sourceID = this.getSourceNode().getId();
		Node neighborNode;
		if (sourceID.compareTo(parent) == 0)
			neighborNode = this.getTargetNode();
		else
			neighborNode = this.getSourceNode();
		return new Pair<Edge, Node>(this, neighborNode);
	}
	
	/**
	 * @return true, iff the edge is a SameAs edge.
	 */
	public boolean isSameAs() {
		return getType() == SAME_AS_EDGE;
	}

	/**
	 * @return specificity score of the edge.
	 */
	public Specificity getSpecificity() {
		return specificity == null ?
				new Specificity(getSourceNode().getRepresentative(), getLabel()): specificity.get();
	}

	/**
	 * @return true, iff the edge's a specificity supplier defined.
	 */
	public boolean hasSpecificity() {
		return specificity != null;
	}
	
	/**
	 * Assign a specificity supplier function to an edge.
	 */
	public void setSpecificity(Supplier<Specificity> s) {
		this.specificity = s;
	}
	public void setSpecificityValue(double s) {
		this.specificityValue = s; 
	}
	
	public double getSpecificityValue() {
		if (specificityValue == 0) {
			this.specificityValue = getSpecificity().get(); 
		}
		return specificityValue; 
	}

	@Override
	public boolean equals (Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}

		return (Edge.class.isInstance(o)
				&& this.getSourceNode ().equals (((Edge) o).getSourceNode ())
				&& this.getTargetNode ().equals (((Edge) o).getTargetNode ())
				&& this.getLabel ().equals (((Edge) o).getLabel ())) ||
				(Edge.class.isInstance(o) && this.getId().equals(((Edge) o).getId()));
	}

	@Override
	public int hashCode() {
		return hash;
	}

	/**
	 * @return the JSON format of the node, syntax: <br>
	 * <code>
	 * {
	 * 	"src": json of Node source, 
	 * 	"label": label of this edge, 
	 * 	"dest": json of Node destination 
	 * }
	 * </code>
	 * @throws JSONException 
	 */
	@Override
	public JSONObject serialize() {
		JSONObject result = new JSONObject(); // initialize a new JSON object representing the edge
		
		// extract the label of an edge
		result.put("label", label);

		System.out.println("result=" + result);
		
		return result;
	}
	
	/**
	 * @return the edge's confidence
	 */
	public double confidence() {
		return confidence;
	}

	/**
	 * @return the reverse of the current edge
	 */
	public Edge reverse() {
		return new Edge(getId().reverse(), this.target, this.source, this.label, this.confidence,
				this.specificity);
	}

	public String debugEdge() {
		String s = this.source.getId() + "-" + this.label + 
				"->" + this.target.getId().toString();
		return s.replaceAll("NumericNodeID", "");
	}
	
	/** The association between extracted entity types and the types of the extraction edges. */
	public static Edge.Types decodeExtractedEdgeType(Node target) {
		switch(target.getType()) {
		case ENTITY_PERSON: return EXTRACTED_PERSON;
		case ENTITY_ORGANIZATION: return EXTRACTED_ORGANIZATION;
		case ENTITY_LOCATION: return EXTRACTED_LOCATION;
		case FIRST_NAME: return EXTRACTED_FIRSTNAME;
		case EMAIL: return EXTRACTED_EMAIL;
		case HASHTAG: return EXTRACTED_HASHTAG;
		case DATE: return EXTRACTED_DATE;
		case MENTION: return EXTRACTED_MENTION;
		default: throw new IllegalStateException("Error decoding extracted edge types for node type " + target.getType());
		}
	}
	
	public static String decodeExtractedEdgeLabel(Node target) {
		Edge.Types et = decodeExtractedEdgeType(target); 
		return decodeType(et); 
	}

	/**
	 * Returns true iff an edge points to an extracted entity (only Person, Organisation, Location).
	 * @return true iff an edge points to an extracted entity.
	 */
	public boolean isExtractionEdge() {
		switch(this.getType()) {
			case EXTRACTED_PERSON:
			case EXTRACTED_ORGANIZATION:
			case EXTRACTED_LOCATION:
			case EXTRACTED_DATE:
			case EXTRACTED_EMAIL:
			case EXTRACTED_FIRSTNAME:
			case EXTRACTED_HASHTAG:
			case EXTRACTION_EDGE:
			case EXTRACTED_MENTION:
				return true;
			default:
				return false;
		}
	}

	public static String getExtractionTypes() {
		// (3,4,5,6,7,8,9,10,11,12)
		return EXTRACTION_EDGE.ordinal() + "," + EXTRACTED_PERSON.ordinal() + "," + EXTRACTED_LOCATION.ordinal() + "," +
				EXTRACTED_ORGANIZATION.ordinal() + "," + EXTRACTED_FIRSTNAME.ordinal() + "," + EXTRACTED_EMAIL.ordinal() +
				"," + EXTRACTED_HASHTAG.ordinal() + "," + EXTRACTED_DATE.ordinal() + "," + EXTRACTED_MENTION.ordinal(); 
	}

	/**
	 * Returns true if at least one of the given types corresponds to the type of the edge.
	 * @param types the list of types to be compared with the edge's type.
	 * @return
	 */
	public boolean typeEquals(Edge.Types... types) {
		for(Edge.Types type : types) {
			if(this.edgeType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Edge specificity.
	 */
	public static class Specificity implements Serializable{
		
		/** The representative node. */
		final Node node;
		
		/** The label. */
		final String label;
		
		/** The number of edges incoming to the same target, and sharing the same label. */
		final int in;
		
		/** The number of edges outgoing to the same source, and sharing the same label. */
		final int out;
		
		/**
		 * Instantiates a new specificity.
		 *
		 * @param n the node
		 * @param l the label
		 */
		public Specificity(Node n, String l) {
			this(n, l, 0, 0);
			System.out.println("created a new specificity (" + n + ", " + l + ")");
		}
		
		/**
		 * Instantiates a new specificity.
		 *
		 * @param n the node
		 * @param l the label
		 * @param in the in-degree
		 * @param out the out-degree
		 */
		public Specificity(Node n, String l, int in, int out) {
			super();
			this.node = n;
			this.label = l;
			this.in = in;
			this.out = out;
		}
		
		/**
		 * @return the computed specificity value.
		 */
		public double get() {
			//log.info("Specificity(" + node.id + ", " + label + ", " + in + ", " + out + ")");
			return 2.0 / max(in+out, 2);
		}
		
		/**
		 * @return the number of incoming edges sharing the same label.
		 */
		public int in() {
			return this.in;
		}
		
		/**
		 * @return the number of outgoing edges sharing the same label.
		 */
		public int out() {
			return this.out;
		}
		
		/**
		 * @return the node
		 */
		public Node node() {
			return node;
		}
		
		/**
		 * @return the label
		 */
		public String label() {
			return label;
		}
		
		/**
		 * @return true, iff is undefined both in and out are equal to 0.
		 */
		public boolean isUndefined() {
			return in == 0 && out == 0;
		}

		@Override
		public String toString() {
			return "Specificity(" + node.id + ", " + label + ", " + in + ", " + out + ")";
		}

		@Override
		public int hashCode() {
			return Objects.hash(node, label
//					, in, out
					);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!getClass().isInstance(obj)) {
				return false;
			}
			Specificity other = (Specificity) obj;
			return node.equals(other.node)
					&& label.equals(other.label);
		}
	}
	
	
	/**
	 * Get either the confidence of a same_as edge or the specificity of a normal edge in order to sort them 
	 */
	public Double getSortingScore() {
		if (this.isSameAs())
			return this.getConfidence();
		return this.getSpecificity().get();
	}
	
	@Override
	public int compareTo(Object o) {
		if (this == o) {
			return 0;
		}
		if (o == null) {
			return 1; 
		}
		if (!getClass().isInstance(o)) {
			throw new IllegalStateException("Comparing incomparable objects"); 
		}
		else {
			return this.id.compareTo(((Edge)o).id);
		}
	}

}

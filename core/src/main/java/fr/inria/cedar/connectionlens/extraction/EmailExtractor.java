package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.source.DataSource;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class extracts information, such as emails, uri, hashtag and dates of an input (i.e. a node).
 *
 * @author nellybarret
 */
public class EmailExtractor extends PatternBasedExtractor {

    public EmailExtractor() {
        super();
    }

    public EmailExtractor(Locale locale) {
        super(locale);
    }

    /**
     * Extract emails from nodes.
     * @param ds the node's datasource
     * @param input the content of the node
     * @param structuralContext the context of the node (not used here).
     * @return the collection of extracted emails.
     */
    @Override
    public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
        HashSet<Entity> extractedNodes = new HashSet<>();
        List<String> extractedEmails = this.findEmails(input);
        if(!extractedEmails.isEmpty()) {
            for(String extractedEmail : extractedEmails) {
                Entity entity = new Entity(extractedEmail, extractedEmail, 1.0, 0, Types.EMAIL, ds.getLocalURI().toString());
                extractedNodes.add(entity);
            }
        }

        return extractedNodes;
    }

    /**
     * Find emails, using a regex, in the content of the node
     * @param text the content of the node
     * @return a lit of strings which are the emails found in the content of the node.
     */
    public List<String> findEmails(String text) {
        String pattern = "[a-zA-Z0-9._+-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z]+)+";
        return this.getMatches(pattern, text);
    }

    /**
     * Get the name of the extractor.
     * @return the string containing the name of the extractor.
     */
    @Override
    public String getExtractorName() { return "EmailExtractor"; }

    /**
     * Get the locale.
     * @return the locale.
     */
    @Override
    public Locale getLocale() { return locale; }
}

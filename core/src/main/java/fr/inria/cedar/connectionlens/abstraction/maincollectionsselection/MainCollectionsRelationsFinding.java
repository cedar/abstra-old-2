package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection;

import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.*;

public class MainCollectionsRelationsFinding extends AbstractionTask {
    public static final Logger log = Logger.getLogger(MainCollectionsRelationsFinding.class);

    private HashMap<Integer, ArrayList<String>> leafCollectionValues;

    public MainCollectionsRelationsFinding(Graph graph, DataSource ds) {
        this.graph = (RelationalGraph) graph;
        this.localCollector = new StatisticsCollector();
        this.ds = ds;
        this.isRdf = this.ds.getLocalURI().toString().endsWith(".nt");

        this.leafCollectionValues = new HashMap<>();
    }

    public void run() throws SQLException {
        this.localCollector.start(StatisticsCollector.total());

        // looking for relationships between collections that have been reported
        log.debug("looking for relationships between main entity collections");
        long startTime = System.currentTimeMillis();
        for(int mainEntity1 : CollectionGraph.getInstance().getMainCollections()) {
            for(int mainEntity2 : CollectionGraph.getInstance().getMainCollections()) {
                ArrayList<Path> pathsBetweenMainEntity1AndMainEntity2;
                if(mainEntity1 == mainEntity2 && CollectionGraph.getInstance().checkIfCollectionIsInCycle(mainEntity1)) {
                    // collections that have a loop on themselves should also obtain relations (labelled with the loop labels concatenation)
                    pathsBetweenMainEntity1AndMainEntity2 = CollectionGraph.getInstance().getAllCyclicPathsFromCollection(mainEntity1);
                } else {
                    pathsBetweenMainEntity1AndMainEntity2 = CollectionGraph.getInstance().getAllPathsBetweenCollections(mainEntity1, mainEntity2);
                }
                if(!pathsBetweenMainEntity1AndMainEntity2.isEmpty()) {
                    for(Path path : pathsBetweenMainEntity1AndMainEntity2) {
                        if(!path.containsEntityCollectionsInPath()) {
                            String pathBetweenMainEntity1AndMainEntity2 = path.toStringLabelWithoutFirstAndLast();
                            CollectionGraph.getInstance().addRelationshipsBetweenMainEntityCollections(mainEntity1, mainEntity2, pathBetweenMainEntity1AndMainEntity2);
                            log.debug("added a relation between C" + mainEntity1 + " and C" + mainEntity2 + " as being " + pathBetweenMainEntity1AndMainEntity2);
                        }
                    }
                }
            }
        }
        log.info("found relationships between main entity collections in " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public HashMap<Integer, ArrayList<String>> getLeafCollectionValues() {
        return this.leafCollectionValues;
    }
}

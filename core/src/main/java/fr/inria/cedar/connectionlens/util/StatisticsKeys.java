/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

/**
 * Some keys used by the statistics collector. Unless specified otherwise, all times are in 
 * milliseconds.
 */
public enum StatisticsKeys {

	/** Reason for early termination */
	STOP,

	/** Key for total time */
	TOTAL_T, 

	/** Number of hits out of a greater number of pair comparisons */
	HITS_NO,

	/** Number of pair comparisons */
	CAND_PAIRS_NO,
	
	/** The size of the extraction input (in bytes). */
	INPUT_SIZE,
	
	/** The initialization time. */
	INIT_T,
	
	/** The number of entities. */
	ENT_NO,

	/** The number of person entities. */
	PERS_NO,

	/** The number of organization entities. */
	ORG_NO,

	/** The number of location entities. */
	LOC_NO,
	
	/** The time to flush the extractor cache entries. */
	FLSH_T, 
	
	/** The loading time. */
	STOR_T,
	
	/** The extraction time. */
	EXTR_T,
	
	/** The indexing time. */
	INDEX_T,
	
	/** The logging time. */
	LOG_T,
	
	/** The time spent in finding sameas links. */
	SAMEAS_T,
	
	/** The time spent in building the adjacency map. */
	BUILDMAP_T,
	
	/** The query answering time. */
	ANSWER_T,
	
	/** The number of nodes. */
	NODES_NO,
	
	/** The number of edges. */
	EDGES_NO,
	
	/** The number of answers. */
	ANSW_NO,
	
	/** The time spent finding useful datasets for a query. */
	USEFUL_DS_T,
	
	/** The datasets deemed useful for a query. */
	USEFUL_DS_NO,
	
	/** The time spent by treetagger. */
	TTAGG_T,
	
	/** The number of sameas links. */
	SAMEAS_NO,

    /** The number of distinct representatatives. */
    REP_NO,

	/** The number of weak-sameas links. */
	WSA_NO,

	/** The number of calls to the Jaccard similarity function. */
	JACCARD_NO,
	
	/** The number of calls to the Jaro similarity function. */
	JARO_NO,
	
	/** The number of calls to the Levenshtein similarity function. */
	LEVEN_NO,
	
	/** The number of calls to the Hamming similarity function. */
	HAMMING_NO,

	POST_T,

	/** Catch-all label for time spent in none-specify part of the search algorithm */
	SRC_T,
	
	/** Time spent in interaction with the SQL database */
	SQL_T,

	/** Time spent in minimization */
	MIN_T,

	/** Time spent in merging */
	MRG_T,
	
	/** Time spent computing specificity */
	SPEC_T,
	
	/** Time spent cleaning specificity */
	SPCL_T, 

	/** The time spent in writes due to comparisons (same-as edges) */ 
	SIM_LOAD_T,

	/** The abstraction time */
	REGISTRATION_T,
	NORMALIZATION_TIME,
	SUMMARY_TIME,
	RECORDS_COLL_TIME,
	SEPARATION_TIME,
	CLASSIFICATION_TIME,

	/** NB 19/08/2022 Abstra performance tracking */
	NORM_INSERT_NN_DISK, // 1. insert original nodes in norm_nodes
	NORM_INSERT_NE_DISK, // 2. insert original unlabelled edges in norm_edges
	NORM_INSERT_NORM_TMP_DISK, // 3. insert the original labelled edges into normalization_tmp
	NORM_INSERT_NE_2_DISK, // 4. insert unlabelled edges replacing labelled edges
	NORM_INSERT_NN_2_DISK, // 5. insert nodes replacing labelled edges
	NORM_UPDATE_URI_DISK, // 6. sanitize URIs and labels

	SUMM_RDF_RUN_RDFQUOTIENT_DISK, // 1. run RDFQuotient on the original dataset
	SUMM_RDF_COPY_OUT_RDFQUOTIENT_MEM, // 2. copy RDFQuotient summary into files
	SUMM_RDF_CREATE_TABLES_DISK, // 3. create tables to store RDFQuotient summary (in Abstra)
	SUMM_RDF_COPY_IN_RDFQUOTIENT_DISK, // 4. copy RDFQuotient files into Abstra
	SUMM_RDF_UPDATE_DICTIONARY_DISK, // 5. sanitize RDFQuotient dictionary to have the same labels as in Abstra's nodes
	SUMM_RDF_MAPPING_RDF_CL_IDS_DISK, // 6. create the mapping between RDFQuotient nodes and Abstra nodes based on their label
	SUMM_RDF_ONTOLOGY_TRIPLES_DISK, // 7. store triples that are part of the ontology (not the data) based on their property label
	SUMM_RDF_ONTOLOGY_NODES_DISK, // 8. find nodes that are part of the ontology and that should not be part of the collection graph
	SUMM_RDF_INTERESTING_EDGES_DISK, // 9. get interesting summary (data) edges
	SUMM_RDF_GET_SUMMARY_TYPES_DISK, // 10. get types of collections if exist (useful to compute collection label)

	SUMM_INSERT_COLLECTIONS_NODES_DISK, // 1. insert collections and the nodes they represent + nodes that may miss in case of a partial summary
	SUMM_INSERT_COLLECTIONS_INFORMATION_DISK, // 2. insert collections informations (id, size, is leaf, ...)
	SUMM_INSERT_COLLECTION_GRAPH_DISK, // 3. insert the collection edges into the collection graph
	SUMM_INDEXES_DISK, // 4. build useful indexes

	COLLS_GET_COLLECTION_GRAPH_MEM, // 1. retrieve the collection graph from the dsk into memory
	COLLS_GET_COLLECTIONS_LABELS_MEM, // 2. compute the collections labels
	COLLS_GET_ID_IDREF_EDGES_MEM, // 3. compute ID/IDREF edges
	COLLS_WRITE_COLLECTION_GRAPH_DISK, // 4. write back the collection graph on disk after modifying it for ID/IDREF edges
	COLLS_GET_COLLECTIONS_SIZES_DISK, // 5. compute collections sizes (useful for etf computation)
	COLLS_COMPUTE_ETF_DISK, // 6. compute edge transfer factors
	COLLS_COMPUTE_AT_MOST_ONE_DISK, // 7. compute at-most-one boolean
	COLLS_COMPUTE_ENTITY_PROFILES_DISK, // 8. compute entity profiles
	COLLS_INIT_COLLECTION_GRAPH_OBJECT_MEM, // 9. init collection graph variables in memory - requires few SELECT queries
	COLLS_ENUMERATE_ALL_PATHS_MEM, // 10. enumerate all the paths of the collection graph
	COLLS_RETRIEVE_AND_SET_VARIABLES_MEM, // 11. retrieve computed values in memory (is leaf, etf, at-most-one, entity profiles)
	COLLS_COMPUTE_PTF_MEM, // 12. compute path transfer factors using an aggregator (min, max or sum)

	MAINCOLLS_COMPUTE_COLLS_SCORES_MEM_DISK, // 1. compute initial scores
	MAINCOLLS_CHOOSE_NEXT_COLLECTION_MEM, // 2. choose the next collection to report
	MAINCOLLS_COMPUTE_BOUNDARY_MEM, // 3. compute the boundary of cstar
	MAINCOLLS_COMPUTE_COVERAGE_DISK, // 4. compute the coverage of nodes
	MAINCOLLS_UPDATE_GRAPH_DISK, // 5. update the collection graph to reflect the selection of cstar
	MAINCOLLS_RECOMPUTE_COLLS_SCORES_MEM_DISK, // 6. if needed, recompute the scores
	MAINCOLLS_GET_RELATIONSHIPS_MEM, // 7. get relationships between main collections

	CLASS_INSERT_NODE_AND_EDGES_DISK, // 1. copy norm nodes and norm edges into the classification tables
	CLASS_BUILD_SEMANTIC_PROPERTIES_MEM, // 2. build the set of semantic properties from the JSON resources
	CLASS_INIT_MODEL_MEM, // 3. load Word2Vec model
	CLASS_GET_INTERESTING_COLLECTIONS_DISK, // 4. compute interesting collections to classify
	CLASS_GET_COLLECTIONS_PROPERTIES_MEM, // 5. compute the set of properties (children) for each interesting collection
	CLASS_CLASSIFY_INTERESTING_COLLECTIONS_MEM_DISK, // 6. classify interesting collections by comparing the set of semantic properties and the data properties

	REPORTING_CREATE_DESCRIPTION_MEM, // 1. build the HTML description and the E-R schema

	/** IM 27/11/2020 performance tracing */
	XX_EXTRACT_T, 
	XXX_FLAIR_T, 
	XXX_URL_MGMT_T, 
	XXX_GOBBLE_T,  
	XXX_XTR_CALL_NO,
	XT_FLAIR_T,
	XT_FLAIR_URL_T, 
	XT_FLAIR_GOBBLE_T,  
	XT_CALL_NO,
	XT_GENERICS_T,
	XT_HANDENTITY_T,
	NODE_LOOKUP_T,
	XT_FINALIZE_T,
	XT_FLUSH_T,
	XT_SNER_T, 
	
	/** The disambiguation time */
	DISAMB_T
	; 
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import com.wcohen.ss.Jaro;
import com.wcohen.ss.Levenstein;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.DataSourceNode;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.score.Score.DoubleScore;
import fr.inria.cedar.connectionlens.score.Score.DoubleVectorScore;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.search.Query;

import static com.google.common.base.Preconditions.checkArgument;


/**
 * A scoring function combined the level of matching between the query and the object, as well as
 * connection score (typically <code>AverageConfidenceSpecificity</code>).
 */
public class MatchingScore implements ScoringFunction {

	/** 
	 * String matching tool; use static to avoid recreating too many instances of Jaro and 
	 * Levenstein */
	private static Jaro jaro = new Jaro();
	
	/**
	 * The query for which the score is to be computed.
	 */
	private Query q;
	
	/** The levenshtein function. */
	private static Levenstein leven = new Levenstein();
	
	/**
	 * Instantiates a new match and connection score.
	 */
	public MatchingScore(Query q) {
		this.q = q;
		//System.out.println("Setting query MatchingScore:"+q.toString());
	}
	
	/**
	 * Compute the score of the answer tree, then the result is stored in <code>this.score</code>.
	 * Always call this function before calling <code>this.score</code>
	 */
	@Override
	public DoubleScore compute(Scorable s) {
		checkArgument(s instanceof AnswerTree);
		
		if(q == null) // Madhu: When called directly by GAMSearch's findTrees().
			return new DoubleScore(1.0);
		
		// initialize the score vector of size [num_keyword + 1])
		AnswerTree at = (AnswerTree) s;

		/*
		 * ----------------- 1. Compute component score (score of matching) -----------------
		 */
		// create a 2-d array, uri each line is a keyword, each column is either a node/an edge
		int num_items = at.nodes().size() + at.edges().size();
		int num_keywords = at.matches().size();
		double[][] matrix_score = new double[num_keywords][num_items];
		int node_label_match_length = Config.getInstance().getIntProperty ("node_label_matching_length",20);
		DoubleVectorScore result = new DoubleVectorScore(num_keywords);
		int i_keyword = 0, j_item = 0, threshold = 10;

		// for each keyword, fill in the table
		for (int i=0; i<q.size(); i++) {
			QueryComponent kw = q.getComponents().get(i);
			if(!(at.matches().keySet().contains(i)))
				continue;
			String keyword = kw.toString();

			// for each node
			for (Node node : at.nodes()) {
				if (node instanceof DataSourceNode) {
					continue;
				}
				String labelNode = node.getLabel();

				// partial matching of keyword and node's label
				double meanLength = (labelNode.length() + keyword.length()) / 2;

				double matchingScore = 0;
				if(labelNode.length() > node_label_match_length ){
					matchingScore = labelNode.contains(keyword) ? 1:0;
				}

				// if the mean length of both string >= threshold i.e. 10 --> use Levenstein
				else if (meanLength >= threshold) {
					double levenScore = leven.score(labelNode, keyword);
					matchingScore = 1 - Math.abs(levenScore)
							/ Math.max(labelNode.length(), keyword.length());
				} else { // otherwise, use Jaro for short strings
					matchingScore = jaro.score(labelNode, keyword);
				}

				matrix_score[i_keyword][j_item++] += matchingScore;
			}

			// for each edge
			for (Edge edge : at.edges()) {
				// match edge label to keyword
				if (edge.getLabel().equals(keyword)) {
					matrix_score[i_keyword][j_item++] += 1; // match edge label to keyword
				}
			}

			i_keyword += 1;
			j_item = 0;
		}

		// print out the matrix_score for testing
		for (int i = 0; i < num_keywords; i++) {
			double mean = 0.0;
			for (int j = 0; j < num_items; j++) {
				mean += matrix_score[i][j] / num_items;
			}
			result.set(i, mean);
		}
		return result.mean();
	}
	
	public boolean equals(Object o) {
		if (o instanceof MatchingScore) {
			MatchingScore ws = (MatchingScore) o;
			return true; 
		}
		else {
			return false; 
		}
	}
}

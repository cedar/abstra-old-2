/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import java.util.function.Supplier;

import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * Node encapsulating an entity.
 */
public class EntityOccurrenceNode extends Node {
		
	/** Accepted entity types */
	public static enum Metadata { OFFSET, LENGTH }
	
	/** Offset of the entity of this node */
	private final int offset;
	
	/** Length of the entity of this node */
	private final int length;
	
	/**
	 * Create a new entity occurrence node ID
	 * @param id the unique global ID
	 * @param d data source URI the node comes from
	 */
	public EntityOccurrenceNode(NodeID id, String label, DataSource d, Supplier<Node> rep,
			int offset, int length) {
		super(id, label, d, rep, null);
		this.offset = offset;
		this.length = length;
	}
	
	/**
	 * Create a new entity occurrence node ID
	 * @param id the unique global ID
	 * @param d data source URI the node comes from
	 */
	public EntityOccurrenceNode(NodeID id, String label, String normaLabel, DataSource d, Supplier<Node> rep,
			int offset, int length) {
		super(id, label, normaLabel, d, rep, null);
		this.offset = offset;
		this.length = length;
	}
	/** self-represented */
	public EntityOccurrenceNode(NodeID id, String label, DataSource d, int offset, int length, Node.Types type) {
		super(id, label, d, null);
		this.offset = offset;
		this.length = length;
		this.representative = () -> this; 
		if (type != null) {
			this.nodeType = type; 
		}
	}
	/** self-represented */
	public EntityOccurrenceNode(NodeID id, String label, DataSource d, int offset, int length, String normaLabel) {
		super(id, label, d, null);
		this.offset = offset;
		this.length = length;
		this.representative = () -> this; 
		this.normalizedLabel = normaLabel; 
	}

	/**
	 * Get offset information extracted from the global ID
	 * @return the offset of the field
	 */
	public int getOffset() {
		return this.offset;
	}
	
	/**
	 * Get length information extracted from the global ID
	 * @return the field length
	 */
	public int getLength() {
		return this.length;
	}
	
	/**
	 * Retrieve information from the string <code>uri</code>
	 * @return 0: path to data source, 1: offset, 2: length, 3: entity type
	 */
	public static String[] retrieveInfo(String uri) {
		String[] info = new String[4];
		
		String[] tokens = uri.split("\\|"); // escape because "|" is a special character to use in regexp 
		
		info[0] = tokens[1];
		info[1] = tokens[2].substring("offset".length());
		info[2] = tokens[3].substring("length".length());
		info[3] = tokens[4];
		
		return info;
	}
	@Override
	void specificNormalization() {
		// nop
		//log.info(this.getClass().getSimpleName() + " should not be called");
	}
}

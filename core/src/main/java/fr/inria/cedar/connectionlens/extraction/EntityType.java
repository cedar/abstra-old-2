/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import java.util.BitSet;

/**
 * Types for extracted entities.
 */
public class EntityType {

	/** Supported entity types. */
	public static final BitSet ENTITY_TYPES = new BitSet(Types.values().length);
	static {
		ENTITY_TYPES.set(Types.FIRST_NAME.ordinal());
		ENTITY_TYPES.set(Types.ENTITY_PERSON.ordinal()); 
		ENTITY_TYPES.set(Types.ENTITY_LOCATION.ordinal()); 
		ENTITY_TYPES.set(Types.ENTITY_ORGANIZATION.ordinal());
		ENTITY_TYPES.set(Types.EMAIL.ordinal());
		ENTITY_TYPES.set(Types.HASHTAG.ordinal());
		ENTITY_TYPES.set(Types.DATE.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_EVENT.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_CREATIVE_WORK.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_OTHER.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_PERSON.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_ORGANIZATION.ordinal());
//		ENTITY_TYPES.set(Types.RECORD_LOCATION.ordinal());
		ENTITY_TYPES.set(Types.MENTION.ordinal());
		ENTITY_TYPES.set(Types.NEO4J_ENTITY.ordinal());
		ENTITY_TYPES.set(Types.NEO4J_EDGE.ordinal());
	}

	/** Person sub-types. */
	private final static String[] ARR_PERSON = { 
			"person", "emailaddress", "journalist", // OpenCalais
			"politician", "president", "deputy", "officeholder", // Dandellion
			"PER"
	};
	
	/** Location sub-types. */
	private final static String[] ARR_LOCATION = { 
			"continent", "country", "provinceorstate", "region", "facility", "city", // OpenCalais
			"populatedplace", "place", "uri", "settlement", "state", "territory", "administrativeregion", "region", // Dandellion
			"LOC"
	};
	
	/** Organization sub-types. */
	private final static String[] ARR_ORGANIZATION = { 
			"company", "organization", // OpenCalais
			"newspaper", "organisation", "politicalparty", // Dandellion
			"ORG"
	};
	
	/**
	 * Map the given tag to a predefined set of entities: PERSON, LOCATION, ORGANIZATION, MISC
	 * @param tag
	 */
	public static Types mapToEntity(String tag) {
		if (exists(ARR_PERSON, tag)) {
			return ENTITY_PERSON;
		}
		if (exists(ARR_LOCATION, tag)) {
			return ENTITY_LOCATION;
		}
		if (exists(ARR_ORGANIZATION, tag)) {
			return ENTITY_ORGANIZATION;
		}
		return null;
	}
	
	/**
	 * A simple function used to check whether the given string exists in the pre-defined list of strings
	 * @param arr list of strings
	 * @param s a new string
	 * @return <code>true</code> if s is in arr, <code>false</code> otherwise
	 */
	private static boolean exists(String[] arr, String s) {
		for (int i = 0; i < arr.length; i++) {
			if (s.equalsIgnoreCase(arr[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the given type is an entity type.
	 *
	 * @param t the type to check
	 * @return true, iff t is an entity type
	 */
	public static boolean isEntityType(Types t) {
		return ENTITY_TYPES.get(t.ordinal());
	}


}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql.schema;

import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.AbstractProcessNodesSession;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * A Index session to support concurrent modification on a relational index.
 */
public class PostgresFullTextIndexSession extends AbstractProcessNodesSession {

	/** The index. */
	final PostgresFullTextIndex index;
	
	/**
	 * Instantiates a new relational index session.
	 *
	 * @param i the index
	 */
	protected PostgresFullTextIndexSession(PostgresFullTextIndex i, EntityExtractor ex,
										   MorphoSyntacticAnalyser mas, StatisticsCollector extractorStats, 
										   StatisticsCollector indexStats, Graph g) {
		super(ex, mas, extractorStats, indexStats, g);
		this.index = i;
	}

	@Override
	public void addEdge(Edge e) {
		this.edgeBuffer.add(e);
	}

	@Override
	protected void indexNode(Node n) {
		this.nodeBuffer.add(n);
		//log.info(this.getClass().getName() + " exits index node " + n.getLabel() + " type: " + n.decodeType(n.getType()));
	}
}

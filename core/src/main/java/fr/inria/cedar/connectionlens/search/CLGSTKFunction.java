package fr.inria.cedar.connectionlens.search;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import fr.inria.cedar.connectionlens.graph.Node;

public interface CLGSTKFunction {
	public void prepareInputs(List<Set<Node>> inputs, Integer statsObj) ;
	public void findTrees(Consumer<AnswerTree> processor);
	public void collectStats();
}

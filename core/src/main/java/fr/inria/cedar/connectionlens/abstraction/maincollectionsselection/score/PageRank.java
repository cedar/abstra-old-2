package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;
/*
 * (C) Copyright 2016-2021, by Dimitrios Michail and Contributors.
 *
 * JGraphT : a free Java graph-theory library
 *
 * See the CONTRIBUTORS.md file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the
 * GNU Lesser General Public License v2.1 or later
 * which is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR LGPL-2.1-or-later
 */

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PageRank {
    public static final Logger log = Logger.getLogger(PageRank.class);

    private final HashMap<Integer, ArrayList<CollectionEdge>> collectionGraphWithInvertedEdges;
    private HashMap<Integer, Integer> collectionId2PageRankId;
    private HashMap<Integer, Integer> pageRankId2CollectionId;

    private final int nbVertices;
    private double[][] G;
    private double[][] Gt;
    private double[][] U;
    private double[][] M;
    private double[] v;
    private double[] oldv;
    private final double d; // damping factor
    private final double k; // nb iterations
    private final double tau; // convergence

    /**
     * Create and execute an instance of PageRank.
     */
    public PageRank(HashMap<Integer, ArrayList<CollectionEdge>> collectionGraphWithInvertedEdges, HashMap<Integer, HashMap<Integer, Double>> edgeWeights, Configuration configuration) {
        this.collectionGraphWithInvertedEdges = collectionGraphWithInvertedEdges;
        this.collectionId2PageRankId = new HashMap<>();
        this.pageRankId2CollectionId = new HashMap<>();
        int pageRankId = 0;
        for(ArrayList<CollectionEdge> ces : collectionGraphWithInvertedEdges.values()) {
            for(CollectionEdge ce : ces) {
                if(!this.collectionId2PageRankId.containsKey(ce.getSource())) {
                    this.collectionId2PageRankId.put(ce.getSource(), pageRankId);
                    this.pageRankId2CollectionId.put(pageRankId, ce.getSource());
                    pageRankId++;
                }
                if(!this.collectionId2PageRankId.containsKey(ce.getTarget())) {
                    this.collectionId2PageRankId.put(ce.getTarget(), pageRankId);
                    this.pageRankId2CollectionId.put(pageRankId, ce.getTarget());
                    pageRankId++;
                }
            }
        }
        this.nbVertices = this.collectionId2PageRankId.size();
        this.G = new double[this.nbVertices][this.nbVertices]; // G est la matrice qui contient les probabilités d'arêtes
        this.Gt = new double[this.nbVertices][this.nbVertices]; // Gt est la matrice transposée de G
        this.U = new double[this.nbVertices][this.nbVertices]; // U est la matrice qui ne contient que 1/n
        this.M = new double[this.nbVertices][this.nbVertices]; // M est le résultat de PageRank (à multiplier par v)
        this.v = new double[this.nbVertices]; // v est le vecteur (vertical) qui contient le poids des noeuds, i.e. 1/n
        this.oldv = new double[this.nbVertices]; // oldv est la copie de v à l'itération précédente - utile pour calculer la stabilisation
        this.d = 0.15;
        this.k = 100;
        this.tau = 0.00001;

        // initialisation de G et U
        for(int i = 0 ; i < this.G.length ; i++) {
            this.v[i] = 1d/this.nbVertices;
            for(int j = 0 ; j < this.G[0].length ; j++) {
                int source = this.pageRankId2CollectionId.get(i);
                int target = this.pageRankId2CollectionId.get(j);
                if(edgeWeights.containsKey(source) && edgeWeights.get(source).containsKey(target)) {
                    this.G[i][j] = edgeWeights.get(source).get(target);
                } else {
                    this.G[i][j] = 0.0d;
                }
                this.U[i][j] = 1d/this.nbVertices;
            }
            // pour les lignes "sinks", i.e. toutes les valeurs à 0, on remplace ça par 1/n
            boolean isSink = true;
            for(int j = 0 ; j < this.G[0].length ; j++) {
                if(this.G[i][j] > 0) {
                    isSink = false;
                    break;
                }
            }
            if(isSink) {
                for(int j = 0 ; j < this.G[0].length ; j++) {
                    this.G[i][j] = 1d/this.nbVertices;
                }
            }
        }
    }

    public void run() {
        double maxChange = 1;
        int iteration = 1;

        // on calcule M une 1ère fois
        this.Gt = this.transpose(this.G);
        this.M = this.matricesAddition(this.scalarProduct(1-this.d, this.Gt), this.scalarProduct(this.d, this.U));

        // while there is no stabilization and iterations to do
        while (iteration <= this.k && maxChange >= this.tau) {
            this.oldv = this.copyVector(this.v);
            this.v = this.matricesMultiplication(this.M, this.v);
            maxChange = this.max(this.vectorsDifference(this.v, this.oldv));
            iteration++;
        }
        log.debug("finished in " + (iteration-1) + " iterations with a maxChange of " + maxChange);
    }

    // transpose la matrice donnée
    private double[][] transpose(double[][] m) {
        double[][] mT = new double[m.length][m[0].length];
        for(int i = 0 ; i < m.length ; i++) {
            for(int j = 0 ; j < m[0].length ; j++) {
                mT[i][j] = m[j][i];
            }
        }
        return mT;
    }

    // multiple chaque élément de m par scalar
    private double[][] scalarProduct(double scalar, double[][] m) {
        double[][] mScal = new double[m.length][m[0].length];
        for(int i = 0 ; i < m.length ; i++) {
            for(int j = 0 ; j < m[0].length ; j++) {
                mScal[i][j] = m[i][j] * scalar;
            }
        }
        return mScal;
    }

    // ajoute les deux matrices : chaque element [i][j] de m1 est additionné avec l'element [i][j] de m2
    // on considère que m1 et m2 sont de la même taille
    private double[][] matricesAddition(double[][] m1, double[][] m2) {
        double[][] mSum = new double[m1.length][m1[0].length];
        for(int i = 0 ; i < m1.length ; i++) {
            for(int j = 0 ; j < m1[0].length ; j++) {
                mSum[i][j] = m1[i][j] + m2[i][j];
            }
        }
        return mSum;
    }

    // multiplie un vecteur avec une matrice
    private double[] matricesMultiplication(double[][] mv, double[] v) {
        double[] mMult = new double[mv.length];
        for(int i = 0 ; i < mv.length ; i++) {
            for(int j = 0 ; j < mv[0].length ; j++) {
                mMult[i] += mv[i][j] * v[j];
            }
        }
        return mMult;
    }

    // copie une matrice dans un nouveau tableau
    private double[] copyVector(double[] v) {
        double[] vCopy = new double[v.length];
        for(int i = 0 ; i < v.length ; i++) {
            vCopy[i] = v[i];
        }
        return vCopy;
    }

    // copie une matrice dans un nouveau tableau
    private double[][] copyMatrix(double[][] m) {
        double[][] mCopy = new double[m.length][m[0].length];
        for(int i = 0 ; i < m.length ; i++) {
            for(int j = 0 ; j < m[0].length ; j++) {
                mCopy[i][j] = m[i][j];
            }
        }
        return mCopy;
    }

    // calcule la différence entre deux vecteurs en comparant deux à deux les éléments
    // on suppose que v1 et v2 sont de la même taille
    private double[] vectorsDifference(double[] v1, double[] v2) {
        double[] vDiff = new double[v1.length];
        for(int i = 0 ; i < v1.length ; i++) {
            vDiff[i] = Math.abs(v1[i] - v2[i]);
        }
        return vDiff;
    }

    // retourne la valeur maximale dans le vecteur
    private double max(double[] v) {
        double maxVal = 0.0d;
        for(int i = 0 ; i < v.length ; i++) {
            if(v[i] > maxVal) {
                maxVal = v[i];
            }
        }
        return maxVal;
    }

    private void displayVector(double[] v) {
        log.info(Arrays.toString(v));
    }

    private void displayMatrix(double[][] matrix){
        DecimalFormat df = new DecimalFormat("0.000000");
        int dim = matrix.length;
        for (int i = 0; i < dim; i ++){
            StringBuffer sb = new StringBuffer();
            sb.append("[");
            for (int j = 0; j < dim; j ++){
                sb.append(df.format(matrix[i][j]));
                if (j < dim - 1){
                    sb.append(" ");
                }
                else{
                    sb.append("]");
                }
            }
        }
    }

    // renvoie les scores "user-friendly"
    public HashMap<Integer, Double> getScores() {
        HashMap<Integer, Double> scores = new HashMap<>();
        for(int i = 0 ; i < this.v.length ; i++) {
            scores.put(i, this.v[i]);
        }
        return scores;
    }

    public double getScoreFromPageRankId(int pageRankId) {
        return this.v[pageRankId];
    }

    public double getScoreFromCollectionId(int collectionId) {
        return this.v[this.collectionId2PageRankId.get(collectionId)];
    }

    public HashMap<Integer, ArrayList<CollectionEdge>> getCollectionGraphWithInvertedEdges() {
        return this.collectionGraphWithInvertedEdges;
    }

    public double getD() {
        return this.d;
    }

    public double getK() {
        return this.k;
    }

    public double getTau() {
        return this.tau;
    }

    public double[] getV() {
        return this.v;
    }

    public double[][] getG() {
        return this.G;
    }

    public double[][] getGt() {
        return this.Gt;
    }

    public double[][] getU() {
        return this.U;
    }

    public int getNbVertices() {
        return this.nbVertices;
    }

    public int getCollectionIdFromPageRankId(int pageRankId) {
        return this.pageRankId2CollectionId.get(pageRankId);
    }

    public int getPageRankIdFromCollectionId(int collectionId) { return this.collectionId2PageRankId.get(collectionId); }


}

package fr.inria.cedar.connectionlens.search;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;

/**
 * The priority queue used by {@link GAMSearch}.
 *
 */
public class GAMPriorityQueue {
	/** The logger. */
	private final Logger log = Logger.getLogger(GAMPriorityQueue.class);

	PriorityQueue<QueueEntry> Q;

	GAMSearch gs;
	int size;

	/** The search heuristic for growth. */
	public static final QueuePriority queuePriority = GAMPriorityQueue.QueuePriority
			.valueOf(Config.getInstance().getProperty("gam_heuristics_growth"));

	public GAMPriorityQueue(GAMSearch gs) {
		this.gs = gs;
		initPQ();
	}

	private void initPQ() {
		Comparator<Object> comparator;
		switch (GAMPriorityQueue.queuePriority) {
		case NUMBER_OF_NODES:
			comparator = Comparator.comparingInt(this::getNodesNo);
			break;
		case ROOT_ADJACENT_EDGES:
			comparator = Comparator.comparingInt(a -> gs.getCachedAdjacentEdges(getLastVisitedNode(a)).size());
			break;
		case NUMBER_OF_NODES_ROOT_ADJACENT_EDGES:
			comparator = Comparator.comparingInt(this::getNodesNo)
			.thenComparingInt(a -> gs.getCachedAdjacentEdges(getLastVisitedNode(a)).size());
			break;
		case NODES_MINUS_MATCHES:
			comparator = Comparator.comparingInt(this::getNodesNoMinusMatchesNo);
			break;
		case MATCHES_MINUS_NODES:
			comparator = Comparator.comparingInt(this::getMatchesNoMinusNodesNo);
			break;
		case NODES_MINUS_MATCHES_THEN_FANOUT:
			comparator = Comparator.comparingInt(this::getNodesNoMinusMatchesNo)
			.thenComparingInt(a -> gs.getCachedAdjacentEdges(getLastVisitedNode(a)).size());
			break;
		case MATCHES_MINUS_NODES_THEN_FANOUT:
			comparator = Comparator.comparingInt(this::getMatchesNoMinusNodesNo)
			.thenComparingInt(a -> gs.getCachedAdjacentEdges(getLastVisitedNode(a)).size());
			break;
		case NODES_MINUS_MATCHES_THEN_SPECIFICITY:
			comparator = Comparator.comparing(this::getNodesNoMinusMatchesNo).thenComparing(this::compareSpecificity);
			break;
		case MATCHES_MINUS_NODES_THEN_SPECIFICITY:
			comparator = Comparator.comparing(this::getMatchesNoMinusNodesNo).thenComparing(this::compareSpecificity);
			break;
		case MATCHES_THEN_NODES_THEN_SPECIFICITY:
			comparator = Comparator.comparingInt(this::getMatchesNo).thenComparingInt(this::getNodesNo)
			.thenComparing(this::compareSpecificity);
			break;
		case FIXED_SCORING:
			comparator = Comparator.comparing(this::getScore);
			break;
		case NUM_EDGES:
			comparator = Comparator.comparing(this::getEdgesNo);
			break;
		default:
			throw new IllegalStateException("No such GAM growth heuristic: " + GAMPriorityQueue.queuePriority);
		}
		this.setPQ(new PriorityQueue<>(comparator));
		this.size=0;

	}

	/**
	 * This(old) method is used to basically get a tree's root. It seems to do just
	 * that, but TODO it's worth trying to get rid of it and just asking for the
	 * root...
	 * 
	 * @param object
	 * @return
	 */
	Node getLastVisitedNode(Object object) {
		AnswerTree tree;
		if (object instanceof AnswerTree) {
			tree = (AnswerTree) object;
		} else {
			Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
			tree = pair.getLeft();
		}
		return (Node) tree.nodes().toArray()[tree.nodes().size() - 1];
	}

	public Integer getNodesNoMinusMatchesNo(Object object) {
		Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
		return pair.getLeft().nodes().size() - countMatchesFromTreeAndEdge(pair.getLeft(), pair.getRight());
	}

	public Integer getMatchesNoMinusNodesNo(Object object) {
		Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
		return pair.getLeft().nodes().size() - countMatchesFromTreeAndEdge(pair.getLeft(), pair.getRight());
	}

	public Integer getMatchesNo(Object object) {
		Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
		return ((-1) * countMatchesFromTreeAndEdge(pair.getLeft(), pair.getRight()));
	}

	public Double getScore(Object object) {
		QueueEntry qe = (QueueEntry) object;
		return qe.getScore();
	}

	public Integer getNodesNo(Object object) {
		Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
		return pair.getLeft().nodes().size();
	}

	public Integer getEdgesNo(Object object) {
		Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) object).getPair();
		return pair.getLeft().edges().size();
	}

	/**
	 * @param at
	 * @param e
	 * @return the number of query nodes matched, together, by the tree and the edge
	 */
	private int countMatchesFromTreeAndEdge(AnswerTree at, Edge e) {
		int k = at.matches().size();
		Node source = e.getSourceNode();
		if (!at.nodes.contains(source)) {// maybe this node brings an extra match
			Integer nodesMatchedBySource = gs.getKwdsMatchedBy(source);

			Integer extraMatches = (at.keywordMask & nodesMatchedBySource);
			if(!(extraMatches==0))
				k=k+Integer.bitCount(extraMatches);
		}
		
		Node target = e.getTargetNode();
		if (!at.nodes.contains(target)) {// maybe this node brings an extra match
			Integer nodesMatchedByTarget = gs.getKwdsMatchedBy(target);
			Integer extraMatches = (at.keywordMask & nodesMatchedByTarget);
			if(!(extraMatches==0))
				k=k+Integer.bitCount(extraMatches);
		}
		return k;
	}

	@SuppressWarnings("unchecked")
	/**
	 * @param object1
	 * @param object2
	 * @return 1 if the first specificity is lower, -1 if it is higher
	 */
	public int compareSpecificity(Object object1, Object object2) {
		Pair<GAMAnswerTree, Edge> pair1 = ((QueueEntry) object1).getPair();
		Pair<GAMAnswerTree, Edge> pair2 = ((QueueEntry) object2).getPair();
		if (pair1.getRight().getSpecificityValue() < pair2.getRight().getSpecificityValue())
			return +1;
		if (pair1.getRight().getSpecificityValue() == pair2.getRight().getSpecificityValue())
			return 0;
		return -1;
	}
	/**
	 * The search heuristics.
	 */
	public enum QueuePriority {
		/**
		 * By number of nodes
		 */
		NUMBER_OF_NODES,
		/**
		 * By number of root-adjacent edges
		 */
		ROOT_ADJACENT_EDGES,
		/**
		 * By number of nodes then number of root-adjacent edges
		 */
		NUMBER_OF_NODES_ROOT_ADJACENT_EDGES,
		/**
		 * By number of nodes minus the number of matches
		 */
		NODES_MINUS_MATCHES,
		/**
		 * By number of matches minus the number of nodes
		 */
		MATCHES_MINUS_NODES,
		/**
		 * By number of nodes minus the number of matches then number of root-adjacent
		 * edges
		 */
		NODES_MINUS_MATCHES_THEN_FANOUT,
		/**
		 * By number of matches minus the number of nodes then number of root-adjacent
		 * edges
		 */
		MATCHES_MINUS_NODES_THEN_FANOUT,
		/**
		 * By number of nodes minus the number of matches plus specificity
		 */
		NODES_MINUS_MATCHES_THEN_SPECIFICITY,
		/**
		 * By number of matches minus the number of nodes plus specificity
		 */
		MATCHES_MINUS_NODES_THEN_SPECIFICITY,
		/**
		 * By number of matches (higher is better), then the number of nodes plus
		 * specificity
		 */
		MATCHES_THEN_NODES_THEN_SPECIFICITY,

		/**
		 * A fixed score which is computed just once.
		 */
		FIXED_SCORING, // Added by MM on 4/11/2021
		/**
		 * A fixed score which is computed just once.
		 */
		NUM_EDGES // Added by MM on 15/11/2021
	}

	/** Just used when debugging */
	//@SuppressWarnings({ "unused", "unchecked" }) 
	public void printQueue() {
		log.info("== Queue (" + Q.size() + ") ==");
		Iterator<?> i = Q.iterator();
		while (i.hasNext()) {
			Pair<GAMAnswerTree, Edge> pair = ((QueueEntry) i.next()).getPair();
			log.info(pair.getLeft().toString() + ",  " + pair.getRight().debugEdge());
		}
	}


	public void add(QueueEntry qe) {
		if(GAMPriorityQueue.queuePriority==QueuePriority.FIXED_SCORING)
			qe.setScore(computeScore(qe.getPair()));
		this.Q.add(qe);
		this.size++;
	}

	/**
	 * Used only for FIXED_SCORING to compute the score once per Pair.
	 * @param pair
	 * @return the score of the pair.
	 */
	private double computeScore(Pair<GAMAnswerTree, Edge> pair) {
		return pair.getLeft().nodes().size() - countMatchesFromTreeAndEdge(pair.getLeft(), pair.getRight()) + pair.getRight().getSpecificityValue();
	}

	public boolean isEmpty() {
		return this.Q.isEmpty();
	}

	public QueueEntry poll() {
		this.size--;
		return this.Q.poll();
	}

	public void setPQ(PriorityQueue<QueueEntry> pq) {
		this.Q = pq;
	}

	public QueueEntry peek() {
		return this.Q.peek();
	}

	public int size() {
		return this.size;
	}



}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import com.google.common.collect.Multimap;

import java.util.Locale;
import java.util.Set;

/**
 * High-level interface for morpho-syntactic analysers.
 */
public interface MorphoSyntacticAnalyser {
	
	/**
	 * @param input a piece of text
	 * @return a multimap mapping each token in the input stream to one or more POS.
	 */
	Multimap<String, PartOfSpeech> process(String input);
	
	/**
	 * @return the sets POSs that correspond to entities.
	 */
	Set<String> potentialEntityPOS();
	

	/**
	 * @param pos a POS
	 * @return true, iff this is indexable
	 */
	boolean isIndexablePOS(PartOfSpeech pos);

	Locale getLocale();
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import fr.inria.cedar.connectionlens.graph.sim.Selectors.ConjunctiveSelector;
import fr.inria.cedar.connectionlens.graph.sim.Selectors.DisjunctiveSelector;

public interface NodePairSelector {

	default ConjunctiveSelector and(NodePairSelector other) {
		return new ConjunctiveSelector(this, other);
	}
	default DisjunctiveSelector or(NodePairSelector other) {
		return new DisjunctiveSelector(this, other);
	}
	String printName();
}

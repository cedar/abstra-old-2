/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

/**
 * Scoring function interface.
 */
@FunctionalInterface
public interface ScoringFunction {
	
	/**
	 * Computes a score for the given scorable object.
	 *
	 * @param e the object to compute a score for.
	 * @return the computed score
	 */
	Score compute(Scorable e);
}

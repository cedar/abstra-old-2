/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static com.google.common.collect.Range.greaterThan;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isBoolean;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isEntity;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isURI;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofRelativeLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameNormaLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.EITHER;

import com.wcohen.ss.Jaccard;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class LongStringPairProcessor extends CachedSimilarPairProcessor {

	/** The maximum string length for which to apply this similarity */
	private final int lengthThreshold;

	/** The length to between candidate pairs */
	private final double lengthRatio;

	private final Jaccard sim = new Jaccard();
	
	/** If true, we need to find identical-label strings */
	private final boolean requireEquality; 

	public LongStringPairProcessor(StatisticsCollector stats, double th) {
		this(stats, th,
			Config.getInstance().getIntProperty("short_string_threshold"),
			Config.getInstance().getDoubleProperty("length_difference_ratio"), 
			false);
	}
	public LongStringPairProcessor(StatisticsCollector stats, double th, boolean requireEquality) {
		this(stats, th,
			Config.getInstance().getIntProperty("short_string_threshold"),
			Config.getInstance().getDoubleProperty("length_difference_ratio"), 
			requireEquality);
	}
	
	public LongStringPairProcessor(StatisticsCollector stats, double th, 
			int lengthThreshold, double lengthRatio, boolean requireEquality) {
		super(stats, th);
		this.lengthThreshold = lengthThreshold;
		this.lengthRatio = lengthRatio;
		this.requireEquality=requireEquality;
	}

	@Override
	public Double apply(String a, String b) {
		if (this.requireEquality) { // In this case, the selector already did the job
			return 1.0; 
		}
		return sim.score(a, b);
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result = not(isBoolean(EITHER))
				.and(not(isNumeric(EITHER)))
				.and(not(isURI(EITHER)))
				.and(not(isEntity(EITHER)))
				.and(ofRelativeLength(lengthRatio))
				//.and(commonWords(IndexingModels.valueOf(Config.getInstance().getProperty("indexing_model")) == SQL))
				.and(ofAbsoluteLength(BOTH, greaterThan(lengthThreshold)));
		if(requireEquality) {
			result = sameNormaLabel().and(result); 
		}
		else {
			result = sameStoredPrefix().and(result); 
		}
		return result; 
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	public boolean requireEqual() {
		return requireEquality; 
	}
}

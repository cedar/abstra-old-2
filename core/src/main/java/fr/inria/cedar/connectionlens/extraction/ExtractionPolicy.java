package fr.inria.cedar.connectionlens.extraction;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.source.DataSource;

/**
 *  
 * The class should be used as follows.
 * 
 * 1. First, it should be asked whether the node can be ignored 
 * 
 * 2. Only if has answered "no", it should be asked if one among its policies says that a node
 * should be a Person/Organization/Location.
 * 
 * @author ioanamanolescu
 *
 */
public class ExtractionPolicy {
	HashMap<String, String> noExtractContexts;
	HashMap<String, String> noExtractAllContexts;
	HashMap<String, String> personContexts;
	HashMap<String, String> organizationContexts;
	HashMap<String, String> locationContexts;


	static Logger log = Logger.getLogger(ExtractionPolicy.class);

	public ExtractionPolicy(String s) {
		noExtractContexts = new HashMap<>();
		noExtractAllContexts = new HashMap<>();
		personContexts = new HashMap<>();
		organizationContexts = new HashMap<>();
		locationContexts = new HashMap<>();

		String[] rules = s.split(",");
		for (String rule: rules) {
			String[] components = rule.trim().split(" ");
			if ((components.length > 0) && (components.length != 2)) {
				if (components[0].length() > 0) { // complain if not empty
					log.warn("Ignoring rule of length " + components.length + " (should be 2).");
				}
				continue;  // ignore if incomprehensible
			}
			boolean understood = false;
			if (components[1].toLowerCase().equals("noextract")) {
				//log.info("On path " + components[0] + ": no extraction");
				noExtractContexts.put(components[0],"");
				understood = true; 
			}
			if (components[1].toLowerCase().equals("noextractall")) {
				//log.info("On path " + components[0] + " and below: no extraction");
				noExtractAllContexts.put(components[0],"");
				understood = true; 
			}
			if (components[1].toLowerCase().equals("person")) {
				//log.info("On path " + components[0] + ": Person");
				personContexts.put(components[0],"");
				understood = true; 
			}
			if (components[1].toLowerCase().equals("organization")) {
				//log.info("On path " + components[0] + ": Organization");
				organizationContexts.put(components[0],"");
				understood = true; 
			}
			if (components[1].toLowerCase().equals("location")) {
				//log.info("On path " + components[0] + ": Location");
				locationContexts.put(components[0],"");
				understood = true; 
			}
			if(components[1].toLowerCase().startsWith("{")) {
				// we have a group of entities separated by some delimiter
				String[] pair = components[1].split(":");
				String sep = StringUtils.chop(pair[1].replace("\"",""));
				if(pair[0].toLowerCase().replace("\"","").equals("person")) {
					personContexts.put(components[0], sep);
					understood = true;
				}
				else if(pair[0].toLowerCase().replace("\"","").equals("organization")) {
					organizationContexts.put(components[0], sep);
					understood = true;
				}
				else if(pair[0].toLowerCase().replace("\"","").equals("location")) {
					locationContexts.put(components[0], sep);
					understood = true;
				}
			}

			if (!understood) {
				log.warn("Could not understand rule " + components[0] + " " + components[1]);
			}
		}
		int k = (personContexts.size() + organizationContexts.size() + 
				locationContexts.size() + noExtractContexts.size() + noExtractAllContexts.size());
		if (k > 0) {
			log.info(k + " extraction rules.");
		}
	}

	public boolean isEmpty() {
		return (personContexts.size() == 0 && organizationContexts.size() == 0 &&
				locationContexts.size() == 0 && noExtractContexts.size() == 0); 
	}

	public boolean saysPerson(DataSource ds, int nodeContext) {
		//log.info("Should a node on context " + nodeContext + " be a person for data source with " + personContexts.size() + " person contexts?"); 
		String contextString = ds.getStructuralContext(nodeContext);
		return (personContexts.get(contextString) != null);
	}
	public boolean saysOrganization(DataSource ds, int nodeContext) {
		String contextString = ds.getStructuralContext(nodeContext);
		return (organizationContexts.get(contextString) != null);
	}
	public boolean saysLocation(DataSource ds, int nodeContext) {
		String contextString = ds.getStructuralContext(nodeContext);
		return (locationContexts.get(contextString) != null);
	}
	public boolean saysNoExtract(DataSource ds, int nodeContext) {
		if (nodeContext == PolicyDrivenExtractor.noExtractContext()) {
			return true; 
		}
		String contextString = ds.getStructuralContext(nodeContext);
		//log.info("structural context is: "+contextString);
		return (noExtractContexts.get(contextString) != null);
	}

	public boolean saysNoExtractAll(String path) {
		return (noExtractAllContexts.get(path) != null);
	}

	public String getSeparator(DataSource ds, int structuralContext) {
		String contextString = ds.getStructuralContext(structuralContext);
		return (personContexts.get(contextString));
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;

import org.apache.log4j.Logger;


import java.io.*;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Draws ConnectionLens graphs using DOT. Is only feasible for small(ish)
 * graphs. 
 */
public class GraphPrintingByRepresentative {

	private final BufferedWriter bufferedWriter;
	private final Set<Node> printedNodes;
	private final HashMap<Node, HashSet<Node>> representation;
	private final HashMap<Node, Node> nodeRepresentedBy;
	private String drawingDir;
	private final String dotFileName;
	private final String pngFileName;
	private final String pdfFileName;
	private final String jsonFileName;
	private final Graph.GraphType typeOfGraph;
	

	private final HashMap<Integer, String> datasetToColorMap;
	private final String[] colors = { "RED", "BLUE", "BROWN", "VIOLET", "ORANGE" };
	private int iColor = 0;

	private Graph currentGraph; 
	
	// source to confidence to target
	private final HashMap<Node, HashMap<Node, Double>> printedWeakSameAsEdges;

	public static final Logger log = Logger.getLogger(GraphPrintingByRepresentative.class);

	public GraphPrintingByRepresentative(Graph.GraphType typeOfGraph) {
		this.typeOfGraph = typeOfGraph;
		this.printedNodes = new HashSet<>();
		this.representation = new HashMap<>();
		this.nodeRepresentedBy = new HashMap<>();
		String prefix = "";
		switch(this.typeOfGraph) {
			case NORMALIZED_GRAPH:
				prefix = "normalized";
				break;
			case SUMMARY_GRAPH:
				prefix = "summary";
				break;
			case CLASSIFIED_GRAPH:
				prefix = "classified";
				break;
			case ORIGINAL_GRAPH:
			default:
				prefix = "cl";
				break;
		}
		this.dotFileName = prefix +"_graph.dot"; // TODO NELLY: add dataset name
		this.pngFileName = prefix +"_graph.png";
		this.pdfFileName = prefix +"_graph.pdf";
		this.jsonFileName = prefix +"_graph.json";
		this.datasetToColorMap = new HashMap<>();
		this.printedWeakSameAsEdges = new HashMap<>();
		

		try {
			this.drawingDir = Config.getInstance().getStringProperty("temp_dir");

			this.bufferedWriter = new BufferedWriter(new FileWriter(Paths.get(this.drawingDir, this.dotFileName).toString()));
			this.bufferedWriter.write(
					"digraph g{" + "\n splines=polyline;" + "\n rankdir=\"LR\";" + "\n node[shape=plaintext];\n");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(String.format("Drawing directory folder '%s' not found.", this.drawingDir));
		} catch (IOException e) {
			throw new IllegalStateException("Could not open dot file for printing");

		}

	}

	public GraphPrintingByRepresentative() {
		this(Graph.GraphType.ORIGINAL_GRAPH);
	}

	private String getDatasourceColor(int dsID) {
		if (this.datasetToColorMap.get(dsID) == null) {
			if (this.iColor == this.colors.length) {
				this.iColor = 0;
			}
			this.datasetToColorMap.put(dsID, this.colors[this.iColor]);
			this.iColor++;
		}
		return this.datasetToColorMap.get(dsID);
	}

	public long analyzeNodes(Graph graph, DataSource ds) {
		//log.info("\nAnalyze nodes");
		if (graph.countNodes(ds) > 1000) {
			return 1000;
		}
		for (Node node : graph.getNodes(ds)) {
			Node representativeNode = node.getRepresentative();
			// try a transitive closure here to determine the ultimate representative
			// in case n.rep.rep is not n.rep
			Node representativeNode2 = representativeNode.getRepresentative();
			while (!representativeNode2.equals(representativeNode)) {
				representativeNode2 = representativeNode2.getRepresentative();
				representativeNode = representativeNode.getRepresentative();
			}
			// when we exit, representativeNode equals representativeNode2 and is the
			// ultimate
			// representative of node
			HashSet<Node> repByR = this.representation.get(representativeNode);
			if (!this.representation.containsKey(representativeNode)) {
				repByR = new HashSet<>();
				this.representation.put(representativeNode, repByR);
			}
			repByR.add(node);

			if (!this.nodeRepresentedBy.containsKey(node)) {
				// log.info("Learned that " + representativeNode.getType() + " " +
				// representativeNode
				// + " represents " + node.getType() + " " + node);
				this.nodeRepresentedBy.put(node, representativeNode);
			}
		}
		return graph.countNodes(ds);
	}

	public void printNodes() {
		for (Node representativeNode : this.representation.keySet()) {
			this.printRepresentedCluster(representativeNode);
		}
	}

	public void printEdges(Graph g, DataSource ds) {
		//log.info("\nGraphDOTPrinting edges from: " + ds.getID() + " " + ds.getURI());
		for (Edge edge : g.getEdges(ds)) {
			this.print(edge);
		}
	}

	public void printEdges(Set<Edge> edges) {
		for (Edge e : edges) {
			this.print(e);
		}
	}

	public void closeAndDraw() {
		String pathToDOT = Config.getInstance().getProperty("drawing.dot_installation");

		try {
			this.bufferedWriter.write("}\n");
			this.bufferedWriter.close();
		} catch (IOException ioe) {
			throw new IllegalStateException("Could not close dot file");
		}
		try {
			String dotFilePath = Paths.get(this.drawingDir, this.dotFileName).toString();
			//String pngFilePath = Paths.get(drawingDir, pngFileName).toString();
			String pdfFilePath = Paths.get(this.drawingDir, this.pdfFileName).toString();
			//String jsonFilePath = Paths.get(drawingDir, jsonFileName).toString();
			//String[] pngCommand = { pathToDOT, "-Tpng", dotFilePath, "-o", pngFilePath };
			//Runtime.getRuntime().exec(pngCommand);
			String[] pdfCommand = { pathToDOT, "-Tpdf", dotFilePath, "-o", pdfFilePath };
			Runtime.getRuntime().exec(pdfCommand);
			//log.info(
			//		this.getClass().getSimpleName() + " PDF printing from " + dotFilePath + " to " + pdfFilePath);
			//String[] jsonCommand = { pathToDOT, "-Txdot_json", dotFilePath, "-o", jsonFilePath };
			//Runtime.getRuntime().exec(jsonCommand);
		} catch (IOException e) {
			log.info(
					"Could not process .dot files: install graphviz or check the drawing.dot_installation value in local.settings "
							+ e);
		}
	}

	private void print(Edge edge) {
		if (edge.getLabel().equals(Edge.SAME_AS_LABEL) && edge.confidence() == 1.0) {
			return;
		}
		Node sourceNode = edge.getSourceNode().getRepresentative();
		Node targetNode = edge.getTargetNode().getRepresentative();
	
		// log.info("Printing edge from " + sourceNode.getId().value() + " to " + targetNode.getId().value());
		if (this.nodeRepresentedBy.containsKey(sourceNode)) {
			sourceNode = this.nodeRepresentedBy.get(sourceNode);
		}
		if (this.nodeRepresentedBy.containsKey(targetNode)) {
			targetNode = this.nodeRepresentedBy.get(targetNode);
		}
		if (edge.getLabel().equals(Edge.SAME_AS_LABEL) && edge.confidence() < 1.0) {
			if (this.printedWeakSameAs(sourceNode, targetNode, edge.confidence())) {
				return;
			}
			// else {
			// log.info("Edge is: " + edge.getSourceNode().getId().value() + "
			// " + edge.getTargetNode().getId().value() + " " + edge.confidence());
			// log.info("Drawing it from " + sourceNode.getId().value() + " to
			// " + targetNode.getId().value());
			// }
		}
		// log.info("that is, from " + sourceNode.getId().value() + " to "
		// + targetNode.getId().value());
		try {
			DecimalFormat numberFormat = new DecimalFormat("#.00");
			int edgeNo = (Integer) edge.getId().value();
			this.bufferedWriter.write(sourceNode.getId().value().toString() + " -> "
					+ targetNode.getId().value().toString() + " [label=\"" + edgeNo + " " + edge.getLabel() + " c:"
					+ numberFormat.format(edge.confidence()) + (this.isOriginal() ? " s:" + numberFormat.format(edge.getSpecificityValue()) : " ")
					+ "\""
					+ ((edge.getLabel().equals(Edge.SAME_AS_LABEL) && edge.confidence() < 1.0) ? " style=dashed" : "")
					+ "];\n");
		} catch (IOException ioe) {
			ioe.printStackTrace();
			throw new IllegalStateException("Could not write edge " + edge.getLabel());
		}
	}

	private boolean printedWeakSameAs(Node sourceNode, Node targetNode, double confidence) {
		HashMap<Node, Double> eSource = this.printedWeakSameAsEdges.get(sourceNode);
		if (eSource == null) {
			eSource = new HashMap<>();
			this.printedWeakSameAsEdges.put(sourceNode, eSource);
		}
		for (Node n : eSource.keySet()) {
			if (n.equals(targetNode)) {
				return true;
			}
		}
		// log.info("WSA1: " + sourceNode.getId().value() + " " +
		// targetNode.getId().value() + " " + confidence);
		eSource.put(targetNode, confidence);
		HashMap<Node, Double> eTarget = this.printedWeakSameAsEdges.get(targetNode);
		if (eTarget == null) {
			eTarget = new HashMap<>();
			this.printedWeakSameAsEdges.put(targetNode, eTarget);
		}
		for (Node n : eTarget.keySet()) {
			if (n.equals(sourceNode)) {
				return true;
			}
		}
		eTarget.put(sourceNode, confidence);
		// log.info("WSA2: " + targetNode.getId().value() + " " +
		// sourceNode.getId().value() + " " + confidence);

		return false;
	}

	private void printRepresentedCluster(Node representativeNode) {
		if (this.printedNodes.contains(representativeNode)) {
			return;
		}
		this.printedNodes.add(representativeNode);
		String dsColor = this.getDatasourceColor(representativeNode.getDataSource().getID());
		try {
			this.bufferedWriter.write("\"" + representativeNode.getId().value().toString()
					+ "\" [ label=< <TABLE> <TR><TD><FONT COLOR=\"" + dsColor
					+ "\" POINT-SIZE=\"12.0\" FACE=\"Times-Bold\"> " + representativeNode.sanitizeStringLabel() 
					+ "</FONT></TD></TR> <TR><TD><FONT COLOR=\"" + dsColor + "\" POINT-SIZE=\"12.0\">"
					+ representativeNode.shortID() + " (" + Node.decodeType(representativeNode.getType()) + ")");
			this.bufferedWriter.write("</FONT></TD></TR> ");
			HashSet<Node> constituency = this.representation.get(representativeNode);
			// log.info("Printing a cluster of size " + constituency.size() + " on
			// graphical rep ID: "
			// + representativeNode.getId().value().toString());
			if (constituency.size() > 1) {
				this.bufferedWriter.write(
						"<TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times-Italic\"> also represents </FONT> </TD></TR>\n");
			}
			for (Node n : constituency) {
				String constDSColor = this.getDatasourceColor(n.getDataSource().getID());
				if (representativeNode != n) {
					this.bufferedWriter.write("<TR><TD><FONT POINT-SIZE=\"12.0\" FACE=\"Times\" COLOR=\"" + constDSColor
							+ "\"> " + n.shortID() + " (" + Node.decodeType(n.getType())
							+ ")</FONT></TD></TR>\n");
					this.printedNodes.add(n);
				}
			}
			this.bufferedWriter.write("</TABLE>> ];\n");
		} catch (IOException ioe) {
			throw new IllegalStateException("Unable to draw node " + representativeNode.getId());
		}
	}

	// [IM 9/2/20] At this point, the graph is still partially
	// inconsistent, in particular the representatives have not been fully
	// updated.
	// The graph becomes consistent only after calling spillSameAs which is
	// called by GraphSession.commit().
	//
	// To ensure correct graph drawing, the GraphDotPrinting class "fixes"
	// representatives by itself. This is a bit barbaric and cost half a day
	// to figure out.
	//	

	public void printAllSources(ConnectionLens cl) {
		long totalNodeNumber = 0;

		if(this.isAbstract()){
			this.currentGraph = cl.graph().getAbstractGraph();
		} else if (this.isNormalized()) {
			this.currentGraph = cl.graph().getNormalizedGraph();
		} else if(this.isSummary()) {
			this.currentGraph = cl.graph().getSummaryGraph();
		} else if(this.isClassified()) {
			this.currentGraph = cl.graph().getClassifiedGraph();
		} else {
			this.currentGraph = cl.graph();
		}

		for (DataSource ds : cl.catalog().getDataSources()) {
			totalNodeNumber += this.analyzeNodes(this.currentGraph, ds);
			if (totalNodeNumber >= 1000) {
				//log.info("Balking out of drawing the graph (too many nodes).");
				return;
			}
		}

		// draw the graph nodes in DOT:
		this.printNodes();
		for (DataSource ds : cl.catalog().getDataSources()) {
			// draw the graph edges in DOT:
			this.printEdges(this.currentGraph, ds);
		}
		// also print the sameAs edges (only for original graphs, not for abstraction)
		if(this.isOriginal()) {
			this.printEdges(this.currentGraph.getSameAs());
		}
		// transform the DOT in PNG:
		this.closeAndDraw();
	}

	private boolean isAbstract() {
		return this.typeOfGraph == Graph.GraphType.ABSTRACT_GRAPH;
	}

	private boolean isNormalized() {
		return this.typeOfGraph == Graph.GraphType.NORMALIZED_GRAPH;
	}

	private boolean isSummary() {
		return this.typeOfGraph == Graph.GraphType.SUMMARY_GRAPH;
	}

	private boolean isClassified() {
		return this.typeOfGraph == Graph.GraphType.CLASSIFIED_GRAPH;
	}

	private boolean isOriginal() { return this.typeOfGraph == Graph.GraphType.ORIGINAL_GRAPH; }

	private boolean isNotOriginal() { return this.typeOfGraph != Graph.GraphType.ABSTRACT_GRAPH && this.typeOfGraph != Graph.GraphType.NORMALIZED_GRAPH
			&& this.typeOfGraph != Graph.GraphType.SUMMARY_GRAPH && this.typeOfGraph != Graph.GraphType.CLASSIFIED_GRAPH; }

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import java.util.Objects;

/**
 * This class represents an entity extracted by TreeTagger, 
 * including the value of the token, the POS tag, and the offset of the token. 
 * 
 * For instance, a processed "Emmanuel Macron" will return 2 entries: 
 * ("Emmanuel", "NAM", 0) and ("Macron", "NAM", 9) 
 *  
 * @author Mihu
 */
public class PartOfSpeech implements Token {

	/** The value of the token */
	private String value;
	
	/** The original, unstemmed string*/
	private String original;
	
	/** The POS tag */
	private String tag;
	
	/** The offset of the token */
	private long offset;
	
	/** The length of the token. 
	 *  Attention: the length here is the length of the original token, not that of the stemmed one*/
	private int length;
	
	/**
	 * Create a new TreeTagger entry
	 * @param token the value of the token
	 * @param tag the POS tag of the token
	 * @param offset the offset of the token in the original string input
	 */
	public PartOfSpeech(String token, String original, String tag, long offset) {
		this.value = token;
		this.original = original;
		this.tag = tag;
		this.offset = offset;
		this.length = original.length();
	}
	
	/**
	 * Get the value of the token
	 * @return the string value of the token
	 */
	@Override
	public String value() { 
		return this.value; 
	}
	
	/**
	 * Get the original string of the token(unstemmed)
	 * @return the original value of the token
	 */
	public String getOriginal() {
		return this.original;
	}
	
	/**
	 * Get the POS tag of the token
	 * @return the POS tag e.g. "NOM", "NAM", "ABR"
	 */
	public String getTag() {
		return this.tag;
	}
	
	/**
	 * Get the offset of the token
	 * @return the offset of the token in the original input
	 */
	@Override
	public long offset() {
		return this.offset;
	}
	
	/**
	 * Get the length of the original token (not the stemmed one)
	 * @return the length of the original token
	 */
	@Override
	public int length() {
		return this.length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PartOfSpeech)) return false;
		PartOfSpeech entry = (PartOfSpeech)o;
		return entry.offset == this.offset && entry.value.equals(this.value) && entry.tag.equals(this.tag) && entry.length == this.length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaggerEntry: " + this.value + " (" + this.original + "), " + this.tag + ", offset = " + this.offset + ", length = " + this.length;
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(this.value, this.original, this.tag, this.offset, this.length);
	}

}

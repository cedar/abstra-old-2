package fr.inria.cedar.connectionlens.util;

import java.io.*;
import java.util.Random; 

/**
 * Added to help Lucas test some optimizations
 * @author ioanamanolescu
 *
 */
public class SyntheticXMLGenerator {
	String tagPrefix = "elem";
	String keyword1 = "alpha"; 
	String keyword2 = "beta";
	// the file will be generated under this directory and automatically named
	String pathPrefix = "/tmp"; 
	Random r = new Random();
	
	SyntheticXMLGenerator(){
		
	}
	String generateTagAtLevel(int n) {
		return (tagPrefix + n); 
	}
	
	int largestLeafNumber = 0;
	
	/**
	 * Generates a tree which has a root, that has numberOfTrees full binary subtrees.
	 * Thus, there are numberOfTrees*2^(fullTreeDepth-1) leaves.
	 * Of these, the first noMatchesFor1 are labeled alpha, the last noMatchesFor2 are labeled beta, and the others
	 * have different, three-character labels (thus, neither alpha nor beta).
	 * Written for Luca Maia Morais' tests
	 * 
	 * @param noMatchesFor1
	 * @param noMatchesFor2
	 * @param fullTreeDepth
	 * @param numberOfTrees
	 * @param bos
	 */
	
	void generateXMLTree(int noMatchesFor1, int noMatchesFor2, int fullTreeDepth, int numberOfTrees) {
		try {
			int numberOfLeaves = (int) (numberOfTrees * Math.pow(2, fullTreeDepth-1)); 
			System.out.println("Number of leaves: " + numberOfLeaves); 
			
			BufferedWriter bos = new BufferedWriter(new FileWriter(pathPrefix + "/" + numberOfLeaves + 
					"-leaves-"+ noMatchesFor1 + "-" + noMatchesFor2 + ".xml"));
			bos.write("<" + generateTagAtLevel(0) + ">\n"  );
			for (int i = 0; i < numberOfTrees; i ++) {
				// generate here a full binary tree
				generateFullBinaryTree(noMatchesFor1, noMatchesFor2, fullTreeDepth, numberOfLeaves, 1, bos);
			}
			bos.write("</" + generateTagAtLevel(0) + ">\n"  );
			bos.close();
		}
		catch(Exception e) {
			throw new IllegalStateException("Could not open");
		}
		
	}
	
	/** Part of the stuff written for Luca Maia Morais' tests*/
	private void generateFullBinaryTree(int noMatchesFor1, int noMatchesFor2, int fullTreeDepth, int numberOfLeaves, int currentDepth, BufferedWriter bos) {
		try {
		bos.write("<" + generateTagAtLevel(currentDepth) + ">"  );
		
		if (currentDepth == fullTreeDepth) {
			int nextLeafNumber = largestLeafNumber; 
			//System.out.println("Leaf number " + nextLeafNumber + "/" + numberOfLeaves); 
			if (nextLeafNumber < noMatchesFor1) {
				bos.write(keyword1);
			}
			else {
				if (nextLeafNumber >= numberOfLeaves - noMatchesFor2) {
					bos.write(keyword2); 
				}
				else {
					bos.write(getRandomLeaf()); 
				}
			}
			largestLeafNumber ++;
		}
		else {
			generateFullBinaryTree(noMatchesFor1, noMatchesFor2, fullTreeDepth, numberOfLeaves, currentDepth+1, bos);
			generateFullBinaryTree(noMatchesFor1, noMatchesFor2, fullTreeDepth, numberOfLeaves, currentDepth+1, bos);
			
		}
		bos.write("</" + generateTagAtLevel(currentDepth) + ">\n"  );
		}
		catch(Exception e) {
			throw new IllegalStateException("Could not open"); 
		}
	}

	/** Part of the stuff written for Luca Maia Morais' tests*/
	private String getRandomLeaf() {
		char c1 = (char) ('a' + (24 * r.nextDouble())); 
		char c2 = (char) ('a' + (24 * r.nextDouble())); 
		char c3 = (char) ('a' + (24 * r.nextDouble())); 
		return new String("" + c1 + c2 + c3); 
	}

	
	/**
	 * Generates a synthetic XML document as a set of transactions
	 * @param transactionNo How many transactions
	 * @param itemsNo Maximum number of items (they will all be distinct, i.e., no transaction will have an item twice)
	 *        The items will be named a1, a2, ... aitemsNo
	 * @param compulsoryItemRatio The first items will be compulsory (all transactions have them). This coefficient between 0 and 1 says how many of the items are compulsory.
	 * @param optionalItemLikelihood The other items will be added if a random number is above this probability threshold.
	 */
	public void generateTransactions(int transactionNo, int itemsNo, double compulsoryItemRatio, double optionalItemLikelihood) {
		try {
			
			BufferedWriter bos = new BufferedWriter(new FileWriter(pathPrefix + "/" + transactionNo + 
					"-"+ itemsNo  + ".xml"));
			bos.write("<root>\n"  );
			for (int i = 0; i < transactionNo; i ++) {
				// generate here a full binary tree
				generateTransaction(itemsNo,  bos, compulsoryItemRatio, optionalItemLikelihood);
			}
			bos.write("</root>\n"  );
			bos.close();
		}
		catch(Exception e) {
			throw new IllegalStateException("Could not open");
		}
	}
	
	
	private void generateTransaction(int itemsNo, BufferedWriter bos, double compulsoryItemRatio, double optionalItemLikelihood) {
		try {
			bos.write("<trans>");
			for (int i = 0; i < (int)itemsNo*compulsoryItemRatio; i ++) {
				bos.write("<a" + i + "> </a"+i+">"); 
			}
			for (int i = (int)(itemsNo*compulsoryItemRatio)+1; i<= itemsNo; i++) {
				if (r.nextDouble() > optionalItemLikelihood) {
					bos.write("<a" + i + "> </a"+i+">"); 
				}
			}
			bos.write("</trans>\n"); 
		}
		catch(Exception e) {
			throw new IllegalStateException("Could not open"); 
		}
			
	}
	public static void main(String[] args) {
		SyntheticXMLGenerator sxg = new SyntheticXMLGenerator();
		// use with extraction=NONE and value_atomicity=PER_INSTANCE
		//sxg.generateXMLTree(1, 1000, 2, 100000); 
		
		sxg.generateTransactions(20, 8, 0.6, 0.5); 
	}
}

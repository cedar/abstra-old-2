/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds;

/**
 * StatisticsLogger, without the heavy lifting of Log4J or java.util.logging
 *
 * @author Julien Leblay
 */
public class StatisticsLogger implements AutoCloseable, Observer {


	private static final Logger log = Logger.getLogger(StatisticsLogger.class);

	/**
	 * Output formats.
	 */
	public enum OutputFormats {
		DEFAULT, MARKDOWN, LATEX;
	}

	/** The output print stream. */
	private final PrintStream out;

	/** The outputs. */
	private final Map<Kinds, StatisticsCollector> statCollectors = new LinkedHashMap<>();

	/** The command line properties. */
	private final Properties commandLine;

	/** Whether the prolog has already been printed or not. */
	private boolean hasPrintedProlog = false;

	/** Whether the prolog has already been printed or not. */
	private final OutputFormats format;

	/** The global start time. */
	private final long globalStart = System.currentTimeMillis();

	/** The default value for printing out missing values. */
	private static final String MISSING_VALUE = "N/A";

	/** The last printed header. */
	private Set<String> lastHeader = Sets.newLinkedHashSet();

	/**
	 * Instantiates a new logger.
	 *
	 */
	public StatisticsLogger() {
		this(System.out, new Properties(), OutputFormats.DEFAULT);
	}

	/**
	 * Instantiates a new logger.
	 *
	 */
	public StatisticsLogger(Properties cmdLine) {
		this(System.out, cmdLine, OutputFormats.DEFAULT);
	}

	/**
	 * Instantiates a new logger.
	 *
	 * @param out the output print stream
	 */
	public StatisticsLogger(PrintStream out, Properties cmdLine, OutputFormats format) {
		this.out = out;
		this.format = format;
		this.commandLine = cmdLine;
	}

	public void putStatisticsCollector(Kinds key, StatisticsCollector stats) {
		this.statCollectors.put(key, stats);
		stats.setKey(key); 
		stats.addObserver(this);
	}

	public StatisticsCollector getStatisticsCollector(Kinds key) {
		return this.statCollectors.get(key);
	}

	private String getCommentStart() {
		switch(this.format) {
			case LATEX:
				return "";
			case MARKDOWN:
				return "<!-- ";
			default:
				return "";
		}
	}

	private String getCommentPrefix() {
		switch(this.format) {
			case LATEX:
				return "%";
			case MARKDOWN:
				return "  ";
			default:
				return "#";
		}
	}

	private String getCommentEnd() {
		switch(this.format) {
			case LATEX:
				return "";
			case MARKDOWN:
				return " -->";
			default:
				return "";
		}
	}

	/**
	 * Prints the build info.
	 *
	 */
	private void printBuildInfo() {
		String commentOpen = getCommentStart();
		String commentPrefix = getCommentPrefix();
		String commentClose = getCommentEnd();
		Properties properties = Experiment.getBuildProperties();
		StringBuilder result = new StringBuilder();
		result.append(commentOpen).append('\n')
				.append(commentPrefix).append(" Compile-time properties").append('\n')
				.append(commentPrefix).append('\n')
				.append(commentPrefix).append(" Module: ").append(properties.getOrDefault("module", "UNKNOWN")).append('\n')
				.append(commentPrefix).append(" Version: ").append(properties.getOrDefault("version", "UNKNOWN")).append('\n')
				.append(commentPrefix).append(" Branch: ").append(properties.getOrDefault("branch", "UNKNOWN")).append('\n')
				.append(commentPrefix).append(" ChangeSet: ").append(properties.getOrDefault("changeset", "UNKNOWN")).append('\n')
				.append(commentPrefix).append(" Build: ").append(properties.getOrDefault("build", "UNKNOWN")).append('\n')
				.append(commentClose)
		;
		out.println(result.toString());
	}

	/**
	 * Prints some system environment information to the given output.
	 *
	 */
	private void printVersionInfo() {
		String commentOpen = getCommentStart();
		String commentPrefix = getCommentPrefix();
		String commentClose = getCommentEnd();
		StringBuilder result = new StringBuilder();
		result.append(commentOpen).append('\n')
				.append(commentPrefix).append(" Version").append('\n')
				.append(commentPrefix).append('\n');
		Runtime runtime = Runtime.getRuntime();
		try {
			Process process = runtime.exec("git rev-parse --abbrev-ref HEAD");
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
				result.append('\n').append(commentPrefix).append(" Branch: ").append(reader.readLine());
			}
			process = runtime.exec("git rev-parse HEAD");
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
				result.append('\n').append(commentPrefix).append(" ChangeSet: ").append(reader.readLine());
			}
		} catch (IOException e) {
			result.append('\n').append(commentPrefix).append(" Could not retrieve version information");
		}
		result.append(commentClose).append('\n');
		out.println(result.toString());
	}

	/**
	 * Prints some system environment information to the given output.
	 *
	 */
	private void printSystemSettings() {
		String commentOpen = getCommentStart();
		String commentPrefix = getCommentPrefix();
		String commentClose = getCommentEnd();
		StringBuilder result = new StringBuilder();
		result.append(commentOpen).append('\n')
				.append(commentPrefix).append(" System").append('\n')
				.append(commentPrefix);
		result.append('\n').append(commentPrefix).append(" StartTime: ").append(new Date(System.currentTimeMillis()));
		result.append('\n').append(commentPrefix).append(" OS: ").append(System.getProperty("os.name"));
		result.append('\n').append(commentPrefix).append(" OSArch: ").append(System.getProperty("os.arch"));
		result.append('\n').append(commentPrefix).append(" OSVersion: ").append(System.getProperty("os.version"));
		result.append('\n').append(commentPrefix).append(" StartUpMem: ").append((Runtime.getRuntime().totalMemory() / 1000000) + "MB");
		result.append('\n').append(commentPrefix).append(" StartUpMaxMem: ").append((Runtime.getRuntime().maxMemory() / 1000000) + "MB");
		result.append('\n').append(commentPrefix).append(" NumCPUs: ").append(Runtime.getRuntime().availableProcessors());
		result.append('\n').append(commentClose);
		out.println(result.toString());
	}

	/**
	 * Prints the experiment's input parameters to the given output.
	 *
	 */
	private void printConfig() {
		String commentOpen = getCommentStart();
		String commentPrefix = getCommentPrefix();
		String commentClose = getCommentEnd();
		Config config = Config.getInstance();
		StringBuilder result = new StringBuilder();
		result.append(commentOpen).append('\n')
				.append(commentPrefix).append(" Config").append('\n')
				.append(commentPrefix);
		for (String key: Config.getInstance().keySet()) {
			result.append('\n').append(commentPrefix).append(" ")
					.append(key).append('=').append(config.getProperty(key));
		}
		result.append('\n').append(commentClose);
		out.println(result.toString());
		log.info(result.toString());
	}

	/**
	 * Prints the experiment's command line input parameters to the given output.
	 *
	 * @param config
	 */
	private void printCommandLineParameters(Properties config) {
		String commentOpen = getCommentStart();
		String commentPrefix = getCommentPrefix();
		String commentClose = getCommentEnd();
		StringBuilder result = new StringBuilder();
		result.append(commentOpen).append('\n')
				.append(commentPrefix).append(" Command line params").append('\n')
				.append(commentPrefix);
		for (Object key: config.keySet()) {
			result.append('\n').append(commentPrefix).append(" ")
					.append(key).append('=').append(config.get(key));
		}
		result.append('\n').append(commentClose);
		out.println(result.toString());
		// log.info(result.toString());
	}

	/**
	 * Logs a message with the given verbosity level.
	 *
	 */
	public void printProlog() {
		printBuildInfo();
		printSystemSettings();
		printConfig();
		printCommandLineParameters(this.commandLine);
	}

	/**
	 * Logs a message with the given verbosity level.
	 *
	 */
	public void printHeader(StatisticsCollector stats) {
		String prefix = "";
		String sep = "";
		String suffix  = "";
		switch(this.format) {
			case MARKDOWN:
				prefix = "|";
				sep = " | ";
				suffix = " |\n|";
				// for (int i = 0; i < stats.cells().columnKeySet().size() + 1; i++) {
				// suffix += " --- |";
				// }

				// add the correct number of hyphen (to trace header) given the length of keys in the header
				suffix += StringUtils.repeat(" ", "INPUT".length()) + " | ";
				for (String s: sortHeader(stats)) {
					suffix += StringUtils.repeat("-", s.length()) + " | ";
				}
				break;
			case LATEX:
				prefix = "~\\\\\\scalebox{0.78}{\\begin{tabular}{p{4cm}|";
				if(stats.cells().columnKeySet().contains ("SAMEAS_T") &&
						stats.cells().columnKeySet().contains ("TOTAL_T")) {
					stats.cells().columnKeySet().remove ("TOTAL_T");
				}
				for (int i = 0; i < stats.cells().columnKeySet().size() ; i++) {
					prefix += "r|";
				}
				prefix += "}\n";

				sep = " & ";
				suffix = "\\\\\\hline";
				break;
			default:
				sep = "\t";
		}
		StringBuilder result = new StringBuilder(prefix);
		result.append(" INPUT");
		for (String s: sortHeader(stats)) {
			if(!(sortHeader(stats).contains ("SAMEAS_T") && s.equals ("TOTAL_T"))) {
				if (this.format == OutputFormats.LATEX) {
					s = s.replace ("_", "\\_");
				}
				result.append(sep).append(s);
			}
		}
		result.append(suffix);
		out.println(result.toString());

	}

	/**
	 * Sorts the header in alphanumeric order.
	 *
	 * @return the set of header string sorted
	 */
	private Set<String> sortHeader(StatisticsCollector stats) {
		return new TreeSet<>(stats.cells().columnKeySet());
	}

	/**
	 * Prints the given row.
	 *
	 * @param key the name of the row to print
	 */
	public void printRow(StatisticsCollector stats, Object key) {
		checkArgument(stats.cells().containsRow(key), "Invalid statistics row: " + key);
		Map<String, Object> row = stats.cells().row(key);
		String prefix = "";
		String sep = "\t";
		String suffix  = "";
		switch(this.format) {
			case MARKDOWN:
				prefix = "| ";
				sep = " | ";
				suffix = " |";
				break;
			case LATEX:
				sep = " & ";
				suffix = "\\\\";
				break;
		}
		StringBuilder result = new StringBuilder(prefix);
		String fileName = getShortName(key.toString());
		//result.append(key);
		if (this.format == OutputFormats.LATEX) {
			fileName = fileName.replace("_", "\\_");
		}
		result.append(fileName);
		for (String col: sortHeader(stats)) {
			if(!(sortHeader(stats).contains ("SAMEAS_T") && col.equals("TOTAL_T"))) {
				Object value = row.getOrDefault(col, MISSING_VALUE);
				String spaces = "";
				if(col.length() > value.toString().length()) {
					spaces = StringUtils.repeat(" ", col.length()-value.toString().length()); // create spaces to fulfill the column space
				}
				result.append(sep).append(value).append(spaces);
			}
		}
		result.append(suffix);
		out.println(result);
	}

	/**
	 * Prints the all rows.
	 */
	public void printAllRows(StatisticsCollector stats) {
		// Ensuring the column order is preserve across lines.
		Set<String> cols = sortHeader(stats);
		for (Map.Entry<Object, Map<String, Object>> entry: stats.cells().rowMap().entrySet()) {
			printRow(stats, entry.getKey());
		}
		if (this.format == OutputFormats.LATEX) {
			out.println("\\end{tabular}}");
		}
	}

	/**
	 * @return the output stream of this logger
	 */
	public PrintStream out() {
		return out;
	}

	@Override
	public void close() {
		out.println("Total time: " + (System.currentTimeMillis() - globalStart) + " ms.");
		if (this.out != System.out) {
			this.out.close();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!hasPrintedProlog && !Experiment.runAbstraction) {
			// we do not want to print prolog during abstraction statistics reporting
			printProlog();
			hasPrintedProlog = true;
		}
		StatisticsCollector stats = (StatisticsCollector) o;
		Set<String> header = stats.cells().columnKeySet();
		if (!header.equals(lastHeader)) {
			lastHeader = Sets.newLinkedHashSet(header);
			printHeader(stats);
		}
		if (arg != null) {
			printRow(stats, arg);
		} else {
			printAllRows(stats);
		}
	}

	/**
	 *
	 * @param path
	 * @return name of the file without the whole path
	 */
	String getShortName(String path){
		String fileName = new File(path).getName();
		if(fileName.endsWith(")")){
			fileName = fileName.substring(0,fileName.length()-1);
		}
		return fileName;
	}

	public static StatisticsLogger mute() {
		return new SilentStatisticsLogger();
	}

	public static class SilentStatisticsLogger extends StatisticsLogger {

		public void putStatisticsCollector(Kinds key, StatisticsCollector stats) {
			/*NOOP*/
		}

		public StatisticsCollector getStatisticsCollector(Kinds key) {
			return StatisticsCollector.mute();
		}
	}
}

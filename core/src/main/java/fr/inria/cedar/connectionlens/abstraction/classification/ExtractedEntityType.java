package fr.inria.cedar.connectionlens.abstraction.classification;

import java.util.Objects;

public class ExtractedEntityType implements Comparable<Object> {
    private static int idCounter = 0;
    int id;
    String label;

    ExtractedEntityType(int id, String label) {
        this.id = id;
        this.label = label;
        idCounter = this.id;
        idCounter++;
    }

    ExtractedEntityType(String label) {
        this.id = idCounter;
        this.label = label;
        idCounter++;
    }

    public int ordinal() {
        return this.id;
    }



    @Override
    public String toString() {
        return "ExtractedEntityType{" +
                "id=" + id +
                ", label='" + label + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        ExtractedEntityType that = (ExtractedEntityType) o;
        return this.id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.label);
    }

    @Override
    public int compareTo(Object o) {
        return Integer.compare(this.id, ((ExtractedEntityType) o).id);
    }
}

package fr.inria.cedar.connectionlens.abstraction.classification;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.*;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.fasttext.FastText;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.json.simple.parser.ParseException;


import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.*;


/**
 * This class implements the classification process in CL. Concretely it will classify each node (which is eligible
 * to classification) into one of these six categories: Person, Organisation, Location, Event, Creative Work and Other.
 *
 * @author nellybarret
 */
public class Classification extends AbstractionTask {
    public static final Logger log = Logger.getLogger(Classification.class);

    private final double MINIMAL_SIMILARITY_SCORE = (double) Config.getInstance().getDoubleProperty("SIM_MIN"); // The hint should be at least 80% similar with the compared node to be selected
    private final double MINIMAL_SCORE_CATEGORY = (double) Config.getInstance().getDoubleProperty("SCORE_CAT_MIN"); // We should be confident at 30% at least to say that a collection is about a given category, else OTHER

    private Word2Vec word2VecModel;
    private FastText fastTextModel;
    private Locale locale; // need the locale to wisely choose the list of stop words (according to the language)
    private List<SemanticProperty> semanticPropertiesSet;
    private HashMap<Integer, ArrayList<String>> collectionIdToNodeLabels; // TODO NELLY: do we have that already in the collection graph?
    private HashMap<Integer, ArrayList<String>> collectionIdToProperties;
    private HashMap<Integer, ArrayList<Integer>> collectionIdToIdProperties;
    private static Categories listOfCategories;
    private static ExtractedEntityTypes extractedEntityTypes;
    private int similarityMetric; // 1 to use exact match (equal), 2 to use Word2Vec model
    private ArrayList<String> listStopWords;
    private ArrayList<String> listDesignationWords;
    private static HashMap<Integer, Integer> collectionsCategories; // < collection id, category id>
    private HashMap<String, String> class2category; // the mapping between the classes and our 5 categories
    private HashMap<String, ArrayList<String>> categoriesHierarchy; // < subcategory label, < list of super classes labels > >

    public Classification(ConnectionLens cl, DataSource ds, Locale language) throws IOException {
        super(cl, ds);

        collectionsCategories = new HashMap<>();
        this.similarityMetric = 2;
        listOfCategories = new Categories();
        // get the list of stop words (we will remove them from nodes label and hints label)
        // get the list of synonyms for name/designation words
        this.locale = language;
        this.loadListsOfWords();

        this.categoriesHierarchy = new HashMap<>();
    }

    public Classification() {

    }

    public static ExtractedEntityTypes getExtractedEntityTypes() {
        return extractedEntityTypes;
    }

    public static void setExtractedEntityTypes(ExtractedEntityTypes extractedEntityTypes) {
        Classification.extractedEntityTypes = extractedEntityTypes;
    }

    private void storeCategoriesOnDisk() throws IOException, SQLException {
        String categoriesFilename = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "categories.txt").toString();
        OutputStream os = new FileOutputStream(categoriesFilename);

        for(Map.Entry<Integer, ClassificationCategory> entry : listOfCategories.classificationCategoriesIdToCategory.entrySet()) {
            int categoryId = entry.getKey();
            String categoryName = entry.getValue().getInformativeLabel();
            os.write(Integer.toString(categoryId).getBytes()); // category id
            os.write("\t".getBytes());
            os.write(categoryName.getBytes()); // category label
            os.write("\n".getBytes());
        }
        os.flush();
        os.close();

        this.graph.createTable(SchemaTableNames.TYPES_TABLE_NAME, "(id INTEGER, label VARCHAR)", false, true);
        this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.TYPES_TABLE_NAME + " FROM STDIN ", new FileReader(categoriesFilename));
    }

    public void run() throws SQLException, IOException, ParseException {
        log.info("CLASSIFICATION STARTS");

        this.localCollector.start(StatisticsCollector.total());

        log.debug("initialize classification");
        this.initialize();

        log.debug("classify collections");
        this.classifyCollections();
        log.debug("finished to classify collections");
        this.localCollector.stop(StatisticsCollector.total());
    }

    public void initialize() throws SQLException, IOException, ParseException {
        // 0. collect data from the normalized graph
        log.info("Collect nodes and edges to be classified");
        this.graph.createTable(SchemaTableNames.CLASSIFIED_NODES_TABLE_NAME, "(id NUMERIC, ds integer, type smallint, label varchar, normalabel varchar, labelprefix varchar, representative NUMERIC, signature integer[1])", false, true);
        this.graph.createTable(SchemaTableNames.CLASSIFIED_EDGES_TABLE_NAME, "(id SERIAL, ds integer, type smallint, source NUMERIC, target NUMERIC, label varchar, confidence float, signature integer[1], primary key (id, label))", false, true);

        PreparedStatement stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.CLASSIFIED_NODES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + ";");
        stmt.executeUpdate();
        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.CLASSIFIED_EDGES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + ";");
        stmt.executeUpdate();
        this.localCollector.tick(StatisticsCollector.total(), CLASS_INSERT_NODE_AND_EDGES_DISK);

        // 1. build set of extracted entities, namely classes(E)
        setExtractedEntityTypes(new ExtractedEntityTypes());

        // 2. build the set of hints into a hashmap from the json file
        log.info("Building set of hints");
        this.buildSetOfSemanticProperties();
        this.localCollector.tick(StatisticsCollector.total(), CLASS_BUILD_SEMANTIC_PROPERTIES_MEM);

        // 3. Load the semantic model (Word2Vec or Glove)
        log.info("Loading semantic model");
        this.loadWord2VecModel();
        this.localCollector.tick(StatisticsCollector.total(), CLASS_INIT_MODEL_MEM);

        // 4. Store the node types (categories) on disk
        this.storeCategoriesOnDisk();

        // 5. build the mapping between the classes and our 5 categories (person, product, event, ...)
        this.createMappingClasses2Categories();
    }

    /**
     * Build the set of hints defined in the JSON file (located by the property 'set_of_hints' in the local.settings).
     */
    private void buildSetOfSemanticProperties() {
        // add the basic categories in the set of categories (PERSON, LOCATION, EVENT, ORGANIZATION, PRODUCT, CREATIVE_WORK, OTHER)
        // TODO NELLY: should we stick to our categories or instead directly use the classes in KB for the properties that we defined?
        int categoryId = listOfCategories.addDefaults();

        // generate the set of hints (stored in lib/set-of-semantic-properties-extended.json) and expand it with GitTables properties
        try (Stream<String> lines = Files.lines(Paths.get(Config.getInstance().getStringProperty("set_of_semantic_properties_extended")))) {
            // load the original set of hints
            String taxonomyString = lines.collect(Collectors.joining(System.lineSeparator()));
            this.semanticPropertiesSet = JSON.parseObject(taxonomyString, new TypeReference<List<SemanticProperty>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }

        // load the extension of categories and add them in the set of categories
        try(Stream<String> lines =  Files.lines(Paths.get(Config.getInstance().getStringProperty("gitTables_classes")))) {
            HashSet<String> extendedCategories = lines.collect(Collectors.toCollection(HashSet::new));
            for(String categoryString : extendedCategories) {
//                    String categoryStringSanitized = camelToSnake(categoryString.replace("http://dbpedia.org/ontology/","")
//                            .replace("schema:","")).toLowerCase()
//                            .replace("_"," ");
                listOfCategories.add(categoryId, categoryString);
                categoryId++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // load the set of semantic properties from GitTables
        // and whenever a hint in GitTable already exists in the exising set of hints, we fuse its domain and range with the existing hint
        try(Stream<String> lines =  Files.lines(Paths.get(Config.getInstance().getStringProperty("gitTables_semantic_properties")))) {
            String gitTablePropertiesString = lines.collect(Collectors.joining(System.lineSeparator()));
            List<SemanticProperty> gitTableSemanticProperties = JSON.parseObject(gitTablePropertiesString, new TypeReference<List<SemanticProperty>>() {});
//            int nbPropertiesBroughtByGitTables = 0;
//            int nbPropertiesWithDomain = 0;
//            int nbPropertiesWithRange = 0;
//            for(SemanticProperty p : gitTableSemanticProperties) {
//                if(!this.semanticPropertiesSet.contains(p)) {
//                    nbPropertiesBroughtByGitTables++;
//                    if(p.getDomain() != null && !p.getDomain().isEmpty()) {
//                        nbPropertiesWithDomain++;
//                    }
//                    if(p.getRange() != null && !p.getRange().isEmpty()) {
//                        nbPropertiesWithRange++;
//                    }
//                }
//            }
//            log.info(nbPropertiesBroughtByGitTables);
//            log.info(nbPropertiesWithDomain);
//            log.info(nbPropertiesWithRange);
//            log.info(this.semanticPropertiesSet.size());


            for(SemanticProperty p : gitTableSemanticProperties) {
                if(!p.getDomain().isEmpty()) { // we don't want to use semantic properties with empty domain because they bring nothing to the classification result
                    if(this.semanticPropertiesSet.contains(p)) {
                        // then this is a duplicate, we need to merge the domain and the range of the new hint with the existing one
                        SemanticProperty existingHint = this.semanticPropertiesSet.get(this.semanticPropertiesSet.indexOf(p));
                        ArrayList<Integer> existingHintDomain = existingHint.getDomain();
                        ArrayList<Integer> existingHintRange = existingHint.getRange();
                        ArrayList<Integer> newHintDomain = p.getDomain();
                        ArrayList<Integer> newHintRange = p.getRange();
                        newHintDomain.addAll(existingHintDomain); // update the new hint domain with the existing one
                        p.setDomainWithIntTypes(newHintDomain);
                        newHintRange.addAll(existingHintRange); // update the new hint range with the existing one
                        p.setRangeWithIntTypes(newHintRange);
                        ArrayList<String> normalizedLabel = this.buildNormalizedLabel(p.getLabel());
                        p.setNormalizedLabel(normalizedLabel);
                        this.semanticPropertiesSet.remove(existingHint);
                        this.semanticPropertiesSet.add(p);
                    } else {
                        ArrayList<String> normalizedLabel = this.buildNormalizedLabel(p.getLabel());
                        p.setNormalizedLabel(normalizedLabel);
                        this.semanticPropertiesSet.add(p);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        // build the hierarchy of classes
        try(Stream<String> lines =  Files.lines(Paths.get(Config.getInstance().getStringProperty("schemaorg_class_hierarchy")))) {
            String fileContent = lines.collect(Collectors.joining(System.lineSeparator()));
            HashMap<String, ArrayList<String>> schemaorgClasses = JSON.parseObject(fileContent, new TypeReference<HashMap<String, ArrayList<String>>>() {});

            for (String subclass : schemaorgClasses.keySet()) {
                ArrayList<String> superClasses = new ArrayList<>();
                superClasses.addAll(schemaorgClasses.get(subclass));
                this.categoriesHierarchy.put(subclass, superClasses);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        log.debug(this.categoriesHierarchy);
    }

    /**
     * Load the Word2Vec model (stored in a file, located by the property 'word_embedding_model_path' in the local.settings).
     */
    private void loadWord2VecModel() {
        String filePath = Config.getInstance().getProperty("word_embedding_model_path");
        File modelForWord2Vec = new File(filePath);
        this.word2VecModel = WordVectorSerializer.readWord2VecModel(modelForWord2Vec);
    }

    private void loadListsOfWords() {
        String filenameForStopWords = Config.getInstance().getStringProperty((this.locale.getLanguage().equals("french")) ? "stop_words_french" : "stop_words_english");
        try (Scanner s = new Scanner(new File(filenameForStopWords))) {
            this.listStopWords = new ArrayList<>();
            while (s.hasNextLine()) {
                this.listStopWords.add(s.nextLine());
            }
        } catch (Exception e) {
            log.info(e);
        }

        String filenameForNameWords = Config.getInstance().getStringProperty("synonyms_denomination_file");
        try (Scanner s = new Scanner(new File(filenameForNameWords))) {
            this.listDesignationWords = new ArrayList<>();
            while (s.hasNextLine()) {
                this.listDesignationWords.add(s.nextLine());
            }
        } catch (Exception e) {
            log.info(e);
        }
    }

    private void createMappingClasses2Categories() {
        String filenameForClassesE = Config.getInstance().getStringProperty("mapping_classes_to_categories");
        try (Stream<String> lines = Files.lines(Paths.get(filenameForClassesE))) {
            String jsonString = lines.collect(Collectors.joining(System.lineSeparator()));
            this.class2category = new Gson().fromJson(jsonString, new TypeToken<HashMap<String, String>>() {}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void classifyCollections() throws IOException, SQLException {
        String filenameForCollectionsCategories = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "collections_categories.txt").toString();
        BufferedOutputStream osCollections = new BufferedOutputStream(new FileOutputStream(filenameForCollectionsCategories));

//        log.debug("get collections to classify");
        ArrayList<Integer> interestingCollections = new ArrayList<>();
        if(Config.getInstance().getBooleanProperty("classify_all_collections")) {
            // classify all entity collections
            // use this only when using the GUI
            // TODO NELLY: do this in memory + classify only collections of entities -> c.collId IN (...)
            PreparedStatement stmt;
            if(this.isRdf) {
                stmt = this.graph.getPreparedStatement(
                        "SELECT DISTINCT c.collId " +
                                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c " +
                                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e " +
                                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n2 " +
                                "WHERE c.collId = e.source AND e.target=n2.id " +
                                "AND n.type = " + Node.Types.RDF_URI.ordinal() + " AND n2.type = " + Node.Types.NORMALIZATION_NODE.ordinal() + " AND n2.label <> 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' AND n2.label <> 'http://www.w3.org/2002/07/owl#sameAs'");
            } else { // in this case, RDF URIS are considered values
                stmt = this.graph.getPreparedStatement(
                        "SELECT DISTINCT c.collId " +
                                "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c " +
                                ", " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " e " +
                                ", " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " n2 " +
                                "WHERE c.collId=e.source AND e.target=n2.id " +
                                "AND n.type NOT IN (" + Node.getValueTypes() + ", " + Node.Types.RDF_URI.ordinal() + ") AND n2.type NOT IN (" + Node.getValueTypes() + ", " + Node.Types.RDF_URI.ordinal() + ") ");
            }
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                interestingCollections.add(rs.getInt(1));
            }
        } else {
            // classify only main entity collections. We already have them in memory.
            // use this only when generating the description and the E/R schema
            interestingCollections.addAll(CollectionGraph.getInstance().getMainCollections());
        }
        this.localCollector.tick(StatisticsCollector.total(), CLASS_GET_INTERESTING_COLLECTIONS_DISK);
        if(interestingCollections.isEmpty()) {
            // if there are no interesting (main) entity collections, then we have nothing to classify
            return;
        }

//        log.debug("get record label and properties of each collection");
        this.collectionIdToNodeLabels = new HashMap<>(CollectionGraph.getInstance().getCollectionsLabels()); // deep copy
        this.collectionIdToNodeLabels.keySet().retainAll(interestingCollections);

        this.collectionIdToProperties = new HashMap<>();
        this.collectionIdToIdProperties = new HashMap<>();
        for(Integer collectionId : interestingCollections) {
            if(CollectionGraph.getInstance().getCollectionGraph().containsKey(collectionId)) {
                ArrayList<String> properties = new ArrayList<>();
                ArrayList<Integer> propertiesIds = new ArrayList<>();
                for(CollectionEdge ce : CollectionGraph.getInstance().getCollectionGraph().get(collectionId)) {
                    properties.add(CollectionGraph.getInstance().getCollectionLabel(ce.getTarget()));
                    propertiesIds.add(ce.getTarget());
                }
                this.collectionIdToProperties.put(collectionId, properties);
                this.collectionIdToIdProperties.put(collectionId, propertiesIds);
            }
        }
        this.localCollector.tick(StatisticsCollector.total(), CLASS_GET_COLLECTIONS_PROPERTIES_MEM);

        // With this loop, we classify each "interesting" collection
//        log.debug("classify each collection in " + interestingCollections);
        for(int collectionId : interestingCollections) { // TODO NELLY: à tester
            log.info("classify C" + collectionId + " (" + CollectionGraph.getInstance().getCollectionLabel(collectionId) + ")");
            Integer winnerCategoryForCollection = getCategoryId("CATEGORY_OTHER");
            if(CollectionGraph.getInstance().getCollectionType(collectionId) != null) {
                ArrayList<String> collectionTypes = CollectionGraph.getInstance().getCollectionType(collectionId);
                // there is a data type (e.g. rdf:Type), so no need to classify, we assign it as the final category of the collection
                if(collectionTypes.size() == 1) {
                    String collectionType = collectionTypes.get(0);
                    // if there is only one type for this collection, we can assign it to the collection
                    // else we need to classify the collection
                    if(Classification.getCategoryId(collectionType) == -1) {
                        // this category doesn't exist yet, we add it
                        Classification.listOfCategories.add(collectionType);
                    }
                    winnerCategoryForCollection = Classification.getCategoryId(collectionType);
                }
            } else {
                if(CollectionGraph.getInstance().getCollectionGraph().containsKey(collectionId)) {
                    ArrayList<ImmutablePair<String, ArrayList<ExtractedEntityType>>> dataPropertiesSet = this.buildDPsetForCollection(collectionId);
                    log.debug(dataPropertiesSet);
                    HashMap<Integer, Double> scoresForCategories = new HashMap<>();

                    HashMap<SemanticProperty, Double> scoresDpiPi = new HashMap<>(); // corresponds to P' set
                    for(ImmutablePair<String, ArrayList<ExtractedEntityType>> dpi : dataPropertiesSet) {
        //                log.debug("working on dpi=" + dpi);
        //                log.debug("signatures  found in dpi: " + dpi.getRight());
                        scoresDpiPi = new HashMap<>();
                        // create P' set by adding the semantic properties that are close to the data property dpi
                        for (SemanticProperty pi : this.semanticPropertiesSet) {
                            // we have found entities in dpi, we use them to compare dpi and pi
                            if (dpi.getRight() != null && !dpi.getRight().isEmpty()) {
                                double similarityDpiPi = this.similarity(dpi, pi, collectionId);
                                if (similarityDpiPi >= this.MINIMAL_SIMILARITY_SCORE) {
        //                            log.debug(pi + " is similar to " + dpi + " with a score of " + similarityDpiPi);
                                    scoresDpiPi.put(pi, similarityDpiPi);
                                }
                            } else {
                                // we have NOT found entities in dpi, we compare dpi and pi on the label only
                                double similarityDpiPi = this.labelSimilarity(dpi.getLeft(), pi);
                                if (similarityDpiPi >= this.MINIMAL_SIMILARITY_SCORE) {
        //                            log.debug(pi + " is similar to " + dpi + " with a score of " + similarityDpiPi);
                                    scoresDpiPi.put(pi, similarityDpiPi);
                                }
                            }
                        }
        //                log.debug(scoresDpiPi);

                        // P' is now constructed. We need to make each P'_i vote for each class in dom(P'_i)
                        double dataPropertyFrequency = CollectionGraph.getInstance().getCollectionFrequency(collectionId, dpi.getLeft());
                        for(SemanticProperty pi : scoresDpiPi.keySet()) {
                            for(Integer domainClass : pi.getDomain()) {
                                double currentScore = 0.0d;
                                if(scoresForCategories.containsKey(domainClass)) {
                                    // the category already have a score, we need to add the current score
                                    currentScore = scoresForCategories.get(domainClass);
                                } else {
                                    // the category has no score yet, its score is the current score
                                }
                                scoresForCategories.put(domainClass, currentScore + scoresDpiPi.get(pi) * dataPropertyFrequency * ((double) 1 / pi.getDomain().size()));
                            }
        //                    log.debug("adding score (" + scoresDpiPi.get(pi) + " * " + dataPropertyFrequency + " * 1/" + pi.getDomain().size() + ") to each category in " + pi.getDomain());
                        }
                    }

                    // if the label of the records into the collection is equal to one of the categories, this is a good insight to classify it
                    // because RDF data ends up with one unique label for each node, and then for each transaction, we have to loop on them...
                    ArrayList<String> nodeLabelsInCollection = this.collectionIdToNodeLabels.getOrDefault(collectionId, new ArrayList<>());
                    for(String recordLabel : nodeLabelsInCollection) {
                        for(Integer t : listOfCategories.keySet()) {
                            double labelSimilarity = this.labelSimilarity(listOfCategories.getCategoryLabel(t).replaceAll("record", "").replaceAll("collection", ""), recordLabel.toLowerCase());
                            if(labelSimilarity >= this.MINIMAL_SIMILARITY_SCORE) {
        //                        log.debug("record label similarity between " + recordLabel + " and " + listOfCategories.getCategoryLabel(t) + " is: "+labelSimilarity);
                                if(scoresForCategories.containsKey(t)) { // we add more weight to an existing potential category because the record label says the same
                                    scoresForCategories.put(t, scoresForCategories.get(t) + labelSimilarity);
                                } else { // we want to add weight to a new category, according to the record label
                                    scoresForCategories.put(t, labelSimilarity);
                                }
                            }
                        }
                    }

                    // the winner category is the category with the highest score
                    if(scoresForCategories.isEmpty()) {
                        winnerCategoryForCollection = getCategoryId("CATEGORY_OTHER");
                    } else {
                        winnerCategoryForCollection = this.selectCategoryWithMaximalScore(scoresForCategories, this.collectionIdToProperties.get(collectionId), collectionId);
                        // if the chosen category is part of class2category, we can take its corresponding CATEGORY (e.g. schema:Person corresponds to CATEGORY_PERSON)
                        String key = getCategoryLabel(winnerCategoryForCollection);
                        if(this.class2category.containsKey(key) && getCategoryId(this.class2category.get(key)) != -1) {
                            winnerCategoryForCollection = getCategoryId(this.class2category.get(key));
                        }
                    }
                }
            }

            // we set the type/category of nodes...
            log.info("collection " + CollectionGraph.getInstance().getCollectionLabel(collectionId) + " is classified as " + listOfCategories.getCategoryLabel(winnerCategoryForCollection) + " (" + winnerCategoryForCollection + ")");
            PreparedStatement stmt = this.graph.getPreparedStatement("" + // all the nodes
                    "UPDATE " + SchemaTableNames.CLASSIFIED_NODES_TABLE_NAME + " " +
                    "SET type = " + winnerCategoryForCollection + " " +
                    "WHERE id IN (SELECT nodeId FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " WHERE collId = " + collectionId + ")");
            stmt.executeUpdate();

            // ... and, if possible, we assign the corresponding type to the collection
            // collections of nodes that are something else than person, org, loc, event, CW, prod are collections of others
            // Side not: cannot do a switch here because it requires constant values (and does not accept values computed at runtime)
            int categoryIdInListOfCategories;
            if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_PERSON()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_PERSON));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_PERSON.name());
            } else if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_LOCATION()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_LOCATION));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_LOCATION.name());
            } else if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_ORGANIZATION()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_ORGANIZATION));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_ORGANIZATION.name());
            } else if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_CREATIVE_WORK()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_CREATIVE_WORK));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_CREATIVE_WORK.name());
            } else if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_EVENT()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_EVENT));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_EVENT.name());
            } else if(winnerCategoryForCollection == listOfCategories.getCategoryId_CATEGORY_PRODUCT()) {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_PRODUCT));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_PRODUCT.name());
            } else {
                categoryIdInListOfCategories = getCategoryId(String.valueOf(Node.Types.CATEGORY_OTHER));
                log.debug("classified collection " + collectionId + " as being " + Node.Types.CATEGORY_OTHER.name());
            }
            osCollections.write(Integer.toString(collectionId).getBytes());
            osCollections.write("\t".getBytes());
            osCollections.write(Integer.toString(categoryIdInListOfCategories).getBytes());
            osCollections.write("\n".getBytes());
            CollectionGraph.getInstance().setCollectionCategory(collectionId, categoryIdInListOfCategories);
        }
        osCollections.flush();
        osCollections.close();
        this.localCollector.tick(StatisticsCollector.total(), CLASS_CLASSIFY_INTERESTING_COLLECTIONS_MEM_DISK);

        // update collections categories
        log.debug("copy collection categories into postgres and update categories");
        this.graph.createTable(SchemaTableNames.COLLECTIONS_CATEGORIES_TABLE_NAME, "(collId INTEGER, categoryId INTEGER)", true, true);
        this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.COLLECTIONS_CATEGORIES_TABLE_NAME + " FROM STDIN ", new FileReader(filenameForCollectionsCategories));
        this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c SET collCategory = cc.categoryId FROM " + SchemaTableNames.COLLECTIONS_CATEGORIES_TABLE_NAME + " cc WHERE c.collId = cc.collId;").executeUpdate();
    }

    private ArrayList<ImmutablePair<String, ArrayList<ExtractedEntityType>>> buildDPsetForCollection(int collectionId) {
        ArrayList<ImmutablePair<String, ArrayList<ExtractedEntityType>>> DPforCollection = new ArrayList<>();

        for(Integer dataProperty : this.collectionIdToIdProperties.get(collectionId)) {
            String dataPropertyLabel = CollectionGraph.getInstance().getCollectionLabel(dataProperty);
            ImmutablePair<String, ArrayList<ExtractedEntityType>> dpi;
            HashMap<ExtractedEntityType, java.lang.Integer[]> entitiesExtractedInDp = CollectionGraph.getInstance().getEntityProfilesOfCollection(collectionId+dataPropertyLabel);
            if(entitiesExtractedInDp == null) {
                dpi = new ImmutablePair<>(dataPropertyLabel, new ArrayList<>());
            } else {
                dpi = new ImmutablePair<>(dataPropertyLabel, new ArrayList<>(entitiesExtractedInDp.keySet()));
            }
//            log.info(dpi);
            DPforCollection.add(dpi);
        }
//        log.info(DPforCollection);
        return DPforCollection;
    }

    /**
     * Build the normalized label of a given label, i.e. build an arraylist of the parts of the label with a bit of processing (no stop-word, ...).
     *
     * @param label the label to be normalized.
     * @return the normalized label.
     */
    protected ArrayList<String> buildNormalizedLabel(String label) {
        String preprocessedLabel;
        try { // check if the string is a URI, if true get the last part of the URI (like title in dbpedia.org/property/title)
            new URL(label).toURI();
            preprocessedLabel = label.substring(label.lastIndexOf('/') + 1);
        } catch (Exception e) {
            // there was an exception, so the label is not a URI so keep the label as is
            preprocessedLabel = label;
        }

        preprocessedLabel = preprocessedLabel.trim();
        preprocessedLabel = preprocessedLabel.replace("_", " ");
        preprocessedLabel = preprocessedLabel.replace("-", " ");
        preprocessedLabel = preprocessedLabel.replace("\\.", " ");
        preprocessedLabel = preprocessedLabel.replace("/", " ");
        preprocessedLabel = preprocessedLabel.replace("@", " ");
        preprocessedLabel = StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(preprocessedLabel), " "); // transform camel case
        preprocessedLabel = preprocessedLabel.toLowerCase();

        // stem the property name to its root form (if exists in the word2Vec model)
        // NB (Jan 11 2022): commented for now, some stems are incorrect / do not make sense (the root form of 'died' is 'di'??)
//        PorterStemmer ps = new PorterStemmer();
//        String preprocessedLabel1 = ps.stem(preprocessedLabel);
//        log.debug(preprocessedLabel);
//        if (word2VecModel.hasWord(preprocessedLabel1)) preprocessedLabel = preprocessedLabel1;
//        log.debug(preprocessedLabel);


        ArrayList<String> normalizedLabel = new ArrayList<>(Arrays.asList(preprocessedLabel.split(" +")));
        normalizedLabel.removeAll(this.listStopWords);
        normalizedLabel.removeAll(Collections.singleton("")); // have to use singleton because removeAll takes a collection as argument

        return normalizedLabel;
    }

    /**
     * Load the FastText model (stored in a file, located by the property 'word_embedding_model_path' in the local.settings).
     */
    private void loadFastTextModel() {
        String filePath = Config.getInstance().getProperty("word_embedding_model_path");
        FastText fastTextModel = new FastText();
        fastTextModel.loadBinaryModel(filePath);
        this.fastTextModel = fastTextModel;
    }

    public static String camelToSnake(String str) {

        String regex = "([a-z])([A-Z]+)";
        String replacement = "$1_$2";

        // Replace the given regex
        // with replacement string
        // and convert it to lower case.
        str = str.replaceAll(regex, replacement).toLowerCase();

        // return string
        return str;
    }

    /**
     * Get the most frequent type in the given list.
     * If several categories have the maximal frequency, then we have a trick to not select randomly:
     *   if there are some fields describing the name/designation of something, we use this information by adding the type of the extracted entity in this field to the possible categories
     *
     * @param scoresForCategories the list.
     * @return the most frequent element (i.e. a Node.Types) in the given list.
     */
    protected Integer selectCategoryWithMaximalScore(HashMap<Integer, Double> scoresForCategories, ArrayList<String> propertiesInCollection, int collectionId) {
        // The winner is the category with the highest score. If there are many winners, we have a trick to decide which is the most valuable to be returned.
//        log.debug("scoresForCategories: "+scoresForCategories);
        double maximalScore = Collections.max(scoresForCategories.values());
//        log.debug("maximal frequency is "+ maximalScore);
        ArrayList<Integer> categoriesWithTheMaximalScore = new ArrayList<>();
        for (Integer category : scoresForCategories.keySet()) {
            if (scoresForCategories.get(category) == maximalScore) {
                categoriesWithTheMaximalScore.add(category);
            }
        }
//        log.debug(categoriesWithTheMaximalScore);

        if(categoriesWithTheMaximalScore.size() == 1) {
            // there is no tie, we return the category with the maximal frequency
//            log.debug("no tie: " + categoriesWithTheMaximalScore.get(0));
            if(maximalScore > this.MINIMAL_SCORE_CATEGORY) {
                return categoriesWithTheMaximalScore.get(0);
            } else {
                return listOfCategories.getCategoryId_CATEGORY_OTHER();
            }
        } else {
            // there is a tie, we don't want to select randomly the category, so we use our trick based on designation synonyms
//            log.debug("there is a tie");

            // first trick: if there is a tie between two categories and one category is a superclass of the other, then we classify c with the subclass
            if(categoriesWithTheMaximalScore.size() == 2) {
                // whenever a property is a superclass of the other, then we can classify it with the subclass for sure
                int c1 = categoriesWithTheMaximalScore.get(0);
                int c2 = categoriesWithTheMaximalScore.get(1);
                if(this.categoriesHierarchy.containsKey(getCategoryLabel(c1)) && this.categoriesHierarchy.get(getCategoryLabel(c1)).contains(getCategoryLabel(c2))) {
//                    log.debug(getCategoryLabel(c1) + " is a subclass of " + getCategoryLabel(c2));
                    return c1;
                }
            }

            // third trick: if one of the property is a designation property we try to use it to classify it with the category that has been extracted into its value
           for (String property : propertiesInCollection) {
               if (this.listDesignationWords.contains(property.toLowerCase())) {
                   ArrayList<ExtractedEntityType> entitiesExtractedFromProperty;
                   if (CollectionGraph.getInstance().getEntityProfiles() != null && CollectionGraph.getInstance().getEntityProfiles().containsKey(collectionId + property)) {
                       entitiesExtractedFromProperty = new ArrayList<>(CollectionGraph.getInstance().getEntityProfilesOfCollection(collectionId + property).keySet());
                   } else {
                       // there are no signatures or there are no extracted entities for the property in the collection
                       // therefore we have no clue to decide, so we classify it as OTHER
                       return listOfCategories.getCategoryId_CATEGORY_OTHER();
                   }

                  if (entitiesExtractedFromProperty.size() == 1) {
                       // we return the record type associated to the entity extracted in the designation field
                       // we check that the record associated to the extracted entity exists in the selected categories (because we don't want to decide the type of a record based on the designation field only)
                      int extractedEntityType = entitiesExtractedFromProperty.get(0).ordinal();
                       if(extractedEntityType == listOfCategories.getCategoryId_CATEGORY_PERSON() || extractedEntityType == Node.Types.FIRST_NAME.ordinal()) {
                           if (categoriesWithTheMaximalScore.contains(listOfCategories.getCategoryId_CATEGORY_PERSON())) {
                               return listOfCategories.getCategoryId_CATEGORY_PERSON();
                           } else {
                               return listOfCategories.getCategoryId_CATEGORY_OTHER();
                           }
                       } else if(extractedEntityType == Node.Types.ENTITY_LOCATION.ordinal()) {
                           if (categoriesWithTheMaximalScore.contains(listOfCategories.getCategoryId_CATEGORY_LOCATION())) {
                               return listOfCategories.getCategoryId_CATEGORY_LOCATION();
                           } else {
                               return listOfCategories.getCategoryId_CATEGORY_OTHER();
                           }
                       } else if(extractedEntityType == Node.Types.ENTITY_ORGANIZATION.ordinal()) {
                           if (categoriesWithTheMaximalScore.contains(listOfCategories.getCategoryId_CATEGORY_ORGANIZATION())) {
                               return listOfCategories.getCategoryId_CATEGORY_ORGANIZATION();
                           } else {
                               return listOfCategories.getCategoryId_CATEGORY_OTHER();
                           }
                       } else {
                           return listOfCategories.getCategoryId_CATEGORY_OTHER();
                       }
                  } else {
                      // there are several extracted entities in the name fields, therefore we cannot choose which category is the best.
                      // to avoid confusing the user, we return OTHER (to say that we don't have found something relevant)
                      return listOfCategories.getCategoryId_CATEGORY_OTHER();
                  }
               }
           }
           return listOfCategories.getCategoryId_CATEGORY_OTHER();
        }
    }

    /**
     * Compute the similarity between two objects, i.e. pi (the object built from the node) and h (a hint).
     * The similarity is the sum between the similarity of the labels and the similarity of the signature/range.
     *
     * @param dpi The node to be compared with the hint.
     * @param h  The hint.
     * @return A double being the similarity between pi and h.
     */
//    protected double similarity(ImmutablePair<String, HashMap<Integer, Integer[]>> pi, SemanticProperty h) {
    protected double similarity(ImmutablePair<String, ArrayList<ExtractedEntityType>> dpi, SemanticProperty h, int collectionId) {
        // sim(pi,h) = sig_sim(pi.signature, h.range) + sum_{k_i in K_1, k_j in K_2} cosine_sim(V(k_i), V(k_j))

        double labelSimilarity = this.labelSimilarity(dpi.getLeft(), h);

        // the mappedRange variable contains the mapping of the classes in h.getRang() to our own extracted entities (Person, Location, Organization)
        // e.g. if range has schema.org/Place, this is the same as ExtractedEntityType.ENTITY_LOCATION
        // this simulates classesE in the paper
        ArrayList<Integer> mappedRange = new ArrayList<>();
        for(Integer type : h.getRange()) {
            if(getExtractedEntityTypes().getExtractedEntityTypesFromId(type) != null)
                mappedRange.add(getExtractedEntityTypes().getExtractedEntityTypesFromId(type).ordinal());
        }

        double signatureSimilarity = this.entityProfileSimilarity(dpi.getRight(), mappedRange, dpi.getLeft(), collectionId);

        // the total similarity is equal to the sum of the cosine similarity between the words and the signature similarity
        return (labelSimilarity + signatureSimilarity) / 2;
    }

    /**
     * Computes the similarity between the signature of a node and the range of a hint.
     * On 9/21/2021, the function only compares how many types are in common.
     *
     * @param dpiSignature the most frequent entity found in the property.
     * @param piRange  the domain of the hint.
     * @return a double being the similarity between the node signature and the hint range.
     */
    private double entityProfileSimilarity(ArrayList<ExtractedEntityType> dpiSignature, ArrayList<Integer> piRange, String dpiLabel, int collectionId) {
        if(dpiSignature == null || piRange == null || dpiSignature.isEmpty() || piRange.isEmpty()) {
            return 0;
        } else {
            // TODO NELLY: uncomment - only for Yelp datasets
            double sumOfSignatureImportance = 0.0d;
            //log.debug("hintRange: "+hintRange);
            Set<Integer> setExtractedEntityTypesIds = new HashSet<>();
            for(ExtractedEntityType eet : dpiSignature) {
                if(eet != null) {
                    setExtractedEntityTypesIds.add(eet.ordinal());
                }
            }
            Sets.SetView<Integer> intersection = Sets.intersection(setExtractedEntityTypesIds, new HashSet<>(piRange));
            Sets.SetView<Integer> union = Sets.union(setExtractedEntityTypesIds, new HashSet<>(piRange));
            if(!union.isEmpty() && (double) intersection.size() / union.size() >= this.MINIMAL_SIMILARITY_SCORE) {
                for(Integer commonType : intersection) {
                    HashMap<ExtractedEntityType, Integer[]> entityProfileOfCollectionAndProperty = CollectionGraph.getInstance().getEntityProfilesOfCollection(collectionId+dpiLabel);
                    ExtractedEntityType entityCommonType = extractedEntityTypes.getExtractedEntityTypesFromId(commonType);
                    Integer[] statsExtractedType = entityProfileOfCollectionAndProperty.get(entityCommonType);
                    int totalSizeValueNodes = statsExtractedType[0];
                    int totalSizeExtractedEntitiesOfThisType = statsExtractedType[1];
                    sumOfSignatureImportance += (double) totalSizeExtractedEntitiesOfThisType / totalSizeValueNodes;
                }
            } else {
                return 0.0d;
            }

            // this.localCollector.tick(StatisticsCollector.total(), CLASS_SIGN_SIM);

            return sumOfSignatureImportance / piRange.size();
        }
    }

    /**
     * Compute the similarity between two strings.
     * It compares using string equality (similarityMetric = 1) or the word2Vec model (similarityMetric = 2).
     *
     * @param dpiLabel the first string.
     * @param semanticProperty the second string.
     * @return the similarity between the two words using the similarity metric.
     */
    private double labelSimilarity(String dpiLabel, SemanticProperty semanticProperty) {
        ArrayList<String> preprocessedNode = this.buildNormalizedLabel(dpiLabel);
        ArrayList<String> preprocessedSemanticProperty = semanticProperty.getNormalizedLabel();
        if(preprocessedSemanticProperty == null) {
            preprocessedSemanticProperty = this.buildNormalizedLabel(semanticProperty.getLabel());
            semanticProperty.setNormalizedLabel(preprocessedSemanticProperty);
        }

        // one (or both) of the words are composed words (e.g. 'birth date') so we should compare each part of the words and compute the mean
        double labelSimilarity = 0.0d;
        int nbComparisons = 0;
        if(dpiLabel.equals(semanticProperty.getLabel())) {
            labelSimilarity = 1.0d;
            nbComparisons = 1;
        } else {
            if(this.similarityMetric == 2) {
                for (String word1 : preprocessedNode) {
                    for (String word2 : preprocessedSemanticProperty) {
                        double sim = (double) this.word2VecModel.similarity(word1, word2);
                        sim = Double.isNaN(sim) ? 0.0d : sim;
                        labelSimilarity += sim;
                        nbComparisons += 1;
                    }
                }
            }
        }
        if (nbComparisons == 0) {
            labelSimilarity = 0.0d; // to avoid dividing by 0
        } else {
            labelSimilarity /= nbComparisons;
        }

//        this.localCollector.tick(StatisticsCollector.total(), CLASS_LABEL_SIM);
        return labelSimilarity;
    }

    // workaround because we can' create a constructor for SemanticProperty, but we need to create a new one for record labels
    // therefore, the workaround is to define the label sim with a string instead of a semanticProperty
    private double labelSimilarity(String dpiLabel, String semanticProperty) {
        ArrayList<String> preprocessedNode = this.buildNormalizedLabel(dpiLabel);
        ArrayList<String> preprocessedSemanticProperty = this.buildNormalizedLabel(semanticProperty);
        if(preprocessedSemanticProperty == null) {
            preprocessedSemanticProperty = this.buildNormalizedLabel(semanticProperty);
        }

        // one (or both) of the words are composed words (e.g. 'birth date') so we should compare each part of the words and compute the mean
        double labelSimilarity = 0.0d;
        int nbComparisons = 0;
        if(dpiLabel.equals(semanticProperty)) {
            labelSimilarity = 1.0d;
            nbComparisons = 1;
        } else {
            if(this.similarityMetric == 2) {
                for (String word1 : preprocessedNode) {
                    for (String word2 : preprocessedSemanticProperty) {
                        double sim = (double) this.word2VecModel.similarity(word1, word2);
                        sim = Double.isNaN(sim) ? 0.0d : sim;
                        labelSimilarity += sim;
                        nbComparisons += 1;
                    }
                }
            }
        }
        if (nbComparisons == 0) {
            labelSimilarity = 0.0d; // to avoid dividing by 0
        } else {
            labelSimilarity /= nbComparisons;
        }

//        this.localCollector.tick(StatisticsCollector.total(), CLASS_LABEL_SIM);
        return labelSimilarity;
    }

    public List<SemanticProperty> getSemanticPropertiesSet() {
        return this.semanticPropertiesSet;
    }

    public void setSemanticPropertiesSet(List<SemanticProperty> semanticPropertiesSet) {
        this.semanticPropertiesSet = semanticPropertiesSet;
    }

    public Word2Vec getWord2VecModel() {
        return this.word2VecModel;
    }

    public void setWord2VecModel(Word2Vec word2VecModel) {
        this.word2VecModel = word2VecModel;
    }

    public double getMinimalScoreHint() {
        return this.MINIMAL_SIMILARITY_SCORE;
    }

    public StatisticsCollector getLocalCollector() {
        return this.localCollector;
    }

    public RelationalGraph getGraph() {
        return this.graph;
    }

    public static Categories getListOfCategories() {
        return listOfCategories;
    }

    public static int getCategoryId(String categoryLabel) {
        return listOfCategories.getCategoryId(categoryLabel);
    }

    public static ArrayList<Integer> getCategoryIds(String... categoryLabels) {
        ArrayList<Integer> correspondingCategoryIds = new ArrayList<>();
        for(String categoryLabel : categoryLabels) {
            int categoryId = listOfCategories.getCategoryId(categoryLabel);
            if(categoryId != -1) {
                correspondingCategoryIds.add(categoryId);
            }
        }
        return correspondingCategoryIds;
    }

    public static String getCategoryLabel(int categoryId) {
        return listOfCategories.getCategoryLabel(categoryId);
    }

    public static String getCategoryInformativeLabel(int categoryId) {
        return listOfCategories.getCategoryInformativeLabel(categoryId);
    }

    public static HashMap<Integer, ClassificationCategory> getClassificationCategories() {
        return listOfCategories.getClassificationCategoriesIdToCategory();
    }

    public static ArrayList<Integer> getDefaultCategories() {
        return listOfCategories.getDefaultCategories();
    }

    public static HashMap<Integer, Integer> getCollectionsCategories() {
        return collectionsCategories;
    }

    public void reportStatistics() {
        if(this.localCollector != null) {
            this.localCollector.stopAll();
            this.localCollector.reportAll();
        }
    }
}

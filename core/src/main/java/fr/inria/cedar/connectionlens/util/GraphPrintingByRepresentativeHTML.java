/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Draws ConnectionLens graphs using DOT. Is only feasible for small(ish)
 * graphs. 
 */
public class GraphPrintingByRepresentativeHTML {

	private BufferedWriter htmlBufferedWriter;
	private BufferedWriter jsBufferedWriter;
	private Set<Node> printedNodes;
	private HashMap<Node, HashSet<Node>> representation;
	private HashMap<Node, Node> nodeRepresentedBy;
	private Map<Node, Integer> newIds;
	private String drawingDir;
	private long epoch;
	private String jsFileName;
	private String htmlFileName;
	private Graph.GraphType typeOfGraph;
	private static final Logger log = Logger.getLogger(GraphPrintingByRepresentativeHTML.class);

	public GraphPrintingByRepresentativeHTML(Graph.GraphType typeOfGraph) {
		this.typeOfGraph = typeOfGraph;
		epoch = System.currentTimeMillis();
		printedNodes = new HashSet<>();
		representation = new HashMap<>();
		nodeRepresentedBy = new HashMap<>();
		newIds = new HashMap<>();

		String prefix = (this.isAbstract()) ? "abstract" : (this.isNormalized()) ? "normalized" : "original";
		jsFileName = prefix + "_graph_" + epoch + ".js";
		htmlFileName = prefix + "_graph_" + epoch + ".html";

		try {
			drawingDir = Config.getInstance().getStringProperty("temp_dir");

			htmlBufferedWriter = new BufferedWriter(new FileWriter(Paths.get(drawingDir, htmlFileName).toString()));
			jsBufferedWriter = new BufferedWriter(new FileWriter(Paths.get(drawingDir, jsFileName).toString()));
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(String.format("Drawing directory folder '%s' not found.", drawingDir));
		} catch (IOException e) {
			throw new IllegalStateException("Could not open file for printing");
		}

	}

	public GraphPrintingByRepresentativeHTML() {
		this(Graph.GraphType.ORIGINAL_GRAPH);
	}

	public long analyzeNodes(Graph graph, DataSource ds) {
		// log.info("\nAnalyze nodes");
		if (graph.countNodes(ds) > 500) {
			return 500;
		}
		for (Node node : graph.getNodes(ds)) {
			Node representativeNode = node.getRepresentative();
			// try a transitive closure here to determine the ultimate representative
			// in case n.rep.rep is not n.rep
			Node representativeNode2 = representativeNode.getRepresentative();
			while (!representativeNode2.equals(representativeNode)) {
				representativeNode2 = representativeNode2.getRepresentative();
				representativeNode = representativeNode.getRepresentative();
			}
			// when we exit, representativeNode equals representativeNode2 and is the
			// ultimate
			// representative of node
			HashSet<Node> repByR = representation.get(representativeNode);
			if (!representation.containsKey(representativeNode)) {
				repByR = new HashSet<>();
				representation.put(representativeNode, repByR);
			}
			repByR.add(node);

			if (!nodeRepresentedBy.containsKey(node)) {
				// log.info("Learned that " + representativeNode.getType() + " " +
				// representativeNode
				// + " represents " + node.getType() + " " + node);
				nodeRepresentedBy.put(node, representativeNode);
			}
		}
		return graph.countNodes(ds);
	}

	public void printNodes() {
		try {
			jsBufferedWriter.write("var dataset = {");
			jsBufferedWriter.write("'nodes' : [");
		} catch (IOException IOE) {
			throw new IllegalStateException("Unable to write");
		}
		int j = 0;
		for (Node representativeNode : representation.keySet()) {
			if (j > 0) {
				try {
					jsBufferedWriter.write(",");
				} catch (IOException IOE) {
					throw new IllegalStateException("Unable to write");
				}
			}
			if (!newIds.containsKey(representativeNode)) {
				printRepresentedCluster(representativeNode, j);
				newIds.put(representativeNode, j);
				j += 1;
			}
		}
		try {
			jsBufferedWriter.write("],'edges':[");
		} catch (IOException IOE) {
			throw new IllegalStateException("Unable to write");
		}

	}

	public int printEdges(Graph g, DataSource ds, int i) {
		// log.info("\nGraphDOTPrinting edges from: " + ds.getID() + " " + ds.getURI());
		for (Edge edge : g.getEdges(ds)) {
			if ((!edge.getLabel().equals(Edge.SAME_AS_LABEL))
							|| (edge.confidence() < 1 && edge.confidence() > 0.9)) {

				if (i > 0) {
					try {
						jsBufferedWriter.write(",");
					} catch (IOException IOE) {
						throw new IllegalStateException("Unable to write");
					}
				}
				print(edge);
				i += 1;
			}
		}
		return i;
	}

	public void printEdges(Set<Edge> edges, int i) {
		for (Edge e : edges) {
			if ((!e.getLabel().equals(Edge.SAME_AS_LABEL) || 
					(e.confidence() < 1 && e.confidence() > 0.9))) {

				if (i > 0) {
					try {
						jsBufferedWriter.write(",");
					} catch (IOException IOE) {
						throw new IllegalStateException("Unable to write");
					}
				}
				print(e);
				i += 1;
			}
		}
	}

	public static String getColorFunction() {
		String cols = " function coloring(label) { /** return color depending on label */" + " switch(label){ "
				+ " case 'COLLECTION': return ['#808080',15];" + " case 'TLRECORD': return ['#fc000f',13];"
				+ " case 'SLRECORD': return ['#fc8200',10];" + " case 'DATA_SOURCE': return ['#000000',18];"
				+ " case 'JSON_STRUCT': return ['#318ce7',15];" + " case 'JSON_VALUE': return ['#318ce7',10];"
				+ " case 'RDF_LITERAL': return ['#318ce7',10];" + " case 'RDF_URI': return ['#318ce7',15];"
				+ " case 'RELATIONAL_STRUCT': return ['#318ce7',15];"
				+ " case 'RELATIONAL_VALUE': return ['#318ce7',10];" + " case 'ENTITY_GENERIC': return ['#34c924',10];"
				+ " case 'ENTITY_PERSON': return ['#34c924',10];" + " case 'ENTITY_LOCATION': return ['#34c924',10];"
				+ " case 'ENTITY_ORGANIZATION': return ['#34c924',10];" + " case 'TEXT_VALUE': return ['#318ce7',10];"
				+ " case 'HTML_NODE': return ['#318ce7',15];" + " case 'HTML_VALUE': return ['#318ce7',10];"
				+ " case 'FIRST_NAME': return ['#34c924',10];" + " case 'EMAIL': return ['#34c924',10];"
				+ " case 'HASHTAG': return ['#34c924',10];" + " case 'XML_NODE': return ['#318ce7',15];"
				+ " case 'XML_VALUE': return ['#318ce7',10];" + " case 'DO_NOT_LINK_VALUE': return ['#000000',10];"
				+ " default : return ['#111111',10]; }} ";
		return cols;
	}

	public void closeAndDraw() {

		try {
			String html = "<!DOCTYPE html> <html lang='en'> " + "<head> <meta charset='utf-8'>"
					+ "<title>Force Layout with labels on edges</title>"
					+ " <script src='https://d3js.org/d3.v3.min.js' charset='utf-8'></script>"
					+ " <style type='text/css'> </style> </head> <body> <script src='" + jsFileName
					+ "' charset='utf-8'></script> </body></html>";
			htmlBufferedWriter.write(html);
			htmlBufferedWriter.close();

			String js = "]}; var w = 3000;" + " var h = 3000;" + " var linkDistance = 200;"
					+ " var svg = d3.select('body') .append('svg') .attr({ 'width': w, 'height': h }) .append('g') .call(d3.behavior.zoom()"
					+ ".on('zoom', function() { svg.attr('transform', 'translate(' + d3.event.translate + ')' + ' scale(' + d3.event.scale + ')') }));"
					+ " function linkdist(d) { return d.target / 100 }"
					+ " var force = d3.layout.force() .nodes(dataset.nodes) .links(dataset.edges) .size([w, h]) .gravity(0.1) .distance(linkDistance) .charge(-800) .start(); "
					+ " var edges = svg.selectAll('line') .data(dataset.edges) .enter() .append('line') .attr('id', function (d, i) { return 'edge' + i }) "
					+ ".attr('marker-end', 'url(#arrowhead)') .style('stroke', '#aaa') .style('pointer-events', 'none'); "

					+ getColorFunction()

					+ " var nodes = svg.selectAll('circle') .data(dataset.nodes) .enter() .append('circle') .attr({ 'r': function(d) { return coloring(d.type)[1] }})"
					+ " .attr({ 'fill': function(d) { return coloring(d.type)[0] } }) .call(force.drag) .on('mouseover', handleMouseOver) .on('mouseout', handleMouseOut);"
					+ " var edgepaths = svg.selectAll('.edgepath') .data(dataset.edges) .enter() .append('path')"
					+ " .attr({ 'd': function(d) { return 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y }, "
					+ " 'class': 'edgepath', 'fill-opacity': 0, 'stroke-opacity': 0, 'fill': 'blue', 'stroke': 'red',"
					+ " 'id': function (d, i) { return 'edgepath' + i } }) .style('pointer-events', 'none');"
					+ " var edgelabels = svg.selectAll('.edgelabel') .data(dataset.edges) .enter() .append('text') .style('pointer-events', 'none')"
					+ " .attr({ 'class': 'edgelabel', 'id': function (d, i) { return 'edgelabel' + i }, "
					+ " 'dx': function(d) { return (linkDistance - d.label.length * 5) / 2 }, 'dy': 0, 'font-size': 9, 'fill': 'black' });"
					+ " edgelabels.append('textPath') .attr('xlink:href', function (d, i) { return '#edgepath' + i }) .style('pointer-events', 'none') "
					+ " .text(function (d, i) { return d.label; }); svg.append('defs').append('marker') "
					+ " .attr({ 'id': 'arrowhead', 'viewBox': '-0 -5 10 10', 'refX': 25, 'refY': 0, 'orient': 'auto', 'markerWidth': 10, 'markerHeight': 10, 'xoverflow': 'visible' }) "
					+ ".append('svg:path') .attr('d', 'M 0,-5 L 10 ,0 L 0,5') .attr('fill', '#ccc') .attr('stroke', '#ccc');"
					+ " force.on('tick', ticked); function ticked() { edges.attr({ 'x1': function(d) { return d.source.x; }, "
					+ " 'y1': function(d) { return d.source.y; }, 'x2': function(d) { return d.target.x; }, "
					+ "'y2': function(d) { return d.target.y; }, }); nodes.attr({ 'cx': function(d) { return d.x; }, "
					+ " 'cy': function(d) { return d.y; } }); "
					+ " edgepaths.attr('d', function(d) { var path = 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y; "
					+ "/** console.log(d) */ return path }); "
					+ "edgelabels.attr('transform', function (d, i) { if (d.target.x < d.source.x) { bbox = this.getBBox();"
					+ " rx = bbox.x + bbox.width / 2; ry = bbox.y + bbox.height / 2; return 'rotate(180 ' + rx + ' ' + ry + ')'; } else { return 'rotate(0)'; } }); } "
					+ " function handleMouseOver(d, i) {  /** Add interactivity */ /** Use D3 to select element, change color and size */ "
					+ "d3.select(this).attr({ 'stroke': 'black','stroke-width' : 3, 'r': 30 }); var j = 0; var l = 0; for (property in d)"
					+ " { if (!['fixed', 'weight', 'index', 'x', 'y', 'px', 'py'].includes(property) && d[property] != '') { j = j + 1; if (d[property].toString().length + property.length > l)"
					+ " { l = d[property].toString().length + property.length; } } } svg.append('rect') .attr('fill', 'darkgrey') .attr('fill-opacity', 0.9) .attr('id', 'hover') "
					+ ".attr('x', d.x + 25) .attr('y', d.y - j * 8 - 20) .attr('width', l * 20 + 50) .attr('height', j * 15 + 20); "
					+ "var k = 0; for (property in d) { if (!['fixed', 'weight', 'index', 'x', 'y', 'px', 'py'].includes(property) && d[property] != '') "
					+ "{ svg.append('text').attr({ id: 'hoverid' + i + k,  /** Create an id for text so we can select it later for removing on mouseout */ "
					+ "x: function() { return d.x + 30; }, y: function() { return d.y - j * 8 + k * 15; } }) .attr('fill', 'white') "
					+ ".text(function() { return [property + ' : ' + d[property]];  /** Value of the text */ }); k = k + 1; } } } "
					+ " function handleMouseOut(d, i) { /** Use D3 to select element, change color back to normal */"
					+ " d3.select(this).attr({ 'stroke': 'transparent' ,'r': function(d) { return coloring(d.type)[1] } }); var j = 0;"
					+ " d3.select('#hover').remove(); for (property in d) { d3.select('#hoverid' + i + j).remove(); j = j + 1; } }";

			jsBufferedWriter.write(js);
			jsBufferedWriter.close();
		} catch (IOException ioe) {
			throw new IllegalStateException("Could not write in or close the files");
		}

	}

	private void print(Edge edge) {

		Node sourceNode = edge.getSourceNode().getRepresentative();
		Node targetNode = edge.getTargetNode().getRepresentative();

		if (nodeRepresentedBy.containsKey(sourceNode)) {
			sourceNode = nodeRepresentedBy.get(sourceNode);
		}
		if (nodeRepresentedBy.containsKey(targetNode)) {
			targetNode = nodeRepresentedBy.get(targetNode);
		}

		try {
			jsBufferedWriter.write(
					"{'target':" + newIds.get(targetNode) + " ,'source':" + newIds.get(sourceNode) + " ,'label':'"
							+ edge.getLabel().replaceAll("['|’|«|»|\n]", " ") + "','type':'" + edge.getType() + "' }");

		} catch (IOException IOE) {
			throw new IllegalStateException("Unable to draw node " + sourceNode.getId() + targetNode.getId());
		}

	}

	private void printRepresentedCluster(Node representativeNode, int id) {
		if (printedNodes.contains(representativeNode)) {
			return;
		}
		printedNodes.add(representativeNode);
		try {

			HashSet<Node> constituency = representation.get(representativeNode);

			String also_represents = "";
			if (constituency.size() > 1) {
				int i = 0;
				for (Node n : constituency) {
					if (representativeNode != n) {
						if (i > 0) {
							also_represents += " | ";
						}
						also_represents += n.shortID() + " -> " + n.getType();
						printedNodes.add(n);
						i += 1;
					}
				}
			}

			jsBufferedWriter
					.write("{'id':" + id + " ,'label':'" + representativeNode.getLabel().replaceAll("['|’|«|»|\n]", " ")
							+ "','type':'" + ((representativeNode.getType()))
							+ "', 'parents':'' , 'also represents' : '" + also_represents + "'}");
		} catch (IOException IOE) {
			throw new IllegalStateException("Unable to draw node " + representativeNode.getId());
		}
	}

	public void printAllSources(ConnectionLens cl) {
		long totalNodeNumber = 0;
		Graph g;
		if (this.isAbstract()) {
			g = cl.graph().getAbstractGraph();
		} else if(this.isNormalized()) {
			g = cl.graph().getNormalizedGraph();
		} else {
			g = cl.graph();
		}

		for (DataSource ds : cl.catalog().getDataSources()) {
			totalNodeNumber += analyzeNodes(g, ds);
			if (totalNodeNumber >= 500) {
				// log.info("Balking out of drawing the graph (too many nodes).");
				return;
			}
		}

		printNodes();
		int i = 0;
		for (DataSource ds : cl.catalog().getDataSources()) {
			// draw the graph edges in DOT:
			i = printEdges(g, ds, i);
		}
		// also print the sameAs edges:
		printEdges(g.getSameAs(), i);
		closeAndDraw();
	}

	private boolean isAbstract() {
		return this.typeOfGraph == Graph.GraphType.ABSTRACT_GRAPH;
	}

	private boolean isNormalized() {
		return this.typeOfGraph == Graph.GraphType.NORMALIZED_GRAPH;
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import com.wcohen.ss.Jaro;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.SOURCE;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.TARGET;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofRelativeLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.withTypes;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix;

public class EntityPairProcessor extends CachedSimilarPairProcessor {

	private final Node.Types entityType;
	private final double lengthRatio;
	
	private final Jaro sim = new Jaro();

	protected EntityPairProcessor(StatisticsCollector stats, Node.Types t, String thParam) {
		this(stats, t, Config.getInstance().getDoubleProperty(thParam));
	}

	protected EntityPairProcessor(StatisticsCollector stats, Node.Types t, double th) {
		this(stats, t, th, Config.getInstance().getDoubleProperty("length_difference_ratio"));
	}

	protected EntityPairProcessor(StatisticsCollector stats, Node.Types t, double th, double lengthRatio) {
		super(stats, th);
		this.entityType = t;
		this.lengthRatio = lengthRatio;
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result = (withTypes(SOURCE, entityType) // source must be of this type
				.and(withTypes(TARGET, entityType)) 
				.and(sameStoredPrefix())
				// target is of the same type 
				.and(ofRelativeLength(lengthRatio)));
		if (Config.getInstance().getBooleanProperty("compare_stored_label_prefix")) {
			result = sameStoredPrefix().and(result); 
		}
		return result; 
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length()};
	}

//	@Override
//	public void recordStats(Object key, StatisticsCollector stats) {
//		stats.put(key,  JARO_NO, count);
//	}

	@Override
	public Double apply(String a, String b) {
		return sim.score(a, b);
	}
	


	public static class LocationPairProcessor extends EntityPairProcessor {
		public LocationPairProcessor(StatisticsCollector stats, double th) {
			super(stats, ENTITY_LOCATION, th);
		}

		public LocationPairProcessor(StatisticsCollector stats) {
			super(stats, ENTITY_LOCATION, "similarity_threshold_location");
		}
	}

}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class LeafK extends ScoringMethod {
    public static final Logger log = Logger.getLogger(LeafK.class);
    private int k;

    public LeafK(int k, Configuration configuration) {
        super(configuration);
        this.k = k;
    }

    @Override
    public void compute() {
        // get the number of leaves of each collection
        for(int collectionId : CollectionGraph.getInstance().getCollectionsIds()) {
            double nbLeaves;
            if(CollectionGraph.getInstance().getCollectionGraph().containsKey(collectionId)) {
                ArrayList<Integer> descendants = CollectionGraph.getInstance().getAllDescendants(collectionId, this.k, true);
                descendants = descendants.stream().filter(desc -> CollectionGraph.getInstance().getLeafCollections().contains(desc)).collect(Collectors.toCollection(ArrayList::new));
                nbLeaves = descendants.size();
                log.debug("number of leaves at depth " + this.k + " for " + collectionId + " is " + nbLeaves);
            } else {
                nbLeaves = 0.0d;
            }
//            this.scores.put(collectionId, nbLeaves);
            CollectionGraph.getWorkingInstance().setCollectionScore(collectionId, nbLeaves);
        }
//        log.debug(this.scores);
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
    }
}

package fr.inria.cedar.connectionlens.search;

import java.util.Map;

import com.google.common.collect.Maps;

public class ProvenanceSignature {

	Provenance finalProv;
	Map<Provenance,Short> signature;
	//short numGrows, numMerges, numGrow2Reps, numGrowSameAs, numMoESP;
	
	/**
	 * This returns the provenance signature in the form: (#INIT, #GROW, #MERGE, #MoESP).
	 * #MERGE includes the trees with special passes for merge, MERGESPECIAL.
	 */
	@Override
	public String toString() {
		String retVal = "(";
		
		if(signature.get(Provenance.INIT)!=null)
			retVal = retVal + signature.get(Provenance.INIT)+",";
		else
			retVal = retVal + "0,";
		
		if(signature.get(Provenance.GROW)!=null)
			retVal = retVal + signature.get(Provenance.GROW)+",";
		else
			retVal = retVal + "0,";
		
		int mergeCount = 0;
		if(signature.get(Provenance.MERGE)!=null)
			mergeCount = mergeCount + signature.get(Provenance.MERGE);
		if(signature.get(Provenance.MERGESPECIAL)!=null)
			mergeCount = mergeCount + signature.get(Provenance.MERGESPECIAL);
		
		retVal = retVal + mergeCount+",";
		
		if(signature.get(Provenance.MoESP)!=null)
			retVal = retVal + signature.get(Provenance.MoESP)+")";
		else
			retVal = retVal + "0)";
		
		return retVal;
		//return signature.toString();
	}

	public ProvenanceSignature() {
		this.signature = Maps.newHashMap();
	}
	
	public ProvenanceSignature(Provenance p) {
		this.signature = Maps.newHashMap();
		this.addProvenance(p);
	}
	
	public ProvenanceSignature(Map<Provenance, Short> signature) {
		this.signature = signature;
	}

	public void addProvenance(Provenance p){
		if(signature.get(p)==null) {
			signature.put(p, (short) 0);
		}
		short old = signature.get(p);
		signature.put(p, (short) (old+1));
		finalProv=p;
	}
	public void addProvenanceCount(Provenance p, Short count){
		signature.put(p, count);
	}
	
	/**
	 * The last operation that lead to creating this AT
	 */
	public enum Provenance {
		/**
		 * AT obtained initially from the index (1 node)
		 */
		INIT,
		/**
		 * AT obtained by local GROW
		 */
		GROW,
		/**
		 * AT obtained by GROW-TO-REPRESENTATIVE (currently it is not used, we are considering Grow To Representative as a grow operation)//TO DO: this needs to be discussed
		 */
		GROW_TO_REP,
		/**
		 * AT obtained by Merge
		 */
		MERGE,
		/**
		 * AT obtained by growing with a weak sameAs edge; 
		 */
		WEAK_SAME_AS,
		/**
		 * Redundant AT with a different root created by MoESP; 
		 */
		MoESP,
		/**
		 * Redundant AT with a different root created by special passes for merges at roots reachable from >2 kwds; 
		 */
		MERGESPECIAL; 
	}


	public static ProvenanceSignature copyOf(ProvenanceSignature provSign) {
		ProvenanceSignature ps = new ProvenanceSignature();
		for(Provenance p: provSign.signature.keySet())
			ps.addProvenanceCount(p, provSign.signature.get(p));
		return ps;
	}

	public static ProvenanceSignature merge(ProvenanceSignature provSign, ProvenanceSignature provSign2, boolean mergeSpecial) {
		ProvenanceSignature newps = new ProvenanceSignature();
		for(Provenance p: provSign.signature.keySet()) {
			if(provSign2.signature.get(p)!=null)
				newps.addProvenanceCount(p,(short) (provSign.signature.get(p)+provSign2.signature.get(p)));
			else
				newps.addProvenanceCount(p,(short) (provSign.signature.get(p)));
		}
		for(Provenance p: provSign2.signature.keySet()) {
			if(provSign.signature.get(p)!=null)
				newps.addProvenanceCount(p,(short) (provSign.signature.get(p)+provSign2.signature.get(p)));
			else
				newps.addProvenanceCount(p,(short) (provSign2.signature.get(p)));
		}
		if(mergeSpecial)
			newps.addProvenance(Provenance.MERGESPECIAL);
		else
			newps.addProvenance(Provenance.MERGE);
		return newps;
	}
	
}

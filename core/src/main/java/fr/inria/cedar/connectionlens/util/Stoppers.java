/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.score.Scorable;
import fr.inria.cedar.connectionlens.search.AnswerTree;

/**
 * Algorithm stoppers are predicate that are typically used to stop early long-running algorithms.
 *
 * @param <S> the generic type
 */
public interface Stoppers<S extends Scorable> extends Predicate<S[]> {
	
	boolean test(S... s);

	/** The logger. */
	Logger log = Logger.getLogger(Stoppers.class);
	
	/**
	 * Creates are new Timeout stopper.
	 *
	 * @param <S> the type of object on which to check the condition.
	 * @param ms the timeout period in milliseconds
	 * @return the new instance of Timeout object
	 */
	static <S extends Scorable> Stoppers<S> forward(Predicate<S[]> p) {
		return new ForwardingStopper<>(p);
	}
	
	/**
	 * Creates are new Timeout stopper.
	 *
	 * @param <S> the type of object on which to check the condition.
	 * @param ms the timeout period in milliseconds
	 * @return the new instance of Timeout object
	 */
	static <S extends Scorable> Timeout<S> timeout(long ms) {
		return new Timeout<>(ms);
	}
	
	/**
	 * Creates are new TopK stopper.
	 *
	 * @param <S> the type of object on which to check the condition.
	 * @param k the number of object after which to stop
	 * @return the new instance of TopK object
	 */
	static <S extends Scorable> FirstK<S> topK(long k) {
		//log.info("Create topK stopper");
		return new FirstK<>(k, new ConcurrentSkipListSet<>());
	}

	/**
	 * Creates are new TopK stopper.
	 *
	 * @param <S> the type of object on which to check the condition.
	 * @param k the number of object after which to stop
	 * @return the new instance of TopK object
	 */
	static <S extends Scorable> FirstK<S> topK(long k, SortedSet<S> set) {
		//log.info("Create topK stopper"); 
		return new FirstK<>(k, set);
	}
	
	/**
	 * Creates are new TopK stopper.
	 *
	 * @param <S> the type of object on which to check the condition.
	 * @param k the timeout period in milliseconds between answers
	 * @return the new instance of Stationarity object
	 */
	static <S extends Scorable> Stationarity<S> stationarity(long k) {
		//log.info("Create topK stopper");
		return new Stationarity<>(k);
	}
	
	
	/**
	 * A global timeout.
	 *
	 * @param <S> the generic type
	 */
	public static class Timeout<S extends Scorable> implements Stoppers<S> {
		/** The logger. */
		Logger log = Logger.getLogger(Timeout.class);
		
		/** The start time. */
		final long start = System.currentTimeMillis();
		
		
		/** The time period after which to stop. */
		final long duration;
		
		/**
		 * Instantiates a new timeout.
		 *
		 * @param d the time period after which to stop
		 */
		Timeout(long d) {
			//log.info("Timeout stopper(" + d + ")"); 
			this.duration = d;
		}

		@Override
		public boolean test(S... u) {
			boolean done = ( (System.currentTimeMillis() - start) > duration); 
			if (done) {
				log.info("Timeout!"); 
			}
			return done; 
		}
	}

	/**
	 * A global top-k, that will stop after a fixed number of item have been observed.
	 *
	 * @param <S> the type object to based the condition on.
	 */
	public static class ForwardingStopper<S extends Scorable> implements Stoppers<S> {
		/** The logger. */
		Logger log = Logger.getLogger(ForwardingStopper.class);
	
		/** The number of items after which to stop. */
		final Predicate<S[]> delegate;

		/**
		 * Instantiates a new top K.
		 *
		 * @param k the number of items after which to stop
		 */
		ForwardingStopper(Predicate<S[]> d) { 
			this.delegate = d;
		}

		@Override
		public boolean test(S... s) {
			return delegate.test(s);
		}
	}

	/**
	 * A global top-k, that will stop after a fixed number of item have been observed.
	 *
	 * @param <S> the type object to based the condition on.
	 */
	public static class FirstK<S extends Scorable> implements Stoppers<S> {
		/** The logger. */
		Logger log = Logger.getLogger(FirstK.class);
		
		/** The number of items after which to stop. */
		final long k;
		final SortedSet<S> firstK;
		long current; 
	
		/**
		 * Instantiates a new top K.
		 *
		 * @param k the number of items after which to stop
		 */
		FirstK(long k, SortedSet<S> s) { 
			this.k = k;
			this.firstK = s;
			current = 0; 
		}

		@Override
		public boolean test(S... p) {
			for (S s: p) {
				if (this.test(s)) {
					return true;
				}
			}
			return false;
		}

		private boolean test(S p) {
			// Do not measure the size of the SortedSet, this is horribly slow!!!
			// https://gitlab.inria.fr/cedar/connection-lens/-/issues/624#note_597138
			if (current < k) {
				firstK.add(p); 
				current ++;
			}
			if (current == k) {
				return true; 
			}
			return false; 
		}
		
		public Set<S> topk() {
			return this.firstK;
		}
	}

	/**
	 * A global timeout after which to stop when no new answers are found.
	 *
	 * @param <S> the generic type
	 */
	public static class Stationarity<S extends Scorable> implements Stoppers<S> {
		/** The logger. */
		Logger log = Logger.getLogger(Stationarity.class);
		
		/** The last answer time. */
		long lastAnsTime;
		
		
		/** The time period after which to stop. */
		final long duration;
		
		/**
		 * Instantiates a new stationarity stopper.
		 *
		 * @param d the time period after which to stop when no new answers are found.
		 */
		Stationarity(long d) {
			//log.info("Stationarity stopper(" + d + ")"); 
			this.duration = d;
		}

		@Override
		public boolean test(S... u) {
			log.info("Stationarity Stopper called!"+u.length);
			boolean done = ( (System.currentTimeMillis() - lastAnsTime) > duration); 
			if (done) {
				log.info("Timeout for Stationarity Stopper!"); 
				return done;
			}
			 
			for (S s: u) {
				if (this.test(s)) {
					return true;
				}
			}
			return false;
		}
		
		private boolean test(S p) {
			this.lastAnsTime=System.currentTimeMillis();
			return false; 
		}
	}

}

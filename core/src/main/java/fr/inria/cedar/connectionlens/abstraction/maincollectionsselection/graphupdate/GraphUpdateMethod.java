package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate;

import java.io.IOException;
import java.sql.SQLException;

public class GraphUpdateMethod {
    int cstar;

    public GraphUpdateMethod(int cstar) {
        this.init(cstar);
    }

    public void init(int cstar) {
        this.cstar = cstar;
    }

    public void update() throws SQLException, IOException {
        throw new IllegalStateException();
    }
}

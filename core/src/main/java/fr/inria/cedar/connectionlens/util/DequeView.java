/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import com.google.common.collect.ForwardingDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

/**
 * A Deque view.
 *
 * @param <E> the element type
 */
public final class DequeView<E> extends ForwardingDeque<E> {
	
	/** The delegate deque. */
	private final Deque<E> delegate;
	
	/**
	 * Instantiates a new deque view.
	 *
	 * @param d the delegate deque
	 */
	DequeView(Deque<E> d) {
		this.delegate = d;
	}

	/**
	 * Creates a fresh Deque view based on the given deque
	 *
	 * @param <E> the element type
	 * @param l the delegate deque
	 * @return the deque view
	 */
	public static <E> DequeView<E> of(Deque<E> l) {
		return new DequeView<>(l);
	}
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#delegate()
	 */
	@Override
	protected Deque<E> delegate() {
		return delegate;
	}

	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#add(java.lang.Object)
	 */
	@Override
	public boolean add(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends E> c) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingCollection#clear()
	 */
	@Override
	public void clear() { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#addFirst(java.lang.Object)
	 */
	@Override
	public void addFirst(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#addLast(java.lang.Object)
	 */
	@Override
	public void addLast(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#offerFirst(java.lang.Object)
	 */
	@Override
	public boolean offerFirst(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#offerLast(java.lang.Object)
	 */
	@Override
	public boolean offerLast(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#removeFirst()
	 */
	@Override
	public E removeFirst() { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#removeLast()
	 */
	@Override
	public E removeLast()  { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#pollFirst()
	 */
	@Override
	public E pollFirst() { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#pollLast()
	 */
	@Override
	public E pollLast()  { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#removeFirstOccurrence(java.lang.Object)
	 */
	@Override
	public boolean removeFirstOccurrence(Object o) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#removeLastOccurrence(java.lang.Object)
	 */
	@Override
	public boolean removeLastOccurrence(Object o)  { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingQueue#offer(java.lang.Object)
	 */
	@Override
	public boolean offer(E e) { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingQueue#remove()
	 */
	@Override
	public E remove()  { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingQueue#poll()
	 */
	@Override
	public E poll() { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#push(java.lang.Object)
	 */
	@Override
	public void push(E e)  { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#pop()
	 */
	@Override
	public E pop() { throw new UnsupportedOperationException(); }
	
	/**
	 * {@inheritDoc}
	 * @see com.google.common.collect.ForwardingDeque#descendingIterator()
	 */
	@Override
	public Iterator<E> descendingIterator() { throw new UnsupportedOperationException(); }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;


import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author Tayeb Merabti
 */
public class XMLPolicies extends DocPolicies {

    private static XMLPolicies xmlPolicies;

    /** The location of the default policies file. */
    public static final String DEFAULT_CONFIG_FILE_PATH = "xml.policies";

    /** Interpreter logger. */
    static Logger log = Logger.getLogger(XMLPolicies.class);

    public XMLPolicies (String path) {

        props = new Properties();
        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path);
            InputStreamReader reader = new InputStreamReader(in);
            props.load(reader);
            return;

        } catch (IOException e) {
            log.warn("Loading failed: " + e.getMessage());
        }

        try(InputStreamReader reader = new FileReader(path)) {
            props.load(reader);
            return;
        } catch (final IOException e) {
            log.warn("Loading failed: " + e.getMessage());
        }
    }

    public static XMLPolicies getInstance (String path) {
        if (xmlPolicies == null) {
            xmlPolicies = new  XMLPolicies(path);
        }
        return xmlPolicies;
    }


    public static XMLPolicies getInstance() {
        return getInstance(DEFAULT_CONFIG_FILE_PATH);
    }
}

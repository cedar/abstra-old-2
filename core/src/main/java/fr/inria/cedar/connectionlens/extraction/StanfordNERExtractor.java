/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import static java.util.Locale.FRENCH;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ie.crf.CRFCliqueTree;
import edu.stanford.nlp.international.french.process.FrenchTokenizer;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.XT_SNER_T;

/**
 * StanfordNER implementation of an EntityExtractor.
 */
public class StanfordNERExtractor extends TopLevelExtractor {

	/** The serialized classifier. */
	private final String pathToModel;

	/** The classifier. */
	private final CRFClassifier<CoreLabel> classifier;

	/** The label label for when word is not any of the tags. */
	String labelOther = "O";

	/** French Tokenizer properties */
	static Properties props;

	String configStanfordModel = Paths.get(Config.getInstance().getStringProperty("stanford_models")).toString();

	/**
	 * Instantiates a new Stanford NER extractor.
	 *
	 * @param locale the locale
	 */
	public StanfordNERExtractor(Locale locale) {
		super(locale);
		log = Logger.getLogger(StanfordNERExtractor.class);
		// System.out.println("This is the Stanford Named Entity Extractor, the locale
		// is: " + this.locale);
		// System.out.println("The model is expected at: " + pathTotheModel);
		ResourceBundle bundle = ResourceBundle.getBundle("corenlp", locale);

		this.pathToModel = Paths.get(configStanfordModel, bundle.getString("model_location")).toString();
		// log.info("The serialized classifier is expected at: " +
		// serializedClassifier);
		try {
			this.classifier = CRFClassifier.getClassifier(new File(pathToModel));
		} catch (ClassNotFoundException | IOException e) {
			throw new IllegalArgumentException(e);
		}

		props = new Properties();
		props.setProperty("strictTreebank3", "true");
		props.setProperty("untokenizable", "allKeep");
		props.setProperty("escapeForwardSlashAsterisk", "false");
		props.setProperty("normalizeFractions", "false");
		props.setProperty("normalizeAmpersandEntity", "false");
		props.setProperty("invertible", "true");

		// when used in a normal setting, CL sets its statistics.
		// when used in the tests, the extractor is created
		// "standalone" and there is no statistics to be passed from CL.
		// this avoids NPEs in tests.
		if (stats == null) {
			stats = new StatisticsCollector();
		}

	}

	@Override
	public String getExtractorName() {
		return "SNER";
	}

	@Override
	public Locale getLocale() {
		return this.locale;
	}

	@Override
	public void flushMemoryCache() {

	}

	@Override
	/**
	 * Unused when extraction result in cache.
	 */
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		String s = input;
		if (locale.equals(FRENCH)) {
			s = PreProcessing.getTokenizeInput(input);
		}
		if(s.contains("^^<"))
			s = s.substring(0, s.lastIndexOf("^^<")); // remove the RDF type if it exists (in RDF literals only)
		return extractEntities(ds, classifier.classify(s), input, structuralContext);
	}

	/**
	 * @param ds  the data source
	 * @param out the SNER library output
	 * @return the list of entities found in out. Unused when extraction result in
	 *         cache
	 */
	private List<Entity> extractEntities(DataSource ds, List<List<CoreLabel>> out, String input,
			int structuralContext) {
		long t1 = System.currentTimeMillis();
		List<CoreLabel> outFlat = out.stream().flatMap(List::stream).collect(Collectors.toList());

		List<Entity> result = new ArrayList<>();

		if (outFlat.isEmpty()) {
			return result;
		}
		// log.info("OutFlat is: " + outFlat);
		// to extract confidence score
		CRFCliqueTree<String> cliqueTree = classifier.getCliqueTree(outFlat);

		// the same occurrence of an entity may have many instances at different offsets
		// in the text
		List<Entity> entities = new LinkedList<>();
		String dsURL = ds.getLocalURI().toString();
		for (int w = 0; w < outFlat.size(); w++) {

			CoreLabel word = outFlat.get(w);
			String type = word.get(CoreAnnotations.AnswerAnnotation.class);
			// if it is an entity
			if (!type.equals(labelOther)) {
				Types entityType = Types.valueOf("ENTITY_" + type);
				String value = word.word(); // get Occurrence::value
				String commonName = value;
				// get confidence score
				int typeIndex = classifier.classIndex.indexOf(type);
				double confidence = cliqueTree.prob(w, typeIndex);
				long offset = word.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class); // get Occurrence::offset

				Entity entity = new Entity(value, commonName, confidence, offset, entityType, dsURL);

				if (w - 1 < 0 || entities.size() == 0) {
					entities.add(entity);
					continue;
				}

				// check word before
				String typeBefore = outFlat.get(w - 1).get(CoreAnnotations.AnswerAnnotation.class);

				// if word before has the same tag
				if (typeBefore.equals(type)) {
					// if previous word has the same tag, it was the last added to the entities list
					Entity entityBefore = entities.get(entities.size() - 1);
					String valueBefore = entityBefore.value();
					long offsetBefore = entityBefore.offset();
					int lengthBefore = entityBefore.length();
					double confidenceBefore = entityBefore.confidence();
					String newValue;
					if (offsetBefore + lengthBefore == offset) {
						newValue = valueBefore + value;
					} else {
						newValue = valueBefore + " " + value;
					}

					long newOffset = offsetBefore;
					String newCommonName = newValue;

					// new confidence is the mean
					int numWords = valueBefore.split(" ").length;
					double newConfidence = (confidenceBefore * numWords + confidence) / (numWords + 1);

					entities.set(entities.size() - 1,
							new Entity(newValue, newCommonName, newConfidence, newOffset, entityType, dsURL));

				} else {
					Entity newEntity = new Entity(value, commonName, confidence, offset, entityType, dsURL);
					entities.add(newEntity);
				}
			}
		}
		long t2 = System.currentTimeMillis();
		stats.increment(StatisticsCollector.total(), XT_SNER_T, (new Long(t2 - t1)).intValue());
		List<Entity> allEntities = PostProcessing.separate_words(entities);

		if (disambiguation) {
			for (Entity entity : allEntities) {
				// log.info("ENTITY: " + entity.commonName() + " " + entity.type().name() + "
				// context " + structuralContext);
				disambiguateIfNeeded(ds, entity, structuralContext, input);
			}
		}
		return PostProcessing.separate_words(entities);

	}

}

class PostProcessing {

	static public List<Entity> separate_words(List<Entity> entities) {
		for (int l = entities.size() - 1; l >= 0; l--) {

			Entity ent = entities.get(l);

			String value = ent.value();
			double confidence = ent.confidence();
			Types type = ent.type();
			long offset = ent.offset();
			String ds = ent.docID();

			String[] newWords = value.split("\\xa0");

			if (newWords.length > 1) {

				int i = 0;

				for (int w = l + 1; w < l + newWords.length; w++) {
					String wValue = newWords[i];
					long wOffset = offset;

					if (w == entities.size())
						entities.add(new Entity(wValue, wValue, confidence, wOffset, type, ds));
					else
						entities.add(w, new Entity(wValue, wValue, confidence, wOffset, type, ds));

					wOffset += wValue.length();
					i++;
				}
				entities.remove(l);
			}
		}

		return entities;

	}

}

class PreProcessing {

	static public String getTokenizeInput(String input) {

		String tokenized = "";

		FrenchTokenizer<CoreLabel> ptbtf = null;
		ptbtf = new FrenchTokenizer<>(new StringReader(input), new CoreLabelTokenFactory(), StanfordNERExtractor.props,
				false);
		while (ptbtf.hasNext()) {
			CoreLabel label = ptbtf.next();
			String ori = label.get(CoreAnnotations.OriginalTextAnnotation.class);
			tokenized += ori + " ";
		}
		return tokenized;
	}

}

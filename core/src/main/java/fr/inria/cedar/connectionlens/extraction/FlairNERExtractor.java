/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.extraction;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.DISAMB_T;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.XT_CALL_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.XT_FLAIR_T;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.collect.Lists;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.PythonUtils;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
public class FlairNERExtractor extends TopLevelExtractor {

	Process flairExtractorProcess;

	int i = 0;

	DataOutputStream os; 
	StringBuilder jsonString; 
	String restUrl; 
	InputStream inputStream; 
	BufferedReader br; 
	String s;
	
	static URL flairExtractorServiceURL;  

	
	static long overallExtractionTime = 0;
	static long justFlairWorkingTime = 0; 
	static long URLManagementTime = 0;
	static long gobbleTime = 0; 
	

	public FlairNERExtractor(Locale locale) throws IOException {	
		super(locale); 		
		this.log = Logger.getLogger(FlairNERExtractor.class);
		log.info("This is the Flair Named Entity Extractor (NER), locale is: " + this.locale);

		flairExtractorServiceURL = new URL("http://127.0.0.1:5000/fner");

		
		String language;
		if (locale == null) {
			language = Locale.forLanguageTag(Config.getInstance().getProperty("default_locale")).toString();
		} else {
			language = locale.getLanguage();
		}
		// Check if there is an instance of Flask running.
		// If yes, kill the process to start a new instance.
//		if (FlairNERExtractor.isFlairInstalledAndRunning()) {
//			log.info("An instance of Flask is running");
//			PythonUtils.getInstance().stopFlairExtractor();
//		}

		try {
			flairExtractorProcess = PythonUtils.getInstance().runFlairExtractor(language);
			StreamGobbler errorGobbler = new StreamGobbler(flairExtractorProcess.getErrorStream(), "QUERY");
			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(flairExtractorProcess.getInputStream(), "OUT");
			// kick them off
			errorGobbler.start();
			outputGobbler.start();	

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Collection<Entity> run(DataSource ds, URL input, int structuralContext) {
		try {
			return run(ds, IOUtils.slurpURL(input), structuralContext);
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
			return Lists.newArrayList();
		}
	}

	@Override
	public String getExtractorName() {
		return "flairNER";
	}


	
	/** IM method factorized from getFlairRequest where it appeared twice */
	private String sanitize1(String s) {
		return s.replaceAll("\\@[a-z]{2,}"," ")
				.replaceAll("[^a-zA-Z0-9’'.,_\u00C0-\u017F\\-\\s\\^^]", "")
				.replaceAll("''", "").replaceAll("\\^+", " ");
	}
	/** IM method factorized from getFlairRequest where it appeared twice */
	private String sanitize2(String s) {
		return s.replaceAll("[^a-zA-Z0-9’'.,_\u00C0-\u017F\\-\\s\\^^]", "");
	}
	/**
	 @param inputs
	 @return build the new JSON Flask Request
	 */
	private String getFlairRequest(ArrayList<Pair<Node, String>> inputs){
		JSONObject snippets = new JSONObject();
		JSONArray allInputs = new JSONArray();

		if(inputs.size() == 1
				&& inputs.get(0).getLeft() == null){
			JSONArray inputArray = new JSONArray();
			inputArray.put("1");
			String inputText = sanitize1(inputs.get(0).getRight()); 
			if(inputText.trim().isEmpty()){
				inputText = sanitize2(inputs.get(0).getRight()); 
			}
			inputArray.put(inputText);
			allInputs.put(inputArray);
		}
		else{
			for(Pair<Node, String> input : inputs){
				JSONArray inputArray = new JSONArray();
				input.getLeft().getDataSource();
				inputArray.put(input.getLeft().getId().value());
				String inputText = sanitize1(input.getRight()); 
				if(inputText.trim().isEmpty()){
					inputText = sanitize2(input.getRight()); 
				}
				inputArray.put(inputText);
				allInputs.put(inputArray);
			}
		}
		if(allInputs.length() > 0){
			snippets.put("snippets", allInputs);
			return snippets.toString();
		}
		return "";
	}


	
	@Override
	/** Calls the Flair extraction service */
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		Collection<Entity> entities = new LinkedList<>();
		ArrayList<Pair<Node, String>> inputs = new ArrayList<>();
		inputs.add(Pair.of(null,input));
		String flairRequest = getFlairRequest(inputs);
		if(!flairRequest.isEmpty()){
			String flairMultipleAnnotation = null;
			try{
				long t1 = System.currentTimeMillis(); 
				flairMultipleAnnotation = callFlairExtractionServiceWithMessage(flairRequest.toString());
				long t2 = System.currentTimeMillis(); 
				stats.increment(ds.getLocalURI(), XT_CALL_NO);
				stats.increment(StatisticsCollector.total(), XT_CALL_NO);	
				stats.increment(ds.getLocalURI(), XT_FLAIR_T, (new Long(t2-t1)).intValue());
				stats.increment(StatisticsCollector.total(), XT_FLAIR_T, (new Long(t2-t1)).intValue());
				entities = extractSingleInputEntity(ds, flairMultipleAnnotation,structuralContext);
			} catch(IOException e){
				e.printStackTrace();
			}
		}
		return entities;
	}




	@Override
	public ArrayList<Pair<Node, Collection<Entity>>> run(ArrayList<Pair<Node, String>> inputs) {
		HashMap<Object,Node> NodeId = new HashMap<>();
		ArrayList<Pair<Node, Collection<Entity>>>  results = new ArrayList<>();
		for(Pair<Node, String> input:inputs){
			NodeId.put(input.getLeft().getId().value(),input.getLeft());
		}

		
		String flairRequest = getFlairRequest(inputs);
		if(!flairRequest.isEmpty()){
			String flairMultipleAnnotation = null;
			try{
				long t1 = System.currentTimeMillis(); 
				flairMultipleAnnotation = callFlairExtractionServiceWithMessage(flairRequest);
				long t2 = System.currentTimeMillis(); 
				stats.increment(StatisticsCollector.total(), XT_CALL_NO);
				stats.increment(StatisticsCollector.total(), XT_FLAIR_T, (new Long(t2-t1)).intValue());
				results = extractMultipleInputs(NodeId, flairMultipleAnnotation);
			} catch(IOException e){
				e.printStackTrace();
			}
		}	
		return results;
	}


	/**
	 update entities list with ambiID
	 @param extractedEntities
	 @param ds
	 @param context
	 @param textWithEntity
	 */
	private void updateEntitiesWithAmbiverse(List<Entity> extractedEntities, DataSource ds, int context, String textWithEntity){
		long t1 = System.currentTimeMillis();
		if(disambiguation && extractedEntities.size() > 0){
			for(Entity entity : extractedEntities){
				if(!this.disambigPolicy.requestDisambiguation(ds, entity.type(), context)){
					log.info("No disambiguation on " + entity.commonName() + " " + entity.type().name());
					continue;
				}
				String ambiverseRequest = EntityExtractor.getAmbRequest(entity, textWithEntity, locale.getLanguage().toString());
				try{
					String responseFromAmbiverse = EntityExtractor.getAnnotationDisambiguated(ambiverseRequest);
					if(!isNullOrEmpty(responseFromAmbiverse)){
						JSONArray disambiguationMatches = new JSONObject(responseFromAmbiverse).getJSONArray("matches");
						if(disambiguationMatches.length() > 0){
							for(int j = 0; j < disambiguationMatches.length(); j++){
								JSONObject disambmatch = (JSONObject) disambiguationMatches.get(j);
								JSONObject entityAmbiverse = disambmatch.getJSONObject("entity");
								if(entityAmbiverse.length() > 0){
									if(entity != null){
										entity.setAmbiID(entityAmbiverse.get("id").toString());
									}
								}

							}
						}
					}

				} catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		long t2 = System.currentTimeMillis();
		stats.increment(StatisticsCollector.total(), DISAMB_T, (new Long(t2-t1)).intValue());
	}


	/**
	 Extract only entities for single input and with node null
	 @param ds
	 @param flairMultipleAnnotation
	 @param context
	 @return
	 */
	private Collection<Entity> extractSingleInputEntity(DataSource ds, String flairMultipleAnnotation, int context){

		List<Entity> extractedEntities =  new LinkedList<>();
		if(!isNullOrEmpty(flairMultipleAnnotation)){
			JSONArray allMatches = new JSONObject(flairMultipleAnnotation).getJSONArray("snippets");
			if(allMatches.length() > 0){
				for(int i = 0; i < allMatches.length(); i++){
					JSONArray match = (JSONArray) allMatches.get(i);
					JSONObject entities = match.getJSONObject(1);
					JSONArray allEntities = entities.getJSONArray("entities");
					String textWithEntity = entities.getString("text");
					extractedEntities = extractInputEntities(ds, allEntities);
					if(disambiguation && extractedEntities.size() > 0){
						updateEntitiesWithAmbiverse(extractedEntities,ds,context,textWithEntity);
					}
				}
			}
		}
		return extractedEntities;
	}
	/**

	 @return extract multiple or one annotation and put them into a Map
	         according to the new implementation of Parallel extraction.
	 @param nodeIds
	 @param flairMultipleAnnotation
	 */
	private ArrayList<Pair<Node, Collection<Entity>>> extractMultipleInputs(HashMap<Object, Node> nodeIds,
																			String flairMultipleAnnotation){
		ArrayList<Pair<Node, Collection<Entity>>>  results = new ArrayList<>();

		if (!isNullOrEmpty(flairMultipleAnnotation)) {

			JSONArray allMatches = new JSONObject(flairMultipleAnnotation).getJSONArray("snippets");

			if (allMatches.length() > 0) {
			for (int i = 0; i < allMatches.length(); i++) {
					JSONArray match = (JSONArray) allMatches.get(i);
					JSONObject entities = match.getJSONObject(1);
					JSONArray allEntities = entities.getJSONArray("entities");
					Node nodeWithInput = nodeIds.get(match.get(0));
					String textWithEntity = entities.getString("text");
					List<Entity> extractedEntities = extractInputEntities(nodeWithInput.getDataSource(), allEntities);
					if (disambiguation && extractedEntities.size() > 0) {
						updateEntitiesWithAmbiverse(extractedEntities,nodeWithInput.getDataSource(),
								nodeWithInput.getContext(),textWithEntity);
					}
				;
				results.add(Pair.of(nodeWithInput,extractedEntities));
				}
			}
		}
		return results;
	}


	/**
	 Calls the Flask Web service encapsulating the Flair entity extractor
	 (based on https://gitlab.inria.fr/cedar/cl_extraction_simplified, version=38649657)
	 @param flairRequestAnnotation
	 @return
	 @throws IOException
	 */
	public String callFlairExtractionServiceWithMessage(String flairRequestAnnotation) throws IOException {
		DataOutputStream os = null;
		StringBuilder jsonString = new StringBuilder();
		HttpURLConnection conn = (HttpURLConnection)this.flairExtractorServiceURL.openConnection();
		try {
			conn.setRequestMethod("POST");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("accept", "application/json");
			conn.setConnectTimeout(5000);
			byte[] postData = flairRequestAnnotation.getBytes(StandardCharsets.UTF_8);
			conn.setDoOutput(true);
			os = new DataOutputStream(conn.getOutputStream());
			os.write(postData);
			os.flush();
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			InputStream inputStream = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader((inputStream)));
			String s;
			while ((s = br.readLine()) != null) {
				jsonString.append(s);
			}
			br.close();
			conn.disconnect();
		} finally {
			conn.disconnect();
		}
		return jsonString.toString();
	}

	
	
	/**
	 * Extract an Entity object from a JSON (before postprocessing, each entity has
	 * only one occurrence, but each occurrence has many mentions)
	 *
	 * @return the entity if jso is a proper JSONObject, null otherwise
	 */
	private String extractEntitiesToAmbiverse(JSONArray allEntityObjects, String text) {

		checkNotNull(allEntityObjects);
		JSONObject annotatedMentions = new JSONObject();
		JSONArray allAmbEntities = new JSONArray();
		String originalText = "";
		for (int i = 0; i < allEntityObjects.length(); i++) {
			JSONObject specificEntity = (JSONObject) allEntityObjects.get(i);
			originalText = ((JSONObject) allEntityObjects.get(i)).getString("text");
			originalText = originalText.replaceAll("[^a-zA-Z0-9’'.,_\u00C0-\u017F\\-\\s]", " ").replaceAll("'", "''");
			// log.info(originalText);
			JSONArray entities = specificEntity.getJSONArray("entities");
			for (int j = 0; j < entities.length(); j++) {
				// get the mapped entity type
				JSONObject entityObject = new JSONObject();
				JSONObject entity = entities.getJSONObject(j);
				Node.Types type = EntityType.mapToEntity(entity.getString("type"));
				String commonName = entity.getString("text");

				if (type == null || commonName.matches("[\\\\p{Punct}@',&]")) {
					continue;
				}

				double confidence = 0.0;
				try {
					confidence = entity.getDouble("confidence");
				} catch (JSONException e) {
					// in case JSONObject doesn't have confidence object,
					// catch the exception and let the program continue quietly
				}

				String value = entity.getString("text");
				value = value.replaceAll("[^a-zA-Z0-9’'.,_\u00C0-\u017F\\-\\s]", " ").replaceAll("'", "''");
				long offset = entity.getLong("start_pos");
				int length = value.length();
				entityObject.put("confidence", confidence);
				entityObject.put("charLength", length);
				entityObject.put("charOffset", originalText.indexOf(value));
				entityObject.put("type", type.toString().split("ENTITY_")[1].substring(0, 3));
				allAmbEntities.put(entityObject);
			}
		}
		if (!isNullOrEmpty(allAmbEntities.toString()) && allAmbEntities.length() > 0) {

			annotatedMentions.put("annotatedMentions", allAmbEntities);
			annotatedMentions.put("text", originalText);
			annotatedMentions.put("docId", "doc");
			annotatedMentions.put("language", locale.getLanguage().toString());
			// annotatedMentions.put("language", "en");
			annotatedMentions.put("extractConcepts", "false");
			// log.info(annotatedMentions);
			return annotatedMentions.toString();
		}
		return "";

	}

	private static List<Entity> extractInputEntities(DataSource ds, JSONArray entities) {
		checkNotNull(entities);
		List<Entity> occ = new LinkedList<>();
			for (int j = 0; j < entities.length(); j++) {
				// get the mapped entity type
				JSONObject entity = entities.getJSONObject(j);
				Node.Types type = EntityType.mapToEntity(entity.getString("type"));
				String commonName = entity.getString("text");

				if (type == null || commonName.matches("[\\\\p{Punct}@',&]")) {
					continue;
				}

				// get Occurrence::confidence (only PERSON is provided a confidence score)
				double confidence = 0.0;
				try {
					confidence = entity.getDouble("confidence");
				} catch (JSONException e) {
					// in case JSONObject doesn't have confidence object,
					// catch the exception and let the program continue quietly
				}
				String value = entity.getString("text");
				long offset = entity.getLong("start_pos");
				int length = value.length();
				occ.add(new Entity(value, commonName, confidence, offset, type, ds.getLocalURI().toString()));
			}
		return occ;
	}


	/**
	 * Extract an Entity object from a JSON (before postprocessing, each entity has
	 * only one occurrence, but each occurrence has many mentions)
	 *
	 * @return the entity if jso is a proper JSONObject, null otherwise
	 */
	private static List<Entity> extractEntities(DataSource ds, JSONArray allEntityObjects) {
		checkNotNull(allEntityObjects);
		List<Entity> occ = new LinkedList<>();
		for (int i = 0; i < allEntityObjects.length(); i++) {
			JSONObject specificEntity = (JSONObject) allEntityObjects.get(i);
			JSONArray entities = specificEntity.getJSONArray("entities");
			for (int j = 0; j < entities.length(); j++) {
				// get the mapped entity type
				JSONObject entity = entities.getJSONObject(j);
				Node.Types type = EntityType.mapToEntity(entity.getString("type"));
				String commonName = entity.getString("text");

				if (type == null || commonName.matches("[\\\\p{Punct}@',&]")) {
					continue;
				}

				// get Occurrence::confidence (only PERSON is provided a confidence score)
				double confidence = 0.0;
				try {
					confidence = entity.getDouble("confidence");
				} catch (JSONException e) {
					// in case JSONObject doesn't have confidence object,
					// catch the exception and let the program continue quietly
				}
				String value = entity.getString("text");
				long offset = entity.getLong("start_pos");
				int length = value.length();
				occ.add(new Entity(value, commonName, confidence, offset, type, ds.getLocalURI().toString()));
			}
		}
		return occ;
	}

	class StreamGobbler extends Thread {
		InputStream is;
		String type;
		FileWriter writer;

		StreamGobbler(InputStream is, String type) throws IOException {
			long tt1 = System.currentTimeMillis();
			File directory = new File(Config.getInstance().getProperty("temp_dir"));
			File tmpFile = File.createTempFile("log_flairNER", ".tmp",directory);
			tmpFile.deleteOnExit();
			writer = new FileWriter(tmpFile);
			this.is = is;
			this.type = type;
			long tt2 = System.currentTimeMillis();
			FlairNERExtractor.addGobbleTime(tt2-tt1); 
		}

		public void run() {
			long tt1 = System.currentTimeMillis();	
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
					writer.write(line);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			long tt2 = System.currentTimeMillis();
			FlairNERExtractor.addGobbleTime(tt2-tt1); 
		}
	}

	/**
	 * @return True if an instance of Flask-Flair is running
	 */
	public static boolean isFlairInstalledAndRunning() {
		final Socket socket;
		try {
			socket = new Socket(flairExtractorServiceURL.getHost(), flairExtractorServiceURL.getPort());
		} catch (IOException e) {
			return false;
		}
		try {
			socket.close();
		} catch (IOException e) {
		}
		return true;
	}

	// IM: performance trouble-shooting, 27/11/2020
	public static void addGobbleTime(long l) {
		gobbleTime += l; 
	}
	public static int getOverallExtractionTime(){
		//System.out.println("Overall extracting: " + overallExtractionTime); 
		return (int)overallExtractionTime; 
	}
	public static int getJustFlairWorkingTime(){
		//System.out.println("Overall just Flair:"  +  justFlairWorkingTime); 
		return (int)justFlairWorkingTime; 
	}
	public static int getURLManagementTime(){
		//System.out.println("Overall URL mgmt:"  +  URLManagementTime); 
		return (int)URLManagementTime; 
	}
	public static int getGobbleTime(){
		//System.out.println("Overall gobble time:"  +  gobbleTime); 
		return (int)gobbleTime; 
	}
	
	@Override
	public Class getBasicExtractorClass() {
		return this.getClass(); 
	}

}
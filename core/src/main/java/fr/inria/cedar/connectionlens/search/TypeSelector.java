package fr.inria.cedar.connectionlens.search;

import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import static fr.inria.cedar.connectionlens.graph.Node.Types;

public class TypeSelector implements Comparable<QueryComponent>, QueryComponent {
	private static int allocatedTypeSelectors = 0; 
	Types nodeType;
	private int myID = 0;
	
	public TypeSelector(String nodeTypeName) {
		String typeName = nodeTypeName.toLowerCase();
		if (typeName.equals("person")) {
			nodeType = Types.ENTITY_PERSON;
		}
		else {
			if (typeName.equals("organization")){
				nodeType = Types.ENTITY_ORGANIZATION;
			}
			else {
				if (typeName.equals("location")) {
					nodeType = Types.ENTITY_LOCATION; 
				}
				else {
					throw new IllegalStateException(nodeTypeName + " not supported yet. Try type:Person, type:Organization or type:Location instead"); 
				}
			}
		}
		myID = allocatedTypeSelectors;
		allocatedTypeSelectors ++;
	}

	@Override
	public String value() {
		StringBuffer sb = new StringBuffer("T");
		switch(nodeType) {
		case ENTITY_PERSON: sb.append("Person"); break;
		case ENTITY_ORGANIZATION: sb.append("Organization"); break;
		case ENTITY_LOCATION: sb.append("Location"); break; 
		default: throw new IllegalStateException("Wrong type");
		}
		sb.append(this.myID);
		return new String(sb); 
	}

	public Types getNodeType() {
		return nodeType; 
	}
	public String toString() {
		return value();
	}

	@Override
	public int compareTo(QueryComponent o) {
		return this.value().compareTo(o.value());
	}
}

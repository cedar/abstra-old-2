/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Common interface to vendor-specific entity extraction implementations. There
 * are 2 ways to use it:
 * <ul>
 * <li>to extract entities from a file (plain text document): call
 * <code>chooseDocument(filename)</code>, then <code>run()</code></li>
 * <li>to extract entities from a string (during indexing phase): call
 * <code>extractEntities(Map<Item, String>)</code></li>
 * </ul>
 */
public interface EntityExtractor {

	/**
	 * Run the extractor. In all but test cases, we can run from Node; some tests just start from a DataSource. 
	 */
	Collection<Entity> run(DataSource ds, String input, int structuralContext); 
	
	
	/**
	 * Run the extractor on the given input file
	 */
	default Collection<Entity> run(DataSource ds, URL input, int structuralContext) {
		try {
			return run(ds, IOUtils.slurpURL(input), structuralContext);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	String getExtractorName();

	Locale getLocale();

	void flushMemoryCache();

	void setStats(StatisticsCollector extractStats);
	
	/**
	 * 
	 * @return The basic extractor class (TopLevel, SNER or Flair) within an entity extractor.
	 * We need this independently of whether the extracthr has batch or not, cache or not.
	 */
	public Class getBasicExtractorClass(); 

	static HostnameVerifier hostNameChecker() {

		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			sc.init(null, trustAllCerts, new SecureRandom());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		return allHostsValid;
	}

	/**
	 * 
	 * @param entityAnnotation
	 * @return call the service and get the answer.
	 * @throws IOException
	 */
	static String getAnnotationDisambiguated(String entityAnnotation) throws IOException {
		DataOutputStream os = null;
		StringBuilder jsonString = new StringBuilder();
		// Accept all https connection, the url was changed from http -> https.
		HostnameVerifier allHostsValid = hostNameChecker();
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		URL url = new URL("https://ambiversenlu.saclay.inria.fr/");
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		try {
			conn.setRequestMethod("POST");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("accept", "application/json");
			conn.setConnectTimeout(5000);
			byte[] postData = entityAnnotation.getBytes(StandardCharsets.UTF_8);
			conn.setDoOutput(true);
			os = new DataOutputStream(conn.getOutputStream());
			os.write(postData);
			os.flush();
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			InputStream inputStream = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader((inputStream)));
			String s;
			while ((s = br.readLine()) != null) {
				jsonString.append(s);
			}
			br.close();
			conn.disconnect();

		} finally {
			conn.disconnect();
		}
		// }
		// log.info("flair "+entityAnnotation);
		return jsonString.toString();
	}

	/**
	 *
	 * @param entity
	 * @param input
	 * @param local
	 * @return build the Ambiverse query
	 */
	static String getAmbRequest(Entity entity, String input, String local) {
		JSONObject entityObject = new JSONObject();
		JSONArray allAmbEntities = new JSONArray();
		JSONObject annotatedMentions = new JSONObject();
		entityObject.put("confidence", entity.confidence());

		String normalizedEntity = Node.entityLabelPreprocessing(entity.value());
		entityObject.put("charLength", normalizedEntity.length());

		int charOffset = input.indexOf(normalizedEntity);
		if (charOffset == -1) {
			input = normalizedEntity;
		}
		entityObject.put("charOffset", input.indexOf(normalizedEntity));
		entityObject.put("type", entity.type().toString().split("ENTITY_")[1].substring(0, 3));
		allAmbEntities.put(entityObject);
		if (!isNullOrEmpty(allAmbEntities.toString()) && allAmbEntities.length() > 0) {
			annotatedMentions.put("annotatedMentions", allAmbEntities);
			// log.info(input+"--"+entity.value()+"--"+cleanInput);
			annotatedMentions.put("text", input);
			annotatedMentions.put("docId", "doc");
			annotatedMentions.put("language", local);
			// annotatedMentions.put("language", "en");
			annotatedMentions.put("extractConcepts", "false");
			// log.info(annotatedMentions.toString());
			return annotatedMentions.toString();
		}
		return "";
	}

	/** This inputs and outputs sorted collections to make sure nodes are numbered in a deterministic order.
	 *  This is not important per se, but it's more intuitive than out-of-order numbering, and tests depend
	 *  on node and edge numbers. */
	ArrayList<Pair<Node, Collection<Entity>>> run(ArrayList<Pair<Node, String>> inputs);

}

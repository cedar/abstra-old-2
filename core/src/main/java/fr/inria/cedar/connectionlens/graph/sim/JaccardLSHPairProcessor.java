/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.minHashLSH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class JaccardLSHPairProcessor extends JaccardPairProcessor {

	/** */
	protected final int bands;

//	/** */
//	protected final int buckets;
	
	private final AtomicInteger counter = new AtomicInteger(1);
	private final Map<String, Integer> dictionary = new LinkedHashMap<>();
	private final int vocabSize;
	private MinHashLSH lsh;
	
	public JaccardLSHPairProcessor(StatisticsCollector stats, int bands, double th) {
		this(stats, bands,
				Config.getInstance().getIntProperty("minhash_lsh_rows_per_bands"),
				Config.getInstance().getIntProperty("minhash_lsh_buckets"), 
				th,
				Config.getInstance().getIntProperty("minhash_dictionary_size"));
	}
	
	public JaccardLSHPairProcessor(StatisticsCollector stats, int bands, int rpb, 
			int buckets, double th, int vocabSize) {
		super(stats, th);
		this.vocabSize = vocabSize;
        this.lsh = new MinHashLSH(bands, rpb, buckets, vocabSize, 
        		Config.getInstance().getLongProperty("random_seed"), th);
        this.bands = bands;
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result = not(sameLabel())
					.and(minHashLSH(Range.closedOpen(2, 2 + bands), threshold()));
		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		StringTokenizer tokens = new StringTokenizer(str);
		Set<Integer> keys = new TreeSet<>();
		while (tokens.hasMoreTokens()) {
			String tok = tokens.nextToken();
			Integer key = dictionary.get(tok);
			if (key == null) {
				dictionary.put(tok, (key = counter.getAndIncrement() % vocabSize));
			}
			Preconditions.checkState(key != null, tok + " not part of vocabulary.");
			keys.add(key);  
		}
		int[] hash = lsh.hash(keys);
		int[] result = new int[hash.length + 2];
		result[0] = -1;
		result[1] = -1;
		System.arraycopy(hash, 0, result, 2, hash.length);
		return result;
	}
}

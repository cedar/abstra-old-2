package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class BoundaryNode {
    private int id;
    private final String label; // the label of the collection node represented by the BoundaryNode instance
    private ArrayList<BoundaryNode> children;
    private final CollectionBoundary referenceToRoot;

    public BoundaryNode(int id, CollectionBoundary referenceToRoot) {
        this.id = id;
        this.label = CollectionGraph.getInstance().getCollectionLabel(this.id);
        this.children = new ArrayList<>();
        this.referenceToRoot = referenceToRoot;
        this.referenceToRoot.addCollectionToList(this.id); // add the root in the list of collections involved in the boundary
    }

    public BoundaryNode(String label, CollectionBoundary referenceToRoot) {
        this.label = label;
        this.children = new ArrayList<>();
        this.referenceToRoot = referenceToRoot;
    }


    public void addChild(BoundaryNode bn) {
        this.children.add(bn);
        this.referenceToRoot.addCollectionToList(bn.id);
    }

    public void addChildString(BoundaryNode bn) {
        this.children.add(bn);
        this.referenceToRoot.addCollectionToListString(bn.label);
    }

    public void sortByFrequency(HashMap<Integer, HashMap<Integer, Double>> frequencies) { // TODO NELLY: check how to sort everything
        this.children.sort(new Comparator<BoundaryNode>() {
            @Override
            public int compare(BoundaryNode o1, BoundaryNode o2) {
                // sort the grand-children first...
                o1.sortByFrequency(frequencies);
                o2.sortByFrequency(frequencies);
                // ... and then the current children
                if(frequencies.get(BoundaryNode.this.id).get(o1.id) > frequencies.get(BoundaryNode.this.id).get(o2.id)) {
                    return -1;
                } else if(frequencies.get(BoundaryNode.this.id).get(o1.id).doubleValue() == frequencies.get(BoundaryNode.this.id).get(o2.id).doubleValue()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
    }

    public ArrayList<BoundaryNode> getChildren() {
        return this.children;
    }

    public int getId() {
        return this.id;
    }

    public String getLabel() { return this.label; }

    public void setId(int id) {
        this.id = id;
    }

    public void setChildren(ArrayList<BoundaryNode> children) {
        this.children = children;
    }

    public String toString2() {
        return "BoundaryNode{" +
                "boundaryNodeId=" + this.id +
                ", boundaryNodeLabel='" + this.label + '\'' +
                ", children=" + this.children +
                '}';
    }

    @Override
    public String toString() {
//        if(!this.label.endsWith("#val")) {
            return "{\"" + this.label + "\": " + this.children + "}";
//        } else {
//            return "";
//        }
    }

    //    public String toString() { return "BoundaryNode{" + "boundaryNode=" + this.boundaryNodeLabel + ", children=" + this.children + '}'; }
}
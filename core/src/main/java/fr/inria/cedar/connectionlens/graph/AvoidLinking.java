/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import java.util.HashSet;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;


public class AvoidLinking {
	static HashSet<String> ls = null;
	public static final Logger log = Logger.getLogger(AvoidLinking.class);


	public static HashSet<String> getDoNotLinkLabels() {
		if (ls == null) {
			ls = new HashSet<String>();
			String allLabels = Config.getInstance().getProperty("do_not_link_labels");
			if (allLabels != null && allLabels.length() > 0) {
				log.info("No-link labels are: " + allLabels);
				String[] labelSet = allLabels.split(",");
				for (String s : labelSet) {
					s = Node.genericNormalization(s);
					s = s.replaceAll("\"", "").replaceAll("\'", "");
					s = s.trim();
					if (s.length() > 0) {
						ls.add(s);
					}
				}
			}
		}
		return ls;
	}
}

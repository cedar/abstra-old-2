package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.SRC_T;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Stack;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.ProvenanceSignature.Provenance;
import fr.inria.cedar.connectionlens.util.AnswerTreeDOTPrinting;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.Stoppers;

/**
 * This class implements the BFS based search for performing a
 * keyword search on the graph.
 * 
 * @author Madhulika Mohanty
 *
 */
public class BFSSearch extends QuerySearch implements CLGSTKFunction{

	//boolean isPath = false;

	/** The logger. */
	private final Logger log = Logger.getLogger(BFSSearch.class);

	/**
	 * History of BFS.
	 */
	private EdgeSetCollection historyOfTrees = new EdgeSetCollection();

	HashMap<Integer, Set<AnswerTree>> bfsHistory = new HashMap<Integer, Set<AnswerTree>>();

	private Set<AnswerTree> minimizedAnswerTrees = new LinkedHashSet<>();

	private Map<Integer, Set<Node>> leaves;
	private Map<Integer, Set<Node>> leafRepresentatives;
	protected Map<Node, Integer> invLeaves;

	Predicate<AnswerTree> soluChecker = at -> isSolutionToQuery(at);

	private AnswerTreeDOTPrinting treeDrawer;

	boolean drawAllTrees = false; // set this to true for debugging purposes only

	private int maxMatchesPerKwd = Config.getInstance().getIntProperty("max_matches_per_kwd");

	private boolean oneEntityMatchPerText = Config.getInstance().getBooleanProperty("one_entity_match_per_text");

	protected Map<Node, Set<Edge>> adjacencyLocalCache = new LinkedHashMap<>();
	private boolean getSpecific;

	/**
	 * Allows to filter the edges we explore (ignore any data edge whose specificity
	 * is below this threshold
	 */
	protected double minSpecificity;

	private Object statsObj; // Identifier to send to StatisticsCollector.

	private int totalIterations = 0;
	
	protected Stoppers<AnswerTree> searchStoppers;
	
	boolean isPathQuery = true;

	/**
	 * Resets all internal collections and search memory
	 * 
	 * @param q
	 */
	private void resetState(Object statsObj) {
		leaves = new LinkedHashMap<>();
		leafRepresentatives = new LinkedHashMap<>();
		invLeaves = new LinkedHashMap<>();
		adjacencyLocalCache = new LinkedHashMap<>();
		historyOfTrees = new EdgeSetCollection();
		bfsHistory = new HashMap<Integer, Set<AnswerTree>>();
		minimizedAnswerTrees = new LinkedHashSet<>();

		treeDrawer = new AnswerTreeDOTPrinting(statsObj);
		this.statsObj = statsObj;
		stats.start(statsObj);
		//pss=new ProvenanceSignatureStatistics();
	}
	
	/** Actual stopper initialization */
	public void resetStoppers(long to, long firstk, long stationarity) {
		Predicate<AnswerTree[]> result = (a) -> false;
		if (to > 0) {
			log.info("Search stopper: run for at most " + to + " ms.");
			result = result.or(Stoppers.timeout(to));
		}
		if (firstk > 0) {
			log.info("Search stopper: find at most " + firstk + " results.");
			final SortedSet<AnswerTree> resultSet = new ConcurrentSkipListSet<>(
					new AnswerTree.Comparator(this.scoring()));
			result = result.or(Stoppers.topK(firstk, resultSet));
		}
		if (stationarity > 0) {
			log.info("Search stopper: maximum allowed time lapse between two results is " + stationarity + " ms.");
			result = result.or(Stoppers.stationarity(stationarity));
		}
		this.searchStoppers = Stoppers.forward(result);
	}


	// this is used by Experiment
	public BFSSearch(IndexAndProcessNodes i, Graph g, ScoringFunction sf, StatisticsCollector st,
			boolean queryStrategy) {
		super(i, g, sf, st);
		this.getSpecific = queryStrategy;
		//resetState(null); // passing null query.
		setStoppers();
		setMinSpecificity();
	}
	
	/** Here, BFSSearch's stoppers are set from the configuration. */
	protected void setStoppers() {
		Config config = Config.getInstance();
		resetStoppers(config.getLongProperty("search_stopper_timeout"), config.getLongProperty("search_stopper_topk"),
				config.getLongProperty("search_stopper_stationarity"));
	}

	protected void setMinSpecificity() {
		// log.info(this.getClass().getSimpleName() + " setMinSpecificity, graph is
		// null: " + (this.graph == null));
		try {
			Pair<Double, Double> specStats = this.graph.getSpecificityStat();
			this.minSpecificity = specStats.getLeft() / 100;
			// set the threshold at 1% the avg spec
			// sample spec distribution:
			// https://gitlab.inria.fr/cedar/connection-lens/-/issues/421#note_428167
		} catch (Exception e) {// the above block may fail if spec stats have not been computed
			e.printStackTrace();
			this.minSpecificity = 0.05; // in this case just pick a value
		}
		log.info("Min specificity is: " + minSpecificity);
	}


	@Override
	public void run(Query q, Consumer<AnswerTree> processor) {
		resetState(q);
		prepareInputs(q);
		findTrees(processor);
	}

	@Override
	public void collectStats() {
		// TODO Auto-generated method stub
		stats.tick(statsObj, SRC_T);
		stats.stop(statsObj);
		stats.put(statsObj, "ATs_all", GAMAnswerTree.getLastNo());
		stats.put(statsObj, "Num_Iterations", this.totalIterations);

	}

	protected boolean isSolutionToQuery(AnswerTree answerTree) {
		return (answerTree.matches().keySet().size() == leaves.size());
	}

	private void prepareInputs(Query q) {
		
		if(q.size()<=2)
			isPathQuery = true;
		else
			isPathQuery = false;

		// populate leaves: query component --> nodes
		int index = 0;
		for (QueryComponent queryComponent : q.getComponents()) {
			log.info("Component: " + queryComponent + " of class: " + queryComponent.getClass().getSimpleName());
			Set<Node> nodes = getNodeMatchesAndFilter(queryComponent);
			StringBuffer sb = new StringBuffer();
			sb.append(nodes.size() + ((nodes.size() == 1) ? " match" : " matches") + " for " + queryComponent);

			Set<Node> allNodes = new HashSet<>(nodes);
			// 28/4/21: building leafRepresentatives
			Set<Node> allNodeReps = new HashSet<>();
			for (Node n : allNodes) {
				allNodeReps.add(n.getRepresentative());
			}
			// leaves and leafRepresentatives are assumed to "have seen" queryComponent (or
			// not), on sync
			if (leaves.containsKey(index)) {
				leaves.get(index).addAll(allNodes);
				leafRepresentatives.get(index).addAll(allNodeReps);
			} else {
				leaves.put(index, allNodes);
				leafRepresentatives.put(index, allNodeReps);
			}
			// Build the invLeaves
			for (Node node : allNodes) {
				if (invLeaves.containsKey(node)) {
					Integer oldVal = invLeaves.get(node);
					Integer newVal = oldVal | (1 << Integer.valueOf(index));
					invLeaves.put(node, newVal);
				} else {
					invLeaves.put(node, (1 << Integer.valueOf(index)));
				}
			}
			log.info(new String(sb));
			index++;
		}
	}

	/**
	 * Helper method that extracts matches from the index and *for query purposes*
	 * removes entities from among the matches.
	 * 
	 * @param qc
	 * @return
	 */
	private Set<Node> getNodeMatchesAndFilter(QueryComponent qc) {
		Set<Node> nodes = null;
		if (qc.value().startsWith("exact:")) {
			QueryComponent qc2 = new AtomicKeyword(qc.value().replace("exact:", ""));
			nodes = this.index.getExactNodeMatches(qc2);
		} else {
			// log.info("Looking up in the index for " + qc);
			nodes = this.index.getNodeMatches(qc);
			// log.info("Index lookup finished");
		}
		filterInitialMatchSet(nodes);
		return nodes;
	}

	/**
	 * All these nodes match a single query component. For each text match having at
	 * least one entity child match: - remove the text node - if we only allow 1
	 * entity child of a given text node to match, also remove all but one of its
	 * entity child matches
	 * 
	 * @param nodes
	 */
	private void filterInitialMatchSet(Set<Node> nodes) {
		// log.info("Starting with " + nodes.size() + " nodes");
		HashMultimap<Node, Node> entityChildrenPerText = HashMultimap.create();
		// int i = 0;
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				if (isEntityType(thisNode.getType())) {
					Set<Edge> adjacentThisNode = graph.getAdjacentEdges(thisNode, EXTRACTION_EDGE);
					for (Edge e : adjacentThisNode) {
						if (e.getTargetNode().equals(thisNode)) {
							if (nodes.contains(e.getSourceNode())) {
								entityChildrenPerText.put(e.getSourceNode(), thisNode);
								// log.info("Matching text " + e.getSourceNode() + " has entity child: " +
								// e.getTargetNode());
							}
						}
					}
				} else {
					// log.info(i + " not entity! " + n.getLabel());
				}
			} else {
				// log.info(i + " not a node");
			}
			// i++;
		}
		// now we know for every nodes text that is a parent of some nodes entity,
		// all the entities of whom the text is a parent
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				if (!isEntityType(thisNode.getType())) {
					Set<Node> entityChildrenOfThisNode = entityChildrenPerText.get(thisNode);
					if (entityChildrenPerText.get(thisNode) != null && entityChildrenPerText.get(thisNode).size() > 0) {
						// log.info("Removing matching text " + thisNode);
						nodes.remove(thisNode); // the text node has entity children: remove it
						if (oneEntityMatchPerText) { // if only one entity is wished for, also remove all but that
							// entity
							boolean oneAlreadyPreserved = false;
							for (Node entityChild : entityChildrenOfThisNode) {
								if (!oneAlreadyPreserved) {
									oneAlreadyPreserved = true;
								} else {
									nodes.remove(entityChild);
									// log.info("Removing its entity child " + entityChild);
								}
							}
						}
					}
				}
			}
		}
		// for each representative keep only the node with the highest ID
		HashMap<Node, Node> surviversByRep = new HashMap<Node, Node>();
		for (Item n : nodes) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				Node previousN = surviversByRep.get(thisNode.getRepresentative());
				if (previousN != null) {
					int comparison = thisNode.getId().compareTo(previousN.getId());
					if (comparison > 0) {
						surviversByRep.put(thisNode.getRepresentative(), thisNode);
					} // else leave the previous node
				} else {
					surviversByRep.put(thisNode.getRepresentative(), thisNode);
				}
			}
		}
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				Node retainedFromThisNode = surviversByRep.get(thisNode.getRepresentative());
				if (!(retainedFromThisNode.equals(thisNode))) {
					nodes.remove(thisNode);
				}
			}
		}
		// limit the number of matches per kwd, if desired
		if (maxMatchesPerKwd > 0) {
			for (Item n : new HashSet<>(nodes)) {
				if (nodes.size() > maxMatchesPerKwd) {
					nodes.remove(n);
				}
			}
		}
	}


	public void prepareInputs(List<Set<Node>> inputs, Integer statsObj) {

		resetState(statsObj);
		
		if(inputs.size()<=2)
			isPathQuery = true;
		else
			isPathQuery = false;

		//this.startTime = System.currentTimeMillis();

		// Fill in leaves, invLeaves and leafRepresentatives from the sets of nodes.

		int index = 0;
		for (Set<Node> nodes : inputs) {

			StringBuffer sb = new StringBuffer();
			sb.append(nodes.size() + ((nodes.size() == 1) ? " match" : " matches") + " for " + index);

			Set<Node> allNodes = new HashSet<>(nodes);
			// 28/4/21: building leafRepresentatives
			Set<Node> allNodeReps = new HashSet<>();
			for (Node n : allNodes) {
				allNodeReps.add(n.getRepresentative());
			}
			// leaves and leafRepresentatives are assumed to "have seen" queryComponent (or
			// not), on sync
			if (leaves.containsKey(index)) {
				leaves.get(index).addAll(allNodes);
				leafRepresentatives.get(index).addAll(allNodeReps);
			} else {
				leaves.put(index, allNodes);
				leafRepresentatives.put(index, allNodeReps);
			}
			// Build the invLeaves
			for (Node node : allNodes) {
				if (invLeaves.containsKey(node)) {
					Integer oldVal = invLeaves.get(node);
					Integer newVal = oldVal | (1 << Integer.valueOf(index));
					invLeaves.put(node, newVal);
				} else {
					invLeaves.put(node, (1 << Integer.valueOf(index)));
				}
			}
			log.info(new String(sb));
			index++;
		}


	}

	private void initHistory(Consumer<AnswerTree> processor) {
		HashSet<Node> coveredNodes = new HashSet<Node>();
		for (int i = 0; i < this.leaves.size(); i++) {
			Set<Node> nodesForThisComponent = this.leaves.get(i);
			for (Node node : nodesForThisComponent) {

				if (( !coveredNodes.contains(node))) {
					coveredNodes.add(node);
					HashSet<Integer> kwdsMatchedByThisNode = new HashSet<Integer>();
					kwdsMatchedByThisNode.add(i);
					for (int j = 0; j < this.leaves.size(); j++) {
						if (leaves.get(j).contains(node)) {
							kwdsMatchedByThisNode.add(j);
						}
					}
					GAMAnswerTree initialTree = GAMAnswerTree.wrap(new AnswerTree(node, kwdsMatchedByThisNode), new ProvenanceSignature(Provenance.INIT));
					boolean solution = soluChecker.test(initialTree);
					if(solution)
						processSolution(initialTree, processor);
					else
						addToHistory(0,initialTree);
					// log.info("Initial tree for " + q + " rooted in " + node.getLabel() +
					// "(" + node.getId() +
					// ") has " + kwdsMatchedByThisNode.size() + " matches");
					//processAnswerTree(initialTree, processor, false, false, false);
					if (drawAllTrees && drawingEnabled) { // TREE PRINTING FOR DEBUG
						treeDrawer.print(initialTree, "init", soluChecker.test(initialTree), scoring);
					}
				}
			}
		}

	}

	private void addToHistory(int i, AnswerTree tree) {
		Set<AnswerTree> trees = new HashSet<AnswerTree>();
		if(this.bfsHistory.get(i)!=null)
			trees = this.bfsHistory.get(i);
		trees.add(tree);
		this.bfsHistory.put(i, trees);
	}

	private void processSolution(AnswerTree tree, Consumer<AnswerTree> processor) {
		
		AnswerTree minimizedTree = getMinimizedSolutions(tree);
		//for (AnswerTree minimizedTree : minimizedTrees) {
		
		if(minimizedTree!=null) {
			GAMAnswerTree sol = GAMAnswerTree.wrap(minimizedTree, null);
			if (drawAllTrees && drawingEnabled) { // TREE PRINTING FOR DEBUG
				treeDrawer.print(sol, "-final", true, scoring);
			}
			processor.accept(sol);
			if (searchStoppers.test(sol)) {
				log.info("Maximum number of solutions reached");
				throw new ConditionReachedException("TOPK");
			}
		}
		
			// System.out.println("Minimal solution! " + minimizedTree.shortLabelString() +
			// "\n");
		//}

	}

	public void findTrees(Consumer<AnswerTree> processor) {
		//log.info("Starting init iteration-->");
		initHistory(processor);
		//log.info("Done!");
		//log.info("Starting remaining iteration-->");
		int iteration = 1;
		while(this.bfsHistory.get(iteration-1)!=null){
			//log.info("Iteration-"+iteration);
			//log.info("History at this point:"+this.bfsHistory);
			search(iteration, processor);
			this.totalIterations  = iteration;
			iteration++;
		}
		
		log.info("BFSSearch finished, total iterations taken: " + this.totalIterations);
	}

	private void search(int iteration, Consumer<AnswerTree> processor) {
		// for each tree in this iteration:
		// 	for each node in this tree:
		// 		grow one edge from each node.
		//		Process the new tree formed such.
		Set<AnswerTree> treesInPreviousIter = this.bfsHistory.get(iteration-1);
		//log.info("Last set of trees:" + treesInPreviousIter);
		this.bfsHistory.remove(iteration-1);
		for(AnswerTree tree : treesInPreviousIter) {
			Set<Node> nodesGrown = new HashSet<Node>();
			//log.info("Tree nodes:"+ tree.nodes());
			for( Node n : tree.nodes()) {
				if(!nodesGrown.contains(n))
				{
					nodesGrown.add(n);
					Set<Edge> adjEdges = getCachedAdjacentEdges(n);
					//log.info("Adjacent edges:"+ adjEdges);
					for(Edge ae : adjEdges) {

						// check a bunch of other cases where we should NOT push this in Q
						if (ae.getSourceNode().equals(ae.getTargetNode()) // this edge is a loop
								|| (tree.nodes().contains(ae.getTargetNode())
										&& tree.nodes().contains(ae.getSourceNode()))
								//|| tree.datasetLoop(ae) // this edge closes a dataset loop
								|| (ae.getLabel().equals(Edge.SAME_AS_LABEL) && tree.isGrownThroughSameAs())
								// we would be concatenating sameAs
								|| (!noRedundantMatch(tree, ae)))// this edge would bring redundancy
							// it is important to keep this condition last
						{
							continue;
						}

						//log.info("Grow opportunity-"+tree+" with edge "+ae.getLabel());
						AnswerTree grownTree = getAnswerTreeFromGrow(tree, ae);
						processTree(grownTree, processor);

					}

				}
				if (searchStoppers.test()) {
					// throw new ConditionReachedException("T/O");
					log.info("Time-out (1)");
					throw new ConditionReachedException("T/O");
				}
			}

		}

	}

	/**
	 * @param answerTree
	 * @param e
	 * @return true if adding e to answerTree does not introduce redundancy between
	 *         matches, that is: if there are more than 1 matches for a kwd, then
	 *         they are all on nodes, and the nodes have the same representative.
	 */
	private boolean noRedundantMatch(AnswerTree answerTree, Edge e) {
		SetMultimap<Integer, Item> matches = answerTree.matches();
		for (Integer qc : answerTree.matches().keys()) { // each qc already matched in answerTree
			if ((!answerTree.nodes.contains(e.getSourceNode())) // e.s is the new proposed root
					&& leafRepresentatives.get(qc).contains(e.getSourceNode().getRepresentative())) {
				// e.s matches qc
				// check that in answerTree, it's matched in a *node* with *the same rep*
				Set<Item> items = matches.get(qc);
				for (Item i : items) {
					if (i instanceof Node) { // matched on an answerTree node
						if (!(((Node) i).getRepresentative().equals(e.getSourceNode().getRepresentative()))) {
							return false;
						}
					} else { // matched on an answerTree edge
						return false;
					}
				}
			}
			if ((!answerTree.nodes.contains(e.getTargetNode())) // e.t is the new proposed root
					&& leafRepresentatives.get(qc).contains(e.getTargetNode().getRepresentative())) {
				// check that in answerTree, it's matched in a *node* with *the same rep*
				Set<Item> items = matches.get(qc);
				for (Item i : items) {
					if (i instanceof Node) {
						if (!(((Node) i).getRepresentative().equals(e.getTargetNode().getRepresentative()))) {
							return false;
						}
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	protected Set<Edge> getCachedAdjacentEdges(Node lastVisitedNode) {
		Set<Edge> result = adjacencyLocalCache.get(lastVisitedNode);
		if (result == null && this.getSpecific == false) {
			adjacencyLocalCache.put(lastVisitedNode, (result = getAdjacentEdgesToNode(lastVisitedNode)));
		} else if (result == null && this.getSpecific == true) {
			adjacencyLocalCache.put(lastVisitedNode, (result = getSpecificAdjacentEdgesToNode(lastVisitedNode)));
		}
		return result;
	}

	// Get the adjacent edges from the last visited node
	protected Set<Edge> getAdjacentEdgesToNode(Node node) {
		return Sets.union(graph.getWeakSameAs(node), graph.getAdjacentEdges(node));
	}

	protected Sets.SetView<Edge> getSpecificAdjacentEdgesToNode(Node node) {
		// Pair<Double, Double> p = graph.getSpecificityStat();
		return Sets.union(graph.getWeakSameAs(node),
				Sets.union(graph.getSpecificIncomingEdgesPostLoading(node, this.minSpecificity),
						graph.getSpecificOutgoingEdgesPostLoading(node, this.minSpecificity)));
	}

	private AnswerTree getAnswerTreeFromGrow(AnswerTree tree, Edge ae) {
		Set<Node> nodes = new HashSet<>(tree.nodes());
		Node target = ae.getTargetNode();
		Node source = ae.getSourceNode();
		if (nodes.contains(target)) {
			target = source;
		} 
		nodes.add(target);
		Set<Edge> edges = new HashSet<>(tree.edges());
		edges.add(ae);
		SetMultimap<Integer, Item> matches = HashMultimap.create(tree.matches());
		updateAnswerTreeMatches(matches, target);
		@SuppressWarnings("unchecked")
		AnswerTree at = new AnswerTree(target, nodes, edges, // getKwdsMatchedBy(target),
				matches);
		return at; 
	}

	/**
	 * Enriches matches to reflect those the query keywords matched by target. These
	 * are retrieved from the leaves data structure.
	 * 
	 * @param matches
	 * @param target
	 */
	private void updateAnswerTreeMatches(SetMultimap<Integer, Item> matches, Node target) {
		for (int i = 0; i < this.leaves.size(); i++) {
			if (leaves.get(i).contains(target)) {
				matches.get(i).add(target);
			}
		}
	}

	private void processTree(AnswerTree tree, Consumer<AnswerTree> processor) {
		boolean solution = soluChecker.test(tree);
		if(solution)
			processSolution(tree, processor);
		else if(this.historyOfTrees.addAndStateIfNew(tree, null)) {
			GAMAnswerTree at = GAMAnswerTree.wrap(tree, null);
			this.addToHistory(at.edges.size(), at); 
			if (drawingEnabled) {
				treeDrawer.print(at, at.shortLabel() + "-grow", false, scoring);
			}
		}
	}



	private AnswerTree getMinimizedSolutions(AnswerTree answerTree) {
		// System.out.println("Entering getMinimizedSolutions, prov=" +
		// answerTree.prov());
		AnswerTree minimizedTree = null;
		if (!minimizedAnswerTrees.contains(answerTree)) {
			minimizedAnswerTrees.add(answerTree);

			minimizedTree = minimize(answerTree);

		}

		return minimizedTree;
	}

	/**
	 * Given AT that is a solution, returns a set of all the minimal subtrees that
	 * can be obtained from AT.
	 * 
	 * @param at
	 * @return
	 */
	AnswerTree minimize(AnswerTree at) {
		AnswerTree minimalTree = at;
		
		if(at.nodes().size()<2 )
		{
			return minimalTree; // If this tree has just 1 node, return the tree itself.
		}
		
		
		Stack<AnswerTree> workingTrees = new Stack<>();
		workingTrees.push(at);
		while (!workingTrees.empty()) { // invariant: as the loop runs, we take t from workingTrees
			// and put back only trees that are strictly smaller.
			AnswerTree t = workingTrees.pop();
			boolean tIsMinimal = true;
			for (Edge e : t.edges()) { // see if we can derive smaller trees from t, that are still solutions
				AnswerTree te = excludeOneEdge(t, e);
				Set<AnswerTree> componentsTE = this.connectedComponentsOf(te);
				for (AnswerTree compTE : componentsTE) {
					if (soluChecker.test(compTE)) {
						tIsMinimal = false;
						workingTrees.push(compTE);
					}
				}
			}
			if (tIsMinimal && t.edges.size()<minimalTree.edges().size()) {
				minimalTree = t;
			}
			if (searchStoppers.test()) {
				// throw new ConditionReachedException("T/O");
				log.info("Time-out (2)");
				throw new ConditionReachedException("T/O");
			}
		}
		return minimalTree;
	}

	/**
	 * Minimization helper: builds an AT from t by excluding edge e. This may or may
	 * not remove one or even 2 nodes; it may or may not remove matches from the
	 * query.
	 * 
	 * @param t
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private AnswerTree excludeOneEdge(AnswerTree t, Edge e) {
		HashSet<Edge> rEdges = new HashSet<>();
		rEdges.addAll(t.edges);
		rEdges.remove(e);

		HashSet<Node> rNodes = new HashSet<>();
		rNodes.addAll(t.nodes);
		SetMultimap<Integer, Item> rMatches = HashMultimap.create(t.matches());
		boolean removeSource = onlyAppearsIn(e.getSourceNode(), t.edges, e);
		if (removeSource) {
			rNodes.remove(e.getSourceNode());
			removeMatchesInto(t.matches(), rMatches, e.getSourceNode());
		}
		boolean removeTarget = onlyAppearsIn(e.getTargetNode(), t.edges, e);
		if (removeTarget) {
			rNodes.remove(e.getTargetNode());
			removeMatchesInto(t.matches(), rMatches, e.getTargetNode());
		}
		removeMatchesInto(t.matches(), rMatches, e);
		return new AnswerTree(rNodes, rEdges, rMatches);
	}

	/**
	 * Minimization helper: Removes from matchesFromWhichToErase (initially a copy
	 * of origMatches) any match into e. origMatches is not modified, just
	 * traversed, and we erase from the other.
	 * 
	 * @param origMatches
	 * @param matchesFromWhichToErase
	 * @param i
	 */
	private void removeMatchesInto(SetMultimap<Integer, Item> origMatches,
			SetMultimap<Integer, Item> matchesFromWhichToErase, Item i) {
		Map<Integer, Collection<Item>> matchMap = origMatches.asMap();
		for (Integer qc : matchMap.keySet()) {
			for (Item i2 : matchMap.get(qc)) {
				if (i.equals(i2)) {
					matchesFromWhichToErase.remove(qc, i);
				}
			}
		}
	}

	/**
	 * Minimization helper: returns true if in the whole set of edges edges, n only
	 * appears in e
	 * 
	 * @param n
	 * @param edges
	 * @param e
	 * @return
	 */
	private boolean onlyAppearsIn(Node n, Set<Edge> edges, Edge e) {
		for (Edge e2 : edges) {
			if (e2.equals(e)) {
				continue;
			}
			if (e2.getSourceNode().equals(n) || e2.getTargetNode().equals(n)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Minimization helper: decomposing an AT in connected components
	 * 
	 * @param at
	 * @return
	 */
	private Set<AnswerTree> connectedComponentsOf(AnswerTree at) {
		HashSet<AnswerTree> result = new HashSet<>();
		for (Node n : at.nodes) {
			if (noComponentAlreadyHas(result, n)) {
				AnswerTree nComponent = connectedComponentOf(at, n);
				result.add(nComponent);
			}
		}
		return result;
	}

	/**
	 * Minimization helper: identifies the connected component of n in at
	 * 
	 * @param at
	 * @param n
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private AnswerTree connectedComponentOf(AnswerTree at, Node n) {
		HashSet<Edge> compEdges = new HashSet<>();
		HashSet<Node> compNodes = new HashSet<>();
		Stack<Node> visited = new Stack<>();
		visited.push(n);
		SetMultimap<Integer, Item> compMatches = HashMultimap.create();
		compNodes.add(n);
		copyMatchesOfNodeFromInto(n, at.matches(), compMatches);
		boolean changed = true;
		while (changed || (!visited.isEmpty())) {
			// as long as we are still adding nodes/edges or there are nodes to explore
			changed = false;
			Node n2 = visited.pop(); // take a node to explore
			for (Edge e : at.edges) {
				if ((!e.getSourceNode().equals(n2)) && (!e.getTargetNode().equals(n2))) {
					continue;
				}
				// e is an edge adjacent to the node to be explored
				if (compNodes.contains(e.getSourceNode()) && (!compNodes.contains(e.getTargetNode()))) {
					compNodes.add(e.getTargetNode()); // copy the target in the component
					// and its matches:
					copyMatchesOfNodeFromInto(e.getTargetNode(), at.matches(), compMatches);
					compEdges.add(e); // copy the edge in the component, and its matches
					copyMatchesOfNodeFromInto(e, at.matches(), compMatches);
					visited.push(e.getTargetNode()); // make sure we will explore from this node, also
					changed = true;
				}
				if (compNodes.contains(e.getTargetNode()) && (!compNodes.contains(e.getSourceNode()))) {
					compNodes.add(e.getSourceNode()); // copy the source, and its matches:
					copyMatchesOfNodeFromInto(e.getSourceNode(), at.matches(), compMatches);
					compEdges.add(e); // copy the edge in the component, and its matches:
					copyMatchesOfNodeFromInto(e, at.matches(), compMatches);
					visited.push(e.getSourceNode()); // make sure we will explore from this node, also
					changed = true;
				}
			}
		}
		return new AnswerTree(compNodes, compEdges, compMatches);
	}

	/**
	 * Minimization helper: Copies into compMatches those matches (if any) from
	 * matches that are on the query component i
	 * 
	 * @param i
	 * @param matches
	 * @param compMatches
	 */
	private void copyMatchesOfNodeFromInto(Item i, SetMultimap<Integer, Item> matches,
			SetMultimap<Integer, Item> compMatches) {
		// System.out.println("Copying matches into: " + i);
		Map<Integer, Collection<Item>> matchMap = matches.asMap();
		for (Integer qc : matchMap.keySet()) {
			// System.out.println(" Query component: " + qc);
			for (Item i2 : matchMap.get(qc)) {
				// System.out.println(" Originally matched into: " + i);
				if (i.equals(i2)) {
					// System.out.println(" Copied match of " + qc + " into " + i);
					compMatches.put(qc, i);
				}
			}
		}
	}

	/**
	 * Minimization helper: checks that n is not present in any of the given trees.
	 * 
	 * @param setOfTrees
	 * @param n
	 * @return
	 */
	private boolean noComponentAlreadyHas(HashSet<AnswerTree> setOfTrees, Node n) {
		for (AnswerTree at : setOfTrees) {
			if (at.nodes.contains(n)) {
				return false;
			}
		}
		return true;
	}





}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.score;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.score.Score.DoubleScore;

public class SpecificityScore implements ScoringFunction {

	@Override
	public Score compute(Scorable elem) {
		//log.info(this.getClass().getSimpleName() + " evaluated on " + elem.getClass().getSimpleName());
		if (elem instanceof AnswerTree) {
			AnswerTree at = (AnswerTree) elem;

			double d = 1000.0; // we start at 1000 not to go too fast at 0 when multiplying by low constants
			for (Edge e : at.edges()) {
				d = d * e.getSpecificityValue();
				//log.info("Edge has specificity: " + e.getSpecificityValue());
			}
			return new DoubleScore(d / 1000.0);
		}
		return new DoubleScore(0.0);
	}

	public boolean equals(Object o) {
		if (o instanceof SpecificityScore) {
			return true; 
		}
		else {
			return false; 
		}
	}
}

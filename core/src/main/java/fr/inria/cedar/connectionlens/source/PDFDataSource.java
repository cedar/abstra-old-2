/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import fr.inria.cedar.connectionlens.util.PythonUtils;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * This does not extend DataSource since it behaves differently. 
 */
public class PDFDataSource {


    String pdfPath;
    String pdfName;

	/** The logger. */
	private static final Logger log = Logger.getLogger(PDFDataSource.class);


    public PDFDataSource(String pdfPath) {
          this.pdfPath = pdfPath;
          this.pdfName = new File(pdfPath).getName()
                  //.replaceAll("\\s+|\\-", "_").toLowerCase()
                  .replaceAll (".pdf","");
    }


    /**
     *
     * @return run the python script
     */
    public  int runScraping() {
        int exitProcess = 0;
        try {
            log.info(pdfPath);
            Process process = PythonUtils.getInstance().runPDFScrapper(pdfPath);
            exitProcess = process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return exitProcess;
    }

    public void cleanPDFExtractedFolder(){

    }

    public String getPDFPath() {
        return pdfPath;
    }

    public String getPDFName() {
        return pdfName;
    }



}

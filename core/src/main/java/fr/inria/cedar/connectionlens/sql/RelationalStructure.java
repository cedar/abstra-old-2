/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql;

import static java.util.Spliterator.IMMUTABLE;
import static java.util.Spliterator.NONNULL;
import com.google.common.collect.Iterables;
import fr.inria.cedar.connectionlens.graph.Item;
import java.sql.Array;
import java.sql.Connection;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Map;
import java.util.Spliterators;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parent class to all data structure relying on some relational database implementation.
 */
public abstract class RelationalStructure {

	protected final Map<String, PreparedStatement> statementMap = new ConcurrentHashMap<>();

	/** The database connection. */
	protected final Connection conn;
	/** The copy manager */
	protected final CopyManager copyMgr;

	/** Whether the structure is already initialized. */
	protected boolean createdInPreviousRun;

	/** A logger to print out messages in a conventional way */
	private static final Logger logger = LoggerFactory.getLogger(RelationalStructure.class);



	/**
	 * Instantiates a new relational structure.
	 */
	public RelationalStructure() {
		this.conn = ConnectionManager.getMasterConnection();
		try {
			this.copyMgr = new CopyManager((BaseConnection) conn);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new IllegalStateException("Unable to get a copy manager"); 
		}
		createdInPreviousRun = true; // conservatively
	}
	
	public CopyManager getCopyManager() {
		return this.copyMgr;
	}

	public Connection getConnection() {
		return conn; 
	}
	
	/**
	 * Checks if the structure is already initialized.
	 *
	 * @return true, iff it is initialized
	 */
	public boolean isCreatedInPreviousRun() {
		return createdInPreviousRun;
	}


	/**
	 * Drops then recreates the underlying tables.
	 */
	public void reset() {
		logger.info("RESET"); 
		drop();
		create();
	}

	/** Fetches prepared statements (from a cache if already needed before) */
	public PreparedStatement getPreparedStatement(String query) {
		try {
			PreparedStatement result = statementMap.get(query);
			if (result == null) {
				statementMap.put(query, (result = conn.prepareStatement(query)));
			}
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	public void createTable(String tableName, String tableSchema, boolean isUnlogged, boolean dropTableIfExists) {
		try {
//			System.out.println("CREATE TABLE " + tableName);
			this.conn.prepareStatement((dropTableIfExists ? "DROP TABLE IF EXISTS " + tableName + " CASCADE; " : "") +
					"CREATE " + (isUnlogged ? "UNLOGGED" : "") + " TABLE IF NOT EXISTS " + tableName + " " + tableSchema + ";").execute();
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Executes an SQL update.
	 *
	 * @param sql some SQL statement
	 * @return true, if the database was changed as a result of the update
	 */
	public boolean executeUpdate(String sql) {
		//System.out.println("Trying to execute: " + sql);
		return executeUpdate(sql, true);
	}

	/**
	 * Executes an SQL update.
	 *
	 * @param sql some SQL statement
	 * @param strict if true, forwards any exception that occurs during the query process.
	 * @return true, if the database was changed as a result of the update
	 * @throws IllegalStateException if some exception occurred
	 */
	public boolean executeUpdate(String sql, boolean strict) {
		try(Statement stmt = conn.createStatement()) {
			stmt.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			if (!strict) {
				logger.warn(sql + " - " + e.getMessage());
				return false;
			}
			throw new IllegalStateException(e);
		}
	}

	@SuppressWarnings("resource")
	protected <T> Stream<T> stream(PreparedStatement stmt, Function<ResultSet, T> f) {
		try {
			ResultSet rs = stmt.executeQuery();
			return StreamSupport.stream(
					Spliterators.spliterator(new ResultSetIterator<>(rs, ()->f.apply(rs)),
							Long.MAX_VALUE, IMMUTABLE|NONNULL), false)
					.onClose(()->{
						try {
							rs.close();
						} catch (SQLException e) {
							throw new IllegalStateException(e);
						}
					});
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}
	
	@SuppressWarnings("resource")
	protected <T> Stream<T> stream(String sql, Function<ResultSet, T> f) {
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			return StreamSupport.stream(
					Spliterators.spliterator(new ResultSetIterator<>(rs, ()->f.apply(rs)),
							Long.MAX_VALUE, IMMUTABLE|NONNULL), false)
					.onClose(()->{
						try {
							rs.close();
							stmt.close();
						} catch (SQLException e) {
							throw new IllegalStateException(e);
						}
					});
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Executes an update.
	 *
	 * @param stmt some statement
	 * @param sql some SQL statement
	 */
	public static void executeUpdate(Statement stmt, String sql) {
		executeUpdate(stmt, sql, true);
	}

	/**
	 * Executes an update.
	 *
	 * @param stmt some statement
	 * @param sql some SQL statement
	 * @param strict if true, forwards any exception that occurs during the query process.
	 * @return true, if the database was changed as a result of the update
	 * @throws IllegalStateException if some exception occurred
	 */
	public static boolean executeUpdate(Statement stmt, String sql, boolean strict) {
		try {
			stmt.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			if (!strict) {
				logger.warn(sql + " - " + e.getMessage());
				return false;
			}
			throw new IllegalStateException(e);
		}
	}
	
	public static boolean executeUpdate(PreparedStatement stmt, boolean isBatch) {
		try {
			if(isBatch) {
				stmt.executeBatch();
			} else {
				stmt.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			SQLException e2 = e.getNextException();
			e.printStackTrace();
			if (e2 != null) {
				e2.printStackTrace();
			}
			throw new IllegalStateException(e);
		}
	}

	public static ResultSet executeQuery(PreparedStatement stmt) {
		try {
			return stmt.executeQuery();
		} catch (SQLException e) {
			throw new IllegalStateException("Could not execute query " + stmt, e);
		}
	}

	public static ResultSet executeQuery(Statement stmt, String query) {
		try {
			return stmt.executeQuery(query);
		} catch (SQLException e) {
			throw new IllegalStateException("Could not execute query " + stmt, e);
		}
	}

	/**
	 * Initializes the structure. Effectively drops, then recreates the underlying tables.
	 */
	public void init() {
		drop();
		create();
		createdInPreviousRun = false;
		logger.info("init() method has run, isInitialized: " + createdInPreviousRun); 
	}

	/**
	 * Drops the underlying tables.
	 */
	protected abstract void drop();
	
	/**
	 * Creates the underlying tables.
	 */
	protected abstract void create();

	public Array createArray(String type, Iterable<Object> it) {
		
		try {
			return conn.createArrayOf(type, Iterables.toArray(it, Object.class));
		} catch (SQLException e) {
			throw new IllegalStateException("Could not create array " + it, e);
		}
	}

	public Array createArray(String type, Function<String, int[]> sigFunc, Item item) {
		int[] ints = sigFunc.apply(item.getLabel());
		Object[] result = new Object[ints.length];
		try {
			for (int i = 0; i < result.length; i++) {
				result[i] = ints[i];
			}
			return conn.createArrayOf(type, result);
		} catch (SQLException e) {
			throw new IllegalStateException("Could not create array " + Arrays.toString(result), e);
		}
	}

	/**
	 * @param o some string
	 * @return a new string based of the input string from which the quote are escaped.
	 */
	public static String stripQuotes(Object o) {
		return String.valueOf(o).replace("'", "''");
	}
	
}

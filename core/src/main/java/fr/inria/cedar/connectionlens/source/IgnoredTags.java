/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;


import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Nelly Barret
 */
public class IgnoredTags {

    public ArrayList<String> ignoredStartingTags;
    public ArrayList<String> ignoredEndingTags;

    static Logger log = Logger.getLogger(IgnoredTags.class);

    public IgnoredTags() {
        this.ignoredStartingTags = new ArrayList<>();
        this.ignoredStartingTags.add("<center>");
        this.ignoredStartingTags.add("<em>");
        this.ignoredStartingTags.add("<b>");
        this.ignoredStartingTags.add("<font>");
        this.ignoredStartingTags.add("<i>");
        this.ignoredStartingTags.add("<s>");
        this.ignoredStartingTags.add("<small>");
        this.ignoredStartingTags.add("<strong>");
        this.ignoredStartingTags.add("<strike>");
        this.ignoredStartingTags.add("<sub>");
        this.ignoredStartingTags.add("<sup>");
        this.ignoredStartingTags.add("<u>");
        this.ignoredStartingTags.add("<bold>");
        this.ignoredStartingTags.add("<emph>");
        this.ignoredStartingTags.add("<keyword>");
        this.ignoredEndingTags = new ArrayList<>();
        this.ignoredEndingTags.add("</center>");
        this.ignoredEndingTags.add("</em>");
        this.ignoredEndingTags.add("</b>");
        this.ignoredEndingTags.add("</font>");
        this.ignoredEndingTags.add("</i>");
        this.ignoredEndingTags.add("</s>");
        this.ignoredEndingTags.add("</small>");
        this.ignoredEndingTags.add("</strong>");
        this.ignoredEndingTags.add("</strike>");
        this.ignoredEndingTags.add("</sub>");
        this.ignoredEndingTags.add("</sup>");
        this.ignoredEndingTags.add("</u>");
        this.ignoredEndingTags.add("</bold>");
        this.ignoredEndingTags.add("</emph>");
        this.ignoredEndingTags.add("</keyword>");
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Package containing utility classes, i.e. classes that may be used through the project.
 */
package fr.inria.cedar.connectionlens.util;
package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class MentionExtractor extends PatternBasedExtractor {

    public MentionExtractor() {
        super();
    }

    public MentionExtractor(Locale locale) {
        super(locale);
    }

    /**
     * Extract mentions from nodes.
     * @param ds the node's datasource
     * @param input the content of the node
     * @param structuralContext the context of the node (not used here).
     * @return the collection of extracted mentions.
     */
    @Override
    public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
        HashSet<Entity> extractedNodes = new HashSet<>();
        List<String> extractedMentions = this.findMentions(input);
        if(!extractedMentions.isEmpty()) {
            for(String extractedMention : extractedMentions) {
                Entity entity = new Entity(extractedMention, extractedMention, 1.0, 0, Node.Types.MENTION, ds.getLocalURI().toString());
                extractedNodes.add(entity);
            }
        }

        return extractedNodes;
    }

    /**
     * Find mentions, in Twitter-style (@somebody), in the content of the node.
     * @param text the content of the node
     * @return a list of strings which are the mentions found in the content of the node.
     */
    public List<String> findMentions(String text) {
        String pattern = " @[a-zA-Z0-9-_]*"; // force to have a space before (to not recognize the domain of emails as mentions)
        return this.getMatches(pattern, text);
    }

    /**
     * Get the name of the extractor.
     * @return the string containing the name of the extractor.
     */
    @Override
    public String getExtractorName() { return "MentionExtractor"; }

    /**
     * Get the locale.
     * @return the locale.
     */
    @Override
    public Locale getLocale() { return locale; }
}

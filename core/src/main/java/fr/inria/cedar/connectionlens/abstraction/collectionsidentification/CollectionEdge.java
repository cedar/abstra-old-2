package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import org.apache.log4j.Logger;

public class CollectionEdge {
    public static final Logger log = Logger.getLogger(CollectionEdge.class);

    private int id;
    private int source;
    private int target;
    private double edgeTransferFactor;
    private boolean comesFromXMLRelation = false;
    private boolean isAtMostOne = false;

    public CollectionEdge(int id, int source, int target) {
        this.id = id;
        this.source = source;
        this.target = target;
    }

    public CollectionEdge(int id, int source, int target, boolean comesFromXMLRelation) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.comesFromXMLRelation = comesFromXMLRelation;
    }

    public CollectionEdge(int id, int source, int target, boolean comesFromXMLRelation, boolean isAtMostOne) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.comesFromXMLRelation = comesFromXMLRelation;
        this.isAtMostOne = isAtMostOne;
    }

    public CollectionEdge(int id, int source, int target, double transfer, boolean comesFromXMLRelation, boolean isAtMostOne) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.edgeTransferFactor = transfer;
        this.comesFromXMLRelation = comesFromXMLRelation;
        this.isAtMostOne = isAtMostOne;
    }

    public int getId() { return this.id; }

    public int getSource() { return this.source; }

    public int getTarget() { return this.target; }

    public void setEdgeTransferFactor(double edgeTransferFactor) { this.edgeTransferFactor = edgeTransferFactor; }

    public double getEdgeTransferFactor() { return this.edgeTransferFactor; }

    public boolean getComesFromXMLRelation() { return this.comesFromXMLRelation; }

    public void setComesFromXMLRelation(boolean comesFromXMLRelation) { this.comesFromXMLRelation = comesFromXMLRelation; }

    public boolean getIsAtMostOne() {
        return this.isAtMostOne;
    }

    public void setIsAtMostOne(boolean atMostOne) {
        this.isAtMostOne = atMostOne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        CollectionEdge that = (CollectionEdge) o;
        return this.id == that.id && this.source == that.source && this.target == that.target;
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(this.id, this.source, this.target);
//    }

    @Override
    public String toString() {
        return "CE" + this.id + "_{" + System.identityHashCode(this) + "}(" + this.source + "-" + this.target + ";etf=" + this.edgeTransferFactor + ";XML="+this.comesFromXMLRelation+";atMostOne="+ this.isAtMostOne +")"; // "CE{"+this.source+"->"+this.target+"}"; // etf="+this.edgeTransfer +";IDREF="+this.comesFromXMLRelation+"}";
    }
}

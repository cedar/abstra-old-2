/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import static com.google.common.base.Preconditions.checkArgument;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.RandomAccess;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.log4j.Logger;


/**
 * An array list with constant time add complexity.
 *
 * @param <E> the element type
 */
public class BucketList<E> extends AbstractList<E>
		implements RandomAccess, Cloneable, java.io.Serializable {

	/** Generated ID */
	private static final long serialVersionUID = 1421820006242103795L;

	private static final int BUCKET_SIZE = 1024;
	
	private ArrayList<Object[]> buckets = new ArrayList<>();
	private int size = 0;

	private static final Logger log = Logger.getLogger(BucketList.class);


	public BucketList() {
		super();
	}
	
	public BucketList(int initialCapacity) {
		this();
		ensureCapacity(initialCapacity);
	}
	
	public void ensureCapacity(int capacity) {
		int l = bucketIndex(capacity) + 1;
		if (buckets.size() < l) {
			for (int i = buckets.size(); i < l; i++) {
				buckets.add(new Object[BUCKET_SIZE]);
			}
		}
	}
	
	@Override @SuppressWarnings("unchecked")
	public E get(int index) {
		return(E) buckets.get(bucketIndex(index))[indexWithinBucket(index)];
	}

	private int bucketIndex(int i) {
		return i/BUCKET_SIZE;
	}

	private int indexWithinBucket(int i) {
		return i%BUCKET_SIZE;
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		char sep = '[';
		for (int i = 0; i < size; i++) {
			result.append(sep).append(get(i));
			sep = ',';
		}
		return result.append(']').toString();
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		return buckets.stream().anyMatch(this::contains);
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {

			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < size;
			}

			@Override
			public E next() {
				return get(i++);
			}
			
		};
	}
	
	@Override
	public Object[] toArray() {
		return toArray(new Object[size]);
	}

	@Override
	public <T> T[] toArray(T[] a) {
		checkArgument(a.length == size);
		for (int i = 0, l = buckets.size(); i < l; i++) {
			System.arraycopy(buckets.get(i), 0, a, i*BUCKET_SIZE, 
					(i < l - 1 ? BUCKET_SIZE : indexWithinBucket(size + 1)));
		}
		return a;
	}

	@Override
	public boolean add(E e) {
		ensureCapacity(size);
		Object[] array = buckets.get(bucketIndex(size));
		int i = indexWithinBucket(size);
		assert array[i] == null;
		array[i] = e;
		size++;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		int i = indexOf(0, o);
		boolean result = false;
		while (i <= 0) {
			result = true;
			remove(i);
			i = indexOf(i, o);
		}
		return result;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return c.stream().allMatch(this::contains);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		int count = this.buckets.size();
		c.forEach(this::add);
		return this.buckets.size() != count;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new NotImplementedException("TODO");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		int count = this.buckets.size();
		c.forEach(this::remove);
		return this.buckets.size() != count;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new NotImplementedException("TODO");
	}

	@Override
	public void clear() {
		this.buckets.clear();
		this.size = 0;
	}

	@Override
	public E set(int index, E element) {
		ensureCapacity(index);
		int b = bucketIndex(index);
		int i = indexWithinBucket(index);
		E result = (E) buckets.get(b)[i];
		buckets.get(b)[i] = element;
		if (index >= size) {
			size = index + 1;
		}
		return result;
	}

	@Override
	public void add(int index, E element) {
		throw new NotImplementedException("TODO");
	}

	@Override
	public E remove(int index) {
		checkArgument(index >= 0);
		checkArgument(index < size);
		int bIndex = bucketIndex(index);
		int wIndex = indexWithinBucket(index);
		E result = (E) buckets.get(bIndex)[wIndex];
		compact(bIndex, wIndex);
		return result;
	}

	private void compact(int bucketIndex, int indexWithinBucket) {
		throw new NotImplementedException("TODO");
	}
	
	public int indexOf(int offset, Object o) {
		checkArgument(offset >= 0);
		checkArgument(offset < size);
		for (int i = offset, l = buckets.size(); i < l; i++) {
			Object[] array = buckets.get(i);
			for (int j = (i == offset ? indexWithinBucket(offset) : 0), k = array.length; j < k; j++) {
				if (o != null && o.equals(array[j])) {
					return i*BUCKET_SIZE + j;
				}
			}
		}
		return -1;
	}
	
	@Override
	public int indexOf(Object o) {
		return indexOf(0, o);
	}

	@Override
	public int lastIndexOf(Object o) {
		for (int i = buckets.size(), l = 0; i >= l; i--) {
			Object[] array = buckets.get(i);
			for (int j = (i == bucketIndex(size) ? indexWithinBucket(size) - 1 : array.length), k = 0; j >= k; j--) {
				if (o != null && o.equals(array[j])) {
					return i*BUCKET_SIZE + j;
				}
			}
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		return new ListIterator<E>() {

			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < size;
			}

			@Override
			public E next() {
				return get(i++);
			}

			@Override
			public boolean hasPrevious() {
				return 0 < i;
			}

			@Override
			public E previous() {
				return get(--i);
			}

			@Override
			public int nextIndex() {
				return i + 1;
			}

			@Override
			public int previousIndex() {
				return i - 1;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public void set(E e) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void add(E e) {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return new ListIterator<E>() {

			private int i = index;
			
			@Override
			public boolean hasNext() {
				return i < size;
			}

			@Override
			public E next() {
				return get(i++);
			}

			@Override
			public boolean hasPrevious() {
				return 0 < i;
			}

			@Override
			public E previous() {
				return get(--i);
			}

			@Override
			public int nextIndex() {
				return i + 1;
			}

			@Override
			public int previousIndex() {
				return i - 1;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public void set(E e) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void add(E e) {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		throw new NotImplementedException("TODO");
	}
	
	public static void main (String... args) {
		List<String> arrayList = new ArrayList<>();
		List<String> bucketArray = new BucketList<>();
		Random random = new Random(0l);
		int limit = 10000000;
		long tick, tack;
		tick = System.currentTimeMillis();
		// Add to array
		for (int i = 0; i < limit; i++) {
			arrayList.add("a" + random.nextInt(limit * 2));
		}
		log.info("Added " + arrayList.size() + " elements in " + ((tack = System.currentTimeMillis()) - tick));
		tick = tack;
		// Add to bucketarray
		for (int i = 0; i < limit; i++) {
			bucketArray.add("a" + random.nextInt(limit * 2));
		}
		log.info("Added " + bucketArray.size() + " elements in " + ((tack = System.currentTimeMillis()) - tick));
		tick = tack;
	}
}

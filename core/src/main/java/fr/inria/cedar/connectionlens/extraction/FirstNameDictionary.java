/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.extraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.HashMultimap;

import fr.inria.cedar.connectionlens.Config;

public class FirstNameDictionary {
	/**
	 * to store associations between our language locales and those known in the
	 * Prenoms.csv resource
	 */
	private static HashMap<String, String> localePrefixes;
	private static String locale; 
	private HashMultimap<String, String> firstNamesWithLocales;

	private static FirstNameDictionary theInstance = null;
	
	private static FirstNameDictionary getInstance() {
		if (theInstance == null) {
			theInstance = new FirstNameDictionary();
		}
		return theInstance; 
	}
	private FirstNameDictionary() {
		localePrefixes = new HashMap<String, String>();
		locale = Config.getInstance().getProperty("default_locale"); 
		firstNamesWithLocales  =  HashMultimap.create();
		populateFirstNames(); 
	}
	private void populateFirstNames() {
		localePrefixes = new HashMap<>();
		localePrefixes.put("fr", "french");
		localePrefixes.put("en", "english");
		try {

			BufferedReader br = new BufferedReader(
					new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("Prenoms.csv"),"ISO-8859-1"));

			/*BufferedReader br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemClassLoader().getResourceAsStream("Prenoms.csv"),
					"ISO-8859-1"));*/
			String line = null;
			boolean over = false;
			while (!over) {
				try{
					line = br.readLine();
					//log.info(line);
					if (line != null) {
						String[] components = line.split(";");
						String prenom = components[0];
						if (prenom.indexOf("(") > 0) { // some first names have different variants
							// in different languages, indicated as " (1)", " (2)" etc.
							// Here we clean up the name to forget about the variant.
							int firstParen = prenom.indexOf("(");
							prenom = prenom.substring(0, firstParen).trim();
						}
						HashSet<String> simpleSpellings = eraseAccents(prenom);
						String allLocales = components[2];
						String[] locales = allLocales.split(",");
						for (String locale: locales) {
							// all locales but the first come with a leading whitespace
							//log.info("Prénom: " + prenom + " locale: |" + locale.replace(" ","") + "|");
							firstNamesWithLocales.put(prenom, locale.replace(" ", ""));
							for (String simpleSpelling: simpleSpellings) {
								this.firstNamesWithLocales.put(simpleSpelling, locale.replace(" ", ""));
							}
						}
					}
					else {
						over = true; 
					}
				}
				catch (IOException e) {
					over = true; 
					//e.printStackTrace();
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			throw new IllegalStateException("File not found: resources/Prenoms.csv"); 
		} catch (IOException e) {
			throw new IllegalStateException("IOException working with resources/Prenoms.csv"); 
		} 
	}
	
	/**
	 * We could replace any subset of the accented characters with their accent-less
	 * counterparts.
	 * In practice we take a "all or nothing" approach: replace all occurrences of all accented characters, with their counterparts.
	 * @param prenom
	 * @return
	 */
	private HashSet<String> eraseAccents(String prenom) {
		HashSet<String> simplifiedSpellings = new HashSet<String>();
		String result = prenom.replaceAll("à", "a");
		result = result.replaceAll("â", "a");
		result = result.replaceAll("ä", "a"); 
		result = result.replaceAll("é", "e");
		result = result.replaceAll("è", "e");
		result = result.replaceAll("ë", "e");
		result = result.replaceAll("ï", "i");
		result = result.replaceAll("î", "i");
		result = result.replaceAll("í", "i");
		result = result.replaceAll("ô", "o");
		result = result.replaceAll("û", "u");
		result = result.replaceAll("ù", "u");
		result = result.replaceAll("ü", "u");
		result = result.replaceAll("ç", "c");
		simplifiedSpellings.add(result); 
		return simplifiedSpellings; 
	}
	
	/**
	 * The table of given names (prénoms) has names from many languages of the
	 * world, e.g.: paris;m;greek mythology;264.10 It makes sense to ensure that the
	 * word we found is a prénom in the right language. 
	 * This also searches for composite names (with "-").
	 */
	private boolean inFirstNameDictionary(String label) {
		String lowerLabel = label.toLowerCase();
		//log.info("\nISFIRSTNAME? |" + lowerLabel + "|");
		Set<String> locales = firstNamesWithLocales.get(lowerLabel);
		if (locales != null && locales.size() > 0) { 
			//log.info("Found some locales for " + lowerLabel);
			if (locales.contains(localePrefixes.get(locale))){
				//log.info("Direct first name: " + lowerLabel);
				return true;
			}
			//else {
			//	log.info("But not " + localePrefixes.get(locale));
			//}
		}
		else { 
			// locales is empty (with a Multimap, it's never null)
			// maybe this is a composite first name (with a "-")? 
			if (lowerLabel.indexOf('-') >= 0) {
				String tVal1 = lowerLabel.substring(0, lowerLabel.indexOf('-'));
				String tVal2 = lowerLabel.substring(lowerLabel.indexOf('-') + 1, lowerLabel.length());
				//log.info("Trying: |" + tVal1 + "| and |" + tVal2 + "|" );
				if (!FirstNameDictionary.isFirstName(tVal1) ) {
					//log.info(tVal1 + " not a first name!");
					return false; 
				}
				if (!FirstNameDictionary.isFirstName(tVal2) ) {
					//log.info(tVal2 + " not a first name!");
					return false;
				}
				//log.info("Composite first name: " + lowerLabel);
				return true; 	
			}	
			else { // try simplified spellings
				for (String simplified: eraseAccents(lowerLabel)) {
					if (!simplified.equals(lowerLabel)) { // avoid infinite loop
						if (FirstNameDictionary.isFirstName(simplified)) {
							return true; 
						}
					}
				}
			}
		}	
		//log.info("No");
		return false; 
	}
	
	public static boolean isFirstName(String label) {
		return getInstance().inFirstNameDictionary(label); 
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import java.util.Collection;

/**
 * Objects for which a score may be computed notably using its underlying nodes and edges.
 */
public interface Scorable {
	
	/**
	 * @return a collection of nodes the Scorable consists of
	 */
	Collection<Node> nodes();
	
	/**
	 * @return a collection of edges the Scorable consists of
	 */
	Collection<Edge> edges();
	
	/**
	 * @param f some scoring function
	 * @return the computed score
	 */
	Score score(ScoringFunction f);
}

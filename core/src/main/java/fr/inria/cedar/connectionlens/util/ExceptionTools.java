package fr.inria.cedar.connectionlens.util;

import javax.annotation.Nullable;

public class ExceptionTools {
	public static String prettyPrintException(Exception e, @Nullable Integer linesNo) {
		StackTraceElement[] allFrames = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		int iFrame = 0; 
		for (StackTraceElement ste: allFrames) {
			if (ste.toString().contains("connectionlens")) {
				if ((linesNo==null) || ((linesNo != null) && (iFrame < linesNo.intValue()))){
					sb.append(ste.toString());
					sb.append("\n");
					iFrame ++; 
				}
			}
		}
		return new String(sb); 
	}
}

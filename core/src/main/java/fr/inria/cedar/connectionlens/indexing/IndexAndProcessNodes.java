/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.indexing;

import java.util.Set;
import java.util.function.Consumer;

import fr.inria.cedar.connectionlens.extraction.ExtractorBatch;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.util.Session;

/**
 * IM 22/3/2020
 * This does two things:
 * - interface for a node index (getting the nodes matching a certain kwd)
 * - it is also the single place in which entity extraction is handled. 
 * The reason why we have two functionalities in one class is that processing a node (notably indexing it)
 * leads to creating other nodes. 
 */
public interface IndexAndProcessNodes {

	/**
	 * IM index
	 * Resets the index.
	 */
	void reset();

	/**
	 * IM index
	 * @param kw some query component
	 * @return the set of nodes matching the given query component
	 */
	Set<Node> getNodeMatches(QueryComponent kw, Node.Types... types);

	/**
	 * IM index
	 * @param kw some query component
	 * @return the set of nodes matching the given query component
	 */
	Set<Node> getExactNodeMatches(QueryComponent kw, Node.Types... types);

	
	/**
	 * IM index
	 * @param kw some query component
	 * @return the set of edges matching the given query component
	 */
	Set<Edge> getEdgeMatches(QueryComponent kw, Edge.Types... types);

	/**
	 * IM index
	 * @param kw some query component
	 * @return the set of items matching the given query component
	 */
	Set<Item> getItemMatches(QueryComponent kw);

	/**
	 * IM index
	 * @param ds some data source
	 * @param kw some query component
	 * @return the set of items matching the given query component, within the given
	 *         data source.
	 */
	Set<Item> getItemMatches(DataSource ds, QueryComponent kw);

	/**
	 * IM index
	 * @param kw some query component
	 * @return the set of data sources containing items matching the given query
	 *         component.
	 */
	Set<DataSource> getDataSourceMatches(QueryComponent kw);

	/**
	 * IM index
	 * @return the indexing model used by the underlying index implementation.
	 */
	IndexingModels indexingModel();


	/**
	 * IM index
	 * @param kw
	 * @return the stem of this string using the stemming method used in this Indexing method.
	 */
	String getStemOfString(String kw);

	/**
	 * IM index
	 * Open an index session, that may be used for updates.
	 *
	 * @return the new index session
	 */
	default IndexAndProcessNodesSession openSession(Graph g) {
		return openSession(-1, g);
	}

	/**
	 * IM This is in principle for indexing but it is also the one who does the entity extraction.
	 * The entity extraction logic has to be moved somewhere else. 
	 * 
	 * Open an index session, that may be used for updates, with a given batch size.
	 *
	 * @param batchSize determines the size of batches to use for updates of the
	 *                  index.
	 * @return the new index session
	 */
	IndexAndProcessNodesSession openSession(int batchSize, Graph g);
	
	/**
	 * Close.
	 **/
	void close();


	void commit();


	/**
	 * 
	 * @return true if the graph is of type ABSTRACT
	 */
	boolean isAbstract();

	/**
	 *
	 * @return true if the graph is of type NORMALIZED
	 */
	boolean isNormalized();

	/**
	 * 
	 * @return the abstract index. Null if not previously set.
	 */
	IndexAndProcessNodes abstractIndex();

	/**
	 * Updates the value of the abstract index.
	 * @param index the new abstract index
	 */
	void updateAbstractIndex(IndexAndProcessNodes index);

	/**
	 * Interface for IndexSession.
	 */
	public static interface IndexAndProcessNodesSession extends Session {

		/**
		 * Adds an edge.
		 *
		 * @param e the edge to add
		 */
		void addEdge(Edge e);

		/**
		 * Adds the given entry (i.e. keyword, item pair) to the index.
		 *
		 * @param n the node to add
		 */
		void addNode(Node n, Consumer<Edge> processor);

		/**
		 * @param n some node
		 * @return true, iff the given node has already been processed within this
		 *         session.
		 */
		boolean hasProcessed(Node n);

		/**
		 * @param e some edge
		 * @return true, iff the given edge has already been processed within this
		 *         session.
		 */
		boolean hasProcessed(Edge e);
		
		/** Needed for batched extraction, to clean up the last batch which may be incomplete */
		void processAndFlushExtractorBatch(ExtractorBatch extractorBatch); 

	}

	void setCatalog(DataSourceCatalog catalog);  


}
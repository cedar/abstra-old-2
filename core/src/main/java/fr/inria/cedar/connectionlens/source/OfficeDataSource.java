package fr.inria.cedar.connectionlens.source;

import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeUtils;
import org.jodconverter.local.JodConverter;
import org.jodconverter.local.office.LocalOfficeManager;

import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.apache.commons.io.FileUtils.copyFile; 


/**
 * This does not extend DataSource since it behaves differently. 
 */

public class OfficeDataSource {

	static boolean exportToHTML = true; 
	// if false, export to XHTML then rename into XML

    String officePath;
    String officeName;

	/** The logger. */
	private static final Logger log = Logger.getLogger(OfficeDataSource.class);


    public OfficeDataSource(String givenOfficePath) {
          this.officePath = givenOfficePath;
          this.officeName = givenOfficePath.replaceAll(".doc","").replaceAll(".docx", "")
        		  .replaceAll(".odp", "").replaceAll(".ods", "")
        		  .replaceAll(".odt", "").replaceAll(".ppt", "")
        		  .replaceAll(".pptx", "").replaceAll(".xls", "")
        		  .replaceAll(".xlsx", ""); 
          log.info("Path to Office document: " + officePath);
          //log.info("Office name: " + officeName); 
    }


    /**
     * This is supposed to lead to the creation of the converted file.
     * @throws IOException 
     * @throws OfficeException 
     */
    public  ArrayList<URI> convert() throws IOException {
    	File inputFile = new File(officePath);
    	
		Path officeExtractedPath = Paths.get(inputFile.getParent(), "extracted_files_" + getOfficeName());

		//log.info("Directory for extracted files" + officeExtractedPath);
		if (Files.exists(officeExtractedPath)) {
			File oldExtractedFolder = new File(
					Paths.get(inputFile.getParent(), "extracted_files_" + getOfficeName()).toString());
			deleteDirectory(oldExtractedFolder);
		}

		File outputFile = new File(officeExtractedPath.toString() + File.separator + officeName + 
				(exportToHTML?".html":".xhtml"));
	    log.info("Exporting in: " + outputFile.getCanonicalPath()); 
    	// Create an office manager using the default configuration.
    	// The default port is 2002. Note that when an office manager
    	// is installed, it will be the one used by default when
    	// a converter is created.
    	ArrayList<URI> results = new ArrayList<URI>(); 
    	final LocalOfficeManager officeManager = LocalOfficeManager.install(); 
    	try {

    	    // Start an office process and connect to the started instance (on port 2002).
    	    officeManager.start();

    	    // Convert
    	    JodConverter
    	             .convert(inputFile)
    	             .to(outputFile)
    	             .execute();
    	    if (exportToHTML) {
    	    	results.add(new URI("file:" + outputFile.getPath())); 
    	    }
    	    else {
    	    	File renamedOutputFile = new File(officeExtractedPath.toString() + File.separator + officeName + ".xml");
    	    	copyFile(outputFile, renamedOutputFile); 
    	    	results.add(new URI("file:" + renamedOutputFile.getPath()));  
    	    }
    	} 
    	catch(OfficeException | URISyntaxException e) {
    		log.info(e.getMessage()); 
    	}
    	finally {
    	    // Stop the office process
    	    OfficeUtils.stopQuietly(officeManager);
    	}
    	return results;  
    }


    public String getOfficePath() {
        return officePath;
    }

    public String getOfficeName() {
        return officeName;
    }


}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

public class GraphExport2JSON {
    private HashMap<Node, HashSet<Node>> representation = new HashMap<>();
    private HashMap<Node, Node> nodeRepresentedBy = new HashMap<>();
    private String epoch="";
    private  String epochDrawDir="";
    private Boolean isLocalId = false; // use localId instead of globalId
    private Boolean includeSimilarEdges = false; //include similar links.


    public GraphExport2JSON() {

    }


    public GraphExport2JSON (String epochDrawDir, String epoch) {

        this.epochDrawDir = epochDrawDir;
        this.epoch = epoch;
    }

    public void setIncludeSameAs (Boolean includeSimilarEdges) {
        this.includeSimilarEdges = includeSimilarEdges;
    }
    public void setLocalId (Boolean localId) {
        isLocalId = localId;
    }

    private void processRepresentatives(Graph graph, DataSource ds) {
        for (Node node : graph.getNodes(ds)) {
            Node representativeNode = node.getRepresentative();
            if (!representation.containsKey(representativeNode)) {
                representation.put(representativeNode, new HashSet<Node>(){{add(node);}});
            } else {
                representation.get(representativeNode).add(node);
            }
            if (!nodeRepresentedBy.containsKey(node)) {
                nodeRepresentedBy.put(node, representativeNode);
            }
        }
    }



    private static String sanitizeString(String s) {
        return s.replace("&", "|")
                .replace("NumericNodeID", "")
                .replace("(", "")
                .replace(")", "").trim();
    }




    private JSONArray processEdges(Graph graph, DataSource ds){
        JSONArray edges = new JSONArray();
        for (Edge edge : graph.getEdges(ds)) {

            DecimalFormat numberFormat = new DecimalFormat("#.00");
            JSONObject edgeObject = new JSONObject();
            if(isLocalId){
                String sourceLocalID = edge.getSourceNode().getId()
                        .toString()
                        +"@"+Math.abs(edge.getSourceNode().getDataSource().getLocalURI().hashCode());
                String targetLocalID = edge.getTargetNode().getId()
                        .toString()
                        +"@"+Math.abs(edge.getTargetNode().getDataSource().getLocalURI().hashCode());
                edgeObject.put("source", sourceLocalID);
                edgeObject.put("target", targetLocalID);
            }
            else{
                Node sourceNode = edge.getSourceNode().getRepresentative();
                Node targetNode = edge.getTargetNode().getRepresentative();

                if (nodeRepresentedBy.containsKey(edge.getTargetNode())) {
                    targetNode = nodeRepresentedBy.get(edge.getTargetNode());
                }
                edgeObject.put("source", sourceNode.getId().value().toString());
                edgeObject.put("target", targetNode.getId().value().toString());
            }
            edgeObject.put("label", edge.getLabel());
            edgeObject.put("confidence",  numberFormat.format(edge.confidence()));
            //log.info("extract final specificity... "+edge.getLabel()+" soec "+numberFormat.format(edge.getSpecificityValue()));
            edgeObject.put("specificity", numberFormat.format(edge.getSpecificityValue()));
            edges.put(edgeObject);
        }
        return edges;
    }



    private JSONArray processSimilarEdges(Graph graph){
        JSONArray edges = new JSONArray();
        for (Edge edge : graph.getEdges(Edge.SAME_AS_LABEL)) {
            Node sourceNode = edge.getSourceNode().getRepresentative();
            Node targetNode = edge.getTargetNode().getRepresentative();
            if (nodeRepresentedBy.containsKey(edge.getTargetNode())) {
                targetNode = nodeRepresentedBy.get(edge.getTargetNode());
            }
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            JSONObject edgeObject = new JSONObject();
            if(isLocalId){
                String sourceLocalID = edge.getSourceNode().getId()
                        .toString()
                        +"@"+Math.abs(edge.getSourceNode().getDataSource().getLocalURI().hashCode());
                String targetLocalID = edge.getTargetNode().getId()
                        .toString()
                        +"@"+Math.abs(edge.getTargetNode().getDataSource().getLocalURI().hashCode());

                //weak-same as in the same-edge table (are in one direction)
                if(edge.confidence() < 1.0){
                    Integer nodeSource = (Integer) edge.getSourceNode().getId().value()
                            ;
                    Integer nodeTarget = (Integer) edge.getTargetNode().getId().value()
                            ;
                    if(nodeSource < nodeTarget ){
                        edgeObject.put("source", sourceLocalID);
                        edgeObject.put("target", targetLocalID);
                    }
                    else{
                        edgeObject.put("source", targetLocalID);
                        edgeObject.put("target", sourceLocalID);
                    }
                }
                else{
                    edgeObject.put("source", sourceLocalID);
                    edgeObject.put("target", targetLocalID);
                }
            }
            else{
                edgeObject.put("source", sourceNode.getId().value().toString());
                edgeObject.put("target", targetNode.getId().value().toString());
            }
            edgeObject.put("label", edge.getLabel());
            edgeObject.put("confidence",  numberFormat.format(edge.confidence()));
            edgeObject.put("specificity", numberFormat.format(edge.getSpecificityValue()));
            edges.put(edgeObject);
        }
        return edges;
    }
    private  JSONArray processNodes() {
        JSONArray nodes = new JSONArray();
        for (Node representativeNode : representation.keySet()) {
            JSONObject nodeObject = new JSONObject();
            if(isLocalId){
                nodeObject.put("id", representativeNode.getId().value());
                        //+"@"+Math.abs(representativeNode.getDataSource().getURI().hashCode()));
            }
            else{
                nodeObject.put("id", sanitizeString(representativeNode.getId().toString()));
                nodeObject.put("datasetId", representativeNode.getDataSource().getID());
            }
            nodeObject.put("label", representativeNode.getLabel());
            nodeObject.put("type", representativeNode.getType().name());

            HashSet<Node> representedNodes = representation.get(representativeNode);
            Set<String> repIds = new HashSet<>();
            for (Node n : representedNodes) {
                /*if (representativeNode != n) {
                    repIds.add(isLocalId ? n.getId().localID()
                            +"@"+Math.abs(n.getDataSource().getURI().hashCode())
                            :sanitizeString(n.getId().toString()));
                }*/

                if (representativeNode != n) {
                    repIds.add(isLocalId ? n.getRepresentative().getId().value().toString()
                            :sanitizeString(n.getId().toString()));
                }
            }

            if (!repIds.isEmpty()) {
                nodeObject.put("representativeNodeIds", new JSONArray(repIds));
            }

            nodes.put(nodeObject);
        }
        return nodes;
    }

    public void export(Graph graph,
                       Collection<DataSource> dataSources) {

        if(epochDrawDir.isEmpty()) {
            epochDrawDir = Config.getInstance().getStringProperty ("temp_dir");
        }

        if(epoch.isEmpty()){
            epoch = String.valueOf(System.currentTimeMillis());
        }


        for (DataSource ds : dataSources) {
            processRepresentatives(graph, ds);
        }

        JSONArray links = new JSONArray();
        for (DataSource ds : dataSources) {
            links.put(processEdges(graph, ds));
        }

        JSONArray similarLinks = new JSONArray();


        //log.info(Paths.get(epochDrawDir, epoch + ".json").toString());
        String prefix = "";
        if (graph.isAbstract()){
            prefix = "abstract_graph_";
        }

        try (FileWriter file = new FileWriter(Paths.get(epochDrawDir, prefix + epoch + ".json").toString())) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("nodes", processNodes());
            jsonObject.put("links", links);
            if(this.includeSimilarEdges){
               jsonObject.put("similarLinks",processSimilarEdges(graph));
            }
            file.write(jsonObject.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

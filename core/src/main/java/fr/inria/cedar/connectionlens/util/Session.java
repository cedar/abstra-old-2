/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.extraction.ExtractorBatch;

/**
 * Interface for Session, i.e. sequence of modification of large possibly concurrent objects.
 */
public interface Session extends AutoCloseable {
	
	/**
	 * Commit.
	 */
	void commit();

	@Override
	default void close() {
		commit();
	}

}

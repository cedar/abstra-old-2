/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.indexing;

import com.google.common.base.Joiner;
import java.util.List;
import java.util.Objects;

/**
 * A NGram is a order-preserving conjunction of keywords.
 */
public class NGram extends Conjunction {

	/**
	 * Instantiates a new n gram.
	 *
	 * @param values the values
	 */
	public NGram(List<AtomicKeyword> values) {
		super(values);
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.Conjunction#value()
	 */
	@Override
	public String value() {
		return Joiner.on(" ").join(values);
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.Conjunction#toString()
	 */
	@Override
	public String toString() {
		return '"' + value() + '"';
	}
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.Conjunction#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(values);
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.indexing.Conjunction#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NGram other = (NGram) obj;
		return values.equals(other.values);
	}

	/**
	 * @param words an array of keywords.
	 * @return a fresh NGram instance matching the input array of keywords.
	 */
	public static NGram of(String... words) {
		return new NGram(AtomicKeyword.asList(words));
	}
}

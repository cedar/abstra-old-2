/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

/**
 * Common interface for Token, whether from morpho-syntactic analysis or entity extraction.
 */
public interface Token {
	
	/**
	 * @return the token's offset.
	 */
	long offset();
	
	/**
	 * @return the token's length.
	 */
	int length();
	
	/**
	 * @return the token's value.
	 */
	String value();
	
//	/**
//	 * @param parent the parent node, i.e. the node from which the entity was extracted.
//	 * @param type the entity type.
//	 * @return the URI representation of the token
//	 */
//	default String toUri(Node parent, Types type) {
//		NodeID uri = parent.getId();
//		if (parent instanceof DataSourceNode) {
//			uri = parent.getDataSource().getID();
//		} else {
//			uri = uri.substring(uri.indexOf("|") + 1).replace("|", ".");
//		}
//		return parent.getDataSource().getID() +
//				"|" + uri  + "|" +
//				"offset" + offset() + "|" +
//				"length" + length() + "|" +
//				type;
//	}
}

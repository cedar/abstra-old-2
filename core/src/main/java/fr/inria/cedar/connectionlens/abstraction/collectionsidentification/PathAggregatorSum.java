package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import java.util.ArrayList;

public class PathAggregatorSum implements PathAggregator {
    @Override
    public double aggregate(ArrayList<Path> pathsToAggregate, boolean workingGraph) {
        double sumPathTransferFactor = 0.0d;
        for(Path p : pathsToAggregate) {
            if(p.doesNotContainCyclicEdges(workingGraph)) { // we don't take into account path with cyclic edges because they are not part of the path tranfer factor
                sumPathTransferFactor += p.getPathTransferFactor();
            }
        }
        return sumPathTransferFactor;
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.function.Consumer;


import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.NumericIDFactory;
import fr.inria.cedar.connectionlens.sql.schema.SQLPathUtils;
import fr.inria.cedar.connectionlens.util.AnswerTreeDOTPrinting;
import fr.inria.cedar.connectionlens.util.SolutionTimeCurve;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;

public class SQLSearch extends QuerySearch {

	int maxPathLength = Config.getInstance().getIntProperty("sql_search_max_length"); 
	long startTime; 
	
	private final boolean drawAnswerTrees =
			Config.getInstance().getBooleanProperty("drawing.draw");
	private final boolean drawSolutionTimes =
			Config.getInstance().getBooleanProperty("drawing.solution_times"); 
	 
	private AnswerTreeDOTPrinting treeDrawer; 
	
	  /** The logger. */
    private final Logger log = Logger.getLogger(SQLSearch.class);
    
    private Query statsObj;

	public SQLSearch(IndexAndProcessNodes i, Graph g, ScoringFunction sf, StatisticsCollector st) {
		this(i, g, sf, st, false);
		//treeDrawer = new AnswerTreeDOTPrinting();
	}
	public SQLSearch(IndexAndProcessNodes i, Graph g, ScoringFunction sf, StatisticsCollector st,
			boolean ignoreGrowAcross) {
		super(i, g, sf, st);

		//treeDrawer = new AnswerTreeDOTPrinting();
	}
	 
	@Override
	public void run(Query q, Consumer<AnswerTree> processor) {
		this.statsObj = q;
		treeDrawer = new AnswerTreeDOTPrinting(q);
		if (q.components.size() != 2) {
			log.error("SQLSearch currently only implemented for queries of size 2");
			return; 
		}
		long startTime = System.currentTimeMillis();
		NumericIDFactory factory = new NumericIDFactory(); 
		String kwd1 = q.components.get(0).toString();
		String kwd2 = q.components.get(1).toString();
		for (int i = 1; i <= maxPathLength; i ++) {
			String queryAtI = SQLPathUtils.twoKwdPathQuerySelectAll(kwd1, kwd2, i); 
			log.info("Running:\n" + queryAtI);
			Connection conn = ConnectionManager.getMasterConnection(); 
			try {
				ResultSet rs = conn.createStatement().executeQuery(queryAtI);
				while (rs.next()) {
					//log.info("Solution of " +  i + " edges! ");
					StringBuffer sb = new StringBuffer();
					Node n1 = graph.resolveNode(factory.parseNodeID(rs.getInt(1)));
					Node n2 = graph.resolveNode(factory.parseNodeID(rs.getInt(i+2))); 
					SetMultimap<Integer, Item> matches = HashMultimap.create(); 
					matches.put(0, n1);
					matches.put(1, n2);
					HashSet<Edge> edges = new HashSet<Edge>();
					HashSet<Node> nodes = new HashSet<Node>();
					nodes.add(n1);
					nodes.add(n2); 
					for (int j = 2; j<=i+1; j++) {
						Edge e = graph.resolveEdge(factory.parseEdgeID(rs.getInt(j)));
						nodes.add(e.getSourceNode());
						nodes.add(e.getTargetNode()); 
						edges.add(e); 
						if (j>2) {
							sb.append(", "); 
						}
						sb.append(e.toString());
					}
					AnswerTree at = new AnswerTree(nodes, edges, 
							matches); 
					// IM 25/11/2021: too lazy to actualy look into which kwds are matched by the root. TODO 
					processor.accept(at);
					if(drawAnswerTrees) {
						treeDrawer.print(at, "SQL", true, this.scoring);
					}
					if(this.drawSolutionTimes) {
						SolutionTimeCurve.recordSolutionTime(q, System.currentTimeMillis() - this.startTime);  
					}
					log.info(at.toString());
					//log.info(new String(sb));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}

	@Override
	public void collectStats() {
		if (this.drawAnswerTrees){
			treeDrawer.generateSolutionLatexBook(); 
		}
		if (this.drawSolutionTimes) {
			SolutionTimeCurve.printSolutionTimes(statsObj, this.getClass().getName()); 
		}
	}

}

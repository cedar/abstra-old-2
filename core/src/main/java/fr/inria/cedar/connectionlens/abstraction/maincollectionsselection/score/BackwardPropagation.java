package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.*;

public class BackwardPropagation {
    public static final Logger log = Logger.getLogger(BackwardPropagation.class);

    public BackwardPropagation() {
        super();
    }

    public void initWeights(HashMap<Integer, Double> initialOwnDataWeights) {
        log.debug("init odw with " + initialOwnDataWeights);
        // we initialize dw of leaf collections as being their odw given in the map
        for(int i : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            if(initialOwnDataWeights.containsKey(i)) { // a leaf, its dw is initialized to its odw
                CollectionGraph.getWorkingInstance().setCollectionScore(i, initialOwnDataWeights.get(i));
            } else { // not a leaf or no weight for the leaf, its dw is initialized to 0
                CollectionGraph.getWorkingInstance().setCollectionScore(i, 0.0d);
            }
        }
    }

    /**
     * Compute the data weight of each collection in the collection graph.
     * The data weight of a collection is first initialized with the number of value nodes it contains, 0 if none
     * Then the data weight is back-propagated along edges (with a special care for cyclic edges) from leaf collections (that have a dw > 0) to all other collections
     * by multiplying the leaf data weight by the total transfer for the pair (leaf collection, the collection)
     * @throws SQLException if a SQL query fails
     */
    public void propagateWeights(boolean workingGraph) {
        log.info("PROPAGATE WEIGHTS");
        // we propagate weight backward, only outside of the collection graph cycles.
        // Specifically, we assign to each collection a data weight dw, which on leaf collection is initialized to odw, and on others, to 0. (done before with initWeights method)
        // Then, for each non-leaf Ci, and non-cyclic path from Ci to a leaf collection Ck, we increase dw[Ci] by ptf[k][i]*odw[Ck].
        // for (collection i that is a non-leaf in the collection graph)
        //   for (collection k such that k is a leaf collection and there exists at least a path from i to k in the collection graph)
        //      dw[i] = dw[i] + (ptf[k][i] * dw[k]) // here dw[k] is equal to odw[k]
        for (int i : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            if (!CollectionGraph.getWorkingInstance().getLeafCollections().contains(i)) {
                for (int k : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
                    if(CollectionGraph.getWorkingInstance().getLeafCollections().contains(k)) {
                        this.increaseDataWeightOfCollectionIBasedOnLeafK(i, k, workingGraph);
                    }
                }
            }
        }
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
    }

    private void increaseDataWeightOfCollectionIBasedOnLeafK(int collI, int collK, boolean workingGraph) {
        // check if there exists at least a path from k to i in the collection graph
        // and check that this path does not contain a cyclic edge (i.e. the path should be free of cyclic edges)
        // because there may be several path from J to I, we iterate over each on -cyclic path to increase the weight of Cj according to all the non-cyclic paths
        ArrayList<Path> pathsFromItoK = CollectionGraph.getWorkingInstance().getAllPathsBetweenCollections(collI, collK);
        if(pathsFromItoK != null && !pathsFromItoK.isEmpty()) {
            for(Path path : pathsFromItoK) {
                // we check that none of the edge in the path is involved in a cycle
                if (path.doesNotContainCyclicEdges(workingGraph)) {
                    // propagate the data weight of the leaf collection k backwards to i
                    double newCollectionDataWeight = CollectionGraph.getWorkingInstance().getCollectionScore(collI) + (CollectionGraph.getWorkingInstance().getCollectionScore(collK) * path.getPathTransferFactor());
                    log.debug("dw(C" + collI + ") = dw(C" + collI + ") + (dw(C" + collK + ") * ptf[" + collI + "][" + collK + "]) = " + CollectionGraph.getWorkingInstance().getCollectionScore(collI) + " + (" + CollectionGraph.getWorkingInstance().getCollectionScore(collK) + " * " + path.getPathTransferFactor() + ") = " + newCollectionDataWeight + " on path " + path);
                    CollectionGraph.getWorkingInstance().setCollectionScore(collI, newCollectionDataWeight);
                } else {
//                    log.debug("path " + path  + " is cyclic, thus not countable for dw propagation");
                }
            }

        }
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.indexing.NGram;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;

/**
 * A keyword query.
 */
public class Query {

	/** Regular expression for parsing queries. */
	// was:
	//public static final Pattern QUERY_PATTERN = Pattern.compile("([^\"]\\S*|\".+?\")\\s*");
	// Ioana's: 
	public static final Pattern QUERY_PATTERN = Pattern.compile("((exact:)??([^\"]\\S*|\".+?\"))\\s*");

	/** The logger. */
	private static final Logger log = Logger.getLogger(Query.class);

	/** List of the original, unprocessed keywords */
	protected final List<QueryComponent> components;

	/**
	 * Create a new processedQuery, and pre-process keywords in a way that 
	 * it would match the keyword already stored in Index
	 * @param q
	 */
	Query(Collection<QueryComponent> q) {
		this.components = Lists.newArrayList(q);
	}

	/**
	 * Instantiates a new query.
	 *
	 * @param q the q
	 */
	Query(QueryComponent... q) {
		this(Lists.newArrayList(q));
	}

	/**
	 * Parses the input string and instantiates a Query based on it.
	 *
	 * @param queryString the query string
	 * @return the resulting query
	 */
	public static Query parse(String queryString) {
		String normalized = Strings.nullToEmpty(queryString);
		normalized = normalized.replaceAll(" +", " ");
		normalized = normalized.trim();
		Matcher matcher = QUERY_PATTERN.matcher(normalized);
		List<QueryComponent> result = new LinkedList<>();
		int i = 1; 
		while (matcher.find()) {
			String comp = matcher.group(); 
			if (comp != null) {
				comp = comp.trim(); 
				log.info("At " + i + " found |" + comp + "|");
				if (comp.startsWith("exact:")) {
					result.add(new AtomicKeyword(comp.replaceAll("\\+", " ")));
				}
				else {
					if (comp.startsWith("type:")) {
						result.add(new TypeSelector(comp.replace("type:", "").replaceAll("\\+", " ")));
					}
					else {
						if (comp.startsWith("\"") && comp.endsWith("\"")){
							String norm = comp.substring(1, comp.length() - 1);
							norm = norm.replaceAll("[^a-zA-Z0-9_\u00C0-\u017F\\-\\s\"]", " ");
							norm = norm.trim();
							List<AtomicKeyword> keywords = new ArrayList<>();
							for (String kwd : norm.split(" ")) {
								keywords.add(new AtomicKeyword(kwd));
							}
							result.add(new NGram(keywords));
						} else {
							result.add(new AtomicKeyword(comp));
						}
					}
				}
				i++;
			}
			else {
				break; 
			}
		}
		Query q = new Query(result);
		//log.info("Understood a query of " + result.size() + " components: " + q); 
		return q; 
	}

	/**
	 * @return the list of query components of the query
	 */
	public List<QueryComponent> getComponents() {
		return components;
	}

	/**
	 * @return the number of query components in the query
	 */
	public int size() {
		return components.size();
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object that) {
		if(!(that instanceof Query)) {
			return false;
		}
		if (this == that) {
			return true;
		}
		Query thatQuery = (Query) that;
		return components.equals(thatQuery.getComponents());
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(components);
	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Joiner.on("_").join(this.components);
	}
}

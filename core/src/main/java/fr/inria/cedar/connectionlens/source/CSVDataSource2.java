/*
 * Copyright(C) 2021 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 */

package fr.inria.cedar.connectionlens.source;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.util.Map;
import java.util.function.Consumer;

import static fr.inria.cedar.connectionlens.graph.Node.Types.RELATIONAL_STRUCT;

/**
 * Implementation of a CSV DataSource.
 *
 * @author Prajna Upadhyay
 *
 */
public class CSVDataSource2 extends OrderedTreeDataSource implements Serializable {

    private static final long serialVersionUID = 1L;

    public CSVDataSource2(Factory f, int identifier, URI uri, URI origURI, EntityExtractor extractor,
                          StatisticsCollector graphStats, StatisticsCollector extractStats, Graph graph) {
        super(f, identifier, uri, origURI, Types.CSV, graphStats, extractStats, graph);
        log = Logger.getLogger(CSVDataSource2.class);
    }


    @Override
    public String getContext(Graph g, Node n) {
        return "<i>Content of the CSV node:</i> <b>" + n.getLabel() + "</b>";
    }


    // TODO: 04/03/2022  : is the CSV library reading efficient?
    // TODO: 04/03/2022 test it on large benchmarks , load with per_value, without extraction

    @Override
    public void traverseEdges(Consumer<Edge> processor) throws IOException {
        String path = "";
        processEdgesToLocalAndOriginalURIs(processor);
        String file = localURI.toString().replace("file:","");
        Reader reader = new BufferedReader(new FileReader(file));
        Node topNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
        processor.accept(buildLabelledEdge(this.root, topNode, Edge.ROOT_LABEL));
        path = topNode.getLabel();
        try {
            // this part succeeds if there is a header for the csv file,
            // and associates each value to each header
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
            Map<String, Integer> header = csvParser.getHeaderMap();
            log.info("header of CSV file is: "+header);
            for(CSVRecord csvRecord: csvParser.getRecords()) {
                Node rowNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
                processor.accept(buildLabelledEdge(topNode, rowNode, Edge.TUPLE_EDGE_LABEL));
                this.recordSummaryNode(rowNode, topNode.getLabel());
                for(String columnName:csvRecord.toMap().keySet()) {
                    Node valNode = createOrFindValueNodeWithAtomicity(csvRecord.toMap().get(columnName), "", this, Node.Types.RELATIONAL_VALUE);
                    Edge edge = buildLabelledEdge(rowNode, valNode, columnName);
                    edge.setDataSource(this);
                    processor.accept(edge);
                    this.recordSummaryNode(rowNode, rowNode.getLabel());
                }
            }
        }
        catch(IllegalArgumentException e) {
            // in case there is no header in the csv file,
            // then we read it as an array of strings. We
            // populate the edges with empty labels
            try {
                log.info("CSV file does not contain a header");
                CSVReader csvreader = new CSVReader(new FileReader(file));
                String[] lineInFile;
                while ((lineInFile = csvreader.readNext()) != null) {
                    Node rowNode = buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
                    processor.accept(buildLabelledEdge(topNode, rowNode, Edge.TUPLE_EDGE_LABEL));
                    for(int i=0; i<lineInFile.length;i++) {
                        Node valNode = createOrFindValueNodeWithAtomicity(lineInFile[i], "", this, Node.Types.RELATIONAL_VALUE);
                        // since we do not have headers, we build unlabelled edges
                        Edge edge = buildUnlabelledEdge(rowNode, valNode, 1.0);
                        edge.setDataSource(this);
                        processor.accept(edge);
                    }
                }
            }
            catch (IOException | CsvValidationException ioException) {
                log.info("Something went wrong while reading the CSV file");
            }
        }
    }


    @Override
    public void postprocess(Graph graph) {
        // No-OP.
    }


}

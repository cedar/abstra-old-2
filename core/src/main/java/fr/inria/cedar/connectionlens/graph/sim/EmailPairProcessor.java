/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.Node.Types.EMAIL;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.withTypes;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;

import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
public class EmailPairProcessor extends EqualityProcessor {

    public EmailPairProcessor (StatisticsCollector stats, double th) {
        super (stats, th);
    }
    // no apply() method, use the one of the parent class.  

    @Override
    public NodePairSelector selector() {
       return super.selector().and(withTypes(BOTH, EMAIL)); 
    }

}

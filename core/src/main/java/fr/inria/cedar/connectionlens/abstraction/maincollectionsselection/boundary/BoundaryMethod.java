package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.boundary;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.*;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

import java.util.ArrayList;

import static fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification.MINIMAL_ETF_FLOODING;

public class BoundaryMethod {
    public static final Logger log = Logger.getLogger(BoundaryMethod.class);

    protected int cstar;
    protected Configuration configuration;
    protected ArrayList<Integer> alreadyVisitedCollections;

    public BoundaryMethod() {
    }

    public BoundaryMethod(int cstar, Configuration configuration) {
        this.init(cstar, configuration);
    }

    public void init(int cstar, Configuration configuration) {
        this.cstar = cstar;
        this.configuration = configuration;
        this.alreadyVisitedCollections = new ArrayList<>();
    }

    public CollectionBoundary compute() {
        throw new IllegalStateException();
    }

    void buildBoundaryRecursive(int currentNode, BoundaryNode currentBoundaryNode, CollectionBoundary rootNodeBoundary, Path currentPath, int currentDepth, int maxDepth) {
//        log.info("buildBoundaryRecursive(" + currentNode + ", " + currentBoundaryNode + ", " + rootNodeBoundary + ", " + currentPath + ", " + currentDepth + ", " + maxDepth + ", " + this.configuration.getScoringMethod() + ", " + this.configuration.getBoundaryMethod() + ")");
//        log.info(CollectionGraph.getWorkingInstance().getOutgoingEdges(currentNode));
        for(CollectionEdge ce : CollectionGraph.getWorkingInstance().getOutgoingEdges(currentNode)) {
            int nextNode = ce.getTarget();
            currentDepth++;
            boolean conditions = false;
            switch(this.configuration.getBoundaryMethod()) {
                case DAG:
                    // Take into the boundary only collections that have participated in the score of Cstar.
                    // However, this is not feasible for wPR because PR is global thus when there are cyclic all collections contribute to Cstar.
                    if(this.configuration.getScoringMethod() == Configuration.SCORING_METHOD.W_PR) {
                        conditions = this.canCallRecursionOnEdge(ce);
//                        log.debug("conditions are " + !this.alreadyVisitedCollections.contains(nextNode));
                    } else {
                        conditions = this.canCallRecursionOnEdge(ce) && CollectionGraph.getWorkingInstance().getAggregatedPathTransferFactor(nextNode, currentNode) > 0.0d;
//                        log.debug("conditions are " + !this.alreadyVisitedCollections.contains(nextNode) + " && " + (CollectionGraph.getWorkingInstance().getPathTransferFactor(nextNode, currentNode) > 0.0d));
                    }
                    break;
                case FL:
                    switch (this.configuration.getIdrefEdgesUsage()) {
                        case DISABLE:
                        case ENABLE_SCORE:
                            // in that case, we also verify that the edge is not an ID/IDREF edge. If this is, we do not add this edge to the boundary
                            conditions = this.canCallRecursionOnEdge(ce) && (ce.getEdgeTransferFactor() >= MINIMAL_ETF_FLOODING || ce.getIsAtMostOne()) && !CollectionGraph.getWorkingInstance().isIdRefEdge(ce);
                            break;
                        case ENABLE_BOUNDARY:
                            // we allow to add the IDREF collection edge to the boundary only if we explicitly allow it (if it is enabled for scoring or disabled, we don't add it to the boundary)
                            conditions = this.canCallRecursionOnEdge(ce) && (ce.getEdgeTransferFactor() >= MINIMAL_ETF_FLOODING || ce.getIsAtMostOne() || CollectionGraph.getWorkingInstance().isIdRefEdge(ce));
                            break;
                        default:
                            break;
                    }
//                    log.debug("conditions for C" + nextNode + " are (" + ce.getEdgeTransfer() + " >= " + MINIMAL_ETF_FLOODING + " = " + (ce.getEdgeTransfer() >= MINIMAL_ETF_FLOODING) + " || " + ce.getIsAtMostOne() + ") && " + !this.alreadyVisitedCollections.contains(nextNode));
                    break;
                case FLAC:
                    switch (this.configuration.getIdrefEdgesUsage()) {
                        case DISABLE:
                        case ENABLE_SCORE:
                            // in that case, we also verify that the edge is not an ID/IDREF edge. If this is, we do not add this edge to the boundary
                            conditions = this.canCallRecursionOnEdge(ce) && (ce.getEdgeTransferFactor() >= MINIMAL_ETF_FLOODING || ce.getIsAtMostOne()) && !CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(ce) && !CollectionGraph.getWorkingInstance().isIdRefEdge(ce);
                            break;
                        case ENABLE_BOUNDARY:
                            // in other cases, we allow to add the IDREF collection edge to the boundary
                            conditions = this.canCallRecursionOnEdge(ce) && (ce.getEdgeTransferFactor() >= MINIMAL_ETF_FLOODING || ce.getIsAtMostOne()) && !CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(ce);
                            break;
                        default:
                            break;
                    }
//                    log.debug("conditions for C" + nextNode + " are ((" + ce.getEdgeTransfer() + " >= " + MINIMAL_ETF_FLOODING + " = " + (ce.getEdgeTransfer() >= MINIMAL_ETF_FLOODING) + " && edgeInCycle = " + CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(ce) + ") || " + ce.getIsAtMostOne() + ") && " + !this.alreadyVisitedCollections.contains(nextNode));
                    break;
                case DESC:
                case LEAF:
                    conditions = this.canCallRecursionOnEdge(ce) && currentDepth <= maxDepth;
//                    log.debug("conditions are " +(currentDepth <= maxDepth));
                default:
                    break;
            }
//            log.info("conditions are " + (conditions ? "true" : "false") + " for " + ce);
            if(conditions) {
                // these two collections are linked and the path linking them is not cyclic
                this.alreadyVisitedCollections.add(currentNode);
                CollectionGraph.getWorkingInstance().addCollectionToSetOfCollectionsInvolvedInABoundary(currentNode); // we also register the fact that this collection is now part of a boundary
                BoundaryNode targetBoundaryNode = new BoundaryNode(nextNode, rootNodeBoundary);
                currentBoundaryNode.addChild(targetBoundaryNode);
                Path newPath = new Path(currentPath);
                newPath.addEdge(ce);

                this.buildBoundaryRecursive(nextNode, targetBoundaryNode, rootNodeBoundary, newPath, currentDepth, maxDepth);
            }
            currentDepth--;
        }

        if(CollectionGraph.getWorkingInstance().getOutgoingEdges(currentNode).isEmpty()) {
            // in case of leaves, we also want to add them to the set of collections involved in a boundary
            // because they have no children, when the recursion is called, the for loop does not execute, thus not adding it to the arraylist...
            CollectionGraph.getWorkingInstance().addCollectionToSetOfCollectionsInvolvedInABoundary(currentNode);
        }
    }

    // we call the recursive build of the boundary if:
    // - the target node has not been seen (e can safely go on this node)
    // - or if the target has been visited but the collection edge is not involved in a cycle. This allows to go on the same collection with multiple paths and avoids going back in the cycles.
    private boolean canCallRecursionOnEdge(CollectionEdge ce) {
        return !this.alreadyVisitedCollections.contains(ce.getTarget()) || (this.alreadyVisitedCollections.contains(ce.getTarget()) && !CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(ce));
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.graph.Edge;
import java.util.Comparator;

/**
 * Compares edges by their specificities, in ascending order.
 */
public class EdgeSpecificityComparator implements Comparator<Edge> {

	/**
	 * {@inheritDoc}
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Edge e1, Edge e2) {
		return(int) Math.signum(e1.getSpecificityValue() - e2.getSpecificityValue());
	}
}

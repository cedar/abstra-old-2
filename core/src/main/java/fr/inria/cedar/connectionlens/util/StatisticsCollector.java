/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.util;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.TOTAL_T;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

/**
 * A statistic collection, which keeps various values (times, cardinalities) in a table 
 * structure.
 */
public class StatisticsCollector extends Observable implements Serializable {
	
	public enum Kinds {GRAPH, EXTRACTION, INDEX, SIMILARITY, PATH, QUERY, ABSTRACTION_WORK}

	private static final Logger log = Logger.getLogger(StatisticsCollector.class);

	/** The statistics, in tuples of the form: <to whom we assign the stats; stat name; stat value> */
	private final Table<Object, String, Object> cells = HashBasedTable.create();

	/** The stop watches currently in use. */
	private final Map<Object, StopWatch> stopWatches = Maps.newLinkedHashMap();

	private Kinds kind;
	
	/** This serves as the single key for summing up lines in various statistic objects **/
	private static final String totalToken="TOTAL"; 
	public static String total() {
		return totalToken; 
	}
	
	/**
	 * Clear all statistics.
	 */
	public void clear() {
		this.cells.clear();
	}

	/**
	 * @return the whole statistics table
	 */
	public Table<Object, String, Object> cells() {
		return this.cells;
	}
	
	/**
	 * Puts a value in the given row and column.
	 *
	 * @param row the row
	 * @param col the column
	 * @param val the value
	 */
	public void put(Object row, String col, Object val) {
		this.cells.put(row, col, val);
	}

	/**
	 * Puts a value in the given row and column.
	 *
	 * @param row the row
	 * @param key the column's key
	 * @param val the value
	 */
	public void put(Object row, StatisticsKeys key, Object val) {
		if (val instanceof Integer) {
			put(row, key.toString(), new AtomicInteger((Integer) val));
		} else {
			put(row, key.toString(), val);
		}
	}
	
	/**
	 * Increments a value in the given row and column.
	 *
	 * @param row the row
	 * @param key the column's key
	 * @param delta the value
	 */
	public void increment(Object row, StatisticsKeys key, int delta) {
		increment(row, key.toString(), delta);
	}
	
	/**
	 * Increments a value in the given row and column.
	 *
	 * @param row the row
	 * @param key the column's key
	 * @param delta the value to add
	 */
	public void increment(Object row, String key, int delta) {
		synchronized(row) {
			AtomicInteger count = (AtomicInteger) get(row, key);
			if (count == null) {
				count = new AtomicInteger(0);
			}
			count.addAndGet(delta);
			put(row, key, count);
		}
	}
	
	/**
	 * Increments a value in the given row and column.
	 *
	 * @param row the row
	 * @param key the column's key
	 */
	public void increment(Object row, StatisticsKeys key) {
		increment(row, key.toString(), 1);
	}
	
	/**
	 * Increments a value in the given row and column.
	 *
	 * @param row the row
	 * @param key the column's key
	 */
	public void increment(Object row, String key) {
		increment(row, key, 1);
	}

	/**
	 * Notifies that the given [Ioana: row?] has had an update
	 *
	 * @param row the row to which the notification applies.
	 */
	public void report(Object row) {
		setChanged();
		notifyObservers(row);
	}
	
	/**
	 * Notifies all observers that object has changed.
	 */
	public void reportAll() {
		System.out.println("=== " + kind + " statistics: "); 
		setChanged();
		notifyObservers();
	}
//
//	public void reportInline(){
//		log.info("|" + Joiner.on("|").join(cells.rowKeySet()) + "|");
//		List<String> values = new ArrayList<>();
//		for (Object key : cells.rowKeySet()) {
//			Object value = cells.get(key, cells.columnKeySet().toArray()[0]) == null ?
//					cells.get(key, cells.columnKeySet().toArray()[1]) :
//					cells.get(key, cells.columnKeySet().toArray()[0]);
//			values.add(value.toString());
//		}
//		log.info("|" + Joiner.on("|").join(values) + "|");
//	}
	
	/**
	 * Gets or create a stop watch for the given row (= 1 stop watch per object to whom we attach statistics, e.g.
	 * dataset or query)
	 * 
	 * @param row the row's name
	 * @return a (possibly fresh) stop watch for the given row.
	 */
	private StopWatch getOrCreate(Object row) {
		StopWatch sw = this.stopWatches.get(row);
		if (sw == null) {
			cells.put(row, TOTAL_T.toString(), 0);
			this.stopWatches.put(row, (sw = new StopWatch()));
		}
		return sw;
	}
	
	/**
	 * Gets a stop watch for the given row or fails if no stop watch exists for this row.
	 *
	 * @param row the row's name
	 * @return the stop watch for the given row.
	 */
	private StopWatch getOrFail(Object row) {
		StopWatch sw = this.stopWatches.get(row);
		if (sw == null) {
			display();
			throw new IllegalArgumentException("No stop watch for: |" + row + "| " + row.getClass().getSimpleName() + " in " + this);
			
		}
		return sw;
	}
	public void display() {
		System.out.println ("StatsCollector: " + this.cells == null? "null": (cells.size() + " cells"));
		for (Cell<Object, String, Object> c: this.cells.cellSet()) {
			System.out.println(c.getRowKey());
		}
		System.out.println("Stop watches: " + this.stopWatches==null?"null":(stopWatches.keySet().size() + " stop watches"));
		for (Object key: stopWatches.keySet()) {
			System.out.println("|"+key + "|" + key.getClass().getSimpleName() + ": " + stopWatches.get(key));
		}
	}
	/**
	 * Starts the stop watch on the given row.
	 *
	 * @param row the row's name
	 */
	public void start(Object row) {
		getOrCreate(row).start();
	}
	
	/**
	 * Pauses the stop watch on the given row.
	 *
	 * @param row the row's name
	 */
	public void pause(Object row) {
		getOrFail(row).pause();
	}
	
	/**
	 * Resumes the stop watch on the given row.
	 *
	 * @param row the row's name
	 */
	public void resume(Object row) {
		getOrCreate(row).resume();
	}

	/**
	 * Ticks the stop watch on the given row for the given key, i.e. accumulates on the given key
	 * the time spent since the start or the last tick, whichever comes last.
	 *
	 * @param row the row's name
	 * @param key the key
	 */
	public void tick(Object row, StatisticsKeys key) {
		tick(row, key.toString());
	}

	/**
	 * Ticks the stop watch on the given row for the given key, i.e. accumulates on the given key
	 * (e.g., dataset or query)
	 * the time spent since the start or the last tick, no matter what the tick was for (whichever comes last).
	 *
	 * @param row the row's name
	 * @param col the column
	 */
	public void tick(Object row, String col) {
		StopWatch sw = getOrFail(row);
		long lap = sw.lap();
		Object existing = cells.get(row, col);
		if (existing != null) {
			lap += Long.valueOf(String.valueOf(existing));
		}
		cells.put(row, col, lap);
	}

	/**
	 * Stops the stop watch on the given row and stores the total time..
	 *
	 * @param row the row's name
	 */
	public void stop(Object row) {
		StopWatch sw = getOrFail(row);
		cells.put(row, TOTAL_T.toString(), sw.total());
	}

	/**
	 * Stops all the stop watches and stores the total time.
	 */
	public void stopAll() {

		stopWatches.entrySet().forEach(e->{
			cells.put(e.getKey(), TOTAL_T.toString(), e.getValue().total());
		});
	}

	/**
	 * Returns the total time for the stop watch on the given row.
	 *
	 * @param row the row's name
	 * @return the total time recorded by the stop watch on the given row.
	 */
	public Long total(Object row) {
		return getOrFail(row).total();
	}
	
	/**
	 * Gets the value on the given row and column
	 *
	 * @param row the row's name
	 * @param key the column's key
	 * @return the value of the given row and column
	 */
	public Object get(Object row, String key) {
		return this.cells.get(row, key);
	}
	
	/**
	 * Gets the value on the given row and column
	 *
	 * @param row the row's name
	 * @param key the column's key
	 * @return the value of the given row and column
	 */
	public Object get(Object row, StatisticsKeys key) {
		return get(row, key.toString());
	}
	
	public static StatisticsCollector mute() {
		return new SilentStatsCollector();
	}
	
	/**
	 * A dummy statistics collector that has no effect. Typically used to spend the system when 
	 * internal statistics are not necessary. 
	 */

	public static class SilentStatsCollector extends StatisticsCollector {

		@Override
		public void put(Object row, String col, Object val) {/* No-op */}

		@Override
		public void report(Object row) {/* No-op */}

		@Override
		public void reportAll() {/* No-op */}

		@Override
		public void start(Object row) {/* No-op */}

		@Override
		public void pause(Object row) {/* No-op */}

		@Override
		public void resume(Object row) {/* No-op */}

		@Override
		public void tick(Object row, String col) {/* No-op */}

		@Override
		public void stop(Object row) {/* No-op */}

		@Override
		public Long total(Object row) {return 0l;}
	}

	/** To allow a stat collector to know what kind of statistics it has */
	public void setKey(Kinds key) {
		this.kind = key; 
	}
}

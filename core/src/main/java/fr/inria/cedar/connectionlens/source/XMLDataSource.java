/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import fr.inria.cedar.connectionlens.extraction.PolicyDrivenExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.text.StringEscapeUtils;

import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.function.Consumer;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;

/**
 * @author Tayeb Merabti, Ioana Manolescu
 */
public class XMLDataSource extends OrderedTreeDataSource {

	/**
	 * get the XML policies
	 **/
	private final XMLPolicies xmlPolicies = XMLPolicies.getInstance();
	private final IgnoredTags ignoredTags = new IgnoredTags();


	public XMLDataSource(ItemID.Factory f, int identifier, URI uri, URI origURI, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.XML, graphStats, extractStats, graph);

	}
	

	/** This is used by the GUI to show some info about a node. It has nothing to do 
	 * with the structural contexts used a. to avoid or simplify extraction and
	 * b. to build the dataguide
	 */
	public String getContext(Graph g, Node n) {
		StringBuffer sb = new StringBuffer();
		Collection<Edge> incoming = g.getKIncomingEdgesPostLoading(n, Graph.maxEdgesReadForContext);
		for (Edge e: incoming) {
			Node parent = e.getSourceNode();
			if (n.getType() == Node.Types.XML_TAG_VALUE) { // literal, or attribute value
				if (e.getLabel().length() == 0) { // literal
					sb.append("Text content <b>" + n.getLabel() + "</b>" + 
							" of an XML element labeled <b>" + parent.getLabel()+ "</b>"); 
				}
				else { // attribute value
					sb.append("Value <b>" + n.getLabel() + "</b> of attribute <b>" + 
							e.getLabel()  + "</b> of XML element labeled <b>" + parent.getLabel()+
							"</b>"); 
				}
			}
			if (n.getType() == Node.Types.XML_TAG_NODE) { // element
				sb.append("XML element labeled <b>" + n.getLabel() + "</b>"); 
			}
			break; // this value node may come from different
			// contexts. We just show the first.
		}
		sb.append(" from XML doc: <i>" + n.getDataSource().getLocalURI()+ "</i>");
		return new String(sb); 
	}

	public void traverseEdges(Consumer<Edge> processor) {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty("javax.xml.stream.isCoalescing", true);
		XMLEventReader reader = null;
		List<String> xmlNodeAttributes = xmlPolicies.getAttributesPolicies();
		try {
			reader = factory.createXMLEventReader(new FileReader(localURI.toURL().getFile().replaceAll("%20", " ")));
			Set<Edge> edges = new LinkedHashSet<>();
			Node top = null;
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.getEventType() == XMLStreamConstants.START_ELEMENT) {
					String rootElementLabel = event.asStartElement().getName().toString();
					top = traverseXMLTree(reader, rootElementLabel, "1", rootElementLabel, edges, processor);
					this.processAttributesOfNode(event, top, null, top.getLabel(), top.getLabel(), processor, edges);
					Edge edge = buildLabelledEdge(root, top, Edge.ROOT_LABEL);
					processor.accept(edge);
					processEdgesToLocalAndOriginalURIs(processor);
					break;

				}
			}

		} catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
		// last thing: 
		finalizeSummaryComputation();
	}

	/**
	 * traverse an XML document
	 *
	 * @param dew
	 * @param path
	 * @param edges
	 * @param processor
	 * @return HTML node
	 * @throws IOException 
	 */
	private Node traverseXMLTree(XMLEventReader reader, String elementName, String dew, String path, Set<Edge> edges,
			Consumer<Edge> processor) throws XMLStreamException {
		int currDew = 1;
		Node xmlNode = buildLabelNodeOfType(" "+elementName+" ", Node.Types.XML_TAG_NODE);
		boolean thisElementStartsNoExtractZone = false; // local variable to know when to turn extraction off
		if (graph.extractPolicy().saysNoExtractAll(path)) {
			this.noExtractionInterval = true;
			// log.info("Entered noExtractAll zone on " + path);
			thisElementStartsNoExtractZone = true;
		}
		//log.info(xmlNode.getId().value() + " " + path ); //+ " and dewey: " + dew);
		this.recordSummaryNode(xmlNode, path);
		while (reader.hasNext()) {
			Node child = null;
			String currDews = makeDewElement(dew, currDew);
			XMLEvent event = reader.nextEvent();
			if (event.getEventType() == XMLStreamReader.CHARACTERS) {
				Characters characters = event.asCharacters();
				StringBuilder result = new StringBuilder();
				result.append(StringEscapeUtils.unescapeXml(characters.getData()));
				String text = result.toString().trim();
				if (!isNullOrEmpty(text)) {
					for (String sent : getSentences(text)) {
						int thisNodeContext;
						if (noExtractionInterval) { // we are in a no-extraction interval
							thisNodeContext = PolicyDrivenExtractor.noExtractContext();
						} else { // outside of a no-extraction interval, we record paths in the context map
							Integer pathNo = contextMap.get(path);
							if (pathNo == null) {
								pathNo = contextMap.size(); // Extraction context numbering starts at 0
								contextMap.put(path, pathNo);
								revContextMap.put(pathNo, path);
								//log.info("Path " + path + " is " + pathNo);
							}
							thisNodeContext = pathNo.intValue();
						}
						// log.info(path + "#." + sent);
						String textDews = makeDewElement(dew, currDew);
						String textPath = makePathElement(path, "#" + currDew);
						//log.info(textDews + " " + textPath);

						if (sent.startsWith("file:")) {
							child = createOrFindValueNodeWithAtomicity(sent, makeLocalKey(textDews, textPath, sent),
									this, thisNodeContext, RDF_URI);
						} else {
							child = createOrFindValueNodeWithAtomicity(sent, makeLocalKey(textDews, textPath, sent),
									this, thisNodeContext, Node.Types.XML_TAG_VALUE);
							//log.info("Created node: " + child.toString());
						}
						Edge edge = buildLabelledEdge(xmlNode, child, ""); 
						edge.setDataSource(this);
						processor.accept(edge);
						edges.add(edge);
						currDew++;
					}
				}
			} else if (event.getEventType() == XMLStreamReader.START_ELEMENT) {
				StartElement startElement = event.asStartElement();
				String currElem = startElement.getName().getLocalPart();
				String currPath = makePathElement(path, currElem);
				if (!xmlPolicies.getIgnoredNodes().contains(currElem)) {
					if(!ignoredTags.ignoredStartingTags.contains(startElement.toString())) { // we skip presentation tags (e.g. <b>)
						child = traverseXMLTree(reader, currElem, currDews, currPath, edges, processor);
						this.processAttributesOfNode(event, child, currDews, path, currPath, processor, edges);
						Edge edge = buildLabelledEdge(xmlNode, child, "");
						processor.accept(edge);
						edges.add(edge);
						currDew++;
					}
				} else {
					reader.nextEvent();
				}
			} else if (event.getEventType() == XMLStreamReader.END_ELEMENT) {
				if (thisElementStartsNoExtractZone) {// this element also ends it
					// log.info("Exiting noExtractZone on path " + path);
					noExtractionInterval = false;
				}
				if(!ignoredTags.ignoredEndingTags.contains(event.asEndElement().toString())) {
					return xmlNode;
				}
			}
		}
		return xmlNode;
	}

	/**
	 * create edge attribute from the child Node.
	 * 
	 * @param dew
	 * @param processor
	 * @param edges
	 * 
	 */
	private void traverseXMLAttribute(Node parentNode, String attValue, String attName, String dew,
			int currDewAttribut, String parentPath, Consumer<Edge> processor, Set<Edge> edges, int thisNodeContext) {

		//log.info("XML attribute " + attName + "=" + attValue + " of node " +		parentNode.getLabel());
		String textDewsAttNode = dew + "#" + currDewAttribut;
		String textPathAttNode = makePathElement(parentNode.getLabel(), "#" + attName);
		//log.info("Own path is: " + textPathAttNode + " dewey is: " + textDewsAttNode); 
		Node attrNode = createOrFindValueNodeWithAtomicity(attValue, makeLocalKey(textDewsAttNode, textPathAttNode, ""), this,
				thisNodeContext, Node.Types.XML_TAG_VALUE);
		Edge edge = buildLabelledEdge(parentNode, attrNode, attName);
		processor.accept(edge);
		edges.add(edge);
		currDewAttribut++;
		
		this.recordSummaryNode(attrNode, parentPath + ".#" + attName);
	}

	private void processAttributesOfNode(XMLEvent event, Node node, String currDews, String path, String currentPath, Consumer<Edge> processor, Set<Edge> edges) {
		// dewPrefix = "1"
		// dew = 1
		// path = top.getLabel
		int thisNodeContext;
		if (noExtractionInterval) { // we are in a no-extraction interval
			thisNodeContext = PolicyDrivenExtractor.noExtractContext();
		} else { // outside of a no-extraction interval, we record paths in the context map
			Integer pathNo = contextMap.get(path);
			if (pathNo == null) {
				pathNo = contextMap.size();
				contextMap.put(path, pathNo);
				revContextMap.put(pathNo, path);
				//log.info("Path " + path + " is " + pathNo);
			}
			thisNodeContext = pathNo.intValue();
		}

		StartElement startElement = event.asStartElement();
		Iterator it = startElement.getAttributes();
		int currDewAttr = 1;
		if(currDews == null) {
			currDews = makeDewElement("1", 1);
		}
		while (it.hasNext()) {
			Attribute attribute = (Attribute) it.next();
			traverseXMLAttribute(node, attribute.getValue(), attribute.getName().toString(), currDews, currDewAttr, currentPath, processor, edges, thisNodeContext);
			currDewAttr = currDewAttr + 2;
		}
	}


	public void postprocess(Graph graph) {
		// noop
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql.schema;

import static fr.inria.cedar.connectionlens.sql.RelationalStructure.executeUpdate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.inria.cedar.connectionlens.graph.Graph;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import fr.inria.cedar.connectionlens.graph.Graph.GraphSession;
import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * A Graph session to support concurrent modification on a relational graph.
 */
public abstract class RelationalGraphSession implements GraphSession {

	public static final Logger log = Logger.getLogger(RelationalGraphSession.class);

	static String insertNodeString;

	static String insertEdgeString;

	/** The edge buffer. */
	final Set<Edge> edgeBuffer = Sets.newConcurrentHashSet();

	/** The node buffer. */
	final Set<Node> nodeBuffer = Sets.newConcurrentHashSet();

	/** The virtual graph. */
	final RelationalGraph graph;

	/**
	 * An array of function to apply to a newly registered node label to build its
	 * signature
	 */
	final Function<String, int[]> signatureFunctions;

	private PreparedStatement updateRepresentative;

	/**
	 * A boolean specifying if we are dealin with an abstracted graph
	 */
	protected Graph.GraphType typeOfGraph;

	/**
	 * The name of the tables where information is stored in the database
	 */
	protected final String nodesTableName;
	protected final String edgesTableName;
	protected final String sameAsEdgesTableName;
	protected final String specificityTableName;
	protected final String weakSameAsEdgesTableName;
	protected final String disambiguatedTableName;

	
	protected StatisticsCollector graphStats;
	protected StatisticsCollector extractStats;
	protected StatisticsCollector simStats;

	protected String tempDir = Config.getInstance().getStringProperty("temp_dir");

	/**
	 * Instantiates a new relational graph session.
	 *
	 * @param g the graph
	 * @param f the signature functions
	 */
	protected RelationalGraphSession(RelationalGraph g, Function<String, int[]> f, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
		this.graph = g;
		this.signatureFunctions = f;
		if (this.graph.isOriginal()) {
			this.nodesTableName = SchemaTableNames.ORIGINAL_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME;
			this.sameAsEdgesTableName = SchemaTableNames.ORIGINAL_SAME_AS_EDGES_TABLE_NAME;
			this.weakSameAsEdgesTableName = SchemaTableNames.ORIGINAL_WEAK_SAME_AS_TABLE_NAME;
			this.specificityTableName = SchemaTableNames.ORIGINAL_SPECIFICITY_TABLE_NAME;
			this.disambiguatedTableName = SchemaTableNames.ORIGINAL_DISAMBIGUATED_TABLE_NAME;
		} else {
			// abstract graph for GUI
			this.nodesTableName = SchemaTableNames.ABSTRACT_NODES_TABLE_NAME;
			this.edgesTableName = SchemaTableNames.ABSTRACT_EDGES_TABLE_NAME;
			this.sameAsEdgesTableName = null;
			this.weakSameAsEdgesTableName = null;
			this.specificityTableName = SchemaTableNames.ABSTRACT_SPECIFICITY_TABLE_NAME;
			this.disambiguatedTableName = null;
		}

		// IM, 14/6/2020: with PER_GRAPH value node creation, we should not be using these statements anyway
		// (id, ds, type, label, normalabel, labelprefix, representative, signature)
		insertNodeString = "INSERT INTO " + nodesTableName + " VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
				+ "ON CONFLICT ON CONSTRAINT " + nodesTableName
				+ "_pkey DO UPDATE SET representative = excluded.representative";

		insertEdgeString = "INSERT INTO " + edgesTableName + " VALUES (?, ?, ?, ?, ?, ?, ?,?) "
				+ "ON CONFLICT ON CONSTRAINT " + edgesTableName + "_pkey DO NOTHING ";	

		String updateRepresentative = "UPDATE " + nodesTableName + " SET representative=?  WHERE id=?";
		this.updateRepresentative = graph.getPreparedStatement(updateRepresentative);

		String updateTypeNode = "UPDATE " + nodesTableName + " SET nodeType=?  WHERE id=?";

		this.graphStats = graphStats;
		this.extractStats = extractStats;
		this.simStats = simStats;
		
		log.setLevel(Level.INFO);
	}

	/**
	 * 
	 * @param g
	 */
	protected RelationalGraphSession(RelationalGraph g, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
		this(g, s -> new int[0], graphStats, extractStats, simStats); 
	}

	/**
	 * Prepares a statement for the given node to be stored in the underlying
	 * database.
	 *
	 * @param node              the node to serialize
	 * @param preparedStatement to insert a node
	 * @return the preparedStatement representation of the input node
	 */
	protected PreparedStatement prepareNode(PreparedStatement preparedStatement, Node node) {
		try {
			Integer nid = (Integer) node.getId().value();
			Integer rid = (Integer) node.getRepresentative().getId().value();
			preparedStatement.setBigDecimal(1, new BigDecimal(nid));
			preparedStatement.setInt(2, node.getDataSource().getID());
			preparedStatement.setInt(3, node.getType().ordinal());
			String nodeLabel = Strings.nullToEmpty(node.getLabel());
			preparedStatement.setString(4, nodeLabel);
			String normaLabel = node.getNormalizedLabel();
			String labelPrefix = node.getLabelPrefix();
			preparedStatement.setString(5, normaLabel);
			preparedStatement.setString(6, labelPrefix);
			preparedStatement.setBigDecimal(7, new BigDecimal(rid));
			preparedStatement.setArray(8, graph.createArray("int4", signatureFunctions, node));
			graph.setOnDisk(node.getId());
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return preparedStatement;
	}
	
	/**
	 * Prepares a statement for the given edge to be stored in the underlying
	 * database.
	 *
	 * @param edge              the edge to serialize
	 * @param preparedStatement to insert an edge
	 * @return the preparedStatement representation of the input edge
	 */
	PreparedStatement prepareEdge(PreparedStatement preparedStatement, Edge edge) {
		try {
			Integer eid = (Integer) edge.getId().value();
			Integer sid = (Integer) edge.getSourceNode().getId().value();
			Integer tid = (Integer) edge.getTargetNode().getId().value();
			preparedStatement.setBigDecimal(1, new BigDecimal(eid));
			preparedStatement.setInt(2, edge.getDataSource().getID());
			preparedStatement.setInt(3, edge.getType().ordinal());
			preparedStatement.setBigDecimal(4, new BigDecimal(sid));
			preparedStatement.setBigDecimal(5, new BigDecimal(tid));
			preparedStatement.setString(6, Strings.nullToEmpty(edge.getLabel()));
			preparedStatement.setDouble(7, edge.confidence());
			preparedStatement.setArray(8, graph.createArray("int4", signatureFunctions, edge));
			return preparedStatement;
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}


	/**
	 */
	public void spillDisambiguatedTable() {
		HashMap<ItemID.NodeID, String> disambiguatedEntities = new HashMap<> ();
		disambiguatedEntities = graph.disambiguatedEntitiesNode;
		String disambiguatedCopyFile = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "disambiguatedEntities.txt").toString();
		char separator = '\t';
		try {
			OutputStream os = new FileOutputStream(new File(disambiguatedCopyFile));
			for (ItemID.NodeID nodeID:disambiguatedEntities.keySet()) {
				os.write(nodeID.value().toString().getBytes());
				os.write(separator);
				os.write(disambiguatedEntities.get(nodeID).getBytes());
				os.write('\n');
			}
			os.close();
			graph.getCopyManager().copyIn("COPY " + disambiguatedTableName + " FROM STDIN ",  new FileReader(disambiguatedCopyFile));
		}
		catch(Exception e) {
			throw new IllegalStateException(e);
		}
	}
			
	/**
	 * Updates the representative for the given target node.
	 *
	 * @param targetNode the target node
	 * @param newRep     the new representative
	 * @return true, if the result was changed as a result to the update.
	 */
	private boolean updateRepresentative(Node targetNode, Node newRep) {
		//log.info(this.getClass().getSimpleName() + " updateRepresentative of " +
		// targetNode.getType() + " " + targetNode.getLabel() + " " +
		// targetNode.shortID()
		// + " to " + //newRep.getType() + " " + newRep.getLabel() + " " +
		// newRep.getId());
		try {
			Integer rid = (Integer) newRep.getId().value();
			Integer tid = (Integer) targetNode.getId().value();
			updateRepresentative.setBigDecimal(1, new BigDecimal(rid));
			updateRepresentative.setBigDecimal(2, new BigDecimal(tid));
			//log.info("SQL UPDATE: " + updateRepresentative.toString());
			return executeUpdate(updateRepresentative, false);
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public boolean hasProcessed(Node n) {
		return nodeBuffer.contains(n);
	}

	@Override
	public boolean hasProcessed(Edge e) {
		return edgeBuffer.contains(e);
		//return edgeBuffer.stream ().anyMatch (edge->edge.equals (e));
	}



	/**
	 * A Session performing updates in a sequential manner, i.e. without delay.
	 * TODO it is not gathering any statistics.
	 */
	public final static class SequentialSession extends RelationalGraphSession {

		/**
		 * Instantiates a new sequential session.
		 * 
		 * @param g     the graph
		 * @param f     the similarity function
		 */

		protected SequentialSession(RelationalGraph g, Function<String, int[]> f, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, f, graphStats, extractStats,simStats); 
		}
		protected SequentialSession(RelationalGraph g, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, graphStats, extractStats, simStats); 
		}

		@Override
		public boolean addEdge(Edge edge, Consumer<Edge> processor) {
			// @Tayeb: same a spillsameAs to avoid adding weak-same as in both direction.
			if (edge.confidence() < 1.0 && edge.isSameAs()) {
				if (edge.getSourceNode().getDataSource().getID() < edge.getTargetNode().getDataSource().getID()) {
					return updateWeakSameAs(edge);
				}
				if (edge.getSourceNode().getDataSource().getID() == edge.getTargetNode().getDataSource().getID()) {
					if (edge.getSourceNode().getId().compareTo(edge.getTargetNode().getId()) > 0) {
						return false;
					} else {
						return updateSameAs(edge);
					}
				}
			} else if (edge.isSameAs()) {
				return updateSameAs(edge);
			}
			synchronized(edgeBuffer) {
				PreparedStatement insertEdge = graph.getPreparedStatement(insertEdgeString);
				boolean result = executeUpdate(prepareEdge(insertEdge, edge), false);
				if(result) {
					processor.accept(edge);
					graph.outgoingCache.invalidate(edge.getSourceNode());
					graph.incomingCache.invalidate(edge.getTargetNode());
					edgeBuffer.add(edge);
				}
				return result;
			}
		}

		@Override
		public boolean addNode(Node node, Consumer<Node> processor) {
			synchronized(nodeBuffer) {
				PreparedStatement insertNode = graph.getPreparedStatement(insertNodeString);
				boolean result = executeUpdate(prepareNode(insertNode, node), false);
				if(result) {
					processor.accept(node);
					nodeBuffer.add(node);
				}
				return result;
			}
		}

		/**
		 * Updates the weak same-as table with the given edge.
		 *
		 * @param edge the weak same-as edge
		 * @return true, if the result was changed as a result to the update.
		 */
		private boolean updateWeakSameAs(Edge edge) {
			Node source = edge.getSourceNode();
			Node target = edge.getTargetNode();

			String weakSameAsString = " INSERT INTO " + weakSameAsEdgesTableName
					+ "  VALUES (?, ?, ?, ?, ?, ?) ON CONFLICT ON " + " CONSTRAINT " + weakSameAsEdgesTableName
					+ "_pkey DO UPDATE SET confidence=? " + " WHERE " + weakSameAsEdgesTableName + ".node1=? AND "
					+ weakSameAsEdgesTableName + ".node2=?";

			PreparedStatement preparedStatement = graph.getPreparedStatement(weakSameAsString);
			try {
				Integer eid = (Integer) edge.getId().value();
				Integer sid = (Integer) source.getId().value();
				Integer tid = (Integer) target.getId().value();
				preparedStatement.setBigDecimal(1, new BigDecimal(eid));
				preparedStatement.setBigDecimal(2, new BigDecimal(sid));
				preparedStatement.setInt(3, source.getDataSource().getID());
				preparedStatement.setBigDecimal(4, new BigDecimal(tid));
				preparedStatement.setInt(5, target.getDataSource().getID());
				preparedStatement.setDouble(6, edge.confidence());
				preparedStatement.setDouble(7, edge.confidence());
				preparedStatement.setBigDecimal(8, new BigDecimal(sid));
				preparedStatement.setBigDecimal(9, new BigDecimal(tid));
				return executeUpdate(preparedStatement, false);
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

		/**
		 * Updates the tables with the given same-as edge.
		 *
		 * @param e the same-as edge
		 * @return true, if the result was changed as a result to the update.
		 * @Tayeb: modified the function to add representatives in a deterministic way:
		 *         the lower one.
		 */
		private boolean updateSameAs(Edge e) {
			synchronized(graph) {
				Node n1 = e.getSourceNode();
				Node n2 = e.getTargetNode();

				if (n1.equals(n2)) {
					return false;
				}
				if (e.confidence() < 1.) {
					return updateWeakSameAs(e);
				}

				boolean result;
				Node rep1 = n1.getRepresentative();
				Node rep2 = n2.getRepresentative();
				if (rep1.equals(rep2)) {
					return false;
				}

				if (rep1.getId().compareTo(rep2.getId()) == -1) {
					result = super.updateRepresentative(n2, rep1);
					if (!n2.equals(rep2)) {
						for (Edge e2 : graph.getSameAs(n2, 1.0)) {
							Node n3 = e2.getTargetNode();
							result &= super.updateRepresentative(n3, rep1);
							n3.updateRepresentative(rep1);
						}
					}
					n2.updateRepresentative(rep1);
				} else {
					result = super.updateRepresentative(n1, rep2);
					n1.updateRepresentative(rep2);
				}
				return result;
			}
		}

		@Override
		public void commit() {
			if (Config.getInstance().getBooleanProperty("storage_use_materialized_strong_same_as_edges")) {
				graph.executeUpdate("REFRESH MATERIALIZED VIEW " + sameAsEdgesTableName);
			}
			if (!edgeBuffer.isEmpty() || !nodeBuffer.isEmpty()) {
				//computeInitialSpecificities();
				edgeBuffer.clear();
				nodeBuffer.clear();
			}
		}
	}

	/**
	 * A session pooling all updates in-memory, and spilling the changes to database
	 * update closing the session.
	 */
	public static class BatchSession extends RelationalGraphSession {
		private final String insertWncString;
		private final String insertSncString;
		private final String insertSpecificitiesString;
	
		/** The same-as edge buffer. */
		final Set<Edge> sameAsBuffer = Sets.newConcurrentHashSet();

		/** The specificity buffer. */
		final Set<Specificity> specBuffer = Sets.newConcurrentHashSet();



		/**
		 * Instantiates a new batch session.
		 * 
		 * @param g     the graph
		 * @param f     the signature functions
		 */
		protected BatchSession(RelationalGraph g, Function<String, int[]> f, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, f, graphStats, extractStats, simStats); 
			this.insertWncString = "WITH b2 (id, n1, d1, n2, d2, c) AS (VALUES (?, ?, ?, ?, ?, ?)) " + "INSERT INTO "
					+ weakSameAsEdgesTableName + " SELECT DISTINCT id, n1, d1, n2, d2, c FROM b2 "
					+ "ON CONFLICT ON CONSTRAINT " + weakSameAsEdgesTableName + "_pkey DO "
					+ "UPDATE SET confidence = excluded.confidence";
			this.insertSncString = "WITH b1 (d, r, t, i) AS (VALUES (?, ?, ?, ?),(?, ?, ?, ?)) " + "UPDATE " + nodesTableName
					+ " SET representative = b1.r " + "FROM b1 WHERE "+nodesTableName+".id = b1.i";
			this.insertSpecificitiesString = "WITH b3 (r, l, i, o) AS (VALUES (?, ?, ?, ?)) INSERT INTO "
					+ specificityTableName
					+ "(representative, edge_label, nin, nout) SELECT DISTINCT r, l, i, o FROM b3 "
					+ "ON CONFLICT ON CONSTRAINT " + specificityTableName + "_pkey DO "
					+ "UPDATE SET nin=excluded.nin, nout=excluded.nout";
		}

		protected BatchSession(RelationalGraph g, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, graphStats, extractStats, simStats); 
			this.insertWncString = "WITH b2 (id, n1, d1, n2, d2, c) AS (VALUES (?, ?, ?, ?, ?, ?)) " + "INSERT INTO "
					+ weakSameAsEdgesTableName + " SELECT DISTINCT id, n1, d1, n2, d2, c FROM b2 "
					+ "ON CONFLICT ON CONSTRAINT " + weakSameAsEdgesTableName + "_pkey DO "
					+ "UPDATE SET confidence = excluded.confidence";
			this.insertSncString = "WITH b1 (d, r, t, i) AS (VALUES (?, ?, ?, ?),(?, ?, ?, ?)) " + "UPDATE " + nodesTableName
					+ " SET representative = b1.r " + "FROM b1 WHERE "+nodesTableName+".id = b1.i";
			this.insertSpecificitiesString = "WITH b3 (r, l, i, o) AS (VALUES (?, ?, ?, ?)) INSERT INTO "
					+ specificityTableName
					+ "(representative, edge_label, nin, nout) SELECT DISTINCT r, l, i, o FROM b3 "
					+ "ON CONFLICT ON CONSTRAINT " + specificityTableName + "_pkey DO "
					+ "UPDATE SET nin=excluded.nin, nout=excluded.nout";
			this.graphStats = graphStats;
			this.extractStats = extractStats; 
			this.simStats = simStats; 
		}

		
		@Override
		public boolean addEdge(Edge e, Consumer<Edge> processor) {
			if (e.isSameAs()) {
				return updateSameAs(e);
			}
			synchronized(edgeBuffer) {
				edgeBuffer.add(e);
				processor.accept(e);
				graph.outgoingCache.invalidate(e.getSourceNode());
				graph.incomingCache.invalidate(e.getTargetNode());
				return true;
			}

		}

		@Override
		public boolean addNode(Node n, Consumer<Node> processor) {
			synchronized(nodeBuffer) {
				//log.info(this.getClass().getSimpleName() + " addNode");
				nodeBuffer.add(n);
				//log.info(this.getClass().getSimpleName() + " added");
				processor.accept(n);
				//log.info(this.getClass().getSimpleName() + " accepted");
				
				return true;
			}
		}

		/**
		 * Updates the table to accounting for the given same-as edge..
		 *
		 * @param e the same-as edge.
		 * @return true, if the result was changed as a result to the update.
		 *
		 * @Tayeb: I modified the function to add representatives in a deterministic
		 *         way: the lower one.
		 */
		protected boolean updateSameAs(Edge e) {
			synchronized(sameAsBuffer) {
				Node n1 = e.getSourceNode();
				Node n2 = e.getTargetNode();
				if (n1.equals(n2) || sameAsBuffer.stream ().anyMatch (edge->edge.equals (e))) {
					return false;
				}
				boolean result = sameAsBuffer.add(e);
				if (e.confidence() < 1.) {
					return result;
				}
				Node rep1 = n1.getRepresentative();
				Node rep2 = n2.getRepresentative();
				if (rep1.equals(rep2)) {
					return false;
				}
				if (rep1.getId().compareTo(rep2.getId()) == -1) {
					result = super.updateRepresentative(n2, rep1);
					if (!n2.equals(rep2)) {
						for (Edge e2 : graph.getSameAs(n2, 1.0)) {
							e2.getTargetNode().updateRepresentative(rep1);	
						}
					}
					n2.updateRepresentative(rep1);
				} else {
					result = super.updateRepresentative(n1, rep2);
					n1.updateRepresentative(rep2);
				}

				return result;
			}
		}

		/**
		 * Returns prepared statement of the given edge as a tuple to update the strong
		 * or weak same-as table.
		 *
		 * @param edge the edge
		 * @return the prepared statement version of the edge.
		 */
		protected PreparedStatement prepareWeakSameAs(Edge edge) throws SQLException {
			PreparedStatement result = graph.getPreparedStatement(insertWncString);
			Node source = edge.getSourceNode();
			Node target = edge.getTargetNode();
			Node rep = source.getRepresentative();

			if (rep.equals(source)) {
				rep = target.getRepresentative();
			}
			Integer eid = (Integer) edge.getId().value();
			Integer sid = (Integer) source.getId().value();
			Integer tid = (Integer) target.getId().value();
			result.setBigDecimal(1, new BigDecimal(eid));
			result.setBigDecimal(2, new BigDecimal(sid));
			result.setInt(3, source.getDataSource().getID());
			result.setBigDecimal(4, new BigDecimal(tid));
			result.setInt(5, target.getDataSource().getID());
			result.setDouble(6, edge.confidence());
			return result;
		}

		/**
		 * Returns prepared statement of the given edge as a tuple to update the strong
		 * or weak same-as table.
		 *
		 * @param edge the edge
		 * @return the prepared statement version of the edge.
		 */
		protected PreparedStatement prepareStrongSameAs(Edge edge) throws SQLException {
			Node source = edge.getSourceNode();
			Node target = edge.getTargetNode();
			Node rep = source.getRepresentative();

			if (rep.equals(source)) {
				rep = target.getRepresentative();
			}

			Integer rid = (Integer) rep.getId().value();
			Integer sid = (Integer) source.getId().value();
			Integer tid = (Integer) target.getId().value();
			PreparedStatement result = graph.getPreparedStatement(insertSncString);
			result.setInt(1, rep.getDataSource().getID());
			result.setBigDecimal(2, new BigDecimal(rid));
			result.setInt(3, rep.getType().ordinal());
			result.setBigDecimal(4, new BigDecimal(sid));

			result.setInt(5, rep.getDataSource().getID());
			result.setBigDecimal(6, new BigDecimal(rid));
			result.setInt(7, rep.getType().ordinal());
			result.setBigDecimal(8, new BigDecimal(tid));

			return result;
		}

		/**
		 * Serializes the given specificity as a tuple to update the specificity table.
		 *
		 * @param specificity the specificity to serialize
		 * @return the preparedStatement representation of the input specificity
		 */
		protected PreparedStatement prepareSpecificity(Specificity specificity) throws SQLException {
			PreparedStatement result = graph.getPreparedStatement(insertSpecificitiesString);
			Node source = specificity.node();
			Integer sid = (Integer) source.getId().value();
			result.setBigDecimal(1, new BigDecimal(sid));
			result.setString(2, specificity.label());
			result.setInt(3, specificity.in());
			result.setInt(4, specificity.out());
			return result;
		}

		/**
		 * Spill the edges to the database.
		 */
		void spillEdges() {
			//log.info("Edge spill");
			graphStats.resume(StatisticsCollector.total()); 
			
			PreparedStatement insertEdges = graph.getPreparedStatement(insertEdgeString);
			try {
				for (Edge edge : edgeBuffer) {
					insertEdges = prepareEdge(insertEdges, edge);
					insertEdges.addBatch();
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
			executeUpdate(insertEdges, true);
			graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.STOR_T);
			//log.info("Edges spilled");
		}

		/**
		 * Spill the nodes to the database.
		 * @throws IOException 
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		void spillNodes() {	
			//log.info("Node spill ");
			graphStats.resume(StatisticsCollector.total());
			PreparedStatement insertNodes = graph.getPreparedStatement(insertNodeString);
			try {
				for (Node node : nodeBuffer) {
					insertNodes = prepareNode(insertNodes, node);
					insertNodes.addBatch();
//					log.info("Spilling: " + node);
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}	
			executeUpdate(insertNodes, true);
			graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.STOR_T);
			//log.info("Nodes spilled");
		}

		/**
		 * Spill the same-as edges to the database.
		 */
		void spillSameAs() {
			//log.info("Same-as edge spill");
			simStats.resume(StatisticsCollector.total());
			PreparedStatement insertSnc = null, insertWnc = null;
			for (Edge edge : sameAsBuffer) {
				try {
					if (edge.confidence() < 1.0) {
						// Avoid spilling n1 weak sameAs n2 *and* n2 weak sameAs n1:
						// log.info("Considering weak sameAs edge: " + edge.debugEdge());
						// log.info("Data sources: " +
						// edge.getSourceNode().getDataSource().getID() + " and " +
						// edge.getTargetNode().getDataSource().getID());
						boolean spillEdge = false;
						if (edge.getSourceNode().getDataSource().getID() < // ds1 smaller
								edge.getTargetNode().getDataSource().getID()) {
							spillEdge = true;
						} else { // ds1 == ds2, n1.id smaller
							if (edge.getSourceNode().getDataSource().getID() == edge.getTargetNode().getDataSource()
									.getID()) {
								// log.info("Same ds, source: " + edge.getSourceNode().getId()+ "
								// target: " + edge.getTargetNode().getId() +
								// " comparison: " +
								// edge.getSourceNode().getId().compareTo(edge.getTargetNode().getId()));
								if (edge.getSourceNode().getId().compareTo(edge.getTargetNode().getId()) < 0) {
									spillEdge = true;
								}
							}
						}

						if(spillEdge) {
							insertWnc = prepareWeakSameAs(edge);
							insertWnc.addBatch();
						}
					} else { // strong sameAs
						// log.info("Spilling strong sameAs: " +
						// edge.getSourceNode().getLabel() + " " + edge.debugEdge() + " " +
						// edge.getTargetNode().getLabel());
						insertSnc = prepareStrongSameAs(edge);
						insertSnc.addBatch();
					}
				} catch (SQLException e) {
					throw new IllegalStateException(e);
				}
			}
			if (insertSnc != null) {
				executeUpdate(insertSnc, true);
			}
			if (insertWnc != null) {
				executeUpdate(insertWnc, true);
			}
			simStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.SIM_LOAD_T);
			//log.info("Same-as edges spilled");
		}

		/**
		 * Spill the specificities to the database.
		 */
		void spillSpecificities() {
			//log.info("Specificity spill");
			graphStats.resume(StatisticsCollector.total());
			try {
				PreparedStatement insertSpecificities = null;
				for (Specificity specificity : specBuffer) {
					insertSpecificities = prepareSpecificity(specificity);
					insertSpecificities.addBatch();
				}
				if (insertSpecificities != null) {
					executeUpdate(insertSpecificities, true);
				}
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
			graph.inDegreeCache.invalidateAll(
					specBuffer.stream().map(s -> Pair.of(s.node(), s.label())).collect(Collectors.toList()));
			graph.outDegreeCache.invalidateAll(
					specBuffer.stream().map(s -> Pair.of(s.node(), s.label())).collect(Collectors.toList()));
			graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.SPEC_T);
		}

		@Override
		public void commit() {
			//log.info("Commit");
			boolean recomputeSpecs;
			synchronized(edgeBuffer) {
				recomputeSpecs = !nodeBuffer.isEmpty() || !edgeBuffer.isEmpty();
				if (!edgeBuffer.isEmpty()) {
					spillEdges();
					edgeBuffer.clear();
				}
			}
			synchronized(nodeBuffer) {
				if (!nodeBuffer.isEmpty()) {
					spillNodes();
					nodeBuffer.clear();
				}
			}

			synchronized (graph.getDisambiguatedEntitiesNode()){
				if(!graph.getDisambiguatedEntitiesNode().isEmpty()
						&& Config.getInstance().getBooleanProperty("entity_disambiguation")){
					spillDisambiguatedTable();
					graph.getDisambiguatedEntitiesNode().clear();
				}
			}

			synchronized(sameAsBuffer) {
				if (!sameAsBuffer.isEmpty()) {
					spillSameAs();
					sameAsBuffer.clear();
					if (Config.getInstance().getBooleanProperty("storage_use_materialized_strong_same_as_edges")) {
						graph.executeUpdate("REFRESH MATERIALIZED VIEW " + sameAsEdgesTableName);
					}
				}
			}
            if(Config.getInstance().getBooleanProperty("compare_nodes") ) {
                graph.createNodeLabelPrefixIndex();
            }
		}
	}

	/**
	 * A Session pooling updates in memory and spilling to the database every time a
	 * predefined number of update is in the pool.
	 */
	public static class BufferedSession extends BatchSession {

		/** The batch size. */
		final int batchSize;

		/**
		 * Instantiates a new buffered session.
		 * 
		 * @param g     the graph
		 * @param f     the signature functions
		 * @param size  the batch size
		 */
		protected BufferedSession(RelationalGraph g, Function<String, int[]> f, int size, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, f, graphStats, extractStats, simStats);
			this.batchSize = size;
		}
		protected BufferedSession(RelationalGraph g, int size, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, graphStats, extractStats, simStats);
			this.batchSize = size;
		}

		@Override
		public boolean addEdge(Edge e, Consumer<Edge> processor) {
			// log.info(this.getClass().getSimpleName() + " adding " +
			// (e.isSameAs()?"sameAs ":"") + e.getType() + " " + e);

			if (e.isSameAs()) {
				return updateSameAs(e);
			}
			synchronized(edgeBuffer) {
				if (edgeBuffer.size() >= batchSize) {
					spillEdges();
					edgeBuffer.clear();
					graph.outgoingCache.invalidate(e.getSourceNode());
					graph.incomingCache.invalidate(e.getTargetNode());
				}
				return super.addEdge(e, processor);
			}
		}

		@Override
		public boolean addNode(Node n, Consumer<Node> processor) {
			synchronized(nodeBuffer) {
				if (nodeBuffer.size() >= batchSize) {
					spillNodes();
					nodeBuffer.clear();
				}
				return super.addNode(n, processor);
			}
		}

		/**
		 * Update the graph with the given same-as edge.
		 *
		 * @param e the same-as edge.
		 * @return true, if the result was changed as a result to the update.
		 */
		@Override
		protected boolean updateSameAs(Edge e) {
			//log.info(this.getClass().getSimpleName() + " UPDATE SAME AS " + e);
			synchronized(sameAsBuffer) {
				if (sameAsBuffer.size() >= batchSize) {
					spillSameAs();
					sameAsBuffer.clear();
				}
				return super.updateSameAs(e);
			}
		}
	}
	public static class CopyBatchSession extends BufferedSession {
		String tempFileForCopy;
		protected CopyBatchSession(RelationalGraph g, Function<String, int[]> f, int size, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, f, size, graphStats, extractStats, simStats);
			tempFileForCopy = Paths.get(tempDir, "a.txt").toString();
			
//			log.info("Relational batch session uses COPY");
		}
		protected CopyBatchSession(RelationalGraph g, int size, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats) {
			super(g, size, graphStats, extractStats, simStats);
			tempFileForCopy = Paths.get(tempDir, "a.txt").toString();
			
			log.info("Relational batch session uses COPY");
		}
		

		/**
		 * Spill the edges to the database using COPY. Edges need to be all unique.
		 */
		void spillEdges() {
//			log.info("Edge spill");
			graphStats.resume(StatisticsCollector.total());
			OutputStream os = null; 
			try {
				os = new FileOutputStream(new File(tempFileForCopy));
				for (Edge edge : edgeBuffer) {
					this.printEdgeToFile(edge, os);
				}
				os.close();
				graph.getCopyManager().copyIn("COPY " + edgesTableName + " FROM STDIN ",  new FileReader(tempFileForCopy));
			}
			catch(Exception e) {
				if (os != null) {
					try{
						os.close();
					}
					catch(Exception e2) {
						log.info("Closing temporary file failed: " + e2.toString());
					}
				}
				throw new IllegalStateException(e);
			}
			graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.STOR_T);
//			log.info("Edges spilled");
		}
		
		/**
		 * Spill the nodes to the database using COPY and controlling unique node spill
		 */
		void spillNodes() {
//			log.info("Node spill");
			graphStats.resume(StatisticsCollector.total());
			OutputStream os = null; 
			try {
				os = new FileOutputStream(new File(tempFileForCopy));
				for (Node node : nodeBuffer) {
					if (!graph.isOnDisk(node.getId())) {
						this.printNodeToWriter(node, os);
						graph.setOnDisk(node.getId()); 
					}
				}
				os.close(); 
				graph.getCopyManager().copyIn("COPY " + nodesTableName + " FROM STDIN ",  new FileReader(tempFileForCopy));
			}
			catch(Exception e) {
				try {
					OutputStream lsos = Runtime.getRuntime().exec("ls -l " + tempFileForCopy).getOutputStream();
					log.info(lsos.toString()); 
					Runtime.getRuntime().exec("cp " + tempFileForCopy + " " + Paths.get("/tmp", "saved.txt").toString());
				} catch (IOException e1) {
					log.info("Failed to copy file: " + e1.toString());
				}
				throw new IllegalStateException(e); 
			}	
			graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.STOR_T);
//			log.info("Nodes spilled");
		}

		/** Writes 1 line in a temp file describing 1 node for subsequent COPY */
		private void printNodeToWriter(Node node, OutputStream os) throws IOException {
			char separator = '\t';
			Integer nid = (Integer) node.getId().value();
			os.write(nid.toString().getBytes()); 
			os.write(separator);
			os.write(Integer.toString(node.getDataSource().getID()).getBytes());
			os.write(separator);
			os.write(Integer.toString(node.getType().ordinal()).getBytes());
			os.write(separator);
			os.write((Strings.nullToEmpty(replaceLineSeparator(node.getLabel()))).getBytes());
//			os.write((Strings.nullToEmpty(escapeLineSeparator(node.getLabel()))).getBytes()); // Nelly 21 sept 2022: we want to write labels exactly as they are (mainly for RDF data, see issue 165)
			os.write(separator);
			os.write(replaceLineSeparator(replaceLineSeparator(node.getNormalizedLabel())).getBytes());
			os.write(separator);
			os.write(replaceLineSeparator(replaceLineSeparator(node.getLabelPrefix())).getBytes());
			os.write(separator);
			Integer rid = (Integer) node.getRepresentative().getId().value();
			os.write(rid.toString().getBytes());
			os.write(separator);
			os.write('{');
			int[] ints = signatureFunctions.apply(node.getLabel());
			for (int i =  0; i < ints.length; i ++) {
				os.write((Integer.toString(ints[i]).getBytes()));
				if (i<ints.length-1) {
					os.write(',');
				}
			}
			os.write('}');
			os.write('\n');
		}
		/** Writes 1 line in a temp file describing 1 edge for subsequent COPY */	
		private void printEdgeToFile(Edge edge, OutputStream os) {
			char separator = '\t';
			//log.info("Printing to file edge: " + edge.debugEdge() + "\n" + edge.toString());
			try {
				os.write(edge.getId().value().toString().getBytes());
				os.write(separator);
				os.write(Integer.toString(edge.getDataSource().getID()).getBytes());
				os.write(separator);
				os.write(Integer.toString(edge.getType().ordinal()).getBytes());
				os.write(separator);
				os.write(edge.getSourceNode().getId().value().toString().getBytes());
				os.write(separator);
				os.write(edge.getTargetNode().getId().value().toString().getBytes());
				os.write(separator);
				os.write(Strings.nullToEmpty(replaceLineSeparator(edge.getLabel())).getBytes());
				os.write(separator);
				os.write(Strings.nullToEmpty(String.valueOf(edge.confidence())).getBytes());
				os.write(separator);
				os.write('{');
				int[] ints = signatureFunctions.apply(edge.getLabel());
				for (int i = 0; i < ints.length; i ++) {
					os.write(new Integer(ints[i]).toString().getBytes());
					if (i < ints.length - 1) {
						os.write(',');
					}
				}
				os.write('}');
				os.write('\n');
			}
			catch(Exception e) {
				throw new IllegalStateException(e);
			}
		}

		/**
		 *
		 * @param value
		 * @return to avoid the problem of column with \n, \r and \t
		 */
		private String replaceLineSeparator(String value){
			return value.replaceAll("[\n\r\t]+", " ").replaceAll("\\\\", "");  
			// Simplified on Jan 15, 2021, was: 
			// 
			// return value.replaceAll("[\n\r]+", "\\\\"+System.getProperty("line.separator"))
			// .replaceAll ("\t","\\\\t")
			// .replaceAll("\\\\", "\\\\\\\\");
			// System.getProperty("line.separator") is the OS-dependent line separator  https://stackoverflow.com/questions/36796136/difference-between-system-getpropertyline-separator-and-n
		}

		// escape all the line separator - but does NOT remove them form the input value
		private String escapeLineSeparator(String value) {
			String escapedValue = value;
			escapedValue = escapedValue.replaceAll("\n", "\\\\\\\\n"); // to write \\n in the file so that PSQL does not interpret the \n
			escapedValue = escapedValue.replaceAll("\t", "\\\\\\\\t");
			escapedValue = escapedValue.replaceAll("\r", "\\\\\\\\r");
			return escapedValue;
		}
	}
}

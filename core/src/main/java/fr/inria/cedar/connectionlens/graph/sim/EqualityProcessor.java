/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameNormaLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameType;

import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class EqualityProcessor extends CachedSimilarPairProcessor {
	
	public EqualityProcessor(StatisticsCollector stats, double th) {
		super(stats, th);
	}

	@Override
	public Double apply(String a, String b) {
		//return a.equals(b) ? 1.0 : 0.0;
		return 1.0; 
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result = sameNormaLabel().and(sameType());
		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length()};
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;

import static fr.inria.cedar.connectionlens.source.DataSource.Types.*;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds.*;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.EDGES_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.LOC_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.NODES_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.ORG_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.PERS_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.SAMEAS_T;
import static java.lang.Math.min;
import static java.util.Collections.singleton;
import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.google.common.base.Joiner;
import fr.inria.cedar.connectionlens.abstraction.AbstractionsComparator;
import fr.inria.cedar.connectionlens.abstraction.Normalization;
import fr.inria.cedar.connectionlens.abstraction.ReportingToUser;
import fr.inria.cedar.connectionlens.abstraction.classification.Classification;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraphBuilding;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.CollectionGraphPrinting;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.analysis.function.Exp;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.beust.jcommander.ParameterException;
import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.extraction.BatchExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.ExtractorBatch;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Graph.GraphSession;
import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.score.AnswerSizeScore;
import fr.inria.cedar.connectionlens.score.ConnectionScore;
import fr.inria.cedar.connectionlens.score.MatchingScore;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.score.SpecificityScore;
import fr.inria.cedar.connectionlens.score.WeightedScore;
import fr.inria.cedar.connectionlens.search.Query;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.source.OfficeDataSource;
import fr.inria.cedar.connectionlens.source.PDFDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.RelationalSourceCatalog;
import fr.inria.cedar.connectionlens.sql.StorageModels;
import fr.inria.cedar.connectionlens.sql.schema.NumericIDFactory;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.StatisticsLogger;

/**
 * ConnectionLens is point of entry for running the application. An instance of
 * this class can be seen as an instance of the whole system.
 */
public class ConnectionLens implements AutoCloseable {

	/** The logger. */
	static Logger log = Logger.getLogger(ConnectionLens.class);

	/** The data source catalog. */
	private final DataSourceCatalog catalog;

	/** The graph. */
	private final Graph graph;

	/** The morpho-syntactic analyser. */
	private final MorphoSyntacticAnalyser mas;

	/** The system's configuration. */
	private final Config config = Config.getInstance();

	/** The entity extractor. */
	private final EntityExtractor extractor;

	/** The similarity pair processor. */
	private final SimilarPairProcessor pairProcessor;

	/** The statistics logger. */
	private StatisticsLogger statsLog;

	/**
	 * Statistic collectors. These are usually created in Experiments and passed
	 * here; in other cases (e.g. tests) initialized.
	 */
	private StatisticsCollector graphStats;
	private StatisticsCollector extractStats;
	private StatisticsCollector indexStats;
	private StatisticsCollector simStats;
	private StatisticsCollector queryStats;
	private StatisticsCollector abstractionStats;

	private ReportingToUser reportingToUser;

	/** The item ID factory */
	protected final Factory factory;

	private Boolean indexLater;

	/**
	 * Instantiates a new ConnectionLens instance
	 *
	 * @param extractor  the entity extractor
	 * @param analyzer   the morpho-syntactic analyser
	 * @param resetFirst true if the instance should be reset upon construction
	 */
	public ConnectionLens(EntityExtractor extractor, MorphoSyntacticAnalyser analyzer, SimilarPairProcessor pp,
			boolean resetFirst) {
		this(StatisticsLogger.mute(), extractor, analyzer, pp, resetFirst, null, null, null, null, null, null, false);
		ensureStatistics();
	}

	public ConnectionLens(StatisticsLogger sl, EntityExtractor e, MorphoSyntacticAnalyser a, SimilarPairProcessor pp,
			boolean resetFirst, StatisticsCollector graphStats, StatisticsCollector extractStats,
			StatisticsCollector indexStats, StatisticsCollector simStats, StatisticsCollector queryStats,
			StatisticsCollector abstractionStats, boolean indexLater) {

		boolean forceReset = maybeCreateDatabase();
		this.statsLog = sl;
		this.extractor = e;
		this.graphStats = graphStats;
		this.extractStats = extractStats;
		this.indexStats = indexStats;
		this.simStats = simStats;
		this.queryStats = queryStats;
		this.abstractionStats = abstractionStats;
		this.mas = a;
		this.factory = buildItemIDFactory();
		this.indexLater = indexLater;

		// we pass the three stat collectors through the catalog

		if (resetFirst || forceReset) {
//			log.info("Reset graph, index and catalog");
			// create the graph first, with a null catalog
			this.graph = buildGraph(factory, null);
			// create a new catalog
			this.catalog = new RelationalSourceCatalog(factory, extractor, graphStats, extractStats, indexStats);
			this.graph.setCatalog(this.catalog);
			this.graph.reset();
			this.graph.index().reset();
			// give the catalog the right connection, and reset the catalog
			this.catalog.init(graph, true);
		} else {
			log.info("Creating graph from pre-existing catalog");
			// create the catalog first, it may read things from the database
			this.catalog = new RelationalSourceCatalog(factory, extractor, graphStats, extractStats, indexStats);
			// create the graph using that catalog
			this.graph = buildGraph(factory, catalog);
			// give to the catalog the connection of the graph. Do not reset the catalog.
			this.catalog.init(graph, false);
		}

		this.pairProcessor = pp;

		this.graph.setIDCounter(resetFirst); // reset maxNode to 0 if reset or getTheMaxId (nodesId and EdgesId)
		log.setLevel(Level.DEBUG);
	}

	/**
	 * Instantiates a new ConnectionLens instance
	 *
	 * @param e          the entity extractor
	 * @param a          the morpho-syntactic analyser
	 * @param resetFirst true if the instance should be reset upon construction
	 */
	public ConnectionLens(StatisticsLogger sl, EntityExtractor e, MorphoSyntacticAnalyser a, SimilarPairProcessor pp,
			boolean resetFirst, StatisticsCollector graphStats, StatisticsCollector extractStats,
			StatisticsCollector indexStats, StatisticsCollector simStats, StatisticsCollector queryStats,
			StatisticsCollector abstractionStats) {
		this(sl, e, a, pp, resetFirst, graphStats, extractStats, indexStats, simStats, queryStats, abstractionStats,
				false);
//		this.treeScoring = new MatchAndConnectionScore(this.pathScoring);

	}

	/**
	 * Resets the instance, i.e. deleting the content of the graph and index.
	 */
	public void reset() {
		graph.reset();
		graph().index().reset();
		// it used to reset the catalog but that has moved away.
	}

	private Graph buildGraph(ItemID.Factory factory, DataSourceCatalog catalog) {
		return new fr.inria.cedar.connectionlens.sql.schema.RelationalGraph(factory, catalog, extractor, mas, statsLog,
				indexLater);
	}

	/**
	 * @param uri the URI of the data source
	 * @return the input URI
	 */
	public static URI maybeLoadDatabase(URI uri) {
		/*if (uri.toString().toLowerCase().endsWith(".csv")) {
			return load(uri);
		}*/
		return uri;
	}

	/**

	 * Attempts to create database the database holding the application's working
	 * information, if the database does not already exists.
	 *
	 * @return true, if successful
	 */
	public static boolean maybeCreateDatabase() {
		try (Connection c = ConnectionManager.getAdminConnection(); Statement statement = c.createStatement()) {
			statement.executeUpdate("CREATE DATABASE " + Config.getInstance().getProperty("RDBMS_DBName"));
			return true;
		} catch (SQLException e) {
			// log.info(e.getMessage());
			return false;
		}
	}

	/**
	 * Register the data source at the given URI.
	 *
	 * @param uri the URI of the data source to register.
	 * @return the registered data source
	 * @throws IllegalStateException if the given cannot be parsed to a valid URI.
	 */
	public DataSource register(String uri) {
		return register(uri, null);
	}

	/**
	 * Register the data source at the given URI.
	 *
	 * @param uri the URI of the data source to register.
	 * @return the registered data source
	 * @throws IllegalStateException if the given cannot be parsed to a valid URI.
	 */
	public DataSource register(String uri, String origURI) {
		try {
			String formatted = uri.replace("\\", "/");
			String normalized = formatted.toLowerCase();
			String originalURI = Optional.ofNullable(origURI).orElse("");
			if (normalized.startsWith("jdbc:")) {
				return register(new URI(formatted), new URI(originalURI));
			} else if (!normalized.startsWith("file:") && !normalized.contains(".njn")) {
				return register(new URI("file:" + formatted), new URI(originalURI));
			}
			return register(new URI(formatted), new URI(originalURI));
		} catch (URISyntaxException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Given a set of input URIs, it registers them all, runs comparisons if
	 * applicable, and finalizes the specificity tables (thus, the graph).
	 */
	public void completeDataSourceSetRegistration(List<URI> inputURIs) {
		List<URI> sources = Lists.newArrayList();
		inputURIs.forEach(s -> {
			sources.add(s);
		});
		List<DataSource> newSources = Lists.newLinkedList();
		register(sources, new HashMap<>()).forEach(newSources::add);
		if (Config.getInstance().getBooleanProperty("compare_nodes")) {
			processSimilarities(catalog().getDataSources(), newSources);
		}
		graph().countEdgesForSpecificity();
		graph().generateEdgeSpecificityComputeMeanStdDev(graphStats());
	}


	/**
	 * Perform the abstraction task on the CL graph produced by the registration module.
	 * This function expects that ONLY ONE DATASET IS LOADED AT A TIME!
	 * @throws Exception if something wrong happens
	 */
	public void runAbstraction(String eMax, String covMin, String simMin, String scoreCatMin, String k, String scoring, String boundary) throws Exception {

		// 0. we set the local.settings in GUI as being the main local.settings
		String newLocalSettingsPath = Paths.get(Config.getInstance().getStringProperty("tomcat_dir"), "WEB-INF", "local.settings").toString();
		Config.getInstance().override(newLocalSettingsPath);
		Config.getInstance().setProperty("E_MAX", eMax);
		Config.getInstance().setProperty("COV_MIN", covMin);
		Config.getInstance().setProperty("SIM_MIN", simMin);
		Config.getInstance().setProperty("SCORE_CAT_MIN", scoreCatMin);

		// 2. we run the abstraction
		if(scoring.equals("DESC") || scoring.equals("LEAF")) {
			scoring = scoring + "_" + k;
		}
		this.runAbstraction(scoring, boundary);
	}

	public void runAbstraction(String scoringMethod, String boundaryMethod) throws Exception {
		if (Experiment.runAbstraction) {
			DataSource ds = this.getDataSourcesAsList().get(0);

			if(this.config.getBooleanProperty("reset_abstraction")) {
				// set values
				Experiment.scoringMethod = scoringMethod;
				Experiment.boundaryMethod = boundaryMethod;
				String endAt = this.config.getStringProperty("end_at"); // endAt values are ["normalization","collections","mainCollections","classification","reporting"]
				long startTime;
				boolean stop = false;

				// clean the tables
				this.cleanTablesBeforeAbstraction();

				// start the abstraction
				System.out.println("Running abstraction with: " + scoringMethod + " - " + boundaryMethod);
				Normalization normalization = new Normalization();
				CollectionGraphBuilding collectionGraphBuilding = new CollectionGraphBuilding();
				MainCollectionsIdentification mainCollectionsIdentification = new MainCollectionsIdentification();
				Classification classification = new Classification();
				this.reportingToUser = new ReportingToUser(); // this one is a class variable to get it in the JSP

				if(!Config.getInstance().getBooleanProperty("start_from_collection_graph")) {
					// 1. Normalization
					startTime = System.currentTimeMillis();
					normalization = new Normalization(this, ds);
					normalization.run();
					GraphPrintingByRepresentative normalizedGraphPrinting = new GraphPrintingByRepresentative(Graph.GraphType.NORMALIZED_GRAPH);
					normalizedGraphPrinting.printAllSources(this);
					log.info("Normalization finished in " + (System.currentTimeMillis() - startTime) + " ms");
					if (endAt.equals("normalization")) {
						stop = true;
					}
				}

				// 3. Collections identification
				if(!stop) {
					startTime = System.currentTimeMillis();
					collectionGraphBuilding = new CollectionGraphBuilding(this, ds);
					collectionGraphBuilding.run();
//					log.info("Collection graph building finished in " + (System.currentTimeMillis() - startTime) + " ms");
					if(endAt.equals("collections")) { stop = true; }
				}

				// 6. we are almost done: we need to compute some statistics on the abstraction
				// we run this only if we have done all steps of abstraction (else it produces null pointer exceptions)
				if(!stop) {
					startTime = System.currentTimeMillis();
					mainCollectionsIdentification = new MainCollectionsIdentification(this, ds);
					mainCollectionsIdentification.run();
					log.info("Main collections identification finished in " + (System.currentTimeMillis() - startTime) + " ms");
					if(endAt.equals("mainCollections")) { stop = true; }
				}

				if(!stop) {
					startTime = System.currentTimeMillis();
					classification = new Classification(this, ds, new Locale(Config.getInstance().getStringProperty("default_locale"))); // we give the classified graph to the constructor to initialize it (will be useful for the GUI)
					classification.run();
					log.info("Classification finished in " + (System.currentTimeMillis() - startTime) + " ms");
					if(endAt.equals("classification")) { stop = true; }
				}

				if(!stop) {
					startTime = System.currentTimeMillis();
					this.reportingToUser = new ReportingToUser(this, ds, mainCollectionsIdentification);
					this.reportingToUser.run();
					log.info("Reporting finished in " + (System.currentTimeMillis() - startTime) + " ms");
					if(endAt.equals("reporting")) { stop = true; }

					CollectionGraphPrinting collectionGraphPrinting = new CollectionGraphPrinting(this, ds, mainCollectionsIdentification);
					collectionGraphPrinting.printAllSources(false);

					log.info("Finished successfully the abstraction of the dataset \"" + collectionGraphBuilding.getDatasetName() + "\". Selected " + mainCollectionsIdentification.getNbSelectedMainEntities() + " main entities.");

					if(Config.getInstance().getBooleanProperty("compare_with_reference")) {
						AbstractionsComparator comparingDescriptions = new AbstractionsComparator();
						comparingDescriptions.compare();
					}
				}

				// 7. we are done, we can report statistics and finish work!
//				this.abstractionWorkStats.stopAll();
				normalization.reportStatistics();
				collectionGraphBuilding.reportStatistics();
				mainCollectionsIdentification.reportStatistics();
				classification.reportStatistics();
				this.reportingToUser.reportStatistics();
			}

		} else {
//			this.abstractionWorkStats = null;
		}
	}

	private void cleanTablesBeforeAbstraction() {
		// we also need to delete the abstraction content if needed
		RelationalGraph graph = (RelationalGraph) this.graph();
		if(Config.getInstance().getBooleanProperty("start_from_collection_graph")) {
			// we start from the collection graph - therefore we need to keep the following tables:
			// - collection_graph
			// - collections_nodes
			// - collections_informations
			// - transfers
			// - signatures
			// - tables used during normalization

			// for the other tables - we don't care, so we will not spend time to drop them
			// in the end we have nothing to delete
		} else {
			// if we start from the beginning we have to drop all the tables
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.ODW_COMPUTATION_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_SUMMARY_TS_REP_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_SUMMARY_TS_EDGES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_MAPPING_RDFQ_ABSTRA_IDS_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.RDFQ_IGNORED_NODES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.COLLECTIONS_TYPES + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.COLLECTIONS_SIZES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.SIGNATURES_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.PAIRS_FOR_TRANSFERS_TABLE_NAME + " CASCADE");
			graph.executeUpdate("DROP TABLE IF EXISTS " + SchemaTableNames.ALL_INFORMATION_FOR_TRANSFERS_TABLE_NAME + " CASCADE");
		}
	}

	public ReportingToUser getReportingToUser() {
		return this.reportingToUser;
	}

	/**
	 * Register the data source at the given URI.
	 *
	 * @param uri the URI of the data source to register.
	 * @return the registered data source
	 * @throws IllegalStateException if the given cannot be parsed to a valid URI.
	 */
	public DataSource register(URI uri, URI origURI) {
		int batch = config.getIntProperty("update_batch_size");
		DataSource result = null;
		try (final GraphSession gs = graph.openSession(batch, pairProcessor::makeSignature, graphStats, extractStats,
				simStats);
				final IndexAndProcessNodes.IndexAndProcessNodesSession is = graph.index().openSession(batch, graph)) {
			log.info("Loading " + uri);
			result = register(this.graph, gs, is, uri, origURI);
		} catch (Exception e) {
			e.printStackTrace();
		}
		result.postprocess(graph);
		// stats.tick(result.getLocalURI(), "POST_PROCESS");
		return result;
	}

	/**
	 * Register the data source at the given URI.
	 *
	 * @param uris the URI of the data source to register.
	 * @return the registered data source
	 */
	public Iterable<DataSource> register(Iterable<URI> uris, @Nullable HashMap<URI, URI> OriginalURIMap) {
		List<DataSource> result = Lists.newLinkedList();
		int batch = config.getIntProperty("update_batch_size");
		try (final GraphSession gs = graph.openSession(batch, pairProcessor::makeSignature, graphStats, extractStats,
				simStats);
				final IndexAndProcessNodes.IndexAndProcessNodesSession is = graph.index().openSession(batch, graph)) {
			for (URI uri : uris) {
				//log.info(Config.getInstance().getProperty("value_atomicity") + " registration of: " + uri);
				URI originalURI = OriginalURIMap.get(uri);
				final DataSource ds = register(graph, gs, is, uri, originalURI);
				// log.info("So far we have: " +
				// graph.getEdges(Edge.SAME_AS_LABEL).size() + " sameas edges");
				if (ds != null) { // if not already added
					result.add(ds);
				}
			}
		}

		result.forEach(s -> {
			// s.postprocess(graph);
			// graphStats.tick(s.getLocalURI(), POST_T);
			int sNodesNo = graph.countNodes(s);
			graphStats.put(s.getLocalURI(), NODES_NO, sNodesNo);
			graphStats.increment(StatisticsCollector.total(), NODES_NO, sNodesNo);

			int sPersonNo = graph.countNodes(s, ENTITY_PERSON);
			extractStats.put(s.getLocalURI(), PERS_NO, sPersonNo);
			extractStats.increment(StatisticsCollector.total(), PERS_NO, sPersonNo);

			int sLocationNo = graph.countNodes(s, ENTITY_LOCATION);
			extractStats.put(s.getLocalURI(), LOC_NO, sLocationNo);
			extractStats.increment(StatisticsCollector.total(), LOC_NO, sLocationNo);

			int sOrgNo = graph.countNodes(s, ENTITY_ORGANIZATION);
			extractStats.put(s.getLocalURI(), ORG_NO, sOrgNo);
			extractStats.increment(StatisticsCollector.total(), ORG_NO, sOrgNo);

			int sEdgeNo = graph.countEdges(s);
			graphStats.put(s.getLocalURI(), EDGES_NO, sEdgeNo);
			graphStats.increment(StatisticsCollector.total(), EDGES_NO, sEdgeNo);

//			graphStats.put(s.getLocalURI(), REP_NO, graph.countDistinctRepresentatives(s)); no: expensive!
//			stats.put(s.getURI(), ENTITIES_NO, graph.countNodes(s, ImmutableSet.of(
//					ENTITY_GENERIC, ENTITY_PERSON, ENTITY_ORGANIZATION, ENTITY_LOCATION)));
			graphStats.stop(s.getLocalURI());
			// extractStats.stop(s.getLocalURI());
			// nothing about simStats here because it's not reported by input
		});
		return result;
	}

	/**
	 *
	 * @param uri
	 * @param pdfOriginalURI
	 * @return datasources from PDF registration.
	 * @throws Exception
	 * IM, 17/2/22: Called only from the GUI and from a test
	 */
	public Iterable<DataSource> registerPDF(String uri, URI pdfOriginalURI) throws Exception {
		String formatted = uri.replace("\\", "/");
		List<DataSource> result = Lists.newLinkedList();
		int batch = config.getIntProperty("update_batch_size");
		final GraphSession gs = graph.openSession(batch, pairProcessor::makeSignature, graphStats, extractStats,
				simStats);
		final IndexAndProcessNodes.IndexAndProcessNodesSession is = graph.index().openSession(batch, graph);
		List<URI> uriExtracted = extractPDFContent(new URI(formatted));
		HashMap<URI, URI> originalURIMap = setOriginalURItoPDFextracted(uriExtracted, pdfOriginalURI);
		register(uriExtracted, originalURIMap).forEach(result::add);
		return result;
	}

	Consumer<Edge> edgeProcessor;

	/**
	 * Register the data source at the given URI.
	 *
	 * @param uri the URI of the data source to register.
	 * @return the registered data source
	 */
	private DataSource register(Graph graph, GraphSession gs, IndexAndProcessNodes.IndexAndProcessNodesSession is,
			URI uri, URI origURI) {
		graphStats.start(uri);
		extractStats.start(uri);

		final DataSource dataSource = catalog.addEntry(graph, resolve(uri), uri, origURI);
		if (dataSource == null) { // we had already registered this
			return null;
		}
		dataSource.setGraphStats(graphStats);
		dataSource.setExtractStats(extractStats);
		dataSource.setIndexStats(indexStats);
		dataSource.init();
		// log.info("Extraction and indexing");
		edgeProcessor = edge -> {
			if (!gs.hasProcessed(edge)) {
				Node source = edge.getSourceNode();
				Node target = edge.getTargetNode();
				//
//				log.info("\nregister edge: " + source.getLabel() + 
//						"@ds" + source.getDataSource().getID() + 
//						" (" +
//						Node.decodeType(source.getType()) + ") --" + edge.getLabel() + "-->" +
//				target.getLabel() +
//				"@ds" + target.getDataSource().getID() + 
//				" (" + Node.decodeType(target.getType())+ ")");

				gs.addEdge(edge, is::addEdge);
				
				if (source.getDataSource().equals(dataSource) && !gs.hasProcessed(source)) {
					gs.addNode(source, n -> is.addNode(n, edgeProcessor));
				}
				if (target.getDataSource().equals(dataSource) && !gs.hasProcessed(target)) {
					gs.addNode(target, n -> is.addNode(n, edgeProcessor));
				}
			}
		};
		try {
			dataSource.traverseEdges(edgeProcessor);
		} catch(Exception e) {
			e.printStackTrace();
		}
		// added IM 28/11/2020, necessary to flush the last batch of extraction
		if (this.extractor instanceof BatchExtractor) {
			ExtractorBatch extractorBatch = ((BatchExtractor) extractor).getExtractorBatch();
			is.processAndFlushExtractorBatch(extractorBatch);
		}
		return dataSource;
	}

	private void ensureStatistics() {
		if (this.graphStats == null) {
			graphStats = statsLog.getStatisticsCollector(GRAPH);
			if (this.graphStats == null) {
				this.graphStats = new StatisticsCollector();
				statsLog.putStatisticsCollector(GRAPH, graphStats);
			}
		}
		if (this.extractStats == null) {
			extractStats = statsLog.getStatisticsCollector(EXTRACTION);
			if (this.extractStats == null) {
				this.extractStats = new StatisticsCollector();
				statsLog.putStatisticsCollector(EXTRACTION, extractStats);
			}
		}
		extractor.setStats(extractStats);
		if (this.indexStats == null) {
			indexStats = statsLog.getStatisticsCollector(INDEX);
			if (this.indexStats == null) {
				this.indexStats = new StatisticsCollector();
				statsLog.putStatisticsCollector(INDEX, indexStats);
			}
		}
		if (this.simStats == null) {
			simStats = statsLog.getStatisticsCollector(SIMILARITY);
			if (this.simStats == null) {
				this.simStats = new StatisticsCollector();
				statsLog.putStatisticsCollector(SIMILARITY, simStats);
			}
		}
		if (this.queryStats == null) {
			queryStats = statsLog.getStatisticsCollector(QUERY);
			if (this.queryStats == null) {
				this.queryStats = new StatisticsCollector();
				statsLog.putStatisticsCollector(QUERY, queryStats);
			}
		}
	}

	/**
	 * Generates pairs of sources to compare
	 * 
	 * @param old
	 * @param newElements
	 * @return
	 */
	protected static <E> List<Pair<E, E>> listPairs(Collection<E> old, Collection<E> newElements) {
		List<E> elements = newElements instanceof List ? (List<E>) newElements : Lists.newArrayList(newElements);
		List<Pair<E, E>> result = new LinkedList<>();
		int k = 0;
		for (E element : elements) {
			Iterator<E> i = elements.subList(k, elements.size()).iterator();
			while (i.hasNext()) {
				result.add(Pair.of(element, i.next()));
			}
			k++; // up to here, we compare each newElement with the the elements _after_ it,
					// including itself
			old.forEach(o -> result.add(Pair.of(element, o))); // here we compare each old element with the new one BUT
																// WE DID NOT ADD IT TO RESULT!
		}
		return result;
	}

	public void processSimilarities(Collection<DataSource> dsList) {
		if (Config.getInstance().getBooleanProperty("compare_nodes")) {
			processSimilarities(Collections.emptyList(), dsList);
		}
	}

	public void processSimilarities(Collection<DataSource> from, Collection<DataSource> to) {
		// log.info("Processing similarities");
		int batch = config.getIntProperty("update_batch_size");
		try (GraphSession gs = graph.openSession(batch, graphStats, extractStats, simStats)) {
			for (Pair<DataSource, DataSource> pair : listPairs(from, to)) {
				// log.info("\n\nCL.PROCESS SIMILARITIES FOR " + pair.getLeft() + " to
				// " + pair.getRight());
				pairProcessor.process(graph, singleton(pair.getLeft()), singleton(pair.getRight()), gs::addEdge);
				// log.info(pair.getLeft().getURI
				// ()+"--"+WSA_NO+"--"+graph.getWeakSameAs(null,0.0).size());
			}
		}
	}

	/**
	 * Process similarities between the given data source and all the others.
	 *
	 * @param from the data source for which to compute the similarities.
	 */
	protected void processSimilarities(GraphSession gs, Collection<DataSource> from, Collection<DataSource> to,
			SimilarPairProcessor pp) {
//		simStats.resume(from.getURI());
		simStats.resume(pp.getClass().getSimpleName());
		pp.process(graph, from, to, gs::addEdge);
//		stats.tick(from.getURI(), SAMEAS);
		simStats.tick(pp.getClass().getSimpleName(), SAMEAS_T);
//		pp.recordStats(from.getURI(), stats);

//		stats.put(from.getURI(),  SAMEAS_NO, graph.countSameAs(from));
//		stats.tick(pp.getClass().getSimpleName(), LOGGING);
//		stats.tick(from.getURI(), LOGGING);

//		stats.pause(from.getURI());
		simStats.pause(pp.getClass().getSimpleName());
	}

	private Factory buildItemIDFactory() {
		StorageModels model = StorageModels.valueOf(config.getProperty("storage_model"));
		switch (model) {
		/*
		 * case STRING: return new StringIDFactory();
		 */
		case COMPACT:
			return new NumericIDFactory();
		default:
			throw new IllegalStateException();
		}
	}

	/**
	 * @return the virtual graph
	 */
	public Graph graph() {
		return this.graph;
	}

	/**
	 * @return the data source catalog
	 */
	public DataSourceCatalog catalog() {
		return catalog;
	}

	public MorphoSyntacticAnalyser mas() {
		return this.mas;
	}

	public Collection<DataSource> getDataSources() {
		return this.catalog.getDataSources();
	}

	public ArrayList<DataSource> getDataSourcesAsList(){
		ArrayList<DataSource> datasources = new ArrayList<>();
		for(DataSource ds : this.catalog.getDataSources()) {
			datasources.add(ds);
		}
		return datasources;
	}
	/**
	 * @param uri the URI for which to resolve the data source type.
	 * @return the data source types associated with the given URI
	 */
	@Nullable
	private static DataSource.Types resolve(URI uri) {
		String normalized = uri.toString().toLowerCase();
		if (normalized.endsWith(".nt") || normalized.endsWith(".ttl")) {
			return RDF;
		}
		if (normalized.endsWith(".json")) {
			return JSON_V2;
		}
		if (normalized.contains("jdbc:")) {
			return RELATIONAL;
		}
		if (normalized.endsWith(".html")) {
			return HTML;
		}
		if (normalized.endsWith(".xml")) {
			return XML_V2;
		}
		if (DataSource.isPDFFileURI(uri)) {
			return PDF;
		}
		if (normalized.contains(".njn") && normalized.contains(".nje")) {
			return NEO4J;
		}
		if(normalized.endsWith(".csv")) {
			return CSV_V2;
		}
		if (DataSource.isDocumentURI(uri)) {
			return DOC;
		}
		if (DataSource.isSpreadsheetURI(uri)) {
			return XCL;
		}
		if (DataSource.isPresentationURI(uri)){
			return PRE;
		}
		return TEXT;
	}

	/**
	 * register PDF file. This registration is quite different compared to other
	 * registrations, since it leads to multiple datasource from one input.
	 * 
	 * @param PDFFile
	 * @return
	 * @throws Exception
	 * IM, 17/12/2022: only called from test
	 */
	public Iterable<DataSource> registerPDF(String PDFFile) throws Exception {
		return registerPDF(PDFFile, null);
	}

	/**
	 * latest: this should also be removed
	 * This class describes a column in a CSV file.
	 */
	public static final class ColumnDescriptor {

		/** The column name. */
		final String name;

		/** The column type. */
		final String type;

		/**
		 * Instantiates a new column descriptor.
		 *
		 * @param name the name
		 * @param type the type
		 */
		public ColumnDescriptor(String name, String type) {
			super();
			Objects.requireNonNull(name);
			Objects.requireNonNull(type);
			this.name = name;
			this.type = type;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hash(name, type);
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			return getClass().isInstance(obj) && ((ColumnDescriptor) obj).name.equals(name)
					&& ((ColumnDescriptor) obj).name.equals(name);
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return name + " " + type;
		}
	}

	/**
	 * prajna (25.02.2022): should be removed because it returns string as a type for all the fields
	 *
	 * Analyzes the CSV file at the given URI, and returns a list of column
	 * descriptors inferred from the first lines.
	 *
	 * @param uri the URI of the file to analyze
	 * @return a list of column description for the content of the CSV file
	 * @throws FileNotFoundException if the file not found exception
	 * @throws IOException           signaling that an I/O exception has occurred.
	 */
	private static List<ColumnDescriptor> csvAnalyse(URI uri) throws FileNotFoundException, IOException {
		String[] fields = new String[2];
		String[] values = new String[2];
		List<ColumnDescriptor> result = new LinkedList<>();
		try (Scanner scanner = new Scanner(uri.toURL().openStream(), "UTF-8")) {
			if (scanner.hasNextLine()) {
				fields = scanner.nextLine().split(",");
			} else {
				throw new IllegalArgumentException("Fields could not be detected.");
			}
			if (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("\"")) {
					line = line.substring(1, line.length() - 1);
					values = line.split("\",\\s*\"");
				} else {
					values = scanner.nextLine().split(",");
				}
				checkState(fields.length == values.length);
			}
			for (int i = 0, l = fields.length; i < l; i++) {
				result.add(new ColumnDescriptor(fields[i], inferType(values[i])));
			}
		}
		return result;
	}

	/**
	 * Infers the type of the given value. Currently, this always return TEXT.
	 *
	 * @param value the value for which to infer type.
	 * @return the inferred type.
	 */
	private static String inferType(String value) {
		return "TEXT";
	}

	/**
	 * Picks a name database name for the file at the given URI. The name is based
	 * on the file's simple name, from which we remove any extension, and replace
	 * hyphen into underscores.
	 *
	 * @param uri the URI for which to pick a name
	 * @return the name
	 */
	private static String pickName(URI uri) {
		String path = uri.toString();
		String result = path.substring(path.lastIndexOf('/') + 1, min(path.lastIndexOf('.'), path.length() - 1));
		result = result.replaceAll("-", "_");
		return result;
	}

	/**
	 * Load the(CSV) file at the given URI into a new data source, erasing any
	 * pre-existing database with same name.
	 *
	 * @param input the input URI
	 * @return the URI of the newly loaded data source.
	 */
	static URI load(URI input) {
		return load(input, true);
	}

	/**

	 * Resolves the scoring function based on the given name.
	 *
	 * @param name the scoring function's name
	 * @return a fresh instance of scoring function
	 */
	public ScoringFunction resolveScoringFunction(String name, Query q) {
		// log.info("Scoring function being resolved.");
		if (isNullOrEmpty(name) || "default".equalsIgnoreCase(name)) {
			double alpha = config.getDoubleProperty("score_alpha");
			MatchingScore ms = new MatchingScore(q);
			double beta = config.getDoubleProperty("score_beta");
			ConnectionScore cs = new ConnectionScore(beta);
			SpecificityScore ss = new SpecificityScore();
			return new WeightedScore(new ScoringFunction[] { ms, cs, ss },
					new Double[] { alpha, beta, (1 - alpha - beta) });
		} else if ("answersize".equalsIgnoreCase(name)) {
			return new AnswerSizeScore();
		}

		throw new ParameterException("No such scoring function: " + name);
	}

	protected void printExtractedEntities(Graph g) {
		log.info("PEOPLE (" + graph.getNodes(Types.ENTITY_PERSON).size() + "):");
		int iPeople = 1;
		for (Node n : graph.getNodes(Types.ENTITY_PERSON)) {
			log.info(iPeople + ": " + n);
			iPeople++;
		}
		log.info("LOCATION (" + graph.getNodes(Types.ENTITY_LOCATION).size() + "):");
		int iLocs = 1;
		for (Node n : graph.getNodes(Types.ENTITY_LOCATION)) {
			log.info(iLocs + ": " + n);
			iLocs++;
		}
		log.info("ORGANIZATION (" + graph.getNodes(Types.ENTITY_ORGANIZATION).size() + "):");
		int iOrgs = 1;
		for (Node n : graph.getNodes(Types.ENTITY_ORGANIZATION)) {
			log.info(iOrgs + ": " + n);
			iOrgs++;
		}
	}

	/**
	 * Load the(CSV) file at the given URI into a new data source.
	 *
	 * @param input the input URI
	 * @param force if true, erases any pre-existing database with same name.
	 * @return the URI of the newly loaded data source.
	 */
	static URI load(URI input, boolean force) {
		String sql = "";
		try {
			final String container = "csv";
			final String schemaName = pickName(input);
			final String tableName = schemaName + ".contents";
			List<ColumnDescriptor> descriptors = csvAnalyse(input);
			setupDB(container, schemaName);

			String defaultUser = Config.getInstance(true).getProperty("RDBMS_user");
			String defaultPassword = Config.getInstance().getProperty("RDBMS_password");
			URI result = new URI("jdbc:postgresql://localhost:5432/" + container + "?currentSchema=" + schemaName
					+ "&user=" + defaultUser + "&password=" + defaultPassword);
			try (Connection c = ConnectionManager.getConnectionByURL(result.toString());
					Statement s = c.createStatement()) {
				if (force) {
					try {
						s.executeUpdate("DROP TABLE " + tableName);
					} catch (SQLException e) {
						// Ignore
					}
				}
				log.info(Joiner.on(',').join(descriptors));
				s.executeUpdate((sql = "CREATE TABLE " + tableName + " (" + Joiner.on(',').join(descriptors) + ")"));
				s.executeUpdate((sql = "COPY " + tableName + " FROM '" + Paths.get(input) + "' CSV HEADER"));
			} catch (SQLException e) {
				if (!force) {
					log.error("Check if table '" + tableName + "' already exists.");
				}
				throw new IllegalStateException(sql, e);
			}

			return result;
		} catch (SQLException | IOException | URISyntaxException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * extractPDF corresponds to the script developed by @Youssr
	 *
	 * @param uri
	 * This is actually called during registration
	 */
	public List<URI> extractPDFContent(URI uri) throws IOException, URISyntaxException {
		List<URI> conversionOutputs = new ArrayList<>();
		File file = new File(uri.getPath());
		PDFDataSource pdfDataSource = new PDFDataSource(uri.toString());

		Path pdfExtractedPath = Paths.get(file.getParent(), "extracted_files_", pdfDataSource.getPDFName());

		log.info("Path to PDF: " + pdfExtractedPath);
		if (Files.exists(pdfExtractedPath)) {
			File oldExtractedFolder = new File(
					Paths.get(file.getParent(), "extracted_files_", pdfDataSource.getPDFName()).toString());
			// log.info("oldExtractedFolder=" + oldExtractedFolder);
			deleteDirectory(oldExtractedFolder);
		}

		int process = pdfDataSource.runScraping();
		log.info("PDF extracting process: " + process);
		if (process > 0)
			log.error("PDF scraping failed");
		// log.info(uri.getPath());

		File extractedFolder = new File(
				Paths.get(file.getParent(), "extracted_files_" + pdfDataSource.getPDFName()).toString());
		while (!extractedFolder.exists()) {
			try {
				this.wait(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (!extractedFolder.exists()) {
					log.error("the folder:" + "extracted_files_" + pdfDataSource.getPDFName() + " was not created.");
				}
			}
		}

		if (extractedFolder.listFiles().length > 0) {
			for (File extractedFile : extractedFolder.listFiles()) {
				String newPath = extractedFile.getPath().substring(extractedFile.getPath().indexOf(file.getParent()));
				URI fileURI = new URI("file:" + newPath);
				conversionOutputs.add(fileURI);
			}
		}
		return conversionOutputs;
	}


	public List<URI> extractOfficeContent(URI uri) throws IOException, URISyntaxException {
		return (new OfficeDataSource(uri.getPath())).convert();
	}

	/**
	 *
	 * @param dataSourceURIs
	 * @param originalPDFURI
	 * @return map the PDF to the URIs of the data sources extracted from it
	 */
	public HashMap<URI, URI> setOriginalURItoPDFextracted(List<URI> dataSourceURIs, URI originalPDFURI) {
		HashMap<URI, URI> urisToOrigURIs = new HashMap<>();
		for (URI ds : dataSourceURIs) {
			urisToOrigURIs.put(ds, originalPDFURI);
		}
		return urisToOrigURIs;
	}

	/**
	 *
	 * @param dataSourceURIs
	 * @param originalURIs
	 * @return set multiple ORIUris
	 * @throws Exception
	 */
	public HashMap<URI, URI> getOriginalURIMap(List<URI> dataSourceURIs, List<URI> originalURIs) throws Exception {
		HashMap<URI, URI> map = new HashMap<>();
		if (originalURIs.isEmpty()) {
			dataSourceURIs.stream().forEach(s -> map.put(s, null));
		} else {
			if (originalURIs.size() == dataSourceURIs.size()) {
				for (URI ds : dataSourceURIs) {
					URI orignURI = originalURIs.get(dataSourceURIs.indexOf(ds));
					if (orignURI.equals("")) {
						map.put(ds, null);
					} else {
						map.put(ds, orignURI);
					}
				}
			} else {
				throw new Exception("The number of URI must be the same as the number of datasources");
			}
		}

		return map;
	}

	/**
	 * Setup the databases that will contain the content CSV file(s).
	 *
	 * @param container the database name
	 * @param schemas   the schemas
	 * @throws SQLException
	 */
	public static void setupDB(String container, String... schemas) throws SQLException {
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL, "postgres");
				Statement statement = c.createStatement()) {
			statement.executeUpdate("CREATE DATABASE " + container);
		} catch (SQLException e) {
			log.warn("Database '" + container + "' already exists. May be erased.");
		}
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL, container)) {
			for (String schema : schemas) {
				try (Statement statement = c.createStatement()) {
					statement.executeUpdate("CREATE SCHEMA " + schema);
				} catch (SQLException e) {
					log.warn("Schema '" + schema + "' already exists. May be erased.");
				}
			}
		}
	}

	/**
	 * Tears down the database containing CSV files.
	 *
	 * @param container the database name
	 * @throws SQLException
	 */
	public static void tearDownDB(String container) throws SQLException {
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL, "postgres");
				Statement statement = c.createStatement()) {
			statement.executeUpdate("DROP DATABASE " + container);
		}
	}

	@Override
	public void close() {
		graph().close();
		graph().index().close();
		catalog().close();
		// IM, 12/4/21
		ConnectionManager.close();
	}

	public Factory factory() {
		return this.factory;
	}

	public EntityExtractor extractor() {
		return this.extractor;
	}

	public StatisticsCollector graphStats() {
		return graphStats;
	}

	public StatisticsCollector extractStats() {
		return extractStats;
	}

	public StatisticsCollector indexStats() {
		return indexStats;
	}

	public StatisticsCollector simStats() {
		return simStats;
	}

	public StatisticsCollector queryStats() {
		return queryStats;
	}

	public StatisticsCollector abstractionStats() {
		return this.abstractionStats;
	}

	public StatisticsLogger statsLog() {
		return statsLog;
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;

import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;

public class NumericNodeID extends NumericItemID implements NodeID {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8677343537869662686L;

	@Override
	public String toString() {
		return "NumericNodeID(" + this.value + ")";
	}

	public NumericNodeID(int v) {
		super(v);
		//if (v%10000 == 0) {
		//	log.info(v + " nodes");
		//}
	}

	@Override
	public Integer value() {
		return value;
	}

	@Override
	public int compareTo(ItemID id) {
		if (!getClass().isInstance(id)) {
			throw new IllegalStateException("Comparing incomparable objects"); 
		}
		NumericNodeID i2 = (NumericNodeID) id;
		if (this.value == i2.value()) {
			return 0;
		}
		else {
			return this.value.compareTo(i2.value); 
		}
	}
}


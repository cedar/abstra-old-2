/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;

import fr.inria.cedar.connectionlens.util.ExceptionTools;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The Config class give access to external parameters to the system. These parameters are by
 * default defined in the file named 'local.settings', but may be overridden by arbitrary Properties
 * file.   
 */
public class Config implements Serializable {
	
	/** Interpreter logger. */
	static Logger log = Logger.getLogger(Config.class);

	/** The location of the default config file. */
	public static final String DEFAULT_CONFIG_FILE_PATH = "local.settings"; //$NON-NLS-1$

	/** The singleton. */
	private static Config theConfig = null;
	
	/** The underlying Properties. */
	private Properties props; 

	/** The list of locations of files used so far to override the local.settings. */
	private List<String> locations = new LinkedList<>();

	/**
	 * Private constructor.
	 *
	 * @param path the path to the Properties file.
	 */
	private Config(String path) {
		props = new Properties();
		override(path);
		log.info("local.settings file loaded: "+path);
		for (Object key: props.keySet()) {
			log.info("props: "+key.toString()+"="+props.getProperty((String) key));
		}
//		try {
//			throw new Exception("Created a config " + this.hashCode());
//		}
//		catch(Exception e) {
//			e.printStackTrace(); 
//		}
	}
	
	/**
	 * Gets the single instance of Config.
	 *
	 * @return single instance of Config
	 */
	public static Config getInstance() {
		return getInstance(DEFAULT_CONFIG_FILE_PATH, false);
	}
	
	/**
	 * Gets the single instance with the path to an alternative file for defaults.
	 *
	 * @param path the path
	 * @return single instance of Config
	 */
	public static Config getInstance(String path) {
		return getInstance(path, false);
	}
	
	/**
	 * Gets the single instance of Config.
	 *
	 * @return single instance of Config
	 */
	public static Config getInstance(boolean reload) {
		return getInstance(DEFAULT_CONFIG_FILE_PATH, reload);
	}
	
	/**
	 * Gets the single instance with the path to an alternative file for defaults.
	 *
	 * @param path the path
	 * @return single instance of Config
	 */
	public static Config getInstance(String path, boolean reload) {
		if (theConfig == null || reload) {
//			try {
//				throw new Exception("");
//			}
//			catch(Exception e) {
//				System.out.println("Creating! TheConfig is null: " + (theConfig==null) + " reload: " + reload); 
//				System.out.println(ExceptionTools.prettyPrintException(e, 5)); 
//			}
			theConfig = new Config(path);
		}
		return theConfig;
	}
	
	/**
	 * Override existing key-values.
	 * @param values the value to override
	 */
	public void override(Map<String, String> values) {
		for (Map.Entry<String, String> e: values.entrySet()) {
			if (props.get(e.getKey()) != null) {
				System.out.println("Overriding " + props.getProperty(e.getKey()) + " with " + e.getValue());
				props.put(e.getKey(), e.getValue());
			}
			else {
				throw new IllegalStateException("A value for parameter " + e.getKey() + 
						" was passed, but this parameter is not supported.\nPlease check the documentation"); 
			}
		}
	}
	
	/**
	 * Loads the parameter on the given path, overriding existing key-values.
	 * @param path
	 */
	public void override(String path) {
		try(InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(path); 
			InputStreamReader reader = new InputStreamReader(in)) {
			props.load(reader);
			locations.add(path);
			return;
		} catch (final Exception e) {
			log.warn("Loading " + path + " (1) failed: " + e.getMessage());
		}
		try(InputStreamReader reader = new FileReader(path)) {
			props.load(reader);
			locations.add(path);
			return;
		} catch (final IOException e) {
			log.warn("Loading " + path + " (2) failed: " + e.getMessage());
		}
	}

	@Override
	public String toString() {
		return "Config(" + Joiner.on(", ").join(locations) + ")";
	}
	
	/**
	 * Gets the property for the given key
	 *
	 * @param key the key of property to retrieve
	 * @return the property for the given key
	 */
	public String getProperty(String key) {
		String result = props.getProperty(key);
		if (result != null) {
			return result.trim(); 
		}
		throw new IllegalStateException("No such parameter: " + key);
	}


	/**
	 * Gets the property for the given key, as a string.
	 *
	 * @param key the key of property to retrieve
	 * @param defaultValue the value used if the key does not exist
	 * @return the property as a string (or its default value)
	 */
	public String getStringProperty(String key, String defaultValue) {
		if(!getProperty(key).isEmpty()){
			return getProperty(key).trim();
		}
		else{
			return defaultValue;
		}
	}

	/**
	 * Gets the property for the given key, as a string.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as a string
	 * @throws RuntimeException if the given property is not set
	 */
	public String getStringProperty(String key) {
		if(!getProperty(key).isEmpty()){
			log.info("On " + key + " found: " + getProperty(key));
			return getProperty(key).trim();
		}
		else{
			throw new RuntimeException("The key " + key + "does not exist in the configuration file.");
		}
	}
	
	/**
	 * Gets the property for the given key, as a double.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as a double
	 * @throws IllegalStateException if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid double
	 */
	public double getDoubleProperty(String key) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Double.valueOf(result.trim());
		}
		throw new IllegalStateException("No such parameter: " + key);
	}
	
	/**
	 * Gets the property for the given key, as a boolean.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as a boolean
	 * @throws IllegalStateException if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid boolean
	 */
	public boolean getBooleanProperty(String key) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Boolean.valueOf(result.trim());
		}
		throw new IllegalStateException("No such parameter: " + key);
	}
	
	/**
	 * Gets the property for the given key, as an int.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as an integer
	 * @throws IllegalStateException if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid integer
	 */
	public int getIntProperty(String key) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Integer.valueOf(result.trim());
		}
		throw new IllegalStateException("No such parameter: " + key);
	}
	
	/**
	 * Gets the property for the given key, as a long.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as an long
	 * @throws IllegalStateException if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid long
	 */
	public long getLongProperty(String key) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Long.valueOf(result.trim());
		}
		throw new IllegalStateException("No such parameter: " + key);
	}
	
	/**
	 * Gets the property for the given key, as a double.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as a double, or the default value if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid double
	 */
	public double getDoubleProperty(String key, double d) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Double.valueOf(result.trim());
		}
		return d;
	}
	
	/**
	 * Gets the property for the given key, as a boolean.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as a boolean, or the default value if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid boolean
	 */
	public boolean getBooleanProperty(String key, boolean b) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Boolean.valueOf(result.trim());
		}
		return b;
	}
	
	/**
	 * Gets the property for the given key, as an int.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as an integer, or the default value if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid integer
	 */
	public int getIntProperty(String key, int i) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Integer.valueOf(result.trim());
		}
		return i;
	}
	
	/**
	 * Gets the property for the given key, as a long.
	 *
	 * @param key the key of property to retrieve
	 * @return the property as an long, or the default value if the given property is not set
	 * @throws NumberFormatException if the value for the given property is not a valid long
	 */
	public long getLongProperty(String key, long l) {
		String result = props.getProperty(key);
		if (!Strings.isNullOrEmpty(result)) {
			return Long.valueOf(result.trim());
		}
		return l;
	}




	/**
	 * This method can be called by any class that needs to set global local.settings
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, String value) {
		props.setProperty(key, value);
	}
	
	/**
	 * @return the set of keys as Strings.
	 */
	public Set<String> keySet(){
		return props.keySet().stream().map(String.class::cast).collect(Collectors.toSet()); 
	}
	
	/**
	 * @return the number of keys.
	 */
	public int size(){
		return props.keySet().size(); 
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;

public class SchemaTableNames {

    public static final String ORIGINAL_NODES_TABLE_NAME = "nodes";
    public static final String ORIGINAL_EDGES_TABLE_NAME = "edges";
    public static final String ORIGINAL_BIDIREDGES_TABLE_NAME = "bidiredges";
    public static final String ORIGINAL_SAME_AS_EDGES_TABLE_NAME = "same_as_edges";
    public static final String ORIGINAL_SPECIFICITY_TABLE_NAME = "specificity";
    public static final String ORIGINAL_SPECIFICITY_EDGE_TABLE_NAME = "edge_specificity";
    public static final String ORIGINAL_WEAK_SAME_AS_TABLE_NAME = "weak_same_as";
    public static final String ORIGINAL_CATALOG_TABLE_NAME = "catalog";
    public static final String ORIGINAL_DISAMBIGUATED_TABLE_NAME = "disambiguatedInto";
    public static final String ORIGINAL_SPEC_STAT_TABLE_NAME = "spec_stats";

    public static final String NORMALIZED_NODES_TABLE_NAME = "norm_nodes";
    public static final String NORMALIZED_EDGES_TABLE_NAME = "norm_edges";
    public static final String NORMALIZED_SPEC_STAT_TABLE_NAME = "norm_spec_stats";

    public static final String ABSTRACT_NODES_TABLE_NAME = "abs_nodes";
    public static final String ABSTRACT_EDGES_TABLE_NAME = "abs_edges";
    public static final String ABSTRACT_SPECIFICITY_TABLE_NAME = "abs_specificity";

    public static final String CLASSIFIED_NODES_TABLE_NAME = "class_nodes"; // nodesTableName
    public static final String CLASSIFIED_EDGES_TABLE_NAME = "class_edges"; // edgesTableName
    public static final String CLASSIFIED_BIDIREDGES_TABLE_NAME = "class_bidiredges"; // biDirEdgesTableName
    public static final String CLASSIFIED_SAME_AS_EDGES_TABLE_NAME = "class_same_as_edges"; // sameAsEdgesTableName
    public static final String CLASSIFIED_SPECIFICITY_TABLE_NAME = "class_specificity"; // specificityTableName
    public static final String CLASSIFIED_SPECIFICITY_EDGE_TABLE_NAME = "class_edge_specificity"; // computedSpecificityTableName
    public static final String CLASSIFIED_WEAK_SAME_AS_TABLE_NAME = "class_weak_same_as"; // weakSameAsEdgesTableName
    public static final String CLASSIFIED_DISAMBIGUATED_TABLE_NAME = "class_disambiguatedInto"; // disambiguatedTableName
    public static final String CLASSIFIED_SPEC_STAT_TABLE_NAME = "class_spec_stats"; // specStatTableName

    // normalization temporary tables
    public static final String NORMALIZATION_TMP_TABLE_NAME = "normalization_tmp";
    public static final String ODW_COMPUTATION_TABLE_NAME = "computation_dw_tmp";

    // tables used for the quotient summary creation
    public static final String COLLECTIONS_INFORMATIONS_TABLE_NAME = "collections_informations";
    public static final String COLLECTION_GRAPH_TABLE_NAME = "collection_graph";
    public static final String COLLECTIONS_NODES_TABLE_NAME = "collections_nodes";
    // following tables are specific to RDF summarization (RDFQuotient)
    public static final String RDFQ_SUMMARY_TS_REP_TABLE_NAME = "rdfq_summary_ts_rep";
    public static final String RDFQ_ENCODED_TRIPLES_TABLE_NAME = "rdfq_encoded_triples";
    public static final String RDFQ_DICTIONARY_TABLE_NAME = "rdfq_dictionary";
    public static final String RDFQ_SUMMARY_TS_EDGES_TABLE_NAME = "rdfq_summary_ts_edges";
    public static final String RDFQ_MAPPING_RDFQ_ABSTRA_IDS_TABLE_NAME = "map_rdfq_abstra_ids";
    public static final String RDFQ_ONTOLOGY_TRIPLES_TABLE_NAME = "tmp_ontology_triples";
    public static final String RDFQ_IGNORED_SUBJECTS_OBJECTS_TABLE_NAME = "tmp_ignored_subjects_objects";
    public static final String RDFQ_IGNORED_NODES_TABLE_NAME = "tmp_ignored_nodes";
    public static final String COLLECTIONS_TYPES = "collections_types";
    public static final String COLLECTIONS_SIZES_TABLE_NAME = "collections_sizes_tmp";
    public static final String AT_MOST_ONE_TABLE_NAME = "at_most_one_tmp";

    public static final String SUMMARY_EDGE_IDS_TABLE_NAME = "summary_edge_ids";

    // used in the classification to select hints
    public static final String SIGNATURES_TABLE_NAME = "signatures";

    // used in the class "Results". Mainly used for reporting purposes.
    public static final String PAIRS_FOR_TRANSFERS_TABLE_NAME = "pairs_for_transfers_tmp";
    public static final String ALL_INFORMATION_FOR_TRANSFERS_TABLE_NAME = "all_information_for_transfers";
    public static final String INACTIVE_NODES_TABLE_NAME = "inactive_nodes";
    public static final String INACTIVE_COLLECTIONS_TABLE_NAME = "inactive_collections";

    public static final String COLLECTIONS_CATEGORIES_TABLE_NAME = "collections_categories";
    public static final String TYPES_TABLE_NAME = "node_types";
}
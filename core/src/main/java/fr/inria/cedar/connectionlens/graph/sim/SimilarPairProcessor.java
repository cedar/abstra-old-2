/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.sim.EntityPairProcessor.LocationPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xml.utils.URI;
import org.apache.xml.utils.URI.MalformedURIException;

import java.text.ParseException;
import java.util.Collection;
import java.util.function.Consumer;

public interface SimilarPairProcessor {

	void process(Graph graph, Collection<DataSource> from, Collection<DataSource> to, Consumer<Edge> processor);
	NodePairSelector selector();
	int[] makeSignature(String s);
	static Logger log = Logger.getLogger(SimilarPairProcessor.class);


	/**
	 * 
	 * @param str
	 * @return an integer reflecting the type of the given string, 
	 *   0 for boolean, 1 for number, 3 otherwise.
	 */
	static int coerceType(String str) {
		String s = String.valueOf(str).trim();
		if ("true".equalsIgnoreCase(s) || "false".equalsIgnoreCase(s)
				|| "yes".equalsIgnoreCase(s) || "no".equalsIgnoreCase(s)) {
			return 1;
		}
		if (StringUtils.isNumeric(s)) {
			return 2;
		}
		try {
			DatePairProcessor.TWITTER.parse(s);
			return 3;
		} catch (ParseException e) {
			// Noop
		}
		try {
			new URI(s);
			return 4;
		} catch (MalformedURIException e) {
			// Noop
		}
		return 0;
	}
	
	/**
	 * Builds an instance of CandidatePairFinder using external parameters.
	 * 
	 * @return a fresh instance of candidate pair finder
	 */
	static SimilarPairProcessor buildPairFinder(Config config) {
		return buildPairFinder(config, StatisticsCollector.mute());
	}
	
	/**
	 * Builds an instance of CandidatePairFinder using external parameters.
	 * 
	 * @return a fresh instance of candidate pair finder
	 */
	static SimilarPairProcessor buildPairFinder(Config config, StatisticsCollector stats) {
		ChainedPairProcessor.Builder result = ChainedPairProcessor.builder(stats);
		double uriThreshold = config.getDoubleProperty("similarity_threshold_uri", -1);
		if (uriThreshold > .0) {
			result.add(new URIPairProcessor(stats, uriThreshold));
		}
		double personThreshold = config.getDoubleProperty("similarity_threshold_person", -1);
		double firstNameThreshold = config.getDoubleProperty("similarity_threshold_fname", -1);
		double lastNameThreshold = config.getDoubleProperty("similarity_threshold_lname", -1);
		if (personThreshold > .0) {
			result.add(new PersonPairProcessor(stats, personThreshold,firstNameThreshold,lastNameThreshold));
		}
		double locationThreshold = config.getDoubleProperty("similarity_threshold_location", -1);
		if (locationThreshold > .0) {
			result.add(new LocationPairProcessor(stats, locationThreshold));
		}
		double orgThreshold = config.getDoubleProperty("similarity_threshold_organization", -1);
		if (orgThreshold > .0) {
			result.add(new OrganizationPairProcessor(stats, orgThreshold));
		}
		double numberThreshold = config.getDoubleProperty("similarity_threshold_number", -1);
		if (numberThreshold > .0) {
			result.add(new NumberPairProcessor(stats, numberThreshold));
		}
		double dateThreshold = config.getDoubleProperty("similarity_threshold_datetime", -1);
		if (dateThreshold > .0) {
			result.add(new DatePairProcessor(stats, dateThreshold));
		}
		double emailThreshold = config.getDoubleProperty("similarity_threshold_email", -1);
		if (emailThreshold > .0) {
			result.add(new EmailPairProcessor (stats,emailThreshold));
		}

		double hashtagThreshold = config.getDoubleProperty("similarity_threshold_hashtag", -1);
		if (hashtagThreshold > .0) {
			result.add(new HashTagPairProcessor (stats,hashtagThreshold));
		}

		if (!config.getProperty("short_string_comparison").equals("NONE")) {
			result.add(new ShortStringPairProcessor(stats, config.getDoubleProperty("similarity_threshold_levenshtein"),
					config.getProperty("short_string_comparison").equals("EQUAL"))); 
		}
		if (!config.getProperty("long_string_comparison").equals("NONE")) {
			result.add(new LongStringPairProcessor(stats, config.getDoubleProperty("similarity_threshold_jaccard"),
					config.getProperty("long_string_comparison").equals("EQUAL")));
		}
		return result.build();
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.function.Consumer;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;

/**
 * Implementation of a JSON DataSource.
 *
 */
public class JSONDataSource2 extends OrderedTreeDataSource implements Serializable {

	int shortenedLabels = 0;
	int maxShortenedLabels = 1000;
	public static final Logger log = Logger.getLogger(JSONDataSource2.class);
	/**
	 * Instantiates a new JSON data source.
	 *
	 * @param f          the item factory
	 * @param identifier the identifier
	 * @param uri        the URI
	 * @param            graphStats, StatisticsCollector extractStats
	 */
	public JSONDataSource2(Factory f, int identifier, URI uri, URI origURI, StatisticsCollector graphStats,
                           StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.JSON_V2, graphStats, extractStats, graph);
	}

	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Scanner scanner = new Scanner(localURI.toURL().openStream())) {
			while (scanner.hasNextLine()) {
				contentBuilder.append(scanner.nextLine()).append('\n');
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		String jsonString = contentBuilder.toString();
		Set<Edge> edges = new LinkedHashSet<>();
		Node top = null;

		try {
			JSONObject jso = new JSONObject(jsonString);
			top = this.traverseJSONObject(jso, "1", "", edges, processor, false);
		} catch (JSONException e1) {
			try {
				JSONArray jso = new JSONArray(jsonString);
				top = this.traverseJSONArray(jso, "", "1", "", edges, processor, true, false);
			} catch (JSONException e2) {
				throw new IllegalStateException(e2);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Edge edge = this.buildLabelledEdge(this.root, top, Edge.ROOT_LABEL);
		processor.accept(edge);
		this.processEdgesToLocalAndOriginalURIs(processor);
		// last thing:
		this.finalizeSummaryComputation();
	}
	
	/**
	 * Traverses a JSON object.
	 *
	 * @param jso            the json object
	 * @param dew            the parent's Dewey code
	 * @param path           the parent's path
	 * @param edges          the edges
	 * @param processor      the edge processor to be used on newly traversed edges.
	 * @return the JSON node being traversed.
	 */
	private Node traverseJSONObject(JSONObject jso, String dew, String path, Set<Edge> edges, Consumer<Edge> processor, boolean isAnObjectInArray) throws IOException {
//		log.info("traverseJSONObject(" + jso + ", " + path+")");
		Iterator<String> names = jso.keys();
		int currDew = 1;
		Node objectNode = null;
		objectNode = this.buildEmptyLabelNodeOfType(JSON_STRUCT);
//		log.info("created object node: " + objectNode);
		String objectPath;
		if(isAnObjectInArray){
			objectPath = path  + ".";
		} else {
			objectPath = path;
		}
		objectNode.setLabel(objectPath); // to avoid empty labels for maps, we set their label to their path
//				log.info("recordSummaryNode(" + objectNode + ", " + objectPath + ")");
		this.recordSummaryNode(objectNode, objectPath);
		while(names.hasNext()) {
			String fieldName = names.next();
			Object fieldValue = jso.get(fieldName);
//			log.info("working on field \"" + fieldName + "\": \"" + fieldValue + "\"");
			Node childNode = null;
			String currDews = makeDewElement(dew, currDew);
			String fieldPath = "";
			fieldPath = fieldName;
			// need to act differently depending on value's type
			if(!(fieldValue instanceof JSONObject) && !(fieldValue instanceof JSONArray)) {
//				log.info(fieldValue + " is not an object nor an array");
				String value = fieldValue == JSONObject.NULL ? "" : String.valueOf(fieldValue);
				Node fieldNode = this.buildLabelNodeOfType(fieldName, JSON_STRUCT);
				Node valueNode = this.buildLabelNodeOfType(value, JSON_VALUE);

				Edge edge = this.buildUnlabelledEdge(objectNode, fieldNode);
				Edge edge2 = this.buildUnlabelledEdge(fieldNode, valueNode);
				processor.accept(edge);
				processor.accept(edge2);
				// record the field node as an EC
//						log.info("recordSummaryNode(" + fieldNode + ", " + fieldPath + ")");
				this.recordSummaryNode(fieldNode, fieldPath);
				if(fieldPath.isEmpty()) {
//							log.info("recordSummaryEdge(" + edge + ", " + fieldPath+"." + ")");
					this.recordSummaryEdge(edge, fieldPath+".");
				} else {
//							log.info("recordSummaryEdge(" + edge + ", " + objectPath + ")");
					this.recordSummaryEdge(edge, objectPath);
				}
				// record the value node as a value EC (on fieldName#val)
//						log.info("recordSummaryNode(" + valueNode + ", " + fieldName+ this.labelValues + ")");
				this.recordSummaryNode(valueNode, fieldName+ this.labelValues);
//						log.info("recordSummaryEdge(" + edge2 + ", " + fieldName + ")");
				this.recordSummaryEdge(edge2, fieldName);
				currDew += 1;
			} else {
				if (fieldValue instanceof JSONObject) {
//					log.info(fieldValue + " is an object");
					childNode = this.traverseJSONObject((JSONObject) fieldValue, currDews, fieldPath, edges, processor, false);
				} else if (fieldValue instanceof JSONArray) {
//					log.info(fieldValue + " is an array");
					childNode = this.traverseJSONArray((JSONArray) fieldValue, fieldName, currDews, fieldPath, edges, processor, false, true);
				}
				childNode.setLabel(fieldName);

				Edge edgeObjectToChild = this.buildUnlabelledEdge(objectNode, childNode);
				processor.accept(edgeObjectToChild);
				edges.add(edgeObjectToChild);
//				log.info(edgeObjectToChild);
				currDew += 1;
				if(objectPath.isEmpty() || objectPath.endsWith(".")) {
//							log.info("recordSummaryEdge(" + edgeObjectToChild + ", " + objectPath + ")");
					this.recordSummaryEdge(edgeObjectToChild, objectPath);
				} else {
//							log.info("recordSummaryEdge(" + edgeObjectToChild + ", " + objectNode.getLabel() + ")");
					this.recordSummaryEdge(edgeObjectToChild, objectNode.getLabel());
				}
			}
		}
		return objectNode;
	}

	/**
	 * Traverses a JSON array.
	 *
	 * @param jsa            the json array
	 * @param dew            the parent's Dewey code
	 * @param path           the parent's path
	 * @param edges          the edges
	 * @param processor      the edge processor to be used on newly traversed edges.
	 * @return the JSON node being traversed.
	 */
	private Node traverseJSONArray(JSONArray jsa, String arrayName, String dew, String path, Set<Edge> edges, Consumer<Edge> processor, boolean isRoot, boolean isArrayInObjectField) throws IOException {
//		log.info("traverseJSONArray(" + jsa + ", " + path + ")");
		int currDew = 1;
		int len = jsa.length();
		Node arrayNode = this.buildLabelNodeOfType(arrayName, JSON_STRUCT);
//		log.info("created array node: " + arrayNode);
		String arrayPath;
		if(isRoot || isArrayInObjectField) {
			arrayPath = path;
		} else {
			arrayPath = path + ".";
		}
//			log.info("recordSummaryNode(" + arrayNode + ", " + arrayName + ")");
		this.recordSummaryNode(arrayNode, arrayName);

		for (int k = 0; k < len; k++) {
			Object arrayElement = jsa.get(k);
//			log.info("working on array element: " + arrayElement);
			Node childNode = null;
			String currDews = makeDewElement(dew, currDew);
			String arrayElementPath;
			arrayElementPath = arrayPath;
			if (!(arrayElement instanceof JSONObject) && !(arrayElement instanceof JSONArray)) {
//				log.info(arrayElement + " is not an object nor an array");
				String value = arrayElement == JSONObject.NULL ? "" : String.valueOf(arrayElement);
				this.createValueNodes(arrayNode, "", this.getSentences(value), currDew, currDews, arrayElementPath, edges, processor);
				currDew += 1;
			} else {
				if (arrayElement instanceof JSONObject) {
//					log.info(arrayElement + " is an object");
					childNode = this.traverseJSONObject((JSONObject) arrayElement, currDews, path, edges, processor, true);
				} else if (arrayElement instanceof JSONArray) {
//					log.info(arrayElement + " is an array");
					arrayElementPath = arrayPath+".";
					childNode = this.traverseJSONArray((JSONArray) arrayElement, "", currDews, arrayElementPath, edges, processor, false, false);
				}

//				log.info(arrayElement);
//				log.info(childNode);

				Edge edgeArrayNodeToChild = this.buildUnlabelledEdge(arrayNode, childNode);
				processor.accept(edgeArrayNodeToChild);
				edges.add(edgeArrayNodeToChild);
//				log.info("created edgeArrayNodeToChild " + edgeArrayNodeToChild);
				currDew += 1;
				if(childNode.getLabel().isEmpty()) {
//						log.info("recordSummaryEdge(" + edgeArrayNodeToChild + ", " + arrayNode.getLabel() + ")");
					this.recordSummaryEdge(edgeArrayNodeToChild, arrayNode.getLabel());
				} else {
//						log.info("recordSummaryEdge(" + edgeArrayNodeToChild + ", " + path + ")");
					this.recordSummaryEdge(edgeArrayNodeToChild, path);
				}
			}
		}
		return arrayNode;
	}

	/**
	 * Creates one or several value nodes and attaches them to a given node.
	 */
	private void createValueNodes(Node node, String currName, List<String> sentences, int currDew, String dew, String path, Set<Edge> edges, Consumer<Edge> processor) throws IOException {
//		log.info("create value nodes for " + node + " with currName = " + currName);
		String currDews = dew;
		String currPath = path;
		//log.info("Value(s) on " + path);
		for (String val : sentences) {
			Node child = null; 
			if (val.startsWith("file:")) {
				child = this.createOrFindValueNodeWithAtomicity(val, this.makeLocalKey(currDews, currPath, val), this,
						RDF_URI);
			} else {
				child = this.createOrFindValueNodeWithAtomicity(val, this.makeLocalKey(currDews, currPath, val), this,
						Node.Types.JSON_VALUE);
			}
			Edge edge = null;
			if (!isNullOrEmpty(currName)) {
				edge = this.buildLabelledEdge(node, child, currName);
			} else {
				edge = this.buildUnlabelledEdge(node, child);
			}
			edge.setDataSource(this);
			processor.accept(edge);
			edges.add(edge);
			currDew += 1;
			currDews = makeDewElement(dew, currDew);
			currPath = makePathElement(path, null);

			// record the value node as a value EC (on label#val)
//				log.info("recordSummaryNode(" + child + ", " + node.getLabel()+labelValues + ")");
			this.recordSummaryNode(child, node.getLabel()+labelValues);
//				log.info("recordSummaryEdge(" + edge + ", " + node.getLabel() + ")");
			this.recordSummaryEdge(edge, node.getLabel());
		}
	}


	@Override
	public void postprocess(Graph graph) {
		// do whatever ¯\_(ツ)_/¯
	}

	@Override
	public String getContext(Graph g, Node n) {		
		boolean fromTweet = false; 
		String context = "";
		// if then node comes from a tweet, return the tweet URI	
		String query = "select n1.label from nodes n1, nodes n2, edges e " 
				+ " where n2.ds=" + n.getDataSource().getID()
				+ " and n2.type = 0 and n2.label like '%tweet%' " 
				+ " and n2.ds = n1.ds " + " and e.target=n1.id "
				+ " and e.label = 'expanded_url'" 
				+ "  and n1.label like 'https://twitter.com/%'";

		// log.info(query);
		try (Statement stm = ConnectionManager.getMasterConnection().createStatement();
				ResultSet result = stm.executeQuery(query)) {
			if (result.next()) {
				context = result.getString(1);
				fromTweet = true; 
			}
		} catch (SQLException e) {
			log.error("Cannot open a connection to fetch the content of this JSON. "
					+ "The default context will be printed out.");
		}

		if (fromTweet) {
			return "This node comes from a tweet: <a target='_blank' href='" + context + "'>" + context + "</a>.";
		}
		StringBuffer sb = new StringBuffer();
		if (n.getType() == Node.Types.JSON_STRUCT) {// map or array
			Collection<Edge> outgoing = g.getKOutgoingEdgesPostLoading(n, Graph.maxEdgesReadForContext);
			for (Edge e: outgoing) {
				if (e.getLabel().length() == 0) { // empty label --> this is an array
					sb.append("JSON array <b>" + n.getLabel() + "</b>");
				}
				else {
					sb.append("JSON map"); 
				}
				break; 
			}
		} else { // JSON value: attribute value or list element
			Collection<Edge> incoming = g.getKIncomingEdgesPostLoading(n, 1); // just 1 parent, otherwise confusing
			for (Edge e: incoming) {
				if (e.getLabel().length() == 0) {
					sb.append("JSON string <b>" + n.getLabel() + "</b> in an array");
				}
				else {
					sb.append("JSON value <b>" + n.getLabel() + "</b> of property <b>" +
							e.getLabel() + "</b> in a map"); 
				}
				break; 
			}
		}
		sb.append(" from JSON doc: <i>" + n.getDataSource().getLocalURI()+ "</i>");
		return new String(sb);
	}

}

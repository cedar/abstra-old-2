/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.CAND_PAIRS_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.HITS_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.SAMEAS_T;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.PersonOccurrenceNode;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public abstract class CachedSimilarPairProcessor 
			implements SimilarPairProcessor, BiFunction<String, String, Double>  {
	
	/** The logger. */
	protected static final Logger log = Logger.getLogger(CachedSimilarPairProcessor.class);

	/** The similarity cache size. */
	final int simCacheSize = Config.getInstance().getIntProperty("similarity_cache");

	/** The similarity cache for all but people pairs. */
	final Cache<Pair<String, String>, Double> simCache; 

	/** The similarity cache for people pairs. */
	final Cache<Pair<PersonOccurrenceNode, PersonOccurrenceNode>, Double> personSimCache; 

	private final StatisticsCollector stats;

	protected final double threshold;
	
	protected transient AtomicInteger count = new AtomicInteger(0);
	
	protected CachedSimilarPairProcessor(StatisticsCollector stats, double th) {
		this.stats = stats;
		this.threshold = th;
		this.simCache = CacheBuilder.newBuilder()
			.recordStats()
			.maximumSize(simCacheSize)
			.initialCapacity(simCacheSize / 5)
			.build();
		this.personSimCache = CacheBuilder.newBuilder()
				.recordStats()
				.maximumSize(simCacheSize)
				.initialCapacity(simCacheSize / 5)
				.build();
	}
	

	private double maybeCachedScore(String a, String b) {
		String first = a;
		String second = b;
		int comparison = a.compareTo(b);
		if (comparison == 0) {
			return 1.0;
		}
		if (comparison > 0) {
			first = b;
			second = a;
		}
		if (simCacheSize > 0 ) {
			try {
				return this.simCache.get(Pair.of(first, second), ()->apply(a, b));
			} catch (ExecutionException e) {
				throw new IllegalStateException(e);
			}
		}
		return apply(a, b);
	}
	
	public double threshold() {
		return this.threshold;
	}

	@Override
	public void process(Graph g, 
			Collection<DataSource> from, Collection<DataSource> to, Consumer<Edge> processor) {
		String tKey = StatisticsCollector.total();
		String pKey = this.getClass().getSimpleName();
		stats.resume(pKey); // start counting the time for this PairProcessor
		stats.resume(tKey);
		g.enumeratePairs(from, to, selector(), e->{
			Node source = e.getSourceNode();
			Node target = e.getTargetNode();
			//log.info("Comparing: " + source.getLabel() + " and " + target.getLabel());
			//log.info("Comparing types "+ source.getType()+" "+target.getType());
			//log.info("Source: " + source.getLabel() + " " + source.getClass().getSimpleName() +
			//		 " and ID: " + source.shortID());
			//log.info("Target: " + target.getLabel() + " " + target.getClass().getSimpleName() +
			//		 " and ID: " + target.shortID());
			double score = 0.0;
			// IM 1/3/2020: added special treatment here
			if (source instanceof PersonOccurrenceNode && target instanceof PersonOccurrenceNode) {
				PersonOccurrenceNode p1 = (PersonOccurrenceNode) source;
				PersonOccurrenceNode p2 = (PersonOccurrenceNode) target; 
				score = PersonPairProcessor.getPersonSimilarity(p1, p2); 
			}
			else {
				score = maybeCachedScore(source.getNormalizedLabel(), target.getNormalizedLabel());	
			}
			if (score >= threshold) {
				//log.info("\nWill connect: " + source.getType() + " " + source.getLabel() + " " +
				//		source.shortID() + " and " + target.getType() + " " + target.getLabel() + " " + target.shortID()); 		
				DataSource sds = source.getDataSource();
				DataSource tds = target.getDataSource();
				stats.increment(pKey, HITS_NO); // count the pair, for both keys
				stats.increment(tKey, HITS_NO);
				//log.info("Source dataset that will build sameAs edge: " + sds.getClass().getSimpleName());
				//log.info("Edge 1");
				processor.accept(sds.buildSameAsEdge(source, target, score));
				if (score == 1.0) { // IM 7/2/20:  we don't need weak same as edges in both directions
					//log.info("Edge 2");
					processor.accept(tds.buildSameAsEdge(target, source, score));
					//stats.increment(pKey, SAMEAS_NO);
					//stats.increment(sKey, SAMEAS_NO);
				}

				//DecimalFormat numberFormat = new DecimalFormat("#.00");
				//if (score < 1.0) {
				//	log.info("Obtained similarity: " + source.getLabel() + " " +
				//		source.shortID() + " -- sim (" + (numberFormat.format(score)) + ")--> " + target.getLabel() + " " + target.shortID()); 
				//}
			}
			stats.increment(pKey, CAND_PAIRS_NO); // count the candidate pair, for both keys
			stats.increment(tKey, CAND_PAIRS_NO);
			count.incrementAndGet();
		});

		stats.tick(pKey, SAMEAS_T); // count the sameAs time, for both keys
		stats.tick(tKey, SAMEAS_T);
		stats.pause(tKey);
		stats.pause(tKey);
	}
}

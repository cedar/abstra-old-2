/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;

/**
 * A node that represents the entire data source, used for the relationship <code>origDS</code>
 * 
 * A dataset node can be characterized by two attributes:
 * - the original URL (where this came from), e.g., http://monde.fr/article.html
 * - the local URL (where this was in the local file system when we loaded it), e.g., 
 *   /Users/ioanamanolescu/article.html
 * Until today (21/04/20)
 * - the label attribute inherited from Item (through Node) has stored the local URL
 * - the original URL is not stored.
 * The necessary refactoring is to take the original and local URIs out of the DataSourceNode and make them URI nodes.
 * 
 * @author Mihu, Ioana Manolescu
 */
public class DataSourceNode extends Node {
	
	/**
	 * Create a new node representing a data source
	 * @param id the node ID
	 * @param ds the data source
	 */
	public DataSourceNode(NodeID id, DataSource ds) {
		super(id, "", ds, null, Node.Types.DATA_SOURCE); // label used to be: ds.getLocalURI().toString()
	}
	public DataSourceNode(NodeID id, DataSource ds, String origURL) {
		super(id, "", ds, null, Node.Types.DATA_SOURCE); // label used to be: ds.getLocalURI().toString()
	}
	@Override
	void specificNormalization() {
		// nop
	}
}

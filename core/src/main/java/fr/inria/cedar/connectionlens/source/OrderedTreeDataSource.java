package fr.inria.cedar.connectionlens.source;

import java.io.*;
import java.net.URI;
import java.nio.file.Paths;
import java.util.HashMap;

import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class OrderedTreeDataSource extends TreeDataSource {


	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(OrderedTreeDataSource.class);

	// the next five attributes are for computing the Dataguide in case we want to abstract and are piggy-backing some summarization on data registration
	String datasetName;
	BufferedWriter collectionsNodes;
	BufferedWriter collectionsLabels;
	BufferedWriter collectionsEdges;
	String nodesFileName;
	String labelsFileName;
	String edgesFileName;
	RelationalGraph graph;
	HashMap<Integer, HashMap<Integer, String>> collectionsEdgesMap;
	String labelValues = "#val";
	boolean isXml = false;
	boolean isRdf = false;

	public OrderedTreeDataSource(Factory f, int id, URI localURI, URI origURI, Types t, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, id, localURI, origURI, t, graphStats, extractStats, graph);
		this.graph = (RelationalGraph) graph;
		this.contextMap = new HashMap<>();
		this.revContextMap = new HashMap<>();
		this.collectionsEdgesMap = new HashMap<>();
		this.isXml = localURI.toString().endsWith(".xml");
		this.isRdf = this.getLocalURI().toString().endsWith(".nt");

		// we build the dataguide if:
		// - we are creating an abstraction
		// - and we do not just compute the normalization (in this case building dataguide is not necessary because we do not build the summary)
		String filenameTmp = (localURI.toString().startsWith("file:")) ? localURI.toString().substring("file:".length()) : localURI.toString(); // remove the 'file:' prefix
		this.datasetName = filenameTmp.substring(Math.max(0, filenameTmp.lastIndexOf(System.getProperty("file.separator")) + 1), Math.max(0, filenameTmp.lastIndexOf("."))); // between the last / (or \) and the last dot position
		this.nodesFileName = Paths.get(Config.getInstance().getProperty("temp_dir"), "collections-nodes-" + this.datasetName + ".txt").toString();
		this.edgesFileName = Paths.get(Config.getInstance().getProperty("temp_dir"), "collections-edges-" + this.datasetName + ".txt").toString();
		this.labelsFileName = Paths.get(Config.getInstance().getProperty("temp_dir"), "collections-labels-" + this.datasetName + ".txt").toString();

		// reset the files only if we start the whole abstraction (with registration)
		if(!Experiment.noResetFirst) {
			try {
				log.info("reinitialize nodes and path files for summary!");
				this.collectionsNodes = new BufferedWriter(new FileWriter(this.nodesFileName));
				this.collectionsLabels = new BufferedWriter(new FileWriter(this.labelsFileName));
				this.collectionsEdges = new BufferedWriter(new FileWriter(this.edgesFileName));
			} catch (IOException ioe) {
				throw new IllegalStateException(ioe.toString());
			}
			log.info("Saving (collection id, collection label) pairs in " + this.labelsFileName + ", (collection id, node id) pairs in " + this.nodesFileName + " (collection id source, label, collection id target) triples in " + this.edgesFileName);
		}
	}

	public String getDatasetName() { return this.datasetName; }

	public String getNodesFileName() { return this.nodesFileName; }

	public String getLabelsFileName() { return this.labelsFileName; }

	public String getEdgesFileName() { return this.edgesFileName; }
	
	void recordSummaryNode(Node n, String path){
//		log.info("record node " + n + " on path " + path);
		String pathForNode = path;
		Integer pathNo = this.contextMap.get(pathForNode);
		if (pathNo == null) {
			pathNo = this.contextMap.size(); // We start path at id 0 (useful for abstraction during reporting)
			this.contextMap.put(pathForNode, pathNo);
			this.revContextMap.put(pathNo, pathForNode);
			try {
				String pathIdEntry = pathNo + "\t" + pathForNode;
//				log.info(pathIdEntry);
				this.collectionsLabels.write(pathIdEntry+"\n", 0, (pathIdEntry+"\n").length());
			} catch(IOException ioe) {
				throw new IllegalStateException(ioe.toString());
			}
		}
		try {
			String pathNodeEntry = pathNo + "\t" + n.getId().value();
//			log.info(pathNodeEntry);
			this.collectionsNodes.write(pathNodeEntry+"\n", 0, (pathNodeEntry+"\n").length());
		} catch(IOException ioe){
			throw new IllegalStateException(ioe.toString());
		}
	}

	void recordSummaryEdge(Edge e, String path) throws IOException {
//		log.info("recording new summary edge: " + e + " on " + path);
		String childLabel;
		String pathWithChild = null;
		if(e.getTargetNode().typeEquals(Node.getValueTypesAsList(), Node.Types.RDF_URI)) {
			// this cannot be RDF since we are in OrderedTreeDatasource
			// so RDF_URI is a value node type and not a structural node type for XML and JSON data
			childLabel = path + this.labelValues;
			pathWithChild = path + this.labelValues;
		} else {
			childLabel = e.getTargetNode().getLabel();
			if(childLabel.startsWith("@") && this.isXml) {
				// for XML attributes, we register with the parent label (@id -> item@id)
				// we check that we deal with an XML datasource, in case of a JSON datasource contains @ in its labels too
				pathWithChild = path + childLabel;
			} else {
				pathWithChild = childLabel;
			}
		}

		HashMap<Integer, String> newEdgeToBeInserted;
		if(!this.collectionsEdgesMap.containsKey(this.contextMap.get(path))) {
			newEdgeToBeInserted = new HashMap<>();
		} else {
			newEdgeToBeInserted = this.collectionsEdgesMap.get(this.contextMap.get(path));
			if(this.collectionsEdgesMap.get(this.contextMap.get(path)).containsKey(this.contextMap.get(pathWithChild))) {
				return; // we already have this summary edge so we stop here
			} else {
				this.collectionsEdgesMap.put(this.contextMap.get(path), newEdgeToBeInserted);
			}
		}
//		log.info(this.summaryEdgesMap);
//		log.info(this.contextMap);

		if(childLabel.startsWith("@") && this.isXml) {
			// For XML attributes, we register with the parent label + attribute name (author@id)
			newEdgeToBeInserted.put(this.contextMap.get(pathWithChild), pathWithChild);
		} else {
			newEdgeToBeInserted.put(this.contextMap.get(childLabel), childLabel);
		}
		String str;
		if(childLabel.isEmpty()) {
			str = this.contextMap.get(path) + "\t" + this.contextMap.get(path+".");
		} else {
			if(childLabel.startsWith("@") && this.isXml) {
				// for XML attributes, we have registered parentLabel@attributeName
				str = this.contextMap.get(path) + "\t" + this.contextMap.get(pathWithChild);
			} else {
				str = this.contextMap.get(path) + "\t" + this.contextMap.get(childLabel);
			}
		}
//			log.info(str);
		if(str.contains("null")) {
			log.info("recording new summary edge: " + e + " on " + path);
			log.info("path = '" + path + "' ; pathWithChild = '" + pathWithChild + "'");
			log.info(this.contextMap);
			log.info(str);
		}
		this.collectionsEdges.write(str+"\n", 0, (str+"\n").length());
		this.collectionsEdgesMap.put(this.contextMap.get(path), newEdgeToBeInserted);
	}

	void finalizeSummaryComputation() {
		try {
			this.collectionsNodes.close();
			this.collectionsLabels.close();
			this.collectionsEdges.close();
		} catch(IOException ioe) {
			throw new IllegalStateException(ioe.toString());
		}
	}
}

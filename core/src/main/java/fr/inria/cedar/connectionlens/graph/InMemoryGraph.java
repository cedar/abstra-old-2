package fr.inria.cedar.connectionlens.graph;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.cache.Cache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;

import fr.inria.cedar.connectionlens.extraction.ExtractionPolicy;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import fr.inria.cedar.connectionlens.graph.Item.Types;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.sim.NodePairSelector;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class InMemoryGraph implements Graph {

	HashMap<ItemID, Node> nodes;
	HashMultimap<Node.Types, Node> nodesByType;
	HashMap<ItemID, Edge> edges;
	HashMap<Node, Set<Edge>> outgoingEdges;
	HashMap<Node, Set<Edge>> incomingEdges;
	HashMap<Node, Node> otherRepresentatives;
	HashMap<Node, Node> representsOthers;

	HashMap<Node, Set<Edge>> outgoingWeakSameAs;

	Graph originalGraph;

	DataSourceCatalog catalog;

	private final Logger log = Logger.getLogger(InMemoryGraph.class);

	public InMemoryGraph(Graph g) {
		log.info("Loading graph in memory");
		this.originalGraph = g;

		if (!(originalGraph instanceof RelationalGraph)) {
			throw new IllegalStateException("We only know how to build this from a RelationalGraph");
		}
		RelationalGraph relG = (RelationalGraph) g;
		nodes = new HashMap<>();
		nodesByType = HashMultimap.create();
		edges = new HashMap<>();

		outgoingEdges = new HashMap<Node, Set<Edge>>();
		incomingEdges = new HashMap<Node, Set<Edge>>();

		otherRepresentatives = new HashMap<Node, Node>();
		representsOthers = new HashMap<Node, Node>();

		this.catalog = g.getCatalog();

		// getting the nodes first
		PreparedStatement stmt = relG.getPreparedStatement("SELECT id, ds, type, label, normalabel, representative FROM nodes");
		HashMap<NodeID, NodeID> representativePatchesNeeded = new HashMap<>();
		try {
			ResultSet rs = relG.executeQuery(stmt);
			while (rs.next()) {
				int thisNID = rs.getInt(1);
				NodeID nID = relG.getFactory().parseNodeID(thisNID);
				DataSource ds = catalog.getEntry(rs.getInt(2));
				Node.Types nodeType = Node.Types.values()[Integer.valueOf(rs.getString(3))];
				String reconstitutedLabel = rs.getString(4).replaceAll("''", "'");
				String reconstitutedNormaLabel = rs.getString(5).replaceAll("''", "'");
				int repNID = rs.getInt(6);
				NodeID repID = relG.getFactory().parseNodeID(repNID);

				Node n = null;
				if (thisNID != repNID) { // n is not represented by itself
					final Node theRep = (Node) nodes.get(repID);
					if (theRep == null) { // we have not seen the representative before n
						representativePatchesNeeded.put(nID, repID);
						// for now, self-represent n; we will need to patch it later
						n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, null, nodeType);
					} else { // we have retrieved from the database n's representative, before n
						n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, () -> theRep, nodeType);
					}
				} else { // n is self-represented, use a self-representing constructor
					n = ds.rebuildNodeFromStore(nID, reconstitutedLabel, reconstitutedNormaLabel, null, nodeType);
				}
				// record the information of this node, anyway
				nodes.put(nID, n);
				nodesByType.put(n.getType(), n);
				if (!n.getRepresentative().equals(n)) {
					otherRepresentatives.put(n, n.getRepresentative());
					representsOthers.put(n.getRepresentative(), n);
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		for (NodeID nID: representativePatchesNeeded.keySet()) {
			NodeID repID = representativePatchesNeeded.get(nID);
			nodes.get(nID).updateRepresentative(nodes.get(repID));
		}

		// getting the edges
		stmt = relG
				.getPreparedStatement("SELECT e.id, e.ds, e.type, e.source, e.target, e.label, e.confidence, " +
						" s.specificity FROM edges e, edge_specificity s WHERE e.id=s.id");
		try {
			ResultSet rs = relG.executeQuery(stmt);
			while (rs.next()) {
				EdgeID eID = relG.getFactory().parseEdgeID(rs.getInt(1));
				DataSource ds = catalog.getEntry(rs.getInt(2));
				Edge.Types eType = Edge.Types.values()[Integer.valueOf(rs.getString(3))];
				NodeID sID = relG.getFactory().parseNodeID(rs.getInt(4));
				Node source = nodes.get(sID);
				NodeID tID = relG.getFactory().parseNodeID(rs.getInt(5));
				Node target = nodes.get(tID);
				String eLabel = rs.getString(6).replaceAll("''", "'");
				double confidence = rs.getDouble(7);
				double specificity = rs.getDouble(8);
				Edge e = ds.buildEdge(eID, source, target, eType, eLabel, confidence, null);
				e.setSpecificityValue(specificity);
				// inserting this edge into our own structures:
				Set<Edge> sOut = outgoingEdges.get(source);
				if (sOut == null) {
					sOut = new HashSet<Edge>();
					outgoingEdges.put(source, sOut);
				}
				sOut.add(e);
				Set<Edge> sIn = incomingEdges.get(target);
				if (sIn == null) {
					sIn = new HashSet<Edge>();
					incomingEdges.put(target, sIn);
				}
				sIn.add(e);
			}
		}
		catch(SQLException e) {
			throw new IllegalStateException(e);
		}

		// TODO treat the sameAs edges like the other two above
		outgoingWeakSameAs = new HashMap<>();
		for (Edge w : g.getSameAs(0.0)) {
			Node source = w.getSourceNode();
			Node target = w.getTargetNode();
			Set<Edge> wSource = outgoingWeakSameAs.get(source);
			if (wSource == null) {
				wSource = new HashSet<Edge>();
				outgoingWeakSameAs.put(source, wSource);
			}
			wSource.add(w);
		}
		//log.info("Loaded!");
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	@Override
	public void createNodeLabelPrefixIndex() {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDisambiguatedIndex() {
		// TODO Auto-generated method stub

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public Node resolveNode(NodeID id) {
		return nodes.get(id);
	}

	@Override
	public Edge resolveEdge(EdgeID id) {
		return edges.get(id);
	}

	@Override
	public Node resolveDataSource(int id) {
		return null;
	}

	@Override
	public int nextID() {
		throw new IllegalStateException("not implemented here");
	}

	@Override
	public void setIDCounter(Boolean reset) {
		throw new IllegalStateException("not implemented here");
	}

	@Override
	public Specificity resolveSpecificity(Node n1, Node n2, String label) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Set<Edge> getEdgesBetweenSets(Collection<Node> oldNodes, Collection<Node> newNodes) {
		Set<Edge> result = new HashSet<Edge>();
		for (Node n1 : newNodes) {
			Set<Edge> outgoingN1 = outgoingEdges.get(n1);
			for (Edge e : outgoingN1) {
				Node n2 = e.getTargetNode();
				if (oldNodes.contains(n2) || newNodes.contains(n2)) {
					result.add(e);
				}
			}
			Set<Edge> incomingN1 = incomingEdges.get(n1);
			for (Edge e : incomingN1) {
				Node n2 = e.getSourceNode();
				if (oldNodes.contains(n2) || newNodes.contains(n2)) {
					result.add(e);
				}
			}
		}
		return result;
	}

	@Override
	public Set<Node> getNodes(String label, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Node> getNodes(DataSource ds, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Node> getNodes(DataSource ds, String label, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Edge> getEdges(Node n1, Node n2, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Edge> getEdges(String label, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Edge> getEdges(DataSource ds, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Edge> getEdges(DataSource ds, String label, Types... types) {
		throw new Error("not implemented yet");
	}

	@Override
	public void setType(NodeID id, fr.inria.cedar.connectionlens.graph.Node.Types t) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Node> getDisambiguatedNodes(DataSource ds) {
		throw new Error("not implemented yet");
	}

	@Override
	public String getDisambiguatedID(String label) {
		throw new Error("not implemented yet");
	}

	@Override
	public String getDisambiguatedForTheNode(Node node) {
		throw new Error("not implemented yet");
	}

	@Override
	public Set<Node> getNodes(fr.inria.cedar.connectionlens.graph.Node.Types nodeType) {
		return nodesByType.get(nodeType);
	}

	@Override
	public Set<Edge> getOutgoingEdges(Node n, Types... types) {

		HashSet<Edge> result = new HashSet<>();
		if (outgoingEdges.get(n) == null) {
			return result;
		}
		if (types == null || types.length == 0) {
			return outgoingEdges.get(n);
		}

		for (Edge e : outgoingEdges.get(n)) {
			Item.Types eType = (Item.Types) e.getType();
			for (Item.Types t : types) {
				if (eType == t) {
					result.add(e);
				}
			}
		}
		log.info(result.size() + " outgoing edges out of " + n);
		return result;
	}

	@Override
	public Set<Edge> getIncomingEdges(Node n, Types... types) {
		// log.info("Incoming edges in " + n + " with " +
		// ((types!=null)?types.length:"no") +
		// " types");
		HashSet<Edge> result = new HashSet<>();
		if (incomingEdges.get(n) == null) {
			return result;
		}
		if (types == null || types.length == 0) {
			return incomingEdges.get(n);
		}

		for (Edge e : incomingEdges.get(n)) {
			Item.Types eType = (Item.Types) e.getType();
			for (Item.Types t : types) {
				if (eType == t) {
					result.add(e);
				}
			}
		}
		// log.info(result.size() + " incoming edges in " + n);
		return result;
	}

	@Override
	public Node resolveEntityNode(String entityNodeLabel) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Node resolveNodeByLabel(String nodeLabel) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Boolean computeSimilarDisambiguatedEntities(Consumer<Edge> processor) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public IndexAndProcessNodes index() {
		return originalGraph.index();
	}

	public Set<Edge> getWeakSameAs(Node from) {
		Set<Edge> possibleResult = outgoingWeakSameAs.get(from);
		return (possibleResult == null) ? (new HashSet<>()) : possibleResult;
	}

	@Override
	public Set<Edge> getWeakSameAs(Node from, double threshold) {
		HashSet<Edge> result = new HashSet<Edge>();
		Set<Edge> possibleResult = outgoingWeakSameAs.get(from);
		if (possibleResult == null) {
			return result;
		}
		for (Edge e : possibleResult) {
			if (e.getSpecificityValue() >= threshold) {
				result.add(e);
			}
		}
		return result;
	}

	@Override
	public Set<Edge> getWeakSameAs(Node from, DataSource ds, double threshold) {
		HashSet<Edge> result = new HashSet<Edge>();
		Set<Edge> possibleResult = outgoingWeakSameAs.get(from);
		if (possibleResult == null) {
			return result;
		}
		for (Edge e : possibleResult) {
			if (e.getDataSource().equals(ds) && e.getSpecificityValue() >= threshold) {
				result.add(e);
			}
		}
		return result;
	}

	@Override
	public Set<Edge> getSameAs(Node from, double threshold) {
		return getWeakSameAs(from, threshold); // TODO this misses equivalence edges
	}

	@Override
	public Set<Edge> getSameAs(Node from, DataSource ds, double threshold) {
		return getWeakSameAs(from, ds, threshold); // TODO this misses equivalence edges
	}

	@Override
	public Set<Edge> getSameAs(double threshold) {
		HashSet<Edge> result = new HashSet<Edge>();
		for (Node n : outgoingWeakSameAs.keySet()) {
			for (Edge e : outgoingWeakSameAs.get(n)) {
				if (e.getSpecificityValue() >= threshold) {
					result.add(e);
				}
			}
		}
		return result;
	}

	@Override
	public Pair<Double, Double> getSpecificityStat() {
		return originalGraph.getSpecificityStat();
	}

	@Override
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, Double threshold, Types... types) {

		HashSet<Edge> result = new HashSet<>();
		if (outgoingEdges.get(n) == null) {
			return result;
		}
		for (Edge e : outgoingEdges.get(n)) {
			if (e.getSpecificityValue() >= threshold) {
				if (types == null || types.length == 0) {
					result.add(e);
				} else {
					if (types.length > 0) {
						for (int i = 0; i < types.length; i++) {
							if (e.getType() == types[i]) {
								result.add(e);
							}
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, Double threshold, Types... types) {
		HashSet<Edge> result = new HashSet<>();
		if (incomingEdges.get(n) == null) {
			return result;
		}
		for (Edge e : incomingEdges.get(n)) {
			if (e.getSpecificityValue() >= threshold) {
				if (types == null || types.length == 0) {
					result.add(e);
				} else {
					if (types.length > 0) {
						for (int i = 0; i < types.length; i++) {
							if (e.getType() == types[i]) {
								result.add(e);
							}
						}
					}
				}
			}
		}
		return result;
	}

	public Set<Edge> pickKMostSpecificEdges(Set<Edge> edges, Integer k, Types... types) {
		HashSet<Edge> result = new HashSet<Edge>();
		TreeMultimap<Double, Edge> specEdges = TreeMultimap.create(Ordering.natural().reverse(), Ordering.natural());
		for (Edge e : edges) {
			specEdges.put(e.getSpecificityValue(), e);
		}
		Iterator<Double> sortedSpecs = specEdges.keySet().iterator();
		while (sortedSpecs.hasNext()) {
			if (result.size() == k) {
				return result;
			}
			Double d = sortedSpecs.next();
			Set<Edge> de = specEdges.get(d);
			for (Edge e : de) {
				if (types == null || types.length == 0) {
					result.add(e);
				} else {
					if (types.length > 0) {
						for (int i = 0; i < types.length; i++) {
							if (e.getType() == types[i]) {
								result.add(e);
							}
						}
					}
				}
				if (result.size() == k) {
					return result;
				}
			}
		}
		return result;
	}

	@Override
	public Set<Edge> getKOutgoingEdgesPostLoading(Node n, Integer k, Types... types) {
		if (outgoingEdges.get(n) == null) {
			return new HashSet<>();
		}
		return pickKMostSpecificEdges(outgoingEdges.get(n), k, types);
	}

	@Override
	public Set<Edge> getKIncomingEdgesPostLoading(Node n, Integer k, Types... types) {
		if (incomingEdges.get(n) == null) {
			return new HashSet<>();
		}
		return pickKMostSpecificEdges(incomingEdges.get(n), k, types);
	}

	@Override
	public NodeID getOriginalNodeID(Node n) {
		throw new IllegalStateException("Abstract graphs should not be InMemory, originalNodeID not supported");
	}

	@Override
	/** This method's code is asymmetric. The default implementation in Graph is symmetric.
	 * TODO check more about completeness.*/
	public Set<Edge> getSameAsBetweenSets(Collection<Node> oldNodes, Collection<Node> newNodes, double threshold) {
		Set<Edge> result = new HashSet<Edge>();
		for (Node n1 : newNodes) {
			Set<Edge> outgoingN1 = outgoingWeakSameAs.get(n1);
			for (Edge e : outgoingN1) {
				Node n2 = e.getTargetNode();
				if (oldNodes.contains(n2) || newNodes.contains(n2)) {
					result.add(e);
				}
			}
		}
		return result;
	}

	@Override
	/**
	 * This method's code is asymmetric. The default implementation in Graph is
	 * symmetric. TODO check more about completeness.
	 */
	public Set<Edge> getSameAs(Node from) {
		if (outgoingWeakSameAs.get(from) == null) {
			return new HashSet<Edge>();
		}
		return outgoingWeakSameAs.get(from);
	}

	@Override
	public boolean isAbstract() {
		return false;
	}

	@Override
	public boolean isSummary() {
		return false;
	}

	@Override
	public boolean isClassified() {
		return false;
	}

	@Override
	public boolean isNotOriginal() {
		return false;
	}

	@Override
	public boolean isNormalized() {
		return false;
	}

	@Override
	public boolean isOriginal() {
		return false;
	}

	@Override
	public Graph getAbstractGraph() {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Graph getNormalizedGraph() {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Graph getClassifiedGraph() {
		return null;
	}

	@Override
	public Graph getSummaryGraph() {
		return null;
	}

	@Override
	public GraphType getGraphType() {
		return null;
	}

	@Override
	public void updateAbstractGraph(RelationalGraph g) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void updateSummaryGraph(RelationalGraph g) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void updateNormalizedGraph(RelationalGraph g) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void updateClassifiedGraph(RelationalGraph g) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void updateGraph(RelationalGraph g, GraphType graphType) {
		throw new IllegalStateException("not implemented yet");

	}

	@Override
	public void enumeratePairs(Collection<DataSource> from, Collection<DataSource> to, NodePairSelector selector,
			Consumer<Edge> processor) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public boolean generateEdgeSpecificityComputeMeanStdDev(StatisticsCollector statisticsCollector) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public int countEdges(DataSource ds, Types... types) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public int countSameAs(DataSource ds) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public int countNodes(DataSource ds, Types... types) {
		return this.getNodes(ds, types).size();
	}

	@Override
	public Factory getIDFactory() {
		return originalGraph.getIDFactory();
	}

	@Override
	public GraphSession openSession(int batchSize) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public GraphSession openSession(int batchSize, Function<String, int[]> f, StatisticsCollector graphStats,
			StatisticsCollector extractStats, StatisticsCollector simStats) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public GraphSession openSession(int batch, StatisticsCollector graphStats, StatisticsCollector extractStats,
			StatisticsCollector simStats) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public HashMap<NodeID, String> getDisambiguatedEntitiesNode() {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Node getNodeFromCacheOrStorage(String sLabel) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void addToNodeCache(String oLabel, Node objectNode) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public boolean isOnDisk(NodeID nodeID) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public void setOnDisk(NodeID nodeID) {
		throw new IllegalStateException("not implemented yet");

	}

	@Override
	public void addToDisambiguatedEntities(NodeID nodeID, String url) {
		throw new IllegalStateException("not implemented yet");

	}

	@Override
	public ExtractionPolicy extractPolicy() {
		return originalGraph.extractPolicy();
	}

	@Override
	public void countEdgesForSpecificity() {
		originalGraph.countEdgesForSpecificity();
	}

	@Override
	public void setCatalog(DataSourceCatalog catalog) {
		throw new IllegalStateException("not implemented yet");

	}

	@Override
	public DataSourceCatalog getCatalog() {
		return originalGraph.getCatalog();
	}

	@Override
	public Cache<NodeID, Node> nodeCache() {
		return originalGraph.nodeCache(); 
	}

	@Override
	public Cache<NodeID, Set<Edge>> sameAsCache() {
		return originalGraph.sameAsCache(); 
	}

	@Override
	public int sameAsCacheSize() {
		return originalGraph.sameAsCacheSize(); 
	}

	@Override
	public Cache<EdgeID, Edge> edgeCache() {
		return originalGraph.edgeCache();
	}

	@Override
	public Cache<Pair<Node, String>, Specificity> inDegreeCache() {
		return originalGraph.inDegreeCache(); 
	}

	@Override
	public Cache<Pair<Node, String>, Specificity> outDegreeCache() {
		return originalGraph.outDegreeCache();
	}

	@Override
	public int nodeCacheSize() {
		return originalGraph.nodeCacheSize();
	}

	@Override
	public int edgeCacheSize() {
		return originalGraph.edgeCacheSize();
	}

	@Override
	public Set<Edge> getOutgoingEdges(Node n, String[] labels, Types... types) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Set<Edge> getIncomingEdges(Node n, String[] labels, Types... types) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, String[] labels, Double threshold, Types... types) {
		throw new IllegalStateException("not implemented yet");
	}

	@Override
	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, String[] labels, Double threshold, Types... types) {
		throw new IllegalStateException("not implemented yet");
	}

}

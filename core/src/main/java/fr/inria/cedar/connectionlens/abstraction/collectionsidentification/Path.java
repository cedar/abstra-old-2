package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Objects;

public class Path implements Comparable<Path> {
    public static final Logger log = Logger.getLogger(Path.class);

    private ArrayList<CollectionEdge> path;
    private double pathTransferFactor;

    public Path(Path path) {
        this.path = new ArrayList<>();
        this.path.addAll(path.getPath()); // we need a deep copy to avoid modifying path and p2 at the same time
        this.pathTransferFactor = path.getPathTransferFactor();
    }

    public Path() {
        this.path = new ArrayList<>();
        this.pathTransferFactor = 1.0d;
    }

    public void addEdge(CollectionEdge e) {
        if(!this.path.contains(e)) {
            this.path.add(e);
        }
    }

    public String toStringLabel() {
        String result = "";
        for(CollectionEdge ce : this.path) {
            result += CollectionGraph.getInstance().getCollectionLabel(ce.getSource()) + ".";
        }
        result += CollectionGraph.getInstance().getCollectionLabel(this.getLastCollection());
        return result;
    }

    public String toStringLabelWithoutFirstAndLast() {
        String result = "";
        int counter = 0;
        for(CollectionEdge ce : this.path) {
            if(counter < this.getCollectionsInPath().size()-2 && counter >= 0) { // we want to remove two elements (the first and the last) -> < size-2
                result += CollectionGraph.getInstance().getCollectionLabel(ce.getTarget()) + ".";
                counter++;
            }
        }
        result = (!result.isEmpty() ? result.substring(0, result.length()-1) : ""); // remove last dot
        return result;
    }

    public double getPathTransferFactor() {
        return this.pathTransferFactor;
    }

    public void setPathTransferFactor(double pathTransferFactor) {
        this.pathTransferFactor = pathTransferFactor;
    }

    /**
     * Get the set of collections (no duplicates) that are in the path
     * @return the set of collections that are in the path
     */
    public ArrayList<Integer> getCollectionsInPath() {
        ArrayList<Integer> collectionsInPath = new ArrayList<>();
        if(this.path != null) {
            if(!this.path.isEmpty()) {
                collectionsInPath.add(this.path.get(0).getSource());
            }
            for(CollectionEdge se : this.path) {
                collectionsInPath.add(se.getTarget());
            }
        }
        return collectionsInPath;
    }

    public int getLastCollection() {
        CollectionEdge lastCollectionEdge = this.getLastCollectionEdge();
        if(lastCollectionEdge.getComesFromXMLRelation()) {
            // we ignore collection edges that have been added for XML
            return lastCollectionEdge.getSource();
        } else {
            return lastCollectionEdge.getTarget();
        }
    }

    public ArrayList<Integer> getAllCollectionsExceptLast() {
        ArrayList<Integer> collectionsIds = this.getCollectionsInPath();
        collectionsIds.remove(collectionsIds.size()-1); // remove the last collection
        return collectionsIds;
    }

    public CollectionEdge getLastCollectionEdge() {
        return this.path.get(this.path.size()-1);
    }

    /**
     * Returns true if the path contains an entity collection in its inside collections (i.e. not the first source, not the last target)
     * Useful for reporting relationships between main entity collections.
     * @return
     */
    public boolean containsEntityCollectionsInPath() {
        // can be merged with "does not contain collection in set"
        int i = 0;
        for(CollectionEdge ce : this.path) {
            i++;
            if(i < this.path.size()) { // we don't check the last edge
                if(CollectionGraph.getInstance().getMainCollections().contains(ce.getTarget())) {
                    return true;
                }
            }
        }
        return false;
    }

    public Integer getFirstCollectionOfPath() {
        if(!this.path.isEmpty()) {
            return this.path.get(0).getSource();
        } else {
            return null;
        }
    }

    public boolean firstCollectionOfPathIsEntity(ArrayList<Integer> entityCollections) {
        return entityCollections.contains(this.getFirstCollectionOfPath());
    }

    public Integer getLastCollectionOfPath() {
        if(!this.path.isEmpty()) {
            return this.path.get(this.path.size()-1).getTarget();
        } else {
            return null;
        }
    }

    public boolean lastCollectionOfPathIsEntity(ArrayList<Integer> entityCollections) {
        return entityCollections.contains(this.getLastCollectionOfPath());
    }

    public boolean isAtMostOne() {
        // returns true if all the edges along the path are atMostOne
        for(CollectionEdge ce : this.path) {
            if(!ce.getIsAtMostOne()) {
                return false;
            }
        }
        return true;
    }

    public boolean doesNotContainCollectionInSet(ArrayList<Integer> forbiddenCollections) {
        // we check that the path does not contain collection of the forbidden list,
        // except for that source and the target of the path
        for(CollectionEdge ce : this.path) {
            if(ce == this.path.get(0)) {
                if(forbiddenCollections.contains(ce.getTarget())) {
//                    log.debug(ce + " contains an element in " + forbiddenCollections);
                    return false;
                }
            } else if(ce == this.path.get(this.path.size()-1)) {
                 if(forbiddenCollections.contains(ce.getSource())) {
//                    log.debug(ce + " contains an element in " + forbiddenCollections);
                    return false;
                 }
            } else if(forbiddenCollections.contains(ce.getSource()) || forbiddenCollections.contains(ce.getTarget())) {
//                log.debug(ce + " contains an element in " + forbiddenCollections);
                return false;
            }
        }
        return true;
    }

    public boolean doesNotContainCyclicEdges(boolean workingGraph) {
        if(workingGraph) {
            return this.path.stream().noneMatch(e -> CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(e));
        } else {
            return this.path.stream().noneMatch(e -> CollectionGraph.getInstance().getInCycleCollectionEdges().contains(e));
        }
    }

    public ArrayList<CollectionEdge> getPath() {
        return this.path;
    }

    public int getSize() { return this.path.size(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Path path1 = (Path) o;
        if(this.path.size() != path1.path.size()) {
            return false;
        } else {
            // two paths are equals if they contain the same edges (in the same order)
            for(int i = 0 ; i < this.path.size() ; i++) {
                boolean equality = this.path.get(i).equals(path1.getPath().get(i));
                if(!equality) {
                    return false;
                }
            }
//            log.debug("" + this + " and " + path1 + " are equal");
            return true;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.path);
    }

    @Override
    public String toString() {
        return "Path{ptf=" + this.pathTransferFactor + ", " + this.path + '}';
    }
//    public String toString() {
//        String str = "{";
//        if(!this.path.isEmpty()) {
//            str += this.path.get(0).getSource();
//            for(CollectionEdge ce : this.path) {
//                str += "-" + ce.getTarget();
//            }
//        }
//        str += "}_(" + this.pathTransferFactor + ")";
//        return str;
//    }

    @Override
    public int compareTo(Path o) {
        if (this == o) return 0;
        return Integer.compare(this.path.size(), o.path.size());
    }
}

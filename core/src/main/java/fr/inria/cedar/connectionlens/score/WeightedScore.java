/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import static com.google.common.base.Preconditions.checkArgument;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.score.Score.DoubleScore;

/**
 * A scoring function combined the level of matching between the query and the object, as well as
 * connection score (typically <code>AverageConfidenceSpecificity</code>).
 */
public class WeightedScore implements ScoringFunction {

	private static DecimalFormat df2 = new DecimalFormat("#.##");

	/** The underlying scores. */
	private final ScoringFunction[] scores;

	/** The logger. */
	private final Logger log = Logger.getLogger(WeightedScore.class);
	
	/** The weights. */
	private final Double[] weights;
	
	/**
	 * Instantiates a new match and connection score.
	 */
	public WeightedScore(ScoringFunction[] s, Double[] w) {
		checkArgument(s.length == w.length);
		this.scores = s;
		this.weights = w;
		//log.info(toString());
	}
	
	/**
	 * Compute the score of the answer tree, then the result is stored in <code>this.score</code>.
	 * Always call this function before calling <code>this.score</code>
	 */
	@Override
	public DoubleScore compute(Scorable element) {
		double result = 0.0;
		long start = System.currentTimeMillis();
		//StringBuffer sb = new StringBuffer();
		for (int i = 0; i < scores.length; i++) {
			double d = ((DoubleScore) element.score(scores[i])).value(); 
			result += weights[i] * d;
			//sb.append(df2.format(weights[i]) + "*" + df2.format(d));
				if (i<scores.length - 1) {
				//sb.append(" + ");
			}
		}
		//sb.append(" = " + df2.format(result)); 
		//log.info(sb);
		return new DoubleScore(result);
	}
	
//	public String toString() {
//		StringBuffer sb = new StringBuffer();
//		sb.append("[");
//		for (int i = 0; i < scores.length; i ++) {
//			sb.append(scores[i].getClass().getSimpleName() + " " + weights[i]);
//			if (i < scores.length - 1) {
//				sb.append(", "); 
//			}
//		}
//		sb.append("]");
//		return new String(sb);
//	}
	
	public boolean equals(Object o) {
		if (o instanceof WeightedScore) {
			WeightedScore ws = (WeightedScore) o;
			if (ws.scores.length != scores.length) {
				return false; 
			}
			for (int i = 0; i < ws.scores.length; i ++) {
				if (!scores[i].equals(ws.scores[i])){
					return false; 
				}
				if (weights[i] != ws.weights[i]) {
					return false; 
				}
			}
			return true; 
		}
		else {
			return false; 
		}
	}
}

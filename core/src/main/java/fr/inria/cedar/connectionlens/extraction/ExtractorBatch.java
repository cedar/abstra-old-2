package fr.inria.cedar.connectionlens.extraction;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import fr.inria.cedar.connectionlens.graph.Node;

/**
 * Stores entries that await batch extraction.
 *
 * @author ioanamanolescu
 *
 */
public class ExtractorBatch {
	static Logger log = Logger.getLogger(ExtractorBatch.class);

	private int extractorBatchSize;
	private int lastOccupiedPosition; 
	private ArrayList<Pair<Node, String>> entries; 
	
	public ExtractorBatch(int extractorBatchSize) {
		this.entries = new ArrayList<Pair<Node, String>>();
		this.extractorBatchSize = extractorBatchSize;
		lastOccupiedPosition = -1; 
	}
	/** It is important to send the label separately as it may no longer be n.label(),
	 *  but have undergone some processing before the entry is batched **/
	public void addEntry(Node n, String currentLabel) {
		addEntry(Pair.of(n, currentLabel)); 
		//log.info("Added entry " + currentLabel + " size: " + entries.size() + "/" + extractorBatchSize);
	}
	private void addEntry(Pair<Node, String> entry) {
		this.entries.add(entry);
		lastOccupiedPosition++; 
	}
	public boolean isFull() {
		return (lastOccupiedPosition >= (extractorBatchSize-1));
	}
	public void reset() {
		lastOccupiedPosition = -1; 
		entries.clear();
	}
	public ArrayList<Pair<Node, String>> getEntries(){
		return entries;
	}
	public int usedSize() {
		return (lastOccupiedPosition+1);
	}
}

package fr.inria.cedar.connectionlens.abstraction;

import org.apache.log4j.Logger;
import java.util.*;
import java.util.stream.Collectors;

class LongestCommonPrefix {
    public static final Logger log = Logger.getLogger(LongestCommonPrefix.class);

    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("java2python");
        words.add("javaGuava");
        String longestCommonPrefix = getLongestCommonPrefix(words);
        log.debug(longestCommonPrefix);
    }

    public static String getLongestCommonPrefix(ArrayList<String> words) {
        String longestCommonPrefix;
        if (words.isEmpty()) {
            // The longest common prefix of an empty arraylist is "".
            longestCommonPrefix = "";
        } else if (words.size() == 1) {
            // The longest common prefix of an arraylist containing only one element is that element itself.
            longestCommonPrefix = words.get(0);
        } else {
            // Sort the array
            ArrayList<String> sortedWords = (ArrayList<String>) words.stream().sorted().collect(Collectors.toList());
            int length = sortedWords.get(0).length();
            StringBuilder res = new StringBuilder();
            // Comapre the first and the last strings character
            // by character.
            for(int i = 0; i < length; i++){
                // If the characters match, append the character to the result.
                if(sortedWords.get(0).charAt(i) == sortedWords.get(sortedWords.size() - 1).charAt(i)){
                    res.append(sortedWords.get(0).charAt(i));
                }
                // Else, stop the comparison.
                else{
                    break;
                }
            }
            longestCommonPrefix = res.toString();
        }
        log.debug("Longest common prefix: '" + longestCommonPrefix + "'");
        return longestCommonPrefix;
    }
}
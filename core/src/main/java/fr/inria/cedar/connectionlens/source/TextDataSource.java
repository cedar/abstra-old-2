/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Node.Types.TEXT_VALUE;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.ENT_NO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;
import java.util.function.Consumer;

import org.apache.log4j.Logger;

import com.google.common.collect.Lists;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.Entity;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Implementation of a text DataSource.
 */
public class TextDataSource extends TreeDataSource {

	/** List of entities found from text */
	private List<Entity> entities = Lists.newLinkedList();

	/** The entity extractor. */
	protected final EntityExtractor extractor;

	/**
	 * Define a new text document data source
	 * 
	 * @param f         the item factory
	 * @param id        the ID of the data source
	 * @param uri       path to the data source
	 * @param extractor the entity extractor
	 */
	public TextDataSource(Factory f, int id, URI uri, URI origURI, EntityExtractor extractor,
			StatisticsCollector graphStats, StatisticsCollector extractStats, Graph graph) {
		super(f, id, uri, origURI, Types.TEXT, graphStats, extractStats, graph);
		this.extractor = extractor;
		log = Logger.getLogger(TextDataSource.class);
	}

	/**
	 * Returns the list of entities extracted from this data source
	 * 
	 * @return list of extracted entities
	 */
	public List<Entity> getEntities() {
		return this.entities;
	}

	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		// measure indexing time
		// add the new data source to the catalog

		String textLabel = "";
		try {
			textLabel = IOUtils.slurpURL(getLocalURI().toURL());
			for (String sentence : getSentences(textLabel)) {
				String key = String.valueOf(Math.abs(root.getDataSource().getLocalURI().hashCode()));
				//key += "|" + String.valueOf(0);
				key += "|" + sentence; // String.valueOf(sentence.length());
				Node textNode = createOrFindValueNodeWithAtomicity(sentence, key, this, TEXT_VALUE);

				Edge rootTextEdge = buildLabelledEdge(root, textNode, Edge.ROOT_LABEL);
				processor.accept(rootTextEdge);
				processEdgesToLocalAndOriginalURIs(processor);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		extractStats.put(this.localURI, ENT_NO, entities.size());
	}

	
	@Override
	public void postprocess(Graph graph) {
		// Do whatever ¯\_(ツ)_/¯
	}

	public String getContextForKeywords(Graph g, Node n, List<String> keywords, IndexAndProcessNodes index,
			int tolerence) {
		String context = "";
		StringBuilder sb = new StringBuilder();
		Boolean replaceAtLeastOne = false;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(getLocalURI().toURL().openStream(), "UTF-8"))) {
			String line;
			while ((line = br.readLine()) != null) {
				context += line; // read the entire text
			}
			br.close();
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
			return "";
		}

		int length = context.length();
		for (String kw : keywords) {
			sb.append(getContextForOneKW(kw, context, index, tolerence) + "\n");
		}

		if (sb.toString().isEmpty()) {
			return context;
		}

		return sb.toString();
	}

	private String getContextForOneKW(String kw, String context, IndexAndProcessNodes index, int tol) {
		String updateContext = "";
		String textLabel = context;
		int tolerance = tol;
		int exactOffset = context.indexOf(kw); // remove spaces after ' e.g. "d' Allianz" --> "d'Allianz"
		if (exactOffset != -1) {
			// if the offset and the entity label is correct as shown in the text, show only
			// a fragment of text
			textLabel = textLabel.substring(0, exactOffset) + "<b>" + kw + "</b>"
					+ textLabel.substring(exactOffset + kw.length());
			int begin = exactOffset - tolerance >= 0 ? exactOffset - tolerance : 0;
			int end = exactOffset + kw.length() + tolerance > textLabel.length() ? textLabel.length()
					: exactOffset + kw.length() + tolerance;
			updateContext = textLabel.substring(begin, end);
			updateContext = "..." + updateContext + "...";
		} else {
			String stem = index.getStemOfString(kw);
			if (!stem.isEmpty()) {
				stem = stem.replaceAll("'", "");
				int exactOffset_1 = textLabel.toLowerCase().indexOf(stem); // remove spaces after ' e.g. "d' Allianz"
																			// --> "d'Allianz"
				// log.info(textLabel.toLowerCase());
				// log.info(exactOffset_1);

				if (exactOffset_1 != -1) {
					textLabel = textLabel.substring(0, exactOffset_1) + "<b>" + stem + "</b>"
							+ textLabel.substring(exactOffset_1 + stem.length());
					int begin = exactOffset_1 - tolerance >= 0 ? exactOffset_1 - tolerance : 0;
					int end = exactOffset_1 + stem.length() + tolerance > textLabel.length() ? textLabel.length()
							: exactOffset_1 + stem.length() + tolerance;
					updateContext = textLabel.substring(begin, end);
					updateContext = "..." + updateContext + "...";
				} else {
					updateContext = "";
				}

			}
		}
		return updateContext;
	}

	public String getContext(Graph g, Node n) {
		StringBuffer sb = new StringBuffer();
		String context = ""; 
		
		if (n.getType() == TEXT_VALUE) {
			String s = n.getLabel();
			return "Text: <b>" + s.substring(0, Math.min(s.length() - 1, 1000)) + "...</b> from text document <i> " + n.getDataSource().getLocalURI() + "</i>" ;
		}

		return context;
	}
}

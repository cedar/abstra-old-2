/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.MRG_T;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.SQL_T;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.SRC_T;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.ProvenanceSignature.Provenance;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.AnswerTreeDOTPrinting;
import fr.inria.cedar.connectionlens.util.SolutionTimeCurve;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.Stoppers;

/**
 * Global search algorithm - building exhaustive trees.
 */
public class GAMSearchExhaustive extends GAMSearch implements CLGSTKFunction{

	/** The logger. */
	private final static Logger log = Logger.getLogger(GAMSearchExhaustive.class);

	boolean isPathQuery = true;

	// the set of all the solutions found
	// for a given edge set, at most one tree is stored, because
	// GAMAnswerTree.equals ignores the root
	Set<GAMAnswerTree> solutions = new HashSet<GAMAnswerTree>();

	// Memory of queue.
	private HashSet<Pair<GAMAnswerTree, Edge>> queueHistory = new HashSet<Pair<GAMAnswerTree, Edge>>();
	// 30/1/21: check what happens for G2R (when nodes are represented by other
	// nodes)

	// 07/02/2022: Used to store and check merge candidates.
	private Map<Node, PriorityQueue<GAMAnswerTree>> answerTreesByRoot; // Used for a 3-kwd query to store all ATs which are not solutions.

	private Map<Integer, Set<Node>> leaves;
	private Map<Integer, Set<Node>> leafRepresentatives;
	protected Map<Node, Integer> invLeaves;

	private Set<Integer> typeSelectorIndices = new HashSet<Integer>();

	private boolean oneEntityMatchPerText = Config.getInstance().getBooleanProperty("one_entity_match_per_text");

	private long startTime; // used to record the time until each solution

	private int maxMatchesPerKwd = Config.getInstance().getIntProperty("max_matches_per_kwd");
	
	private boolean bidirectionalSearch = Config.getInstance().getBooleanProperty("gam_bidirectional_search");

	private String edgeFilters = Config.getInstance().getProperty("edge_filter");

	protected Map<Node, Set<Edge>> adjacencyLocalCache = new LinkedHashMap<>();

	protected Stoppers<AnswerTree> searchStoppers;

	private boolean growToRep = true;
	private boolean getSpecific;

	private AnswerTreeDOTPrinting treeDrawer;

	boolean drawAllTrees = false; // set this to true for debugging purposes only


	/**
	 * Allows to filter the edges we explore (ignore any data edge whose specificity
	 * is below this threshold
	 */
	protected double minSpecificity;

	private boolean queryHasAtLeastOneTypeSelector = false;

	private Object statsObj; // Identifier to send to StatisticsCollector.

	private ProvenanceSignatureStatistics pss;

	Predicate<AnswerTree> soluChecker = at -> isSolutionToQuery(at);

	/** This constructor needed in order for Explore to extend GAMSearch */
	public GAMSearchExhaustive() {
		setStoppers(); // don't set min specificity here as this is called
		// by Exploration with a null graph
	}

	// this is used only in tests
	public GAMSearchExhaustive(IndexAndProcessNodes i, Graph g, ScoringFunction sf, StatisticsCollector st) {
		this(i, g, sf, st, false);
		setMinSpecificity();
	}

	/**
	 * Resets all internal collections and search memory
	 * 
	 * @param q
	 */
	private void resetState(Object statsObj) {
		leaves = new LinkedHashMap<>();
		leafRepresentatives = new LinkedHashMap<>();
		invLeaves = new LinkedHashMap<>();
		answerTreesByRoot = new LinkedHashMap<>();
		adjacencyLocalCache = new LinkedHashMap<>();
		treeDrawer = new AnswerTreeDOTPrinting(statsObj);
		solutions = new HashSet<GAMAnswerTree>();
		this.statsObj = statsObj;
		stats.start(statsObj);
		pss=new ProvenanceSignatureStatistics();
	}

	// this is used by Experiment and by the KeywordSearchServlet
	public GAMSearchExhaustive(IndexAndProcessNodes i, Graph g, ScoringFunction sf, StatisticsCollector st,
			boolean queryStrategy) {
		super(i, g, sf, st);
		this.getSpecific = queryStrategy;
		setStoppers();
		setMinSpecificity();
	}

	/**
	 * GAMSearch algorithm.
	 *
	 * @param q         the query
	 * @param processor the processor to use on each answer tree found for the given
	 *                  query
	 */
	@Override
	public void run(Query q, Consumer<AnswerTree> processor) { // , Stoppers<AnswerTree> stoppers) {
		// log.info("BINARY run method called"); , stoppers is: " +
		// stoppers.getClass().getName());
		resetState(q);

		if (q.components.size() <= 2) {
			isPathQuery = true;
			log.info(q.components.size() + " query components, " + (isPathQuery ? "" : " not") + " a path query");
		}
		else
			isPathQuery = false;
		if (q.components.get(0).toString().equals("explore")) {
			performExploration(q, processor);
		} else {

			prepareInputs(q);

			// log.info("After initialization:");
			// gampq.printQueue();
			findTrees(processor);

		}
	}

	private void prepareInputs(Query q) {
		this.startTime = System.currentTimeMillis();

		createQueue();
		// populate leaves: query component --> nodes
		int index = 0;
		for (QueryComponent queryComponent : q.getComponents()) {
			log.info("Component: " + queryComponent + " of class: " + queryComponent.getClass().getSimpleName());
			Set<Node> nodes = getNodeMatchesAndFilter(queryComponent);
			StringBuffer sb = new StringBuffer();
			sb.append(nodes.size() + ((nodes.size() == 1) ? " match" : " matches") + " for " + queryComponent);

			Set<Node> allNodes = new HashSet<>(nodes);
			// 28/4/21: building leafRepresentatives
			Set<Node> allNodeReps = new HashSet<>();
			for (Node n : allNodes) {
				allNodeReps.add(n.getRepresentative());
			}
			// leaves and leafRepresentatives are assumed to "have seen" queryComponent (or
			// not), on sync
			if (leaves.containsKey(index)) {
				leaves.get(index).addAll(allNodes);
				leafRepresentatives.get(index).addAll(allNodeReps);
			} else {
				leaves.put(index, allNodes);
				leafRepresentatives.put(index, allNodeReps);
			}
			// Build the invLeaves
			for (Node node : allNodes) {
				if (invLeaves.containsKey(node)) {
					Integer oldVal = invLeaves.get(node);
					Integer newVal = oldVal | (1 << Integer.valueOf(index));
					invLeaves.put(node, newVal);
				} else {
					invLeaves.put(node, (1 << Integer.valueOf(index)));
				}
			}
			log.info(new String(sb));
			index++;
		}
	}

	public void prepareInputs(List<Set<Node>> inputs, Integer statsObj) {

		resetState(statsObj);

		if (inputs.size() <= 2) {
			isPathQuery = true;
			log.info(inputs.size() + " input components, " + (isPathQuery ? "" : " not") + " a path query");
		}
		else
			isPathQuery=false;

		this.startTime = System.currentTimeMillis();

		createQueue();

		// Fill in leaves, invLeaves and leafRepresentatives from the sets of nodes.

		int index = 0;
		for (Set<Node> nodes : inputs) {

			StringBuffer sb = new StringBuffer();
			sb.append(nodes.size() + ((nodes.size() == 1) ? " match" : " matches") + " for " + index);

			Set<Node> allNodes = new HashSet<>(nodes);
			// 28/4/21: building leafRepresentatives
			Set<Node> allNodeReps = new HashSet<>();
			for (Node n : allNodes) {
				allNodeReps.add(n.getRepresentative());
			}
			// leaves and leafRepresentatives are assumed to "have seen" queryComponent (or
			// not), on sync
			if (leaves.containsKey(index)) {
				leaves.get(index).addAll(allNodes);
				leafRepresentatives.get(index).addAll(allNodeReps);
			} else {
				leaves.put(index, allNodes);
				leafRepresentatives.put(index, allNodeReps);
			}
			// Build the invLeaves
			for (Node node : allNodes) {
				if (invLeaves.containsKey(node)) {
					Integer oldVal = invLeaves.get(node);
					Integer newVal = oldVal | (1 << Integer.valueOf(index));
					invLeaves.put(node, newVal);
				} else {
					invLeaves.put(node, (1 << Integer.valueOf(index)));
				}
			}
			log.info(new String(sb));
			index++;
		}
	}

	public void findTrees(Consumer<AnswerTree> processor) {
		createInitialTreesFromMatches(processor);
		search(processor);
		// log.info("GAMSearch finished, queue size is: " + this.Q.size());
	}

	/** Here, GAMSearch's stoppers are set from the configuration. */
	protected void setStoppers() {
		Config config = Config.getInstance();
		resetStoppers(config.getLongProperty("search_stopper_timeout"), config.getLongProperty("search_stopper_topk"),
				config.getLongProperty("search_stopper_stationarity"));
	}

	/** Actual stopper initialization */
	public void resetStoppers(long to, long firstk, long stationarity) {
		Predicate<AnswerTree[]> result = (a) -> false;
		if (to > 0) {
			log.info("Search stopper: run for at most " + to + " ms.");
			result = result.or(Stoppers.timeout(to));
		}
		if (firstk > 0) {
			log.info("Search stopper: find at most " + firstk + " results.");
			final SortedSet<AnswerTree> resultSet = new ConcurrentSkipListSet<>(
					new AnswerTree.Comparator(this.scoring()));
			result = result.or(Stoppers.topK(firstk, resultSet));
		}
		if (stationarity > 0) {
			log.info("Search stopper: maximum allowed time lapse between two results is " + stationarity + " ms.");
			result = result.or(Stoppers.stationarity(stationarity));
		}
		this.searchStoppers = Stoppers.forward(result);
	}

	protected void setMinSpecificity() {
		// log.info(this.getClass().getSimpleName() + " setMinSpecificity, graph is
		// null: " + (this.graph == null));
		try {
			Pair<Double, Double> specStats = this.graph.getSpecificityStat();
			this.minSpecificity = specStats.getLeft() / 100;
			// set the threshold at 1% the avg spec
			// sample spec distribution:
			// https://gitlab.inria.fr/cedar/connection-lens/-/issues/421#note_428167
		} catch (Exception e) {// the above block may fail if spec stats have not been computed
			e.printStackTrace();
			this.minSpecificity = 0.05; // in this case just pick a value
		}
		log.info("Min specificity is: " + minSpecificity);
	}

	protected Set<Edge> getCachedAdjacentEdges(Node lastVisitedNode) {
		stats.tick(this.statsObj, SRC_T);
		Set<Edge> result = adjacencyLocalCache.get(lastVisitedNode);
		if (result == null && this.getSpecific == false) {
			adjacencyLocalCache.put(lastVisitedNode, (result = getAdjacentEdgesToNode(lastVisitedNode)));
		} else if (result == null && this.getSpecific == true) {
			adjacencyLocalCache.put(lastVisitedNode, (result = getSpecificAdjacentEdgesToNode(lastVisitedNode)));
		}
		stats.tick(this.statsObj, SQL_T);
		return result;
	}

	// Get the adjacent edges from the last visited node
	protected Set<Edge> getAdjacentEdgesToNode(Node node) {
		if(this.bidirectionalSearch) {
			if(this.edgeFilters.length()==0)
				return Sets.union(graph.getWeakSameAs(node), graph.getAdjacentEdges(node));
			else
				return filterEdges(graph.getAdjacentEdges(node));

		}
		else {
			if(this.edgeFilters.length()==0)
				return Sets.union(graph.getWeakSameAs(node), graph.getIncomingEdges(node));
			else
				return filterEdges(graph.getIncomingEdges(node));
		}
	}

	protected Sets.SetView<Edge> getSpecificAdjacentEdgesToNode(Node node) {
		// Pair<Double, Double> p = graph.getSpecificityStat();
		if(this.bidirectionalSearch) {
			if(this.edgeFilters.length()==0)
				return Sets.union(graph.getWeakSameAs(node),
						Sets.union(graph.getSpecificIncomingEdgesPostLoading(node, this.minSpecificity),
								graph.getSpecificOutgoingEdgesPostLoading(node, this.minSpecificity)));
			else
				return filterEdges(Sets.union(graph.getSpecificIncomingEdgesPostLoading(node, this.minSpecificity),
						graph.getSpecificOutgoingEdgesPostLoading(node, this.minSpecificity)));
		}
		else {
			if(this.edgeFilters.length()==0)
				return Sets.union(graph.getWeakSameAs(node),
						graph.getSpecificIncomingEdgesPostLoading(node, this.minSpecificity));
			else
				return filterEdges(graph.getSpecificIncomingEdgesPostLoading(node, this.minSpecificity));
		}
	}

	private Sets.SetView<Edge> filterEdges(Set<Edge> edges) {
		Set<Edge> filteredEdges = new HashSet<Edge>();
		
		String[] edgeFiltersList = this.edgeFilters.split(";");

		for(Edge e: edges) {
			if(ArrayUtils.contains(edgeFiltersList, e.getLabel()))
				filteredEdges.add(e);
		}
		return Sets.union(filteredEdges, new HashSet<Edge>());
	}

	public Integer getKwdsMatchedBy(Node node) {
		// new method: using invLeaves
		Integer res = 0;
		if (invLeaves.containsKey(node)) {
			res = invLeaves.get(node);
		}
		return res;
	}

	/**
	 * When this is called, either answerTree is *known* to be a solution, or we may
	 * not know whether it is one (and still need to test). The distinction is made
	 * in isSolution (false means maybe). Solutions post-processed; necessary
	 * processing is applied; non-solutions are enqueued.
	 * 
	 * @param answerTree
	 * @param processor
	 * @param isSolution
	 */
	private void processAnswerTree(GAMAnswerTree answerTree, Consumer<AnswerTree> processor, // Query q,
			boolean isSolution) {
		// log.info("");
		// log.info("processAnswerTree");

		// we either process the solution
		// or, we inject it back into the rest of the search:

		boolean solution = isSolution;
		boolean isNew = isNew(answerTree);
		if (isNew) {
			
			if (soluChecker.test(answerTree)) {
				// log.info("Solution!");
				solution = true;
			}
			if (solution) {
				boolean usefulSolution = !solutions.contains(answerTree); 
				if (usefulSolution) {
					solutions.add(answerTree);
					processor.accept(answerTree);
					if (searchStoppers.test(answerTree)) {
						log.info("Maximum number of solutions reached");
						throw new ConditionReachedException("TOPK");
					}

					if (queryHasAtLeastOneTypeSelector) {
						// IM, Jan 2021: uncomment this to obtain query suggestions
						// log.info("Solution of " + answerTree.edges().size() + " edges: " +
						// answerTree.labelsOfMatchingNodes());
					}
					if (drawingEnabled) {
						treeDrawer.print(answerTree, answerTree.decipherProvenance() + "-final", true, scoring);
						// print the merge log history of this AT
						// try {
						// String path = Config.getInstance().getStringProperty("temp_dir");
						// path+="/";
						// String folderName = "";
						// for(QueryComponent qc: q.getComponents()){
						// if(!folderName.equals("")){
						// folderName+="_";
						// }
						// folderName+= qc.toString();
						// }
						// path+=folderName;
						// BufferedWriter br = new BufferedWriter(new FileWriter(path +
						// "/CreationHistoryofTree_" + answerTree.getNo() + ".txt"));
						// br.write(answerTree.getCreationHistory());
						// br.close();
						// } catch (IOException e){
						// log.info(e.getMessage());
						// }
					}
					if (this.drawSolutionTimes) {
						SolutionTimeCurve.recordSolutionTime(statsObj, System.currentTimeMillis() - this.startTime); 
						// TODO:
						// Madhu to fix this class  to not depend Query.
					}	
				} else { // not useful solution
					// ignore it: this is a solution that we already found, but with another
					// root
				}

			} else { // not a solution => enqueue for further processing
				processMergeCandidate(answerTree);
				if (drawAllTrees && drawingEnabled) { // TREE DRAWING FOR DEBUG
					treeDrawer.print(answerTree, (answerTree.decipherProvenance() + answerTree.encodeMatches()), false,
							this.scoring);
				}
				feedPriorityQueue(answerTree);
			}
		}

	}

	/**
	 * This method replenishes the priority queue Q based on the node, that is: - it
	 * adds (tree, root adjacent edge) pairs for GROW - it adds (tree', adjacent
	 * edge') pairs for GROW-ACROSS, where tree' is obtained by adding to answerTree
	 * a strong sameAs edge and adjacent edge' is an non-strong-same-As edge
	 * adjacent to the root of tree'
	 * 
	 * @param answerTree
	 * @param q
	 */
	@SuppressWarnings({ "unchecked", "serial" })
	private void feedPriorityQueue(GAMAnswerTree answerTree) {// , Query q) {
		Node node = answerTree.root();

		Set<Edge> adjacentEdges = getCachedAdjacentEdges(node);
		// log.info("Feeding priority queue based on: " + answerTree.getNo() + " rooted
		// in " + answerTree.root.getId());
		// int addedNo = 0;
		// Growing with data or weak sameAs edges
		for (Edge adjacentEdge : adjacentEdges) { // do not connect through such edges (#581)
			if (adjacentEdge.getLabel().equals(Edge.TABLE_EDGE_LABEL)) {
				continue;
			}
			// log.info("Considering edge: " + adjacentEdge.debugEdge());
			if (searchStoppers.test()) {
				// throw new ConditionReachedException("T/O");
				log.info("Time-out (1)");
				throw new ConditionReachedException("T/O");
			}
			Pair<GAMAnswerTree, Edge> pair = Pair.of(answerTree, adjacentEdge);
			// check that this tree and this edge did not already make a tour in the queue:

			if (queueHistory.contains(pair)) {
				// log.info("Give up 2");
				continue;
			} else {
				queueHistory.add(pair);
			}

			// check a bunch of other cases where we should NOT push this in Q
			if (adjacentEdge.getSourceNode().equals(adjacentEdge.getTargetNode()) // this edge is a loop
					|| (answerTree.nodes().contains(adjacentEdge.getTargetNode())
							&& answerTree.nodes().contains(adjacentEdge.getSourceNode()))
					|| answerTree.datasetLoop(adjacentEdge) // this edge closes a dataset loop
					|| (adjacentEdge.getLabel().equals(Edge.SAME_AS_LABEL) && answerTree.isGrownThroughSameAs())
					// we would be concatenating sameAs
					|| (!noRedundantMatch(answerTree, adjacentEdge)))// this edge would bring redundancy
				// it is important to keep this condition last
			{
				// log.info("Give up 3");
				continue;
			}
			// log.info("Pushing in Q " + answerTree.getNo() + " and edge " +
			// adjacentEdge.getId());
			QueueEntry qe = new QueueEntry(pair);
			qg.add(qe);
			// addedNo++;
		}
		/*
		 * Grow To Representative: In this part, GAM will try to create a new tree t’
		 * that is rooted in n2 (where n2 is the representative of the current root).
		 */
		if (growToRep) {
			Node representative = node.getRepresentative();
			if (!node.equals(representative)) {
				// log.info("Trying G2R from " + node.getId().value());
				Edge strongSameAsEdgeToRepresentative = node.getDataSource().buildSameAsEdge(node, representative);
				// log.info("Created edge " + strongSameAsEdgeToRepresentative.getId().value()
				// + " | " + strongSameAsEdgeToRepresentative.debugEdge());
				if (searchStoppers.test()) { // here we test just for T/O
					log.info("Time-out (2)");
					throw new ConditionReachedException("T/O");
				}
				// growToRep
				Pair<GAMAnswerTree, Edge> pair = Pair.of(answerTree, strongSameAsEdgeToRepresentative);
				// log.info("Built a G2R pair of " + answerTree.edges().size() + " edges + 1");
				// check that this tree and this edge did not already make a tour in the queue:

				if (!queueHistory.contains(pair)) {
					// log.info("Pushed G2R pair (tree " + answerTree.getNo() + " rooted in " +
					// answerTree.root().getLabel() + ", edge " +
					// strongSameAsEdgeToRepresentative.getId().value() + ")");
					queueHistory.add(pair);
					QueueEntry qe = new QueueEntry(pair);
					qg.add(qe);
					// addedNo++;

					// else {
					// log.info("Discarded it");
					// }
				}
			}
		}
		// log.info("Added: " + addedNo + ", now queue size is: " + Q.size());
	}


	/**
	 * @param answerTree
	 * @param e
	 * @return true if adding e to answerTree does not introduce redundancy between
	 *         matches, that is: if there are more than 1 matches for a kwd, then
	 *         they are all on nodes, and the nodes have the same representative.
	 */
	private boolean noRedundantMatch(GAMAnswerTree answerTree, Edge e) {
		SetMultimap<Integer, Item> matches = answerTree.matches();
		for (Integer qc : answerTree.matches().keys()) { // each qc already matched in answerTree
			if (this.typeSelectorIndices.contains(qc)) { // IM 23/1/21, to accomodate type:Person type:Person
				continue;
			}
			if ((!answerTree.nodes.contains(e.getSourceNode())) // e.s is the new proposed root
					&& leafRepresentatives.get(qc).contains(e.getSourceNode().getRepresentative())) {
				// e.s matches qc
				// check that in answerTree, it's matched in a *node* with *the same rep*
				Set<Item> items = matches.get(qc);
				for (Item i : items) {
					if (i instanceof Node) { // matched on an answerTree node
						if (!(((Node) i).getRepresentative().equals(e.getSourceNode().getRepresentative()))) {
							return false;
						}
					} else { // matched on an answerTree edge
						return false;
					}
				}
			}
			if ((!answerTree.nodes.contains(e.getTargetNode())) // e.t is the new proposed root
					&& leafRepresentatives.get(qc).contains(e.getTargetNode().getRepresentative())) {
				// check that in answerTree, it's matched in a *node* with *the same rep*
				Set<Item> items = matches.get(qc);
				for (Item i : items) {
					if (i instanceof Node) {
						if (!(((Node) i).getRepresentative().equals(e.getTargetNode().getRepresentative()))) {
							return false;
						}
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Adds answerTree to the global data structure in which we search for merge
	 * candidates.
	 * 
	 * @param answerTree
	 */
	private void processMergeCandidate(GAMAnswerTree answerTree) {
		if (!answerTreesByRoot.containsKey(answerTree.root())) {
			PriorityQueue<GAMAnswerTree> answerTrees = new PriorityQueue<>(
					Comparator.comparingInt(AnswerTree::countNodes));
			answerTrees.add(answerTree);
			answerTreesByRoot.put(answerTree.root(), answerTrees);
		} else {
			answerTreesByRoot.get(answerTree.root()).add(answerTree);
		}

	}

	protected boolean isSolutionToQuery(AnswerTree answerTree) {// , Query q) {
		// log.info("Checking if valid answer:"+answerTree.toString());
		return (answerTree.matches().keySet().size() == leaves.size());
	}

	/**
	 * Creates the initial search trees of GAM, for a given query keyword.
	 *
	 * @param processor
	 */
	private void createInitialTreesFromMatches(Consumer<AnswerTree> processor) {
		boolean coverEachNodeOnlyOnce = (!this.queryHasAtLeastOneTypeSelector);
		HashSet<Node> coveredNodes = new HashSet<Node>();
		for (int i = 0; i < this.leaves.size(); i++) {
			Set<Node> nodesForThisComponent = this.leaves.get(i);
			for (Node node : nodesForThisComponent) {
				if (searchStoppers.test()) { // here we test just for timeout
					log.info("Time-out (3)");
					throw new ConditionReachedException("T/O");
				}
				if ((coverEachNodeOnlyOnce && !coveredNodes.contains(node)) || !coverEachNodeOnlyOnce) {
					coveredNodes.add(node);
					HashSet<Integer> kwdsMatchedByThisNode = new HashSet<Integer>();
					kwdsMatchedByThisNode.add(i);
					if (!(this.typeSelectorIndices.contains(i))) { // if this component is a plain kwd, it should
						// accumulate matches
						for (int j = 0; j < this.leaves.size(); j++) {
							if (leaves.get(j).contains(node)) {
								kwdsMatchedByThisNode.add(j);
							}
						}
					}
					GAMAnswerTree initialTree = GAMAnswerTree.wrap(new AnswerTree(node, kwdsMatchedByThisNode), new ProvenanceSignature(Provenance.INIT));
					this.pss.add(new ProvenanceSignature(Provenance.INIT));

					// log.info("Initial tree for " + q + " rooted in " + node.getLabel() +
					// "(" + node.getId() +
					// ") has " + kwdsMatchedByThisNode.size() + " matches");
					processAnswerTree(initialTree, processor, false);
					if (drawAllTrees && drawingEnabled) { // TREE PRINTING FOR DEBUG
						treeDrawer.print(initialTree, "init", soluChecker.test(initialTree), scoring);
					}
				}
			}
		}
	}

	/**
	 * Merges newTree with all other compatible trees, then merges the resulting
	 * trees etc. until no new tree results from merge.
	 * 
	 * @param newTree
	 * @param q
	 * @param processor
	 * @param soluChecker
	 */
	private void mergeAll(GAMAnswerTree newTree, Consumer<AnswerTree> processor) {
		Set<GAMAnswerTree> newTreesToMerge = new HashSet<GAMAnswerTree>();
		newTreesToMerge.add(newTree);

		while (!newTreesToMerge.isEmpty()) {
			// log.info("MERGE ROUND: " + mergeRound);
			Set<GAMAnswerTree> treesToIterateOver = newTreesToMerge;
			newTreesToMerge = new HashSet<GAMAnswerTree>();

			for (GAMAnswerTree tree : treesToIterateOver) {
				Set<GAMAnswerTree> mergePartners = answerTreesByRoot.get(tree.root()) == null ? null
						: new HashSet<>(answerTreesByRoot.get(tree.root()));
				// the priority appears destroyed here, as we load into a HashSet<GAMAnswerTree>
				// TODO
				if (mergePartners != null) {
					// if(follow) {
					// log.info(mergePartners.size() + " potential merge partners");
					// }
					for (GAMAnswerTree t2 : mergePartners) {
						if (searchStoppers.test()) { // test just for timeout
							log.info("Time-out (5)");
							throw new ConditionReachedException("T/O");
						}

						Integer unionMask = (tree.keywordMask | t2.keywordMask);
						// if(follow) {
						// log.info("Potential merge partner: " + t2.debugString());
						// }
						if ((unionMask != tree.keywordMask) && (unionMask != t2.keywordMask) && (!t2.equals(tree))) {
							boolean canMerge = false;
							if (isPathQuery) { 
								canMerge = ((t2.keywordMask & tree.keywordMask) == 0)
										&& disjointNodesExceptRoot(t2, tree);
							} else {
								boolean overlappingKwds = false;
								// if at least 3 kwds, test more
								// IM, 26/11/2021:
								int tMinusRootMatches = tree.keywordMask & (~tree.rootKeywordMask);
								int t2MinusRootMatches = t2.keywordMask & (~t2.rootKeywordMask);
								overlappingKwds = ((tMinusRootMatches & t2MinusRootMatches) != 0);

								// IM, 25/11/2021:
								// in fact, if there are no equivalent nodes in the graph, we could
								// also simplify the test to use just the masks (?) TODO think more
								// for (Integer matchedT: tree.matches().keySet()) {
								// if (t2.matches().keySet().contains(matchedT)) {
								// for (Item matchingItemT: tree.matches().get(matchedT)) {
								// if (!matchingItemT.equals(tree.root())) {
								// overlappingKwds = true;
								// }
								// }
								// for (Item matchingItemT2: t2.matches().get(matchedT)) {
								// if (!matchingItemT2.equals(t2.root())) {
								// overlappingKwds = true;
								// }
								// }
								// }
								// }

								canMerge = !overlappingKwds && disjointNodesExceptRoot(t2, tree);
							}

							if (canMerge) {
								ProvenanceSignature newps = ProvenanceSignature.merge(t2.provSign(), tree.provSign(), false);
								GAMAnswerTree mergedTree = new GAMAnswerTree(AnswerTree.merge(t2, tree), t2, tree,
										newps);
								this.pss.add(newps);
								
								// GAMAnswerTree mergedTree = GAMAnswerTree.wrap(AnswerTree.merge(t2, tree, q),
								// MERGE);
								// log.info("MERGE of " + t2.getNo() + " and " + tree.getNo() + " lead to: " +
								// mergedTree.getNo());
								// to: " + mergedTree.debugString());
								if(isNew(mergedTree))
									newTreesToMerge.add(mergedTree);
								if (drawAllTrees && drawingEnabled && (mergedTree != null)) { // TREE DRAWING FOR DEBUG
									// treeDrawer.printMergeFamily(t2, tree, mergedTree, "merge", false, q,
									// scoring);
									treeDrawer.print(mergedTree, ("merge" + mergedTree.encodeMatches()), false,
											scoring);
									// log.info("MERGE(" + t2.getNo() + ", " + tree.getNo() + ")=" +
									// mergedTree.getNo());
								}
								// the call below also feeds the queue:
								processAnswerTree(mergedTree, processor, false); // this is not known to be
								// a solution
							}
						}
					}
				}
			}
		}

	}


	/**
	 * Check if an {@link AnswerTree} is new or not.
	 * @param answerTree : The AnswerTree to be checked.
	 * @return true if the tree has not been seen before, false otherwise.
	 */
	private boolean isNew(AnswerTree answerTree) {
		boolean isNew = false;


		if (answerTreesByRoot.containsKey(answerTree.root())) {
			if (answerTreesByRoot.get(answerTree.root()).contains(answerTree)) {
				isNew = false;
			} else {
				isNew = true;
			}
		} else {
			isNew = true;
		}
		if(this.solutions.contains(answerTree))
			isNew = false;
		return isNew;
	}

	/**
	 * Helper for merge
	 * 
	 * @param t1
	 * @param t2
	 * @return true if the trees have no common node except the root
	 */
	private boolean disjointNodesExceptRoot(AnswerTree t1, AnswerTree t2) {
		// when we call this, roots of t1 and t2 are equal
		for (Node n1 : t1.nodes()) {
			if (t2.nodes.contains(n1) && (!n1.equals(t1.root))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Enriches matches to reflect those the query keywords matched by target. These
	 * are retrieved from the leaves data strucure.
	 * 
	 * @param matches
	 * @param target
	 */
	private void updateAnswerTreeMatches(SetMultimap<Integer, Item> matches, Node target) {
		for (int i = 0; i < this.leaves.size(); i++) {
			if (searchStoppers.test()) {// here we test just for timeout
				log.info("Time-out (6)");
				throw new ConditionReachedException("T/O");
			}
			if (leaves.get(i).contains(target)) {
				matches.get(i).add(target);
			}
		}
	}

	/**
	 * Creates a new AT by growing oldAnswerTree with newEdge
	 * 
	 * @param oldAnswerTree
	 * @param newEdge
	 * @param target        the root of the new tree (if one is built)
	 * @param q
	 * @return
	 */
	private GAMAnswerTree getAnswerTreeFromGrow(GAMAnswerTree oldAnswerTree, Edge newEdge, Node target) {// , Query q) {
		// log.info("Trying to grow from root " +
		// oldAnswerTree.root.getId() + " with edge: " + newEdge.getId().value());

		// if (drawAllTrees && drawingEnabled) { // TREE DRAWING FOR DEBUG
		// treeDrawer.print(oldAnswerTree,
		// (oldAnswerTree.decipherProvenance() + this.encodeMatches(oldAnswerTree)),
		// false, q, scoring);
		// }
		Set<Node> nodes = new HashSet<>(oldAnswerTree.nodes());
		nodes.add(target);
		Set<Edge> edges = new HashSet<>(oldAnswerTree.edges());
		edges.add(newEdge);
		SetMultimap<Integer, Item> matches = HashMultimap.create(oldAnswerTree.matches());
		updateAnswerTreeMatches(matches, target);
		@SuppressWarnings("unchecked")
		AnswerTree at = new AnswerTree(target, nodes, edges, // getKwdsMatchedBy(target),
				matches);

		// Check if this tree has already been processed.
		if(!isNew(at))
			return null;

		

		GAMAnswerTree grownTree = null;
		if (newEdge.getLabel().equals(Edge.SAME_AS_LABEL)) {
			// log.info("Same-as edge");
			if (oldAnswerTree.isGrownThroughSameAs()) {
				return null; // we do not grow with weak same as, on top of weak same as
			}
			if (newEdge.confidence() < 1.0) {
				ProvenanceSignature newPS = ProvenanceSignature.copyOf(oldAnswerTree.provSign());
				newPS.addProvenance(Provenance.WEAK_SAME_AS);
				grownTree = new GAMAnswerTree(at, oldAnswerTree, newEdge, newPS);
				this.pss.add(newPS);
			} else {
				ProvenanceSignature newPS = ProvenanceSignature.copyOf(oldAnswerTree.provSign());
				newPS.addProvenance(Provenance.GROW_TO_REP);
				grownTree = new GAMAnswerTree(at, oldAnswerTree, newEdge, newPS);
				this.pss.add(newPS);
				// log.info("A G2R tree is born!");
			}
		} else {
			ProvenanceSignature newPS = ProvenanceSignature.copyOf(oldAnswerTree.provSign());
			newPS.addProvenance(Provenance.GROW);
			grownTree = new GAMAnswerTree(at, oldAnswerTree, newEdge, newPS);
			this.pss.add(newPS);
		}
		// log.info("GROW(" + oldAnswerTree.getNo() + ")=" + grownTree.getNo());
		if (drawAllTrees && drawingEnabled) { // TREE DRAWING FOR DEBUG
			treeDrawer.print(grownTree, (grownTree.decipherProvenance() + grownTree.encodeMatches()), false, scoring);
		}
		return grownTree;
	}

	public void collectStatsOld() {
		stats.tick(statsObj, SRC_T);
		stats.stop(statsObj);

		long allTreesNo = 0;
		long treesFromGrow = 0;
		long treesFromMerge = 0;
		long treesFromG2R = 0;
		long treesFromWSA = 0;
		long treesFromInit = 0;
		for (Node n : answerTreesByRoot.keySet()) {
			allTreesNo += answerTreesByRoot.get(n).size();
			for (GAMAnswerTree at : answerTreesByRoot.get(n)) {
				switch (at.provSign().finalProv) {
				case GROW:
					treesFromGrow++;
					break;
				case MERGE:
				case MERGESPECIAL:
					treesFromMerge++;
					break;
				case GROW_TO_REP:
					treesFromG2R++;
					break;
				case WEAK_SAME_AS:
					treesFromWSA++;
					break;
				case INIT:
					treesFromInit++; 
					break;
				default:
					break;
				}
			}
		}
		allTreesNo += solutions.size();
		for (GAMAnswerTree at : solutions) {
			switch (at.provSign().finalProv) {
			case GROW:
				treesFromGrow++;
				break;
			case MERGE:
			case MERGESPECIAL:
				treesFromMerge++;
				break;
			case GROW_TO_REP:
				treesFromG2R++;
				break;
			case WEAK_SAME_AS:
				treesFromWSA++;
				break;
			case INIT:
				treesFromInit++; 
				break;
			default:
				break;
			}
		}
		//}
		//}
		stats.put(statsObj, "ATs", allTreesNo); // actual number of distinct trees (including distinct roots)
		stats.put(statsObj, "ATs_Init", treesFromInit); 
		stats.put(statsObj, "ATs_Grow", treesFromGrow);
		stats.put(statsObj, "ATs_Merge", treesFromMerge);
		stats.put(statsObj, "ATs_G2R", treesFromG2R);
		stats.put(statsObj, "ATs_WSA", treesFromWSA);
		stats.put(statsObj, "ATs_all", GAMAnswerTree.getLastNo()); // generated trees (the same tree may be generated in
		// many
		// ways)
		// using the same or different operations, thus, may be double counting here
		if (this.drawingEnabled) {
			treeDrawer.generateSolutionLatexBook();
		}
		if (this.drawSolutionTimes) {
			SolutionTimeCurve.printSolutionTimes(statsObj, Config.getInstance().getProperty("gam_heuristics_growth"));
		}
		//stats.put(statsObj, "Queue_pairs", this.hasBeenInQueue.getSize());
		//stats.put(statsObj, "Queue_Size", (this.Q == null ? 0 : this.Q.size()));
	}

	public void collectStats() {
		stats.tick(statsObj, SRC_T);
		stats.stop(statsObj);
		//stats.put(statsObj, "ATs_not_sol", (isPathQuery? this.exploredAnswerTreesIgnoringRoot.size():getAnswerTreeByRootSize()));
		//stats.put(statsObj, "ATs_sols", this.solutions.size());
		stats.put(statsObj, "ATs_all", GAMAnswerTree.getLastNo()); // generated trees (the same tree may be generated in
		// many
		// ways)
		// using the same or different operations, thus, may be double counting here
		//stats.put(statsObj, "Queue_Size", (this.Q == null ? 0 : this.Q.size()));

		// Print count of trees with each {@link ProvenanceSignature}.
		for(String ps: this.pss.stats().keySet()) {
			String tabVal = ps.toString().replace(")", ",") + this.pss.stats().get(ps) + ")";
			stats.put(statsObj, ps.toString(), tabVal); 
		}


		if (this.drawingEnabled) {
			treeDrawer.generateSolutionLatexBook();
		}
		if (this.drawSolutionTimes) {
			SolutionTimeCurve.printSolutionTimes(statsObj, Config.getInstance().getProperty("gam_heuristics_growth"));
		}

	}

	@SuppressWarnings("unused")
	private int getAnswerTreeByRootSize() {
		int size = 0;
		for(Node n : this.answerTreesByRoot.keySet()) {
			for(GAMAnswerTree at: this.answerTreesByRoot.get(n)) {
				size++;
			}
		}
		return size;
	}

	private void performExploration(Query q, Consumer<AnswerTree> processor) {
		log.info("Exploration starts");
		for (int i = 1; i < q.components.size(); i++) {
			QueryComponent qc = q.components.get(i);
			// log.info("Query component: " + qc);
			if (qc.toString().startsWith("node:")) {
				String qcs = qc.toString();
				int colPos = qcs.indexOf(':');
				String nodeIDString = qcs.substring(colPos + 1);
				NodeID nodeID = new NumericNodeID(Integer.parseInt(nodeIDString));
				Node n = graph.resolveNode(nodeID);
				exploreFromSeed(q, n, qc, processor, searchStoppers);
			} else {
				for (Node n : getNodeMatchesAndFilter(qc)) {
					exploreFromSeed(q, n, qc, processor, searchStoppers);
				}
			}
		}
		if (drawingEnabled) {
			treeDrawer.generateSolutionLatexBook();
		}
		log.info("Exploration finished.");
		return;

	}

	protected void createQueue() {
		this.qg = new QueueGroup(this); // TODO:Madhu to remove this dependency of PQ on GAMSearch object
	}

	private void exploreFromSeed(Query q, Node n, QueryComponent qc, Consumer<AnswerTree> processor,
			Stoppers<AnswerTree> stoppers) {
		log.info("Seed: " + n + " rep: " + n.getRepresentative());
		Exploration e = new Exploration(this.graph);
		AnswerTree at = e.explore(n, qc, 30, false); // 6 is radius
		// log.info(at);
		if (at != null) {
			if (drawingEnabled) {
				treeDrawer.print(at, "explore", true, scoring);
			}
			processor.accept(at);
		}
	}

	/**
	 * All these nodes match a single query component. For each text match having at
	 * least one entity child match: - remove the text node - if we only allow 1
	 * entity child of a given text node to match, also remove all but one of its
	 * entity child matches
	 * 
	 * @param nodes
	 */
	private void filterInitialMatchSet(Set<Node> nodes) {
		// log.info("Starting with " + nodes.size() + " nodes");
		HashMultimap<Node, Node> entityChildrenPerText = HashMultimap.create();
		// int i = 0;
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				if (isEntityType(thisNode.getType())) {
					Set<Edge> adjacentThisNode = graph.getAdjacentEdges(thisNode, EXTRACTION_EDGE);
					for (Edge e : adjacentThisNode) {
						if (e.getTargetNode().equals(thisNode)) {
							if (nodes.contains(e.getSourceNode())) {
								entityChildrenPerText.put(e.getSourceNode(), thisNode);
								// log.info("Matching text " + e.getSourceNode() + " has entity child: " +
								// e.getTargetNode());
							}
						}
					}
				} else {
					// log.info(i + " not entity! " + n.getLabel());
				}
			} else {
				// log.info(i + " not a node");
			}
			// i++;
		}
		// now we know for every nodes text that is a parent of some nodes entity,
		// all the entities of whom the text is a parent
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				if (!isEntityType(thisNode.getType())) {
					Set<Node> entityChildrenOfThisNode = entityChildrenPerText.get(thisNode);
					if (entityChildrenPerText.get(thisNode) != null && entityChildrenPerText.get(thisNode).size() > 0) {
						// log.info("Removing matching text " + thisNode);
						nodes.remove(thisNode); // the text node has entity children: remove it
						if (oneEntityMatchPerText) { // if only one entity is wished for, also remove all but that
							// entity
							boolean oneAlreadyPreserved = false;
							for (Node entityChild : entityChildrenOfThisNode) {
								if (!oneAlreadyPreserved) {
									oneAlreadyPreserved = true;
								} else {
									nodes.remove(entityChild);
									// log.info("Removing its entity child " + entityChild);
								}
							}
						}
					}
				}
			}
		}
		// for each representative keep only the node with the highest ID
		HashMap<Node, Node> surviversByRep = new HashMap<Node, Node>();
		for (Item n : nodes) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				Node previousN = surviversByRep.get(thisNode.getRepresentative());
				if (previousN != null) {
					int comparison = thisNode.getId().compareTo(previousN.getId());
					if (comparison > 0) {
						surviversByRep.put(thisNode.getRepresentative(), thisNode);
					} // else leave the previous node
				} else {
					surviversByRep.put(thisNode.getRepresentative(), thisNode);
				}
			}
		}
		for (Item n : new HashSet<>(nodes)) {
			if (n instanceof Node) {
				Node thisNode = (Node) n;
				Node retainedFromThisNode = surviversByRep.get(thisNode.getRepresentative());
				if (!(retainedFromThisNode.equals(thisNode))) {
					nodes.remove(thisNode);
				}
			}
		}
		// limit the number of matches per kwd, if desired
		if (maxMatchesPerKwd > 0) {
			for (Item n : new HashSet<>(nodes)) {
				if (nodes.size() > maxMatchesPerKwd) {
					nodes.remove(n);
				}
			}
		}
	}

	/**
	 * Helper method that extracts matches from the index and *for query purposes*
	 * removes entities from among the matches.
	 * 
	 * @param qc
	 * @return
	 */
	private Set<Node> getNodeMatchesAndFilter(QueryComponent qc) {
		Set<Node> nodes = null;
		if (qc.value().startsWith("exact:")) {
			QueryComponent qc2 = new AtomicKeyword(qc.value().replace("exact:", ""));
			nodes = this.index.getExactNodeMatches(qc2);
		} else {
			if (qc instanceof TypeSelector) {
				queryHasAtLeastOneTypeSelector = true;
				TypeSelector ts = (TypeSelector) qc;
				nodes = this.graph.getNodes(ts.getNodeType());
			} else {
				// log.info("Looking up in the index for " + qc);
				nodes = this.index.getNodeMatches(qc);
				// log.info("Index lookup finished");
			}
		}
		filterInitialMatchSet(nodes);
		return nodes;
	}

	/**
	 * GAMSearch algorithm.
	 *
	 * @param processor the processor to use on each answer tree found for the given
	 *                  query
	 */
	protected void search(Consumer<AnswerTree> processor) {
		Node lastVisitedNode, target;
		Edge currentEdge;
		GAMAnswerTree answerTree, newTree;
		Pair<GAMAnswerTree, Edge> currentPair;
		// the limit commented out below served for debugging
		while (!qg.isEmpty()) {
			if (searchStoppers.test()) { // here we test just for timeout
				log.info("Time-out (7)");
				throw new ConditionReachedException("T/O");
			}
			currentPair = qg.poll().getPair();
			// log.info("Polled tree with root edge spec " +
			// currentPair.getRight().getSpecificityValue());
			answerTree = currentPair.getLeft();

			if (leaves.size() > 1) {
				// The growth:
				lastVisitedNode = this.qg.getLastVisitedNode(answerTree);
				currentEdge = currentPair.getRight();
				target = currentEdge.getTargetNode();
				Node source = currentEdge.getSourceNode();
				if (target.equals(lastVisitedNode)) {
					target = source;
				} else if (!answerTree.nodes().contains(source)) {
					target = source;
				}
				// New tree obtained from grow
				newTree = getAnswerTreeFromGrow(answerTree, currentEdge, target);
				if (newTree != null) {
					//log.info("Grown new tree! " + newTree.debugString());
					processAnswerTree(newTree, processor, false);
					if (!soluChecker.test(newTree)) {
						stats.tick(statsObj, SRC_T);
						mergeAll(newTree, processor);
						stats.tick(statsObj, MRG_T);
					}
				}
			}
		}
	}

	public class ProvenanceSignatureStatistics{
		Map<String, Long> stats;

		public ProvenanceSignatureStatistics() {
			this.stats=Maps.newHashMap();
		}
		public void add(ProvenanceSignature ps) {
			if(stats.get(ps.toString())==null)
				stats.put(ps.toString(), (long) 1);
			else
				stats.put(ps.toString(),(stats.get(ps.toString())+1));
		}

		public Map<String, Long> stats(){
			return this.stats;
		}
	}

}
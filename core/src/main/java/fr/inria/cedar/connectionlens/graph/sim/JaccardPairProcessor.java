/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.commonWords;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isBoolean;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isURI;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofRelativeLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.EITHER;

import com.google.common.collect.Range;
import com.wcohen.ss.Jaccard;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class JaccardPairProcessor extends CachedSimilarPairProcessor {

	/**
	 *  The threshold over which we decide to call Jaro or Levenshtein
	 *  (if shorter than this threshold, call Jaro, otherwise, Levenshtein)
	 */
	private final Jaccard sim = new Jaccard();
	
	private final boolean useCommonWord;
	private final int longStringThreshold;
	private final double lengthDifferenceRatio;


	public JaccardPairProcessor(StatisticsCollector stats, double th) {
		this(stats, 
				Config.getInstance().getBooleanProperty("jaccard_common_word"),
				Config.getInstance().getIntProperty("long_string_threshold"),
				Config.getInstance().getDoubleProperty("length_difference_ratio"),
				th);
	}

	public JaccardPairProcessor(StatisticsCollector stats, boolean useCommonWord, 
			int longStringThreshold, double lengthDiffRatio, double th) {
		super(stats, th);
		this.useCommonWord = useCommonWord;
		this.longStringThreshold = longStringThreshold;
		this.lengthDifferenceRatio = lengthDiffRatio;
	}

	@Override
	public NodePairSelector selector() {
		
		NodePairSelector result = not(isBoolean(EITHER).or(isNumeric(EITHER)).or(isURI(EITHER)).or(sameLabel()));
		if(useCommonWord) {
			result = result.and(commonWords(false));
		}
		if (longStringThreshold > 0) {
			result = result.and(ofAbsoluteLength(BOTH, Range.greaterThan(longStringThreshold)));
		}
		if (lengthDifferenceRatio > 0.) {
			result = result.and(ofRelativeLength(lengthDifferenceRatio));
		}
		return result;
	}


	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	@Override
	public Double apply(String a, String b) {
		return sim.score(a, b);
	}
}

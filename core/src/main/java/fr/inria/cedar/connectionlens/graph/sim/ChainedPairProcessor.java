/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import com.google.common.primitives.Ints;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Consumer;

public class ChainedPairProcessor implements SimilarPairProcessor {

	private final Queue<SimilarPairProcessor> processors;
	private final StatisticsCollector stats;

	ChainedPairProcessor(Queue<SimilarPairProcessor> p, StatisticsCollector stats) {
		this.processors = p;
		//processors.forEach(x->log.info("@@@" + x.getClass().getName()));
		this.stats = stats;
	}

	@Override
	public void process(Graph g, Collection<DataSource> from, Collection<DataSource> to, 
			Consumer<Edge> processor) {
		// IM 9/2/20: all but the last line below is my debugging code 
		//log.info("\n" + this.getClass().getSimpleName() + " process from " + from + " to " + to);
		Iterator<SimilarPairProcessor> it = processors.iterator();
		while(it.hasNext()) {
			SimilarPairProcessor spp = it.next();
			//log.info("\nCPP.process: " + spp.getClass().getSimpleName());
			spp.process(g, from, to, processor);
			//log.info("CPP.process " + spp.getClass().getSimpleName() + " ends");
		}
		//processors.forEach(p->p.process(g, from, to, processor));
	}

	@Override
	public NodePairSelector selector() {
		throw new IllegalStateException();
//		NodePairSelector result = not(isBoolean().or(isNumeric()));
//		boolean flag = false;
//		for (SimilarPairProcessor proc: processors) {
//			if(flag) {
//				result = result.or(proc.selector());
//			} else {
//				result = result.and(proc.selector());
//				flag = true;
//			}
//		}
//		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		List<Integer> result = new ArrayList<>();
		Iterator<SimilarPairProcessor> it = processors.iterator();
		while (it.hasNext()) {
			int[] sf = it.next().makeSignature(str); 
			for (int j = 0; j < sf.length; j++) {
				if (result.size() <= j) {
					result.add(sf[j]);
				} else if (sf[j] >= 0 && !result.get(j).equals(sf[j])) {
					throw new IllegalStateException("Incompatible signatures");
				}
			}
		}
		return Ints.toArray(result);
	}
	
	public static ChainedPairProcessor.Builder builder(StatisticsCollector stats) {
		return new Builder(stats);
	}
	
	public static class Builder {

		LinkedList<SimilarPairProcessor> q = new LinkedList<>();
		StatisticsCollector stats;
		
		private Builder(StatisticsCollector stats) {
			this.stats = stats;
		}

		public void add(SimilarPairProcessor p) {
			q.add(p);
		}

		public ChainedPairProcessor build() {
			return new ChainedPairProcessor(q, stats);
		}
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import java.io.Serializable;

import fr.inria.cedar.connectionlens.sql.StorageModels;

public interface ItemID {

	Object value();

	public static interface NodeID extends ItemID, Serializable {

	}

	public static interface EdgeID extends ItemID {
		EdgeID reverse();
	}

	public static interface Factory {
		
		StorageModels storageModel();
		NodeID parseNodeID(int idValue); 
		EdgeID parseEdgeID(int idValue); 
	}

	public int compareTo(ItemID id);
}
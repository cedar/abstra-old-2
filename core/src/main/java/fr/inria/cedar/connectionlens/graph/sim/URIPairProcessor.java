/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isURI;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameStoredPrefix; 
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import com.google.common.collect.Sets;
import fr.inria.cedar.connectionlens.source.RDFDataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

public class URIPairProcessor extends EqualityProcessor {
	
	public URIPairProcessor(StatisticsCollector stats, double th) {
		super(stats, th);
	}

	@Override
	public Double apply(String a, String b) {
		try {
			return new URI(a).equals(new URI(b)) ? 1.0 : 0.0;
		} catch (URISyntaxException e) {
			log.warn(e.getMessage());
			return 0.0;
		}
	}
	// IM, 4/6/20: given that we require equal(normalized) labels, I don't see the point of re-parsing here. We have parsed before.

	@Override
	public NodePairSelector selector() {
		return super.selector().and(isURI(BOTH)).and(sameStoredPrefix());
	}
	/**
	 * Compute the similarity score between 2 URIs using Jaccard score, that is: segment 2 URI's, 
	 * compute the largest number of common segments over the total number of segments.
	 * @param uri1 the left URI
	 * @param uri2 the right URI
	 * @return the similarity score between 2 URIs using Jaccard score
	 */
	private double similarityScoreURI(URI uri1, URI uri2) {
		// segment the URI
		String regexSplit = ":|//|/|\\.|-|@";
		
		// segment and filter the 1st URI
		Set<String> tokens1 = Sets.newHashSet(String.valueOf(uri1).split(regexSplit));
		tokens1.remove("");
		// segment and filter the 2nd URI
		Set<String> tokens2 = Sets.newHashSet(String.valueOf(uri2).split(regexSplit));
		tokens2.remove("");
		return Sets.intersection(tokens1, tokens2).size()/(double) Sets.union(tokens1, tokens2).size();
	}
	
	/**
	 * Verify whether a segment is relevant: it only contains words, it does not contain schema term
	 * @param segment the segment to be processed
	 * @return <code>true</code> if the segment satisfies the criterion, <code>false</code> otherwise
	 */
	private static boolean toKeep(String segment) {
		return (segment.length() >= 3 && !RDFDataSource.isSchemaTerm(segment));
	}
}

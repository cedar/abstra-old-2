/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;


import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * This class allows to "clean" an HTML document by  modifying the DOM tree
 * before actually traversing it in CL to create nodes and edges.  
 * 
 * @author Tayeb Merabti
 */
public class HTMLPolicies extends DocPolicies{


    /** Interpreter logger. */
    static Logger log = Logger.getLogger(HTMLPolicies.class);

    /** The location of the default policies file. */
    public static final String DEFAULT_CONFIG_FILE_PATH = "html.policies";

    private static HTMLPolicies htmlPolicies;

    private HTMLPolicies (String path) {
        props = new Properties();
        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path);
            InputStreamReader reader = new InputStreamReader(in);
            props.load(reader);
            return;

        } catch (IOException e) {
            log.warn("Loading failed: " + e.getMessage());
        }

        try(InputStreamReader reader = new FileReader(path)) {
            props.load(reader);
            return;
        } catch (final IOException e) {
            log.warn("Loading failed: " + e.getMessage());
        }

    }

    /**
     *
     * @return an Instance of the HTML Policies file.
     */
    public static HTMLPolicies getInstance() {
        return getInstance(DEFAULT_CONFIG_FILE_PATH);
    }


    public static HTMLPolicies getInstance(String path) {
        if (htmlPolicies == null) {
            htmlPolicies = new HTMLPolicies(path);
        }
        return htmlPolicies;
    }


}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import fr.inria.cedar.connectionlens.util.PageRankGraphPrinting;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class wPR extends ScoringMethod {
    public static final Logger log = Logger.getLogger(wPR.class);

    private final boolean edgeWeightsAreEquallyDistributed = true;
    private final String pageRankFilename = "pageRankGraph_" + AbstractionTask.getDatasetName() + "_" + AbstractionTask.getMethodsNames();

    public wPR(Configuration configuration) {
        super(configuration);
    }

    @Override
    public void compute() throws IOException, SQLException {
        // assign dw/odw for original nodes
        this.computeAndSetOdw();

        // build the graph given to PageRank
        // and set the value of each node as the dw value and the edge transmission as the etf
        HashMap<Integer, ArrayList<CollectionEdge>> collectionGraphForPageRank = this.buildInvertedCollectionGraph();
//        HashMap<Integer, ArrayList<CollectionEdge>> collectionGraphForPageRank = this.buildOwnPageRankGraph();
        log.debug(collectionGraphForPageRank);
        // get all collections edges, such that we can compute the edge weight of each
        ArrayList<CollectionEdge> collectionEdges = new ArrayList<>();
        for(ArrayList<CollectionEdge> ces : collectionGraphForPageRank.values()) {
            collectionEdges.addAll(ces);
        }
        // computing the edge weights that will be put on the PR graph edges
        HashMap<Integer, HashMap<Integer, Double>> edgeWeights = new HashMap<>();
        if(this.edgeWeightsAreEquallyDistributed) {
            for(CollectionEdge ce : collectionEdges) {
                if(!edgeWeights.containsKey(ce.getSource())) {
                    edgeWeights.put(ce.getSource(), new HashMap<>());
                    edgeWeights.get(ce.getSource()).put(ce.getTarget(), (1.0d/collectionGraphForPageRank.get(ce.getSource()).size()));
                } else {
                    edgeWeights.get(ce.getSource()).put(ce.getTarget(), (1.0d/collectionGraphForPageRank.get(ce.getSource()).size()));
                }
            }
        } else {
            // 1. for each collection edge in the PR graph, multiply the size of the target by the etf to get the collection edge weight cew
            HashMap<CollectionEdge, Double> cews = new HashMap<>();
            for(CollectionEdge ce : collectionEdges) {
                cews.put(ce, CollectionGraph.getWorkingInstance().getCollectionSize(ce.getTarget()) * ce.getEdgeTransferFactor());
                log.debug(ce + " has a cew of " + CollectionGraph.getWorkingInstance().getCollectionSize(ce.getTarget()) + "*" + ce.getEdgeTransferFactor() + " = " + cews.get(ce));
            }
            // 2. normalize all cews by dividing each by the sum of all the cews (in order to get them sump up to 1)
            double sumOfAllCews = 0.0d;
            for(Double cew : cews.values()) {
                sumOfAllCews += cew;
            }
            log.debug("sum of all cews is " + sumOfAllCews);
            for(Map.Entry<CollectionEdge, Double> entry : cews.entrySet()) {
                cews.put(entry.getKey(), (entry.getValue() / sumOfAllCews));
            }
            // 3. fill the 2D hashmap of edge weights with the cews scores
            for(Map.Entry<CollectionEdge, Double> entry : cews.entrySet()) {
                if(!edgeWeights.containsKey(entry.getKey().getSource())) {
                    edgeWeights.put(entry.getKey().getSource(), new HashMap<>());
                    edgeWeights.get(entry.getKey().getSource()).put(entry.getKey().getTarget(), entry.getValue());
                } else {
                    edgeWeights.get(entry.getKey().getSource()).put(entry.getKey().getTarget(), entry.getValue());
                }
            }
            log.debug(edgeWeights);
        }

        // run PageRank
        PageRank pageRank = new PageRank(collectionGraphForPageRank, edgeWeights, super.configuration);
        pageRank.run();
        Map<Integer, Double> scoresPageRank = pageRank.getScores();
        log.debug(scoresPageRank);
        double sumPageRankScores = 0.0;
        for(Double score : scoresPageRank.values()) {
            sumPageRankScores += score;
        }
        PageRankGraphPrinting pageRankGraphPrinting = new PageRankGraphPrinting(pageRank, this.pageRankFilename);
        pageRankGraphPrinting.printGraph();

        for(Map.Entry<Integer, Double> entry : scoresPageRank.entrySet()) {
            CollectionGraph.getWorkingInstance().setCollectionScore(pageRank.getCollectionIdFromPageRankId(entry.getKey()), entry.getValue());
        }
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
    }

    private HashMap<Integer, ArrayList<CollectionEdge>> buildInvertedCollectionGraph() {
        HashMap<Integer, ArrayList<CollectionEdge>> invertedCollectionGraph = new HashMap<>();
        for(ArrayList<CollectionEdge> ces : CollectionGraph.getWorkingInstance().getCollectionGraph().values()) {
            for(CollectionEdge ce : ces) {
                int collectionSource = ce.getSource();
                int collectionTarget = ce.getTarget();
                CollectionEdge ceInverted = new CollectionEdge(ce.getId(), collectionTarget, collectionSource, ce.getComesFromXMLRelation(), ce.getIsAtMostOne()); // inverted collection edge
                if(!invertedCollectionGraph.containsKey(collectionTarget)) {
                    invertedCollectionGraph.put(collectionTarget, new ArrayList<>());
                    invertedCollectionGraph.get(collectionTarget).add(ceInverted);
                } else {
                    invertedCollectionGraph.get(collectionTarget).add(ceInverted);
                }
            }
        }
        return invertedCollectionGraph;
    }

    // Don't forget to start IDs at 0
    private HashMap<Integer, ArrayList<CollectionEdge>> buildOwnPageRankGraph() {
        return this.tenNodesExample();
    }

    private HashMap<Integer, ArrayList<CollectionEdge>> tenNodesExample() {
        HashMap<Integer, ArrayList<CollectionEdge>> ownPageRankGraph = new HashMap<>();
        ownPageRankGraph.put(0, new ArrayList<>());
        ownPageRankGraph.get(0).add(new CollectionEdge(1, 0, 1));
        ownPageRankGraph.put(1, new ArrayList<>());
        ownPageRankGraph.get(1).add(new CollectionEdge(2, 1, 2));
        ownPageRankGraph.get(1).add(new CollectionEdge(3, 1, 8));
        ownPageRankGraph.get(1).add(new CollectionEdge(4, 1, 5));
        ownPageRankGraph.get(1).add(new CollectionEdge(5, 1, 6));
        ownPageRankGraph.put(2, new ArrayList<>());
        ownPageRankGraph.get(2).add(new CollectionEdge(6, 2, 3));
        ownPageRankGraph.get(2).add(new CollectionEdge(7, 2, 4));
        ownPageRankGraph.put(3, new ArrayList<>());
        ownPageRankGraph.get(3).add(new CollectionEdge(8, 3, 1));
        ownPageRankGraph.put(4, new ArrayList<>());
        ownPageRankGraph.get(4).add(new CollectionEdge(9, 4, 5));
        ownPageRankGraph.get(4).add(new CollectionEdge(10, 4, 9));
        ownPageRankGraph.put(5, new ArrayList<>());
        ownPageRankGraph.get(5).add(new CollectionEdge(11, 5, 0));
        ownPageRankGraph.get(5).add(new CollectionEdge(12, 5, 1));
        ownPageRankGraph.get(5).add(new CollectionEdge(13, 5, 3));
        ownPageRankGraph.put(6, new ArrayList<>());
        ownPageRankGraph.get(6).add(new CollectionEdge(14, 6, 5));
        ownPageRankGraph.get(6).add(new CollectionEdge(15, 6, 7));
        ownPageRankGraph.get(6).add(new CollectionEdge(16, 6, 9));
        ownPageRankGraph.put(7, new ArrayList<>());
        ownPageRankGraph.get(7).add(new CollectionEdge(17, 7, 1));
        ownPageRankGraph.get(7).add(new CollectionEdge(18, 7, 8));
        ownPageRankGraph.get(7).add(new CollectionEdge(19, 7, 9));
        ownPageRankGraph.put(8, new ArrayList<>());
        ownPageRankGraph.get(8).add(new CollectionEdge(20, 8, 1));
        ownPageRankGraph.get(8).add(new CollectionEdge(21, 8, 2));
        ownPageRankGraph.put(9, new ArrayList<>());
        ownPageRankGraph.get(9).add(new CollectionEdge(22, 9, 4));
        return ownPageRankGraph;
    }

    private HashMap<Integer, ArrayList<CollectionEdge>> jointNodes() {
        HashMap<Integer, ArrayList<CollectionEdge>> ownPageRankGraph = new HashMap<>();
        ownPageRankGraph.put(0, new ArrayList<>());
        ownPageRankGraph.get(0).add(new CollectionEdge(1, 0, 1));
        ownPageRankGraph.get(0).add(new CollectionEdge(2, 0, 3));
        ownPageRankGraph.put(1, new ArrayList<>());
        ownPageRankGraph.get(1).add(new CollectionEdge(3, 1, 2));
        ownPageRankGraph.put(3, new ArrayList<>());
        ownPageRankGraph.get(3).add(new CollectionEdge(4, 3, 4));
        return ownPageRankGraph;
    }

    private HashMap<Integer, ArrayList<CollectionEdge>> upDown() {
        HashMap<Integer, ArrayList<CollectionEdge>> ownPageRankGraph = new HashMap<>();
        ownPageRankGraph.put(0, new ArrayList<>());
        ownPageRankGraph.get(0).add(new CollectionEdge(1, 0, 1));
        ownPageRankGraph.get(0).add(new CollectionEdge(2, 0, 2));
        return ownPageRankGraph;
    }

    private HashMap<Integer, ArrayList<CollectionEdge>> downUp() {
        HashMap<Integer, ArrayList<CollectionEdge>> ownPageRankGraph = new HashMap<>();
        ownPageRankGraph.put(0, new ArrayList<>());
        ownPageRankGraph.get(0).add(new CollectionEdge(1, 0, 1));
        ownPageRankGraph.put(2, new ArrayList<>());
        ownPageRankGraph.get(2).add(new CollectionEdge(2, 2, 1));
        return ownPageRankGraph;
    }
}

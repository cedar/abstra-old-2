package fr.inria.cedar.connectionlens.extraction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class extracts information, such as emails, uri, hashtag and mentions of an input (i.e. a node).
 * Each type of extracted entity is implemented in its own class (EmailExtractor, URIExtractor, HashtagExtractor, MentionExtractor).
 *
 * @author nellybarret
 */
public class PatternBasedExtractor extends TopLevelExtractor {

    public PatternBasedExtractor() {
        // super(this.getClass(), null, Config.getInstance().getIntProperty("extractor_batch_size_cl", 1));
        super();
    }

    public PatternBasedExtractor(Locale locale) {
        super(locale);
    }

    /**
     * Apply the regex on the content of the node
     * @param regex the regex to apply on the text
     * @param textToAnalyze the text on which the regex will be applied (i.e. the content of the node)
     * @return a list containing the matches of the regex.
     */
    public List<String> getMatches(String regex, String textToAnalyze) {
        List<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(regex).matcher(textToAnalyze);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

    /**
     * Get the name of the extractor.
     * @return the string containing he name of the extractor.
     */
    @Override
    public String getExtractorName() { return "PatternBasedExtractor"; }

    /**
     * Get the locale.
     * @return the locale.
     */
    @Override
    public Locale getLocale() { return locale; }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.indexing;

/**
 * A query component is any item that may be used in a keyword search.
 */
public interface QueryComponent {

	/**
	 * @return the underlying value of the query component.
	 */
	String value();
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import fr.inria.cedar.connectionlens.extraction.PolicyDrivenExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.text.StringEscapeUtils;

import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.function.Consumer;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;
import static fr.inria.cedar.connectionlens.graph.Node.Types.XML_ATTRIBUTE_NODE;

/**
 * @author Tayeb Merabti, Ioana Manolescu
 */
public class XMLDataSource2 extends OrderedTreeDataSource {

	/**
	 * get the XML policies
	 **/
	private final XMLPolicies xmlPolicies = XMLPolicies.getInstance();
	private final IgnoredTags ignoredTags = new IgnoredTags();

	public XMLDataSource2(ItemID.Factory f, int identifier, URI uri, URI origURI, StatisticsCollector graphStats,
                          StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.XML, graphStats, extractStats, graph);
	}

	/** This is used by the GUI to show some info about a node. It has nothing to do 
	 * with the structural contexts used a. to avoid or simplify extraction and
	 * b. to build the dataguide
	 */
	public String getContext(Graph g, Node n) {
		StringBuffer sb = new StringBuffer();
		Collection<Edge> incoming = g.getKIncomingEdgesPostLoading(n, Graph.maxEdgesReadForContext);
		for (Edge e: incoming) {
			Node parent = e.getSourceNode();
			if (n.getType() == Node.Types.XML_TAG_VALUE || n.getType() == Node.Types.XML_ATTRIBUTE_VALUE) { // literal, or attribute value
				if (e.getLabel().length() == 0) { // literal
					sb.append("Text content <b>" + n.getLabel() + "</b>" + 
							" of an XML element labeled <b>" + parent.getLabel()+ "</b>"); 
				}
				else { // attribute value
					sb.append("Value <b>" + n.getLabel() + "</b> of attribute <b>" + 
							e.getLabel()  + "</b> of XML element labeled <b>" + parent.getLabel()+
							"</b>"); 
				}
			}
			if (n.getType() == Node.Types.XML_TAG_NODE || n.getType() == Node.Types.XML_ATTRIBUTE_NODE) { // element
				sb.append("XML element labeled <b>" + n.getLabel() + "</b>"); 
			}
			break; // this value node may come from different
			// contexts. We just show the first.
		}
		sb.append(" from XML doc: <i>" + n.getDataSource().getLocalURI()+ "</i>");
		return new String(sb); 
	}

	public void traverseEdges(Consumer<Edge> processor) {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty("javax.xml.stream.isCoalescing", true);
		XMLEventReader reader = null;

		try {
			reader = factory.createXMLEventReader(new FileReader(localURI.toURL().getFile().replaceAll("%20", " ")));
			Set<Edge> edges = new LinkedHashSet<>();
			Node top = null;
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.getEventType() == XMLStreamConstants.START_ELEMENT) {
					String rootElementLabel = event.asStartElement().getName().toString();
					top = traverseXMLTree(reader, rootElementLabel, "1", rootElementLabel, edges, processor);
					this.processAttributesOfNode(event, null, top, null, top.getLabel(), top.getLabel(), processor, edges);
					Edge edge = buildLabelledEdge(root, top, Edge.ROOT_LABEL);
					processor.accept(edge);
					processEdgesToLocalAndOriginalURIs(processor);
					break;

				}
			}
		} catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
		// last thing:
		finalizeSummaryComputation();
	}

	/**
	 * traverse an XML document
	 *
	 * @param dew
	 * @param path
	 * @param edges
	 * @param processor
	 * @return HTML node
	 * @throws IOException 
	 */
	private Node traverseXMLTree(XMLEventReader reader, String elementName, String dew, String path, Set<Edge> edges,
			Consumer<Edge> processor) throws XMLStreamException, IOException {
		int currDew = 1;
		Node xmlNode = buildLabelNodeOfType(" "+elementName+" ", Node.Types.XML_TAG_NODE);
		boolean thisElementStartsNoExtractZone = false; // local variable to know when to turn extraction off
		if (graph.extractPolicy().saysNoExtractAll(path)) {
			this.noExtractionInterval = true;
			thisElementStartsNoExtractZone = true;
		}

		this.recordSummaryNode(xmlNode, elementName);
		while (reader.hasNext()) {
			Node child = null;
			String currDews = makeDewElement(dew, currDew);
			XMLEvent event = reader.nextEvent();
			if (event.getEventType() == XMLStreamReader.CHARACTERS) {
				Characters characters = event.asCharacters();
				StringBuilder result = new StringBuilder();
				result.append(StringEscapeUtils.unescapeXml(characters.getData()));
				String text = result.toString().trim();
				if (!isNullOrEmpty(text)) {
					for (String sent : getSentences(text)) {
						int thisNodeContext;
						if (noExtractionInterval) { // we are in a no-extraction interval
							thisNodeContext = PolicyDrivenExtractor.noExtractContext();
						} else { // outside of a no-extraction interval, we record paths in the context map
							Integer pathNo = contextMap.get(elementName);
							if (pathNo == null) {
								pathNo = contextMap.size()+1; // Extraction context numbering starts at 0
								contextMap.put(elementName, pathNo);
								revContextMap.put(pathNo, elementName);
							}
							thisNodeContext = pathNo;
						}
						String textDews = makeDewElement(dew, currDew);
						String textPath = makePathElement(path, "#" + currDew);
						if (sent.startsWith("file:")) {
							child = createOrFindValueNodeWithAtomicity(sent, makeLocalKey(textDews, textPath, sent),
									this, thisNodeContext, RDF_URI);
						} else {
							if(xmlNode.getType() == XML_ATTRIBUTE_NODE) {
								child = createOrFindValueNodeWithAtomicity(sent, makeLocalKey(textDews, textPath, sent),
										this, thisNodeContext, Node.Types.XML_ATTRIBUTE_VALUE);
							} else {
								child = createOrFindValueNodeWithAtomicity(sent, makeLocalKey(textDews, textPath, sent),
										this, thisNodeContext, Node.Types.XML_TAG_VALUE);
							}
						}

						// we link the child to its parent
						Edge edge = buildLabelledEdge(xmlNode, child, "");
						edge.setDataSource(this);
						processor.accept(edge);
						edges.add(edge);
						currDew++;

						// we record the child as a value in the summary,
						// and we record the summary edge linking the parent to the EC value (on path parent.#val)
						this.recordSummaryNode(child, xmlNode.getLabel()+labelValues);
//							log.info("this.recordSummaryEdge(edge, xmlNode.getLabel())");
						this.recordSummaryEdge(edge, xmlNode.getLabel());
					}
				}
			} else if (event.getEventType() == XMLStreamReader.START_ELEMENT) {
				StartElement startElement = event.asStartElement();
				String element = startElement.getName().getLocalPart();
				String elementPath = makePathElement(path, element);
				if (!xmlPolicies.getIgnoredNodes().contains(element)) {
					if(!ignoredTags.ignoredStartingTags.contains(startElement.toString())) { // we skip presentation tags (e.g. <emph>)
						child = traverseXMLTree(reader, element, currDews, elementPath, edges, processor);
						this.processAttributesOfNode(event, xmlNode, child, currDews, path, elementPath, processor, edges);
						Edge xmlNodeToChildEdge = buildLabelledEdge(xmlNode, child, "");
						processor.accept(xmlNodeToChildEdge);
						edges.add(xmlNodeToChildEdge);
						currDew++;
						// record the edge as a summary edge if needed
//								log.info("this.recordSummaryEdge(xmlNodeToChildEdge, xmlNode.getLabel());");
						this.recordSummaryEdge(xmlNodeToChildEdge, xmlNode.getLabel());
					}
				} else {
					reader.nextEvent();
				}
			} else if (event.getEventType() == XMLStreamReader.END_ELEMENT) {
				if (thisElementStartsNoExtractZone) {// this element also ends it
					noExtractionInterval = false;
				}
				if(!ignoredTags.ignoredEndingTags.contains(event.asEndElement().toString())) {
					return xmlNode;
				}
			}
		}
		return xmlNode;
	}

	/**
	 * create edge attribute from the child Node.
	 * 
	 * @param dew
	 * @param processor
	 * @param edges
	 * 
	 */
	private void traverseXMLAttribute(Node parentNode, String attValue, String attName, String dew,
			int currDewAttribute, String parentPath, Consumer<Edge> processor, Set<Edge> edges, int thisNodeContext) throws IOException {

		String textDewsAttNode = dew + "#" + currDewAttribute;
		String textPathAttNode = makePathElement(parentNode.getLabel(), "#" + attName);
		Node attributeValueNode = createOrFindValueNodeWithAtomicity(attValue, makeLocalKey(textDewsAttNode, textPathAttNode, ""), this,
				thisNodeContext, Node.Types.XML_ATTRIBUTE_VALUE);
		Node attributeNameNode = buildLabelNodeOfType("@"+attName, Node.Types.XML_ATTRIBUTE_NODE);
		Edge edgeParentAttribute = buildLabelledEdge(parentNode, attributeNameNode, "");
		Edge edgeAttributeChild = buildLabelledEdge(attributeNameNode, attributeValueNode, "");
		processor.accept(edgeParentAttribute);
		processor.accept(edgeAttributeChild);
		edges.add(edgeParentAttribute);
		edges.add(edgeAttributeChild);
		currDewAttribute++;
		// we need to record the tag with its attribute but also the attribute values
		// in order to get an equivalence class for the attribute and an equivalence class #val for the XML attributes values
		this.recordSummaryNode(attributeNameNode, parentNode.getLabel()+"@"+attName);
//				log.info("this.recordSummaryEdge(edgeParentAttribute, parentNode.getLabel());");
		this.recordSummaryEdge(edgeParentAttribute, parentNode.getLabel());
		this.recordSummaryNode(attributeValueNode, parentNode.getLabel()+"@"+attName+this.labelValues);
//				log.info("this.recordSummaryEdge(edgeParentAttribute, parentNode.getLabel()+\"@\"+attName);");
		this.recordSummaryEdge(edgeAttributeChild, parentNode.getLabel()+"@"+attName);
	}

	private void processAttributesOfNode(XMLEvent event, Node xmlNode, Node node, String currDews, String path, String currentPath, Consumer<Edge> processor, Set<Edge> edges) throws IOException {
		int thisNodeContext = -1;
		if (noExtractionInterval) { // we are in a no-extraction interval
			thisNodeContext = PolicyDrivenExtractor.noExtractContext();
		} else { // outside  of a no-extraction interval, we record paths in the context map
			if(xmlNode != null) {
				Integer pathNo = contextMap.get(xmlNode.getLabel());
				if (pathNo == null) {
					pathNo = contextMap.size()+1; // Extraction context numbering starts at 0
					contextMap.put(xmlNode.getLabel(), pathNo);
					revContextMap.put(pathNo, xmlNode.getLabel());
				}
				thisNodeContext = pathNo;
			} else {
				thisNodeContext = -1;
			}
		}

		StartElement startElement = event.asStartElement();
		Iterator it = startElement.getAttributes();
		int currDewAttr = 1;
		if(currDews == null) {
			currDews = makeDewElement("1", 1);
		}
		while (it.hasNext()) {
			Attribute attribute = (Attribute) it.next();
			traverseXMLAttribute(node, attribute.getValue(), attribute.getName().toString(), currDews, currDewAttr, currentPath, processor, edges, thisNodeContext);
			currDewAttr = currDewAttr + 2;
		}
	}


	public void postprocess(Graph graph) {
		// noop
	}

}

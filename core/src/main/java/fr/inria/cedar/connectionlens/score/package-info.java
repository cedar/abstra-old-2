/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Packages containing classes related to scoring answer trees, paths, etc.
 */
package fr.inria.cedar.connectionlens.score;
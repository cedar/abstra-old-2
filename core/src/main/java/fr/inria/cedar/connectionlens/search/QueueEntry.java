package fr.inria.cedar.connectionlens.search;

import org.apache.commons.lang3.tuple.Pair;

import fr.inria.cedar.connectionlens.graph.Edge;

public class QueueEntry {
	
	Pair<GAMAnswerTree, Edge> pair;
	double score;
	
	public QueueEntry(Pair<GAMAnswerTree, Edge> entry, double score) {
		this.pair = entry;
		this.score = score;
	}

	public Pair<GAMAnswerTree, Edge> getPair() {
		return pair;
	}
	
	public double getScore() {
		return score;
	}
	
	public QueueEntry(Pair<GAMAnswerTree, Edge> entry) {
		this.pair = entry;
	}
	
	public void setScore(double score) {
		this.score = score;
	}

}

package fr.inria.cedar.connectionlens.abstraction;

import java.util.ArrayList;

public class IgnoredRdfProperties {

    public static ArrayList<String> list;

    public IgnoredRdfProperties() {
        list = new ArrayList<>();
        list.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        list.add("http://www.w3.org/2002/07/owl#sameAs");
    }

    public static ArrayList<String> getIgnoredRdfProperties() {
        return list;
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.graph.Node.Types.JSON_STRUCT;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.inria.cedar.connectionlens.graph.AvoidLinking;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Implementation of a JSON DataSource.
 *
 */
public class JSONDataSource extends OrderedTreeDataSource implements Serializable {

	int shortenedLabels = 0;
	int maxShortenedLabels = 1000;

	/**
	 * Instantiates a new JSON data source.
	 * 
	 * @param f          the item factory
	 * @param identifier the identifier
	 * @param uri        the URI
	 * @param            graphStats, StatisticsCollector extractStats
	 */
	public JSONDataSource(Factory f, int identifier, URI uri, URI origURI, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.JSON, graphStats, extractStats, graph);
	}

	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		// log.info(this.getClass().getName() + " *** traverse edges");
		StringBuilder contentBuilder = new StringBuilder();
		try (Scanner scanner = new Scanner(localURI.toURL().openStream())) {
			while (scanner.hasNextLine()) {
				contentBuilder.append(scanner.nextLine()).append('\n');
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		String jsonString = contentBuilder.toString();
		Set<Edge> edges = new LinkedHashSet<>();
		Node top = null;
		try {
			JSONObject jso = new JSONObject(jsonString);
			top = traverseJSONObject(jso, "1", "", edges, processor);
		} catch (JSONException e1) {
			try {
				JSONArray jso = new JSONArray(jsonString);
				top = traverseJSONArray(jso, "1", "", edges, processor);
			} catch (JSONException e2) {
				throw new IllegalStateException(e2);
			}
		}
		Edge edge = buildLabelledEdge(root, top, Edge.ROOT_LABEL);
		processor.accept(edge);
		processEdgesToLocalAndOriginalURIs(processor);
		// last thing: 
		this.finalizeSummaryComputation();
	}
	
	/**
	 * Traverses a JSON object.
	 *
	 * @param jso            the json object
	 * @param dew            the parent's Dewey code
	 * @param path           the parent's path
	 * @param edges          the edges
	 * @param processor      the edge processor to be used on newly traversed edges.
	 * @return the JSON node being traversed.
	 */
	private Node traverseJSONObject(JSONObject jso, String dew, String path, Set<Edge> edges,
			Consumer<Edge> processor) {
		//log.info("Object on " + path); 
		Iterator<String> names = jso.keys();
		int currDew = 1;
		Node node =  buildEmptyLabelNodeOfType(JSON_STRUCT);
		this.recordSummaryNode(node, path);
		while (names.hasNext()) {
			String currName = names.next();
			Object currValue = jso.get(currName);
			Node child = null;
			String currDews = makeDewElement(dew, currDew);
			String currPath = makePathElement(path, currName);
			// need to act differently depending on value's type
			if (!(currValue instanceof JSONObject) && !(currValue instanceof JSONArray)) {
				String val = currValue == JSONObject.NULL ? "" : String.valueOf(currValue);
				createValueNodes(node, currName, getSentences(val), currDew, currDews, currPath, edges,
						processor);
				currDew += 1;
			} else {
				if (currValue instanceof JSONObject) {
					child = traverseJSONObject((JSONObject) currValue, currDews, currPath, edges, processor);
				} else if (currValue instanceof JSONArray) {
					child = traverseJSONArray((JSONArray) currValue, currDews, currPath, edges, processor);
				}
				Edge edge = buildLabelledEdge(node, child, currName);
				processor.accept(edge);
				edges.add(edge);
				currDew += 1;
			}
		}
		return node;
	}

	/**
	 * Traverses a JSON array.
	 *
	 * @param jsa            the json array
	 * @param dew            the parent's Dewey code
	 * @param path           the parent's path
	 * @param edges          the edges
	 * @param processor      the edge processor to be used on newly traversed edges.
	 * @return the JSON node being traversed.
	 */
	private Node traverseJSONArray(JSONArray jsa, String dew, String path, Set<Edge> edges, Consumer<Edge> processor) {
		//log.info("Array on " + path); 
		int currDew = 1;
		int len = jsa.length();
		Node node =  buildEmptyLabelNodeOfType(JSON_STRUCT);
		
		this.recordSummaryNode(node, path);

		for (int k = 0; k < len; k++) {
			Object currValue = jsa.get(k);
			Node child = null;
			String currDews = makeDewElement(dew, currDew);
			String currPath = makePathElement(path, ""); // IM, 2/11/21: was null, turned into empty label which is more correct.
			if (!(currValue instanceof JSONObject) && !(currValue instanceof JSONArray)) {
				String val = currValue == JSONObject.NULL ? "" : String.valueOf(currValue);
				createValueNodes(node, "", getSentences(val), currDew, currDews, currPath, edges, processor);
				currDew += 1;
			} else {
				if (currValue instanceof JSONObject) {
					child = traverseJSONObject((JSONObject) currValue, currDews, currPath, edges, processor);
				} else if (currValue instanceof JSONArray) {
					child = traverseJSONArray((JSONArray) currValue, currDews, currPath, edges, processor);
				}
				Edge edge = buildUnlabelledEdge(node, child); 
				processor.accept(edge);
				edges.add(edge);
				currDew += 1;
			}
		}
		return node;
	}

	/**
	 * Creates one or several value nodes and attaches them to a given node.
	 * 
	 * @param node
	 * @param currName
	 * @param sentences
	 * @param currDew
	 * @param dew
	 * @param path
	 * @param edges
	 * @param processor
	 */
	private void createValueNodes(Node node, String currName, List<String> sentences, int currDew, String dew,
			String path, Set<Edge> edges, Consumer<Edge> processor) {
		String currDews = dew;
		String currPath = path;
		//log.info("Value(s) on " + path); 
		for (String val : sentences) {
			Node child = null; 
			if (val.startsWith("file:")) {
				child = createOrFindValueNodeWithAtomicity(val, makeLocalKey(currDews, currPath, val), this,
						RDF_URI);
			} else {
				child = createOrFindValueNodeWithAtomicity(val, makeLocalKey(currDews, currPath, val), this,
						Node.Types.JSON_VALUE);
			}
			Edge edge = null;
			if (!isNullOrEmpty(currName)) {
				edge = buildLabelledEdge(node, child, currName); 
			} else {
				edge = buildUnlabelledEdge(node, child); 
			}
			edge.setDataSource(this);
			processor.accept(edge);
			edges.add(edge);
			currDew += 1;
			currDews = makeDewElement(dew, currDew);
			currPath = makePathElement(path, null);
		}
	}


	@Override
	public void postprocess(Graph graph) {
		// do whatever ¯\_(ツ)_/¯
	}

	@Override
	public String getContext(Graph g, Node n) {		
		boolean fromTweet = false; 
		String context = "";
		// if then node comes from a tweet, return the tweet URI	
		String query = "select n1.label from nodes n1, nodes n2, edges e " 
				+ " where n2.ds=" + n.getDataSource().getID()
				+ " and n2.type = 0 and n2.label like '%tweet%' " 
				+ " and n2.ds = n1.ds " + " and e.target=n1.id "
				+ " and e.label = 'expanded_url'" 
				+ "  and n1.label like 'https://twitter.com/%'";

		// log.info(query);
		try (Statement stm = ConnectionManager.getMasterConnection().createStatement();
				ResultSet result = stm.executeQuery(query)) {
			if (result.next()) {
				context = result.getString(1);
				fromTweet = true; 
			}
		} catch (SQLException e) {
			log.error("Cannot open a connection to fetch the content of this JSON. "
					+ "The default context will be printed out.");
		}

		if (fromTweet) {
			return "This node comes from a tweet: <a target='_blank' href='" + context + "'>" + context + "</a>.";
		}
		StringBuffer sb = new StringBuffer();
		if (n.getType() == Node.Types.JSON_STRUCT) {// map or array
			Collection<Edge> outgoing = g.getKOutgoingEdgesPostLoading(n, Graph.maxEdgesReadForContext); 
			for (Edge e: outgoing) {
				if (e.getLabel().length() == 0) { // empty label --> this is an array
					sb.append("JSON array <b>" + n.getLabel() + "</b>");
				}
				else {
					sb.append("JSON map"); 
				}
				break; 
			}
		}
		else { // JSON value: attribute value or list element
			Collection<Edge> incoming = g.getKIncomingEdgesPostLoading(n, 1); // just 1 parent, otherwise confusing
			for (Edge e: incoming) {
				if (e.getLabel().length() == 0) {
					sb.append("JSON string <b>" + n.getLabel() + "</b> in an array");
				}
				else {
					sb.append("JSON value <b>" + n.getLabel() + "</b> of property <b>" +
							e.getLabel() + "</b> in a map"); 
				}
				break; 
			}
		}
		sb.append(" from JSON doc: <i>" + n.getDataSource().getLocalURI()+ "</i>");
		return new String(sb);
	}

}

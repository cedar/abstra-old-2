/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import fr.inria.cedar.connectionlens.graph.Edge;

/**
 * The class is a collection of sets of edges. 
 * Internally, they are grouped by the set size. 
 * This is used as a memory of the search process during GAMSearch.
 *  
 * @author ioanamanolescu
 *
 */
public class EdgeSetCollection {
	HashMap<Integer, HashSet<TreeSet<Edge>>> edgeSetsBySize;

	public EdgeSetCollection() {
		this.edgeSetsBySize = new HashMap<Integer, HashSet<TreeSet<Edge>>>();
	}

	public void add(AnswerTree at, Edge e) {
		TreeSet<Edge> edges = new TreeSet<Edge>();
		edges.addAll(at.edges);
		edges.add(e);
		int k = edges.size();
		HashSet<TreeSet<Edge>> treesOfK = edgeSetsBySize.get(k);
		if (treesOfK == null) {
			treesOfK = new HashSet<TreeSet<Edge>>();
			edgeSetsBySize.put(k,  treesOfK); 
		}
		treesOfK.add(edges); 
	}

	/**
	 * Tests if the collection already contained the set formed by the union of at's edges and e.
	 * If the did not already contain the set, it adds the set.
	 * @param at
	 * @param e
	 * @return True if the collection DID NOT contain that set.
	 */
	public boolean addAndStateIfNew(AnswerTree at, Edge e) {
		TreeSet<Edge> edges = new TreeSet<Edge>();
		edges.addAll(at.edges);
		if(e!=null)
			edges.add(e);
		int k = edges.size();
		HashSet<TreeSet<Edge>> treesOfK = edgeSetsBySize.get(k);
		if (treesOfK == null) {
			treesOfK = new HashSet<TreeSet<Edge>>();
			treesOfK.add(edges); 
			edgeSetsBySize.put(new Integer(k),  treesOfK); 
			return true; 
		}
		else {
			if (treesOfK.contains(edges)) {
				return false; 
			}
			else {
				treesOfK.add(edges); 
				return true; 
			}
		}
	}

	public boolean contains(AnswerTree at, Edge e) {
		TreeSet<Edge> edges = new TreeSet<Edge>();
		edges.addAll(at.edges);
		edges.add(e);
		int k = edges.size();
		HashSet<TreeSet<Edge>> treesOfK = edgeSetsBySize.get(k);
		if (treesOfK == null) {
			return false; 
		}
		else {
			return treesOfK.contains(edges);
		}
	}

	public String toString(TreeSet<Edge> s) {
		StringBuffer sb = new StringBuffer();
		sb.append("["); 
		for (Edge e: s) {
			sb.append(e.debugEdge() + " ");
		}
		sb.append("]"); 
		return new String(sb); 
	}

	public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append("|");
		for (Integer k: edgeSetsBySize.keySet()) {
			sb.append(k + ":\n");
			HashSet<TreeSet<Edge>> treesOfK = edgeSetsBySize.get(k);
			for (TreeSet<Edge> s: treesOfK) {
				sb.append(toString(s) + " ");
			}
		}
		sb.append("|");
		return new String(sb); 
	}

	public long getSize() {
		long l=0; 
		for (Integer k: edgeSetsBySize.keySet()) {
			l+= edgeSetsBySize.get(k).size();
		}
		return l; 
	}
}

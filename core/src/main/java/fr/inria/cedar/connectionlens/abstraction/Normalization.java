package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.*;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Normalization extends AbstractionTask {
    public static final Logger log = Logger.getLogger(Normalization.class);

    public Normalization(ConnectionLens cl, DataSource ds) {
        super(cl, ds);

        this.graph.createTable(SchemaTableNames.NORMALIZED_NODES_TABLE_NAME, "(id NUMERIC, ds integer, type smallint, label varchar, normalabel varchar, labelprefix varchar, representative NUMERIC, signature integer[1], primary key(id))", false, true);
        this.graph.createTable(SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME, "(id SERIAL, ds integer, type smallint, source NUMERIC, target NUMERIC, label varchar, confidence float, signature integer[1], primary key (id, label))", false, true);
    }

    public Normalization() {

    }

    /**
     * Create the normalized graph by creating an intermediary node for each labelled edge in the original nodes.
     * This fills NORMALIZED_NODES_TABLE_NAME and NORMALIZED_EDGES_TABLE_NAME tables.
     * @throws SQLException if a SQL query fails.
     */
    public void run() throws SQLException {
        log.info("NORMALIZATION BEGINS");
        this.localCollector.start(StatisticsCollector.total());

        // a. insert all the nodes from the original graph
        // NB 5 juillet 2022: this means that even extracted entities are normalized. (Labelled) extraction edges are then normalized as nodes with the type NORMALIZATION_NODE_EXTRACTION.
        log.debug("Insert all nodes into norm_nodes");
        String insertNodes = "INSERT INTO " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.ORIGINAL_NODES_TABLE_NAME + ";";
        PreparedStatement stmt = this.graph.getPreparedStatement(insertNodes);
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_INSERT_NN_DISK);

        // b. then insert all edges with empty labels (because they don't need normalization)
        log.debug("Insert unlabelled edges into norm_edges");
        String insertEdgesNoLabel = "INSERT INTO " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " WHERE label = '';";
        stmt = this.graph.getPreparedStatement(insertEdgesNoLabel);
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_INSERT_NE_DISK);
        this.localCollector.pause(StatisticsCollector.total());

        // c. finally, insert the labeled edges a --c--> b by creating a new node labeled "c" and the two corresponding edges a --> c and c --> b
        // note that we do NOT normalize edges of label "rdf:Type", "rdfs:subclass", "rdfs:domain", "rdfs:range" and "rdfs:subproperty" because they are not data, but rather knowledge on data.

        // -- 1. create the temporary table to store intermediary nodes
        this.graph.createTable(SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME, "(id_node_interm SERIAL, label VARCHAR, ds NUMERIC, type NUMERIC, confidence FLOAT, old_edge_id NUMERIC, old_source NUMERIC, old_target NUMERIC)", false, true);

        //-- 2. reset the counter to the actual max id, so that intermediary nodes start their ids after the original nodes
        String resetAutoIncrement = "SELECT setval('" + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + "_id_node_interm_seq', (SELECT cast (MAX(max) as integer) FROM ((SELECT MAX(id) FROM " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + ") UNION (SELECT MAX(id) FROM " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + ")) AS sq1));" +
                "SELECT setval('" + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + "_id_seq', (SELECT cast (MAX(max) as integer) FROM ((SELECT MAX(id) FROM " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + ") UNION (SELECT MAX(id) FROM " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + ")) AS sq1));";
        stmt = this.graph.getPreparedStatement(resetAutoIncrement);
        stmt.execute();

        //-- 3. insert labelled edges as intermediary nodes in the temporary table
        log.debug("Insert into normalization_tmp");
        this.localCollector.resume(StatisticsCollector.total());
        String insertInTempTable = "INSERT INTO " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " (label, ds, type, confidence, old_edge_id, old_source, old_target) (" +
                "SELECT label, ds, " + Node.Types.NORMALIZATION_NODE_EXTRACTION.ordinal() + ", confidence, id, source, target " + // extraction edges are labelled edges
                "FROM " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " WHERE type IN (" + Edge.getExtractionTypes() + ") " +
                "UNION ALL " +
                "SELECT label, ds, " + Node.Types.NORMALIZATION_NODE.ordinal() + ", confidence, id, source, target " + // all other labelled edges are also candidates for the normalization
                "FROM " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " WHERE type NOT IN (" + Edge.getExtractionTypes() + ") AND label NOT IN ('', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://www.w3.org/2000/01/rdf-schema#subClassOf', 'http://www.w3.org/2000/01/rdf-schema#domain', 'http://www.w3.org/2000/01/rdf-schema#range', 'http://www.w3.org/2000/01/rdf-schema#subproperty', 'http://www.w3.org/2002/07/owl#sameAs')" +
                ");";
        stmt = this.graph.getPreparedStatement(insertInTempTable);
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_INSERT_NORM_TMP_DISK);

        //-- 4. insert edges in the norm_edges table (based on the temporary table)
        //-- we insert both edges at one time
        //-- source to intermediary edge: edge id, dsId, type, source id, intermediary id, label, confidence, signature
        //-- intermediary to target edge: edge id, dsId, type, intermediary id, target id, label, confidence, signature
        log.debug("Insert into norm_edges from normalization_tmp");
        String insertNormalizedEdges = "INSERT INTO " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " (ds, type, source, target, label, confidence) (" +
                "SELECT nt1.ds, 0, nt1.old_source, nt1.id_node_interm, '', 1 FROM " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " nt1 " +
                "UNION " +
                "SELECT nt2.ds, 0, nt2.id_node_interm, nt2.old_target, '', 1 FROM " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " nt2" +
                ");";
        stmt = this.graph.getPreparedStatement(insertNormalizedEdges);
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_INSERT_NE_2_DISK);

        //-- 5. insert nodes in the norm_nodes table (based on the temporary table)
        //-- intermediary node: nodeId, dsId, type, label, normalized label, label prefix, representative, signature, clId
        log.debug("Insert into norm_nodes from normalization_tmp");
        String insertNormalizedNodes = "INSERT INTO " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " (id, ds, type, label, normalabel, labelprefix, representative, signature) (" +
                "SELECT nt1.id_node_interm, nt1.ds, nt1.type, nt1.label, TRIM(REGEXP_REPLACE(LOWER(nt1.label), '(http(s)?:\\/\\/)|(#|@|\\/|-|_|\\.|,|\")', ' ', 'g')), SUBSTRING(nt1.label, 0, 4), nt1.id_node_interm, null " +
                "FROM " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " nt1" +
                ");";
        stmt = this.graph.getPreparedStatement(insertNormalizedNodes);
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_INSERT_NN_2_DISK);

        //  finally, we clean RDF literals from their type (^^http://w3c/.../#string), the language annotation ("..."@en / "..."@fr) and their quotes
        log.debug("Update norm_nodes URIs and literals");
        String cleanUpLiterals = "UPDATE " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SET label = (CASE WHEN POSITION('^^' in label) >= 1 THEN SUBSTRING(label, 0, POSITION('^^' in label)) ELSE label END) WHERE type = " + Node.Types.RDF_LITERAL.ordinal();
        stmt = this.graph.getPreparedStatement(cleanUpLiterals);
        stmt.execute();
        cleanUpLiterals = "UPDATE " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SET label = (CASE WHEN POSITION('\"@en' in label) >= 1 THEN SUBSTRING(label, 0, POSITION('\"@en' in label)+1) ELSE label END) WHERE type = " + Node.Types.RDF_LITERAL.ordinal();
        stmt = this.graph.getPreparedStatement(cleanUpLiterals);
        stmt.execute();
        cleanUpLiterals = "UPDATE " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SET label = (CASE WHEN POSITION('\"@fr' in label) >= 1 THEN SUBSTRING(label, 0, POSITION('\"@fr' in label)+1) ELSE label END) WHERE type = " + Node.Types.RDF_LITERAL.ordinal();
        stmt = this.graph.getPreparedStatement(cleanUpLiterals);
        stmt.execute();
        cleanUpLiterals = "UPDATE " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SET label = SUBSTRING(label, 2, LENGTH(label)-2) WHERE type = " + Node.Types.RDF_LITERAL.ordinal() + " AND label <> ''";
        stmt = this.graph.getPreparedStatement(cleanUpLiterals); // we should be careful while trimming quotes, because it will fail on empty labels (thus, using the label <> '')
        stmt.execute();
//        cleanUpLiterals = "UPDATE " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " SET label = REGEXP_REPLACE(REGEXP_REPLACE(label, '[\\\\]n|[\\\\]t|[\\\\]r', ' ', 'g'), ' {2,}', ' ', 'g');";
//        stmt = this.graph.getPreparedStatement(cleanUpLiterals); // we remove odd characters and replace multiple spaces by one
//        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), StatisticsKeys.NORM_UPDATE_URI_DISK);
        this.localCollector.stop(StatisticsCollector.total());
        log.info("finished to update norm_nodes");
    }

    public void reportStatistics() {
        if(this.localCollector != null) {
            this.localCollector.stopAll();
            this.localCollector.reportAll();
        }
    }

    public StatisticsCollector getLocalCollector() {
        return this.localCollector;
    }
}

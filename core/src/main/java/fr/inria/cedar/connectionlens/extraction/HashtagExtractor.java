package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;

import java.util.*;

public class HashtagExtractor extends PatternBasedExtractor {

    static final ArrayList<String> dataTypesNotHashtags = new ArrayList<String>() {{
        add("#string");
        add("#float");
        add("#double");
        add("#int");
        add("#long");
        add("#short");
        add("#byte");
        add("#integer");
        add("#decimal");
        add("#Boolean");
    }};
    // list taken from https://jena.apache.org/documentation/notes/typed-literals.html


    public HashtagExtractor() {
        super();
    }

    public HashtagExtractor(Locale locale) {
        super(locale);
    }

    /**
     * Extract hashtags from nodes.
     * @param ds the node's datasource
     * @param input the content of the node
     * @param structuralContext the context of the node (not used here).
     * @return the collection of extracted hashtags.
     */
    @Override
    public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
        HashSet<Entity> extractedNodes = new HashSet<>();
        List<String> extractedHashtags = this.findHashtags(input);

        if(!extractedHashtags.isEmpty()) {
            for(String extractedHashtag : extractedHashtags) {
                if(!isRdfHashtag(extractedHashtag)) { // check that the extracted hashtag does not come from a RDF URI (e.g. w3c/2001/schema#string)
                    Entity entity = new Entity(extractedHashtag, extractedHashtag, 1.0, 0, Node.Types.HASHTAG, ds.getLocalURI().toString());
                    extractedNodes.add(entity);
                }
            }
        }

        return extractedNodes;
    }

    /**
     * Find hashtags, using a regex, in the content of the node
     * @param text the content of the node
     * @return a list of strings which are the hashtags found in the content of the node.
     */
    public List<String> findHashtags(String text) {
        String pattern = "#[^ ,;<>\"#.\n\r]+";
        return this.getMatches(pattern, text);
    }

    /**
     * Return true is the given hashtag is a RDF type.
     * @param hashtag the hashtag to check
     * @return true is the given hashtag is a RDF type, false else.
     */
    private boolean isRdfHashtag(String hashtag) {
        return dataTypesNotHashtags.contains(hashtag);
    }

    /**
     * Get the name of the extractor.
     * @return the string containing the name of the extractor.
     */
    @Override
    public String getExtractorName() { return "HashtagExtractor"; }

    /**
     * Get the locale.
     * @return the locale.
     */
    @Override
    public Locale getLocale() { return locale; }
}

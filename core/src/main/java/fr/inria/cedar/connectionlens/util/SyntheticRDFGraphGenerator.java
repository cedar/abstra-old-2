/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SyntheticRDFGraphGenerator {
	/*
	 * This path should point out to the core folder of ConnectionLens. It will be
	 * used to create the synthetic graphs which will be stored according to their
	 * type (start,line graphs etc..)
	 */
	public static String path = "/Users/ioanamanolescu/GITLAB/connection-lens/core/";
	private static final Logger log = Logger.getLogger(SyntheticRDFGraphGenerator.class);

	public static void generateLineGraph(String kwd1, String kwd2, int n) {
		generateLineGraphFromOffset(kwd1, kwd2, n, 0);
	}

	public static void generateLineGraphFromOffset(String kwd1, String kwd2, int n, int offset) {
		try {
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(new File(path + "data/rdf/generated/line-" + kwd1 + "-" + kwd2 + "-" + n + ".nt")));
			bw.write("<http://graph.cl/line" + (offset + 1) + "> <http://graph.cl/value> \"" + kwd1 + "\" .\n");
			bw.write("<http://graph.cl/line" + (offset + n + 1) + "> <http://graph.cl/value> \"" + kwd2 + "\" .\n");
			for (int i = 1; i <= n; i++) {
				bw.write("<http://graph.cl/line" + (offset + i) + "> <http://graph.cl/next> <http://graph.cl/line"
						+ (offset + i + 1) + "> .\n");
			}
			bw.close();

		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	public static void generateHierarchicalGraph(String kwd1, String kwd2, int n, int m) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					new File(path + "data/rdf/generated/hier-" + kwd1 + "-" + kwd2 + "-" + n + "-" + m + ".nt")));
			bw.write("<http://graph.cl/line1> <http://graph.cl/value> \"" + kwd1 + "\" .\n");
			bw.write("<http://graph.cl/line" + (n + 1) + "> <http://graph.cl/value> \"" + kwd2 + "\" .\n");
			for (int i = 1; i <= n; i++) {
				bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/next> <http://graph.cl/line" + (i + 1)
						+ "> .\n");
			}
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= m; j++) {
					bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/lateral> <http://graph.cl/leaf"
							+ ((i - 1) * m + j) + "> .\n");
				}
			}
			bw.close();

		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	public static void generateChainGraph(String kwd1, String kwd2, int n) {
		try {
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(new File(path + "data/rdf/generated/chain-" + kwd1 + "-" + kwd2 + "-" + n + ".nt")));
			bw.write("<http://graph.cl/line1> <http://graph.cl/value> \"" + kwd1 + "\" .\n");
			bw.write("<http://graph.cl/line" + (n + 1) + "> <http://graph.cl/value> \"" + kwd2 + "\" .\n");
			for (int i = 1; i <= n; i++) {
				bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/upper> <http://graph.cl/line" + (i + 1)
						+ "> .\n");
				bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/lower> <http://graph.cl/line" + (i + 1)
						+ "> .\n");

			}
			bw.close();

		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	public static void generateGridGraph(String kwd1, String kwd2, int n, int m) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					new File("data/rdf/generated/grid-" + kwd1 + "-" + kwd2 + "-" + n + "-" + m + ".nt")));
			bw.write("<http://graph.cl/line1> <http://graph.cl/value> \"" + kwd1 + "\" .\n");
			bw.write("<http://graph.cl/line" + (n + 1) + "> <http://graph.cl/value> \"" + kwd2 + "\" .\n");
			// the main line:
			for (int i = 1; i <= n; i++) {
				bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/next> <http://graph.cl/line" + (i + 1)
						+ "> .\n");
			}
			// the lines below:

			// the first line below "line" URIs is labeled "lin1/col"i:
			for (int i = 1; i <= n; i++) {

				bw.write("<http://graph.cl/line" + i + "> <http://graph.cl/vert> <http://graph.cl/lin1/col" + i
						+ "> .\n");
				if (i < n) {
					bw.write("<http://graph.cl/lin1/col" + i + "> <http://graph.cl/horiz> <http://graph.cl/lin1/col"
							+ (i + 1) + "> .\n");
				}
			}
			// the next lines below are labeled "int"j"/n":
			for (int j = 1; j <= m; j++) {
				for (int i = 1; i <= n; i++) {
					bw.write("<http://graph.cl/lin" + j + "/col" + i + "> <http://graph.cl/vert> <http://graph.cl/lin"
							+ (j + 1) + "/col" + i + "> .\n");
					if (i < n) {
						bw.write("<http://graph.cl/lin" + j + "/col" + i
								+ "> <http://graph.cl/horiz> <http://graph.cl/lin" + j + "/col" + (i + 1) + "> .\n");

					}
				}
			}
			bw.close();

		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	/**
	 * k: size of circle m: number of random connections between nodes in generated
	 * circle maxFanout maxDepth It works fine with the generation, but when
	 * visualized with dot, it still takes some time to identify the circle, but it
	 * is not my concern here.
	 */
	public static void generateCycledGraph(int k, int m, int maxFanout, int maxDepth) {
		try {
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(new File("core/data/rdf/generated/circle-" + k + ".nt")));
			Random ran = new Random();
			for (int i = 1; i < k; i++) {
				bw.write("<http://graph.cl/cycle" + i + "> <http://graph.cl/next> <http://graph.cl/cycle" + (i + 1)
						+ "> .\n");
				String name = "<http://graph.cl/cycle" + i;
				generateChildren(bw, ran, maxFanout, maxDepth, name);
			}
			bw.write("<http://graph.cl/cycle" + k + "> <http://graph.cl/next> <http://graph.cl/cycle" + 1 + "> .\n");
			for (int i = 1; i < m; i++) {
				bw.write("<http://graph.cl/cycle" + ran.nextInt(k) + "> <http://graph.cl/next> <http://graph.cl/cycle"
						+ ran.nextInt(k) + "> .\n");
			}
			bw.close();
		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	/**
	 * A recursion to generate the tree in the way demanded by Ioana
	 *
	 */
	public static void generateChildren(BufferedWriter bw, Random ran, int maxFanout, int maxDepth, String nodeName)
			throws IOException {
		for (int i = 0; i < maxFanout; i++) {
			int depth = 0;
			if (nodeName.split("-") != null) {
				depth = nodeName.split("-").length;
			}
			if (ran.nextBoolean() && depth < maxDepth) {
				String newName = nodeName + "-" + i;
				bw.write(nodeName + "><http://graph.cl/next>" + newName + "> .\n");
				generateChildren(bw, ran, maxFanout, maxDepth, newName);
			}
		}
	}

	/**
	 * This method generates a Star graph to measure the performance of Grow To
	 * Representative. It will create n Line Graph datasets and each Line graph with
	 * size k - Each Line Graph has a different keyword - We take the keywords from
	 * a predefined set of kwds. - Then it adds for each data set a node with a
	 * label string as equivalentLabel. These last nodes from each dataset will be
	 * equivalent, and G2R will leverage them. Max supported value for n is 100 so
	 * far "the limitation is only because we are using 100 different names but this
	 * can simply scale by adding more names to kwds array".
	 * 
	 * @param n the number of branches of the star
	 * @param k the length of the line graph of each graph
	 */
	public static void generateStarGraph(int n, int k, String equivalentLabel) {
		try {
			String[] kwds = { "Michael", "James", "John", "Robert", "David", "William", "Mary", "Christopher", "Joseph",
					"Richard", "Daniel", "Thomas", "Matthew", "Jennifer", "Charles", "Anthony", "Patricia", "Linda",
					"Mark", "Elizabeth", "Joshua", "Steven", "Andrew", "Kevin", "Brian", "Barbara", "Jessica", "Jason",
					"Susan", "Timothy", "Paul", "Kenneth", "Lisa", "Ryan", "Sarah", "Karen", "Jeffrey", "Donald",
					"Ashley", "Eric", "Jacob", "Nicholas", "Jonathan", "Ronald", "Michelle", "Kimberly", "Nancy",
					"Justin", "Sandra", "Amanda", "Brandon", "Stephanie", "Emily", "Melissa", "Gary", "Edward",
					"Stephen", "Scott", "George", "Donna", "Jose", "Rebecca", "Deborah", "Laura", "Cynthia", "Carol",
					"Amy", "Margaret", "Gregory", "Sharon", "Larry", "Angela", "Maria", "Alexander", "Benjamin",
					"Nicole", "Kathleen", "Patrick", "Samantha", "Tyler", "Samuel", "Betty", "Brenda", "Pamela",
					"Aaron", "Kelly", "Heather", "Rachel", "Adam", "Christine", "Zachary", "Debra", "Katherine",
					"Dennis", "Nathan", "Christina", "Julie", "Jordan", "Kyle", "Anna" };
			// create a folder to put all the n datasets in it. This will make it easier to
			// include all the rdf files inside this folder.
			File dir = new File(path + "data/rdf/generated/" + "starGraph-" + n + "-" + k + "-" + equivalentLabel);
			dir.mkdirs();
			// create the queries file which contains the kwds of the query
			BufferedWriter queryFile = new BufferedWriter(
					new FileWriter(new File(path + "data/rdf/generated/" + "queries.txt")));
			for (int l = 0; l < n; l++) {
				String currentkwd = kwds[l];
				if (l != 0)
					queryFile.write(" ");
				queryFile.write(currentkwd);
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(new File(dir, "star-" + currentkwd + "-" + equivalentLabel + "-" + l + ".nt")));
				bw.write("<http://graph.cl/line" + l + "-1> <http://graph.cl/value> \"" + currentkwd + "\" .\n");
				bw.write("<http://graph.cl/line" + l + "-" + (k + 1) + "> <http://graph.cl/value> \"" + equivalentLabel
						+ "\" .\n");
				for (int i = 1; i <= k; i++) {
					bw.write("<http://graph.cl/line" + l + "-" + i + "> <http://graph.cl/next> <http://graph.cl/line"
							+ l + "-" + (i + 1) + "> .\n");
				}
				bw.close();
			}
			queryFile.close();
		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	/**
	 * This method  generates a Star graph to measure the performance of Grow To
	 * Representative. It creates n Line Graph datasets and each Line graph with
	 * size k 
	 * 
	 * - kwdsNum: the number of kwds that will be distributed equally among
	 * the n line graph 
	 * 
	 * - We take the keywords from a predefined set of kwds. 
	 * 
	 * - The it adds for each data set a node with a label string as equivalentLabel.
	 * These last nodes from each dataset will be equivalent, and G2R will leverage
	 * them. Max supported value for n is 100 so far "the limitation is only because
	 * we are using 100 different names but this can simply scale by adding more
	 * names to kwds array".
	 * 
	 * @param n the number of branches of the star
	 * @param k the length og the line graph of each graph
	 */
	public static void generateStarGraphWithFewerKwds(int n, int k, int kwdsNum, String equivalentLabel) {
		try {
			String[] kwds = { "Michael", "James", "John", "Robert", "David", "William", "Mary", "Christopher", "Joseph",
					"Richard", "Daniel", "Thomas", "Matthew", "Jennifer", "Charles", "Anthony", "Patricia", "Linda",
					"Mark", "Elizabeth", "Joshua", "Steven", "Andrew", "Kevin", "Brian", "Barbara", "Jessica", "Jason",
					"Susan", "Timothy", "Paul", "Kenneth", "Lisa", "Ryan", "Sarah", "Karen", "Jeffrey", "Donald",
					"Ashley", "Eric", "Jacob", "Nicholas", "Jonathan", "Ronald", "Michelle", "Kimberly", "Nancy",
					"Justin", "Sandra", "Amanda", "Brandon", "Stephanie", "Emily", "Melissa", "Gary", "Edward",
					"Stephen", "Scott", "George", "Donna", "Jose", "Rebecca", "Deborah", "Laura", "Cynthia", "Carol",
					"Amy", "Margaret", "Gregory", "Sharon", "Larry", "Angela", "Maria", "Alexander", "Benjamin",
					"Nicole", "Kathleen", "Patrick", "Samantha", "Tyler", "Samuel", "Betty", "Brenda", "Pamela",
					"Aaron", "Kelly", "Heather", "Rachel", "Adam", "Christine", "Zachary", "Debra", "Katherine",
					"Dennis", "Nathan", "Christina", "Julie", "Jordan", "Kyle", "Anna" };
			// create a folder to put all the n datasets in it. This will make it easier to
			// include all the rdf files inside this folder.
			File dir = new File(
					path + "data/rdf/generated/" + "starGraph-" + n + "-" + k + "-" + kwdsNum + "-" + equivalentLabel);
			dir.mkdirs();
			// create the queries file which contains the kwds of the query
			BufferedWriter queryFile = new BufferedWriter(
					new FileWriter(new File(path + "data/rdf/generated/" + "queries.txt")));
			for (int i = 0; i < kwdsNum; i++) {
				if (i != 0)
					queryFile.write(" ");
				queryFile.write(kwds[i]);
			}
			queryFile.close();
			for (int l = 0; l < n; l++) {
				String currentkwd = kwds[l % kwdsNum];
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(new File(dir, "star-" + currentkwd + "-" + equivalentLabel + "-" + l + ".nt")));
				bw.write("<http://graph.cl/line" + l + "-1> <http://graph.cl/value> \"" + currentkwd + "\" .\n");
				bw.write("<http://graph.cl/line" + l + "-" + (k + 1) + "> <http://graph.cl/value> \"" + equivalentLabel
						+ "\" .\n");
				for (int i = 1; i <= k; i++) {
					bw.write("<http://graph.cl/line" + l + "-" + i + "> <http://graph.cl/next> <http://graph.cl/line"
							+ l + "-" + (i + 1) + "> .\n");
				}
				bw.close();
			}
		} catch (IOException e) {
			log.info(e.toString());
		}
	}

	/**
	 * Generate a Barabasi-Albert RDF Graph: the graph start with m0 connected
	 * graph. After that it adds all the rest n-m0 nodes iteratively "grow". for
	 * each new node, it will be connected with m already available nodes. The edges
	 * will be chosen randomly and proportional to each node degree "preferential
	 * attachment".
	 * 
	 * @param n              the size of the graph.
	 * @param m0             the size of the initial graph of connected nodes, m0<n
	 * @param m              the number of edges of each new node added to the graph
	 *                       when growing m<m0
	 * @param optionalParams mainly used for additional parameters to support
	 *                       generating multiple AB Graphs (To test GrowToRep) As
	 *                       follows: optionalParams[0] : the folder name where all
	 *                       the generated graphs will be stored. This allows for
	 *                       easier querying all the datasets. 
	 * @param optionalParams[1]: a
	 *                       unique Identifier for the nodes that belongs to each
	 *                       RDF Graph. It will be useful to avoid creating the same
	 *                       node across multiple datsets.
	 */
	public static void generateBarabasiAlbertGraph(int n, int m0, int m, String... optionalParams) {
		try {
			File dir = new File(path + "data/rdf/generated");
			dir.mkdirs();
			String filename = path + "data/rdf/generated";
			filename += optionalParams.length > 0 ? optionalParams[0] : "";
			filename += "AB_Graph-" + n + "-" + m0 + "-" + m;
			System.out.println("FILENAME: " + filename);
			String datasetIdentifier = "node";
			datasetIdentifier += optionalParams.length > 1 ? optionalParams[1] : "";
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)));
			// hold the degree of every node in the graph
			ArrayList<Integer> degrees = new ArrayList<Integer>(n + 1);
			for (int i = 0; i < n; i++) {
				degrees.add(0);
			}
			// create the initial connected graph
			for (int i = 0; i < m0; i++) {
				for (int j = 0; j < i; j++) {
					bw.write("<http://graph.cl/" + datasetIdentifier + i + "> <http://graph.cl/next> <http://graph.cl/"
							+ datasetIdentifier + j + "> .\n");
				}
				degrees.set(i, m0 - 1);
			}
			// this will hold the total degrees of the graph we have so far. It will help us
			// in defining the probability of adding an edge to a new node
			int totalDegrees = m0 * (m0 - 1);
			for (int i = m0; i < n; i++) {
				Collection<Integer> candidateNodes = chooseCandidates(degrees, totalDegrees, i, m);
				int candidateNode;
				Iterator<Integer> it = candidateNodes.iterator();
				while (it.hasNext()) {
					candidateNode = it.next();
					bw.write("<http://graph.cl/" + datasetIdentifier + i + "> <http://graph.cl/next> <http://graph.cl/"
							+ datasetIdentifier + candidateNode + "> .\n");
					degrees.set(i, degrees.get(i) + 1);
					degrees.set(candidateNode, degrees.get(candidateNode) + 1);
				}
				totalDegrees += 2 * m;
			}
			bw.close();
		} catch (IOException e) {
			log.info(e.getMessage());
		}
	}

	/**
	 * Utility function to be used by barabasiAlbertGraphGenerator
	 * 
	 * @param degrees      the degree of each of the nodes in the current graph. To
	 *                     be used as a probability weight to generate the edges
	 * @param totalDegrees the total degree the graph has so far. To be used as a
	 *                     denominator to generate the edges
	 * @param node         the current node that we want to generate the m candidate
	 *                     nodes to connect with
	 * @param m            the number of candidate nodes
	 * @return collection of m candidate nodes to connect the current node with.
	 */
	private static Collection<Integer> chooseCandidates(ArrayList<Integer> degrees, int totalDegrees, int node, int m) {
		// this set will store the nodes we already build an edge between then and node
		// i. It is used to avoid connecting node i with the same node more than once.
		Set<Integer> alreadyConnectedNodes = new HashSet<>();
		Random rand = new Random(node);
		for (int j = 0; j < m; j++) {
			int candidateNode = 0;
			do {
				int randInt = rand.nextInt(totalDegrees);
				int l = 0, r = degrees.get(0);
				candidateNode = node - 1;
				for (int i = 0; i < node - 1; i++) {
					if (randInt >= l && randInt < r) {
						candidateNode = i;
						break;
					}
					l += degrees.get(i);
					r += degrees.get(i + 1);
				}
			} while (alreadyConnectedNodes.contains(candidateNode));
			alreadyConnectedNodes.add(candidateNode);
		}
		return alreadyConnectedNodes;
	}

	/**
	 * Add keywords to the RDF graph stored in the file RDFFileName. The nodes will
	 * be chosen randomly and have at most one kwd
	 * 
	 * @param RDFFileName the filename relative to the path static variable
	 * @param kwds        a map that contains the kwds and their frequencies to be
	 *                    added to the graph. key: the kwd(String), Value: a
	 *                    frequency how many times the kwd should be present in the
	 *                    graph. Please make sure that the total sum of frequencies
	 *                    will not be bigger than the graphSize
	 */
	public static void addKwdsRandomly(String RDFFileName, int graphSize, Map<String, Integer> kwds) {
		try {
			// We will represent the graph as an adjacency List
			// n: the number of randomly selected nodes, computed as the sum of all the kwds
			// frequencies
			int n = 0;
			for (Integer freq : kwds.values()) {
				n += freq;
			}
			// First select n nodes randomly
			Random rand = new Random();
			Set<Integer> randomNodes = new HashSet<>();
			for (int i = 0; i < n; i++) {
				int candidateNode;
				do {
					candidateNode = rand.nextInt(graphSize);
				} while (randomNodes.contains(candidateNode));
				randomNodes.add(candidateNode);
			}
			// A Map that will be used to store the nodes with their new Kwds. It will be
			// used to either modify the nodes URL to add the kwd or to add a new RDF triple
			// to add the kwd as a value for the node. Yamen: for extensibility reasons, I
			// believe it would be better to add a new triple as this will allow us to add
			// multiple kwds for the same node if we want that.
			Map<Integer, String> kwdNodes = new HashMap<>();
			Iterator<Integer> nodesIt = randomNodes.iterator();
			for (Map.Entry<String, Integer> kwdEntry : kwds.entrySet()) {
				String kwd = kwdEntry.getKey();
				int freq = kwdEntry.getValue();
				for (int i = 0; i < freq; i++) {
					int candidateNode = nodesIt.next();
					kwdNodes.put(candidateNode, kwd);
				}
			}
			String nodeIdentifier = getNodeIdentifier(path + RDFFileName);
			// Add the kwds Nodes to the RDF file.
			// rename the file (Add "with-kwds" at the end to avoid confusion with the
			// Auto-generate AB Graph)
			File oldFile = new File(path + RDFFileName);
			File fileWithKwds = new File(path + RDFFileName + "with-kwds.nt");
			oldFile.renameTo(fileWithKwds);
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileWithKwds, true));
			for (Map.Entry<Integer, String> kwdNode : kwdNodes.entrySet()) {
				bw.write("<http://graph.cl/" + nodeIdentifier + kwdNode.getKey() + "> <http://graph.cl/value> \""
						+ kwdNode.getValue() + "\" .\n");
			}
			bw.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}

	/**
	 * Add keywords to the RDF graph stored in the file RDFFileName. The nodes will
	 * be chosen randomly from two categories: 1- Hubs: the set of nodes that have
	 * many connections with other nodes in the graph. 2- Leaves: the set of nodes
	 * that have very few connections with other nodes. Each chosen node will have
	 * at most one kwd.
	 * 
	 * @param RDFFileName the filename relative to the path static variable
	 * @param hubKwds    a map that contains the kwds and their frequencies to be
	 *                    added to the graph (the kwds will be added to the "Hubs").
	 *                    key: the kwd(String), Value: a frequency how many times
	 *                    the kwd should be present in the graph.
	 * @param leafKwds  a map that contains the kwds and their frequencies to be
	 *                    added to the graph (the kwds will be added to the
	 *                    "Leaves"). key: the kwd(String), Value: a frequency how
	 *                    many times the kwd should be present in the graph. Please
	 *                    make sure that the total sum of frequencies will not be
	 *                    bigger than the graphSize.
	 */
	public static void addKwdsToLeavesAndHubs(String RDFFileName, int graphSize, Map<String, Integer> hubKwds,
			Map<String, Integer> leafKwds) {
		try {
			// We will represent the graph as an adjacency List
			ArrayList<ArrayList<Integer>> adjList = getAdjListFromRDF(path + RDFFileName, graphSize, true);
			// n: the number of randomely selected nodes. Computed as the sum of
			// all the kwd frequencies
			int hubNo = 0, numOfLeaves = 0;
			for (Integer freq : hubKwds.values()) {
				hubNo += freq;
			}
			for (Integer freq : leafKwds.values()) {
				numOfLeaves += freq;
			}
			ArrayList<Pair<Integer, Integer>> degrees = new ArrayList<>();
			int nodeInd = 0;
			for (ArrayList<Integer> node : adjList) {
				Pair<Integer, Integer> pair = Pair.of(node.size(), nodeInd);
				degrees.add(pair);
				nodeInd++;
			}
			Collections.sort(degrees);

			degrees.forEach(degree -> {
				System.out.print("(" + degree.getLeft() + "," + degree.getRight() + ")");
			});
			// log.info();
			// A Map that will be used to store the nodes with their new Kwds. It will be
			// used to either modify the nodes URL to add the kwd or to add a new RDF triple
			// to add the kwd as a value for the node. Yamen: for extensibility reasons, I
			// believe it would be better to add a new triple as this will allow us to add
			// multiple kwds for the same node if we want that.
			Map<Integer, String> kwdNodes = new HashMap<>();
			// First select numOFHubs nodes starting from the node with the biggest degree
			// in decreasing order
			int nodesIterator = 0;
			for (Map.Entry<String, Integer> kwdEntry : hubKwds.entrySet()) {
				String kwd = kwdEntry.getKey();
				int freq = kwdEntry.getValue();
				for (int i = 0; i < freq; i++) {
					int candidateNode = degrees.get(degrees.size() - 1 - nodesIterator).getRight();
					kwdNodes.put(candidateNode, kwd);
					nodesIterator++;
				}
			}
			// Second: select numOfLeaves nodes starting from the node with the smalles
			// degree and in increasing order
			nodesIterator = 0;
			for (Map.Entry<String, Integer> kwdEntry : leafKwds.entrySet()) {
				String kwd = kwdEntry.getKey();
				int freq = kwdEntry.getValue();
				for (int i = 0; i < freq; i++) {
					int candidateNode = degrees.get(nodesIterator).getRight();
					kwdNodes.put(candidateNode, kwd);
					nodesIterator++;
				}
			}
			String nodeIdentifier = getNodeIdentifier(path + RDFFileName);
			// Add the kwds Nodes to the RDF file.
			// rename the file (Add "with-kwds" at the end to avoid confucion with the
			// Auto-generate AB Graph)
			File oldFile = new File(path + RDFFileName);
			File fileWithKwds = new File(path + RDFFileName + "with-kwds.nt");
			oldFile.renameTo(fileWithKwds);
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileWithKwds, true));
			for (Map.Entry<Integer, String> kwdNode : kwdNodes.entrySet()) {
				bw.write("<http://graph.cl/" + nodeIdentifier + kwdNode.getKey() + "> <http://graph.cl/value> \""
						+ kwdNode.getValue() + "\" .\n");
			}
			bw.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}

	private static String getNodeIdentifier(String RDFFileName) {
		String res = "node";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(RDFFileName)));
			String line;
			String regex = "<http://graph.cl/(node.*?)(\\d+)> <http://graph.cl/next> <http://graph.cl/(node.*?)(\\d+)> \\.";
			Pattern pattern = Pattern.compile(regex);
			line = br.readLine();
			Matcher matcher = pattern.matcher(line);
			matcher.matches();
			res = matcher.group(1);
			log.info(res);
			return res;
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return res;
	}

	/**
	 * Transform the Synthetic RDF graphs to Adj List graph Representation. This
	 * will be usefull to traverse the graph in order to control the distance
	 * between kwds when experimenting.
	 * 
	 * @param RDFFileName
	 * @param graphSize
	 * @param undirected  to specify if the generated Adj List represent a directed
	 *                    or undirected graph
	 * @return
	 */
	private static ArrayList<ArrayList<Integer>> getAdjListFromRDF(String RDFFileName, int graphSize,
			boolean undirected) {
		// We will represent the graph as an adjecancy List
		ArrayList<ArrayList<Integer>> adjList = new ArrayList<>(graphSize);
		for (int i = 0; i < graphSize; i++) {
			ArrayList<Integer> line = new ArrayList<>();
			adjList.add(line);
		}
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(RDFFileName)));
			String line;
			String regex = "<http://graph.cl/node.*?(\\d+)> <http://graph.cl/next> <http://graph.cl/node.*?(\\d+)> \\.";
			Pattern pattern = Pattern.compile(regex);
			while ((line = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				matcher.matches();
				// log.info(matcher.matches()+","+matcher.start()+","+matcher.end());
				// log.info(matcher.group(1)+","+matcher.group(2));
				int src = Integer.parseInt(matcher.group(1)), dist = Integer.parseInt(matcher.group(2));
				adjList.get(src).add(dist);
				if (undirected) {
					adjList.get(dist).add(src);
				}
			}
			br.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return adjList;
	}

	public static void generateBarabasiAlbertGraphs(String folderName, int numOfDatasets, int sizeOfDataset, int m0,
			int m, Map<String, Integer> hubsKwds, Map<String, Integer> leavesKwds) {
		File dir = new File(path + "data/rdf/generated/" + folderName);
		dir.mkdirs();
		for (int i = 0; i < numOfDatasets; i++) {
			String uniqueFileIdentifier = folderName + "/" + Integer.toString(i);
			String uniqueNodeIdentifier = "ds" + Integer.toString(i) + "_";
			generateBarabasiAlbertGraph(sizeOfDataset, m0, m, uniqueFileIdentifier, uniqueNodeIdentifier);
			String filename = uniqueFileIdentifier + "AB_Graph-" + sizeOfDataset + "-" + m0 + "-" + m;
			String fileBath = "data/rdf/generated/" + filename;
			addKwdsToLeavesAndHubs(fileBath, sizeOfDataset, hubsKwds, leavesKwds);
		}
	}

	public static void generateABGraphsForGrowToRep(String folderName, int numOfDatasets, int sizeOfDataset, int m0,
			int m, Map<String, Integer> hubsKwds, Map<String, Integer> leavesKwds, Map<String, Integer> leaves2Kwds) {
		File dir = new File(path + "data/rdf/generated/" + folderName);
		dir.mkdirs();
		for (int i = 0; i < numOfDatasets; i++) {
			String uniqueFileIdentifier = folderName + "/" + Integer.toString(i);
			String uniqueNodeIdentifier = "ds" + Integer.toString(i) + "_";
			generateBarabasiAlbertGraph(sizeOfDataset, m0, m, uniqueFileIdentifier, uniqueNodeIdentifier);
			String filename = uniqueFileIdentifier + "AB_Graph-" + sizeOfDataset + "-" + m0 + "-" + m;
			String fileBath = "data/rdf/generated/" + filename;
			if (i % 2 == 0) {
				addKwdsToLeavesAndHubs(fileBath, sizeOfDataset, hubsKwds, leavesKwds);
			} else {
				addKwdsToLeavesAndHubs(fileBath, sizeOfDataset, hubsKwds, leaves2Kwds);
			}
		}
	}

	/**
	 * perform BFS to calculate the distance between the starting node and all other
	 * node in the graph.
	 * 
	 * @param adj
	 * @param startingNode
	 * @return
	 */
	private static List<Integer> Bfs(ArrayList<ArrayList<Integer>> adj, int startingNode) {
		List<Integer> dist = new ArrayList<Integer>(adj.size());
		for (int i = 0; i < adj.size(); i++) {
			dist.add(-1);
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		dist.set(startingNode, 0);
		queue.add(startingNode);
		int currentNode;
		while (queue.size() != 0) {
			// Dequeue a vertex from queue and print it
			currentNode = queue.poll();
			// Get all adjacent vertices of the dequeued vertex
			// If a adjacent has not been visited, then mark it
			// visited and enqueue it
			Iterator<Integer> i = adj.get(currentNode).listIterator();
			while (i.hasNext()) {
				int u = i.next();
				if (dist.get(u) == -1) {
					dist.set(u, dist.get(currentNode) + 1);
					queue.add(u);
				}
			}
		}
		return dist;
	}

	/**
	 * Add keywords to the RDF graph stored in the file RDFFileName. the nodes will
	 * be chosen randomly and will guarantee that the shortest path between each
	 * pairs is equal to distance Note: Please check the getDiameter function to
	 * know the maximum possible value of distance before adding it as parameter.
	 * Note2: the function
	 * 
	 * @param RDFFileName the filename relative to the path static variable
	 *
	 */
	public static Set<Pair<Integer, Integer>> addKwdsToABGraphsAtDistance(String RDFFileName, int graphSize,
			String kwd1, String kwd2, int frequency, int distance) {
		Set<Pair<Integer, Integer>> chosenPairs = new HashSet<>();
		try {
			// We will represent the graph as an adjecancy List
			ArrayList<ArrayList<Integer>> adjList = getAdjListFromRDF(path + RDFFileName, graphSize, true);
			TreeMap<Integer, Set<Pair<Integer, Integer>>> distanceMap = getNodesDistances(adjList);
			NavigableMap<Integer, Set<Pair<Integer, Integer>>> goodDistanceMap = distanceMap.tailMap(distance, true);
			// A Map that will be used to store the nodes with their new Kwds. It will be
			// used to either modify the nodes URL to add the kwd or to add a new RDF triple
			// to add the kwd as a value for the node. Yamen: for extensibility reasons, I
			// believe it would be better to add a new triple as this will allow us to add
			// multiple kwds for the same node if we want that.
			Map<Integer, String> kwdNodes = new HashMap<>();
			for (Map.Entry<Integer, Set<Pair<Integer, Integer>>> entry : goodDistanceMap.entrySet()) {
				Set<Pair<Integer, Integer>> pairs = entry.getValue();
				for (Pair<Integer, Integer> pair : pairs) {
					if (frequency == 0) {
						break;
					}
					if (!(kwdNodes.containsKey(pair.getLeft()) && kwdNodes.get(pair.getLeft()) != kwd1)
							&& !(kwdNodes.containsKey(pair.getRight()) && kwdNodes.get(pair.getRight()) != kwd2)) {
						chosenPairs.add(pair);
						frequency -= 1;
						kwdNodes.put(pair.getLeft(), kwd1);
						kwdNodes.put(pair.getRight(), kwd2);
					}
				}
				if (frequency == 0) {
					break;
				}
			}
			String nodeIdentifier = getNodeIdentifier(path + RDFFileName);
			// Add the kwds Nodes to the RDF file.
			// rename the file (Add "with-kwds" at the end to avoid confucion with the
			// Auto-generate AB Graph)
			File oldFile = new File(path + RDFFileName);
			File fileWithKwds = new File(path + RDFFileName + "with-kwds-dist-" + distance + ".nt");
			oldFile.renameTo(fileWithKwds);
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileWithKwds, true));
			for (Map.Entry<Integer, String> kwdNode : kwdNodes.entrySet()) {
				bw.write("<http://graph.cl/" + nodeIdentifier + kwdNode.getKey() + "> <http://graph.cl/value> \""
						+ kwdNode.getValue() + "\" .\n");
			}
			bw.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return chosenPairs;
	}

	/**
	 * This function is the same as the one above but it is used when we will ass
	 * kwds to the same graph several times. Hence we will provide the map of
	 * distances to avoid calculating it each time for the exact same graph. Add
	 * keywords to the RDF graph stored in the file RDFFileName. The nodes will be
	 * chosen randomly and will guarantee that the shortest path between each pairs
	 * is equal to distance Note: Please check the getDiameter function to know the
	 * maximum possible value of distance before adding it as parameter. Note2: the
	 * function
	 * 
	 * @param RDFFileName the filename relative to the path static variable
	 *
	 */
	public static Set<Pair<Integer, Integer>> addKwdsToABGraphsAtDistanceFaster(String RDFFileName, int graphSize,
			String kwd1, String kwd2, int frequency, int distance,
			TreeMap<Integer, Set<Pair<Integer, Integer>>> distanceMap) {
		Set<Pair<Integer, Integer>> chosenPairs = new HashSet<>();
		try {
			NavigableMap<Integer, Set<Pair<Integer, Integer>>> goodDistanceMap = distanceMap.tailMap(distance, true);
			// A Map that will be used to store the nodes with their new Kwds. It will be
			// used to either modify the nodes URL to add the kwd or to add a new RDF triple
			// to add the kwd as a value for the node. Yamen: for extensibility reasons, I
			// believe it would be better to add a new triple as this will allow us to add
			// multiple kwds for the same node if we want that.
			Map<Integer, String> kwdNodes = new HashMap<>();
			for (Map.Entry<Integer, Set<Pair<Integer, Integer>>> entry : goodDistanceMap.entrySet()) {
				Set<Pair<Integer, Integer>> pairs = entry.getValue();
				for (Pair<Integer, Integer> pair : pairs) {
					if (frequency == 0) {
						break;
					}
					if (!(kwdNodes.containsKey(pair.getLeft()) && kwdNodes.get(pair.getLeft()) != kwd1)
							&& !(kwdNodes.containsKey(pair.getRight()) && kwdNodes.get(pair.getRight()) != kwd2)) {
						chosenPairs.add(pair);
						frequency -= 1;
						kwdNodes.put(pair.getLeft(), kwd1);
						kwdNodes.put(pair.getRight(), kwd2);
					}
				}
				if (frequency == 0) {
					break;
				}
			}
			String nodeIdentifier = getNodeIdentifier(path + RDFFileName);
			// Add the kwds Nodes to the RDF file.
			// rename the file (Add "with-kwds" at the end to avoid confucion with the
			// Auto-generate AB Graph)
			File oldFile = new File(path + RDFFileName);
			File fileWithKwds = new File(path + RDFFileName + "with-kwds-dist-" + distance + ".nt");
			oldFile.renameTo(fileWithKwds);
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileWithKwds, true));
			for (Map.Entry<Integer, String> kwdNode : kwdNodes.entrySet()) {
				bw.write("<http://graph.cl/" + nodeIdentifier + kwdNode.getKey() + "> <http://graph.cl/value> \""
						+ kwdNode.getValue() + "\" .\n");
			}
			bw.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return chosenPairs;
	}

	/**
	 * This function will be used to detect the pairs of nodes that will be found at
	 * a specific distance from each other. It will be used to choose a pair of
	 * nodes whose shortest path is at least of length dist. They will be used to
	 * put the kwds on those pairs for experimental results.
	 * 
	 * @param adj
	 * @return a Map where the key:"dist" is the distance between the two nodes and
	 *         the value is a set of pairs of nodes whose distance is equals to
	 *         "dist"
	 */
	private static TreeMap<Integer, Set<Pair<Integer, Integer>>> getNodesDistances(ArrayList<ArrayList<Integer>> adj) {
		List<Integer> distances;
		TreeMap<Integer, Set<Pair<Integer, Integer>>> res = new TreeMap<>();
		for (int i = 0; i < adj.size(); i++) {
			distances = Bfs(adj, i);
			for (int j = 0; j < distances.size(); j++) {
				int dist = distances.get(j);
				int v = Math.min(i, j), u = Math.max(i, j);
				Pair<Integer, Integer> p = Pair.of(v, u);
				if (res.containsKey(dist)) {
					Set<Pair<Integer, Integer>> set = res.get(dist);
					set.add(p);
					res.put(dist, set);
				} else {
					Set<Pair<Integer, Integer>> set = new HashSet<>();
					set.add(p);
					res.put(dist, set);
				}
			}
		}
		return res;
	}

	private static Set<Pair<Integer, Integer>> getPairsOfNodesAtDistance(ArrayList<ArrayList<Integer>> adjList,
			int distance) {
		TreeMap<Integer, Set<Pair<Integer, Integer>>> distanceMap = getNodesDistances(adjList);
		if (distanceMap.containsKey(distance)) {
			return distanceMap.get(distance);
		} else {
			return new HashSet<Pair<Integer, Integer>>();
		}
	}

	private static int getDiameter(ArrayList<ArrayList<Integer>> adjList) {
		TreeMap<Integer, Set<Pair<Integer, Integer>>> distanceMap = getNodesDistances(adjList);
		int diameter = 0;
		for (Integer dist : distanceMap.keySet()) {
			diameter = Math.max(diameter, dist);
		}
		return diameter;
	}

	public static void firstExperiment(int n, int m0, int m, int maxDist) {
		generateBarabasiAlbertGraph(n, m0, m);
		String rdfFileName = "data/rdf/generated/AB_Graph-" + n + "-" + m0 + "-" + m;
		ArrayList<ArrayList<Integer>> adjList = getAdjListFromRDF(path + rdfFileName, n, true);
		TreeMap<Integer, Set<Pair<Integer, Integer>>> distanceMap = getNodesDistances(adjList);
		log.info("diameter is: " + distanceMap.size());
		for (int dist = 1; dist < maxDist; dist += 2) {
			generateBarabasiAlbertGraph(n, m0, m);
			// int diameter = getDiameter(adjList);
			// log.info("diameter is: "+ diameter);
			addKwdsToABGraphsAtDistanceFaster(rdfFileName, n, "Alice", "Bob", 1, dist, distanceMap);
			log.info(String.valueOf(dist));
		}
	}

	public static void firstExperimentSameDist(int n, int m0, int m, int maxDist) {
		generateBarabasiAlbertGraph(n, m0, m);
		String rdfFileName = "data/rdf/generated/AB_Graph-" + n + "-" + m0 + "-" + m;
		ArrayList<ArrayList<Integer>> adjList = getAdjListFromRDF(path + rdfFileName, n, true);
		int diameter = getDiameter(adjList);
		log.info("diameter is: " + diameter);
		Set<Pair<Integer, Integer>> pairs = getPairsOfNodesAtDistance(adjList, maxDist);
		int uniqueIdentifier = 0;
		log.info("Num of pairs of dist 8 is: " + pairs.size());
		for (Pair<Integer, Integer> pair : pairs) {
			if (uniqueIdentifier >= 10) {
				return;
			}
			Map<Integer, String> kwdNodes = new HashMap<>();
			generateBarabasiAlbertGraph(n, m0, m);
			kwdNodes.put(pair.getLeft(), "Alice");
			kwdNodes.put(pair.getRight(), "Bob");
			addKwds(rdfFileName, kwdNodes, Integer.toString(uniqueIdentifier));
			uniqueIdentifier++;
		}
	}

	public static void addKwds(String RDFFileName, Map<Integer, String> kwdNodes, String uniqueIdentifier) {
		try {
			String nodeIdentifier = getNodeIdentifier(path + RDFFileName);
			// Add the kwds Nodes to the RDF file.
			// rename the file (Add "with-kwds" at the end to avoid confucion with the
			// Auto-generate AB Graph)
			File oldFile = new File(path + RDFFileName);
			File fileWithKwds = new File(path + RDFFileName + "with-kwds-identifier-" + uniqueIdentifier + ".nt");
			oldFile.renameTo(fileWithKwds);
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileWithKwds, true));
			for (Map.Entry<Integer, String> kwdNode : kwdNodes.entrySet()) {
				bw.write("<http://graph.cl/" + nodeIdentifier + kwdNode.getKey() + "> <http://graph.cl/value> \""
						+ kwdNode.getValue() + "\" .\n");
			}
			bw.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}

	/*
	 * The Following methods used to run experiments for BDA paper on
	 * 19-June-2020
	 * 
	 */
	public static void lineGraphsExperiment(String kwd1, String kwd2) {
		File dir = new File(path + "Line-Grap-experiment");
		dir.mkdirs();
		for (int i = 100; i <= 1000; i += 100) {
			generateLineGraph(kwd1, kwd2, i);
		}
	}

	public static void chainGraphsExperiment(String kwd1, String kwd2) {
		File dir = new File(path + "Chain-Grap-experiment");
		dir.mkdirs();
		for (int i = 2; i <= 10; i += 1) {
			generateChainGraph(kwd1, kwd2, i);
		}
	}

	public static void starGraphExperiment() {
		for (int i = 2; i < 15; i++) {
			generateStarGraph(i, 10, "river");
		}
	}

	public static void main(String[] argv) {
		// generateChainGraph("Amber", "Bob", 3);
		// generateHierarchicalGraph("Amber", "Bob", 1000, 100);
		// generateCycledGraph(8, 4, 5,4);
		// generateChainGraph("Alice", "Bob", 7);
		// generateStarGraph(3, 3,"river");
		// generateStarGraphWithFewerKwds(10,2,2,"river");
		for (int i = 10; i < 11; i *= 2) {
			generateBarabasiAlbertGraph(i, 3, 1);
			Map<String, Integer> kwds = new HashMap<>();
			kwds.put("John", 1);
			kwds.put("James", 3);
			// addKwdsRandomly("data/rdf/generated/AB_Graph-"+i+"-3-1",i,kwds);
		}
		/*
		 * String rdfFileName = path +
		 * "data/rdf/generated/Barabasi_Albert/AB_Graph-7-7-1";
		 * ArrayList<ArrayList<Integer>> adj = getAdjListFromRDF(rdfFileName,7,true);
		 * log.info("graph diameter is: " + getDiameter(adj));
		 * TreeMap<Integer,Set<Pair<Integer,Integer>>> diameter= getNodesDistances(adj);
		 */
		/*
		 * String rdfFileName = "data/rdf/generated/Barabasi_Albert/AB_Graph-7-7-1";
		 * addKwdsToABGraphsAtDistance(rdfFileName,7,"Alice","Bob",3,3);
		 */
		// firstExperiment(1000,5,2,22);
		// chainGraphsExperiment("James","John");
		// starGraphExperiment();
		// firstExperimentSameDist(1000,5,1,9);
		/*
		 * for(Map.Entry<Integer,Set<Pair<Integer,Integer>>> entry :
		 * diameter.entrySet()){
		 * System.out.print("pairs of nodes whose dist=["+entry.getKey()+"] are: ");
		 * //Set<Pair<Integer,Integer>> s = entry.getValue() for(Pair<Integer,Integer>
		 * pair : entry.getValue()){ System.out.print("("+
		 * pair.getLeft()+","+pair.getRight()+") "); } log.info(); }
		 */
		/*
		 * //generateBarabasiAlbertGraphs("ABGraphs",3,10,3,1,hubsKwd,leavesKwd);
		 * Map<String,Integer> hubsKwd = new HashMap<>(); hubsKwd.put("river",1);
		 * Map<String,Integer> leavesKwd = new HashMap<>(); leavesKwd.put("James",3);
		 * Map<String,Integer> leaves2Kwd = new HashMap<>(); leaves2Kwd.put("John",3);
		 * generateABGraphsForGrowToRep("ABGraphs",64,100,3,1,hubsKwd,leavesKwd,
		 * leaves2Kwd);
		 */
		// generateHierarchicalGraph("Amber", "Bob", 1000, 100);
		// generateLineGraph("yamen","Xin",10);
	}
}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import org.apache.log4j.Logger;

import java.util.ArrayList;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.NON_REPORTABLE;
import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.NO_STATUS;

public class UpdateBoolean extends GraphUpdateMethod {
    public static final Logger log = Logger.getLogger(UpdateBoolean.class);

    public UpdateBoolean(int cstar) {
        super(cstar);
    }

    @Override
    public void update() {
        // each collection involved in the boundary of the selected collection is now non-reportable
        // however, the boundaries can still overlap (i.e. if a conf and a paper have a name, name will belong to paper's boundary and conf's boundary)
        log.debug(this.cstar);
        log.debug(CollectionGraph.getInstance().getCollectionsBoundaries());
        ArrayList<Integer> collectionsInBoundaryOfMainCollection = CollectionGraph.getInstance().getCollectionBoundary(this.cstar).getCollectionsInBoundary();
        log.debug(collectionsInBoundaryOfMainCollection);
        for (Integer i : collectionsInBoundaryOfMainCollection) {
            // we let REPORTED collections as being REPORTED even if they are non-reportable
            if(CollectionGraph.getWorkingInstance().getCollectionStatus(i) == NO_STATUS) {
                log.debug("updating the collection graph by saying that C" + i + " is not reportable anymore");
                CollectionGraph.getWorkingInstance().setStatusOfCollection(i, NON_REPORTABLE);
            }
        }
    }
}

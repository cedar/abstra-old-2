/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import com.google.common.collect.Range;
import fr.inria.cedar.connectionlens.graph.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public final class Selectors {
	public enum Axis { SOURCE, TARGET, BOTH, EITHER }

	enum Types { STRING, BOOLEAN, NUMBER }

	public static class AbsoluteLengthSelector implements NodePairSelector {
		private final Axis axis;
		private final Range range;
		AbsoluteLengthSelector(Axis a, Range r) {
			this.range = r;
			this.axis = a;
		}
		public Range range() {
			return this.range;
		}
		public Axis axis() {
			return this.axis;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class MinHashLSHSelector implements NodePairSelector {
		private final Range range;
		private final double threshold;
		MinHashLSHSelector(Range r, double th) {
			this.range = r;
			this.threshold = th;
		}
		public Range range() {
			return this.range;
		}
		public double threshold() {
			return this.threshold;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class RelativeLengthSelector implements NodePairSelector {
		private final Double ratio;
		RelativeLengthSelector(Double r) {
			this.ratio = r;
		}
		public Double ratio() {
			return this.ratio;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class CommonWordsSelector implements NodePairSelector {
		private final boolean useIndex;
		CommonWordsSelector(boolean i) {
			this.useIndex = i;
		}
		public boolean useIndex() {
			return this.useIndex;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class CommonPrefixSelector implements NodePairSelector {
		private final int length;
		CommonPrefixSelector(int i) {
			this.length = i;
		}
		public int length() {
			return this.length;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class ConjunctiveSelector implements NodePairSelector {
		private final NodePairSelector left;
		private final NodePairSelector right;
		protected ConjunctiveSelector(NodePairSelector l, NodePairSelector r) {
			this.left = l;
			this.right = r;
		}
		public NodePairSelector left() {
			return this.left;
		}
		public NodePairSelector right() {
			return this.right;
		}
		@Override
		public String printName() {
			return "(" + left.printName() + " and " + right.printName() + ")"; 
		}
	}

	public static class DisjunctiveSelector implements NodePairSelector {
		private final NodePairSelector left;
		private final NodePairSelector right;
		protected DisjunctiveSelector(NodePairSelector l, NodePairSelector r) {
			this.left = l;
			this.right = r;
		}
		public NodePairSelector left() {
			return this.left;
		}
		public NodePairSelector right() {
			return this.right;
		}
		@Override
		public String printName() {
			return "(" + left.printName() + " or " + right.printName() + ")"; 
		}
	}

	public static class NotSelector implements NodePairSelector {
		private final NodePairSelector child;
		NotSelector(NodePairSelector other) {
			this.child = other;
		}
		public NodePairSelector child() {
			return this.child;
		}
		@Override
		public String printName() {
			return "not(" + child.printName() + ")"; 
		}
	}

	public static class IsNumericSelector implements NodePairSelector {
		private final Axis sym; 
		IsNumericSelector(Axis b) {
			this.sym = b;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class IsStringSelector implements NodePairSelector {
		private final Axis sym; 
		IsStringSelector(Axis b) {
			this.sym = b;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class IsDateTimeSelector implements NodePairSelector {
		private final Axis sym; 
		IsDateTimeSelector(Axis b) {
			this.sym = b;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class IsBooleanSelector implements NodePairSelector {
		private final Axis sym; 
		IsBooleanSelector(Axis b) {
			this.sym = b;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class IsURISelector implements NodePairSelector {
		private final Axis sym; 
		IsURISelector(Axis b) {
			this.sym = b;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class SameNormaLabelSelector implements NodePairSelector{
		private final Axis axis = Axis.BOTH; 
		SameNormaLabelSelector(){
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}
	
	public static class SameStoredPrefixSelector implements NodePairSelector{
		private final Axis axis = Axis.BOTH;  
		SameStoredPrefixSelector(){
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class LabelSelector implements NodePairSelector {
		private final Axis axis; 
		private final String value; 
		LabelSelector() {
			this.axis = Axis.BOTH;
			this.value = null;
		}
		private LabelSelector(Axis a, String val) {
			this.axis = a;
			this.value = val;
		}
		public Axis axis() {
			return axis;
		}
		public String value() {
			return value;
		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}
	
	public static class LabelDoesNotContainSelector implements NodePairSelector{
		private final Axis axis; 
		private final ArrayList<String> toAvoidInLabels; 
		LabelDoesNotContainSelector() {
			this.axis = Axis.BOTH;
			this.toAvoidInLabels = new ArrayList<String>(); 
		}
		public Axis axis() {
			return axis;
		}
//		public String value() {
//			return value;
//		}
		@Override
		public String printName() {
			return this.getClass().getSimpleName(); 
		}
	}

	public static class TypeSelector implements NodePairSelector {
		private final Axis sym;
		private final List<Integer> type;
		TypeSelector(Axis s, Node.Types... t) {
			this.type = Stream.of(t).map(o->(Integer) o.ordinal()).collect(toImmutableList());
			this.sym = s;
		}
		TypeSelector() {
			this.type = null;
			this.sym = Axis.BOTH;
		}
		public List<Integer> type() {
			return this.type;
		}
		public Axis axis() {
			return sym;
		}
		@Override
		public String printName() {
			return "Type " + type; 
		}
	}
//
//	public static class EntitiesOnlySelector implements NodePairSelector {
//		private EntitiesOnlySelector(boolean b) {
//			this.sym = b;
//		}
//		@Override
//		public boolean isSymmetric() {
//			return sym;
//		}
//	}
	
	// IM, 23/2/2020: there is no selector for FIRST_NAME nodes, nor should any query return them.
	// This is a feature!
	
	public static NodePairSelector minHashLSH(Range<Integer> range, double th) {
		return new MinHashLSHSelector(range, th);
	}
	
	public static NodePairSelector not(NodePairSelector other) {
		return new NotSelector(other);
	}

	public static NodePairSelector isURI(Axis b) {
		return new IsURISelector(b);
	}

	public static NodePairSelector isString(Axis b) {
		return new IsStringSelector(b);
	}
	
	public static NodePairSelector isBoolean(Axis b) {
		return new IsBooleanSelector(b);
	}
	
	public static NodePairSelector isNumeric(Axis b) {
		return new IsNumericSelector(b);
	}
	
	public static NodePairSelector isDateTime(Axis b) {
		return new IsDateTimeSelector(b);
	}
	
	public static NodePairSelector isEntity(Axis b) {
		return new TypeSelector(b, ENTITY_PERSON, ENTITY_LOCATION, ENTITY_ORGANIZATION);
	}
	
	public static NodePairSelector sameLabel() {
		return new LabelSelector();
	}
	
	public static NodePairSelector withLabel(Axis a, String l) {
		return new LabelSelector(a, l);
	}
	public static NodePairSelector labelDoesNotContain() {
		return new LabelDoesNotContainSelector(); 
	}
	public static NodePairSelector sameType() {
		return new TypeSelector();
	}
	
	public static NodePairSelector withTypes(Axis a, Node.Types... t) {
		return new TypeSelector(a, t);
	}
	public static SameNormaLabelSelector sameNormaLabel() {
		return new SameNormaLabelSelector(); 
	}
	public static SameStoredPrefixSelector sameStoredPrefix() {
		return new SameStoredPrefixSelector(); 
	}
	public static NodePairSelector commonWords(boolean useIndex) {
		return new CommonWordsSelector(useIndex);
	}

	// 4/6/2020: this selector is not used because it's inefficient (based on substring). Instead, we store a labelPrefix,
	// whose length is fixed.
	//public static NodePairSelector commonPrefix(int length) {
	//	return new CommonPrefixSelector(length);
	//}
	
	public static NodePairSelector ofAbsoluteLength(Axis a, Range r) {
		return new AbsoluteLengthSelector(a, r);
	}
	
	public static NodePairSelector ofRelativeLength(Double r) {
		return new RelativeLengthSelector(r);
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;


import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;


/**
 * The class provides utility methods for enumerating paths of bounded
 * length in the graph, through conjunctive SQL queries. 
 * These methods serve for debugging / inspecting the graphs.
 * @author ioanamanolescu
 *
 */
public class SQLPathUtils {
 
	private static String nodesTableName = SchemaTableNames.ORIGINAL_NODES_TABLE_NAME;
	private static String biDirEdgesTableName = SchemaTableNames.ORIGINAL_BIDIREDGES_TABLE_NAME;

	public static final Logger log = Logger.getLogger(SQLPathUtils.class);


	/**
	 * @param n the number of edges >= 1
	 * @return
	 */
	private static String makePathFromWhere(int n) {
		StringBuffer sbFrom = new StringBuffer(); 
		sbFrom.append("from "+nodesTableName+" n0, "); 
		for (int i = 1; i <= n; i ++) {
			sbFrom.append(""+nodesTableName+" n" + i + ", "+biDirEdgesTableName+" e" + i); 
			if (i<n) {
				sbFrom.append(", "); 
			}
		}
		StringBuffer sbWhere = new StringBuffer();
		sbWhere.append("where "); 
		for (int j = 0; j < n; j++) {
			// connect the nodes to the edge
			sbWhere.append("n" + j + ".id=e" + (j+1) + ".source and n" + (j+1) + 
					".id=e" + (j+1) + ".target and ");
			// ensure the nodes on the path are all different
			for (int k = 0; k <= j; k ++) {
				sbWhere.append("n" + k + ".id<>n" + (j+1) + ".id");
				if (k<j) {
					sbWhere.append(" and "); 
				}
			}
			if (j<n-1) {
				sbWhere.append(" and "); 
			}
		}
		return (new String(sbFrom) + "\n" + new String(sbWhere)); 
	}
	/**
	 * builds the predicates checking the presence of two keywords
	 * @param kwd0 to be matched in n0
	 * @param kwdn to be matched in nn
	 * @param n: the index of the last node on the path 
	 * @return
	 */
	private static String makeKwdConditions(String kwd0, String kwdn, int n) {
		StringBuffer sb = new StringBuffer();
		sb.append(" and n0.label @@ to_tsquery('" + kwd0 + "') and n" +
		n + ".label @@ to_tsquery('" + kwdn + "')");
		return new String(sb); 
	}
	
	private static String makeEdgeLabelSelectSnippet(int n) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < n; i ++) {
			sb.append("e" + i + ".label");
			if (i<n-1) {
				sb.append(","); 
			}
			sb.append(" "); 
		}
		return new String(sb); 
	}
	
	private static String makeFullEdgeInfoSelectSnippet(int n) {
		StringBuffer sb = new StringBuffer();
		sb.append("n0.id as n0id, "); 
		for (int i = 0; i <= n; i ++) {
			//sb.append("n" + i + ".id as n" + i + "id, n" + i + ".label as n" + i + "l");
			if (i<n) {
				//sb.append(", e" + (i+1) + ".id as e" + (i+1) + "id, e" + (i+1) + ".label as e" + (i+1) + "l, ");
				sb.append("e" + (i+1) + ".id as e" + (i+1) + "id, ");
			}
		}
		sb.append("n" + n + ".id as n" + n + "id"); 
		return new String(sb); 
	}

	private static String makePathEndsNodeLabelSelectSnippet(int n) {
		StringBuffer sb = new StringBuffer();
		sb.append("n0.label, n" + n + ".label ");
		return new String(sb); 
	}
	
	/**
	 * @param kwd1 keyword
	 * @param kwd2 keyword
	 * @param n path length (number of edges)
	 * @return complete SQL query that finds the paths of length n whose ends
	 * match these keywords
	 */
	private static String twoKwdPathQueryEndsNodeLabels(String kwd1, String kwd2, int n) {
		StringBuffer sb = new StringBuffer();
		sb.append("select " + makePathEndsNodeLabelSelectSnippet(n) +
				", " + n + " as length" +  
				"\n" + 
				makePathFromWhere(n) + 
				makeKwdConditions(kwd1.toLowerCase() , kwd2.toLowerCase(), n));
		return new String(sb); 
	}
	
	/**
	 * @param kwd1  keyword
	 * @param kwd2 keyword
	 * @param m maximum path length (number of edges)
	 * @return complete SQL query that finds the paths of length at most m whose ends
	 * match these keywords
	 */
	private static String twoKwdPathQueryUpToEndsNodeLabels(String kwd1, String kwd2, int m) {
		StringBuffer sb = new StringBuffer();
		for (int n = 1; n<=m; n ++) {
			sb.append(twoKwdPathQueryEndsNodeLabels(kwd1, kwd2, n));
			if (n<m) {
				sb.append("\n union all\n"); 
			}
		}
		return new String(sb); 
		
	}
	/**
	 * @param kwd1 keyword
	 * @param kwd2 keyword
	 * @param n path length (number of edges)
	 * @return complete SQL query returning all information about the paths of length n whose ends
	 * match these keywords
	 */
	public static String twoKwdPathQuerySelectAll(String kwd1, String kwd2, int n) {
		StringBuffer sb = new StringBuffer();
		sb.append("select " + makeFullEdgeInfoSelectSnippet(n) + 
				"\n" + 
				makePathFromWhere(n) + 
				makeKwdConditions(kwd1.toLowerCase() , kwd2.toLowerCase(), n));
		return new String(sb); 
	}
	

	public static void main(String[] argv) {
		//log.info(makePathFromWhere(1));
		//log.info(makePathFromWhere(2));
		//log.info(twoKwdPathQueryUpToEndsNodeLabels("Hollande", "Trierweiler", 4));
		log.info(twoKwdPathQuerySelectAll("Hollande", "Trierweiler", 4));
		
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.QueryComponent;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;


public class Exploration extends GAMSearch {

	static int maxResultSize = 50;
	Query query;
	/** If true, exploration only keeps paths that lead to entities. */
	boolean findEntitiesOnly;
	private final Logger log = Logger.getLogger(Exploration.class);

	protected Exploration(Graph g) {
		/** Ensure the stack of intialization calls upstairs */
		super(null, g, null, new StatisticsCollector()); 
	}
	
	public AnswerTree explore(Node seed, QueryComponent qc, int radius, boolean findEntitiesOnly) {
		HashSet<Node> seeds = new HashSet<Node>();
		seeds.add(seed);
		query = new Query(qc); 
		this.findEntitiesOnly = findEntitiesOnly;
		log.info("Exploring from " + seed + " radius " + radius + " findEntities: " + findEntitiesOnly);
		Set<Edge> edges = explore(seeds, radius);
		Set<Node> nodes = getNodes(edges);
		SetMultimap<Integer, Item> matches = HashMultimap.create();
		matches.put(0, seed); // Madhu 13/09/2021: Only one component, indexed by 0.
		AnswerTree at = new AnswerTree(seed, nodes, edges,
				matches);
		log.info("Built AT of " + edges.size() + " edges");
		return at;
	}

	private Set<Node> getNodes(Set<Edge> edges) {
		HashSet<Node> nodes = new HashSet<>();
		for (Edge e : edges) {
			nodes.add(e.getSourceNode());
			nodes.add(e.getTargetNode());
		}
		return nodes;
	}

	public Set<Edge> explore(Set<Node> seeds, int radius) {
		HashMap<Node, Node> reachedFrom = new HashMap<>();
		HashSet<Node> frontier = new HashSet<Node>();
		HashSet<Edge> edges = new HashSet<Edge>();
		HashSet<Edge> result = new HashSet<Edge>();
		HashSet<Node> explored = new HashSet<Node>();

		HashSet<Node> usefulNodes = new HashSet<>();
		usefulNodes.addAll(seeds);
		frontier.addAll(seeds);

		int roundNo = 0;
		while ((frontier.size() > 0) && (result.size() < maxResultSize) && 
				((radius > 0)? (roundNo < radius):true)) {
			if (searchStoppers.test()) {
				log.info("Stop 0");
				return result; 
			}
			log.info(
					"\nRound " + roundNo + " with frontier of " + frontier.size() + " nodes, " + usefulNodes.size()
							+ " useful nodes, " + edges.size() + " edges and " + result.size() + " final edges");
			HashSet<Node> newFrontier = new HashSet<Node>();
			HashSet<Node> newUseful = new HashSet<Node>();
			boolean continueExploration = true; 
			for (Node node : frontier) {
				if (edges.size() == maxResultSize) {
					//log.info("Stop 1");
					continueExploration = false; 
				}
				if (!continueExploration) {
					break; 
				}
				explored.add(node);
				// explore the data edges adjacent to node
				log.info(graph.getAdjacentEdges(node).size() + " adjacent edges to " + node.getLabel()); 
				for (Edge e : graph.getAdjacentEdges(node)) {
					if (edges.contains(e) || (e.getSpecificityValue() < minSpecificity)) {
						continue;
					}
					if (edges.size() == maxResultSize) {
						log.info("Got enough results 1!"); 
						continueExploration = false; 
						break; 
					}
					edges.add(e);
					log.info("Added edge " + e.getId()+ " of spec: " + e.getSpecificityValue() + " minSpecificity is " + minSpecificity);
					Node es = e.getSourceNode();
					Node et = e.getTargetNode();
					recordVisited(es, newFrontier, explored, usefulNodes, newUseful, reachedFrom, node);
					recordVisited(et, newFrontier, explored, usefulNodes, newUseful, reachedFrom, node);
				}
				if (!continueExploration) {
					log.info("Done 1"); 
					break; 
				}
				// explore the same-as edges adjacent to node
				for (Edge e2: graph.getSameAs(node.getRepresentative())){
					if (edges.contains(e2) || (e2.getSpecificityValue() < minSpecificity)) {
						continue;
					}
					if (edges.size() == maxResultSize) {
						log.info("Got enough results 2!"); 
						continueExploration = false; 
						break; 
					}
					edges.add(e2);
					log.info("Added sameAs edge " + e2.getId()+ " of spec: " + e2.getSpecificityValue()+ " minSpecificity is " + minSpecificity);
					Node es = e2.getSourceNode();
					Node et = e2.getTargetNode();
					recordVisited(es, newFrontier, explored, usefulNodes, newUseful, reachedFrom, node);
					recordVisited(et, newFrontier, explored, usefulNodes, newUseful, reachedFrom, node);
				}
				if (!continueExploration) {
					log.info("Done 2"); 
					break; 
				}
//				Set<Node> stronglyEquivToNode = new LinkedHashSet<>();
//				graph.getSameAs(node.representative()).forEach(e -> {
//					Node es = e.getSourceNode();
//					Node et = e.getTargetNode();
//					if (es.equals(node.representative())) {
//						if (!explored.contains(et)) { // don't let e close a loop
//							stronglyEquivToNode.add(et);
//						}
//					} else {
//						if (!explored.contains(es)) {// don't let e close a loop
//							stronglyEquivToNode.add(es);
//						}
//					}
//				});
//				stronglyEquivToNode.remove(node);
//				// it now contains all the nodes with the same representative as the root of
////				// answerTree, except node
//				for (Node intermediaryNode : stronglyEquivToNode) { 
//					for (Edge dataEdge : getCachedAdjacentEdges(query, intermediaryNode)) {
////						// log.debug("Data edge: " + dataEdge.debugEdge()); TODO continue here
//					}
//				}
			}
			if (result.size() < maxResultSize) {
				addNewUsefulNodes(newUseful, reachedFrom, usefulNodes, seeds, edges, result, maxResultSize);
				if (result.size() >= maxResultSize) {
					log.info("Exit 1");
					break;
				}
				frontier = newFrontier;
			} else {
				log.info("Exit 2");
				break;
			}
			roundNo++;
		}
		result.addAll(edges); 
		return result;
	}
		
			
	/** Records the visit of a new node in all the necessary data structures */
	private void recordVisited(Node newNode, HashSet<Node> newFrontier, HashSet<Node> explored, HashSet<Node> usefulNodes,
			HashSet<Node> newUseful, HashMap<Node, Node> reachedFrom, Node node) {
		if (!explored.contains(newNode)) { // new
			newFrontier.add(newNode);
			explored.add(newNode);
			// here we control which nodes are considered useful 
			if ((!findEntitiesOnly) || (findEntitiesOnly && isEntityType(newNode.getType()))) {
				usefulNodes.add(newNode);
				newUseful.add(newNode);
			}
			reachedFrom.put(newNode, node);
		}
	}

	void addNewUsefulNodes(HashSet<Node> newUseful, HashMap<Node, Node> reachedFrom, HashSet<Node> usefulNodes,
			Set<Node> seeds, HashSet<Edge> edges, HashSet<Edge> result, int maxEdges) {
		//log.info("\nAddNewUseful starts with " + newUseful.size() + " new useful nodes and " + usefulNodes.size() + " useful nodes");
		for (Node n : newUseful) {
			if (result.size() < maxEdges) {
				Node ancestor = reachedFrom.get(n);
				while ((ancestor != null) && (!seeds.contains(ancestor))) {
					usefulNodes.add(ancestor);
					addNewFinalResultEdges(edges, result, usefulNodes, maxEdges);
					ancestor = reachedFrom.get(ancestor);
				}
			} else {
				log.info("Exit 3");
				break;
			}
		}
	}

	void addNewFinalResultEdges(HashSet<Edge> edges, HashSet<Edge> finalResult, HashSet<Node> useful, int maxEdges) {
		//log.info("Trying to see what to add among " + edges.size() + " edges with " + useful.size()+ " useful nodes");
		int additions = 0;
		for (Edge e : edges) {
		 //log.info(e.debugEdge());
			if (useful.contains(e.getSourceNode()) && useful.contains(e.getTargetNode())) {
				//log.info("Added to final result " + e.getId()+ " of spec: " + e.getSpecificityValue()+ " minSpecificity is " + minSpecificity);			
				finalResult.add(e);
				 additions++;
			}
			//log.info("Added " + additions + " edges");
		}
	}
}

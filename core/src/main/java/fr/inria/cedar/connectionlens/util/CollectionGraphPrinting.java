/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.*;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.REPORTED;

/**
 * Draws ConnectionLens graphs using DOT. Is only feasible for small(ish)
 * graphs. 
 */
public class CollectionGraphPrinting extends AbstractionTask {

	public static int COUNTER = 1;

	private BufferedWriter bufferedWriter;
	private Set<Integer> printedCollectionNodes;
	private String drawingDir;
	private String collectionGraphDotFileName;
	private String collectionGraphPdfFileName;
	private MainCollectionsIdentification mainCollectionsIdentification;

	public static final Logger log = Logger.getLogger(CollectionGraphPrinting.class);

	public CollectionGraphPrinting(ConnectionLens cl, DataSource ds, MainCollectionsIdentification mainCollectionsIdentification) {
		super(cl, ds);
		this.printedCollectionNodes = new HashSet<>();
		this.mainCollectionsIdentification = mainCollectionsIdentification;

		this.collectionGraphDotFileName = "collectionGraph_" + this.datasetName + "_" + AbstractionTask.getMethodsNames()  + "_" + COUNTER + ".dot";
		this.collectionGraphPdfFileName = "collectionGraph_" + this.datasetName + "_" + AbstractionTask.getMethodsNames() + "_" + COUNTER + ".pdf";
	}

	public void printAllSources(boolean drawWorking) throws SQLException, IOException {
		// we assume the quotient summary fits in memory, we get it as a HashMap

		// draw the quotient graph
		this.initializeWriters(this.collectionGraphDotFileName);
		if(drawWorking) {
			this.drawWorkingCollectionGraph();
		} else {
			this.drawCollectionGraph();
		}
		this.closeAndDraw(this.collectionGraphDotFileName, this.collectionGraphPdfFileName);
		COUNTER++;
	}

	private void initializeWriters(String dotFilename) {
		try {
			this.drawingDir = Config.getInstance().getStringProperty("temp_dir");
			this.bufferedWriter = new BufferedWriter(new FileWriter(Paths.get(drawingDir, dotFilename).toString()));
			this.bufferedWriter.write("digraph g{ \n splines=polyline; \n graph[pad=\"0.5\", nodesep=\"1\", ranksep=\"2\"];\n node[shape=plaintext];\n");
//			this.bufferedWriter.write("digraph g{ \n splines=polyline; \n rankdir=\"LR\"; \n graph[pad=\"0.5\", nodesep=\"1\", ranksep=\"2\"];\n node[shape=plaintext];\n");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(String.format("Drawing directory folder '%s' not found.", this.drawingDir));
		} catch (IOException e) {
			throw new IllegalStateException("Could not open dot file for printing");
		}
	}

	private StringBuilder getNodeDrawing(int collId, boolean printWorking) {
		DecimalFormat numberFormat = new DecimalFormat("0.00");
		StringBuilder node = new StringBuilder();
		if(CollectionGraph.getInstance().getCollectionStatus(collId) == REPORTED) {
			if(printWorking) {
				node.append("C" + collId + "[ label=<<TABLE><TR><TD><B><FONT COLOR=\"RED\">C").append(collId)
						.append(" (size: ").append(CollectionGraph.getWorkingInstance().getCollectionSize(collId))
						.append(", dw=").append(numberFormat.format(CollectionGraph.getWorkingInstance().getCollectionScore(collId)))
						.append(")</FONT></B></TD></TR>");
			} else {
				node.append("C" + collId + "[ label=<<TABLE><TR><TD><B><FONT COLOR=\"RED\">C").append(collId)
						.append(" (size: ").append(CollectionGraph.getInstance().getCollectionSize(collId))
						.append(", dw=").append(numberFormat.format(CollectionGraph.getInstance().getCollectionScore(collId)))
						.append(")</FONT></B></TD></TR>");
			}
		} else {
			if(printWorking) {
				node.append("C"+collId + "[ label=<<TABLE><TR><TD>C").append(collId)
						.append(" (size: ").append(CollectionGraph.getWorkingInstance().getCollectionSize(collId))
						.append(", dw=").append(numberFormat.format(CollectionGraph.getWorkingInstance().getCollectionScore(collId))).
						append(")</TD></TR>");
			} else {
				node.append("C"+collId + "[ label=<<TABLE><TR><TD>C").append(collId)
						.append(" (size: ").append(CollectionGraph.getInstance().getCollectionSize(collId))
						.append(", dw=").append(numberFormat.format(CollectionGraph.getInstance().getCollectionScore(collId))).
						append(")</TD></TR>");

			}
		}

		if(printWorking) {
			node.append("<TR><TD>").append((CollectionGraph.getWorkingInstance().checkIfCollectionHasCategory(collId) ? CollectionGraph.getWorkingInstance().getCollectionCategory(collId) : "no category")).append("</TD></TR>");
			node.append("<TR><TD>").append(CollectionGraph.getWorkingInstance().getCollectionLabel(collId)).append("</TD></TR>");
			node.append("</TABLE>>];\n");
		} else {
			node.append("<TR><TD>").append((CollectionGraph.getInstance().checkIfCollectionHasCategory(collId) ? CollectionGraph.getInstance().getCollectionCategory(collId) : "no category")).append("</TD></TR>");
			node.append("<TR><TD>").append(CollectionGraph.getInstance().getCollectionLabel(collId)).append("</TD></TR>");
			node.append("</TABLE>>];\n");
		}
		return node;
	}

	private StringBuilder getEdgeDrawing(CollectionEdge ce, boolean printWorking) {
		DecimalFormat numberFormat = new DecimalFormat("0.00");
		StringBuilder edge = new StringBuilder();
		edge.append("C").append(ce.getSource());
		edge.append(" -> ");
		edge.append("C").append(ce.getTarget());
		edge.append(" [label=\"" + ce.getId() + "");
		edge.append(" - etf[").append(ce.getTarget()).append("][").append(ce.getSource()).append("]=").append(numberFormat.format(ce.getEdgeTransferFactor())).append("\"");
		if(printWorking) {
			if (CollectionGraph.getWorkingInstance().getInCycleCollectionEdges().contains(ce)) {
				edge.append(", color=\"red\"");
			}
		} else {
			if (CollectionGraph.getInstance().getInCycleCollectionEdges().contains(ce)) {
				edge.append(", color=\"red\"");
			}
		}
//		if(ce.getComesFromXMLRelation()) {
//			edge.append(", style=\"dashed\"");
//		}
		if(ce.getIsAtMostOne()) { // only non at-most-one edges are dashed (e.g. this illustrates that a paper is written by several authors)
			edge.append(", penwidth=\"2\"");
		}
		edge.append("];\n");
		return edge;
	}

	private void drawCollectionGraph() throws IOException {
		for(Map.Entry<Integer, ArrayList<CollectionEdge>> entry : CollectionGraph.getInstance().getCollectionGraph().entrySet()) {
			int collId = entry.getKey();
			if(!this.printedCollectionNodes.contains(collId)) {
				this.printedCollectionNodes.add(entry.getKey());
				StringBuilder sourceNodeDrawing = this.getNodeDrawing(collId, false);
				this.bufferedWriter.write(sourceNodeDrawing.toString());
			}
			// let's attach the source collection to its target collection
			for(CollectionEdge ce : entry.getValue()) {
				collId = ce.getTarget();
				if(!this.printedCollectionNodes.contains(collId)) {
					this.printedCollectionNodes.add(collId);
					StringBuilder targetNodeDrawing = this.getNodeDrawing(collId, false);
					this.bufferedWriter.write(targetNodeDrawing.toString());
				}
				StringBuilder edgeDrawing = this.getEdgeDrawing(ce, false);
				this.bufferedWriter.write(edgeDrawing.toString());
			}
		}

		String datasetPath = AbstractionTask.getDatasetPath();
		int nbNormNodes = CollectionGraph.getInstance().getNbNormalizedNodes();
		int nbCollections = CollectionGraph.getInstance().getTotalNbCollections();
		int nbMainCollections = CollectionGraph.getInstance().getMainCollections().size();
		this.bufferedWriter.write("fontsize=12; label=\"Abstraction of " + datasetPath + " (" +
				"" + nbNormNodes + " normalized nodes, " +
				"" + nbCollections + " collections, " +
				"" + nbMainCollections + " main collections)\n" +
				"with parameters " +
				"" + this.mainCollectionsIdentification.getConfiguration().getScoringMethod() + ", " +
				"" + this.mainCollectionsIdentification.getConfiguration().getBoundaryMethod() + ", " +
				"" + this.mainCollectionsIdentification.getConfiguration().getGraphUpdateMethod() + "\"\n" +
				"labelloc=top; labeljust=center;"); // add a title with the method used
	}

	private void drawWorkingCollectionGraph() throws IOException {
		for(Map.Entry<Integer, ArrayList<CollectionEdge>> entry : CollectionGraph.getWorkingInstance().getCollectionGraph().entrySet()) {
			int collId = entry.getKey();
			if(!this.printedCollectionNodes.contains(collId)) {
				this.printedCollectionNodes.add(entry.getKey());
				StringBuilder sourceNodeDrawing = this.getNodeDrawing(collId, true);
				this.bufferedWriter.write(sourceNodeDrawing.toString());
			}
			// let's attach the source collection to its target collection
			for(CollectionEdge ce : entry.getValue()) {
				collId = ce.getTarget();
				if(!this.printedCollectionNodes.contains(collId)) {
					this.printedCollectionNodes.add(collId);
					StringBuilder targetNodeDrawing = this.getNodeDrawing(collId, true);
					this.bufferedWriter.write(targetNodeDrawing.toString());
				}
				StringBuilder edgeDrawing = this.getEdgeDrawing(ce, true);
				this.bufferedWriter.write(edgeDrawing.toString());
			}
		}

		String datasetPath = AbstractionTask.getDatasetPath();
		int nbNormNodes = CollectionGraph.getInstance().getNbNormalizedNodes();
		int nbCollections = CollectionGraph.getInstance().getTotalNbCollections();
		int nbMainCollections = CollectionGraph.getInstance().getMainCollections().size();
		this.bufferedWriter.write("fontsize=12; label=\"Abstraction of " + datasetPath + " (" +
				"" + nbNormNodes + " normalized nodes, " +
				"" + nbCollections + " collections, " +
				"" + nbMainCollections + " main collections)\n" +
				"with parameters " +
				"" + this.mainCollectionsIdentification.getConfiguration().getScoringMethod() + ", " +
				"" + this.mainCollectionsIdentification.getConfiguration().getBoundaryMethod() + ", " +
				"" + this.mainCollectionsIdentification.getConfiguration().getGraphUpdateMethod() + "\"\n" +
				"labelloc=top; labeljust=center;"); // add a title with the method used
	}

	public void closeAndDraw(String dotFilename, String pdfFilename) {
		String pathToDOT = Config.getInstance().getProperty("drawing.dot_installation");

		try {
			this.bufferedWriter.write("}\n");
			this.bufferedWriter.close();
		} catch (IOException ioe) {
			throw new IllegalStateException("Could not close dot file");
		}
		try {
			String dotFilePath = Paths.get(this.drawingDir, dotFilename).toString();
			String pdfFilePath = Paths.get(this.drawingDir, pdfFilename).toString();
			String[] pdfCommand = { pathToDOT, "-Tpdf", dotFilePath, "-o", pdfFilePath };
			Runtime.getRuntime().exec(pdfCommand);
		} catch (IOException e) {
			log.info("Could not process .dot files: install graphviz or check the drawing.dot_installation value in local.settings " + e.toString());
		}
	}

}

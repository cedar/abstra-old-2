/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.TreeSet;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkArgument;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;

/**
 * The top class for nodes.
 */

public class Node extends Item implements Serializable {

	private static final Logger log = Logger.getLogger(Node.class);

	/** Supported item types. */
	public static enum Types implements Item.Types {
		DATA_SOURCE, // 0
		JSON_STRUCT, JSON_VALUE, // 1, 2
		HTML_NODE, HTML_VALUE, // 3, 4
		RDF_LITERAL, RDF_URI, // 5, 6
		RELATIONAL_STRUCT, RELATIONAL_VALUE, // 7, 8
		ENTITY_GENERIC, ENTITY_PERSON, // 9, 10
		ENTITY_LOCATION, ENTITY_ORGANIZATION, // 11, 12
		TEXT_VALUE, // 13
		FIRST_NAME, // 14
		EMAIL, // 15
		HASHTAG, // 16
		XML_TAG_NODE, XML_ATTRIBUTE_NODE, XML_TAG_VALUE, XML_ATTRIBUTE_VALUE, // 17, 18, 19, 20
		DO_NOT_LINK_VALUE, // 21
		COLLECTION,  // 22
		CATEGORY_PERSON, CATEGORY_ORGANIZATION, CATEGORY_LOCATION, CATEGORY_PRODUCT, // 30, 31, 32, 33
		CATEGORY_EVENT, CATEGORY_CREATIVE_WORK, CATEGORY_OTHER, // 34, 35, 36
		DATE, // 38 - DATE extracted using HeidelTime
		MENTION, // 39 - MENTION found by PatternBasedExtractor
		NEO4J_ENTITY, NEO4J_VALUE, NEO4J_EDGE, // 40, 41, 42 - Added for Neo4jDataSource
		NORMALIZATION_NODE, // 43 - the node built for the edge label during the normalization
		NORMALIZATION_NODE_EXTRACTION, // 44 - the node built for the edge label during the normalization which was an extraction edge before
		NORMALIZATION_NODE_XML_RELATION // 45 - nodes that are coming from XML relation edges
	}

	static int requiredPrefixLength = Config.getInstance().getIntProperty("stored_prefix_length");

	String normalizedLabel;
	// IM 1/3/2020:
	// to be filled and used depending on the node class.
	// Some node classes don't need them.
	// For Person nodes , the first will hold first names and the second last names.
	// For URI nodes, the first will hold URI segments, and I don't see(yet) what
	// to do with the last.
	ArrayList<String> labelComponents1;
	TreeSet<String> labelComponents2;

	static final String locale = Config.getInstance().getProperty("default_locale");

	/**
	 * Which short words to prune from strings before extracting the prefix_label
	 * attribute
	 */
	public static final String frenchCivilityPattern = "((m(me)?)|(M(ME|me)?)|(pr)|(P(r|R))|(dr)|(D(r|R)))(\\.?)";
	public static final String frenchArticlePattern = "(l(a|e|')|(l)|(les)|(du)|(de)|(des)|(d'))";
	public static final String frenchStopWordPattern = "^(" + frenchCivilityPattern + "|" + frenchArticlePattern
			+ ")(\\s+)";
	public static final String englishCivilityPattern = "((m(r)?(s)?)|(M(r|R)?(s|S)?)|(pr)|(P(r|R))|(dr)|(D(r|R)))(\\.)?";
	public static final String englishArticlePattern = "(the)";
	public static final String englishStopWordPattern = "^(" + englishCivilityPattern + "|" + englishArticlePattern
			+ ")(\\s+)";
	static final String URLStartPattern = "(http://)|(https://)";
	static final String frenchPatternForPrefixExtraction = "^(" + frenchStopWordPattern + "|" + URLStartPattern + ")";
	static final String englishPatternForPrefixExtraction = "^(" + englishStopWordPattern + "|" + URLStartPattern + ")";

	// all node types mentioned in the order in which they are defined
	public static String decodeType(Types t) {
		switch (t) {
		case DATA_SOURCE: // 0
			return "Data source";
		case JSON_STRUCT: // 1
			return "JSON struct";
		case JSON_VALUE: // 2
			return "JSON value";
		case HTML_NODE: // 3
			return "HTML node";
		case HTML_VALUE: // 4
			return "HTML value";
		case RDF_LITERAL: // 5
			return "RDF value";
		case RDF_URI: // 6
			return "URI";
		case RELATIONAL_STRUCT: // 7
			return "Rel. struct";
		case RELATIONAL_VALUE: // 8
			return "Rel. value";
		case ENTITY_PERSON: // 10
			return "Person";
		case ENTITY_LOCATION: // 11
			return "Location";
		case ENTITY_ORGANIZATION: // 12
			return "Organization";
		case TEXT_VALUE: // 13
			return "Text";
		case FIRST_NAME: // 14
			return "First name";
		case EMAIL: // 15
			return "Email";
		case HASHTAG: // 16
			return "Hashtag";
		case XML_TAG_NODE: // 17
			return "XML node";
		case XML_ATTRIBUTE_NODE:
			return "XML attribute";
		case XML_TAG_VALUE: // 18
			return "XML value";
		case XML_ATTRIBUTE_VALUE:
			return "XML attribute value";
		case DO_NOT_LINK_VALUE: // 19
			return "No-link value";
//		case COLLECTION: // 20
//			return "Collection";
//		case COLLECTION_PERSON: // 21
//			return "Persons";
//		case COLLECTION_ORGANIZATION: // 22
//			return "Organizations";
//		case COLLECTION_LOCATION: // 23
//			return "Locations";
//		case COLLECTION_PRODUCT: //24
//			return "Products";
//		case COLLECTION_EVENT: // 25
//			return "Events";
//		case COLLECTION_CREATIVE_WORK: // 26
//			return "Creative works";
//		case COLLECTION_OTHER: // 27
//			return "Others";
//		case RECORD_PERSON: // 28
//			return "Person";
//		case RECORD_ORGANIZATION: // 29
//			return "Organization";
//		case RECORD_LOCATION: // 30
//			return "Location";
//		case RECORD_PRODUCT: // 31
//			return "Product";
//		case RECORD_EVENT: // 32
//			return "Event";
//		case RECORD_CREATIVE_WORK: // 33
//			return "Creative Work";
//		case RECORD_OTHER: // 34
//			return "Other";
//		case SUB_RECORD: // 35
//			return "Sub-record";
		case DATE: // 36
			return "Date";
		case MENTION: // 37
			return "Mention";
		case NEO4J_ENTITY: // 38
			return "Neo4j Entity";
		case NEO4J_VALUE: // 39
			return "Neo4j Property";
		case NEO4J_EDGE: // 40
			return "Neo4j Edge Node";
		case NORMALIZATION_NODE: // 41
			return "Edge normalization node";
		case NORMALIZATION_NODE_EXTRACTION: // 42
			return "Extraction edge normalization node";
		case NORMALIZATION_NODE_XML_RELATION:
			return "XML relation edge";
		}
		throw new IllegalStateException("Type not covered: " + t);
	}

	public static boolean isExtractedType(Node.Types t) {
		switch (t) {
			case ENTITY_GENERIC: // 9
			case ENTITY_PERSON: // 10
			case ENTITY_LOCATION: // 11
			case ENTITY_ORGANIZATION: // 12
			case FIRST_NAME: // 14
			case EMAIL: // 15
			case HASHTAG: // 16
			case DATE: // 29
			case MENTION: // 30
				return true;
			default:
				return false;
		}
	}

	public static String getExtractedTypes() {
		// 9, 10, 11, 12, 14, 15, 16, 30, 31
		return ENTITY_GENERIC.ordinal() + "," + ENTITY_PERSON.ordinal() + "," + ENTITY_LOCATION.ordinal() + "," +
				ENTITY_ORGANIZATION.ordinal() + "," + FIRST_NAME.ordinal() + "," + EMAIL.ordinal() + "," +
				HASHTAG.ordinal() + "," + DATE.ordinal() + "," + MENTION.ordinal();
	}

//	public static boolean isRecordOrCollectionType(Node.Types t) {
//		switch (t) {
//			case COLLECTION:
//			case COLLECTION_PERSON: // 20
//			case COLLECTION_ORGANIZATION: // 21
//			case COLLECTION_LOCATION: // 22
//			case COLLECTION_PRODUCT: // 23
//			case COLLECTION_EVENT: // 24
//			case COLLECTION_CREATIVE_WORK: // 25
//			case COLLECTION_OTHER: // 26
//			case RECORD_PERSON: // 27
//			case RECORD_ORGANIZATION: // 28
//			case RECORD_LOCATION: // 29
//			case RECORD_PRODUCT: // 30
//			case RECORD_EVENT: // 31
//			case RECORD_CREATIVE_WORK: // 32
//			case RECORD_OTHER: // 33
//			case SUB_RECORD: // 34
//				return true;
//			default:
//				return false;
//		}
//	}

	public static String getValueTypes() {
		// (2,18,4,8,13,32,5,9,10,11,12,14,15,16,29,30)
		return "" + Types.JSON_VALUE.ordinal()+","+Types.XML_TAG_VALUE.ordinal()+","+Types.XML_ATTRIBUTE_VALUE.ordinal()+","+
				Types.HTML_VALUE.ordinal()+","+Types.RELATIONAL_VALUE.ordinal()+","+Types.TEXT_VALUE.ordinal()+","+Types.NEO4J_VALUE.ordinal()+","+
				Types.RDF_LITERAL.ordinal()+","+//Types.RDF_URI.ordinal()+","+
				ENTITY_GENERIC.ordinal()+","+Types.ENTITY_PERSON.ordinal()+","+Types.ENTITY_LOCATION.ordinal()+","+Types.ENTITY_ORGANIZATION.ordinal()+","+
				Types.FIRST_NAME.ordinal()+","+Types.EMAIL.ordinal()+","+Types.HASHTAG.ordinal()+","+Types.DATE.ordinal()+","+Types.MENTION.ordinal();
	}

	public static String getNonValueTypes() {
		// (0,1,3,7,17,31,33,34)
		return "" + Types.DATA_SOURCE.ordinal()+","+Types.JSON_STRUCT.ordinal()+","+Types.HTML_NODE.ordinal()+","+
				Types.RELATIONAL_STRUCT.ordinal()+","+Types.XML_TAG_NODE.ordinal()+","+Types.XML_ATTRIBUTE_NODE.ordinal()+","+Types.NEO4J_ENTITY.ordinal()+","+
				Types.NEO4J_EDGE.ordinal()+","+Types.NORMALIZATION_NODE.ordinal()+","+Types.NORMALIZATION_NODE_EXTRACTION.ordinal();
	}

	public static ArrayList<Types> getValueTypesAsList() {
		// (2,18,4,8,13,32,5,9,10,11,12,14,15,16,29,30)
		ArrayList<Types> valueTypes = new ArrayList<>();
		valueTypes.add(Types.JSON_VALUE);
		valueTypes.add(Types.XML_TAG_VALUE);
		valueTypes.add(Types.XML_ATTRIBUTE_VALUE);
		valueTypes.add(Types.HTML_VALUE);
		valueTypes.add(Types.RELATIONAL_VALUE);
		valueTypes.add(Types.TEXT_VALUE);
		valueTypes.add(Types.NEO4J_VALUE);
		valueTypes.add(Types.RDF_LITERAL);
		valueTypes.add(ENTITY_GENERIC);
		valueTypes.add(Types.ENTITY_PERSON);
		valueTypes.add(Types.ENTITY_LOCATION);
		valueTypes.add(Types.ENTITY_ORGANIZATION);
		valueTypes.add(Types.FIRST_NAME);
		valueTypes.add(Types.EMAIL);
		valueTypes.add(Types.HASHTAG);
		valueTypes.add(Types.DATE);
		valueTypes.add(Types.MENTION);
		return valueTypes;
	}

	public static ArrayList<Types> getNonValueTypesAsList() {
		// (0,1,3,7,17,31,33,34)
		ArrayList<Types> nonValueTypes = new ArrayList<>();
		nonValueTypes.add(Types.DATA_SOURCE);
		nonValueTypes.add(Types.JSON_STRUCT);
		nonValueTypes.add(Types.HTML_NODE);
		nonValueTypes.add(Types.RELATIONAL_STRUCT);
		nonValueTypes.add(Types.XML_TAG_NODE);
		nonValueTypes.add(Types.XML_ATTRIBUTE_NODE);
		nonValueTypes.add(Types.NEO4J_ENTITY);
		nonValueTypes.add(Types.NEO4J_EDGE);
		nonValueTypes.add(Types.NORMALIZATION_NODE);
		nonValueTypes.add(Types.NORMALIZATION_NODE_EXTRACTION);
		return nonValueTypes;
	}

//	public static ArrayList<Types> getRecordTypesAsList() {
//		ArrayList<Types> recordTypes = new ArrayList<>();
//		recordTypes.add(Types.RECORD_PERSON);
//		recordTypes.add(Types.RECORD_ORGANIZATION);
//		recordTypes.add(Types.RECORD_LOCATION);
//		recordTypes.add(Types.RECORD_PRODUCT);
//		recordTypes.add(Types.RECORD_EVENT);
//		recordTypes.add(Types.RECORD_CREATIVE_WORK);
//		recordTypes.add(Types.RECORD_OTHER);
//		return recordTypes;
//	}

	/** The node's representative. */
	protected transient Supplier<Node> representative;

	/** For entity nodes, this is the one they are extracted from **/
	protected Node extractedFrom;

	/** The node's cached hash code. */
	private final int hash;

	/** the minimum length for long labels and id */
	private int p_len = Config.getInstance().getIntProperty("node_label_matching_length", 40);

	/** node type */
	protected Node.Types nodeType;

	/** node context */
	private int context;

	/**
	 * The next  constructors instantiate a new node. To be used during
	 * registration.
	 *
	 * @param id    the node's ID
	 * @param label the node label
	 * @param rep   the representative
	 */

	public Node(NodeID id, String label, DataSource ds, Supplier<Node> rep, @Nullable Node.Types nodeType) {
		super(id, label, ds);
		this.representative = rep;
		this.hash = Objects.hash(id);
		if (nodeType != null) {
			this.setNodeType(nodeType);
		}
		this.normalizedLabel = genericNormalization(label);
		//log.info("(" + this.getClass().getSimpleName() + ") initially normalized into "  + normalizedLabel); 
	}

	public Node(NodeID id, String label, DataSource ds, Supplier<Node> rep, int context, @Nullable Node.Types nodeType) {
		super(id, label, ds);
		this.representative = rep;
		this.hash = Objects.hash(id);
		if (nodeType != null) {
			this.setNodeType(nodeType);
		}
		this.normalizedLabel = genericNormalization(label);
		this.context = context;
	}

	/**
	 * This constructor is used to create a node out of the storage.
	 * normaLabel has been computed previously, and  at this point,
	 * it is just taken out of the storage)
	 */
	public Node(NodeID id, String label, String normaLabel, DataSource ds, Supplier<Node> rep,
			@Nullable Node.Types nodeType) {
		super(id, label, ds);
		this.representative = rep;
		this.hash = Objects.hash(id);
		this.normalizedLabel = normaLabel;
		if (nodeType != null) {
			this.setNodeType(nodeType);
		}
	}
	/** As above but creates a self-represented node.
	 * This avoids a call to resolveNodes() to find the representative */
	public Node(NodeID globalId, String label, String normaLabel, DataSource dataSource,
			Types nodeType) {
		super(globalId, label, dataSource);
		representative = ()-> this;
		this.hash = Objects.hash(id);
		normalizedLabel = normaLabel;
		if (nodeType != null) {
			this.setNodeType(nodeType);
		}
	}
	/** Self-represented node, to avoid a call to resolveNodes() to find the representative.
	 * This is used to create entity occurrence nodes.  */
	public Node(NodeID id, String label, DataSource d, Node.Types nodeType) {
		super(id, label, d);
		representative = ()-> this;
		this.hash = Objects.hash(id);
		if (nodeType != null) {
			this.setNodeType(nodeType);
		}
	}

	public int getContext() {
		return this.context;
	}

	public void setContext(int context) {
		this.context = context;
	}

	/**
	 * @return the node representative
	 */
	public Node getRepresentative() {
		return this.representative == null ? this : this.representative.get();
	}

	/**
	 * Updates the node's representative.
	 *
	 * @param rep the new representative.
	 */
	public void updateRepresentative(Node rep) {
		checkArgument(rep != null);
		this.representative = rep.representative;
	}

	/**
	 * @return always true {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.Item#isNode()
	 */
	@Override
	public boolean isNode() {
		return true;
	}

	@Override
	public boolean isEntity() { return (this.nodeType == Types.ENTITY_PERSON || this.nodeType == Types.ENTITY_LOCATION || this.nodeType == Types.ENTITY_ORGANIZATION || this.nodeType == ENTITY_GENERIC); }

	@Override
	public boolean isValue() { return this.nodeType == Types.JSON_VALUE || this.nodeType == Types.HTML_VALUE || this.nodeType == Types.RDF_LITERAL || this.nodeType == Types.RELATIONAL_VALUE || this.nodeType == Types.XML_TAG_VALUE || this.nodeType == Types.XML_ATTRIBUTE_VALUE || this.nodeType == Types.NEO4J_VALUE || this.isEntity() || this.isExtractedType(this.nodeType); }

	@Override
	public boolean isExtractedLabel() { return this.label.equals(Edge.EXTRACTED_PERSON_LABEL) || this.label.equals(Edge.EXTRACTED_LOCATION_LABEL) || this.label.equals(Edge.EXTRACTED_ORGANIZATION_LABEL)
			|| this.label.equals(Edge.EXTRACTED_FIRSTNAME_LABEL)
			|| this.label.equals(Edge.EXTRACTED_GENERIC_LABEL)
			|| this.label.equals(Edge.EXTRACTED_EMAIL_LABEL) || this.label.equals(Edge.EXTRACTED_HASHTAG_LABEL) || this.label.equals(Edge.EXTRACTED_DATE_LABEL) || this.label.equals(Edge.EXTRACTED_MENTION_LABEL)
			|| this.label.equals(Edge.EXTRACTED_URI_LABEL); }

	@Override
	public boolean equals(Object n) {
		if (n == this) {
			return true;
		}
		return n instanceof Node && this.id.equals(((Node) n).getId());
	}

	@Override
	public NodeID getId() {
		return (NodeID) this.id;
	}

	@Override
	public int hashCode() {
		return hash;
	}

	@Override
	public Types getType() {
		return getNodeType();
	}

	/**
	 * @return PatternForPrefixExtraction French if locale =fr and English
	 *         otherwise. This patternForPrefix is not static, since it changed
	 *         according to the language.
	 */
	public static String getPatternForPrefixExtraction() {
		String frenchPatternForPrefixExtraction = "^(" + frenchStopWordPattern + "|" + URLStartPattern + ")";
		String englishPatternForPrefixExtraction = "^(" + englishStopWordPattern + "|" + URLStartPattern + ")";

		if (Config.getInstance().getProperty("default_locale").equals("fr")) {
			return frenchPatternForPrefixExtraction;
		} else {
			return englishPatternForPrefixExtraction;
		}

	}

	/** returns node type */
	public Types getNodeType() {
		return nodeType;
	}

	public void setNodeType(Types nodeType) {
		this.nodeType = nodeType;
	}

	/**
	 * The labelPrefix is a string derived from the nodes'
	 * (normalized) label, that can be used as a filter to
	 * reduce the pairs of nodes that are actually compared
	 * for similarity. 
	 * @return
	 */
	public String getLabelPrefix() {
		if (requiredPrefixLength < 0) {
			return normalizedLabel;
		}
		if (this.getType().equals(Types.RDF_URI) && labelComponents1 != null) {
			return labelComponents1.get(labelComponents1.size() - 1);

		}
		return ((normalizedLabel.length() >= requiredPrefixLength) ? normalizedLabel.substring(0, requiredPrefixLength)
				: normalizedLabel);
	}

	@Override
	public JSONObject serialize() {
		JSONObject result = new JSONObject();
		result.put("global_id", this.id.value());
		String l = Strings.nullToEmpty(this.label);
		l = sanitizeStringLabel();
		if (getType() == Types.DATA_SOURCE) {
			l = "DataSource " + getDataSource().getID();
		}
		if (Strings.isNullOrEmpty(l)) {
			l = "";
		}
		result.put("label", l);
		result.put("type", getType());
		return result;
	}

	/**
	 * Used for drawing/printing purposes only:
	 * it trims very long labels into short, more manageable
	 * ones.
	 */
	public String sanitizeStringLabel() {
		String sanitizedLabel = label.replaceAll("\"|\\{|\\}|\\[|\\]", ""); // avoiding characters that might break JSON
		// tree serialization
		if (nodeType.equals(Types.RDF_LITERAL)) {
			int firstOffender = sanitizedLabel.indexOf("^^");
			if (firstOffender > 0) {
				sanitizedLabel = sanitizedLabel.substring(0, firstOffender);
			}
		}
		if (sanitizedLabel.length() > p_len) {
			sanitizedLabel = sanitizedLabel.substring(0, (p_len * 2) / 3) + " [...] "
					+ sanitizedLabel.substring(sanitizedLabel.length() - (p_len / 3));
			return sanitizedLabel;
		}
		return sanitizedLabel;
	}

	/**
	 * This is used to create an entity with the right (most
	 * useful) string, e.g., avoiding "d' " or "Mme..."
	 */
	public static String entityLabelPreprocessing(String s) {
		String normal = s.trim().replaceAll(getPatternForPrefixExtraction(), "");
		normal = normal.replaceAll("(\\s)+", " "); 
		if (normal.length() > 0 && normal.charAt(normal.length() - 1) == '.') {
			normal = normal.substring(0, normal.length() - 1);
		}
		return normal;
	}

	/**
	 * This applies a set of generic normalization steps
	 * that aim to maximize the capacity of approximate
	 * search to succeed within a node's label: we actually
	 * search in the normalized label which removes
	 * punctuation and special characters, moves to lower case
	 * etc. It is used to set the normalized label of all
	 * Nodes by default. One can then override it if desired
	 * with the specificNormalization(). 
	 */
	public static String genericNormalization(String s) {
		String normal = s.toLowerCase().trim().replaceAll(getPatternForPrefixExtraction(), "");
		normal = normal.replaceAll("(#|@|/|-|_|\\.|,|\")", " ");
		normal = normal.replaceAll("(\\s)+", " ").trim();
		return normal;
	}

	public String getNormalizedLabel() {
		return normalizedLabel;
	}

	/**
	 * This implements class-specific normalization steps. 
	 * Specializations of the Node class that need other 
	 * normalizations than genericNormalizationPreprocess(...)
	 * should redefine this method, and should call it 
	 * towards the end of their constructors (to allow the
	 * generic method to apply first.)
	 * 
	 * Specific normalization methods can start from the 
	 * result of the generic normalization, or they can 
	 * re-start from the label itself (as is the case for 
	 * PersonOccurrence nodes). 
	 *
	 * @return
	 */
	void specificNormalization() {
		
	}

	/**
	 * Returns a short, human-readable ID.
	 */
	public String shortID() {
		String s = this.getId().toString().replaceAll("NumericNodeID", "").replace("(", "").replace(")", "");
		s = s + "@ds" + this.getDataSource().getID();
		return s;
	}

	public Edge buildEdge(Node to) {
		return getDataSource().buildUnlabelledEdge(this, to, 1.0);
	}

	public Edge buildEdge(Node to, String l) {
		return getDataSource().buildLabelledEdge(this, to, l, 1.0);
	}

	public Edge buildSameAsEdge(Node to) {
		return getDataSource().buildSameAsEdge(this, to, 1.0);
	}

	public Edge buildSameAsEdge(Node to, double conf) {
		return getDataSource().buildSameAsEdge(this, to, conf);
	}

	public void setExtractedFrom(Node source) {
		this.extractedFrom = source;
	}

	public Object getExtractedFrom() {
		return extractedFrom;
	}

	public void setNormalizedLabel(String normaLabel) {
		this.normalizedLabel = normaLabel;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns true if at least one of the given types correspond the the type of the node.
	 * @param types the list of types to be compared with the node's type.
	 * @return
	 */
	public boolean typeEquals(Types... types) {
		for(Types type : types) {
			if(this.nodeType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	public boolean typeEquals(ArrayList<Types> types) {
		for(Types type : types) {
			if(this.nodeType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	public boolean typeEquals(ArrayList<Types> types, Types... moreTypes) {
		types.addAll(Arrays.asList(moreTypes));
		for(Types type : types) {
			if(this.nodeType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	/** Normalization helper */
	protected boolean isFileExtension(String s) {
		if (s.equals("json") || s.equals("nt") || s.equals("xml") || s.equals("csv") || s.equals("txt") ||
				s.equals("text") || s.equals("html") || s.equals("htm") || s.equals("njn") || s.equals("nje")) {
			return true; 
		}
		return false; 
	}

	@Override
	public String toString() {
		return "Node(" + this.id.value() + ", " + sanitizeStringLabel() + ", " + this.nodeType + ")@ds:" + getDataSource().getID();
	}
}

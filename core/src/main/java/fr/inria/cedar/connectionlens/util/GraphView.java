/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import java.util.Set;

public interface GraphView {
    void draw(Set<Node> nodes, Set<Edge> edges, String graphName, String nodeCollectionName, String edgeCollectionName);
}

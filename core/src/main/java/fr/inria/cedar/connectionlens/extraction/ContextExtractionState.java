package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.graph.Node;

/** This class encapsulates the state of extraction for one given context,
 *  as per Théo's TLDKS paper 
 * @author ioanamanolescu
 *
 */
public class ContextExtractionState {
	public static enum State {
		EXTRACT, 
		MODEL, 
		NONE,
	};
	int context; 
	Node.Types predictedEntityType;  
	State state; 
	
			
	// initialization
	public ContextExtractionState(int context) {
		this.context = context;
		this.state = State.EXTRACT; 
	}
	
	public void transitionExtractModel() {
		if (this.state == State.EXTRACT) {
			this.state = State.MODEL; 
		}
		else {
			throw new IllegalStateException("Wrong transition!"); 
		}
	}
	
	public void transitionExtractNone() {
		if (this.state == State.EXTRACT) {
			this.state = State.NONE;
		}
		else {
			throw new IllegalStateException("Wrong transition!"); 
		}
	}
	
	public State state() {
		return this.state; 
	}
}

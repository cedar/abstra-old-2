package fr.inria.cedar.connectionlens.extraction;

import static java.util.Objects.requireNonNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.SilentStatsCollector;

public class CacheExtractor extends TopLevelExtractor {
	/** The delegate extractor. */
	private final EntityExtractor extractor;

	/** The cache in Memory */
	private final HashMap<String, Collection<Entity>> memoryCache;

	/** the location of cache in disk */
	private String pathCacheLocation;

	/** number of max label cached in memory (naturally, in the current run) */
	private int memoryCacheSize;
	/** number of max label cached on disk *also during the current run* */
	private int diskCacheSize;

	/** if true: cache all in memory */
	private boolean cacheAllInMemory;
	/** if true: cache all on disk */
	private boolean cacheAllOnDisk;

	/** The JSON(de)serializer. */
	private transient final Gson gson = new GsonBuilder().serializeNulls().create();

	private Logger log = Logger.getLogger(CacheExtractor.class);

	public static final int CACHED_NONE = 1;
	public static final int CACHED_RESULTS = 2;
	public static final int NEW_BATCHED = 3;

	/**
	 * Instantiates a new cached entity extractor.
	 *
	 * @param wrapped the wrapped
	 * @param stats   the stats
	 */
	public CacheExtractor(EntityExtractor wrapped, StatisticsCollector stats, int memoryCacheSize, int diskCacheSize) {
		this.extractor = wrapped;
		this.stats = stats;
		this.memoryCacheSize = memoryCacheSize;
		this.diskCacheSize = diskCacheSize;
		this.memoryCache = new HashMap<>();
		try {
			this.pathCacheLocation = prepareCacheLocation(extractor.getExtractorName(),
					extractor.getLocale().getLanguage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		cacheAllInMemory = (memoryCacheSize < 0);
		cacheAllOnDisk = (diskCacheSize < 0);
		if (((0 <= diskCacheSize) && (diskCacheSize < memoryCacheSize)) 
				|| (cacheAllInMemory && !cacheAllOnDisk)) {
			log.warn("Not all memory-cached extraction results will be saved on disk for future use. " + 
					"You may increase max_extraction_disk_cache and/or decrease max_extraction_mem_cache"); 						
		}
		log.info("Created " + getExtractorName() + " with locale: " + getLocale()); 
	}

	public CacheExtractor(EntityExtractor wrapped) {
		this(wrapped, new SilentStatsCollector(), -1, -1);
		log.warn(this.getExtractorName() + " created with unbounded memory and disk cache");
	}

	@Override
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		Collection<Entity> cached = extractionCacheLookup(ds, input, structuralContext);
		if (cached != null) {
			return cached;
		} else {
			return extractAndCache(ds, null, input, structuralContext);
		}
	}

	/** Just the cache look-up. */
	protected Collection<Entity> extractionCacheLookup(DataSource ds, String input, int structuralContext) {
		// log.info(input + " | cache look-up ");
		Collection<Entity> results = null;
		String hash = makeHashKey(input);
		File cached = new File(Paths.get(this.pathCacheLocation, hash).toString());
		/** found results on memory_cache, return and nothing to do */
		if (memoryCache.containsKey(hash)) {
			results = memoryCache.get(hash);
			//log.info("Found " + input + " with " + results.size() + " entities");
		}
		/** look for results on disk */
		// if cache exists on disk, read the output from cache disk and put it into the memory if cacheSize > 0
		else if (cached.exists()) {
			// log.info("input "+input+" exists in cache");
			try (FileReader in = new FileReader(cached)) {
				//log.info("Reading file " + cached + " for " + input); 
				Entity[] entities = gson.fromJson(in, Entity[].class);
				if (entities != null) {
					//log.info("Found " + entities.length + " disk cache results on " + input);
					if (cacheAllInMemory || memoryCacheSize > 0) {
						memoryCache.put(hash, Lists.newArrayList(entities));
						memoryCacheSize--;
						//log.info("We loaded these in memory, " + (cacheAllInMemory?"caching all":(memoryCacheSize + " left")));
					}
					results = Lists.newArrayList(entities);
				}
				else {
					//log.info("Found nothing on " + input); // thus, the null result is enough
				}
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}
		return results;
	}

	/** Actual extraction (no batch, just 1 input) */
	private Collection<Entity> extractAndCache(DataSource ds, Node n, String input, int structuralContext) {
		// log.info(input + " | actual extract and cache");
		Collection<Entity> extractedEntities = extractor.run(ds, input, structuralContext);
		cache(input, extractedEntities);
		return extractedEntities;
	}

	/** Just writes in cache */
	void cache(String input, Collection<Entity> extractedEntities) {
		String inputHash = makeHashKey(input);
		// previously:
		// String inputHash = makeDiskHashKey(input);
		boolean cachedInMemory=false, cachedOnDisk=false;
		if (cacheAllInMemory || memoryCacheSize > 0) {
			memoryCache.put(inputHash, Lists.newArrayList(extractedEntities));
			memoryCacheSize--;
			cachedInMemory = true;
			//log.info("Cached " +  extractedEntities.size() + " from " + input + " in memory, " +
			//		(cacheAllInMemory?"caching all":(memoryCacheSize + " left")));
		}
		if (cacheAllOnDisk || diskCacheSize > 0) {
			File cached = new File(Paths.get(this.pathCacheLocation, inputHash).toString());
			try (FileWriter out = new FileWriter(cached)) {
				out.write(gson.toJson(this.memoryCache.get(inputHash)));
				out.close();
			} catch (IOException e) {
				throw new IllegalStateException(e.toString()); 
			}
			diskCacheSize --; 
			cachedOnDisk = true; 
			//log.info("Cached " +  extractedEntities.size() + " from " + input + " on disk, " +
			//		(cacheAllOnDisk?"caching all":(diskCacheSize + " left")));
		}
		if (!cachedInMemory && !cachedOnDisk) {
			//log.info("Memory and disk cache full, no cache for: " + input);
		}
	}

	/** Ioana's key mechanism, 3/12/2020 */
	private String makeHashKey(String input) {
		try {
			return toHexString(MessageDigest.getInstance("MD5").digest(input.getBytes(Charsets.UTF_8)));
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public ArrayList<Pair<Node, Collection<Entity>>> run(ArrayList<Pair<Node, String>> inputs) {
		// log.info("Batch extraction using " + extractor.getExtractorName() + " on " +
		// inputs.size() + " inputs");
		// This is the batch call:
		ArrayList<Pair<Node, Collection<Entity>>> actualResults = extractor.run(inputs);
		return actualResults;
	}

	@Override
	public String getExtractorName() {
		return "Cache(" + extractor.getExtractorName() + ")";
	}

	@Override
	public Locale getLocale() {
		return this.extractor.getLocale();
	}

	/**
	 * Flush memory cache into disk. The time is allotted imprecisely, to the last
	 * data source that we have seen.
	 */
	@Override
	public void flushMemoryCache() {
		log.info("Flush memory cache"); 
		int howManyFlushed = 0; 
		for (String hashInputLabel : this.memoryCache.keySet()) {
			File cached = new File(Paths.get(this.pathCacheLocation, hashInputLabel).toString());
			if (cacheAllOnDisk || diskCacheSize > 0) {
				try (FileWriter out = new FileWriter(cached)) {
					out.write(gson.toJson(this.memoryCache.get(hashInputLabel)));
					out.close();
					howManyFlushed ++; 
				} catch (IOException e) {
					e.printStackTrace();
				}
				diskCacheSize --;
			}			
		}
		//log.info("Flushed " + howManyFlushed + "/" + memoryCache.size() + " memory extraction cache entries");
	}

	/**
	 * Creates the cache location folders if those do not already exist.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static String prepareCacheLocation(String extractorName, String locale) throws IOException {
		String result = requireNonNull(Config.getInstance().getProperty("cache_location"));
		result = Paths.get(result, "extraction", extractorName, locale).toString();
		Files.createDirectories(Paths.get(result));
		return result;
	}

	/**
	 * @param bytes an array of bytes to compute a hexadecimal representation for
	 * @return an hexadecimal representation of the input byte array.
	 */
	private static String toHexString(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				result.append('0');
			}
			result.append(hex);
		}
		return result.toString();
	}
	@Override
	public Class getBasicExtractorClass() {
		return extractor.getClass(); 
	}

}

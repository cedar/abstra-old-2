package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.boundary.*;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate.GraphUpdateMethod;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate.UpdateBoolean;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate.UpdateExact;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score.*;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.CollectionGraphPrinting;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.*;
import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.REPORTED;
import static fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification.*;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.*;


public class Configuration {
    public static final Logger log = Logger.getLogger(Configuration.class);

    public enum SCORING_METHOD {
        DESC_K,
        LEAF_K,
        W_DAG,
        W_PR
    }

    public enum BOUNDARY_METHOD {
        DESC,
        LEAF,
        DAG,
        FL,
        FLAC
    }

    public enum GRAPH_UPDATE_METHOD {
        UPDATE_BOOLEAN,
        UPDATE_EXACT
    }

    public enum IDREF_EDGES_USAGE {
        DISABLE,
        ENABLE_SCORE,
        ENABLE_BOUNDARY
    }


    protected SCORING_METHOD scoringMethod;
    protected BOUNDARY_METHOD boundaryMethod;
    protected GRAPH_UPDATE_METHOD graphUpdateMethod;
    protected IDREF_EDGES_USAGE idrefEdgesUsage;
    ScoringMethod sm;
    BoundaryMethod bm;
    GraphUpdateMethod gum;
    int k;

    protected int nbCollectionsReportedCounter = 0;
    protected int Cstar = 0;
    protected double nodeCoverage = 0.0d;
    protected double collectionCoverage = 0.0d;

    ConnectionLens cl;
    DataSource ds;
    MainCollectionsIdentification mainCollectionsIdentification;

    protected StatisticsCollector localCollector;

    public Configuration(SCORING_METHOD scoringMethod, int k, BOUNDARY_METHOD boundaryMethod, GRAPH_UPDATE_METHOD graphUpdateMethod, IDREF_EDGES_USAGE idrefEdgesUsage, ConnectionLens cl, DataSource ds, MainCollectionsIdentification mainCollectionsIdentification) {
        this.scoringMethod = scoringMethod;
        this.boundaryMethod = boundaryMethod;
        this.graphUpdateMethod = graphUpdateMethod;
        this.idrefEdgesUsage = idrefEdgesUsage;
        this.k = k;
        this.cl = cl;
        this.ds = ds;
        this.mainCollectionsIdentification = mainCollectionsIdentification;
        this.initMethods();
    }

    private void initMethods() {
        // init the scoring method
        if(this.scoringMethod == SCORING_METHOD.DESC_K) {
            this.sm = new DescK(this.k, this);
        } else if(this.scoringMethod == SCORING_METHOD.LEAF_K) {
            this.sm = new LeafK(this.k, this);
        } else if(this.scoringMethod == SCORING_METHOD.W_DAG) {
            this.sm = new wDAG(this);
        } else if(this.scoringMethod == SCORING_METHOD.W_PR) {
            this.sm = new wPR(this);
        } else {
            throw new IllegalArgumentException("Please choose a scoring function among: desc-k, leaf-k, Csize, wDAG or wPR propagation.");
        }

        // init the boundary method
        if(this.boundaryMethod == BOUNDARY_METHOD.DESC) {
            this.bm = new BoundDesc(this.Cstar, this.k, this);
        } else if(this.boundaryMethod == BOUNDARY_METHOD.LEAF) {
            this.bm = new BoundLeaf(this.Cstar, this.k, this);
        } else if(this.boundaryMethod == BOUNDARY_METHOD.DAG) {
            this.bm = new BoundDAG(this.Cstar, this);
        } else if(this.boundaryMethod == BOUNDARY_METHOD.FL) {
            this.bm = new BoundFl(this.Cstar, this);
        } else if(this.boundaryMethod == BOUNDARY_METHOD.FLAC) {
            this.bm = new BoundFlAc(this.Cstar, this);
        } else {
            throw new IllegalArgumentException("Please choose a boundary function among: bound-desc, bound-leaf, bound-DAG, bound-flood, bound-solidary or bound-strong.");
        }

        // init the graph update method
        if(this.graphUpdateMethod == GRAPH_UPDATE_METHOD.UPDATE_BOOLEAN) {
            this.gum = new UpdateBoolean(this.Cstar);
        } else if(this.graphUpdateMethod == GRAPH_UPDATE_METHOD.UPDATE_EXACT) {
            this.gum = new UpdateExact(this.Cstar, this.cl, this.ds, this.mainCollectionsIdentification);
        } else {
            throw new IllegalArgumentException("Please choose a graph update method among: boolean or exact.");
        }
    }

    public void run(StatisticsCollector localCollector) throws SQLException, IOException {
        this.localCollector = localCollector;

        this.computeScores();
        this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_COMPUTE_COLLS_SCORES_MEM_DISK);
        this.setCollectionsWithTheirOriginalScore();
        while((this.nbCollectionsReportedCounter < E_MAX || E_MAX == -1) && this.nodeCoverage <= COV_MIN) {
            // create the drawing of the collection graph on which we work
            CollectionGraphPrinting collectionGraphPrinting = new CollectionGraphPrinting(this.cl, this.ds, this.mainCollectionsIdentification);
            collectionGraphPrinting.printAllSources(true);

            log.info("already reported " + this.nbCollectionsReportedCounter + " collections out of " + E_MAX + " with a coverage of " + this.nodeCoverage);
            log.info("-- PICK NEXT COLLECTION");
            this.pickNextCollection();
            this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_CHOOSE_NEXT_COLLECTION_MEM);
            if(this.Cstar == -1) { // no more collections to report
                log.info("finished. reported " + this.nbCollectionsReportedCounter + " collections");
                break;
            }

            log.info("-- COMPUTE BOUNDARY OF NEXT COLLECTION");
            CollectionGraph.getWorkingInstance().setStatusOfCollection(this.Cstar, REPORTED);
            CollectionGraph.getInstance().setStatusOfCollection(this.Cstar, REPORTED);
            CollectionBoundary collectionBoundary = this.computeBoundaryOfCstar();
            CollectionGraph.getInstance().setCollectionBoundary(this.Cstar, collectionBoundary);
            log.info("boundary of C" + this.Cstar + " is " + CollectionGraph.getInstance().getCollectionBoundary(this.Cstar));
            this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_COMPUTE_BOUNDARY_MEM);

            this.nodeCoverage = this.computeCoverageOfNodes();
            this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_COMPUTE_COVERAGE_DISK);

//            Scanner scanner = new Scanner(System.in);
//            System.out.println("Press enter to update the graph");
//            scanner.nextLine();

            log.info("-- UPDATE GRAPH");
            this.updateCollectionGraph();
            this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_UPDATE_GRAPH_DISK);


            if(this.graphUpdateMethod == GRAPH_UPDATE_METHOD.UPDATE_EXACT) { // for the boolean method, we don't need to recompute the scores.
                if(!CollectionGraph.getWorkingInstance().getCollectionsIds().isEmpty()) { // we check that there is at least one collection in the CG (when updating the graph, the last update will lead to an empty graph)
                    this.computeScores();
                }
                this.localCollector.tick(StatisticsCollector.total(), MAINCOLLS_RECOMPUTE_COLLS_SCORES_MEM_DISK);
            }
        }
    }

    public void pickNextCollection() {
        log.debug("PICK NEXT COLLECTION based on " + CollectionGraph.getWorkingInstance().getCollectionsScores());

        // we select the highest score in the collections weights.
        // for that we sort the NO_STATUS collections by their score (using a linkedList to allow duplicate scores)
        // while we have not selected a collection or not visited all the collections in the linkedlist, we continue
        // when a collection is reportable, we stop.
        // we also take care of not picking a collection that is not eligible (reported or inactive)
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsStatuses());
        ArrayList<Integer> eligibleCollectionsWithTheHighestScore = new ArrayList<>();
        LinkedList<Integer> collectionsOrderedByScore = new LinkedList<>();
        for (int i : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            if (CollectionGraph.getWorkingInstance().checkIfCollectionStatusIsIn(i, NO_STATUS) && this.isCollectionReportable(i)) {
                collectionsOrderedByScore.add(i);
            }
        }
        log.debug(collectionsOrderedByScore);
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
        collectionsOrderedByScore.sort(Collections.reverseOrder(Comparator.comparing(collId -> CollectionGraph.getWorkingInstance().getCollectionScore(collId))));
        log.debug(collectionsOrderedByScore);

        double currentScore = 0.0;
        boolean first = true;
        while(!collectionsOrderedByScore.isEmpty()) { // && eligibleCollectionsWithTheHighestScore.isEmpty()) {
            int currentCollection = collectionsOrderedByScore.peekFirst();
            collectionsOrderedByScore.removeFirst();
            if(CollectionGraph.getWorkingInstance().getCollectionScore(currentCollection) != currentScore && !first) {
                // the current collection has a different score from its predecessors so we need to stop
                // and this is not the first iteration (where in that case the current score is always different from the collection score)
                break;
            } else {
                first = false;
                eligibleCollectionsWithTheHighestScore.add(currentCollection);
                currentScore = CollectionGraph.getWorkingInstance().getCollectionScore(currentCollection);
            }
//            if(this.isCollectionReportable(nextElement)) {
//                eligibleCollectionsWithTheHighestScore.add(nextElement);
//                double valueNextElement = CollectionGraph.getWorkingInstance().getCollectionScore(nextElement);
//                Iterator<Integer> iterator = CollectionGraph.getWorkingInstance().getCollectionsScores().keySet().iterator(); // we need to use an interator to avoid the concurrentModificationError
//                while(iterator.hasNext()) {
//                    int iteratorElement = iterator.next();
//                    if (CollectionGraph.getWorkingInstance().getCollectionScore(iteratorElement) == valueNextElement) {
//                        if(this.isCollectionReportable(iteratorElement)) {
//                            eligibleCollectionsWithTheHighestScore.add(iteratorElement);
//                        }
//                    } else {
//                        break; // the values differ, so we don't need to continue to look because values are sorted
//                    }
//                }
//            } else {
//                // this collection is not reportable, we pick the next one
//            }
        }
        log.info("eligibleCollectionsWithTheHighestScore = " + eligibleCollectionsWithTheHighestScore);

        // now that we have the set of eligible next collections, we need to choose one
        if(eligibleCollectionsWithTheHighestScore.isEmpty()) {
            // no more eligible collections (all interesting collections have been reported and others have a score of 0).
            this.Cstar = -1;
        } else if(eligibleCollectionsWithTheHighestScore.size() == 1) {
            // no tie, we can select the collection as the next one
            this.Cstar = eligibleCollectionsWithTheHighestScore.get(0);
            log.info("pick next collection without hesitating: C" + this.Cstar);
        } else {
            // there is a tie, i.e. several collections have the maximal score
            // to break as much as possible the tie, we rely on (a) the number of children in the collection - "the higher, the better" and (b) the number of data nodes
            // if at the end, there is still a tie, we pick the first collection
            HashMap<Integer, Integer> nbOfChildrenForCollectionsInTheTie = new HashMap<>();
            for(int collectionId : eligibleCollectionsWithTheHighestScore) {
                nbOfChildrenForCollectionsInTheTie.put(collectionId, CollectionGraph.getWorkingInstance().getAllDescendants(collectionId, 1, true).size());
            }
            log.debug(nbOfChildrenForCollectionsInTheTie);

            int maximalNumberOfChildren = Collections.max(nbOfChildrenForCollectionsInTheTie.values());
            ArrayList<Integer> collectionsHavingTheMaximalValue = new ArrayList<>();
            for(Map.Entry<Integer, Integer> entry : nbOfChildrenForCollectionsInTheTie.entrySet()) {
                if(entry.getValue() == maximalNumberOfChildren) {
                    collectionsHavingTheMaximalValue.add(entry.getKey());
                }
            }

            if(collectionsHavingTheMaximalValue.size() == 1) {
                this.Cstar = collectionsHavingTheMaximalValue.get(0);
                log.debug("break the tie with number of children: C" + this.Cstar);
            } else {
                // we can try with the number of data nodes to break the tie
                HashMap<Integer, Integer> nbOfDataNodesForCollectionsInTheTie = new HashMap<>();
                for(int collectionId : eligibleCollectionsWithTheHighestScore) {
                    nbOfDataNodesForCollectionsInTheTie.put(collectionId, CollectionGraph.getWorkingInstance().getCollectionSize(collectionId));
                }
                log.debug(nbOfDataNodesForCollectionsInTheTie);

                int maximalNumberOfDataNodes = Collections.max(nbOfDataNodesForCollectionsInTheTie.values());
                collectionsHavingTheMaximalValue = new ArrayList<>();
                for(Map.Entry<Integer, Integer> entry : nbOfDataNodesForCollectionsInTheTie.entrySet()) {
                    if(entry.getValue() == maximalNumberOfDataNodes) {
                        collectionsHavingTheMaximalValue.add(entry.getKey());
                    }
                }

                if(collectionsHavingTheMaximalValue.size() == 1) {
                    this.Cstar = collectionsHavingTheMaximalValue.get(0);
                    log.info("break the tie with number of data nodes: C" + this.Cstar);
                } else {
                    this.Cstar = eligibleCollectionsWithTheHighestScore.get(0);
                    log.info("pick one collection randomly: C" + this.Cstar);
                }
            }
        }

        if(this.Cstar != -1) {
            this.nbCollectionsReportedCounter++;
            CollectionGraph.getWorkingInstance().setStatusOfCollection(this.Cstar, NON_REPORTABLE);
        }
    }


    public void computeScores() throws IOException, SQLException {
        log.debug("compute scores");
        this.sm.init(this);
        this.sm.compute();
    }

    public CollectionBoundary computeBoundaryOfCstar() {
        this.bm.init(this.Cstar, this);
        return this.bm.compute();
    }

    public void updateCollectionGraph() throws SQLException, IOException {
        this.gum.init(this.Cstar);
        this.gum.update();
    }

    public double getNodeCoverage() throws SQLException {
        if(this.nodeCoverage == 0.0d) {
            this.nodeCoverage = this.computeCoverageOfNodes();
        }
        return this.nodeCoverage;
    }

    public double getCollectionCoverage() {
        if(this.collectionCoverage == 0.0d) {
            this.collectionCoverage = this.computeCoverageOfCollections();
        }
        return this.collectionCoverage;
    }

    // the node coverage is defined by the number of nodes involved in the boundary of a main collection divided by the total number of nodes.
    private double computeCoverageOfNodes() throws SQLException {
        log.debug(CollectionGraph.getInstance().getCollectionsInvolvedInABoundary());
        String collectionIdsString = "(";
        collectionIdsString += StringUtils.join(CollectionGraph.getInstance().getCollectionsInvolvedInABoundary(), "),(");
        collectionIdsString += ")";
        log.debug(collectionIdsString);
        if(collectionIdsString.equals("()")) {
            return Double.NEGATIVE_INFINITY;
        } else {
            PreparedStatement stmt = CollectionGraph.getInstance().getGraph().getPreparedStatement("SELECT COUNT(c.nodeId) " +
                    "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c " +
                    "WHERE c.collId IN (VALUES "+collectionIdsString+");");
            ResultSet rs1 = stmt.executeQuery();
            rs1.next();
            stmt = CollectionGraph.getInstance().getGraph().getPreparedStatement("SELECT COUNT(c.nodeId) " +
                    "FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c;"); // TODO NELLY: store the number of nodes just after computing the collection graph
            ResultSet rs2 = stmt.executeQuery();
            rs2.next();
            return ((double) rs1.getInt(1)) / ((double) rs2.getInt(1));
        }
    }

    private double computeCoverageOfCollections() {
        HashSet<Integer> collectionsInReporting = new HashSet<>();
        collectionsInReporting.addAll(CollectionGraph.getInstance().getMainCollections());
        collectionsInReporting.addAll(CollectionGraph.getInstance().getCollectionsInvolvedInABoundary());
        int collectionsInReportingSize = collectionsInReporting.size(); // contains the maine entity collection roots + collections involved in at least one boundary
        return ((double) collectionsInReportingSize) / CollectionGraph.getInstance().getCollectionsIds().size();
    }

    public boolean isCollectionReportable(int collection) {
        if(!CollectionGraph.getWorkingInstance().getCollectionGraph().containsKey(collection) && !CollectionGraph.getWorkingInstance().getLeafCollections().contains(collection)) {
            // leaf collections do not appear in the keySet,
            // thus we need to check that the collection is also not present in leaf collections to be sure it does not exist anymore in the collection graph
            log.debug("collection " + collection + " is not selected because it does not exist anymore in the collection graph.");
            CollectionGraph.getWorkingInstance().setStatusOfCollection(collection, NON_REPORTABLE);
            return false;
        } else if(CollectionGraph.getWorkingInstance().getCollectionSize(collection) == 1) {
            log.debug("collection C" + collection + " is not selected because it contains one element only.");
            CollectionGraph.getWorkingInstance().setStatusOfCollection(collection, NON_REPORTABLE);
            return false;
        } else if((this.scoringMethod == SCORING_METHOD.W_DAG || this.scoringMethod == SCORING_METHOD.W_PR)
                && CollectionGraph.getWorkingInstance().getCollectionsScores().containsKey(collection) && CollectionGraph.getWorkingInstance().getCollectionScore(collection) <= 0.0d) { // for other methods, we agree to have collections weights of 0
            log.debug("collection C" + collection + " is not selected because it has score of 0.");
            CollectionGraph.getWorkingInstance().setStatusOfCollection(collection, NON_REPORTABLE);
            return false;
            // 31 aout 2022: we don't want to disable collections involved in  boundary, they maybe be worth it to report
//        } else if(CollectionGraph.getInstance().checkIfCollectionIsInvolvedInBoundary(collection)) {
//            log.debug("collection C" + collection + " is not selected because it is involved in a boundary.");
//            return false;
        }
        return true;
    }

    private void setCollectionsWithTheirOriginalScore() {
        for(Integer collId : CollectionGraph.getWorkingInstance().getCollectionsIds()) {
            CollectionGraph.getInstance().setCollectionScore(collId, CollectionGraph.getWorkingInstance().getCollectionScore(collId));
        }
    }

    public SCORING_METHOD getScoringMethod() {
        return this.scoringMethod;
    }

    public BOUNDARY_METHOD getBoundaryMethod() {
        return this.boundaryMethod;
    }

    public GRAPH_UPDATE_METHOD getGraphUpdateMethod() {
        return this.graphUpdateMethod;
    }

    public IDREF_EDGES_USAGE getIdrefEdgesUsage() {
        return this.idrefEdgesUsage;
    }

    public int getK() {
        return this.k;
    }
}

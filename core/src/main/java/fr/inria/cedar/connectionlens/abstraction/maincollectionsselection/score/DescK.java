package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

public class DescK extends ScoringMethod {
    public static final Logger log = Logger.getLogger(DescK.class);
    private int k;

    public DescK(int k, Configuration configuration) {
        super(configuration);
        this.k = k;
    }

    @Override
    public void compute() {
        // get the number of descendants of each collection
        for(int collectionId : CollectionGraph.getInstance().getCollectionsIds()) {
            double nbDescendants;
            if(CollectionGraph.getInstance().getCollectionGraph().containsKey(collectionId)) {
                nbDescendants = CollectionGraph.getInstance().getAllDescendants(collectionId, this.k, true).size();
                log.debug("number of descendants at depth " + this.k + " for " + collectionId + " is " + nbDescendants);
            } else {
                // leaf collections have no descendant
                nbDescendants = 0.0d;
            }
//            this.scores.put(collectionId, nbDescendants);
            CollectionGraph.getWorkingInstance().setCollectionScore(collectionId, nbDescendants);
        }
//        log.debug(this.scores);
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsScores());
    }
}

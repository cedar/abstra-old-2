package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

public class XMLGenerator {

    public static final Logger log = Logger.getLogger(XMLGenerator.class);

    private static int totalNumberOfTags;

    public XMLGenerator() {

    }

    public static void main(String[] args) throws IOException {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";

        int nbPaper = 1000;
        int nbAuthor = 10000;
        int nbConference = 20;
        boolean writtenBy = true;
        boolean publishedIn = true;
        boolean hasPublished = true;

        totalNumberOfTags = 0;

        String filename = "dataset-P"+nbPaper+".xml";
        File dataset = new File(Paths.get(Config.getInstance().getStringProperty("temp_dir"), filename).toString());
//        log.debug(dataset.getAbsolutePath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(dataset));

        generateData(nbPaper, nbAuthor, nbConference, writtenBy, publishedIn, hasPublished, chars, numbers, bw);
        bw.flush();
        bw.close();

        log.info("number of tags generated: " + totalNumberOfTags);
    }

    private static void generateData(int nbPaper, int nbAuthor, int nbConference, boolean writtenBy, boolean publishedIn, boolean hasPublished, String chars, String numbers, BufferedWriter bw) throws IOException {
        Random rand = new Random();
        ArrayList<String> authorsList = new ArrayList<>();
        ArrayList<String> conferencesList = new ArrayList<>();

        bw.write("<root>\n");
        totalNumberOfTags++;

        log.info("generating list " + nbAuthor + " <author>");
        bw.write("<authors>\n");
        totalNumberOfTags++;
        if(writtenBy || hasPublished) {
            for(int i = 0 ; i < nbAuthor ; i++) {
                String authorId = "a"+i;
                authorsList.add("a"+i);
                bw.write("<author id=\"" + authorId + "\">\n" +
                        "  <name>" + RDFGenerator.generateRandomName(chars) + "</name>\n" +
                        "</author>\n");
                totalNumberOfTags+=2;
            }
        }
        bw.write("</authors>\n");
        totalNumberOfTags++;


        log.info("generating list " + nbConference + " <conference>");
        bw.write("<conferences>\n");
        if(publishedIn) {
            for(int i = 0 ; i < nbConference ; i++) {
                String conferenceId = "c"+i;
                conferencesList.add(conferenceId);
                bw.write("<conference id=\"" + conferenceId + "\">\n" +
                        "  <name>" + RDFGenerator.generateRandomConference(chars) + "</name>\n" +
                        "  <year>" + RDFGenerator.generateRandomYear(numbers) + "</year>\n" +
                        "</conference>\n");
                totalNumberOfTags+=3;
            }
        }
        bw.write("</conferences>\n");
        totalNumberOfTags++;

        log.info("generating data for " + nbPaper + " <paper>");
        bw.write("<papers>\n");
        totalNumberOfTags++;
        for(int i = 0 ; i < nbPaper ; i++) {
            bw.write("<paper id=\"p" + i + "\">\n");
            bw.write("  <title>" + RDFGenerator.generateRandomTitle(chars) + "</title>\n");
            bw.write("  <year>" + RDFGenerator.generateRandomYear(numbers) + "</year>\n");
            bw.write("  <DOI>" + RDFGenerator.generateRandomWord(chars, 10) + "</DOI>\n");
            totalNumberOfTags+=4;
            if(publishedIn) {
                bw.write("  <publishedIn conferenceid=\"" + conferencesList.get(rand.nextInt(nbConference))+"\"/>\n");
                totalNumberOfTags++;
            }
            if(writtenBy) {
                Random rand2 = new Random();
                for(int nbWrittenBy = 0 ; nbWrittenBy < rand2.nextInt(10) ; nbWrittenBy++) {
                    bw.write("  <writtenBy authorid=\"" + authorsList.get(rand.nextInt(nbAuthor))+"\"/>\n");
                    totalNumberOfTags++;
                }
            }
            bw.write("</paper>\n");
            totalNumberOfTags++;
        }
        bw.write("</papers>\n");
        totalNumberOfTags++;
        bw.write("</root>\n");
        totalNumberOfTags++;
    }
}

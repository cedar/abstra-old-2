package fr.inria.cedar.connectionlens.abstraction.classification;

import fr.inria.cedar.connectionlens.graph.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

// class to define the set of categories in which we want to classify records and collections
public class Categories {
    HashMap<Integer, ClassificationCategory> classificationCategoriesIdToCategory; // < category id, category >
    HashMap<String, ClassificationCategory> classificationCategoriesLabelToCategory; // < category label, category >
    ArrayList<Integer> defaultCategories; // < category ids >

    public Categories() {
        this.classificationCategoriesIdToCategory = new HashMap<>();
        this.classificationCategoriesLabelToCategory = new HashMap<>();
        this.defaultCategories = new ArrayList<>();
    }

    public void add(String categoryLabel) {
        int currentMaxId = Collections.max(this.classificationCategoriesIdToCategory.keySet());
        this.add(currentMaxId+1, categoryLabel);
    }

    public void add(Integer categoryId, String categoryLabel) {
        ClassificationCategory category = new ClassificationCategory(categoryId, categoryLabel, false, new ArrayList<>());
        this.classificationCategoriesIdToCategory.put(categoryId, category);
        this.classificationCategoriesLabelToCategory.put(categoryLabel, category);
    }

    public void add(Integer categoryId, String categoryLabel, boolean isADefaultCategory) {
        ClassificationCategory category = new ClassificationCategory(categoryId, categoryLabel, isADefaultCategory, new ArrayList<>());
        this.classificationCategoriesIdToCategory.put(categoryId, category);
        this.classificationCategoriesLabelToCategory.put(categoryLabel, category);
        if(isADefaultCategory) {
            this.defaultCategories.add(categoryId);
        }
    }

    public int addDefaults() {
        int categoryId = 0;
        for(int i = 0 ; i < Node.Types.values().length ; i++) {
            this.add(categoryId, Node.Types.values()[i].name(), true);
            categoryId++;
        }
        return categoryId;
    }

    public ClassificationCategory getCategory(Integer categoryId) {
        return this.classificationCategoriesIdToCategory.get(categoryId);
    }

    public ClassificationCategory getCategory(String categoryLabel) {
        return this.classificationCategoriesLabelToCategory.get(categoryLabel);
    }

    public int getCategoryId(String categoryLabel) {
        if(this.classificationCategoriesLabelToCategory.containsKey(categoryLabel)) {
            return this.classificationCategoriesLabelToCategory.get(categoryLabel).id;
        } else {
            return -1;
        }
    }

    String getCategoryLabel(int categoryId) {
        if(this.classificationCategoriesIdToCategory.containsKey(categoryId)) {
            return this.classificationCategoriesIdToCategory.get(categoryId).label;
        } else {
            return "";
        }
    }

    String getCategoryInformativeLabel(Integer categoryId) {
        if(this.classificationCategoriesIdToCategory.containsKey(categoryId)) {
            return this.classificationCategoriesIdToCategory.get(categoryId).informativeLabel;
        } else {
            return "";
        }
    }

    int getCategoryId_CATEGORY_PERSON() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_PERSON").id;
    }

    int getCategoryId_CATEGORY_LOCATION() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_LOCATION").id;
    }

    int getCategoryId_CATEGORY_ORGANIZATION() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_ORGANIZATION").id;
    }

    int getCategoryId_CATEGORY_PRODUCT() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_PRODUCT").id;
    }

    int getCategoryId_CATEGORY_EVENT() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_EVENT").id;
    }

    int getCategoryId_CATEGORY_CREATIVE_WORK() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_CREATIVE_WORK").id;
    }

    int getCategoryId_CATEGORY_OTHER() {
        return this.classificationCategoriesLabelToCategory.get("CATEGORY_OTHER").id;
    }

    Set<Integer> keySet() { // by default, the keySet is the set of category ids
        return this.classificationCategoriesIdToCategory.keySet();
    }

    Set<Integer> keySetId() {
        return this.classificationCategoriesIdToCategory.keySet();
    }

    Set<String> keySetLabel() {
        return this.classificationCategoriesLabelToCategory.keySet();
    }

    public HashMap<Integer, ClassificationCategory> getClassificationCategoriesIdToCategory() {
        return this.classificationCategoriesIdToCategory;
    }

    public HashMap<String, ClassificationCategory> getClassificationCategoriesLabelToCategory() {
        return this.classificationCategoriesLabelToCategory;
    }

    public ArrayList<Integer> getDefaultCategories() {
        return this.defaultCategories;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "classificationCategoriesIdToCategory=" + this.classificationCategoriesIdToCategory +
                ", classificationCategoriesLabelToCategory=" + this.classificationCategoriesLabelToCategory +
                ", defaultCategories=" + this.defaultCategories +
                '}';
    }
}

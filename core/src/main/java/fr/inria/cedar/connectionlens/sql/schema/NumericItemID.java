/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;

import java.io.Serializable;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;

public abstract class NumericItemID implements ItemID, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4030173812497368874L;
	/**
	 * 
	 */
	protected final Integer value;

	public static final Logger log = Logger.getLogger(NumericItemID.class);
	
	NumericItemID(int v) {
		//if (v % 50000 == 0) {
		//	log.info("Created node ID " + v); 
		//}
		this.value = v;
	}

	@Override
	public boolean equals(Object other) {
		return getClass().isInstance(other) && value.equals(((NumericItemID) other).value);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	public Integer value() {
		return value; 
	}
}


/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing.lucene;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityType;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.indexing.*;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.LUCENE;
import static java.util.Locale.FRENCH;
import static org.apache.lucene.queryparser.classic.QueryParserBase.escape;

public class LuceneBasedIndex implements IndexAndProcessNodes {

	/** The logger */
	private static final Logger logger = Logger.getLogger(LuceneBasedIndex.class);

	private final Path indexPath;
	private Directory directory;
	private DirectoryReader ireader;
	private IndexSearcher isearcher;
	private final Analyzer analyzer;
	private final QueryParser parser;

	protected final StatisticsCollector indexingStats;
	protected final StatisticsCollector extractorStats;
	protected final StatisticsCollector abstractStats;

	private Boolean indexLater;

	/** The data source catalog. */
	private DataSourceCatalog catalog;

	/** The node resolver. */
	private final Function<NodeID, Node> nodeResolver;

	/** The edge resolver. */
	private final Function<EdgeID, Edge> edgeResolver;

	/** The edge resolver. */
	private final Function<Node, Set<Edge>> neighborResolver;

	private final Factory factory;

	protected StatisticsCollector stats;
	protected MorphoSyntacticAnalyser mas;
	protected EntityExtractor extractor;

	private IndexAndProcessNodes abstractIndex;
	private Graph.GraphType typeOfGraph;

	public LuceneBasedIndex(Locale locale, DataSourceCatalog c, Factory f,
			Function<NodeID, Node> nr, Function<EdgeID, Edge> er, Function<Node, Set<Edge>> nbr,
			EntityExtractor ex, MorphoSyntacticAnalyser mas,
							StatisticsCollector extractorStats, StatisticsCollector indexStats, StatisticsCollector abstractStats, Graph.GraphType typeOfGraph) {
		this.catalog = c;
		this.factory = f;
		this.nodeResolver = nr;
		this.edgeResolver = er;
		this.neighborResolver = nbr;
		this.mas = mas;
		this.extractor = ex;
		this.extractorStats = extractorStats;
		this.indexingStats = indexStats;
		this.abstractStats = abstractStats;
		this.analyzer = locale.equals(FRENCH) ? new FrenchAnalyzer() : new StandardAnalyzer();
		this.parser = new QueryParser("label", analyzer);

		Config config = Config.getInstance();
		this.indexPath = Paths.get(config.getProperty("lucene_base_dir"),
				config.getProperty("RDBMS_DBName"));
		this.abstractIndex = null;
		this.typeOfGraph = typeOfGraph;
		try {
			if (!Files.exists(indexPath)) {
				this.directory = FSDirectory.open(Files.createDirectories(indexPath));
				init();
			} else {
				this.directory = FSDirectory.open(indexPath);
			}
			update();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

//	public LuceneBasedIndex(Locale locale, DataSourceCatalog c, Factory f,
//							Function<NodeID, Node> nr, Function<EdgeID, Edge> er, Function<Node, Set<Edge>> nbr,
//							EntityExtractor ex, MorphoSyntacticAnalyser mas,
//							StatisticsCollector indexingStats, StatisticsCollector extractStats, boolean b) {
//		this(locale,c,f,nr,er,nbr,ex,mas,extractStats, indexingStats,false);
//	}
	
	private void init() {
		try(IndexWriter iwriter = new IndexWriter(directory, new IndexWriterConfig(analyzer))) {
			Document doc = new Document();
			doc.add(new Field("cl-instance", indexPath.toString(), TextField.TYPE_NOT_STORED));
			iwriter.addDocument(doc);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void reset() {
		try {
			if (Files.exists(indexPath)) {
				//IOUtils.rm(indexPath); this function seems not working on different OS than macOS.
				File indexPathURI = new File (indexPath.toString());
				deleteDirectory(indexPathURI);
			}
			this.directory = FSDirectory.open(Files.createDirectories(indexPath));
			init();
			update();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	private String format(QueryComponent query) {
		if (query instanceof NGram) {
			return '"' + Joiner.on(" ").join(Iterables.transform((NGram) query, this::format)) + '"';
		}
		if (query instanceof Conjunction) {
			return Joiner.on(" && ").join(Iterables.transform((Conjunction) query, this::format));
		}
		if (query instanceof Disjunction) {
			return Joiner.on(" || ").join(Iterables.transform((Disjunction) query, this::format));
		}
		Preconditions.checkState(query instanceof AtomicKeyword);
		return escape(query.value()).trim();
	}

	/**
	 * Delete the content of the index folder
	 * @param directoryToBeDeleted
	 * @return true were content of the index folder are deleted
	 */
	private boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}
	
	@Override
	public Set<Node> getNodeMatches(QueryComponent kw, Node.Types... types) {
		final Set<Node> result = new HashSet<>();
		String str = format(kw);
		if (str.isEmpty()) {
			return result;
		}
		if (types.length > 0) {
			str = "(" + str + ") AND type:(" + Joiner.on(" || ").join(types) + ")";
		}
		str = "(" + str + ") AND is_edge:false";
		try {
			Query query = parser.parse(str);
			final Set<Node.Types> t = ImmutableSet.copyOf(types.length > 0 ? types: Node.Types.values());
			for (ScoreDoc hit : isearcher.search(query, 10).scoreDocs) {
				try {
                    Node.Types nodeType = Node.Types.valueOf(isearcher.doc(hit.doc).get("type"));
					NodeID nodeId = factory.parseNodeID(new Integer(isearcher.doc(hit.doc).get("id")));
					if (t.contains(nodeType)) {
						result.add(nodeResolver.apply(nodeId));
					}
				} catch (NoSuchElementException e) {
					// if the item is not a node, ignore.
				}
			}
		} catch (IOException | ParseException e) {
			throw new IllegalStateException(e);
		}
		if (Stream.of(types).allMatch(EntityType::isEntityType)) {
			return filterEntityParents(result);
		}
		return result;
	}
	
	private <E extends Item> Set<E> filterEntityParents(Set<E> set) {
		for (Item n: new HashSet<>(set)) {
			if (n instanceof Node && isEntityType(((Node) n).getType())) {
				for (Edge e: neighborResolver.apply((Node) n)) {
					if (e.getType() == Edge.Types.EDGE) {
						set.remove(e.getSourceNode());
					}
				}
			}
		}
		return set;
	}

	@Override
	public Set<Edge> getEdgeMatches(QueryComponent kw, Edge.Types... types) {
		final Set<Edge> result = new HashSet<>();
		String str = format(kw);
		if (str.isEmpty()) {
			return result;
		}
		if (types.length > 0) {
			str = "(" + str + ") AND type:(" + Joiner.on(" || ").join(types) + ")";
		}
		str = "(" + str + ") AND is_edge:true";
		try {
			Query query = parser.parse(str);
			final Set<Edge.Types> t = ImmutableSet.copyOf(types.length > 0 ? types: Edge.Types.values());
			for (ScoreDoc hit : isearcher.search(query, 10).scoreDocs) {
				try {
                    Edge.Types edgeType = Edge.Types.valueOf(isearcher.doc(hit.doc).get("type"));
					EdgeID edgeId = factory.parseEdgeID(new Integer(isearcher.doc(hit.doc).get("id")));
					if (t.contains(edgeType)) {
						result.add(edgeResolver.apply(edgeId));
					}
				} catch (NoSuchElementException e) {
					// if the item is not a node, ignore.
				}
			}
		} catch (IOException | ParseException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	public Set<Item> getItemMatches(QueryComponent kw) {
		final Set<Item> result = new HashSet<>();
		String str = format(kw);
		if (str.isEmpty()) {
			return result;
		}
		try {
			Query query = parser.parse(str);
			for (ScoreDoc hit : isearcher.search(query, 10).scoreDocs) {

				String id = isearcher.doc(hit.doc).get("id");
				try {
					result.add(nodeResolver.apply(factory.parseNodeID(new Integer(id))));
				} catch (Exception e) {
					result.add(edgeResolver.apply(factory.parseEdgeID(new Integer(id))));
				}
			}
		} catch (IOException | ParseException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	@Override
	public Set<Item> getItemMatches(DataSource ds, QueryComponent kw) {
		final Set<Item> result = new HashSet<>();
		String str = format(kw);
		if (str.isEmpty()) {
			return result;
		}
		try {
			Query query = parser.parse("(" + str + ") && +datasource:\"" + escape(ds.getLocalURI().toString()) + '\"');
			for (ScoreDoc hit : isearcher.search(query, 10).scoreDocs) {
				String id = isearcher.doc(hit.doc).get("id");
				try {
					result.add(nodeResolver.apply(factory.parseNodeID(new Integer(id))));
				} catch (Exception e) {
					result.add(edgeResolver.apply(factory.parseEdgeID(new Integer(id))));
				}
			}
		} catch (IOException | ParseException e) {
			throw new IllegalStateException(e);
		}
		return filterEntityParents(result);
	}

	@Override
	public Set<DataSource> getDataSourceMatches(QueryComponent kw) {
		final Set<DataSource> result = new HashSet<>();
		String str = format(kw);
		if (str.isEmpty()) {
			return result;
		}
		try {
			Query query = parser.parse(str);
			for (ScoreDoc hit : isearcher.search(query, 10).scoreDocs) {
				String dss = isearcher.doc(hit.doc).get("datasource");
				result.add(catalog.getEntry(new URI(dss)));
			}
		} catch (IOException | URISyntaxException | ParseException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	protected void update() {
		try {
			this.ireader = DirectoryReader.open(directory);
			this.isearcher = new IndexSearcher(ireader);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	@Override
	public IndexAndProcessNodesSession openSession(int batchSize, Graph g) {
		try {
			return new LuceneBasedIndexSession(this, directory, analyzer, extractor, mas, extractorStats, indexingStats, g);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void close() {
		try {
			this.analyzer.close();
			this.ireader.close();
			this.directory.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		if(abstractIndex != null){
			abstractIndex.close();
		}
	}

	@Override
	public void commit() {

	}


	@Override
	public IndexingModels indexingModel() {
		return LUCENE;
	}

	@Override
	public String getStemOfString (String kw) {
		return  kw;
	}

	@Override
	public IndexAndProcessNodes abstractIndex() {
		return this.abstractIndex;
	}

	@Override
	public void updateAbstractIndex(IndexAndProcessNodes index) {
		this.abstractIndex = index;
	}

	@Override
	public boolean isAbstract() {
		return this.typeOfGraph == Graph.GraphType.ABSTRACT_GRAPH;
	}

	@Override
	public boolean isNormalized() {
		return this.typeOfGraph == Graph.GraphType.NORMALIZED_GRAPH;
	}

	@Override
	public Set<Node> getExactNodeMatches(QueryComponent kw, Types... types) {
		// TODO Implement an exact method if we ever need it. IM 12/12/2020
		return getNodeMatches(kw, types); 
	}

	@Override
	public void setCatalog(DataSourceCatalog catalog) {
		this.catalog = catalog; 
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

/**
 * Exception thrown when a item of the given ID is not found in some data source.
 * 
 * @author Julien Leblay
 */
public class ItemNotFoundException extends Exception {

	/** Generated */
	private static final long serialVersionUID = -6082336679491924593L;

	/** The ID of the item not found. */
	private final Item itemId;
	
	/**
	 * Instantiates a new item not found exception.
	 *
	 * @param itemId the item id
	 */
	public ItemNotFoundException(Item itemId) {
		this.itemId = itemId;
	}
	
	/**
	 * Instantiates a new item not found exception.
	 *
	 * @param itemId the item id
	 * @param msg the optional exception message
	 */
	public ItemNotFoundException(Item itemId, String msg) {
		super(msg);
		this.itemId = itemId;
	}
	
	/**
	 * Instantiates a new node not found exception.
	 *
	 * @param itemId the item id
	 * @param msg the optional exception message
	 * @param cause the optional root cause
	 */
	public ItemNotFoundException(Node itemId, String msg, Throwable cause) {
		super(msg, cause);
		this.itemId = itemId;
	}
}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.boundary;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.BoundaryNode;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate.UpdateBoolean;
import org.apache.log4j.Logger;

import java.util.ArrayList;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.REPORTED;

public class BoundLeaf extends BoundaryMethod {
    public static final Logger log = Logger.getLogger(BoundLeaf.class);

    private int k;

    public BoundLeaf(int cstar, int k, Configuration configuration) {
        super(cstar, configuration);
        this.k = k;
    }

    @Override
    public CollectionBoundary compute() {
        log.debug("COMPUTE LEAF-" + this.k + " BOUNDARY OF C" + this.cstar);
        CollectionGraph.getWorkingInstance().setStatusOfCollection(this.cstar, REPORTED);
        CollectionGraph.getInstance().setStatusOfCollection(this.cstar, REPORTED);
        int currentDepth = 0;

        this.alreadyVisitedCollections = new ArrayList<>();
        CollectionBoundary collectionBoundary = new CollectionBoundary();
        BoundaryNode bn = new BoundaryNode(this.cstar, collectionBoundary);
        collectionBoundary.setMainCollectionRoot(bn);
        log.debug(CollectionGraph.getWorkingInstance().getOutgoingEdges(collectionBoundary.getMainCollectionRoot().getId()));

        // NEW WAY: each collection that transfers some of its weight to the reported collection is part of the boundary
        // we still need to use recursion because the boundary that we want should reflect the DAG of the nodes. Therefore, we cannot just do for(collId) { if(ptf>0) { ... } }
        log.debug("compute boundary of C" + this.cstar);
        this.buildBoundaryRecursive(this.cstar, bn, collectionBoundary, new Path(), currentDepth, this.k);

        return collectionBoundary;
    }
}

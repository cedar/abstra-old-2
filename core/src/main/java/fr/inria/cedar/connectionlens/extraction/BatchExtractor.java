package fr.inria.cedar.connectionlens.extraction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.indexing.AbstractProcessNodesSession;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.ExceptionTools;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class BatchExtractor implements EntityExtractor {
	/** The delegate extractor. */
	protected final EntityExtractor extractor;

	private Logger log = Logger.getLogger(BatchExtractor.class); 
	
	protected final ExtractorBatch extractorBatch; 
	AbstractProcessNodesSession apns; 
	
	public static final int FLUSHED = 0;
	public static final int BATCHED = 1; 
	public static final int CACHED_NONE = 2;
	public static final int CACHED_RESULTS = 3; 
	
	private StatisticsCollector stats; 
	
	/**
	 * Instantiates a new lazy entity extractor.
	 *
	 * @param wrapped the wrapped
	 * @param stats   the stats
	 */
	public BatchExtractor(EntityExtractor wrapped, StatisticsCollector stats, int batchSize) {
		this.extractor = wrapped;
		this.stats = stats;
		this.extractorBatch = new ExtractorBatch(batchSize);
	}
	public ExtractorBatch getExtractorBatch() {
		return this.extractorBatch; 
	}
	public void setAbstractProcessNodesSession(AbstractProcessNodesSession apns) {
		this.apns = apns; 
	}

	public void setStats(StatisticsCollector stats) {
		this.stats = stats; 
	}
	
		
	@Override
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		return extractor.run(ds, input, structuralContext); 
	}
	
	/** adds to batch and flushes if necessary */
	public int batchAndPossiblyFlush(Collection<Entity> entitiesToAddTo, DataSource ds, Node n, String input, int structuralContext) {
		//log.info(input + " | batched");
//		try {
//			throw new Exception("got here");
//		}
//		catch(Exception e) {
//			ExceptionTools.prettyPrintException(e, 5);
//		}
		//log.info("Adding to batch: " + input + " in context " + structuralContext);
		
		if (!extractorBatch.isFull()) {
			extractorBatch.addEntry(n, input);
			if (extractorBatch.isFull()) {
				apns.processAndFlushExtractorBatch(extractorBatch); // this must also reset it 
			}
			return FLUSHED; 
		}
		else {
			apns.processAndFlushExtractorBatch(extractorBatch);
			extractorBatch.addEntry(n, input);
			return BATCHED; 
		}	
				
	}

	@Override
	public ArrayList<Pair<Node, Collection<Entity>>> run(ArrayList<Pair<Node, String>> inputs) {	
		//log.info("Batch extraction using " + extractor.getExtractorName() + " on "  + inputs.size() + " inputs"); 
		// This is the batch call:
		ArrayList<Pair<Node, Collection<Entity>>> results = extractor.run(inputs);
		return results; 
	}

	@Override
	public String getExtractorName() {
		return "Batch(" + extractor.getExtractorName() + ")";
	}

	@Override
	public Locale getLocale() {
		return this.extractor.getLocale();
	}
	@Override
	public void flushMemoryCache() {
		// nothing
	}
	@Override
	/** For a batch extractor, this information comes from its child */
	public Class getBasicExtractorClass() {
		return extractor.getBasicExtractorClass();
	}
	


}

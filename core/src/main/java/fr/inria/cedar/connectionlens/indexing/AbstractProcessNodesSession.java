/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode.Metadata.LENGTH;
import static fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode.Metadata.OFFSET;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.EXTR_T;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.INDEX_T;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.XT_FLUSH_T;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.cedar.connectionlens.extraction.*;
import fr.inria.cedar.connectionlens.extraction.ContextExtractionState.State;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.AvoidLinking;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes.IndexAndProcessNodesSession;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RDFDataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * This class implements the <b>node processing</b> part of the work that is
 * made for every registered node:
 * <ol>
 * <li>Extraction of secondary(entity) nodes, if any</li>
 * <li>Construction of edges that connect the secondary node(s) to their parent
 * node</li>
 * </ol>
 * 
 * This node processing logic is the same regardless of the implementation of
 * the keyword index, this is why we implement it here once and for all.
 * 
 * The indexing logic is implemented in the specializations of this class.
 */
public abstract class AbstractProcessNodesSession implements IndexAndProcessNodesSession {

	protected final StatisticsCollector extractorStats;
	protected final StatisticsCollector indexStats; 
	protected final MorphoSyntacticAnalyser analyser;
	protected final EntityExtractor extractor;
	protected final EmailExtractor emailExtractor;
	protected final HashtagExtractor hashtagExtractor;
	protected final MentionExtractor mentionExtractor;
	protected final HeideltimeExtractor dateExtractor;
	protected final Graph graph;

	Logger log = Logger.getLogger(AbstractProcessNodesSession.class);

	/**
	 * The next two structures help ensure that there is at most one entity node
	 * from a dataset for a given label.
	 */
	HashMap<DataSource, HashMap<String, Node>> entityNodePerDSAndLabel;
	HashMap<DataSource, HashMap<String, Integer>> entityCountPerDSAndLabel;

	/**
	 * The next two structures help ensure that there is at most one entity nodes
	 * per label and loading process, or at most one entity node per label
	 * (including one possibly from a previously loaded dataset).
	 */
	HashMap<String, Node> entityNodePerLabel;
	HashMap<String, Integer> entityCountPerLabel;

	/** The edge buffer. */
	protected transient final Set<Edge> edgeBuffer = Sets.newConcurrentHashSet();

	/** The node buffer. */
	protected transient final Set<Node> nodeBuffer = Sets.newConcurrentHashSet();

	public static enum EntityNodeCreationPolicy {
		PER_OCCURRENCE, PER_DATASET, PER_LOADING, PER_GRAPH;
	}
	
	private ExtractionPolicy extractPolicy; // to be filled based on different nodes
	private final String entityPolicyString = Config.getInstance().getProperty("entity_node_creation_policy");
	private EntityNodeCreationPolicy entityPolicy;

	// fields used for each add(Node), made attributes of the class to prevent reallocation
	private PolicyDrivenExtractor pdExtractor = null;
	private Consumer<Edge> processor;  

	private BatchExtractor batchExtractor;
	
	// To be used by replaceAll() for escaping special characters.
	public final Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]"); 
	// 10/05/21: was: static, final
	

	// When using smartExtract, the extraction state for each context
	HashMap<Integer, ContextExtractionState> extractionStates; 
	ArrayList<Pair<Node, String>> smartExtractBatch; 
	int smartExtractBatchSize = 100; 
	
	
	protected AbstractProcessNodesSession(EntityExtractor extractor, MorphoSyntacticAnalyser analyser,
			StatisticsCollector extractorStats, StatisticsCollector indexStats,  Graph graph) {
		this.extractor = extractor;
		this.emailExtractor = new EmailExtractor();
		this.hashtagExtractor = new HashtagExtractor();
		this.mentionExtractor = new MentionExtractor();
		this.dateExtractor = new HeideltimeExtractor();

		this.analyser = analyser;
		this.indexStats = indexStats; 
		this.extractorStats = extractorStats;
		this.graph = graph;

		this.entityNodePerDSAndLabel = new HashMap<DataSource, HashMap<String, Node>>();
		this.entityCountPerDSAndLabel = new HashMap<DataSource, HashMap<String, Integer>>();
		switch (entityPolicyString) {
		case "PER_OCCURRENCE":
			this.entityPolicy = EntityNodeCreationPolicy.PER_OCCURRENCE;
			break;
		case "PER_DATASET":
			this.entityNodePerDSAndLabel = new HashMap<>();
			this.entityCountPerDSAndLabel = new HashMap<>();
			this.entityPolicy = EntityNodeCreationPolicy.PER_DATASET;
			break;
		case "PER_LOADING":
			this.entityNodePerLabel = new HashMap<>();
			this.entityCountPerLabel = new HashMap<>();
			this.entityPolicy = EntityNodeCreationPolicy.PER_LOADING;
			break;
		case "PER_GRAPH": // using the same data structures as PER_LOADING
			this.entityNodePerLabel = new HashMap<>();
			this.entityCountPerLabel = new HashMap<>();
			this.entityPolicy = EntityNodeCreationPolicy.PER_GRAPH;
			break;
		default:
			throw new IllegalStateException("Unrecognized entity node creation policy " + entityPolicyString);
		}
		if (extractor instanceof BatchExtractor) {
			batchExtractor = (BatchExtractor) extractor; 
			batchExtractor.setAbstractProcessNodesSession(this); 
			log.info("Got a batch extractor: " + batchExtractor.getClass().getSimpleName());
		}
		
		// smart extraction
		extractionStates = new HashMap<>();
		smartExtractBatch = new ArrayList<Pair<Node, String>>();
	}

	@Override
	public void commit() {
		this.nodeBuffer.clear();
		this.edgeBuffer.clear();
	}

	/**
	 * @param n some node
	 * @return true, iff the given node has already been processed within this
	 *         session.
	 */
	@Override
	public boolean hasProcessed(Node n) {
		return this.nodeBuffer.contains(n);
	}

	/**
	 * @param e some edge
	 * @return true, iff the given edge has already been processed within this
	 *         session.
	 */
	@Override
	public boolean hasProcessed(Edge e) {
		return this.edgeBuffer.contains(e);
	}

	@Override
	public void addNode(Node n, Consumer<Edge> processor) {
		this.processor = processor;
		Node currentNode = n;
		String currentLabel = currentNode.getLabel();
		Types currentType = currentNode.getType();
		// 01/02/22
		int currentContext = currentNode.getContext(); 
		
		//log.info("addNode "  + n.getId().value() + ": "+ n.getLabel() + " type: " + currentType);
		if (hasProcessed(n) || Strings.isNullOrEmpty(currentLabel)
				|| AvoidLinking.getDoNotLinkLabels().contains(n.getNormalizedLabel())) {
			// if we don't want to link on this label, we don't want to extract from it, nor
			// index it.
			return;
		}
		// we owe at least to index the node:
		DataSource currentDS = n.getDataSource();
		this.extractPolicy = graph.extractPolicy(); // the policy is stored in the graph not the data source
		this.pdExtractor = PolicyDrivenExtractor.getOrCreate(this.extractPolicy); // extractor uniqueness ensured in the PDE class

		URI currentDSKey = currentDS.getLocalURI();

		// recording the index time for the index
		indexStats.resume(currentDSKey);
		indexStats.resume(StatisticsCollector.total());
		indexNode(n);
		indexStats.tick(currentDSKey, INDEX_T);
		indexStats.tick(StatisticsCollector.total(), INDEX_T);

		// now let's see if we can skip extraction
		if ( ((n.getType() == RDF_URI) && (!Config.getInstance().getBooleanProperty("extract_from_uris")))
				 || (extractor.getBasicExtractorClass().getName().equals("fr.inria.cedar.connectionlens.extraction.TopLevelExtractor"))
                 //(Config.getInstance().getProperty("extractor").equals("NONE")) IM, 15/3/21: become independent of the config file
                 // since we have the extractor anyway
				|| isEntityType(currentType)
				|| currentType == DATA_SOURCE 
				|| currentType == XML_TAG_NODE
				|| currentType == XML_ATTRIBUTE_NODE
				|| currentType == JSON_STRUCT
				|| currentType == RELATIONAL_STRUCT
				|| currentType == HTML_NODE 
				|| currentType == NEO4J_ENTITY
				|| currentType == NEO4J_EDGE
				|| graph.isAbstract()
				// we ask the extraction policy here. Always ask it before calling pdExtractor.run
				|| extractPolicy.saysNoExtract(currentDS, currentContext)
				) {
			//log.info("Ignored " + n); 
			return;
		}

		Collection<Entity> entitiesFromCurrentNode = new HashSet<Entity>();
		// time spent below counts as extraction
		extractorStats.resume(currentDSKey);
		extractorStats.resume(StatisticsCollector.total());

		// attempt to extract by the policy
		boolean policyApplied = false;
		if ((extractPolicy != null) && !extractPolicy.isEmpty()) {
			entitiesFromCurrentNode = pdExtractor.run(currentDS, currentLabel, currentContext);
			if (entitiesFromCurrentNode.size() > 0) {
				policyApplied = true;
				// to handle & store the entities created by a policy
				processExtractorResultsAndFinalizeExtraction(n, currentLabel, processor, entitiesFromCurrentNode);
			}
		}

		if (!policyApplied) { // if the extraction based on policy is not used, we extract emails and hashtags and we extract entities using FLAIR or SNER (with/out the batch)
			//log.info("On: " + label + " on context " + n.getContext() + " the extraction policy didn't apply");
			boolean isValidURI = shouldParseURI(n);
			if (isValidURI) {
				// add the namespace of the URI
				currentLabel = RDFDataSource.preprocessURI(currentLabel);
				if (currentLabel.isEmpty()) {
					return;
				}
				// next, work on all entities that may be found in this URI
			}

			// Here we extract emails, hashtags and mentions using the patternBasedExtractor
			entitiesFromCurrentNode.addAll(patternExtraction(currentNode, processor));
			if(Config.getInstance().getBooleanProperty("extract_dates", false)) {
				entitiesFromCurrentNode.addAll(datesExtraction(currentNode, processor));
			}
			// remove emails, hashtags, mentions and dates from the current label, to avoid
			// costly entity extraction on that text again
			for (Entity e: entitiesFromCurrentNode) {
				currentLabel = currentLabel.replaceAll(SPECIAL_REGEX_CHARS.matcher(e.value()).replaceAll("\\\\$0"), "");
				currentLabel = currentLabel.replaceAll("(\\s)+", " ").trim();
			}

			if (worthExtractingFrom(currentLabel)) {
				// smart extraction:
				ContextExtractionState ces = extractionStates.get(currentContext);
				if (ces == null) {
					ces = new ContextExtractionState(currentContext);
					//log.info("Created new extraction context for " + currentContext + ", its state is: " + ces.state());
					extractionStates.put(currentContext, ces);
				}
				State thisContextState = ces.state();
				boolean sendToExtraction = false;
				switch(thisContextState) {
				case EXTRACT:
					sendToExtraction = true;
					break;
				case MODEL:
					Integer smartExtractDecision = SmartExtractHelper.callAndDecodeResult2(currentNode, currentLabel);
					if (smartExtractDecision == 1) {
						sendToExtraction = true;
					}
					// else, the result is NONE, and we have nothing to do.
					//log.info("model");
					break;
				case NONE:
					log.info("The smart extractor had decided to skip values in this context: " + currentContext);
					break;
					default:
						throw new IllegalStateException("Unexpected value: " + thisContextState);
				}
				if (sendToExtraction) { 
					// This calls: the SNER extractor, the FLAIR extractor, or the CacheExtractor wrapped around one of them.
					if (batchExtractor == null) { // we are not running in batch mode
						// we use addAll here to add the new entities to those possibly found through patterns
						Collection<Entity> entitiesFromExtractor = extractor.run(currentDS, currentLabel, currentContext); 
						entitiesFromCurrentNode.addAll(entitiesFromExtractor);
						// SOURCE:
						if ((!Config.getInstance().getProperty("smart_extraction_profile").isEmpty()) && (Config.getInstance().getProperty("extractor").equals("FLAIR_NER"))) {
							State newState = SmartExtractHelper.callAndDecodeResult1(currentNode, currentLabel, entitiesFromCurrentNode);
							if (newState == State.MODEL) {
								ces.transitionExtractModel();
							} else {
								if (newState == State.NONE) {
									ces.transitionExtractNone();
								}
								// else, the new state is EXTRACT and we don't need to transition
							}
						}
						// this just takes care to put all the entities in the right place in the graph
						processExtractorResultsAndFinalizeExtraction(n, currentLabel, processor, entitiesFromCurrentNode);
					}
					else { // batch mode, possibly with cache
						int resultCode = batchExtractor.batchAndPossiblyFlush(entitiesFromCurrentNode, currentNode.getDataSource(), currentNode, currentLabel, n.getContext());
						// TODO: make the call to "SmartExtract1" to inform it of the output
						if (resultCode != CacheExtractor.NEW_BATCHED) { // CACHED_NONE or CACHED_RESULTS
							processExtractorResultsAndFinalizeExtraction(n, currentLabel, processor, entitiesFromCurrentNode);
						}
					}
				}
			}
		}
		extractorStats.tick(currentDSKey, EXTR_T);
		extractorStats.tick(StatisticsCollector.total(), EXTR_T);
	}

	

	/** True if the string contains at least a letter, has length >= 2, is not true, nor false */
	private boolean worthExtractingFrom(String label) {
		if (label.toLowerCase().equals("false") || label.toLowerCase().equals("true")) {
			return false;
		}
		if (label.length() < 2) {
			return false;
		}
		String pattern = "[a-zA-Z]";
		Matcher m = Pattern.compile(pattern).matcher(label);
		return m.find();
	}
	@Override
	/**
	 * This is only triggered by the BatchExtractor when the buffer is full
	 */
	public void processAndFlushExtractorBatch(ExtractorBatch extractorBatch) {
		long t1 = System.currentTimeMillis();
		extractorStats.resume(StatisticsCollector.total()); 
		// here we do not know the DS (there may be nodes from several data sources in the batch) 
		//log.info("Flushing extractor buffer of size " + extractorBatch.usedSize());
		// recall what "currentLabel" was for each node, before calling
		HashMap<Node, String> nodeCurrentLabels = new HashMap<Node, String>();
		for(Pair<Node, String> entry: extractorBatch.getEntries()) {
			Node n = entry.getLeft();
			String currentLabel = entry.getRight(); 
			nodeCurrentLabels.put(n,  currentLabel); 
		}
		// call the parallel extractor on the whole batch
		ArrayList<Pair<Node, Collection<Entity>>> entitiesFromBatchNodes = extractor.run(extractorBatch.getEntries());
		
		// inform SmartExtract and get its feedback:
		if (!Config.getInstance().getProperty("smart_extraction_profile").isEmpty()) {
			HashMap<Integer, State> smartExtractorFeedback =
					SmartExtractHelper.callAndDecodeResult1Batch(extractorBatch.getEntries(), entitiesFromBatchNodes);
			for (HashMap.Entry<Integer, State> set : smartExtractorFeedback.entrySet()) {
				Integer contextID = set.getKey();
				State newState = set.getValue();
				ContextExtractionState ces = extractionStates.get(contextID);
				State oldState = ces.state();
				//log.info("ETAT EXTRACTION"+ces);
				if (newState == State.MODEL) {
					if (oldState == State.EXTRACT) {
						ces.transitionExtractModel();
					}
					//	log.info("NEW STATE IS MODEL");
				} else {
					if (newState == State.NONE) {
						if (oldState == State.EXTRACT) {
							ces.transitionExtractNone();
						}
					}
				}
			}
		}
		// voir SOURCE plus haut
		//Existing Model

		// create nodes and edges in CL based on this result. 
		for (int i = 0; i < extractorBatch.usedSize(); i ++) {
			Node n = extractorBatch.getEntries().get(i).getLeft(); 
			Collection<Entity> entitiesFromCurrentNode = entitiesFromBatchNodes.get(i).getRight(); 
			String currentLabel = nodeCurrentLabels.get(n); 
			//log.info(extractor.getClass().getName() + " extracting from: " + currentLabel + " got: " + entitiesFromCurrentNode.size() + " entities");
			processExtractorResultsAndFinalizeExtraction(n, currentLabel, processor, entitiesFromCurrentNode); 
		}
		extractorBatch.reset();
		extractorStats.tick(StatisticsCollector.total(), EXTR_T);
		long t2 = System.currentTimeMillis();
		extractorStats.increment(StatisticsCollector.total(), XT_FLUSH_T, (new Long(t2-t1)).intValue());
		//log.info("Flushed");
	}

	/** This is called to handle all entities, after the actual entity extraction (Flair/Stanford), 
	 *  whether or not the entity extraction found some entities. 
	 *  It is called:
	 *  - by this class if we are not using a BatchExtractor, but a "plain" one (this is the case in some tests)
	 *  - by this class if we use a BatchExtractor, and we had result in the cache for a given string, thus, it will 
	 *    not be added to the batch.
	 *  - by the BatchExtractor when its batch is full. 
	 * @param n
	 * @param currentLabel
	 * @param processor
	 * @param entitiesFromCurrentNode
	 */
	private void processExtractorResultsAndFinalizeExtraction(Node n, String currentLabel, Consumer<Edge> processor, Collection<Entity> entitiesFromCurrentNode) {
		//log.info(currentLabel + " | process and finalize extraction results");
		// no timer mgmt here; the flush method does it for us 
		//long t0 = System.currentTimeMillis();
		for (Entity e : new HashSet<>(entitiesFromCurrentNode)) {
			if (AvoidLinking.getDoNotLinkLabels().contains(Node.genericNormalization(e.commonName()))) {
				entitiesFromCurrentNode.remove(e);
			}
		}
		// log.info("=== Node fully covered by entities: " + nodeFullyCoveredByEntities);
		for (Entity entity : entitiesFromCurrentNode) {
			if (entity.type() == ENTITY_PERSON
					&& FirstNameDictionary.isFirstName(Node.genericNormalization(entity.value()))) { // using
				// dictionary
				// log.info("SNER first name!");
				entity.setAmbiID(null);
				handleEntity(processor, n, entity, FIRST_NAME, entity.confidence(), false, entity.getAmbiID());
			} else {
				// log.info("\nSNER entity " + entity.value() + " of type: " +
				// entity.type());
				handleEntity(processor, n, entity, entity.type(), entity.confidence(), false, entity.getAmbiID());
			}

		}
		
		//long t3 = System.currentTimeMillis();
		//extractorStats.increment(StatisticsCollector.total(), XT_FINALIZE_T, (new Long(t3-t0)).intValue());

	}

	protected boolean shouldParseURI(Node n) {
		boolean result = n.getType() != DATA_SOURCE && (n.getType() == RDF_URI || n.getLabel().startsWith("jdbc"));
		try {
			new URL(n.getLabel());
			result |= true;
		} catch (MalformedURLException e) {
			// Noop
		}
		return result;
	}

	/**
	 * Extract emails, mentions and hashtags entities from a node.
	 *
	 * @param n the current node on which we extract.
	 * @param processor the edge processor to add the node to the current graph.
	 */
	protected Collection<Entity> patternExtraction(Node n, Consumer<Edge> processor) {
		ArrayList<Entity> entitiesFromPattern = new ArrayList<Entity>();
		if ((n.getType() == RDF_URI || n.getType() == RDF_LITERAL)
				&& (!Config.getInstance().getBooleanProperty("extract_from_uris"))) {
			return entitiesFromPattern;
		}

		// find the emails in the node using the patternBasedExtractor and add each extracted entity to the graph
		Collection<Entity> extractedEmails = this.emailExtractor.run(n.getDataSource(), n.getLabel(), n.getContext());
		for (Entity extractedEmail : extractedEmails) {
			handleEntity(processor, n, extractedEmail, extractedEmail.type(), 1.0, true, "");
			entitiesFromPattern.add(extractedEmail);
		}

		// find the hashtags in the node using the patternBasedExtractor and add each extracted entity to the graph
		if(!n.getType().equals(RDF_URI)) { // we should not extract hashtags from URIs
			Collection<Entity> extractedHashtags = this.hashtagExtractor.run(n.getDataSource(), n.getLabel(), n.getContext());
			for (Entity extractedHashtag : extractedHashtags) {
				handleEntity(processor, n, extractedHashtag, extractedHashtag.type(), 1.0, true, "");
				entitiesFromPattern.add(extractedHashtag);
			}
		}

		// find the mentions in the node using the patternBasedExtractor and add each extracted entity to the graph
		Collection<Entity> extractedMentions = this.mentionExtractor.run(n.getDataSource(), n.getLabel(), n.getContext());
		for (Entity extractedMention : extractedMentions) {
			handleEntity(processor, n, extractedMention, extractedMention.type(), 1.0, true, "");
			entitiesFromPattern.add(extractedMention);
		}
		return entitiesFromPattern;
	}

	/**
	 * Extract date entities from a node using Heideltime framework.
	 *
	 * @param n the current node on which we extract.
	 * @param processor the edge processor to add the node to the current graph.
	 */
	protected Collection<Entity> datesExtraction(Node n, Consumer<Edge> processor) {
		ArrayList<Entity> entitiesFromPattern = new ArrayList<Entity>();

		// never extract if the node comes from RDF (because of http://w3.org/2001/XMLSchema#string)
		if (n.getType() == RDF_URI || n.getType() == RDF_LITERAL) {
			return entitiesFromPattern;
		}

		// find the emails in the node using the patternBasedExtractor and add each extracted entity to the graph
		Collection<Entity> extractedDates = this.dateExtractor.run(n.getDataSource(), n.getLabel(), n.getContext());
		for (Entity extractedDate : extractedDates) {
			handleEntity(processor, n, extractedDate, extractedDate.type(), 1.0, true, "");
			entitiesFromPattern.add(extractedDate);
		}
		return entitiesFromPattern;
	}

	/**
	 * Creates an entity child of the parent, creates the edge connecting them.
	 * 
	 * @param processor
	 * @param parent
	 * @param token
	 * @param type
	 * @param confidence
	 * @param extractedByCL Whether the entity extraction and/or its classification
	 *                      is something CL has extracted (otherwise, it comes from
	 *                      the entity extractor)
	 */
	protected void handleEntity(Consumer<Edge> processor, Node parent, Token token, Node.Types type, double confidence,
			boolean extractedByCL, String ambID) {
		//long t1 = System.currentTimeMillis();
		Properties p = new Properties();
		p.setProperty(OFFSET.toString(), String.valueOf(token.offset()));
		p.setProperty(LENGTH.toString(), String.valueOf(token.length()));
		DataSource ds = parent.getDataSource();
		String entityLabel = Node.entityLabelPreprocessing(token.value()); // getting rid of leading "l' " and "d' " and
		//log.info("\nTOKEN VALUE " + token.value() + " ==> entity label: " + entityLabel);
		// log.info("ambiID "+ambID);
		Node existingEntity, en;
		switch (this.entityPolicy) {
		case PER_DATASET:
			incrementCounter(ds, entityLabel);
			existingEntity = getPreviousEntity(ds, entityLabel);
			if (existingEntity == null) {
				en = ds.buildChildNode(parent.getId(), entityLabel, type, p);
				// after verification, add extraction edge:
				processor.accept(ds.buildNERExtractionEdge(parent, en, confidence));
				recordEntity(ds, entityLabel, en);
			} else {
				// an extraction edge only
				processor.accept(ds.buildNERExtractionEdge(parent, existingEntity, confidence));

			}

			if (!isNullOrEmpty(ambID)) {
				graph.addToDisambiguatedEntities(getPreviousEntity(ds, entityLabel).getId(), ambID);
			}
			break;
		case PER_OCCURRENCE:
			en = ds.buildChildNode(parent.getId(), entityLabel, type, p);
			processor.accept(ds.buildNERExtractionEdge(parent, en, confidence));
			if (!isNullOrEmpty(ambID)) {
				graph.addToDisambiguatedEntities(en.getId(), ambID);
			}

			break;
		case PER_LOADING:
			incrementCounter(entityLabel);
			existingEntity = entityNodePerLabel.get(entityLabel);
			if (existingEntity == null) {
				en = ds.buildChildNode(parent.getId(), entityLabel, type, p);
				// after verification, add extraction edge:
				processor.accept(ds.buildNERExtractionEdge(parent, en, confidence));
				entityNodePerLabel.put(entityLabel, en);

				if (!isNullOrEmpty(ambID)) {
					graph.addToDisambiguatedEntities(entityNodePerLabel.get(entityLabel).getId(), ambID);
				}
			} else {
				// an extraction edge only
				processor.accept(ds.buildNERExtractionEdge(parent, existingEntity, confidence));
			}
			if (!isNullOrEmpty(ambID)) {
				graph.addToDisambiguatedEntities(entityNodePerLabel.get(entityLabel).getId(), ambID);
			}
			break;
		case PER_GRAPH:
			incrementCounter(entityLabel);
			boolean nodeCreated = false;
			existingEntity = entityNodePerLabel.get(entityLabel); // first search in the cache
			if (existingEntity == null) {
				existingEntity = graph.resolveEntityNode(entityLabel); // then search in the graph on disk
				if (existingEntity == null) { // if it wasn't even on disk, create it, and also store in memory (next
					// time we'll find it there)
					nodeCreated = true;
					en = ds.buildChildNode(parent.getId(), entityLabel, type, p); // build the node
					// after verification, one extraction edge:
					processor.accept(ds.buildNERExtractionEdge(parent, en, confidence));
					entityNodePerLabel.put(entityLabel, en);
					if (!isNullOrEmpty(ambID)) {
						graph.addToDisambiguatedEntities(entityNodePerLabel.get(entityLabel).getId(), ambID);
					}
				}
			}
			if (!nodeCreated) {
				// an extraction edge only
				processor.accept(ds.buildNERExtractionEdge(parent, existingEntity, confidence));
			}
			break;
		default: // do nothing
		}
		//long t2 = System.currentTimeMillis();
		//extractorStats.increment(StatisticsCollector.total(), XT_HANDENTITY_T, (new Long(t2-t1)).intValue());
	}

	/** For PER_DATASET entity creation */
	private Node getPreviousEntity(DataSource ds, String label) {
		HashMap<String, Node> entitiesOfThisDS = this.entityNodePerDSAndLabel.get(ds);
		if (entitiesOfThisDS != null) {
			return entitiesOfThisDS.get(label);
		}
		return null;
	}

	/** For PER_DATASET entity creation */
	private void recordEntity(DataSource ds, String label, Node en) {
		HashMap<String, Node> entitiesOfThisDS = this.entityNodePerDSAndLabel.get(ds);
		if (entitiesOfThisDS == null) {
			entitiesOfThisDS = new HashMap<String, Node>();
			entityNodePerDSAndLabel.put(ds, entitiesOfThisDS);
		}
		entitiesOfThisDS.put(label, en);
	}

	/**
	 * For PER_DATASET entity creation We count how many times a certain entity
	 * appears in a certain document. TODO store this in signature[2] or
	 * signature[3] TODO store this number somewhere in the database
	 * 
	 * @param ds
	 * @param label
	 */
	private void incrementCounter(DataSource ds, String label) {
		HashMap<String, Integer> entityCountOfThisDS = this.entityCountPerDSAndLabel.get(ds);
		if (entityCountOfThisDS == null) {
			entityCountOfThisDS = new HashMap<String, Integer>();
			entityCountOfThisDS.put(label, 1);
			entityCountPerDSAndLabel.put(ds, entityCountOfThisDS);
		} else {
			if (entityCountOfThisDS.get(label) == null) {
				entityCountOfThisDS.put(label, 1);
			} else {
				entityCountOfThisDS.put(label, (entityCountOfThisDS.get(label) + 1));
			}
			// log.info("In " + ds + " entity " + label + " appears " +
			// entityCountOfThisDS.get(label) + " times");
		}
	}

	/**
	 * For PER_LOADING and PER_GRAPH entity creation TODO store this number
	 * somewhere in the database
	 * 
	 * @param label
	 */
	private void incrementCounter(String label) {
		Integer prevCounter = (Integer) (entityCountPerLabel.get(label));
		if (prevCounter == null) {
			entityCountPerLabel.put(label, 1);
		} else {
			entityCountPerLabel.put(label, (prevCounter + 1));
		}
		// log.info("Entity " + label + " appears " +
		// entityCountPerLabel.get(label) + " times");
	}

	/**
	 * Indexing is implemented differently in different implementations of this.
	 * 
	 * @param n
	 */
	protected abstract void indexNode(Node n);

}

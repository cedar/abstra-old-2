/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph.path;

/**
 * This class implements the time out heuristic i.e. 
 * the exploration stops once the assigned time budget 
 * runs out
 *  
 * @author Mihu
 */
public class TimeOutCriterion extends StopCriterion {

	/** The time we allow the process to run in milliseconds */
	double timeOut;
	
	/** The start time. */
	double start;
	
	/**
	 * Instantiates a new time out criterion.
	 *
	 * @param ms the ms
	 */
	public TimeOutCriterion(double ms) {
		this.timeOut = ms;
		this.start = System.currentTimeMillis();
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#canStop()
	 */
	@Override
	public boolean canStop() {
		return (System.currentTimeMillis()-start > timeOut);
	}
	
	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#reset()
	 */
	@Override
	public void reset() {
		this.start = System.currentTimeMillis();
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#update()
	 */
	@Override
	public void update() {
		// no need to explicitly update anything
	}
}

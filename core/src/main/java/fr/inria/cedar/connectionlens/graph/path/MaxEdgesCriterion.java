/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph.path;

/**
 * This class signals an early stopping once the total number of edges 
 * that have been discovered by P2BComputation have reached a fixed number 
 * (e.g. 60% of the edges in the source have been discovered)
 * 
 * @author Mihu
 */
public class MaxEdgesCriterion extends StopCriterion {

	/** The max number of edges. */
	private int max;
	
	/** The current number of edges observed. */
	private int current;
	
	/**
	 * Instantiates a new max edges criterion.
	 *
	 * @param total the total
	 * @param percentage the percentage
	 */
	public MaxEdgesCriterion(int total, double percentage) {
		this.current = 0;
		this.max = (int) (total*percentage);
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#canStop()
	 */
	@Override
	public boolean canStop() {
		return current > max;
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#reset()
	 */
	@Override
	public void reset() {
		this.current = 0;
	}

	/**
	 * {@inheritDoc}
	 * @see fr.inria.cedar.connectionlens.graph.path.StopCriterion#update()
	 */
	@Override
	public void update() {
		this.current++;
	}

}

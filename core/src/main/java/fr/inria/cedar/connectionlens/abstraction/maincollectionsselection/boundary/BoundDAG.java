package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.boundary;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.BoundaryNode;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.graphupdate.UpdateBoolean;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.REPORTED;

public class BoundDAG extends BoundaryMethod {
    public static final Logger log = Logger.getLogger(BoundDAG.class);

    public BoundDAG(int cstar, Configuration configuration) {
        super(cstar, configuration);
    }

    // any collection that transfers weight to C, is within the boundary of C
    // a way to do it is to iterate the collections starting for the selected collection using a DFS saying that
    // we have not already seen the current edge (classic DFS)
    // and that we continue the recursion only if we are not on a path containing cyclic edges (at any position)
    @Override
    public CollectionBoundary compute() {
        log.debug("COMPUTE DAG BOUNDARY OF C" + this.cstar);
        CollectionGraph.getWorkingInstance().setStatusOfCollection(this.cstar, REPORTED);
        CollectionGraph.getInstance().setStatusOfCollection(this.cstar, REPORTED);

        this.alreadyVisitedCollections = new ArrayList<>();
        CollectionBoundary collectionBoundary = new CollectionBoundary();
        BoundaryNode bn = new BoundaryNode(this.cstar, collectionBoundary);
        collectionBoundary.setMainCollectionRoot(bn);

        // NEW WAY: each collection that transfers some of its weight to the reported collection is part of the boundary
        // we still need to use recursion because the boundary that we want should reflect the DAG of the nodes. Therefore, we cannot just do for(collId) { if(ptf>0) { ... } }
        log.debug("buildBoundaryRecursive(" + this.cstar + ", " + bn + ", " + collectionBoundary + ", " + new Path() + ", " + -1 + ", " + -1);
        this.buildBoundaryRecursive(this.cstar, bn, collectionBoundary, new Path(), -1, -1);

        return collectionBoundary;
    }
}

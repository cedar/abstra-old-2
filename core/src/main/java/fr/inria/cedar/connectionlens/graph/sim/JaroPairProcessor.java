/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isBoolean;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isNumeric;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isURI;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.not;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.ofAbsoluteLength;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.EITHER;

import com.google.common.collect.Range;
import com.wcohen.ss.Jaro;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class JaroPairProcessor extends CachedSimilarPairProcessor {

	/** The threshold over which we decide to call Jaro or Levenshtein (if shorter than this threshold, call Jaro, otherwise, Levenshtein) */
	private final int shortStringThreshold;
	
	private final Jaro sim = new Jaro();

	public JaroPairProcessor(StatisticsCollector stats, double th) {
		this(stats, th, Config.getInstance().getIntProperty("short_string_threshold"));
	}

	public JaroPairProcessor(StatisticsCollector stats, double th, int shortStringThreshold) {
		super(stats, th);
		this.shortStringThreshold = shortStringThreshold;
	}

	@Override
	public NodePairSelector selector() {
		NodePairSelector result =
			not(isBoolean(EITHER).or(isNumeric(EITHER)).or(isURI(EITHER)).or(sameLabel()))
					.and(ofAbsoluteLength(BOTH, Range.atMost(shortStringThreshold)));
		return result;
	}

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	@Override
	public Double apply(String a, String b) {
		return sim.score(a, b);
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.search.Query;

//import fr.inria.cedar.connectionlens.search.Query;
import org.apache.log4j.Logger;

public class SolutionTimeCurve {

	// in this array, there are i solutions at the time recorded in position i
	static HashMap<Object, ArrayList<Long>> solutionTimes = new HashMap<Object, ArrayList<Long>>();

	private static String drawingDir;
	private static HashMap<Object, String> drawingDirectories = new HashMap<>();

	static long startTime, endTime;

	public static final Logger log = Logger.getLogger(SolutionTimeCurve.class);

	public static void recordSolutionTime(Object obj, long since) {
		ArrayList<Long> timesForThisQuery = solutionTimes.get(obj);
		if (timesForThisQuery == null) {
			timesForThisQuery = new ArrayList<Long>();
			timesForThisQuery.add(0L); // at time 0 there are 0 solutions
			solutionTimes.put(obj, timesForThisQuery);
		}
		timesForThisQuery.add(since); // at the current time there are as many solution as the array size
	}

	private static void setUpDirs(Object obj) {
		drawingDir = Config.getInstance().getStringProperty("temp_dir");
		if(obj instanceof Query) {
			String keywords = ((Query)obj).getComponents().toString().replace("[", "").replace("]", "").replace(",", "_").replace(" ",
					"");
			drawingDirectories.put(obj, Paths.get(drawingDir, keywords).toString());
		}
		else
			drawingDirectories.put(obj, Paths.get(drawingDir, ((Integer)obj).toString()).toString());
		File queryDrawingDirectory = new File(drawingDirectories.get(obj));
		if (!queryDrawingDirectory.exists()) {
			queryDrawingDirectory.mkdirs();
		}
		startTime = System.currentTimeMillis();
	}

	public static void printSolutionTimes(Object obj, String queueOrderingHeuristic) {
		if (drawingDirectories.get(obj) == null) {
			setUpDirs(obj);
		}
		ArrayList<Long> timesForThisQuery = solutionTimes.get(obj);

		String timesFileName = "cl_times_" + obj.toString() + ".txt";
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(
					new FileWriter(Paths.get(drawingDirectories.get(obj), timesFileName).toString()));
			if (timesForThisQuery != null) {
				for (int i = 0; i < timesForThisQuery.size(); i++) {
					bufferedWriter.write(timesForThisQuery.get(i) + " " + i + "\n");
				}
			}
			bufferedWriter.write("\n");
			bufferedWriter.close();
		} catch (IOException e) {
			throw new IllegalStateException("Unable to write solution times curve to file: " + e.toString());
		}
		String gnuplotCommandFileName = "cl_gnuplot_script.txt";
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(
					new FileWriter(Paths.get(drawingDirectories.get(obj), gnuplotCommandFileName).toString()));
			bufferedWriter.write("set terminal postscript color;\n");
			bufferedWriter.write("set style data linespoints;\n");
			bufferedWriter.write("set xlabel \"Search time(ms)\";\n");
			bufferedWriter.write("set ylabel \"Number of solutions\";\n");

			bufferedWriter.write("set output \"" + Paths.get(drawingDirectories.get(obj), "cl_times_" + obj + ".ps\";\n").toString());
			bufferedWriter.write("plot \"" + Paths.get(drawingDirectories.get(obj), timesFileName + "\""
					+ " title \"" + obj + ", " + queueOrderingHeuristic.replaceAll("_", " ") + "\ntotal wait:"
					+ getTotalWaitingTime(timesForThisQuery) + "\";\n").toString());
			bufferedWriter.close();
		} catch (IOException e) {
			throw new IllegalStateException("Unable to write gnuplot script file: " + e.toString());
		}

		String pathToGnuplotInstallation = Config.getInstance().getStringProperty("drawing.gnuplot_installation");
		if (pathToGnuplotInstallation != null) {
			String[] command = { pathToGnuplotInstallation,
					(Paths.get(drawingDirectories.get(obj), gnuplotCommandFileName).toString()) };
			try {
				Runtime.getRuntime().exec(command);
			} catch (IOException e) {
				log.info("Was not able to run gnuplot: " + e.toString());
			}
		} else {
			log.info("Time curve plotting is requested but gnuplot location is not known.");
		}
	}

	/**
	 * Sums up the times until each solution. This single number materializes the
	 * total waiting time.
	 * 
	 * @param timesForThisQuery
	 * @return
	 */
	private static long getTotalWaitingTime(ArrayList<Long> timesForThisQuery) {
		long total_wait = 0;
		if (timesForThisQuery != null) {
			for (long ti : timesForThisQuery) {
				total_wait += ti;
			}
			return total_wait;
		} else {
			return (System.currentTimeMillis() - startTime);
		}
	}
}

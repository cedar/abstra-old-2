package fr.inria.cedar.connectionlens.extraction;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.ContextExtractionState.State;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.ExceptionTools;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import fr.inria.cedar.connectionlens.graph.Node;

/**
 * The class provides a few helper methods for @Théo's smart extract.
 * @author ioanamanolescu
 *
 */
public class SmartExtractHelper {
	static URL smartExtractorServiceURL;
	//static JSONArray parsedPolicy;
	static String pathProfileFromJson;
	static HashMap<Integer, JSONObject> mapExtractIDProfile;
	static HashMap<String, JSONObject> mapPathProfile;

	static boolean alreadySetUp = false;

	static Logger log  = Logger.getLogger(SmartExtractHelper.class);

	public SmartExtractHelper() {
		if (!Config.getInstance().getProperty("smart_extraction_profile").isEmpty()) {
			System.out.println(Config.getInstance().getProperty("smart_extraction_profile"));
			setUp();
		}
	}

	public static void setUp(){
		if (!alreadySetUp) {
			try {
				smartExtractorServiceURL = new URL("http://127.0.0.1:4001/"); // Théo's service
				//System.out.println("INITIALIZED URL " + smartExtractorServiceURL.toString());
			} catch (Exception e) {
				throw new IllegalStateException("wrong url");
			}
			pathProfileFromJson = Config.getInstance().getProperty("smart_extraction_profile");

			mapExtractIDProfile = new HashMap<>();
			mapPathProfile = new HashMap<>();
			JSONArray pathProfileJson = new JSONArray(pathProfileFromJson);
			for (int i = 0; i < pathProfileJson.length(); i++) {
				JSONObject pathProfile = pathProfileJson.getJSONObject(i);
				String path = pathProfile.getString("path");
				JSONObject profile = pathProfile.getJSONObject("profile");
				mapPathProfile.put(path,profile);
			}


				alreadySetUp = true;
		}
	}

	/** This method is called for a node and its label when the state for the node's context is already MODEL
	 * @param n
	 * @param label
	 * @return either State.EXTRACT or State.NONE
	 */
	public static Integer callAndDecodeResult2(Node n, String label) {
		setUp();
		//Cette méthode n'est pas censée rendre un état (elle ne le modifie pas).
		//Cette méthode donne l'indication de s'il faut extraire sur la string ou non.
		// TODO fill in
		JSONObject message = new JSONObject();
		int contextID = n.getContext(); // le contexte de ce noeud
		HashMap<Integer, String> mapContextIDPath = new HashMap<>();
		String path = mapContextIDPath.get(contextID);
		if(path==null){
			DataSource ds = n.getDataSource();
			path = ds.getStructuralContext(contextID);
			mapContextIDPath.put(contextID, path);
		}

		JSONObject profile = mapPathProfile.get(path);
		if(profile==null) {
			return 1;
		}
		else {
			mapExtractIDProfile.put(contextID, profile);
			message.put("contextID", contextID);
			message.put("profile", profile);
			message.put("nodeID", n.getId().value());
			message.put("stringValue", label);
			String smartRequest = message.toString();
			String smartExtractOutput = callSmartExtractServiceWithMessage(smartRequest);
			JSONObject jsonOutput = new JSONObject(smartExtractOutput);
			Integer decision = jsonOutput.getInt("extract");
			return decision;
		}
	}


	/** This method is called for a node and its label when the state for the node's context is EXTRACT 
	 * @param currentNode
	 * @param currentLabel
	 * @return either State.MODEL, State.NONE or State.EXTRACT
	 */
	public static State callAndDecodeResult1(Node n, String label, Collection<Entity> entitiesCollection) {
		setUp();
		JSONArray organizations= new JSONArray();
		JSONArray locations = new JSONArray();
		JSONArray persons= new JSONArray();
		for (Entity entity: entitiesCollection){
			if (entity.type() == Node.Types.ENTITY_PERSON){
				persons.put(entity.value());
			}
			else if (entity.type() == Node.Types.ENTITY_LOCATION){
				locations.put(entity.value());
			}
			else if (entity.type() == Node.Types.ENTITY_ORGANIZATION) {
				organizations.put(entity.value());
			}
		}
		JSONArray values = new JSONArray();
		JSONObject value = new JSONObject();
		int contextID = n.getContext();
		HashMap<Integer, String> mapContextIDPath = new HashMap<>();
		String path = mapContextIDPath.get(contextID);
		if(path==null){
			DataSource ds = n.getDataSource();
			path = ds.getStructuralContext(contextID);
			mapContextIDPath.put(contextID, path);
		}

		JSONObject profile = mapPathProfile.get(path);
		if(profile==null) {
			return State.EXTRACT;
		}
		else {
			value.put("nodeID", n.getId().value());
			value.put("stringValue", label);
			value.put("extractedOrgs", organizations);
			value.put("extractedLocs", locations);
			value.put("extractedPers", persons);
			value.put("contextID", contextID);
			value.put("profile", profile);
			values.put(value);
			JSONObject message = new JSONObject();
			message.put("extractedData", values);
			String smartRequest = message.toString();
			String smartExtractOutput = callSmartExtractServiceWithMessage(smartRequest);
			JSONArray jsonOutput = new JSONArray(smartExtractOutput);
			JSONObject contextStateResult = jsonOutput.getJSONObject(0);
			String answer = contextStateResult.getString("newState");
			if (answer.equals("model")) {
				return State.MODEL;
			} else if (answer.equals("none")) {
				return State.NONE;
			} else {
				return State.EXTRACT;
			}
		}
	}

	/** This method is the batch version of the method above.
	 * It returns a set of new states, for all the contexts in which there were some nodes in the input.
	 * 
	 * @param nodesWithTheirLabels
	 * @param nodesWithEntities
	 * @return
	 */
	public static HashMap<Integer, State> callAndDecodeResult1Batch(ArrayList<Pair<Node, String>> nodesWithTheirLabels,
			ArrayList<Pair<Node, Collection<Entity>>> nodesWithEntities) {
		setUp();
		JSONArray values = new JSONArray();
		for (Pair<Node, String> nodeString : nodesWithTheirLabels) {
			JSONObject value = new JSONObject();
			Node n = nodeString.getLeft();
			Integer contextID = n.getContext();
			HashMap<Integer, String> mapContextIDPath = new HashMap<>();
			String path = mapContextIDPath.get(contextID);
			if(path==null){
				DataSource ds = n.getDataSource();
				path = ds.getStructuralContext(contextID);
				mapContextIDPath.put(contextID, path);
			}

			JSONObject profile = mapPathProfile.get(path);
			if(profile!=null) {
				String label = nodeString.getRight();
				Collection<Entity> entitiesCollection = new ArrayList();
				for (Pair<Node, Collection<Entity>> nodeEntities : nodesWithEntities) {
					if (n == nodeEntities.getLeft()) {
						entitiesCollection = nodeEntities.getRight();
					}
				}
				JSONArray organizations = new JSONArray();
				JSONArray locations = new JSONArray();
				JSONArray persons = new JSONArray();
				for (Entity entity : entitiesCollection) {
					if (entity.type() == Node.Types.ENTITY_PERSON) {
						persons.put(entity.value());
					} else if (entity.type() == Node.Types.ENTITY_LOCATION) {
						locations.put(entity.value());
					} else if (entity.type() == Node.Types.ENTITY_ORGANIZATION) {
						organizations.put(entity.value());
					}
				}
				value.put("nodeID", n.getId().value());
				value.put("stringValue", label);
				value.put("extractedOrgs", organizations);
				value.put("extractedLocs", locations);
				value.put("extractedPers", persons);
				value.put("contextID", contextID);
				JSONObject profilem = new JSONObject();
				profilem.put("organization", 1);
				value.put("profile", profilem);
				values.put(value);
			}
		}
		JSONObject message = new JSONObject();
		HashMap<Integer, State> contextStates = new HashMap<>();
		if(values.length() <1) {
			return contextStates;
		}
		else {
			message.put("extractedData", values);
			String smartRequest = message.toString();
			String smartExtractOutput = callSmartExtractServiceWithMessage(smartRequest);
			JSONArray jsonOutput = new JSONArray(smartExtractOutput);
			for (int i = 0; i < jsonOutput.length(); i++) {
				JSONObject contextStateResult = jsonOutput.getJSONObject(i);
				Integer contextID = contextStateResult.getInt("contextID");
				String answer = contextStateResult.getString("newState");
				//System.out.println("!!!!! ANSWER IS "+answer);
				if (answer.equals("model")){
				//	System.out.println("ON EST PASSE EN MODEL !!!!!!!!!");
					contextStates.put(contextID, State.MODEL);
				} else if (answer.equals("none")){
				//	System.out.println("ON EST PASSE EN NONE !!!!!!!!!");
					contextStates.put(contextID, State.NONE);
				} else {
				//	System.out.println("ON EST TOUJOURS EN EXTRACT !!!!!!!!!");
					contextStates.put(contextID, State.EXTRACT);
				}
			}
			return contextStates;
		}
	}



	public static String callSmartExtractServiceWithMessage(String message)  {
		//System.out.println(message);
		DataOutputStream os = null;
		StringBuilder jsonString = new StringBuilder();
		HttpURLConnection conn;
		try {
			conn = (HttpURLConnection) smartExtractorServiceURL.openConnection();
		}
		catch(Exception e){
			throw new IllegalStateException("Could not open connection to " +smartExtractorServiceURL.toString());
		}
		try {
			conn.setRequestMethod("POST");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("accept", "application/json");
			conn.setConnectTimeout(5000);
			byte[] postData = message.getBytes(StandardCharsets.UTF_8);
			conn.setDoOutput(true);
			os = new DataOutputStream(conn.getOutputStream());
			os.write(postData);
			os.flush();
			if (conn.getResponseCode() != 200) {
				//log.info("Obtained response code: " + conn.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			InputStream inputStream = conn.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader((inputStream)));

			String s;
			while ((s = br.readLine()) != null) {
				jsonString.append(s);
			}
			br.close();
			//log.info("Check 4");
			conn.disconnect();
		}
		catch(Exception e){
			ExceptionTools.prettyPrintException(e, 10);
			throw new IllegalStateException("could not call smart extract");
		}
		finally {
			conn.disconnect();
		}
		//System.out.println(jsonString.toString());
		return jsonString.toString();
	}
	// faire qques fonctions qui confectionnent des messages JSON (et ne font rien d'autre)
	// + des méthodes qui appellent le service et analysent sa réponse (comme dans FlairNERExtractor)

}

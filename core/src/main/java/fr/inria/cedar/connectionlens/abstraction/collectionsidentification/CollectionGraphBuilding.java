package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.AbstractionTask;
import fr.inria.cedar.connectionlens.abstraction.Utils;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.*;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.SchemaTableNames;
import fr.inria.cedar.connectionlens.util.CollectionGraphPrinting;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.*;

public class CollectionGraphBuilding extends AbstractionTask {
    public static final Logger log = Logger.getLogger(CollectionGraphBuilding.class);

    private final int RDFQuotientPort = Config.getInstance().getIntProperty("RDBMS_port");
    private final String RDFQuotientUser = Config.getInstance().getStringProperty("RDBMS_user");
    private final String RDFQuotientPassword = "''";
    private HashMap<String, Integer> usefulNodeIds;

    public static int collectionId = 0; // collection id starts at 0 (because of arrays used in the reporting phase)
    public Locale locale = new Locale(Config.getInstance().getStringProperty("default_locale").equals("fr") ? "FRENCH" : "ENGLISH");

    /**
     * Creates a RandConverter object from an existing ConnectionLens instance
     */
    public CollectionGraphBuilding(ConnectionLens cl, DataSource ds) {
        super(cl, ds);

        // a. create tables to store the collections
        log.info("Create tables for the collection graph");
        boolean resetCollectionGraph = true; // !Config.getInstance().getBooleanProperty("start_from_collection_graph"); -- TODO NELLY: not sure if this feature works correctly - deactivate it for now
        this.graph.createTable(SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME, "(collEdgeId SERIAL, collIdSource INTEGER, collIdTarget INTEGER, isEdgeInCycle BOOL, isEdgeComingFromIDREF BOOL DEFAULT false, isAtMostOne BOOL DEFAULT false, edgeTransferFactor NUMERIC DEFAULT 0, isActive BOOLEAN DEFAULT true)", false, resetCollectionGraph);
        this.graph.createTable(SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME, "(collId int, collLabel VARCHAR DEFAULT '', collCategory INTEGER DEFAULT -1, collSize INTEGER, isLeaf BOOLEAN DEFAULT false, isActive BOOLEAN DEFAULT true)", false, resetCollectionGraph);
        this.graph.createTable(SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME, "(collId int, nodeId int, isActive BOOLEAN DEFAULT true, odw INT DEFAULT 0, dw INT DEFAULT 0)", false, resetCollectionGraph);
        this.graph.createTable(SchemaTableNames.COLLECTIONS_TYPES, "(collId INTEGER, collType VARCHAR)", false, resetCollectionGraph);
        this.graph.createTable(SchemaTableNames.SIGNATURES_TABLE_NAME, "(collId INTEGER, childLabel VARCHAR, type INTEGER, totalStrLen INTEGER, totalOccurTLength INTEGER, totalNbOccurT INTEGER)", false, resetCollectionGraph);
        this.graph.createTable(SchemaTableNames.COLLECTIONS_SIZES_TABLE_NAME, "(collId INTEGER, collSize INTEGER)", true, resetCollectionGraph);
    }

    public CollectionGraphBuilding() {
        super();
    }

    /**
     * Create collections of records based on the normalized graph and the summary.
     * This fills ABSTRACT_NODES_TABLE_NAME and ABSTRACT_EDGES_TABLE_NAME (which are also used for the GUI)
     * @throws IllegalArgumentException if the clustering method is unknown.
     * @throws IOException if a file cannot be read/write.
     * @throws SQLException if a SQL query fails.
     */
    public void run() throws IllegalArgumentException, IOException, SQLException, URISyntaxException, InterruptedException {
//        log.info("COLLECTION GRAPH BUILDING STARTS");
        this.localCollector.start(StatisticsCollector.total());
        long startTime = System.currentTimeMillis();

        // b. compute the summary depending on the data format
        if (this.isRdf) {
            this.computeSummaryForRdfData();
            this.storeSummaryNodesTypes();
        } else if (this.ds.getLocalURI().toString().endsWith(".xml") || this.ds.getLocalURI().toString().endsWith(".json") || this.ds.getLocalURI().toString().endsWith(".csv")) {
            this.computeSummaryForTreeData();
        } else {
            throw new NotImplementedException("Not implemented format");
        }

        // d. build the indexes (useful for the collection identification step)
        this.buildIndexes();
        log.info("Summarization finished in " + (System.currentTimeMillis() - startTime) + " ms.");

        startTime = System.currentTimeMillis();
        // e. build the collection graph out of the set of collections
        CollectionGraph.getInstance().setGraph(this.graph);
        CollectionGraph.getInstance().setIsRdf(this.isRdf);
        CollectionGraph.getInstance().resetIsActive();
        if(this.isXML) {
            log.info("retrieve collection graph from disk");
            CollectionGraph.getInstance().retrieveCollectionGraphFromDisk();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTION_GRAPH_MEM);
            CollectionGraph.getInstance().computeCollectionsLabels();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTIONS_LABELS_MEM);
            log.debug(CollectionGraph.getInstance().getCollectionGraph());
            if(!Config.getInstance().getStringProperty("use_idrefs_xml").equals("DISABLE")) {
                log.info("build ID/IDREF in memory");
                CollectionGraph.getInstance().buildXMLRefEdgesInCollectionGraph();
                this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_ID_IDREF_EDGES_MEM);
                log.debug(CollectionGraph.getInstance().getCollectionGraph());
                log.info("store new collection graph in memory");
                CollectionGraph.getInstance().writeCollectionGraphOnDisk();
                this.localCollector.tick(StatisticsCollector.total(), COLLS_WRITE_COLLECTION_GRAPH_DISK);
                log.debug(CollectionGraph.getInstance().getCollectionGraph());
                // we don't need to retrieve the collection graph again since we already have it in memory
            }
        } else if(this.isRdf) {
            log.info("retrieve collection graph from disk");
            CollectionGraph.getInstance().retrieveCollectionGraphFromDisk();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTION_GRAPH_MEM);
            CollectionGraph.getInstance().computeCollectionsLabels();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTIONS_LABELS_MEM);
        } else if(this.isJson) {
            CollectionGraph.getInstance().retrieveCollectionGraphFromDisk();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTION_GRAPH_MEM);
            this.applyCliqueSummarization();
            CollectionGraph.getInstance().retrieveCollectionGraphFromDisk();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTION_GRAPH_MEM);
            CollectionGraph.getInstance().computeCollectionsLabels();
            this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTIONS_LABELS_MEM);
        }
        CollectionGraph.getInstance().computeCollectionsSizes(); // we need them to compute etf
        this.localCollector.tick(StatisticsCollector.total(), COLLS_GET_COLLECTIONS_SIZES_DISK);
        log.info("compute transfers on disk");
        CollectionGraph.getInstance().computeEdgeTransferFactorsOnDisk();
        this.localCollector.tick(StatisticsCollector.total(), COLLS_COMPUTE_ETF_DISK);
        log.debug(CollectionGraph.getInstance().getCollectionGraph());
        log.info("compute at most one on disk");
        CollectionGraph.getInstance().computeAtMostOneOnDisk();
        this.localCollector.tick(StatisticsCollector.total(), COLLS_COMPUTE_AT_MOST_ONE_DISK);
        log.info("compute signatures");
        CollectionGraph.getInstance().computeEntityProfilesOnDisk();
        this.localCollector.tick(StatisticsCollector.total(), COLLS_COMPUTE_ENTITY_PROFILES_DISK);

        // f. we ow compute the total transfer for each collection using all the paths that we computed previously
        // this is done for non-cyclic edge only, thus paths containing cyclic edges have a transfer of 0
        CollectionGraph.getInstance().initializeAndGetVariablesInMemory(true); // the parameter says that we init the path enumeration variable
        this.localCollector.tick(StatisticsCollector.total(), COLLS_INIT_COLLECTION_GRAPH_OBJECT_MEM);
        log.info("enumerate paths and cycles");
        CollectionGraph.getInstance().enumeratePathsAndDetectEdgesInCycles(); // enumerate all paths between the nodes of a collection graph
        this.localCollector.tick(StatisticsCollector.total(), COLLS_ENUMERATE_ALL_PATHS_MEM);
        log.debug(CollectionGraph.getInstance().getCollectionGraph());
        CollectionGraph.getInstance().computeLeafCollections(); // compute the set of leaf collections based on the "original" instance
        CollectionGraph.getInstance().retrieveEntityProfilesInMemory(); // retrieve entity profiles in memory
        CollectionGraph.getInstance().getAndSetAtMostOneInMemory(); // set at-most-one on the collection graph edges
        log.debug(CollectionGraph.getInstance().getCollectionGraph());
        CollectionGraph.getInstance().retrieveEdgeTransferFactorsInMemory();
//        CollectionGraph.getInstance().setEdgeTransfersFactorsInCollectionGraph(); // set etf on collection graph edges
        this.localCollector.tick(StatisticsCollector.total(), COLLS_RETRIEVE_AND_SET_VARIABLES_MEM);
        log.debug(CollectionGraph.getInstance().getCollectionGraph());
        log.info("compute path transfer factors (in memory work)");
        CollectionGraph.getInstance().computeAggregatedPathTransferFactors(false); // compute path transfer factor between node pairs in a collection graph
        this.localCollector.tick(StatisticsCollector.total(), COLLS_COMPUTE_PTF_MEM);
        CollectionGraph.getInstance().computeCollectionsFrequencies();
        CollectionGraph.getWorkingInstance(); // to init the working instance with the copy constructor
        this.localCollector.stop(StatisticsCollector.total());
        log.info("Collection graph building finished in " + (System.currentTimeMillis() - startTime) + " ms.");

        log.info("COLLECTIONS IDENTIFICATION ENDS");
    }

    /**
     * Compute the summary of tree data (e.g. mainly XML, JSON, CSV)
     * @throws SQLException if a SQL query fails.
     * @throws IOException if a summary file cannot be read/write.
     */
    private void computeSummaryForTreeData() throws SQLException, IOException {
        log.info("start to compute summary for tree data");

        if (this.ds instanceof XMLDataSource2 || this.ds instanceof JSONDataSource2 || this.ds instanceof CSVDataSource2) {
            this.localCollector.start(StatisticsCollector.total());
            // regardless the data source type, we need to insert the files containing the summary in Postgres:
            // - summary-nodes is a set of pairs (summary node id, node id)
            // - summary-edges is a set of triples (summary node id, label, summary node id)
            // - summary-paths is a set of pairs (summary node id, summary path)

            log.debug("copy quotient summary files into Postgres");
            log.debug("copy files from " + ((OrderedTreeDataSource) this.ds).getNodesFileName());
            log.debug("copy files from " + ((OrderedTreeDataSource) this.ds).getEdgesFileName());
            log.debug("copy files from " + ((OrderedTreeDataSource) this.ds).getLabelsFileName());
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " (collId, nodeId) FROM STDIN ", new FileReader(((OrderedTreeDataSource) this.ds).getNodesFileName()));
            this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTIONS_NODES_DISK);
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " (collIdSource, collIdTarget) FROM STDIN ", new FileReader(((OrderedTreeDataSource) this.ds).getEdgesFileName()));
            this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTION_GRAPH_DISK);
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " (collId, collLabel) FROM STDIN ", new FileReader(((OrderedTreeDataSource) this.ds).getLabelsFileName()));
            this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTIONS_INFORMATION_DISK);
        } else {
            throw new IllegalStateException("Can't recognize the type of the actual data source. Stopping here.");
        }
    }

    // apply a clique summarization on top of the label summarization for JSON data
    // this might help to "deduplicate" the data, e.g. if researchers have co-authors (that are also researchers) they
    // are duplicated in the data, but we want to try to "deduplicated" them
    private void applyCliqueSummarization() throws SQLException {
        log.info("apply clique summarization");
        // 1. build, for each collection, the set of its outgoing collections
        HashMap<Integer, ArrayList<Integer>> collection2sources = new HashMap<>();
        for(Integer c1 : CollectionGraph.getInstance().getCollectionsIds()) {
            log.debug("looking at collection C" + c1);
            log.debug("set of incoming edges of C" + c1 + ": " + CollectionGraph.getInstance().getIncomingEdges(c1));
            ArrayList<Integer> incomingCollections = new ArrayList<>();
            for (CollectionEdge ceIncoming : CollectionGraph.getInstance().getIncomingEdges(c1)) {
                incomingCollections.add(ceIncoming.getSource());
            }
            log.debug("set of incoming collections of C" + c1 + ": " + incomingCollections);
            collection2sources.put(c1, incomingCollections);
        }

        for(Integer c : collection2sources.keySet()) {
            ArrayList<Integer> sources = collection2sources.get(c);
            if(sources.size() > 1) {
                // first we move all the nodes into the same collection
                Integer collectionThatGetOthers = sources.get(0);
                log.info(collectionThatGetOthers);
                List<Integer> collectionsThatAreGot = sources.subList(1, sources.size());
                log.info(collectionsThatAreGot);
                String collectionsThatAreGotString = StringUtils.join(collectionsThatAreGot, ",");
                PreparedStatement stmt = this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " " +
                        "SET collId = ? " +
                        "WHERE nodeId IN (SELECT nodeId FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " WHERE collId IN (" + collectionsThatAreGotString + "));");
                stmt.setInt(1, collectionThatGetOthers);
                log.info(stmt.toString());
                stmt.execute();

                // then we make the sources (parents) of the collections that have been moved pointing to the new collection holding their nodes
                stmt = this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " SET collidtarget = ? WHERE collidtarget IN (" + collectionsThatAreGotString + ");");
                stmt.setInt(1, collectionThatGetOthers);
                log.info(stmt.toString());
                stmt.execute();

                // then we remove the collections edges outgoing the collections that have been move into another
                stmt = this.graph.getPreparedStatement("DELETE FROM " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " WHERE collidsource IN (" + collectionsThatAreGotString + ")");
                log.info(stmt.toString());
                stmt.execute();

                // then, we remove the collections that have been moved in another collection
                stmt = this.graph.getPreparedStatement("DELETE FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " WHERE collId IN (" + collectionsThatAreGotString + ")");
                log.info(stmt.toString());
                stmt.execute();
            }
        }
    }

    /**
     * Compute the summary for RDF data. This uses the RDFQuotient tool.
     * @throws URISyntaxException if the URI for RDFQuotient connection is malformed.
     * @throws IOException if the files used to copy data cannot be read/write.
     * @throws InterruptedException if RDFQuotient process is interrupted.
     * @throws SQLException if a SQL query fails.
     */
    private void computeSummaryForRdfData() throws URISyntaxException, IOException, InterruptedException, SQLException {
        log.debug("start to compute summary for RDF data");

        // 1. run RDF quotient on the original data
        String rdfInputFile = this.ds.getLocalURI().toString();
        rdfInputFile = (rdfInputFile.startsWith("file:")) ? rdfInputFile.substring("file:".length()) : rdfInputFile; // remove the 'file:' prefix
        log.debug("run RDFQuotient on input");
        this.runRdfQuotient(rdfInputFile);
        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_RUN_RDFQUOTIENT_DISK);

        // 2. open a temporary connection to Postgres to retrieve the tables produced by RDFQuotient
        log.debug("copy RDFQuotient to files");
        URI connectionToPostgres = new URI("jdbc:postgresql://localhost:" + this.RDFQuotientPort + "/" + AbstractionTask.getDatasetName() + "?currentSchema=public&user="+ this.RDFQuotientUser +"&password="+ this.RDFQuotientPassword);
        try (Connection c = ConnectionManager.getConnectionByURL(connectionToPostgres.toString())) {
            log.debug("Copying data from RDFquotient to Abstra");
            String pathDictionaryFile = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "dictionary.csv").toString();
            String pathRepresentativesFile = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "summary_ts_rep.csv").toString();
            String pathEdgesFile = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "summary_ts_edges.csv").toString();
            String pathTriplesFile = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "summary_ts_encoded_triples.csv").toString();
            CopyManager copyManager = new CopyManager((BaseConnection) c); // temporary copy manager

            // copy RDFquotient tables to files
            c.prepareStatement("ALTER TABLE dictionary DROP CONSTRAINT IF EXISTS dictionary_pkey;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS dictionary_i_value;").execute();
            copyManager.copyOut("COPY dictionary TO STDOUT ", new FileOutputStream(pathDictionaryFile));
            copyManager.copyOut("COPY summary_ts_rep TO STDOUT ", new FileOutputStream(pathRepresentativesFile));
            copyManager.copyOut("COPY summary_ts_edges TO STDOUT ", new FileOutputStream(pathEdgesFile));
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_ops;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_osp;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_pos;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_pso;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_sop;").execute();
            c.prepareStatement("DROP INDEX IF EXISTS encoded_triples_i_spo;").execute();
            copyManager.copyOut("COPY encoded_triples TO STDOUT ", new FileOutputStream(pathTriplesFile));
            this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_COPY_OUT_RDFQUOTIENT_MEM);

            // create the tables in CL
            this.graph.createTable(SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME, "(key INTEGER, value TEXT)", false, true);
            this.graph.createTable(SchemaTableNames.RDFQ_SUMMARY_TS_REP_TABLE_NAME, "(graphnode INTEGER, summarynode INTEGER)", false, true);
            this.graph.createTable(SchemaTableNames.RDFQ_SUMMARY_TS_EDGES_TABLE_NAME, "(s INTEGER, p INTEGER, o INTEGER, count INTEGER)", false, true);
            this.graph.createTable(SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME, "(s INTEGER, p INTEGER, o INTEGER)", false, true);
            this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_CREATE_TABLES_DISK);

            // import RDFquotient tables in CL
            log.debug("import RDFQuotient files to Postgres");
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " FROM STDIN ", new FileReader(pathTriplesFile));
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.RDFQ_SUMMARY_TS_EDGES_TABLE_NAME + " FROM STDIN ", new FileReader(pathEdgesFile));
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.RDFQ_SUMMARY_TS_REP_TABLE_NAME + " FROM STDIN ", new FileReader(pathRepresentativesFile));
            // this.graph.getPreparedStatement("ALTER TABLE " + SchemaTableNames.RDFQ_SUMMARY_TS_REP + " ADD UNIQUE(graphnode);").execute();
            this.graph.getCopyManager().copyIn("COPY " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " FROM STDIN ", new FileReader(pathDictionaryFile));
            this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_COPY_IN_RDFQUOTIENT_DISK);
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REGEXP_REPLACE(value, '\"(.+)\"(@[a-z][a-z]|\\^\\^<.*>)$', '\\1', 'g');").execute(); // remove ^^<...> and @en around RDF literals
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REGEXP_REPLACE(REPLACE(value, '<', ''), '>', '') WHERE value LIKE '<%>';").execute(); // remove < > around RDF uris (only)
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REPLACE(REPLACE(value, '\"', ''), '\"', '');").execute(); // remove " " around rdf literals
            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REPLACE(REPLACE(value, '\"', ''), '\"', '');").execute(); // remove " " around rdf literals
//            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REGEXP_REPLACE(REGEXP_REPLACE(value, '[\\\\]n|[\\\\]t|[\\\\]r', ' ', 'g'), ' {2,}', ' ', 'g');").execute(); // remove \r, \n and \t in rdf literals + replace multiple space by one
//            this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " SET value = REPLACE(value, '\\\\', '');").execute(); // remove \\ in rdf literals
            this.graph.getPreparedStatement("ALTER TABLE " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " ADD PRIMARY KEY (key);").execute();
//            this.graph.getPreparedStatement("ALTER TABLE " + SchemaTableNames.RDFQ_DICTIONARY + " ADD UNIQUE (value);").execute();
            this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_UPDATE_DICTIONARY_DISK);
            log.debug("finished to import RDFQuotient files to Postgres");
            this.buildSummaryBasedOnRDFQuotient();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void buildSummaryBasedOnRDFQuotient() throws SQLException {
        // we create a table to map an RDFQ id to a CL id based on the URI/literal label
        // we only map s and o because p leads to one CL node for each property (4 URIS having the property <http://inria.fr/title> leads to 4 nodes labelled <http://inria.fr/title>)
        // we also need to say that we don't want to map extracted entity nodes (because they will match on the label, but we don't want to put them in equivalence classes)
        this.graph.createTable(SchemaTableNames.RDFQ_MAPPING_RDFQ_ABSTRA_IDS_TABLE_NAME, "(rdfqNodeId INTEGER, abstraNodeId INTEGER)", true, true);
        String sql = "INSERT INTO " + SchemaTableNames.RDFQ_MAPPING_RDFQ_ABSTRA_IDS_TABLE_NAME +
                "  SELECT DISTINCT sq1.key, sq1.abstraNodeId FROM (" +
                "    SELECT d.key AS key, nn.id AS abstraNodeId FROM " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " d, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " nn WHERE d.key IN (SELECT s FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + ") AND nn.label = d.value AND nn.type NOT IN (" + Node.getExtractedTypes() + ", " + Node.Types.NORMALIZATION_NODE.ordinal() + ") " + // we also check that the node mapped is not coming from a normalized node because we want to map subjects/objects only in this query. See issue https://gitlab.inria.fr/nbarret/abstraction-work/-/issues/178
                "    UNION ALL " +
                "    SELECT d.key AS key, nn.id AS abstraNodeId FROM " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " d, " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " nn WHERE d.key IN (SELECT o FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + ") AND nn.label = d.value AND nn.type NOT IN (" + Node.getExtractedTypes() + ", " + Node.Types.NORMALIZATION_NODE.ordinal() + ") " +
                "  ) AS sq1;";
        this.graph.getPreparedStatement(sql).execute();
        log.debug(sql);
//        this.graph.getPreparedStatement("ALTER TABLE " + SchemaTableNames.RDFQ_MAPPING_RDFQ_CL_IDS_TABLE_NAME + " ADD UNIQUE (rdfqNodeId);").execute();
//        this.graph.getPreparedStatement("ALTER TABLE " + SchemaTableNames.RDFQ_MAPPING_RDFQ_CL_IDS_TABLE_NAME + " ADD UNIQUE (abstraNodId);").execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_MAPPING_RDF_CL_IDS_DISK);

        // then we also prepare a hashmap to store the ids of useful values (e.g. rdf:type, subClassOf, ...)
        // this will avoid to do a string comparison and instead do an int comparison and avoid a join on the dictionary each time
        PreparedStatement stmt = this.graph.getPreparedStatement("SELECT value, key " +
                "FROM " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " " +
                "WHERE value IN (" +
                "    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', " +
                "    'http://www.w3.org/2000/01/rdf-schema#subClassOf', " +
                "    'http://www.w3.org/2000/01/rdf-schema#subPropertyOf', " +
                "    'http://www.w3.org/2000/01/rdf-schema#domain', " +
                "    'http://www.w3.org/2000/01/rdf-schema#range', " +
                "    'http://www.w3.org/2002/07/owl#sameAs'" +
                ")");
        ResultSet rs = stmt.executeQuery();
        this.usefulNodeIds = Utils.buildHashMapSetFromResultSetString(rs);
        log.debug(this.usefulNodeIds);


        // then, we build a table to store the triples that we may want to ignore.
        // in fact, some class nodes / property nodes are more part of an ontology than data, so we want to ignore them and do not report them as part of an EC
        // class nodes are:
        // - objects of a "rdf:type" triple
        // - subjects or objects of a "subClassOf" triple
        // - objects of a "domain" or "range" triple
        // property nodes are:
        // - subjects or objects of a "subProperty" triple
        // - subjects of a "domain" or "range" triple
        // we also want to ignore subjects or objects of a sameAs triple
        this.graph.createTable(SchemaTableNames.RDFQ_ONTOLOGY_TRIPLES_TABLE_NAME, "(s INTEGER, p INTEGER, o INTEGER)", true, true);
        sql = "INSERT INTO " + SchemaTableNames.RDFQ_ONTOLOGY_TRIPLES_TABLE_NAME + " ";

        // objects of a "rdf:type" triple
        // rdf:type triples are already ignored by the normalization which don't normalize such edges
//        int rdfType = this.usefulNodeIds.getOrDefault("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", -1);
//        sql +=  "  SELECT * FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " WHERE p = " + rdfType + " ";

        // subjects or objects of a "subClassOf" triple
        int subClassOf = this.usefulNodeIds.getOrDefault("http://www.w3.org/2000/01/rdf-schema#subClassOf", -1);
//        sql += "  UNION ALL ";
        sql += "  SELECT * FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " WHERE p = " + subClassOf + " ";

        // subjects or objects of a "subProperty" triple
        int subPropertyOf = this.usefulNodeIds.getOrDefault("http://www.w3.org/2000/01/rdf-schema#subPropertyOf", -1);
        sql += "  UNION ALL ";
        sql += "  SELECT * FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " WHERE p = " + subPropertyOf + " ";

        // subjects or objects of a "domain" triple
        int domain = this.usefulNodeIds.getOrDefault("http://www.w3.org/2000/01/rdf-schema#domain", -1);
        sql += "  UNION ALL ";
        sql += "  SELECT * FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " WHERE p = " + domain + " ";

        // subjects or objects of a "range" triple
        int range = this.usefulNodeIds.getOrDefault("http://www.w3.org/2000/01/rdf-schema#range", -1);
        sql += "  UNION ALL ";
        sql += "  SELECT * FROM " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " WHERE p = " + range + " ";

        // subjects or objects of a "sameAs" triple
        // NELLY August 17th 2022: we don't put subject and object of sameAs triples into that table because
        // we don't want to remove these nodes from the collection graph - we just want to remove (do not create) the corresponding sameAs edge
        log.debug(sql);
        stmt = this.graph.getPreparedStatement(sql);
        stmt.execute();

        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_ONTOLOGY_TRIPLES_DISK);

        // with the table RDFQ_IGNORED_NODES, we can now store triples that we want to ignore
        // nodes to ignore are: objects of triples whose subject is part of an ignored triple
        // in French: les objets des triples dont le sujet fait partie d'un triple ignoré
        // ceci ne revient pas à simplement prendre les objets des triples ignorés (cf. issue 115)
        this.graph.createTable(SchemaTableNames.RDFQ_IGNORED_SUBJECTS_OBJECTS_TABLE_NAME, "(s INTEGER)", true, true);
        this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.RDFQ_IGNORED_SUBJECTS_OBJECTS_TABLE_NAME + " SELECT DISTINCT * FROM (" +
                "SELECT s FROM " + SchemaTableNames.RDFQ_ONTOLOGY_TRIPLES_TABLE_NAME + " " +
                "UNION ALL " +
                "SELECT o FROM " + SchemaTableNames.RDFQ_ONTOLOGY_TRIPLES_TABLE_NAME + ") " +
                "AS sq1;").execute();
        this.graph.createTable(SchemaTableNames.RDFQ_IGNORED_NODES_TABLE_NAME, "(ignoredNodeId INTEGER)", true, true);
        this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.RDFQ_IGNORED_NODES_TABLE_NAME + " " +
                "SELECT DISTINCT et.o " +
                "FROM " + SchemaTableNames.RDFQ_IGNORED_SUBJECTS_OBJECTS_TABLE_NAME + " isot, " + SchemaTableNames.RDFQ_ENCODED_TRIPLES_TABLE_NAME + " et " +
                "WHERE et.s = isot.s ").execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_ONTOLOGY_NODES_DISK);

        // first, we get the interesting summary edges, i.e. data edges
        // there may be several different edges between sn1 and sn2, therefore we need to also store the edge label
        this.graph.createTable(SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME,  "(ecId SERIAL, sn1 int, sn2 int, edgeLabel TEXT)", true, true);
        this.graph.getPreparedStatement("SELECT setval('" + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + "_ecId_seq', (SELECT MAX(summaryNode) FROM " + SchemaTableNames.RDFQ_SUMMARY_TS_REP_TABLE_NAME + "));").execute();
        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " (sn1, sn2, edgeLabel) " +
                "SELECT se.s, se.o, d.value " +
                "FROM " + SchemaTableNames.RDFQ_SUMMARY_TS_EDGES_TABLE_NAME + " se, " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " d " +
                "WHERE se.p <> ? AND se.p <> ? AND se.p <> ? AND se.p = d.key;"); // TODO NELLY AND s.p <> subProperty, domain, range
        stmt.setInt(1, this.usefulNodeIds.getOrDefault("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", -1));
        stmt.setInt(2, this.usefulNodeIds.getOrDefault("http://www.w3.org/2000/01/rdf-schema#subClassOf", -1));
        stmt.setInt(3, this.usefulNodeIds.getOrDefault("http://www.w3.org/2002/07/owl#sameAs", -1));
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_INTERESTING_EDGES_DISK);



        // now, we insert the equivalence classes along with their nodes in the quotient_summary_nodes table
        // this is a partial summary because equivalence classes holding normalized nodes (coming form RDF properties) are missing
        // we keep only ECs that are involved in interesting summary edges
        // and we take care of not adding ignored triples in collections
        log.debug("get partial summary");
        sql = "INSERT INTO " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " " +
                "SELECT r.summarynode, i.abstraNodeId " +
                "FROM " + SchemaTableNames.RDFQ_SUMMARY_TS_REP_TABLE_NAME + " r, " + SchemaTableNames.RDFQ_MAPPING_RDFQ_ABSTRA_IDS_TABLE_NAME + " i " +
                "WHERE r.graphnode = i.rdfqNodeId " +
                "  AND r.graphNode NOT IN (SELECT ignoredNodeId FROM " + SchemaTableNames.RDFQ_IGNORED_NODES_TABLE_NAME + ") " +
                "  AND r.summarynode IN (SELECT sn1 FROM " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " UNION ALL SELECT sn2 FROM " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + ");";
        this.graph.getPreparedStatement(sql).execute();
        log.debug(sql);

        // now we add the missing nodes (the normalized ones) into the summary to obtain a complete summary
        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " " +
                "SELECT se.ecId, nt.id_node_interm " +
                "FROM " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " se, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c1, " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " c2, " + SchemaTableNames.NORMALIZATION_TMP_TABLE_NAME + " nt, " + SchemaTableNames.ORIGINAL_EDGES_TABLE_NAME + " e " +
                "WHERE se.sn1 = c1.collId AND se.sn2 = c2.collId " +
                "AND e.source = c1.nodeId AND e.target = c2.nodeId " +
                "AND e.id = nt.old_edge_id " +
                "AND se.edgeLabel = nt.label;"); // the pair (sn1, sn2) may have several edges. Therefore, we need to check the edge label/
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTIONS_NODES_DISK);

        // Next, we insert the collections in collections_informations based on the table COLLECTIONS_NODES_TABLE_NAME
        // TODO NELLY: how to compute isLeaf at this point?
        this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " (collId, collSize) " +
                "SELECT collId, COUNT(nodeId) FROM " + SchemaTableNames.COLLECTIONS_NODES_TABLE_NAME + " GROUP BY collId;").execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTIONS_INFORMATION_DISK);

        // Finally, we need to insert summary edges in the collection graph
        // we have edges like this A --b--> C but we want A --> b --> C (so the UNION ALL)
        // moreover, we need to check that the edges we add are linking collections that "exist"
        // (in the sense that they are part of the Abstra summary, or in other words that they contain data nodes and not ontology nodes)
        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.COLLECTION_GRAPH_TABLE_NAME + " (collIdSource, collIdTarget) " +
                "SELECT se.sn1, se.ecId " +
                "FROM " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " se, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c " +
                "WHERE se.sn1 = c.collId AND se.ecId IN (SELECT collId FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + ") " +
                "UNION ALL " +
                "SELECT se.ecId, se.sn2 " +
                "FROM " + SchemaTableNames.SUMMARY_EDGE_IDS_TABLE_NAME + " se, " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + " c " +
                "WHERE se.sn2 = c.collId AND se.ecId IN (SELECT collId FROM " + SchemaTableNames.COLLECTIONS_INFORMATIONS_TABLE_NAME + ") ");
        stmt.execute();
        this.localCollector.tick(StatisticsCollector.total(), SUMM_INSERT_COLLECTION_GRAPH_DISK);
    }

    private void storeSummaryNodesTypes() throws SQLException {
        if(this.usefulNodeIds.containsKey("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
            String sql = "SELECT se.s, d.value " +
                    "FROM " + SchemaTableNames.RDFQ_SUMMARY_TS_EDGES_TABLE_NAME + " se, " + SchemaTableNames.RDFQ_DICTIONARY_TABLE_NAME + " d " +
                    "WHERE se.p = ? AND se.o = d.key;";
            log.debug(sql);
            PreparedStatement stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.COLLECTIONS_TYPES + " " + sql);
            stmt.setInt(1, this.usefulNodeIds.get("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
            stmt.execute();
        } else {
            // there is no rdf:type edge in the dataset
        }
        this.localCollector.tick(StatisticsCollector.total(), SUMM_RDF_GET_SUMMARY_TYPES_DISK);
    }

    private void runRdfQuotient(String rdfInputFile) throws IOException, InterruptedException {
        // java -jar RDFQuotient-1.8-with-dependencies.jar --load dataset.filename=../data-CL/rdf/test_03.nt
        // java -jar RDFQuotient-1.8-with-dependencies.jar --summarize dataset.filename=../data-CL/rdf/test_03.nt,summary.type=typedstrong,summary.replace_type_with_most_general_type=true
        String pathRdfQuotient = Config.getInstance().getStringProperty("rdf_quotient");
//        log.debug(pathRdfQuotient);

        // 1. first we load the rdf input into RDF quotient
        ProcessBuilder builderLoad = new ProcessBuilder("java", "-jar", pathRdfQuotient, "--load", "\"dataset.filename=" + rdfInputFile + ",database.port=" + this.RDFQuotientPort + ",database.user=" + this.RDFQuotientUser + ",database.password=" + this.RDFQuotientPassword + "\"");
        log.debug(builderLoad.command());
        Process processLoad = builderLoad.inheritIO().start(); // builderLoad.inheritIO().start();
        processLoad.waitFor();
        if (processLoad.exitValue() != 0) {
            throw new IllegalStateException("Something went wrong while loading input '" + rdfInputFile + "' in RDFquotient.");
        } else {
            log.info("Successfully load input in RDF quotient");
        }

        // 2. then we summarize it
        ProcessBuilder builderSummarize = new ProcessBuilder("java", "-jar", pathRdfQuotient, "--summarize", "\"dataset.filename=" + rdfInputFile + ",summary.type=typedstrong" + ",database.port=" + this.RDFQuotientPort + ",database.user=" + this.RDFQuotientUser + ",database.password=" + this.RDFQuotientPassword + ",summary.replace_type_with_most_general_type=true\"");
        log.debug(builderSummarize.command());
        //Process processSummarize = builderSummarize.start();
        Process processSummarize = builderSummarize.inheritIO().start();
        processSummarize.waitFor();
        if (processSummarize.exitValue() != 0) {
            throw new IllegalStateException("Something went wrong while summarizing in RDFquotient.");
        } else {
            log.info("successfully computed the RDFquotient of '" + AbstractionTask.getDatasetName() + "'");
        }
    }

    private void buildIndexes() {
        this.graph.executeUpdate("CREATE INDEX IF NOT EXISTS " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + "_type ON " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + " (type)");
        this.graph.executeUpdate("CREATE INDEX s" + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " on " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " (source)");
        this.graph.executeUpdate("CREATE INDEX t" + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " on " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + " (target)");
        this.localCollector.tick(StatisticsCollector.total(), SUMM_INDEXES_DISK);
    }

    // TODO NELLY: to be moved
    private void insertDataForGui() throws SQLException {
        log.info("Insert data for GUI");
        PreparedStatement stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.ABSTRACT_NODES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.NORMALIZED_NODES_TABLE_NAME + ";");
        stmt.executeUpdate();


        stmt = this.graph.getPreparedStatement("INSERT INTO " + SchemaTableNames.ABSTRACT_EDGES_TABLE_NAME + " SELECT * FROM " + SchemaTableNames.NORMALIZED_EDGES_TABLE_NAME + ";");
        stmt.executeUpdate();

        // update node types with their classified type
//        stmt = this.graph.getPreparedStatement("UPDATE " + SchemaTableNames.ABSTRACT_NODES_TABLE_NAME + " an SET type = cn.nodeType FROM " + SchemaTableNames.COLLECTIONS_TO_NODE_TABLE_NAME + " cn WHERE an.id = cn.nodeId;");
        stmt.executeUpdate();
    }

    public StatisticsCollector getLocalCollector() {
        return this.localCollector;
    }

    public void reportStatistics() {
        if(this.localCollector != null) {
            this.localCollector.stopAll();
            this.localCollector.reportAll();
        }
    }
}

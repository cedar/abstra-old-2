/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.search;

/**
 * Exception thrown when a certain condition is met.
 */
public class ConditionReachedException extends EvaluationException {

	/* Generated */
	private static final long serialVersionUID = -6982986303253208866L;

	/**
	 * Instantiates a new condition reached exception.
	 */
	public ConditionReachedException() {
		super();
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param msg the exception message
	 */
	public ConditionReachedException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param cause the exception's underlying cause
	 */
	public ConditionReachedException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new condition reached exception.
	 *
	 * @param msg the exception message
	 * @param cause the exception's underlying cause
	 */
	public ConditionReachedException(String msg, Throwable cause) {
		super(msg, cause);
	}
}

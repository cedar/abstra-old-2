/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.function.Supplier;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;

import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RDFDataSource;
import fr.inria.cedar.connectionlens.source.DataSource;

public abstract class RDFNode extends Node implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(RDFNode.class);

	public RDFNode(NodeID id, String label, DataSource d, Supplier<Node> rep, Node.Types nodeType) {
		super(id, label, d, rep, nodeType);
	}

	public RDFNode(NodeID id, String label, String normaLabel, DataSource d, Supplier<Node> rep,
			Node.Types nodeType) {
		super(id, label, normaLabel, d, rep, nodeType);
	}
	/** self-represented */
	public RDFNode(NodeID id, String label, String normaLabel, DataSource d, Types nodeType) {
		super(id, label, normaLabel, d, nodeType);
	}

	public static class Literal extends RDFNode implements Serializable {

		/**
		 * Create a new node for RDF Literal type
		 * 
		 * @param id
		 * @param label
		 * @param d     the data source this node appears
		 * @throws IllegalArgumentException
		 */
		public Literal(NodeID id, String label, DataSource d, Supplier<Node> rep) {
			super(id, label, d, rep, Types.RDF_LITERAL);
			specificNormalization();
		}

		public Literal(NodeID id, String label, String normaLabel, DataSource d, Supplier<Node> rep) {
			super(id, label, normaLabel, d, rep, Types.RDF_LITERAL);
			specificNormalization();
		}
		/** As the above but self-represented */
		public Literal(NodeID id, String label, String normaLabel, DataSource dataSource) {
			super(id, label, normaLabel, dataSource, Types.RDF_LITERAL);
			specificNormalization();
		}

		/**
		 * @return the node's literal value
		 */
		public String getLiteralValue() {
			return getLabel();
		}

		@Override
		void specificNormalization() {
			int typePosition = this.normalizedLabel.indexOf("^^");
			if (typePosition >= 0) {
				this.normalizedLabel = this.normalizedLabel.substring(0, typePosition);
			}
		}
	}

	/**
	 * Class that represents a node of an URI in RDF graphs
	 * 
	 * @author Mihu
	 */
	public static class URI extends RDFNode implements Serializable {

		public URI(NodeID globalId, String label, String normaLabel, DataSource dataSource, Supplier<Node> rep) {
			super(globalId, label, dataSource, rep, Types.RDF_URI);
			this.normalizedLabel = normaLabel;
		}

		/** Like the above but self-represented */
		public URI(NodeID globalId, String label, String normaLabel, DataSource dataSource) {
			super(globalId, label, normaLabel, dataSource, Types.RDF_URI);
			this.representative = () -> this;
		}

		public URI(NodeID globalId, String label, DataSource dataSource, Supplier<Node> rep) {
			super(globalId, label, dataSource, rep, Types.RDF_URI);
			this.specificNormalization();
		}

		/**
		 * @return the node's URI
		 */
		public String getURI() {
			return this.getLabel();
		}

		@Override
		protected void specificNormalization() {
			if (normalizedLabel == null || normalizedLabel.length() == 0 || labelComponents1 == null
					|| labelComponents1.size() == 0) {
				labelComponents1 = new ArrayList<String>();
				for (String chunk : label.split("[\\!\\@\\#\\$\\%\\^\\&\\*\\-\\_\\/\\:\\.\\+\\=]")) {
					// remove schema term
					if (!RDFDataSource.schemaTerms.contains(chunk)) { // && !chunk.matches("[0-9]")) {
						chunk = chunk.trim();
						if (chunk.length() > 0) {
							// log.info("Adding URI chunk: " + chunk);
							labelComponents1.add(chunk.toLowerCase());
						}
					}
				}
				normalizedLabel = Joiner.on(" ").join(labelComponents1);
				// log.info("Just normalized " + label + " into: " + normalizedLabel);
			}
		}

		@Override
		public String getLabelPrefix() {
			if (labelComponents1 == null) {
				specificNormalization();
			}
			// try to pick the last URI component before the file extension
			if (labelComponents1.size() == 0) {
				return ""; 
			}
			String lastComponent = labelComponents1.get(labelComponents1.size() - 1);
			if (isFileExtension(lastComponent) && labelComponents1.size() > 1) {
				lastComponent = labelComponents1.get(labelComponents1.size() - 2);
			}
			return lastComponent;
		}
	}
}

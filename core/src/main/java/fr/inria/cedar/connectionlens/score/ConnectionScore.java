/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.score;

import static com.google.common.base.Preconditions.checkArgument;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.score.Score.DoubleScore;

/**
 * Computes a score as a weighted average of confidence vs. specificities of edges in the input 
 * scorable object.
 */
public class ConnectionScore implements ScoringFunction {

	private final double beta;
	
	public ConnectionScore(double b) {
		checkArgument(0. <= b && b <= 1.);
		this.beta = b;
	}

	@Override
	public DoubleScore compute(Scorable e) {
		double result = 1.;
		if (e.edges().isEmpty()) {
			return new DoubleScore(result);
		}
		for (Edge edge : e.edges()) {
			result *= edge.confidence();
		}
		return new DoubleScore(result);
	}
	
	public boolean equals(Object o) {
		if (o instanceof ConnectionScore) {
			ConnectionScore cs = (ConnectionScore) o;
			if (this.beta == cs.beta) {
				return true; 
			}
		}
		return false; 
	}
}

package fr.inria.cedar.connectionlens.util;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionEdge;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score.PageRank;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Draws ConnectionLens graphs using DOT. Is only feasible for small(ish)
 * graphs.
 */
public class PageRankGraphPrinting {
    public static final Logger log = Logger.getLogger(PageRankGraphPrinting.class);
    public static int ID = 0;
    private PageRank pageRank;
    private ArrayList<Integer> collectionVerticesAlreadyDisplayed;
    private BufferedWriter bufferedWriter;
    private final String drawingDir = Config.getInstance().getStringProperty("temp_dir");
    private final String filename;

    public PageRankGraphPrinting(PageRank pageRank, String filename) throws IOException {
        this.pageRank = pageRank;
        this.filename = filename;
        this.bufferedWriter = new BufferedWriter(new FileWriter(Paths.get(this.drawingDir, this.filename+"_"+ID+".dot").toString()));
        this.collectionVerticesAlreadyDisplayed = new ArrayList<>();
    }

    public void printGraph() throws IOException {
        this.bufferedWriter.write("digraph g{\n  splines=polyline;\n  node[shape=box];\n\n");

        // draw the graph
        DecimalFormat numberFormat = new DecimalFormat("0.000000000");
        for(Integer source : this.pageRank.getCollectionGraphWithInvertedEdges().keySet()) {
            if(!this.collectionVerticesAlreadyDisplayed.contains(source)) {
                this.bufferedWriter.write("  N"+source+" [label=\"N"+source+"\\n PR="+numberFormat.format(this.pageRank.getScoreFromCollectionId(source))+"\"]; \n");
                this.collectionVerticesAlreadyDisplayed.add(source);
            }
            if(this.pageRank.getCollectionGraphWithInvertedEdges().containsKey(source)) {
                for(CollectionEdge ce : this.pageRank.getCollectionGraphWithInvertedEdges().get(source)) {
                    int target = ce.getTarget();
                    if(!this.collectionVerticesAlreadyDisplayed.contains(target)) {
                        this.bufferedWriter.write("  N"+target + " [label=\"N" +target + "\\n PR=" +numberFormat.format(this.pageRank.getScoreFromCollectionId(target))+"\"]; \n");
                        this.collectionVerticesAlreadyDisplayed.add(target);
                    }
                    this.bufferedWriter.write("  N"+source + " -> N"+target + ";\n");
                }
            }
        }

        this.closeAndDraw();
    }

    public void closeAndDraw() {
        String pathToDOT = Config.getInstance().getStringProperty("drawing.dot_installation");

        try {
            this.bufferedWriter.write("}\n");
            this.bufferedWriter.close();
        } catch (IOException ioe) {
            throw new IllegalStateException("Could not close dot file");
        }
        try {
            String dotFilePath = Paths.get(this.drawingDir, this.filename+"_"+ID+".dot").toString();
            String pdfFilePath = Paths.get(this.drawingDir, this.filename+"_"+ID+".pdf").toString();
            String[] pdfCommand = { pathToDOT, "-Tpdf", dotFilePath, "-o", pdfFilePath };
            Runtime.getRuntime().exec(pdfCommand);
            ID++;
        } catch (IOException e) {
            System.out.println("Could not process .dot files: install graphviz or check the drawing.dot_installation value in local.settings " + e.toString());
        }
    }
}


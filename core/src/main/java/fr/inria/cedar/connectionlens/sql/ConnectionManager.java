/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql;

import static com.google.common.base.Preconditions.checkArgument;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.util.ExceptionTools;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;


/**
 * Utility class to connect to a relational data source
 */
public class ConnectionManager {

	/** The logger . */
	public static final Logger log = Logger.getLogger(ConnectionManager.class);
	
	/** The prefix to use for dedicated connection lens database. */
	public static final String CONNECTION_LENS_DB_PREFIX = "cl_";
	
	/**
	 * All supported database, with driver class and default port information.
	 */
	public static enum DatabaseTypes {
		//		MYSQL("com.mysql.jdbc.Driver", 3306),

		 POSTGRESQL("org.postgresql.Driver", Integer.parseInt(Config.getInstance().getProperty("RDBMS_port")),
				 "postgres", Config.getInstance().getProperty("RDBMS_host"));

		/** The database driver. */
		public final Class<?> driver;
		
		/** The default port. */
		public final int defaultPort;
		
		/** The master user's default name. */
		public final String master;


		public final String defaultHost;
		
		/**
		 * Instantiates a new database types.
		 *
		 * @param className the class name
		 * @param port the port
		 * @param master the master
		 */
		DatabaseTypes(String className, int port, String master, String host) {
			try {
				this.driver = Class.forName(className);
				this.defaultPort = port;
				this.master = master;
				this.defaultHost = host;
			} catch (ClassNotFoundException e) {
				throw new IllegalStateException(e);
			}
		}
	}
	
	/** Map containing the connection requested so far. */
	private static final Map<String, Connection> connections = new LinkedHashMap<>();
	
	/**
	 * Private constructor
	 */
	private ConnectionManager() {
		// Provide anyone to instantiate this class.
	}

	/**
	 * @return a Connection to the master database.
	 */
	public static Connection getMasterConnection() {
		Config config = Config.getInstance(); 
		DatabaseTypes dbt = getDatabaseType(config); 
		return getConnection(dbt, 
				config.getProperty("RDBMS_DBName"), 
				config.getProperty("RDBMS_host"),
				Integer.parseInt(config.getProperty("RDBMS_port")),
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"));
	}

	/**
	 * Lists the databases.
	 *
	 * @return the list of database names
	 */
	public static List<String> listDatabases() {
		List<String> result = new LinkedList<>();
		try(Statement s = getAdminConnection().createStatement();
			ResultSet rs = s.executeQuery("SELECT datname FROM pg_database")) {
			while (rs.next()) {
				String n = rs.getString(1);
				if (n.startsWith(CONNECTION_LENS_DB_PREFIX)) {
					result.add(n);
				}
			}
		} catch (SQLException e) {
			log.warn(e.getMessage(), e);
		}
		return result;
	}
	
	/**
	 * @return Connection to the default database of the DB server we use (postgres for us, mostly)
	 */
	public static Connection getAdminConnection() {
		return getConnection(getDatabaseType(Config.getInstance()));
	}

	/**
	 * Gets the database type.
	 *
	 * @param config the config
	 * @return the database type
	 */
	private static DatabaseTypes getDatabaseType(Config config) {
		return DatabaseTypes.valueOf(config.getProperty("RDBMS_type"));
	}
	
	/**
	 * @param url
	 * @return a Connection to the database at the given URL.
	 */
	@SuppressWarnings("resource")
	public static Connection getConnectionByURL(String url) {
		if(url.indexOf("password") == -1) {
			try {
				throw new Exception("URL has no password " + url);
			}
			catch(Exception e) {
				e.printStackTrace(); 
			}
		}
		checkArgument(url != null);
		Connection result = connections.get(url);

		// Double-check if the connection has not been closed by some third party.
		try {
//			if (result == null) {
//				log.info("CREATING: " + url);
//			}
//			else {
//				if (result.isClosed()) {
//					log.info("RE-OPENING: " + url); 
//				}
//			}
			if (result == null || result.isClosed()) {
				result = DriverManager.getConnection(url);
//				result.setAutoCommit(false); // do NOT create a transactions or each SQL statement (insert, delete, update) -> improve performances
				connections.put(url, result);
//				if (url.contains("cl_default")) {
//					try {
//						throw new Exception("Accessed cl_default");
//					}
//					catch(Exception e) {
//						System.out.println(ExceptionTools.prettyPrintException(e));
//					}
//				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	/**
	 * Gets a connection using the given parameters.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param host the host
	 * @param port the port
	 * @param username the username
	 * @param password the password
	 * @param schema the schema
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String host,
			int port, String username, String password, String schema) {
		return getConnectionByURL(makeURL(dbType, dbname, host, port, username, password, schema));
	}

	/**
	 * Gets a connection using the given parameters, using the default port and hostname.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param username the username
	 * @param password the password
	 * @param schema the schema
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String username, 
			String password, String schema) {
		return getConnection(dbType, dbname, dbType.defaultHost, dbType.defaultPort,
				username, password, schema);
	}

	/**
	 * Gets a connection using the given parameters, using the default type and schema
	 *
	 * @param dbname the database name
	 * @param host the host
	 * @param port the port
	 * @param username the username
	 * @param password the password
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(String dbname, String host, int port, String username,
			String password) {
		Config config = Config.getInstance(); 
		return getConnection(getDatabaseType(config), dbname, host, port, username, password, null);
	}

	/**
	 * Gets a connection using the given parameters, using the default type, schema and password.
	 *
	 * @param dbname the database name
	 * @param host the host
	 * @param port the port
	 * @param username the username
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(String dbname, String host, int port, String username) {
		Config config = Config.getInstance(); 
		return getConnection(getDatabaseType(config), dbname, host, port, username, 
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default type, schema, port and 
	 * password.
	 *
	 * @param dbname the database name
	 * @param host the host
	 * @param username the username
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(String dbname, String host, String username) {
		Config config = Config.getInstance(); 
		return getConnection(getDatabaseType(config), dbname, host, 
				POSTGRESQL.defaultPort, username, 
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default type, schema, port, username
	 * and password.
	 *
	 * @param dbname the database name
	 * @param host the host
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(String dbname, String host) {
		Config config = Config.getInstance();
		DatabaseTypes dbType = getDatabaseType(config);
		return getConnection(dbType, dbname, host, 
				dbType.defaultPort, 
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default type, schema, port, username,
	 * hostname and password.
	 *
	 * @param dbname the database name
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(String dbname) {
		Config config = Config.getInstance(); 
		DatabaseTypes dbType = getDatabaseType(config);
		return getConnection(getDatabaseType(config), dbname, dbType.defaultHost,
				dbType.defaultPort, 
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters., using the default schema name
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param host the host
	 * @param port the port
	 * @param username the username
	 * @param password the password
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String host,
			int port, String username, String password) {
		return getConnection(dbType, dbname, host, port, username, password, null);
	}

	/**
	 * Gets a connection using the given parameters, using the default password and schema name.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param host the host
	 * @param port the port
	 * @param username the username
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String host,
			int port, String username) {
		return getConnection(dbType, dbname, host, port, username,
				Config.getInstance().getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default port, password and schema
	 * name.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param host the host
	 * @param username the username
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String host, 
			String username) {
		return getConnection(dbType, dbname, host, dbType.defaultPort, username, 
				Config.getInstance().getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default port, username, password and
	 * schema name.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @param host the host
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname, String host) {
		Config config = Config.getInstance(); 
		return getConnection(dbType, dbname, host, dbType.defaultPort, 
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default database name, host, port, 
	 * username, password and schema name.
	 *
	 * @param dbType the database type
	 * @return a connection guaranteed open
	 */
	public static Connection getConnection(DatabaseTypes dbType) {
		Config config = Config.getInstance();
		return getConnection(dbType, dbType.master, dbType.defaultHost, dbType.defaultPort,
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"),
				null);
	}

	/**
	 * Gets a connection using the given parameters, using the default, host, port, 
	 * username, password and schema name.
	 *
	 * @param dbType the database type
	 * @param dbname the database name
	 * @return a connection guaranteed open. Overrides dbType.master with the given dbname
	 */
	public static Connection getConnection(DatabaseTypes dbType, String dbname) {
		Config config = Config.getInstance(); 
		return getConnection(dbType, dbname, dbType.defaultHost, dbType.defaultPort,
				config.getProperty("RDBMS_user"),
				config.getProperty("RDBMS_password"),
				null);
	}
	
	/**
	 * @param dbType the database vendor
	 * @param dbname the database name
	 * @param host the host on which the reach the database
	 * @param port the port on which the reach the database
	 * @param username the connection username (optional)
	 * @param password the connection password (optional)
	 * @param schema the schema(optional)
	 * @return a JDBC URL of the appropriate format for the given parameters.
	 * IM, 13/4/21: this has the database name *and* there is one encapsulated in dbType
	 * Potential for confusion TODO look more into this
	 *  */
	public static String makeURL(DatabaseTypes dbType, String dbname, String host,
			int port, @Nullable String username, @Nullable String password,
			@Nullable String schema) {
		checkArgument(dbType != null);
		checkArgument(host != null);
		checkArgument(dbname != null);
		StringBuilder result = new StringBuilder("jdbc:");
		char sep = '?';
		switch(dbType) {
//			case MYSQL:
//				result.append("mysql://");
//				result.append(host).append(':').append(port);
//				result.append('/').append(dbname);
//				if (!Strings.isNullOrEmpty(username)) {
//					result.append(sep).append("user=").append(username);
//					sep = '&';
//					if (!Strings.isNullOrEmpty(password)) {
//						result.append(sep).append("password=").append(password);
//					}
//				}
//				return result.toString();
			case POSTGRESQL:
				result.append("postgresql://");
				result.append(host).append(':').append(port);
				result.append('/').append(dbname);
				if (!Strings.isNullOrEmpty(schema)) {
					result.append(sep).append("currentSchema=").append(schema);
					sep = '&';
				}
				Config config = Config.getInstance();
				String user = config.getProperty("RDBMS_user");
				String pass = config.getProperty("RDBMS_password");
				if (username != null) {
					user = username;
				}
				result.append(sep).append("user=").append(user);
				sep = '&';
				if (password != null) {
					pass = password;
					result.append(sep).append("password=").append(pass);
				}
				return result.toString();
			default:
				throw new IllegalStateException("Unsupported database type " + dbType);
		}
	}
	
	/**
	 * Closes all connections currently held by the manager.
	 */
	public static void close() {
		//log.info("CLOSING " + connections.keySet().size() + " connections");
		for (String connURL: connections.keySet()) {
			Connection conn = connections.get(connURL); 
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				log.info("Failed to close connection " + conn  + " " + e.toString());
			}
			//log.info(connURL + " closed");
		}
	}
}

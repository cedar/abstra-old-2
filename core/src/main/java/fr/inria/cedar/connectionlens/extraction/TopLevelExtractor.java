/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.collect.ImmutableList;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.DISAMB_T;

/**
 * The TopLevelExtractor implement the EntityExtractor interface, but it never extracts any entity.
 * It is used both to simulate the absence of extraction, and as a top class for fields and methods common to all the other extractors
 */
public class TopLevelExtractor implements EntityExtractor {

	protected StatisticsCollector stats;
	/** Log messages in a conventional way */
	protected Logger log = null; 

	/** The locale. */
	protected Locale locale;
	
	protected Boolean disambiguation = false;
	protected EntityDisambiguationPolicy disambigPolicy;
	
	public TopLevelExtractor(Locale locale) {
		this.locale = locale;
		log = Logger.getLogger(TopLevelExtractor.class); 
		this.disambiguation = Config.getInstance().getBooleanProperty ("entity_disambiguation");
		if(disambiguation)
			log.info("Entity disambiguation is on");
		else
//			log.info("Entity disambiguation is off");
		this.disambigPolicy =  new EntityDisambiguationPolicy(Config.getInstance().getProperty("entity_disambig_policy")); 
	}
	
	public TopLevelExtractor() {
		this.locale =  Locale.forLanguageTag(Config.getInstance().getProperty("default_locale"));
	}

	@Override
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		return ImmutableList.of();
	}
	@Override
	/** Sequential implementation of batched entity extraction. 
	 * 
	 * It receives in input a list of (Dode, String) pairs,
	 * and returns a list of (Node, Collection<Entity>) pairs, 
	 * such that the collection of entities is the result of entity extraction from the respective string.
	 * 
	 * Overriden for parallelism in FlairNERExtractor. 
	 */
	public ArrayList<Pair<Node, Collection<Entity>>> run(ArrayList<Pair<Node, String>> inputs) {
		//log.info(this.getClass().getSimpleName() + " batched (but sequential) extraction");
		ArrayList<Pair<Node, Collection<Entity>>>  results = new ArrayList<Pair<Node, Collection<Entity>>>();
		for (Pair<Node, String> input: inputs) {
			//log.info("Running on " + input);
			results.add(Pair.of(input.getLeft(), run(input.getLeft().getDataSource(), input.getRight(), -1)));  
		}
		return results; 
	}

    @Override
    public String getExtractorName() {
        return "dummy";
    }

    @Override
    public Locale getLocale() {
        return locale; 
    }

    @Override
    public void flushMemoryCache() {

    }

	@Override
	public void setStats(StatisticsCollector extractStats) {
		this.stats = extractStats;
		//log.info("Extraction statistics collector set: " + stats);  
	}
	
	protected void disambiguateIfNeeded(DataSource ds, Entity entity, int structuralContext, String input) {
		if (this.disambigPolicy.requestDisambiguation(ds, entity.type(), structuralContext)) {
			//log.info("Disambiguation of " + entity.type().name() + " " +  entity.commonName() + " in " + input + " on: " + structuralContext);
			setAmbiverseIDToEntity(entity, input);
		}
		else {
			//log.info("No disambiguation of " + entity.type().name() + " " +  entity.commonName() + " on: " + structuralContext);
		}
	}
	protected void setAmbiverseIDToEntity(Entity entity, String input){
		long t1 = System.currentTimeMillis(); 
		//log.info("Disambiguating: " + input); 
		String ambiverseRequest = EntityExtractor.getAmbRequest(entity,input,locale.getLanguage().toString());
		try {
			String responseFromAmbiverse = EntityExtractor.getAnnotationDisambiguated(ambiverseRequest);
			if(!isNullOrEmpty(responseFromAmbiverse) ){
				JSONArray allMatches = new JSONObject(responseFromAmbiverse).getJSONArray("matches");
				if(allMatches.length() > 0) {
					for (int i = 0; i < allMatches.length(); i++) {
						JSONObject match = (JSONObject) allMatches.get(i);
						JSONObject entityAmbiverse = match.getJSONObject("entity");
						if(entityAmbiverse.length() > 0){
							if(entity != null){
								entity.setAmbiID(entityAmbiverse.get("id").toString());
								//log.info("Got:" + entity.getAmbiID());
							}
						}

					}
				}
				else {
					//log.info("Got nothing!");
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		long t2 = System.currentTimeMillis(); 
		stats.increment(StatisticsCollector.total(), DISAMB_T, (new Long(t2-t1)).intValue());
	}

	@Override
	public Class getBasicExtractorClass() {
		return this.getClass(); 
	}


}

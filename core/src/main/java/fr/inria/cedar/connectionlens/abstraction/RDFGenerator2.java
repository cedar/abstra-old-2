package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Random;

public class RDFGenerator2 {

    public static final Logger log = Logger.getLogger(RDFGenerator2.class);

    private static int totalNumberOfTriples;

    public RDFGenerator2() throws IOException {

    }

    public static void main(String[] args) throws IOException {
        totalNumberOfTriples = 0;

        String filename = "dataset";
        filename += ".nt";
        File dataset = new File(Paths.get(Config.getInstance().getStringProperty("temp_dir"), filename).toString());
        log.info(dataset.getAbsolutePath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(dataset));

        generateData(5, 3, 10, bw);
        bw.flush();
        bw.write("\n\n");

        log.info("number of triples generated: " + totalNumberOfTriples);
    }

    private static void generateData(int nbC, int nbD, int nbE, BufferedWriter bw) throws IOException {
        log.info("generating data for " + nbC + " C triples");
        for (int i = 0; i < nbC; i++) {
            bw.write("<http://inria.fr/A" + i + "> <http://inria.fr/c> \"" + i + "\" .\n");
            bw.write("<http://inria.fr/A" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/A> .\n");
            totalNumberOfTriples++;
        }

        log.info("generating data for " + nbD + " D triples");
        for (int i = 0; i < nbD; i++) {
            bw.write("<http://inria.fr/A" + i + "> <http://inria.fr/d> \"" + i + "\" .\n");
            bw.write("<http://inria.fr/A" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/A> .\n");
            bw.write("<http://inria.fr/B" + i + "> <http://inria.fr/d> \"" + i + "\" .\n");
            bw.write("<http://inria.fr/B" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/B> .\n");
            totalNumberOfTriples += 2;
        }

        log.info("generating data for " + nbE + " E triples");
        for (int i = 0; i < nbC; i++) {
            bw.write("<http://inria.fr/B" + i + "> <http://inria.fr/e> \"" + i + "\" .\n");
            bw.write("<http://inria.fr/B" + i + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://inria.fr/B> .\n");
            totalNumberOfTriples++;
        }
    }
}
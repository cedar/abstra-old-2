/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.source;

import static com.google.common.base.Preconditions.checkArgument;
import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.Edge.SAME_AS_LABEL;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.SAME_AS_EDGE;
import static fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode.Metadata.LENGTH;
import static fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode.Metadata.OFFSET;
import static fr.inria.cedar.connectionlens.graph.Node.Types.DATA_SOURCE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.DO_NOT_LINK_VALUE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;

import edu.stanford.nlp.international.french.process.FrenchTokenizer;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.EntityDisambiguationPolicy;
import fr.inria.cedar.connectionlens.extraction.PolicyDrivenExtractor;
import fr.inria.cedar.connectionlens.graph.AvoidLinking;
import fr.inria.cedar.connectionlens.graph.DataSourceNode;
import fr.inria.cedar.connectionlens.graph.DoNotLinkNode;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.PersonOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.RDFNode;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Abstract data source.
 */
public abstract class DataSource implements Serializable {

	/**
	 * The policy for handle values.
	 */
	public enum ValueAtomicities {
		/** Each distinct(leaf) value in the whole stored graph leads to a node */
		PER_GRAPH,
		/** Each distinct(leaf) value per data source and per value leads to a node. */
		PER_DATASET,
		/**
		 * Each distinct(leaf) value per data source and labeled path and value leads to
		 * a node.
		 */
		PER_PATH,
		/** Each occurrence of each value leads to a node */
		PER_INSTANCE
	}

	/**
	 * The supported data source types.
	 */
	public enum Types {
		RDF, 
		TEXT, 
		JSON,
		JSON_V2,
		RELATIONAL,
		HTML,
        XML,
		XML_V2,
		PDF,
		PRE,
		NEO4J,
		DOC,
		XCL,
		CSV,
		CSV_V2
	}

	/**
	 * A logger. IM: this should not be final, so that different data sources can
	 * print meaningful logging. If we make it final, they will all print
	 * AbstractDataSource with wrong line numbers.
	 **/
	static protected transient Logger log = Logger.getLogger(DataSource.class);

	/** The item factory */
	final protected transient Factory factory;

	/** The value atomicity policy. */
	final protected transient ValueAtomicities atomicity;

	/** The data source ID. */
	protected final int id;

	/** The data source type. */
	protected final Types type;

	/** The root node for the data source. */
	protected final DataSourceNode root;

	/** The local source URI. */
	protected final URI localURI;

	/** The original source URI. */
	protected final URI originalURI;

	protected String sanitizedName;
	protected String datasetPath;

	protected static final Pattern regexURI = Pattern
			.compile("^(\\w+):(//)?([A-Za-z0-9_@.-]+)\\.([A-Za-z0-9_@.-]+)(/(\\S*))?$");

	/**
	 * Structural contexts, used both for extraction and for entity disambiguation.
	 */
	protected transient HashMap<String, Integer> contextMap = new HashMap<>();
	protected transient HashMap<Integer, String> revContextMap = new HashMap<>();

	public int getStructuralContextNo(String context) {
		Integer cNo = contextMap.get(context);
		if (cNo != null) {
			return cNo.intValue();
		}
		return -1;
	}

	public String getStructuralContext(int contextNo) {
		return revContextMap.get(new Integer(contextNo));
	}

	/** end of fields and methods for structural contexts */

	/**
	 * The next group of fields and methods serve for policy-based entity extraction
	 * If the policy is empty, extraction is applied on all strings. This uses
	 * structural contexts.
	 */

	protected boolean noExtractionInterval = false;
	/** end of content related to policy-based entity extraction */

	/**
	 * The next group of fields and method serve for policy-based disambiguation If
	 * the disambiguation policy is empty, disambiguation is applied on all nodes
	 * matching the policy. Uses structural contexts
	 */
	protected final String disambigPolicyString = Config.getInstance().getProperty("entity_disambig_policy");
	protected transient EntityDisambiguationPolicy disambigPolicy;

	/** The statistics collector. */

	protected transient StatisticsCollector graphStats;
	protected transient StatisticsCollector extractStats;
	protected transient StatisticsCollector indexStats;

	protected final transient Map<Object, Integer> localNodeIds = new HashMap<>();
	protected final transient Map<Object, Integer> localEdgeIds = new HashMap<>();

	transient HashMap<DataSource, HashMap<String, Node>> nodePerDSandLabel;

	transient HashMap<DataSource, HashMap<String, Node>> getNodePerDSandPath;

	/** Edge leading from the data source root to the local URI */
	protected final Edge toOriginalURI;
	/** Edge leading from the data source to the original URI */
	protected final Edge toLocalURI;
	/** The graph to which this data source belongs. */
	protected final transient Graph graph;

	public int maxLabelLength = Config.getInstance().getIntProperty("max_label_length", 2712);

	/**
	 * Instantiates a new data source.
	 *
	 * @param id the data source ID
	 */

	public DataSource(Factory f, int id, URI localURI, URI origURI, Types t, StatisticsCollector graphStats,
			StatisticsCollector extractStats, Graph graph) {
		checkArgument(id >= 0);
		this.atomicity = ValueAtomicities.valueOf(Config.getInstance().getProperty("value_atomicity"));
		this.id = id;
		this.localURI = localURI;
		this.originalURI = origURI;
		this.datasetPath = this.localURI.toString();
		this.sanitizedName = this.localURI.toString().startsWith("file:") ? this.localURI.toString().substring("file:".length()) : this.localURI.toString(); // remove the 'file:' prefix
		this.type = t;
		if(this.type != Types.RELATIONAL) {
			this.sanitizedName = this.sanitizedName.substring(Math.max(0, this.sanitizedName.lastIndexOf(System.getProperty("file.separator")) + 1), Math.min(this.sanitizedName.lastIndexOf("."), this.sanitizedName.length())); // between the last / (or \) and the last dot position
		}
		log.info("sanitized name: "+this.sanitizedName);
		this.graphStats = graphStats;
		this.extractStats = extractStats;
		this.graph = graph;
		this.factory = f;
		this.root = (DataSourceNode) buildEmptyLabelNodeOfType(DATA_SOURCE);
		if (this.localURI != null && this.localURI.toString().length() > 0) {
			Node localURINode = buildLabelNodeOfType(this.localURI.toString(), RDF_URI);
			toLocalURI = buildEdge(this.root, localURINode, Edge.Types.LOCAL_URL, Edge.LOCAL_URL_LABEL, 1.0, null);
//			try {
//				throw new Exception("Constructing " + localURI); 
//			}
//			catch(Exception e) {
//				System.out.println(ExceptionTools.prettyPrintException(e)); 
//			}
		} else {
			toLocalURI = null;
		}
		if (this.originalURI != null && this.originalURI.toString().length() > 0) {
			Node originalURINode = buildLabelNodeOfType(this.originalURI.toString(), RDF_URI);
			toOriginalURI = buildEdge(this.root, originalURINode, Edge.Types.ORIGINAL_URL, Edge.ORIGINAL_URL_LABEL, 1.0,
					null);
		} else {
			toOriginalURI = null;
		}

		switch (atomicity) {
		case PER_GRAPH:
		case PER_INSTANCE:
		case PER_PATH:
			getNodePerDSandPath = new HashMap<>();
		case PER_DATASET:
			nodePerDSandLabel = new HashMap<>();
		}
	}

	public void setGraphStats(StatisticsCollector graphStats) {
		this.graphStats = graphStats;
	}

	public void setExtractStats(StatisticsCollector extractStats) {
		this.extractStats = extractStats;
	}

	public void setIndexStats(StatisticsCollector indexStats) {
		this.indexStats = indexStats;
	}

	/**
	 * DataSource type specific initialization step. Unlike construction-time
	 * initialization, this can be used for one-time (e.g. cross-execution
	 * initializations). By default, this method is a noop.
	 */
	public void init() {
		/* Noop */
	}

	/**
	 * @return the local data source URI
	 */
	public URI getLocalURI() {
		return this.localURI;
	}

	/**
	 * @return the original data source URI
	 */
	public URI getOriginalURI() {
		return this.originalURI;
	}

	/**
	 * @return the data source ID
	 */
	public int getID() {
		return this.id;
	}

	/**
	 * @return the data source type
	 */
	public Types getType() {
		return this.type;
	}

	/**
	 * @return the root node
	 */
	public DataSourceNode getRoot() {
		return this.root;
	}

	public String getSanitizedName() {
		return this.sanitizedName;
	}

	public String getDatasetPath() {
		return this.datasetPath;
	}

	protected Object makeLocalNodeId(Object key) {
		switch (factory.storageModel()) {
		case COMPACT:
			// Integer result = null;
			// if(localNodeIds.containsIntValue(key.toString())) {
			Integer result = localNodeIds.get(key);
			if (result == null) {
				localNodeIds.put(key, (result = Math.abs(key.hashCode())));
			}
			return result;
		default:
			throw new UnsupportedOperationException("Unsupported storage model " + factory.storageModel());

		}
	}

	protected Object makeLocalEdgeId(Object key) {
		switch (factory.storageModel()) {
		case COMPACT:
			if (key instanceof Number) {
				return key;
			}
			Integer result = localEdgeIds.get(key);
			if (result == null) {
				localEdgeIds.put(key, (result = Math.abs(key.hashCode())));
			}
			return result;
		default:
			throw new UnsupportedOperationException("Unsupported storage model " + factory.storageModel());

		}
	}

	/**
	 * Returns the context (as a string) where the node appears in. The content
	 * varies depending on the type of source.
	 *
	 * @param g the graph
	 * @param n the node for which to get context
	 * @return the context (as a string) where the node appears in.
	 */
	public String getContext(Graph g, Node n) {
		return ""; // + n.getId();
	}

	/**
	 * Traverse the edges of the data source, and processes each of them with the
	 * given processor.
	 *
	 * @param processor the processor to apply to each edge in the data source.
	 */
	public void traverseEdges(Consumer<Edge> processor) throws Exception {

	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		return o instanceof DataSource && getID() == ((DataSource) o).getID();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.localURI);
	}

	@Override
	public String toString() {
		return getType().toString() + '(' + id + ',' + localURI + ')';
	}

	/**
	 * Post-process a data source; for instance it is used to summarized the
	 * enhanced RDF sources; other sources if having no particular operation can
	 * just leave it empty
	 */
	public void postprocess(Graph graph) {
	}

	public Node buildEmptyLabelNodeOfType(Node.Types t) {
		return buildLabelContextNodeOfType("", -1, t);
	}

	public Node buildLabelNodeOfType(String l, Node.Types t) {
		return buildLabelContextNodeOfType(l, -1, t); // context defaults to -1 which means not supported for this data model (or: no context supplied)
	}

	public Edge buildUnlabelledEdge(Node source, Node target, double conf) {
		return buildEdge(source, target, EDGE, "", conf, null);
	}

	public Edge buildLabelledEdge(Node source, Node target, String l, double conf) {
		return buildEdge(source, target, EDGE, l, conf, null);
	}

	public Edge buildSameAsEdge(Node source, Node target, double conf) {
		// log.info(this.getClass().getSimpleName() + " built same as edge from " +
		// source + " " + source.getType() + " to " + target + " " + target.getType() +
		// " confidence: " + conf);
		Edge edge = buildEdge(source, target, SAME_AS_EDGE, SAME_AS_LABEL, conf, null);
		return edge;
	}

	public Edge buildNERExtractionEdge(Node source, Node target, double conf) {
		Edge e = buildEdge(source, target, Edge.decodeExtractedEdgeType(target), Edge.decodeExtractedEdgeLabel(target),
				conf, null);
		target.setExtractedFrom(source);
		// log.info(this.getClass().getSimpleName() + " target is of type: " +
		// target.getClass().getSimpleName());
		return e;
	}

	public Edge buildCLExtractionEdge(Node source, Node target, double conf) {
		Edge e = buildEdge(source, target, Edge.decodeExtractedEdgeType(target), Edge.decodeExtractedEdgeLabel(target),
				conf, null);
		target.setExtractedFrom(source);
		return e;
	}

	public Edge buildEdge(Node source, Node target, Edge.Types t, String label, double conf, Supplier<Specificity> s) {
		EdgeID globalId = new NumericEdgeID(graph.nextID());
		Edge edge = new Edge(globalId, source, target, label, conf, s);
		edge.setEdgeType(t);
		return edge;
	}

	public Edge buildEdge(EdgeID id, Node source, Node target, Edge.Types t, String label, double conf, Supplier<Specificity> s) {
		Edge edge = new Edge(id, source, target, label, conf, s);
		edge.setEdgeType(t);
		return edge;
	}

	public Node buildLabelContextNodeOfType(String l, int context, Node.Types nodeType) {
		NodeID globalId = new NumericNodeID(graph.nextID());
		Node node = buildNode(globalId, l, null, nodeType);
		node.setNodeType(nodeType);
		node.setContext(context);
//		log.info(node.getLabel());
		return node;
	}

	public Node buildChildNode(NodeID parent, String l, Node.Types nodeType, @Nullable Properties p) {
		checkArgument(isEntityType(nodeType));
		NodeID nodeID = new NumericNodeID(graph.nextID());
		Node node = buildNode(nodeID, l, null, p, nodeType);
		node.setNodeType(nodeType);
		return node;
	}

	/**
	 * @param text
	 * @return list of sentences
	 */
	protected List<String> getSentences(String text) {
		List<String> sentenceList = new ArrayList<String>();
		if (Config.getInstance().getBooleanProperty("split_sentences")) {
			// IM, 17/8/20: do not split URIs
			if (isURI(text)) {
				// log.info(text + " is an URI");
				sentenceList.addAll(safetySplit(text));
			} else {
				// log.info(" ");
				// log.info("Considering splitting \"" + text + "\"");
				Reader reader = new StringReader(text);
				DocumentPreprocessor dp = new DocumentPreprocessor(reader);
				if (Config.getInstance().getProperty("default_locale").equals("fr")) {
					dp.setTokenizerFactory(FrenchTokenizer.FrenchTokenizerFactory.newTokenizerFactory());
				} else {
					// TM: ptb3Escaping=false extract original parenthesis
					// and not transformed to -LRB- -RRB-
					PTBTokenizer.PTBTokenizerFactory<Word> tokenizerFactory = PTBTokenizer.PTBTokenizerFactory
							.newWordTokenizerFactory("ptb3Escaping=" + false);
					dp.setTokenizerFactory(tokenizerFactory);
				}
				// IM, 26/8/20: need to make sure that "P. " and "Ph." are not wrongly split as
				// a standalone sentence (#378)
				// Such strings, wrongly considered a sentence by the Stanford tool used here,
				// will be put into leftOver
				// and concatenated as a prefix of the next sentence.
				String leftOver = "";
				for (List<HasWord> sentence : dp) {
					// SentenceUtils.listToString inserts some white spaces before punctuation
					// marks.
					// The white spaces inserted before . and around - are harmful for entity
					// recognition.
					String sentenceString = SentenceUtils.listToString(sentence).replaceAll(" \\.", "\\.")
							.replaceAll(" -", "-").replaceAll("- ", "-");
					// log.info("XXXX " + sentence.toString() + " leads to: " + sentenceString);
					if (leftOver.compareTo("") != 0) {
						sentenceString = leftOver + " " + sentenceString;
						leftOver = "";
					}
					// is this sentence a first name abbreviation?
					boolean isInitials = PersonOccurrenceNode.isInitials(sentenceString);
					boolean isCivility = PersonOccurrenceNode.isCivility(sentenceString);
					if (isInitials) {
						// log.info("Moving " + sentenceString + " as a left over: it's just a first
						// name abbreviation");
						leftOver = sentenceString;
					} else {
						if (isCivility) {
							// log.info("Moving " + sentenceString + " as a left over: it's just a civility
							// prefix");
							leftOver = sentenceString;
						} else {
							sentenceList.addAll(safetySplit(sentenceString));
						}
					}
					// if (sentenceList.size() > 1) {
					// log.info("SPLIT \"" + text + "\" in " + sentenceList.size() + ": " +
					// sentenceList);
					// }
					// log.info("After " + sentenceString + " sentenceList contains: " +
					// sentenceList.size() + " segments");
				}
			}
		} else {
			sentenceList.add(text);
		}
		return sentenceList;
	}

	private static boolean isURI(String text) {
		return regexURI.matcher(text).find();
	}

	/**
	 * As a safety, ensures that no sentence is ever longer than the maximum
	 * authorized length. This can happen when the "text" is not really text but
	 * random characters.
	 *
	 * @param text
	 * @return
	 */
	private Collection<String> safetySplit(String text) {
		ArrayList<String> maximalSegments = new ArrayList<String>();
		if (text.length() >= maxLabelLength) {
			int currentPos = 0;
			while (currentPos < text.length()) {
				if (currentPos + maxLabelLength < text.length()) {
					maximalSegments.add(text.substring(currentPos, currentPos + maxLabelLength));
					currentPos += maxLabelLength;
				} else {
					maximalSegments.add(text.substring(currentPos, text.length()));
					currentPos = text.length();
				}
			}
		} else {
			maximalSegments.add(text);
		}
		if (maximalSegments.size() > 1) {
			StringBuffer sb = new StringBuffer();
			for (String s : maximalSegments) {
				sb.append(s.length() + " ");
			}
			// log.info("A sentence of length " + text.length() + " was cut in " +
			// maximalSegments.size()
			// + " segments of lengths: " + new String(sb));
		}
		return maximalSegments;
	}

	/**
	 * This (and the next) constructor should be used when loading data; it triggers
	 * the computation of the node's normalized label.
	 */
	protected Node buildNode(NodeID globalId, String label, @Nullable Supplier<Node> rep, @Nullable Properties p, Node.Types nodeType) {
		return buildNode(globalId, label, rep, p, 0, nodeType);
	}

	protected Node buildNode(NodeID globalId, String label, @Nullable Supplier<Node> rep, @Nullable Properties p, int context, Node.Types nodeType) {
//		log.info("building node for " + label + ", type=" + nodeType);
		int offset = 0;
		int length = label.length();
		if (p != null) {
			offset = Integer.parseInt(p.getProperty(OFFSET.toString(), "" + offset));
			length = Integer.parseInt(p.getProperty(LENGTH.toString(), "" + length));
		}
		switch (nodeType) {
		case JSON_STRUCT:
		case JSON_VALUE:
		case XML_TAG_NODE:
		case XML_ATTRIBUTE_NODE:
		case XML_ATTRIBUTE_VALUE:
		case XML_TAG_VALUE:
		case RELATIONAL_STRUCT:
		case RELATIONAL_VALUE:
		case TEXT_VALUE:
		case HTML_NODE:
		case HTML_VALUE:
		case NEO4J_ENTITY:
		case NEO4J_VALUE:
		case NEO4J_EDGE:
		case NORMALIZATION_NODE:
		case NORMALIZATION_NODE_EXTRACTION:
		case NORMALIZATION_NODE_XML_RELATION:
//		case COLLECTION:
//		case COLLECTION_PERSON:
//		case COLLECTION_ORGANIZATION:
//		case COLLECTION_LOCATION:
//		case COLLECTION_PRODUCT:
//		case COLLECTION_EVENT:
//		case COLLECTION_CREATIVE_WORK:
//		case COLLECTION_OTHER:
//		case RECORD_PERSON:
//		case RECORD_ORGANIZATION:
//		case RECORD_LOCATION:
//		case RECORD_PRODUCT:
//		case RECORD_EVENT:
//		case RECORD_CREATIVE_WORK:
//		case RECORD_OTHER:
//		case SUB_RECORD:
//			return new Node(globalId, label, this, rep, context, nodeType, ownDataWeight, dataWeight);
		case RDF_LITERAL:
			return new RDFNode.Literal(globalId, label, this, rep);
		case RDF_URI:
			return new RDFNode.URI(globalId, label, this, rep);
		case DO_NOT_LINK_VALUE: 
			return new DoNotLinkNode(globalId, label, this, rep);
		case ENTITY_PERSON:
			return new PersonOccurrenceNode(globalId, label, this, rep, offset, length);
		case EMAIL:
			Node ne = new EntityOccurrenceNode(globalId, label, this, rep, offset, length);
			ne.setNormalizedLabel(label);
			return ne;
		case HASHTAG:
			Node nh = new EntityOccurrenceNode(globalId, label, this, rep, offset, length);
			nh.setNormalizedLabel(label.substring(1));
			return nh;
		case DATE:
			Node nd = new EntityOccurrenceNode(globalId, label, this, rep, offset, length);
			nd.setNormalizedLabel(label);
			return nd;
		case MENTION:
			Node nm = new EntityOccurrenceNode(globalId, label, this, rep, offset, length);
			nm.setNormalizedLabel(label.substring(1));
			return nm;
		case ENTITY_ORGANIZATION:
		case ENTITY_LOCATION:
		case FIRST_NAME:
			return new EntityOccurrenceNode(globalId, label, this, rep, offset, length);
		case DATA_SOURCE:
			return new DataSourceNode(globalId, this);

		default:
			throw new IllegalStateException("Construction not handled for node type: " + nodeType);
		}
	}

	/**
	 * This should be used when querying data; it takes the normalized label from
	 * the store. It does not set the representative; this needs to be set later.
	 */
	public Node rebuildNodeFromStore(NodeID globalId, String label, String normaLabel, @Nullable Supplier<Node> rep, Node.Types nodeType) {
		int offset = 0;
		int length = label.length();
		// if (p != null) {
		// offset = Integer.parseInt(p.getProperty(OFFSET.toString(), "" + offset));
		// length = Integer.parseInt(p.getProperty(LENGTH.toString(), "" + length));
		// }
		switch (nodeType) {
		case JSON_STRUCT:
		case JSON_VALUE:
		case RELATIONAL_STRUCT:
		case RELATIONAL_VALUE:
		case TEXT_VALUE:
		case HTML_NODE:
		case HTML_VALUE:
		case XML_TAG_NODE:
		case XML_ATTRIBUTE_NODE:
		case XML_ATTRIBUTE_VALUE:
		case XML_TAG_VALUE:
		case NEO4J_ENTITY:
		case NEO4J_VALUE:
		case NEO4J_EDGE:
		case NORMALIZATION_NODE:
		case NORMALIZATION_NODE_EXTRACTION:
		case NORMALIZATION_NODE_XML_RELATION:
		case COLLECTION:
		case CATEGORY_PERSON:
		case CATEGORY_ORGANIZATION:
		case CATEGORY_LOCATION:
		case CATEGORY_PRODUCT:
		case CATEGORY_EVENT:
		case CATEGORY_CREATIVE_WORK:
		case CATEGORY_OTHER:
//			return new Node(globalId, label, normaLabel, this, rep, nodeType, ownDataWeight, dataWeight);
		case RDF_LITERAL:
			return new RDFNode.Literal(globalId, label, normaLabel, this, rep);
		case RDF_URI:
			return new RDFNode.URI(globalId, label, normaLabel, this, rep);
		case ENTITY_PERSON:
			// log.info("Creating new Person: " + label + " " + globalId);
			return new PersonOccurrenceNode(globalId, label, this, rep, offset, length, normaLabel);
		case ENTITY_ORGANIZATION:
		case ENTITY_LOCATION:
		case FIRST_NAME:
		case EMAIL:
		case HASHTAG:
		case DATE:
		case MENTION:
			return new EntityOccurrenceNode(globalId, label, normaLabel, this, rep, offset, length);
		case DATA_SOURCE:
			return new DataSourceNode(globalId, this);
		case DO_NOT_LINK_VALUE:
			return new DoNotLinkNode(globalId, label, normaLabel, this, rep);
		default:
			throw new IllegalStateException("Construction from database not handled for node type: " + nodeType);
		}
	}

	/**
	 *
	 * @param label
	 * @param rootedPath
	 * @param ds
	 * @param nodeType
	 * @return Node according to the value atomicity
	 */
	protected Node createOrFindValueNodeWithAtomicity(String label, String rootedPath, DataSource ds,
			Node.Types nodeType) {
		return createOrFindValueNodeWithAtomicity(label, rootedPath, ds, PolicyDrivenExtractor.noContext(), nodeType);
	}

	/** variant that also attaches to each node a structural context */
	protected Node createOrFindValueNodeWithAtomicity(String label, String pathPlusValue, DataSource ds, int context,
			Node.Types nodeType) {
		Node node = null;
		switch (atomicity) {
		case PER_DATASET:
			node = getExistingNodeInDSWithLabel(ds, label);
			if (node == null) {
				node = buildNotExistingNode(label, context, nodeType);
				saveNode(ds, label, node, nodePerDSandLabel);
			}
			break;
		case PER_GRAPH:
			node = graph.getNodeFromCacheOrStorage(label);
			if (node == null) {
				node = buildNotExistingNode(label, context, nodeType);
				// log.info("Could not find node labeled " + label + " in the graph");
				graph.addToNodeCache(label, node);
			}
			// else {
			// log.info("For " + label + " found node of type " + childNode.getType() + "
			// and id " + childNode.getId() );
			// }
			break;
		case PER_PATH:
			node = getExistingNodeInDSWithPath(ds, pathPlusValue); // localID returned is the Path.
			if (node == null) {
				node = buildNotExistingNode(label, context, nodeType);
				saveNode(ds, pathPlusValue, node, getNodePerDSandPath);
			}
			break;
		case PER_INSTANCE:
			node = buildNotExistingNode(label, context, nodeType); // localID is the dewPrefix.
			break;
		}

		return node;
	}

	/**
	 *
	 * @param label
	 * @param context
	 * @param nodeType
	 * @return create new node, may modify its type by analyzing the label
	 */
	private Node buildNotExistingNode(String label, int context, Node.Types nodeType) {
		Node childNode;

		if (AvoidLinking.getDoNotLinkLabels().contains(Node.genericNormalization(label))) {
			childNode = (DoNotLinkNode) buildLabelNodeOfType(label, DO_NOT_LINK_VALUE);
		} else {
			// IM, 25/4/2021
			if (isURI(label)) {
				childNode = buildLabelContextNodeOfType(label.trim(), context, RDF_URI);
			} else {
				// creating as requested
				childNode = buildLabelContextNodeOfType(label, context, nodeType);
			}
		}
		return childNode;
	}

	/**
	 *
	 * @param ds
	 * @param path
	 * @return node for this dataset and path.
	 */
	private Node getExistingNodeInDSWithPath(DataSource ds, String path) {

		HashMap<String, Node> nodeHashMap = getNodePerDSandPath.get(ds);
		if (nodeHashMap != null) {
			return nodeHashMap.get(path);
		}
		return null;
	}

	/**
	 *
	 * @param ds
	 * @param label
	 * @return node for this dataset and this label
	 */
	private Node getExistingNodeInDSWithLabel(DataSource ds, String label) {

		HashMap<String, Node> nodeHashMap = nodePerDSandLabel.get(ds);
		if (nodeHashMap != null) {
			return nodeHashMap.get(label);
		}
		return null;
	}

	/**
	 * Put the node created and save it for this ds and for this node.
	 *
	 * @param ds
	 * @param label
	 * @param childNode
	 */
	private void saveNode(DataSource ds, String label, Node childNode, HashMap<DataSource, HashMap<String, Node>> nodesMap) {
		HashMap<String, Node> nodeHashMap = nodesMap.get(ds);
		if (nodeHashMap == null) {
			nodeHashMap = new HashMap<>();
		}
		nodeHashMap.put(label, childNode);
		nodesMap.put(ds, nodeHashMap);
	}

	/** Adding edges to local and original URI nodes, if they exist */
	void processEdgesToLocalAndOriginalURIs(Consumer<Edge> processor) {
		if (toOriginalURI != null) {
			processor.accept(toOriginalURI);
		}
		if (toLocalURI != null) {
			processor.accept(toLocalURI);
		}
	}

	// for construction
	public Node buildNode(NodeID globalID, String l, Supplier<Node> rep, Node.Types type) {
		return buildNode(globalID, l, rep, null, 0, type);
	}

	public Edge buildUnlabelledEdge(Node source, Node target) {
		return buildUnlabelledEdge(source, target, 1.0);
	}

	public Edge buildLabelledEdge(Node source, Node target, String l) {
		return buildLabelledEdge(source, target, l, 1.0);
	}

	public Edge buildSameAsEdge(Node source, Node target) {
		return buildSameAsEdge(source, target, 1.0);
	}

	public static boolean needsConversionBeforeLoading(URI uri) {
		return (isPDFFileURI(uri) || isOfficeFileURI(uri));
	}

	public static boolean isPDFFileURI(URI uri) {
		return uri.toString().toLowerCase().endsWith(".pdf");
	}

	public static boolean isDocumentURI(URI uri) {
		String s = uri.toString().toLowerCase();
		return (s.endsWith(".doc") || s.endsWith(".docx") || s.endsWith(".dot"));
	}

	public static boolean isSpreadsheetURI(URI uri) {
		String s = uri.toString().toLowerCase();
		return (s.endsWith(".ods") || s.endsWith(".xls") || s.endsWith(".xlsx"));
	}

	public static boolean isPresentationURI(URI uri) {
		String s = uri.toString().toLowerCase();
		return (s.endsWith(".odp") || s.endsWith(".ppt") || s.endsWith(".pptx"));
	}

	public static boolean isOfficeFileURI(URI uri) {
		return (isDocumentURI(uri) || isSpreadsheetURI(uri) || isPresentationURI(uri));
	}

//	public static void main(String[] argv) {
//		String[] inputs = {"A.", "B. ", "A.B.", "A. B. ", "A.-B.", "AB", "A-B.", "Ph.", "J.-Ph.", "Philippe", "IBM", "ABCDE", "UVWXYZEFG"};
//		for (String s: inputs) {
//			Matcher m = frenchFirstNameInitialsPattern.matcher(s);
//			while (m.find()) {
//                log.info("FR  found " + m.group()+
//                    " starting at " + m.start() +
//                    " and ending at " + m.end());
//			}
//			Matcher m2 = englishFirstNameInitialsPattern.matcher(s);
//			while (m2.find()) {
//				log.info("EN  found " + m2.group()+
//	                    " starting at " + m2.start() +
//	                    " and ending at " + m2.end());
//			}
//		}
//	}
}

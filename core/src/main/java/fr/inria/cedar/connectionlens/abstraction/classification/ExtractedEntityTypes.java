package fr.inria.cedar.connectionlens.abstraction.classification;

import fr.inria.cedar.connectionlens.graph.Node;

import java.util.HashMap;

public class ExtractedEntityTypes {
    private HashMap<ExtractedEntityType, Node.Types> extractedEntityTypes;
    private HashMap<Integer, ExtractedEntityType> extractedEntityTypesIds;

    public ExtractedEntityTypes() {
        this.extractedEntityTypes = new HashMap<>();
        this.extractedEntityTypesIds = new HashMap<>();
        this.addTypes();
    }

    public void addTypes() {
        // by first adding our own entity types
        this.addNewType(Node.Types.RDF_URI.name(), Node.Types.RDF_URI, Node.Types.RDF_URI.ordinal());
        this.addNewType(Node.Types.ENTITY_PERSON.name(), Node.Types.ENTITY_PERSON, Node.Types.ENTITY_PERSON.ordinal());
        this.addNewType(Node.Types.ENTITY_LOCATION.name(), Node.Types.ENTITY_LOCATION, Node.Types.ENTITY_LOCATION.ordinal());
        this.addNewType(Node.Types.ENTITY_ORGANIZATION.name(), Node.Types.ENTITY_ORGANIZATION, Node.Types.ENTITY_ORGANIZATION.ordinal());
        this.addNewType(Node.Types.FIRST_NAME.name(), Node.Types.ENTITY_PERSON, Node.Types.FIRST_NAME.ordinal());
        this.addNewType(Node.Types.EMAIL.name(), Node.Types.EMAIL, Node.Types.EMAIL.ordinal());
        this.addNewType(Node.Types.HASHTAG.name(), Node.Types.HASHTAG, Node.Types.HASHTAG.ordinal());
        this.addNewType(Node.Types.DATE.name(), Node.Types.DATE, Node.Types.DATE.ordinal());

        // then add the types that are used in Schema.org and DBpedia
        this.addNewTypeSimple("http://dbpedia.org/ontology/Person", Node.Types.ENTITY_PERSON);
        this.addNewTypeSimple("schema:Person", Node.Types.ENTITY_PERSON);
        this.addNewTypeSimple("http://dbpedia.org/ontology/Organisation", Node.Types.ENTITY_ORGANIZATION);
        this.addNewTypeSimple("schema:Organization", Node.Types.ENTITY_ORGANIZATION);
        this.addNewTypeSimple("http://dbpedia.org/ontology/Place", Node.Types.ENTITY_LOCATION);
        this.addNewTypeSimple("schema:Place", Node.Types.ENTITY_LOCATION);
//        this.addNewTypeSimple("http://www.w3.org/2001/XMLSchema#date", Node.Types.DATE);
//        this.addNewTypeSimple("http://www.w3.org/2001/XMLSchema#gYear", Node.Types.DATE);
//        this.addNewTypeSimple("schema:Date", Node.Types.DATE);
//        this.addNewTypeSimple("schema:DateTime", Node.Types.DATE);
    }

    // wa cannot assign new IDs to extracted types because this will clash with current IDs for extracted types in CL
    public void addNewTypeSimple(String extractedTypeLabel, Node.Types extractedType) {
        ExtractedEntityType extractedEntityType = new ExtractedEntityType(extractedTypeLabel);
        this.extractedEntityTypes.put(extractedEntityType, extractedType);
    }
    public void addNewType(String extractedTypeLabel, Node.Types extractedType, int id) {
        ExtractedEntityType extractedEntityType = new ExtractedEntityType(id, extractedTypeLabel);
        this.extractedEntityTypes.put(extractedEntityType, extractedType);
        this.extractedEntityTypesIds.put(extractedEntityType.id, extractedEntityType);
    }

    public ExtractedEntityType getExtractedEntityTypesFromId(int id) {
        return this.extractedEntityTypesIds.getOrDefault(id, null);
    }

    public HashMap<ExtractedEntityType, Node.Types> getExtractedEntityTypes() {
        return extractedEntityTypes;
    }
}

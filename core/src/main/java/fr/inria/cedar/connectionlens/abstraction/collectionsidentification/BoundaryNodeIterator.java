package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import org.apache.log4j.Logger;

import java.util.*;

public class BoundaryNodeIterator implements Iterator<BoundaryNode> {
    public static final Logger log = Logger.getLogger(BoundaryNodeIterator.class);

    private Set<BoundaryNode> visited = new HashSet<>();
    private Queue<BoundaryNode> queue = new LinkedList<>();
    private CollectionBoundary boundary;


    public BoundaryNodeIterator(CollectionBoundary collectionBoundary) {
        this.boundary = collectionBoundary;
        this.queue.add(this.boundary.getMainCollectionRoot());
        this.visited.add(this.boundary.getMainCollectionRoot());
    }

    @Override
    public boolean hasNext() {
        return !this.queue.isEmpty();
    }

    @Override
    public BoundaryNode next() {
        if(!this.hasNext()) {
            throw new NoSuchElementException();
        } else {
            // remove the first element of the queue
            BoundaryNode next = this.queue.remove();
            // add all the next elements in the queue
            for(BoundaryNode bn : next.getChildren()) {
                if(!this.visited.contains(bn)) {
                    this.queue.add(bn);
                    this.visited.add(bn);
                }
            }
            return next;
        }
    }
}

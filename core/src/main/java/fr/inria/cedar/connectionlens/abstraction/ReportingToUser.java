package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.abstraction.classification.Classification;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.BoundaryNode;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.MainCollectionsIdentification;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.REPORTING_CREATE_DESCRIPTION_MEM;

/**
 * This class is used to report "best" collections to the user after running the abstraction.
 * The output given to the user is a small HTML document describing the dataset he gave. The description lists the top-k collections with their categories and their size.
 * The "best" collections are the ones that are the most-weighted (after the propagation of data weights) in the collection graph.
 */
public class ReportingToUser extends AbstractionTask {
    public static final Logger log = Logger.getLogger(ReportingToUser.class);

    private HashMap<Integer, Double> originalCollectionsDataWeights; // we keep it in order to keep a history of the collection weights (for debugging purposes)
    private String filenameForDescription;
    private MainCollectionsIdentification mainCollectionsIdentification;
    private HashMap<Integer, ArrayList<String>> firstLevelBoundaryNodesMainCollections; // first-level properties, used for the E/R schema
    private final Integer MAX_NB_PROPERTIES = -1;
    private String filenameDotDrawing;
    private String filenamePngDrawing;
    private String filenamePngDrawingTomcat;
    private String pathKeyIcon;
    private ArrayList<String> COLORS_MAP;
    private HashMap<Integer, String> collectionColors;
    private final String pathToReportingFolder = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "..", "reporting").toString();
    private String htmlEntities;
    private String htmlRelationships;
    private boolean showPKs;


    public ReportingToUser(ConnectionLens cl, DataSource ds, MainCollectionsIdentification mainCollectionsIdentification) {
        super(cl, ds);

        this.filenameForDescription = "description_" + AbstractionTask.getDatasetName() + "_" + AbstractionTask.getMethodsNames() + ".html";
        this.firstLevelBoundaryNodesMainCollections = new HashMap<>();
        this.mainCollectionsIdentification = mainCollectionsIdentification;
        this.showPKs = Config.getInstance().getBooleanProperty("show_PK");

        // initialize the color map
        this.COLORS_MAP = new ArrayList<>();
        this.COLORS_MAP.add("#F0F8FF"); // alice blue
        this.COLORS_MAP.add("#FAEBD7"); // antique white
        this.COLORS_MAP.add("#F5F5DC"); // beige
        this.COLORS_MAP.add("#FFF8DC"); // Corn silk
        this.COLORS_MAP.add("#FFF0F5"); // lavender blush
        this.COLORS_MAP.add("#E0FFFF"); // light cyan
        this.COLORS_MAP.add("#FFE4B5"); // moccasin
        this.COLORS_MAP.add("#AFEEEE"); // pale turquoise
        this.collectionColors = new HashMap<>();

        // initialize files
        this.filenameDotDrawing = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "ERSchema_" + AbstractionTask.getDatasetName() + "_" + AbstractionTask.getMethodsNames() + ".dot").toString();
        this.filenamePngDrawing = Paths.get(Config.getInstance().getStringProperty("temp_dir"), "ERSchema_" + AbstractionTask.getDatasetName() + "_" + AbstractionTask.getMethodsNames() + ".png").toString();
        this.filenamePngDrawingTomcat = Paths.get(Config.getInstance().getStringProperty("tomcat_dir"), "img", "ERSchema_" + AbstractionTask.getDatasetName() + "_" + AbstractionTask.getMethodsNames() + ".png").toString();
        this.pathKeyIcon = Paths.get(this.pathToReportingFolder, "key-icon.png").toString();
    }

    public ReportingToUser() {

    }
    
    /**
     * Load the summary graph into memory and initialize allPaths and inCycle variables.
     * allPaths is a 2D array of ArrayList where a cell[i][j] contains all paths from collection i to collection j as an Arraylist.
     * allPaths does not contain cyclic edges.
     * @throws SQLException if a query fails
     * @throws IOException if the file for the final description has a problem
     */
    public void run() throws Exception {
        log.info("REPORTING TO USER starts");
//        log.debug(CollectionGraph.getInstance().getMainCollectionsAsList());
        this.localCollector.start(StatisticsCollector.total());
        this.createDescription();
        this.localCollector.tick(StatisticsCollector.total(), REPORTING_CREATE_DESCRIPTION_MEM);
        this.localCollector.stop(StatisticsCollector.total());
        log.info("REPORTING TO USER ends");
    }
    
    /**
     * Create the HTML file describing the datasets in the terms of the user.
     * We list the first top-k collections that have been reported
     * and give information about them: size, path and most frequent category
     * @throws IOException if there is a problem with the file to write the description
     */
    public void createDescription() throws Exception {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(Paths.get(Config.getInstance().getStringProperty("temp_dir"), this.filenameForDescription).toString()));

        this.selectRandomColorForEachCollection(); // get one color for each reported entity collection

        // a. we get the reported collections, and we sort them by the decreasing order of their size
        ArrayList<ImmutablePair<Integer, Integer>> reportedCollectionsWithSize = new ArrayList<>();
        for(Integer mainCollection : CollectionGraph.getInstance().getMainCollections()) {
            ImmutablePair<Integer, Integer> pairCollectionIdCollectionSize = new ImmutablePair<>(mainCollection, CollectionGraph.getInstance().getCollectionSize(mainCollection));
            reportedCollectionsWithSize.add(pairCollectionIdCollectionSize);
        }
        // decreasing order (with the minus sign)
        reportedCollectionsWithSize.sort((o1, o2) -> -Integer.compare(o1.getRight(), o2.getRight()));

        // create the HTML description
        String descriptionTemplate = FileUtils.readFileToString(new File(Paths.get(Config.getInstance().getStringProperty("temp_dir"), "..", "reporting", "description-template.html").toString()));

        StringBuilder htmlDescriptionEntities = new StringBuilder();
        htmlDescriptionEntities.append("<h4>Entities:</h4>");
        for(ImmutablePair<Integer, Integer> reportedCollection : reportedCollectionsWithSize) {
            int collectionId = reportedCollection.getLeft();
            int collectionSize = reportedCollection.getRight();
            htmlDescriptionEntities.append("<div>");
//            if(CollectionGraph.getInstance().checkIfCollectionHasCategory(collectionId)) {
//                int collectionCategory = CollectionGraph.getInstance().getCollectionCategory(collectionId);
//                if(collectionCategory == Classification.getCategoryId("COLLECTION") || !Classification.getDefaultCategories().contains(collectionCategory)) { // some collections are just COLLECTIONS, therefore we need to classify them as OTHER
//                    collectionCategory = Classification.getCategoryId("CATEGORY_OTHER");
//                }
//                String collectionCategoryLabel = Classification.getCategoryLabel(collectionCategory);
////                log.debug(collectionCategoryLabel);
//                htmlDescriptionEntities.append("<img src=\"" + Paths.get(this.pathToReportingFolder, collectionCategoryLabel + ".png") + "\" width=\"40\" height=\"40\" style=\"margin:5px; vertical-align: middle;\">\n");
//            }
            htmlDescriptionEntities.append("<span style=\"background-color: "+this.collectionColors.get(collectionId)+";\">A collection of " + collectionSize + " ");

            String collectionLabel = CollectionGraph.getInstance().getCollectionLabel(collectionId);
            if(CollectionGraph.getInstance().checkIfCollectionHasCategory(collectionId)) {
                int collectionCategory = CollectionGraph.getInstance().getCollectionCategory(collectionId);
                if(Classification.getCategoryIds("CATEGORY_OTHER", "COLLECTION").contains(collectionCategory)) {
                    // because we don't know what it is, we report the name of the collection
                    // and if the collection path is just one "." then these are maps in an array (in JSON), then saying that the object is not labelled
                    if(!collectionLabel.isEmpty()) {
                        htmlDescriptionEntities.append(collectionLabel);
                    } else {
                        htmlDescriptionEntities.append("Entities (no label)");
                    }
                } else {
                    htmlDescriptionEntities.append(Classification.getCategoryInformativeLabel(collectionCategory));
                }
            } else {
                htmlDescriptionEntities.append(collectionLabel);
            }
//            os.write((" labelled " + this.collectionsPaths.get(collectionId) + " ").getBytes());
            // here, we use the collection boundary to know which collections are considered as "properties" for the current collection
            htmlDescriptionEntities.append(" having the following properties: <i class='fa-solid fa-angles-up' style='padding-left: 5px;'></i></span> \n");

            // TODO NELLY: check ho to sort
//            log.info("sort collection C" + collectionId);
//            log.debug(CollectionGraph.getInstance().getCollectionBoundary(collectionId));
//            CollectionGraph.getInstance().getCollectionBoundary(collectionId).sortByFrequency(CollectionGraph.getInstance().getCollectionsFrequencies());
//            log.debug(CollectionGraph.getInstance().getCollectionBoundary(collectionId));
            log.debug(CollectionGraph.getInstance().getCollectionBoundary(collectionId));
            this.displayBoundaryOfCollection(CollectionGraph.getInstance().getCollectionBoundary(collectionId).getMainCollectionRoot(), htmlDescriptionEntities, true);
            htmlDescriptionEntities.append("</div><br>\n");
        }

        StringBuilder htmlDescriptionRelationships = new StringBuilder();
        if(!CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections().isEmpty()) {
            htmlDescriptionRelationships.append("<h4>Relationships:</h4><ul>");
            for(String key: CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections().keySet()) {
                String[] idsSplit = key.split("-");
                Integer sourceCollection = Integer.valueOf(idsSplit[0]);
                Integer targetCollection = Integer.valueOf(idsSplit[1]);
                Integer sourceCategory = CollectionGraph.getInstance().getCollectionCategory(sourceCollection);
                Integer targetCategory = CollectionGraph.getInstance().getCollectionCategory(targetCollection);
                String sourceCollectionLabel = CollectionGraph.getInstance().getCollectionLabel(sourceCollection); // TODO NELLY uncomment (!Classification.getCategoryIds("COLLECTION", "CATEGORY_OTHER").contains(sourceCategory) ? Classification.getCategoryInformativeLabel(CollectionGraph.getInstance().getCollectionCategory(sourceCollection)) : CollectionGraph.getCollectionPath(sourceCollection));
                String targetCollectionLabel = CollectionGraph.getInstance().getCollectionLabel(targetCollection); // TODO NELLY uncomment (!Classification.getCategoryIds("COLLECTION", "CATEGORY_OTHER").contains(targetCategory) ? Classification.getCategoryInformativeLabel(CollectionGraph.getInstance().getCollectionCategory(targetCollection)) : CollectionGraph.getCollectionPath(targetCollection));
                ArrayList<String> relationshipLabels = CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections().get(key);
                for(String relationshipLabel : relationshipLabels) { // there might be several relationships for two main entities (author has written and reviewed some papers)
                    if(this.isXML && relationshipLabel.contains(",")) {
                        // this is a relation coming from XML where the ID/IDREF relation was nested
                        // in this case we want to keep the first label because this is the XML ID/IDREF label
                        String[] partsOfLabel = relationshipLabel.split(",");
                        relationshipLabel = partsOfLabel[partsOfLabel.length-1];
                    }
                    htmlDescriptionRelationships.append("<li>A collection of <span style=\"background-color: "+this.collectionColors.get(sourceCollection)+";\">" + sourceCollectionLabel + "</span> and a collection of <span style=\"background-color: "+this.collectionColors.get(targetCollection)+";\">" + targetCollectionLabel + "</span> are related with the property \"" + relationshipLabel + "\"</li>");
                }
            }
            htmlDescriptionRelationships.append("</ul>\n");
        }

        // draw the ER schema
        this.createERSchemaDrawing();

        // set the elements for the GUI
        this.htmlEntities = htmlDescriptionEntities.toString();
        this.htmlRelationships = htmlDescriptionRelationships.toString();
        // add all elements to the HTML description
        descriptionTemplate = descriptionTemplate.replace("%%abstraIcon", Paths.get(this.pathToReportingFolder, "abstra-icon.png").toString());
        descriptionTemplate = descriptionTemplate.replace("%%datasetName", datasetName);
        descriptionTemplate = descriptionTemplate.replace("%%description", htmlDescriptionEntities.toString());
        descriptionTemplate = descriptionTemplate.replace("%%relationships", htmlDescriptionRelationships.toString());
        descriptionTemplate = descriptionTemplate.replace("%%keyIcon", Paths.get(this.pathToReportingFolder, "key-icon.png").toString());
        descriptionTemplate = descriptionTemplate.replace("%%filenamePngDrawing", this.filenamePngDrawing);
//        os.write(descriptionTemplate.getBytes());
        String tmpDescription = "<div class=\"container-fluid\">\n" +
                "    <h2>Here's what your dataset %%datasetName contains!</h2>\n<div class=\"row\">\n" +
                "        <div class=\"col-sm-6\">\n" +
                "            <h3>Entity/Relationship schema</h3>\n" +
                "            <img src=\"%%filenamePngDrawing\" style=\"margin-left: 20px; max-height:700px; max-width:700px;\"\n" +
                "                 alt=\"Entity Relationship schema\">\n" +
                "        </div>\n" +
                "        <div class=\"col-sm-6\">\n" +
                "            <h3>Description</h3>";
        tmpDescription += htmlDescriptionEntities.toString();
        tmpDescription += htmlDescriptionRelationships.toString();
        tmpDescription += "</div>\n" +
                "    </div>";
        tmpDescription = tmpDescription.replace("%%datasetName", datasetName);
        tmpDescription = tmpDescription.replace("%%keyIcon", Paths.get(this.pathToReportingFolder, "key-icon.png").toString());
        tmpDescription = tmpDescription.replace("%%filenamePngDrawing", this.filenamePngDrawing);
        System.out.println(tmpDescription);
        os.write(tmpDescription.getBytes());
        os.flush();
        os.close();
    }


    // TODO NELLY: the CollectionBoundary iterator could be used here. Check how to do it
    private void displayBoundaryOfCollection(BoundaryNode boundaryNode, StringBuilder htmlDescriptionEntities, boolean firstLevel) {
        if(!boundaryNode.getChildren().isEmpty()) {
            htmlDescriptionEntities.append("<ul>");
            for(BoundaryNode boundaryNodeChild : boundaryNode.getChildren()) {
                int boundaryNodeChildId = boundaryNodeChild.getId();
                // the child is not a leaf nor a relationship
                String collectionLabel = CollectionGraph.getInstance().getCollectionLabel(boundaryNodeChildId);
                if(!CollectionGraph.getInstance().isLeaf(boundaryNodeChildId) && !this.checkCollectionIsInRelationships(CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections(), collectionLabel)) {
                    if (firstLevel) {
                        if (this.firstLevelBoundaryNodesMainCollections.containsKey(boundaryNode.getId())) {
                            this.firstLevelBoundaryNodesMainCollections.get(boundaryNode.getId()).add(collectionLabel);
                        } else {
                            ArrayList<String> listOfProperties = new ArrayList<>();
                            listOfProperties.add(collectionLabel);
                            this.firstLevelBoundaryNodesMainCollections.put(boundaryNode.getId(), listOfProperties);
                        }
                    }
                    DecimalFormat numberFormat = new DecimalFormat("0");
                    if(this.showPKs && (collectionLabel.endsWith("@id") || collectionLabel.equals("id"))) {
                        htmlDescriptionEntities.append("<li><img src=\""+this.pathKeyIcon+"\" width=\"15\" height=\"15\">" + collectionLabel + " (" + numberFormat.format(CollectionGraph.getInstance().getCollectionFrequency(boundaryNode.getId(), boundaryNodeChildId) * 100) + "%) ");
                    } else {
                        htmlDescriptionEntities.append("<li>" + collectionLabel + " (" + numberFormat.format(CollectionGraph.getInstance().getCollectionFrequency(boundaryNode.getId(), boundaryNodeChildId) * 100) + "%) ");
                    }

                    // we add >> only if the property has at least one child which is not a leaf
                    if(!boundaryNodeChild.getChildren().isEmpty() && !CollectionGraph.getInstance().isLeaf(boundaryNodeChild.getChildren().get(0).getId())) {
                        htmlDescriptionEntities.append("<i class='fa-solid fa-angles-up' style='left-padding: 5px;'></i></li>");
                    }
                    this.displayBoundaryOfCollection(boundaryNodeChild, htmlDescriptionEntities, false);
                }
            }
            htmlDescriptionEntities.append("</ul>");
        }
    }

    private void createERSchemaDrawing() throws IOException, SQLException {
        BufferedWriter bw;
        String pathToDot = Config.getInstance().getProperty("drawing.dot_installation");
        bw = new BufferedWriter(new FileWriter(this.filenameDotDrawing));

        // draw the E/R diagram
        bw.write("digraph { node [shape=box]; rankdir=\"LR\"; ");

        HashMap<Integer, Integer> collectionsNbProperProperties = new HashMap<>();

        for(Integer entityCollection : CollectionGraph.getInstance().getMainCollections()) {
            int nbProperProperties = 0;
            StringBuilder text = new StringBuilder();
            int collectionCategory = CollectionGraph.getInstance().getCollectionCategory(entityCollection);
            String collectionColor = this.collectionColors.get(entityCollection);
            log.debug(collectionCategory);
            log.debug(Classification.getCategoryLabel(collectionCategory));
            String entityFile = (Classification.getDefaultCategories().contains(collectionCategory) && !Classification.getCategoryIds("COLLECTION").contains(collectionCategory) ? Paths.get(this.pathToReportingFolder, Classification.getCategoryLabel(collectionCategory) + ".png").toString() : Paths.get(this.pathToReportingFolder, "CATEGORY_OTHER.png").toString());
            String collectionPath = CollectionGraph.getInstance().getCollectionLabel(entityCollection);
            String entityLabel = (!Classification.getCategoryIds("COLLECTION", "CATEGORY_OTHER").contains(collectionCategory) ? Classification.getCategoryInformativeLabel(collectionCategory) : (collectionPath.isEmpty() ? "Entities" : collectionPath));
            text.append("E").append(entityCollection)
                    .append(" [label=<<table border=\"0\" cellborder=\"1\" cellspacing=\"0\" cellpadding=\"2\">")
                    .append("<tr><td bgcolor=\"").append(collectionColor).append("\">")
                    .append("<img src=\"").append(entityFile).append("\" />")
                    .append("</td><td align='left' bgcolor=\"").append(collectionColor).append("\">").append(entityLabel).append(" (" + collectionPath + ") ")
                    .append(" (").append(CollectionGraph.getInstance().getCollectionSize(entityCollection)).append(")</td></tr>");
            if(this.firstLevelBoundaryNodesMainCollections.containsKey(entityCollection)) {
                for(String property : this.firstLevelBoundaryNodesMainCollections.get(entityCollection)) {
                    if(!this.checkCollectionIsInRelationships(CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections(), property)) {
                        if(this.showPKs && (property.endsWith("@id") || property.equals("id"))) {
                            text.append("<tr>")
                                    .append("<td>").append(property).append("</td>")
                                    .append("<td align='left'><img src=\"").append(this.pathKeyIcon).append("\" /></td>")
                                    .append("</tr>");
                        } else {
                            text.append("<tr><td align=\"left\" colspan=\"2\">")
        //                            .append("<img src=\"").append(keyIconPath).append("\" />")
                                    .append(property).append("</td></tr>");
                        }
                        nbProperProperties++;
                    }
                }
            }
            text.append("</table>> margin=0 shape=none ]\n");
            collectionsNbProperProperties.put(entityCollection, nbProperProperties);
            // finally, we write the entity collection only if it contains at least one proper property (e.g. we don't report the "edges" collection in XMark)
//            if(nbProperProperties > 0) {
                bw.write(text.toString());
//            }
            String datasetPath = AbstractionTask.getDatasetPath();
            int nbNormNodes = CollectionGraph.getInstance().getNbNormalizedNodes();
            int nbCollections = CollectionGraph.getInstance().getTotalNbCollections();
            int nbMainCollections = CollectionGraph.getInstance().getMainCollections().size();
            double dataCoverage = this.mainCollectionsIdentification.getConfiguration().getNodeCoverage();
            int k = this.mainCollectionsIdentification.getConfiguration().getK();
            Configuration.SCORING_METHOD sm = this.mainCollectionsIdentification.getConfiguration().getScoringMethod();
            String scoringMethodName = sm.equals(Configuration.SCORING_METHOD.DESC_K) ? "DESC_" + k : (sm.equals(Configuration.SCORING_METHOD.LEAF_K) ? "LEAF_" + k : this.mainCollectionsIdentification.getConfiguration().getScoringMethod().name());
            DecimalFormat numberFormat = new DecimalFormat("0");
            bw.write("fontsize=12; label=\"Abstraction of " + datasetPath + " (" +
                    "" + nbNormNodes + " normalized nodes, " +
                    "" + nbCollections + " collections, " +
                    "" + nbMainCollections + " main collections, " +
                    "data coverage is " + numberFormat.format(dataCoverage * 100) + "%) \n" +
                    "with parameters " +
                    "" + scoringMethodName + ", " +
                    "" + this.mainCollectionsIdentification.getConfiguration().getBoundaryMethod() + ", " +
                    "" + this.mainCollectionsIdentification.getConfiguration().getGraphUpdateMethod() + ", " +
                    "" + this.mainCollectionsIdentification.getConfiguration().getIdrefEdgesUsage() + "\"\n" +
                    "labelloc=top; labeljust=center;"); // add a title with the method used
        }

        for(String key : CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections().keySet()) {
            String[] idsSplit = key.split("-");
            Integer collSource = Integer.valueOf(idsSplit[0]);
            Integer collTarget = Integer.valueOf(idsSplit[1]);
            if(CollectionGraph.getInstance().getMainCollections().contains(collSource) && CollectionGraph.getInstance().getMainCollections().contains(collTarget)) {
                // we display the relation only if both collections are returned
                ArrayList<String> relationshipLabels = CollectionGraph.getInstance().getRelationshipsBetweenMainEntityCollections(key);
                for(String relationshipLabel : relationshipLabels) {
                    if(this.isXML && relationshipLabel.contains(",")) {
                        // this is a relation coming from XML where the ID/IDREF relation was nested
                        // in this case we want to keep the last label of the path because this is the XML ID/IDREF label
                        String[] partsOfLabel = relationshipLabel.split(",");
                        relationshipLabel = partsOfLabel[partsOfLabel.length-1];
                    }
                    bw.write("E"+collSource+" -> " + "E"+collTarget + "[label=\"" + relationshipLabel + "\"]; \n");
                }
            }
        }

        bw.write("}");
        bw.close();

        // finally, generate the PNG file
        try {
            String[] pngCommand = { pathToDot, "-Tpng", this.filenameDotDrawing, "-o", this.filenamePngDrawing };
            Runtime.getRuntime().exec(pngCommand);
            String[] cpCommand = {"cp", this.filenamePngDrawing, Paths.get(Config.getInstance().getStringProperty("tomcat_dir"), "img").toString() }; // copy also the E-R schema in tomcat
            System.out.println(this.filenamePngDrawing);
            System.out.println(Paths.get(Config.getInstance().getStringProperty("tomcat_dir"), "img").toString());
            Runtime.getRuntime().exec(cpCommand);
        } catch (IOException e) {
            log.info("Could not process .dot files: install graphviz or check the drawing.dot_installation value in local.settings " + e.toString());
        }

    }

    /**
     * Assign a random color to each reported entity collection.
     * This is used to help non-expert users with the E/R schema
     */
    private void selectRandomColorForEachCollection() {
        Random rand = new Random();
        ArrayList<String> alreadyChosenColors = new ArrayList<>();

        for(Integer entityCollection : CollectionGraph.getInstance().getMainCollections()) {
            String randomColor;
            if(alreadyChosenColors.size() == this.COLORS_MAP.size()) { // if all colors have been picked already, we start again
                alreadyChosenColors = new ArrayList<>();
            }
            do {
                randomColor = this.COLORS_MAP.get(rand.nextInt(this.COLORS_MAP.size()));
                this.collectionColors.put(entityCollection, randomColor);
            } while(alreadyChosenColors.contains(randomColor));
            alreadyChosenColors.add(randomColor);
        }
    }

    // return true if the collection label is involved in one of the relationship
    private boolean checkCollectionIsInRelationships(HashMap<String, ArrayList<String>> relationships, String collectionLabel) {
        boolean isInvolved = false;
        for(ArrayList<String> relationship : relationships.values()) {
            for(String relation : relationship) {
                if(relation.startsWith(collectionLabel)) {
                    isInvolved = true;
                    break;
                }
            }
        }
        return isInvolved;
    }

    public HashMap<Integer, Double> getOriginalCollectionsDataWeights() {
        return this.originalCollectionsDataWeights;
    }
    
    public HashMap<Integer, ArrayList<String>> getFirstLevelBoundaryNodesMainCollections() {
        return this.firstLevelBoundaryNodesMainCollections;
    }

    public String getHtmlEntities() {
        return this.htmlEntities;
    }

    public String getHtmlRelationships() {
        return this.htmlRelationships;
    }

    public String getPngDrawing() {
        return this.filenamePngDrawing;
    }

    public String getFilenamePngDrawingTomcat() {
        return this.filenamePngDrawingTomcat;
    }

    public String getPathToReportingFolder() {
        return this.pathToReportingFolder;
    }

    public void reportStatistics() {
        this.localCollector.stopAll();
        this.localCollector.reportAll();
    }
}

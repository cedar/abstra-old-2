/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.sql.schema;

import fr.inria.cedar.connectionlens.graph.Graph;

/**
 * Static SQL Statements.
 */
public class StaticStatements {

	/** The name of tables where the information is stored  */
	private final String nodesTableName ;
	private final String sameAsEdgesTableName ;
	private final String weakSameAsEdgesTableName ;
	
	/** The statements */
	public String GLOBAL_PATHS_VIEW;
	private String SAME_AS_EDGES;
	public String MATERIALIZED_SAME_AS_EDGES;
	public String VIRTUAL_SAME_AS_EDGES;

	/**
	 * instantiates a StaticStatements
	 * @param typeOfGraph the type of the graph, used to retrieve the names of the tables
	 */
	public StaticStatements(){
		this.nodesTableName = SchemaTableNames.ORIGINAL_NODES_TABLE_NAME;
		this.sameAsEdgesTableName = SchemaTableNames.ORIGINAL_SAME_AS_EDGES_TABLE_NAME;
		this.weakSameAsEdgesTableName=SchemaTableNames.ORIGINAL_WEAK_SAME_AS_TABLE_NAME;

		this.set_same_as_edges();
		this.set_materialized_same_as_edges();
		this.set_virtual_same_as_edges();

	}

	/** Materialized view for same-as edges. */
	private void set_same_as_edges(){
		this.SAME_AS_EDGES =    sameAsEdgesTableName
			+" AS "
			+ " SELECT l1.id AS node1, l1.ds AS ds1, l2.id AS node2, l2.ds AS ds2, 1.0 AS confidence "
			+ " FROM   "+   nodesTableName+" l1, "+   nodesTableName+" l2 "
			+ " WHERE  l1.representative = l2.representative "
			+ " AND    (l1.id <> l2.id OR l1.ds <> l2.ds) "
			+ " UNION "
			+ " SELECT l1.id AS node1, l1.ds AS ds1, l1.representative AS node2, l2.ds AS ds2, 1.0 AS confidence "
			+ " FROM   "+   nodesTableName+" l1, "+   nodesTableName+" l2 "
			+ " WHERE  l1.id <> l1.representative "
			+ " AND    l1.representative = l2.id "
			+ " UNION "
			+ " SELECT l1.representative AS node1, l2.ds AS ds1, l1.id AS node2, l1.ds AS ds2, 1.0 AS confidence "
			+ " FROM   "+   nodesTableName+" l1, "+   nodesTableName+" l2 "
			+ " WHERE  l1.id <> l1.representative "
			+ " AND    l1.representative = l2.id "
			+ " UNION "
			+ " SELECT node1, ds1, node2, ds2, confidence "
			+ " FROM "+   weakSameAsEdgesTableName
			+" ; "
			;

	}
	
	/** Materialized view for same-as edges. */
	private void set_materialized_same_as_edges(){
		this.MATERIALIZED_SAME_AS_EDGES = "CREATE MATERIALIZED VIEW " + this.SAME_AS_EDGES
			+ "CREATE INDEX "+   sameAsEdgesTableName+"_node1 ON "+   sameAsEdgesTableName+"(node1);"
			+ "CREATE INDEX "+   sameAsEdgesTableName+"_node2 ON "+   sameAsEdgesTableName+"(node2);"
			;
	}

	/** Virtual view for same-as edges. */
	private void set_virtual_same_as_edges(){
		this.VIRTUAL_SAME_AS_EDGES = "CREATE VIEW " + this.SAME_AS_EDGES;
	}

}


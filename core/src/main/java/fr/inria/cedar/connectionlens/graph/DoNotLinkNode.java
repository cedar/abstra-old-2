/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import java.util.function.Supplier;

import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.source.DataSource;

public class DoNotLinkNode extends Node{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1355920289289589074L;
	public DoNotLinkNode(NodeID id, String label, DataSource ds, Supplier<Node> rep) {
		super(id, label, ds, rep, Node.Types.DO_NOT_LINK_VALUE);
	}
	public DoNotLinkNode(NodeID id, String label, String normaLabel, DataSource ds) {
		this(id, label, normaLabel, ds, null);
	}
	public DoNotLinkNode(NodeID id, String label, String normaLabel, DataSource ds, Supplier<Node> rep) {
		super(id, label, normaLabel, ds, rep, Node.Types.DO_NOT_LINK_VALUE);
	}
	@Override
	void specificNormalization() {
		// nop
	}
}

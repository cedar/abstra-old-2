package fr.inria.cedar.connectionlens.score;

import fr.inria.cedar.connectionlens.score.Score.DoubleScore;

/**
 * Computes a score as the number of edges in the input 
 * scorable object.
 */
public class AnswerSizeScore implements ScoringFunction {
	
	@Override
	public DoubleScore compute(Scorable e) {
		return new DoubleScore(e.edges().size());
	}

}

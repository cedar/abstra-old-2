/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import info.debatty.java.lsh.LSH;
import info.debatty.java.lsh.MinHash;
import org.apache.log4j.Logger;

import java.util.Set;

/**
 * This class is to allow setting alternative threshold. The parent class currently only support a
 * threshold of .5.
 * 
 * @author Julien LEBLAY
 *
 */
public class MinHashLSH extends LSH {

	/** The logger. */
	private static final Logger log = Logger.getLogger(MinHashLSH.class);

    private final MinHash mh;

    /**
     * Instantiates a LSH instance that internally uses MinHash,
     * with s stages (or bands) and b buckets (per stage), for sets out of a
     * dictionary of n elements.
     *
     * Attention: the number of buckets should be chosen such that we have at
     * least 100 items per bucket.
     *
     * @param s stages
     * @param r number of row per stage
     * @param b buckets (per stage)
     * @param n dictionary size
     * @param seed random number generator seed. using the same value will
     * guarantee identical hashes across object instantiations
     * @param th 
     */
    public MinHashLSH(final int s, final int r, final int b, final int n, final long seed, final double th) {
        super(s, b);
        int rowsPerBand = r > 0 ? r : computeRowsPerBand(s, th);
        this.mh = new MinHash(rowsPerBand * s, n, seed);
    }

    /**
     * Instantiates a LSH instance that internally uses MinHash,
     * with s stages (or bands) and b buckets (per stage), for sets out of a
     * dictionary of n elements.
     *
     * Attention: the number of buckets should be chosen such that we have at
     * least 100 items per bucket.
     *
     * @param s stages
     * @param b buckets (per stage)
     * @param n dictionary size
     * @param seed random number generator seed. using the same value will
     * guarantee identical hashes across object instantiations
     * @param th the similarity threshold
     */
    public MinHashLSH(final int s, final int b, final int n, final long seed, final double th) {
        this(s, computeRowsPerBand(s, th), b, n, seed, th);
    }

    /**
     * Compute the size of the signature according to "Mining of Massive
     * Datasets" p88.
     * It can be shown that, using MinHash, the probability that the
     * signatures of 2 sets with Jaccard similarity s agree in all the
     * rows of at least one stage(band), and therefore become a candidate
     * pair, is 1−(1−s^R)^b
     * where R = signature_size / b (number of rows in a stage/band)
     * Thus, the curve that shows the probability that 2 items fall in the
     * same bucket for at least one of the stages, as a function of their
     * Jaccard index similarity, has a S shape.
     * The threshold (the value of similarity at which the probability of
     * becoming a candidate is 1/2) is a function of the number of stages
     * (s, or bands b in the book) and the signature size:
     * threshold ≃ (1/s)^(1/R)
     * Hence the signature size can be computed as:
     * R = ln(1/s) / ln(threshold)
     */
    private static int computeRowsPerBand(final int s, final double th) {
        return(int) Math.floor(Math.log(1.0 / s) / Math.log(th));
    }

    /**
     * Bin this vector to corresponding buckets.
     * @param vector
     * @return
     */
    public final int[] hash(final boolean[] vector) {
        return hashSignature(this.mh.signature(vector));
    }

    /**
     * Bin this vector to corresponding buckets.
     * @return
     */
    public final int[] hash(final Set<Integer> set) {
        return hashSignature(this.mh.signature(set));
    }

    /**
     * Get the coefficients used by internal hashing functions.
     * @return
     */
    public final long[][] getCoefficients() {
        return mh.getCoefficients();
    }
}

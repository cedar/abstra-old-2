/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;

import java.io.Serializable;

import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.sql.StorageModels;

public final class NumericIDFactory implements Factory, Serializable {

	/* generated */
	private static final long serialVersionUID = 3581660854243821992L;

	@Override
	public StorageModels storageModel() {
		return StorageModels.COMPACT;
	}

	//@Override
	//public NodeID parseNodeID(String globalId) {
	//	return new NumericNodeID((new Integer(globalId)).intValue());
	//}
	
	@Override
	public NodeID parseNodeID(int idValue) {
		return new NumericNodeID(idValue);
	}
	
	@Override
	public EdgeID parseEdgeID(int globalId) {
		try {
			return new NumericEdgeID(new Integer(globalId));
		} catch (NumberFormatException e) {
			throw new IllegalStateException(e);
		}
	}
}
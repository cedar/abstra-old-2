package fr.inria.cedar.connectionlens.extraction;

import java.util.Collection;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class BatchCacheExtractor extends BatchExtractor {
	CacheExtractor cacheExtractor; 
	
	private Logger log = Logger.getLogger(BatchCacheExtractor.class); 
	
	public BatchCacheExtractor(EntityExtractor cacheExtractor, StatisticsCollector stats, int batchSize) {
		super(cacheExtractor, stats, batchSize);
		if (!(cacheExtractor instanceof CacheExtractor)) {
			throw new IllegalStateException("This should only be created around a CacheExtractor"); 
		}
		else {
			this.cacheExtractor = (CacheExtractor)cacheExtractor; 
		}
	}
	@Override
	public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
		Collection<Entity> cached = cacheExtractor.extractionCacheLookup(ds, input, structuralContext);
		if (cached != null) {
			return cached; 
		}
		else {
			return extractAndCache(ds, null, input, structuralContext); 
		}
	}

	/** Actual extraction (no batch, just 1 input) */
	private	Collection<Entity> extractAndCache(DataSource ds, Node n, String input, int structuralContext) {
		//log.info(input + " | actual extract and cache");
		Collection<Entity> extractedEntities = cacheExtractor.run(ds, input, structuralContext);
		// the above call also caches as dictated by the cacheExtractor
		return extractedEntities; 
	}
	@Override
	/** This takes as input 1 string for 1 node in 1 context, as well as a collection of entities to add to,
	 *  and 
	 *  - if it can find it from the cache, it returns from the cache;
	 *  - otherwise, it adds this to the batch
	 *  - and if the batch is full, it launches the extraction of the whole batch accumulated so far. 
	 */
	public int batchAndPossiblyFlush(Collection<Entity> entitiesToAddTo, DataSource ds, Node n, String input, int structuralContext) {
		//log.info("Cache look up or batch: " + input + " in context " + structuralContext);
		// first use the wrapped, cache extractor to see if there are cached results: 
		Collection<Entity> results = cacheExtractor.extractionCacheLookup(ds, input, structuralContext); 
		if (results != null) {
			entitiesToAddTo.addAll(results); 
			//log.info("Got from cache " + results.size() + " results"); 
			if (results.size() == 0) {
				return CACHED_NONE; 
			}
			else {
				return CACHED_RESULTS; 
			} 
		}
		// if no cached results, just batch
		if (!extractorBatch.isFull()) {
			extractorBatch.addEntry(n, input);
			if (extractorBatch.isFull()) {
				apns.processAndFlushExtractorBatch(extractorBatch); // this must also reset it 
			}
			return FLUSHED; 
		}
		else {
			apns.processAndFlushExtractorBatch(extractorBatch);
			extractorBatch.addEntry(n, input);
			return BATCHED; 
		}			
	}
	@Override
	public void flushMemoryCache() {
		cacheExtractor.flushMemoryCache();
	}
	
}

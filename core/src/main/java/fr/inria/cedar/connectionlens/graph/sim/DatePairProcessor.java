/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph.sim;

import static fr.inria.cedar.connectionlens.graph.sim.Selectors.isDateTime;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.sameLabel;
import static fr.inria.cedar.connectionlens.graph.sim.Selectors.Axis.BOTH;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class DatePairProcessor extends EqualityProcessor {
	public static final DateFormat TWITTER = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy",Locale.ENGLISH);
	public static final DateFormat LONGEST = new SimpleDateFormat("EEEEE MMMMM yyyy HH:mm:ss.SSSZ");
	public static final DateFormat VERY_LONG = new SimpleDateFormat("EEE MMM yyyy HH:mm:ss.SSSZ");
	public static final DateFormat LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
	public static final DateFormat QUITE_LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static final DateFormat MEDIUM = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final DateFormat SHORT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static final DateFormat SHORTEST = new SimpleDateFormat("yyyy-MM-dd");
	
	public DatePairProcessor(StatisticsCollector stats, double th) {
		super(stats, th);
	}
    // no apply() method, use the one of the parent class.  

	@Override
	public int[] makeSignature(String str) {
		return new int[] {str.length(), SimilarPairProcessor.coerceType(str)};
	}

	@Override
	public NodePairSelector selector() {
		return super.selector().and(isDateTime(BOTH)).and(sameLabel()); 
	}
}

package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import java.util.ArrayList;

public class PathAggregatorMax implements PathAggregator {
    @Override
    public double aggregate(ArrayList<Path> pathsToAggregate, boolean workingGraph) {
        double maxPathTransferFactor = 0.0d;
        for(Path p : pathsToAggregate) {
            if(p.getPathTransferFactor() > maxPathTransferFactor) {
                maxPathTransferFactor = p.getPathTransferFactor();
            }
        }
        return maxPathTransferFactor;
    }
}

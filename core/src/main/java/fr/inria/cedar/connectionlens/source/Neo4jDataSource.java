/*
 * Copyright(C) 2021 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 */

package fr.inria.cedar.connectionlens.source;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.Factory;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Implementation of a Neo4j DataSource.
 * 
 * @author Madhulika Mohanty
 *
 */
public class Neo4jDataSource extends DataSource implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Map of nodes already encountered. It is maintained to prevent creation of redundant nodes having the same Neo4j ID.
	 */
	private static Map<Long,Node> nodes = new HashMap<>();

	public Neo4jDataSource(Factory f, int identifier, URI uri, URI origURI, EntityExtractor extractor,
			StatisticsCollector graphStats, StatisticsCollector extractStats, Graph graph) {
		super(f, identifier, uri, origURI, Types.NEO4J, graphStats, extractStats, graph);
		log = Logger.getLogger(Neo4jDataSource.class);
	}


	@Override
	public String getContext(Graph g, Node n) {
		return "<i>Content of the Neo4j node:</i> <b>" + n.getLabel() + "</b>";
	}


	@Override
	public void traverseEdges(Consumer<Edge> processor) {
		processEdgesToLocalAndOriginalURIs(processor);
		//log.info("Concatenated URI:"+localURI.toString());

		/**
		 * Each {@link Neo4jDataSource} is a collection of all the Neo4j nodes and edges files.
		 * The {@link URI} of this datasource is a concatenation of all the files, delimited by ':'.
		 */
		String[] files = localURI.toString().split(":");
		for(String file : files) {
			try {
				CSVReader csvreader = new CSVReader(new FileReader(file));
				String[] lineInFile;
				boolean isNeo4jNodesFile = file.endsWith(".njn");
				String[] headings = csvreader.readNext();
				
				/*System.out.println("Headings:"+headings.length);
				for(int j=0;j<headings.length;j++)
					System.out.println("Headings:"+j+":"+headings[j]);*/
				while ((lineInFile = csvreader.readNext()) != null) {
					//if (lineInFile.length!=headings.length)
					//	continue;
					if(isNeo4jNodesFile){
						/**
						 * If we are processing a nodes files, we create a main node for the entity with its ID,
						 * and create property nodes for its property. The main entity node and its properties are
						 * connected by a labeled edge.
						 */
						Node node = buildLabelNodeOfType(lineInFile[0], Node.Types.NEO4J_ENTITY); 
						if(!nodes.containsKey(Long.parseLong(lineInFile[0])))
							nodes.put(Long.parseLong(lineInFile[0]),node);
						else
							node = nodes.get(Long.parseLong(lineInFile[0]));
						for(int i=1; i<lineInFile.length;i++) {
							//System.out.println("LineInFile:"+lineInFile[i]);
							if (lineInFile[i]=="")
								continue;
							Node propNode = createOrFindValueNodeWithAtomicity(lineInFile[i], "", this, Node.Types.NEO4J_VALUE);
							Edge edge = buildLabelledEdge(node, propNode, headings[i]);
							edge.setDataSource(this);
							// log.info("Node property edge:"+edge);
							processor.accept(edge);
						}
					}
					else {
						/**
						 * If we are processing an edge file, we create an edge node representing the edge
						 * and connected to the source and target nodes by the label on the edge.
						 * The edges in a Neo4j datasource also have properties and these are represented
						 * by property nodes connected to the edge node.
						 */
						Node source = buildLabelNodeOfType(lineInFile[0], Node.Types.NEO4J_ENTITY); 
						if(!nodes.containsKey(Long.parseLong(lineInFile[0])))
							nodes.put(Long.parseLong(lineInFile[0]),source);
						else
							source = nodes.get(Long.parseLong(lineInFile[0]));
						
						Node target = buildLabelNodeOfType(lineInFile[2], Node.Types.NEO4J_ENTITY); 
						if(!nodes.containsKey(Long.parseLong(lineInFile[2])))
							nodes.put(Long.parseLong(lineInFile[2]),target);
						else
							target = nodes.get(Long.parseLong(lineInFile[2]));
						
						Node edgeNode = buildLabelNodeOfType(lineInFile[1], Node.Types.NEO4J_EDGE);
						Edge edgeToSource = buildUnlabelledEdge(source, edgeNode);
						Edge edgeToTarget = buildUnlabelledEdge(edgeNode, target);
						edgeToSource.setDataSource(this);
						edgeToTarget.setDataSource(this);
						// log.info("Edge Source:"+edgeToSource);
						// log.info("Edge Target:"+edgeToTarget);
						processor.accept(edgeToSource);
						processor.accept(edgeToTarget);
						for(int i=3; i<lineInFile.length;i++) {
							if (lineInFile[i]=="")
								continue;
							Node propNode = createOrFindValueNodeWithAtomicity(lineInFile[i], "", this, Node.Types.NEO4J_VALUE);
							Edge edge = buildLabelledEdge(edgeNode, propNode, headings[i]);
							edge.setDataSource(this);
							// log.info("Edge properties:"+edge);
							processor.accept(edge);
						}

					}
				}			
				csvreader.close();
			} catch (FileNotFoundException e) {
				log.info("Error while loading " + this.originalURI);
				e.printStackTrace();
			} catch (MalformedURLException e) {
				log.info("Error while loading " + this.originalURI);
				e.printStackTrace();
			} catch (IOException e) {
				log.info("Error while loading " + this.originalURI);
				e.printStackTrace();
			} catch (CsvValidationException e) {
				log.info("Error while loading " + this.originalURI);
				e.printStackTrace();
			}
		}
	}


	@Override
	public void postprocess(Graph graph) {
		// No-OP.
	}



}

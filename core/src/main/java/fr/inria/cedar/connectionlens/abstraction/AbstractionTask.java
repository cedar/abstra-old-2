package fr.inria.cedar.connectionlens.abstraction;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.TimeOrderedStatisticsLogger;

import java.util.Locale;

import static fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds.ABSTRACTION_WORK;

public class AbstractionTask {


    protected RelationalGraph graph;
    protected ConnectionLens cl;
    protected StatisticsCollector localCollector;
    protected TimeOrderedStatisticsLogger statisticsLogger;
    protected DataSource ds;
    protected static String datasetPath;
    protected static String datasetName;
    protected static String methodsNames;
    protected boolean isRdf;
    protected boolean isXML;
    protected boolean isJson;

    protected final Locale locale = new Locale(Config.getInstance().getStringProperty("default_locale").equals("fr") ? "FRENCH" : "ENGLISH");

    public AbstractionTask() {
        super();
    }

    public AbstractionTask(ConnectionLens cl, DataSource ds) {
        super();
        this.cl = cl;
        this.graph = (RelationalGraph) this.cl.graph();
        this.localCollector = new StatisticsCollector();
        this.statisticsLogger = new TimeOrderedStatisticsLogger();
        this.statisticsLogger.putStatisticsCollector(ABSTRACTION_WORK, this.localCollector);
        this.ds = ds;
        this.isRdf = this.ds.getLocalURI().toString().endsWith(".nt");
        this.isXML = this.ds.getLocalURI().toString().endsWith(".xml");
        this.isJson = this.ds.getLocalURI().toString().endsWith(".json");
        datasetPath = ds.getDatasetPath();
        datasetName = ds.getSanitizedName();
        String updateMethod = (Experiment.scoringMethod.startsWith("DESC_") || Experiment.scoringMethod.startsWith("LEAF_K")) ? "BOOLEAN" : "EXACT";
        methodsNames = Experiment.scoringMethod + "_" + Experiment.boundaryMethod + "_" + updateMethod + "_" + Config.getInstance().getStringProperty("use_idrefs_xml");

    }

    public StatisticsCollector getLocalCollector() {
        return this.localCollector;
    }

    public RelationalGraph getGraph() {
        return this.graph;
    }

    public static String getDatasetName() {
        return datasetName;
    }

    public static String getDatasetPath() {
        return datasetPath;
    }

    public static String getMethodsNames() {
        return methodsNames;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public TimeOrderedStatisticsLogger getStatisticsLogger() {
        return this.statisticsLogger;
    }

    public ConnectionLens getCl() {
        return this.cl;
    }

    public boolean isRdf() {
        return this.isRdf;
    }

    public DataSource getDs() {
        return this.ds;
    }

    public void reportStatistics() {
        if(this.localCollector != null) {
            this.localCollector.stopAll();
            this.localCollector.reportAll();
        }
    }
}

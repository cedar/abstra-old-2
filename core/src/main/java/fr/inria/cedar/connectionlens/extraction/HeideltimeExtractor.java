package fr.inria.cedar.connectionlens.extraction;

import de.unihd.dbs.heideltime.standalone.DocumentType;
import de.unihd.dbs.heideltime.standalone.HeidelTimeStandalone;
import de.unihd.dbs.heideltime.standalone.OutputType;
import de.unihd.dbs.heideltime.standalone.POSTagger;
import de.unihd.dbs.heideltime.standalone.components.JCasFactory;
import de.unihd.dbs.heideltime.standalone.components.impl.JCasFactoryImpl;
import de.unihd.dbs.heideltime.standalone.exceptions.DocumentCreationTimeMissingException;
import de.unihd.dbs.uima.annotator.heideltime.resources.Language;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.source.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HeideltimeExtractor extends TopLevelExtractor {
    Logger log = Logger.getLogger(HeideltimeExtractor.class);

    public HeideltimeExtractor() {
        // super(this.getClass(), null, Config.getInstance().getIntProperty("extractor_batch_size_cl", 1));
        super();
    }

    public HeideltimeExtractor(Locale locale) {
        super(locale);
    }

    /**
     * Extract dates from nodes.
     * @param ds the node's datasource
     * @param input the content of the node
     * @param structuralContext the context of the node (not used here).
     * @return the collection of extracted entities (i.e. a collection of emails, hashtags, URIs, mentions and dates).
     */
    @Override
    public Collection<Entity> run(DataSource ds, String input, int structuralContext) {
        List<String> extractedDates = this.findDates(input);

        HashSet<Entity> extractedNodes = new HashSet<>();
        if(!extractedDates.isEmpty()) {
            for(String extractedDate : extractedDates) {
                Entity entity = new Entity(extractedDate, extractedDate, 1.0, 0, Types.DATE, ds.getLocalURI().toString());
                extractedNodes.add(entity);
            }
        }

        return extractedNodes;

        //////////////////////
        // BATCH MODE
        //////////////////////
        // ArrayList<ArrayList<String>> extractedDates = this.findDatesWithBatch(input);
        // HashSet<Entity> extractedNodes = new HashSet<>();
        // if(!extractedDates.isEmpty()) {
        //     for (ArrayList<String> oneResultInBatch : extractedDates) {
        //         for(String extractedDate : oneResultInBatch) {
        //             Entity entity = new Entity(extractedDate, extractedDate, 1.0, 0, Types.DATE, ds.getLocalURI().toString());
        //             extractedNodes.add(entity);
        //         }
        //     }
        // }
        // return extractedNodes;
    }

    /**
     * Find dates, using HeidelTime framework, in the content of the node. Even if HeidelTime look for many types of dates (dates, durations, references to time), we only extract dates.
     * @param text the content of the node
     * @return a list of strings which are the dates found in the content of the node.
     */
    public List<String> findDates(String text) {
        try {
            // V0 - redirect output of Heideltime into a temp file
//            PrintStream console = System.out;
//            System.setOut(new PrintStream("/var/connectionlens/heideltime-output.txt"));

            // V1 - does not work
//            Logger.getRootLogger().removeAllAppenders();
//            Logger.getRootLogger().addAppender(new NullAppender());

            ArrayList<String> results = new ArrayList<>();
            HeidelTimeStandalone heidelTime = new HeidelTimeStandalone(Language.getLanguageFromString(this.getLocale().getDisplayLanguage()),
                    DocumentType.NEWS,
                    OutputType.TIMEML,
                    Config.getInstance().getStringProperty("config_heideltime"),
                    POSTagger.TREETAGGER, true);
            // V2 - does not work
//            Logger loggerHeideltime = Logger.getLogger("HeidelTimeStandalone");
//            Logger loggerHeideltime = Logger.getLogger(de.unihd.dbs.heideltime.standalone.HeidelTimeStandalone.class);
//            loggerHeideltime.setLevel(Level.OFF);

            // NB 06/04/2021 - new Date() should be date of creation of the document
            String result = heidelTime.process(text, new Date());

            TypeSystemDescription[] descriptions = new TypeSystemDescription[] {UIMAFramework.getXMLParser().parseTypeSystemDescription(new XMLInputSource(this.getClass().getClassLoader().getResource(de.unihd.dbs.heideltime.standalone.Config.get(de.unihd.dbs.heideltime.standalone.Config.TYPESYSTEMHOME))))};
            JCasFactory jfactory = new JCasFactoryImpl(descriptions);
            JCas cas = jfactory.createJCas();
            Logger loggerJCas = Logger.getLogger(org.apache.uima.jcas.JCas.class);
            loggerJCas.setLevel(Level.OFF);
            cas.setDocumentText(result);
            String regex = "<(\\w+)[^>]* value=\"[\\d-]*\">[^<>]*<\\/(\\w+)>"; // look for tags TIMEX3
            Matcher m = Pattern.compile(regex).matcher(result);
            while(m.find()) {
                String tag = m.group();
                String dateString = tag.substring(tag.indexOf(">")+1, tag.indexOf("<", tag.indexOf("<") + 1)); // get the content between the tags
                if(dateString.length() > 2) { // do not add the date (e.g. 79) if there is only two or three numbers because often this is not a date (percentage, cash amount, ...)
                    results.add(dateString);
                }
            }
            // V0 - does not work
//            System.setOut(console);
            return results;
        } catch (DocumentCreationTimeMissingException | ResourceInitializationException | IOException | InvalidXMLException | CASException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

//    public ArrayList<ArrayList<String>> findDatesWithBatch(String text) throws Exception {
//        try {
//            ArrayList<ArrayList<String>> finalResults = new ArrayList<>();
//            HeidelTimeStandalone heidelTime = new HeidelTimeStandalone(Language.getLanguageFromString(this.getLocale().getDisplayLanguage()),
//                    DocumentType.NEWS,
//                    OutputType.TIMEML,
//                    Config.getInstance().getStringProperty("config_heideltime"),
//                    POSTagger.TREETAGGER, true);
//            // NB 06/04/2021 - new Date() should be date of creation of the document
//
//            ArrayList<String> listOfInputs = new ArrayList<>();
//            listOfInputs.add(text);
//
//            ArrayList<String> results = heidelTime.processWithBatch(listOfInputs, new Date());
//            System.out.println(results);
//
//            for (int i = 0; i < results.size(); i++) {
//                String result = results.get(i);
//                finalResults.add(i, new ArrayList<>());
//                TypeSystemDescription[] descriptions = new TypeSystemDescription[]{UIMAFramework.getXMLParser().parseTypeSystemDescription(new XMLInputSource(this.getClass().getClassLoader().getResource(de.unihd.dbs.heideltime.standalone.Config.get(de.unihd.dbs.heideltime.standalone.Config.TYPESYSTEMHOME))))};
//                JCasFactory jfactory = new JCasFactoryImpl(descriptions);
//                JCas cas = jfactory.createJCas();
//                Logger loggerJCas = Logger.getLogger(org.apache.uima.jcas.JCas.class);
//                loggerJCas.setLevel(Level.OFF);
//                cas.setDocumentText(result);
//                String regex = "<(\\w+)[^>]* value=\"[\\d-]*\">[^<>]*<\\/(\\w+)>"; // look for tags TIMEX3
//                Matcher m = Pattern.compile(regex).matcher(result);
//
//                while (m.find()) {
//                    String tag = m.group();
//                    String dateString = tag.substring(tag.indexOf(">") + 1, tag.indexOf("<", tag.indexOf("<") + 1)); // get the content between the tags
//                    if (dateString.length() > 2) { // do not add the date (e.g. 79) if there is only two or three numbers because often this is not a date (percentage, cash amount, ...)
//                        finalResults.get(i).add(dateString);
//                    }
//                }
//            }
//            return finalResults;
//        } catch (Exception e) {
//            throw new Exception(e);
//        }
//    }

    /**
     * Apply the regex on the content of the node
     * @param regex the regex to apply on the text
     * @param textToAnalyze the text on which the regex will be applied (i.e. the content of the node)
     * @return a list containing the matches of the regex.
     */
    public List<String> getMatches(String regex, String textToAnalyze) {
        List<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(regex).matcher(textToAnalyze);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

    /**
     * Get the name of the extractor.
     * @return the string containing he name of the extractor.
     */
    @Override
    public String getExtractorName() { return "HeideltimeExtractor"; }

    /**
     * Get the locale.
     * @return the locale.
     */
    @Override
    public Locale getLocale() { return locale; }
}

package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.score;

import java.util.Objects;

public class Vertex implements Comparable<Object> {
    private int id;
    private String label;

    Vertex(int id) {
        this(id, "");
    }

    Vertex(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return this.id;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return "V" + this.id + "(\"" + this.label + "\")";
    }

    // need to define equals so that we can find if a vertex exists in the graph (and not compare objecta addresses)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Vertex that = (Vertex) o;
        return this.id == that.id;
    }

    @Override
    public int compareTo(Object o) {
        return Integer.compare(this.id, ((Vertex) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;
import static fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds.*;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.*;
import static java.util.Locale.ENGLISH;
import static java.util.Locale.FRENCH;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.inria.cedar.connectionlens.abstraction.*;
import fr.inria.cedar.connectionlens.extraction.*;
import fr.inria.cedar.connectionlens.util.*;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterDescription;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.IParameterSplitter;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.graph.InMemoryGraph;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.search.BFSMSearch;
import fr.inria.cedar.connectionlens.search.BFSSearch;
import fr.inria.cedar.connectionlens.search.ConditionReachedException;
import fr.inria.cedar.connectionlens.search.EvaluationException;
import fr.inria.cedar.connectionlens.search.GAMSearch;
import fr.inria.cedar.connectionlens.search.GAMSearchExhaustive;
import fr.inria.cedar.connectionlens.search.Query;
import fr.inria.cedar.connectionlens.search.QuerySearch;
import fr.inria.cedar.connectionlens.search.QuerySearch.GlobalSearchAlgorithms;
import fr.inria.cedar.connectionlens.search.SQLSearch;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.Kinds;
import fr.inria.cedar.connectionlens.util.StatisticsLogger.OutputFormats;

/**
 * Runs the experiments.
 */
public class Experiment {

	/**
	 * Interpreter logger.
	 */
	public static Logger log = Logger.getLogger(Experiment.class);

	/**
	 * Program execution command (to appear in usage message).
	 */
	public static final String PROGRAM_NAME = "java -jar connection-lens-full-<version>.jar";

	/**
	 * Program execution command (to appear in usage message).
	 */
	private static final String COMMENT_PREFIX = "#";

	/**
	 * Supported Extractors.
	 */
	public enum Extractors {
		NONE, SNER, FLAIR_NER
	}

	/**
	 * Supported taggers.
	 */
	public enum Taggers {
		TREE_TAGGER, CORE_NLP
	}

	/**
	 * Statistic collectors. They are created here and transmitted to the
	 * ConnectionLens instance.
	 */
	private StatisticsCollector graphStats;
	private StatisticsCollector extractStats;
	private StatisticsCollector indexStats;
	private StatisticsCollector simStats;
	private StatisticsCollector queryStats;
	private StatisticsCollector abstractionWorkStats;

	/**
	 * All supported locales.
	 */
	public static Set<Locale> supportedLocales = ImmutableSet.of(FRENCH, ENGLISH);

	@Parameter(names = { "-v", "--verbose" }, description = "Use verbose mode.")
	private boolean verbose = false; // IM, 27/01/21: was: true (and -v didn't seem to matter)

	protected boolean print = false; 

	@Parameter(names = { "-json",
			"--export CL Graph to json" }, description = "Use to export the CL graph in a json file.")
	protected boolean json = false;

	@Parameter(names = { "-h", "--help" }, help = true, description = "Displays this help message.")
	private boolean help;

	@Parameter(names = { "-c",
			"--config" }, converter = ConfigConverter.class, description = "Path to configuration file. "
					+ "The file will be used to set all default values. "
					+ "If the option is not set, default parameter files will searched "
					+ "in the current directory. If no such file is found, build-in " + "default will be used.")
	private Config config;

	@DynamicParameter(names = "-D", required = false, description = "Force the given parameters, bypassing defaults parameters and "
			+ "those found in parameter files, ignoring those that may be " + "specified in each parameter file."
			+ "For instance, '-Dtimeout=10000' force a timeout 10s seconds " + "on a phase.")
	// IM, 24/1/21: this is never filled in explicitly but parameter parsing is broken without it.
	private Map<String, String> params = new HashMap<>();

	/**
	 * use this parameter if you want to defer the creation of some indexes to future loading runs.
	 */
	@Parameter(names = { "-lateIdx",
			"--index-later" }, description = "Is true, create necessary indexes at the begining of the loading and delay the creation of some tables and index to the last loading.")
	private boolean late = false;

	/**
	 * use this parameter if you want to specify such loading as the last load.
	 */
	@Parameter(names = { "-last",
			"--last" }, description = "Is true, this will be the last loading after multiple loadings.")
	private boolean lastLoad = false;

	@Parameter(names = { "-a",
			"--interactive-mode" }, description = "If true, read incoming query from STDIN after the registration phase, until EOF is reached.")
	private boolean interactive = false;

	@Parameter(names = { "-n", "--noreset-at-start" }, description = "Do NOT reset the data structures upon starting.")
	public static boolean noResetFirst = false;

	@Parameter(names = {
			"--force-similarity-computation" }, description = "For the similarity computation to be run, even if no know datasource was registered.")
	private boolean forceSimilarity = false;


	@Parameter(names = { "-i",
			"--input" }, listConverter = URIOrFileHierachyConverter.class, description = "Comma-separated list of file or directory paths. "
					+ "If a directory is specified, all descendant files are used as inputs.")
	private List<URI> inputs = new ArrayList<>();

	@Parameter(names = { "-ou",
			"--orignal-uri" }, listConverter = URIListConverter.class, description = "Comma-separated list of original uris. ")
	// orignal-uri is a typo, so it should be fixed. Yet it doesn't appear to be used anywhere else. TODO clean this up
	private List<URI> originalUris = new ArrayList<>();

	@Parameter(names = { "-o",
			"--output" }, converter = OutputStreamConverter.class, description = "Path to output file. Default: STDOUT")
	private PrintStream output = System.out;

	@Parameter(names = { "-q", "--query" }, description = "A (single) keyword query to execute.")
	private String query;

	@Parameter(names = { "-Q",
			"--query-file" }, validateWith = ExistingFileValidator.class, description = "A file containing one input query per line.")
	private Path queryFile;

	@Parameter(names = { "-f", "--output-format" }, description = "The format in which to output the statistics.")
	private OutputFormats outputFormat = OutputFormats.DEFAULT;

	@Parameter(names = { "-sm", "--scoring-method" }, description = "The scoring method used for the abstraction.")
	public static String scoringMethod = "wPR";

	@Parameter(names = { "-bm", "--boundary-method" }, description = "The boundary method used for the abstraction.")
	public static String boundaryMethod = "BOUND_FL_AC";

	// the graph update method is not parameter because each pair (sm, bm) has its corresponding graph update method

	@Parameter(names = { "-abs", "--abstraction" },description = "Set to true to register the dataset and do its abstraction. False to only register the dataset.")
	public static boolean runAbstraction = true;

	/**
	 * The expected input language.
	 */
	private Locale locale = FRENCH;


	private EntityExtractor extractor;

	@Parameter(names = { "-rs",
			"--collect-registration-stats" }, description = "If set, logs registration-related statistics")
	private boolean collectRegistrationStats = false;

	@Parameter(names = { "-ss",
			"--collect-similarity-stats" }, description = "If set, logs similarity-related statistics")
	private boolean collectSimilarityStats = false;

	@Parameter(names = { "-qs", "--collect-query-stats" }, description = "If set, logs query-related statistics")
	private boolean collectQueryStats = false;
	
	@Parameter(names = { "-as", "--collect-abstraction-stats" }, description = "If set, logs abstraction-related statistics")
	private boolean collectAbstractionStats = false;

	long start = 0; 
	/**
	 * Sets up an engine for execution.
	 *
	 * @param args the command line parameters as given by the main method.
	 */
	public Experiment(String... args) {
		// parse command-line and run connectionLens (registration + abstraction + queryAnswering)
		final Properties cmdLine = this.parseCommandLine(args);
		try (StatisticsLogger logger = new StatisticsLogger(this.output, cmdLine, this.outputFormat)) {
			this.start = System.currentTimeMillis();
			this.graphStats = makeCollector(GRAPH, logger, this.collectRegistrationStats);
			this.extractStats = makeCollector(EXTRACTION, logger, this.collectRegistrationStats);
			this.indexStats = makeCollector(INDEX, logger, this.collectRegistrationStats);
			this.simStats = makeCollector(SIMILARITY, logger, this.collectSimilarityStats);
			this.queryStats = makeCollector(QUERY, logger, this.collectQueryStats);
			this.abstractionWorkStats = makeCollector(ABSTRACTION_WORK, logger, this.collectAbstractionStats);

			// first initialization of the extractor: if nothing to extract, force TopLevel, to go fast
			this.extractor = (this.inputs.isEmpty())? (new TopLevelExtractor()): buildExtractor(this.extractStats);

			final EntityExtractor wrappedExtractor = possiblyWrapExtractor(this.extractor, this.extractStats);
					
			final SimilarPairProcessor pairFinder = SimilarPairProcessor.buildPairFinder(this.config, this.simStats);

			try (ConnectionLens cl = new ConnectionLens(logger, wrappedExtractor, new TreeTagger(this.locale), pairFinder, !this.noResetFirst,
					this.graphStats, this.extractStats, this.indexStats, this.simStats, this.queryStats, this.abstractionWorkStats, this.late)) {

				// register the data into CL
				this.abstractionWorkStats.start(StatisticsCollector.total());
				long startTime = System.currentTimeMillis();
				// for abstraction only, we ensure that we have the policies we want regarding the data format
				if (Experiment.runAbstraction) {
					if(!this.inputs.isEmpty()) {
						if (this.inputs.get(0).getPath().endsWith(".nt")) {
							this.config.setProperty("value_atomicity", "PER_GRAPH");
						} else {
							this.config.setProperty("value_atomicity", "PER_INSTANCE");
						}
					}
				}

				if(!Experiment.runAbstraction || !this.config.getBooleanProperty("start_from_collection_graph")) {
					if(!this.noResetFirst) {
						this.registerDatasources(cl);
						this.abstractionWorkStats.tick(StatisticsCollector.total(), REGISTRATION_T);
						this.abstractionWorkStats.pause(StatisticsCollector.total());
						log.info("registering datasources in " + (System.currentTimeMillis() - startTime) + " ms");
					}
					// maybe normalize the graph?
					if(Experiment.runAbstraction) this.config.setProperty("perform_normalization", "false");
					if(Config.getInstance().getBooleanProperty("perform_normalization")) {
						DataSource ds = cl.getDataSourcesAsList().get(0);
						this.config.setProperty("create_abstraction", "false"); // NB (July 22nd 2021) to be sure that we don't normalize the graph a second time
						Normalization normalization = new Normalization(cl, ds);
						normalization.run();
					}
				}

				// maybe perform the abstraction pipeline?
				if (Experiment.runAbstraction) {
					cl.runAbstraction(scoringMethod, boundaryMethod);
				}

				// important to test extractor here, not wrappedExtractor which may be a different class
//				if (this.extractor instanceof FlairNERExtractor) {
//					PythonUtils.getInstance().stopFlairExtractor();
//				}

				// We call this in all cases. It does sth only for CacheExtractor and for BatchCacheExtractor.
				wrappedExtractor.flushMemoryCache();

				// maybe query the graph?
				this.queryAnswering(cl, cl.queryStats());
			}
		} catch (Exception e) {
			log.info("Experiment could not complete.");
			log.error(" Cause: " + nullToEmpty(e.getMessage()));
			if (this.verbose) {
				e.printStackTrace(this.output);
			} else {
				this.output.println(" (Use verbose mode for detail)");
			}
			this.graphStats = null;
			this.extractStats = null;
			this.simStats = null;
			this.queryStats = null;
			this.abstractionWorkStats = null;
		}
		log.setLevel(Level.INFO);
	}

	public static EntityExtractor possiblyWrapExtractor(EntityExtractor basicExtractor, StatisticsCollector givenExtractStats) {
		// below we default to 0 to learn if we should really cache
		int memCacheSize = Config.getInstance().getIntProperty("max_extraction_mem_cache", 0);
		int diskCacheSize = Config.getInstance().getIntProperty("max_extraction_disk_cache", 0); 
		boolean shouldCache = (((memCacheSize != 0) || (diskCacheSize != 0)) && // -1 or > 0 means cache.
				(!(basicExtractor instanceof TopLevelExtractor))); 
		// the cache does not help this run if we use PER_GRAPH. The user gets warnings.
		if (shouldCache && Config.getInstance().getProperty("value_atomicity").equals("PER_GRAPH")) {
			log.warn("You requested caching yet create unique values PER_GRAPH."); 
			if (diskCacheSize != 0) {
				log.warn("Some cache results will be written to disk, slowing down this run");
				if (basicExtractor instanceof FlairNERExtractor) {
					log.warn("Flair extraction results will be cached; they will speed up future runs."); 
				}
				if (basicExtractor instanceof StanfordNERExtractor) {
					log.warn("Disk caching SNER extraction results is generally not worth the effort.");
				}
			}
			if (memCacheSize != 0) {
				log.warn("Useless cache results will occupy some memory");
			}
			log.warn("You may disable caching by setting max_extraction_mem_cache and max_extraction_disk_cache to 0.");
		}
		
		int batchSize = Config.getInstance().getIntProperty("extractor_batch_size_cl", 1);
		boolean shouldBatch = (batchSize > 1);
		if (shouldCache) { // cache
			if (shouldBatch) { // cache, batch
				return new BatchCacheExtractor(
						new CacheExtractor(basicExtractor, givenExtractStats, memCacheSize, diskCacheSize),
						givenExtractStats, batchSize); 
			}
			else { // cache, no batch
				return new CacheExtractor(basicExtractor, givenExtractStats, memCacheSize, diskCacheSize); 
			}
			
		}
		else { // no cache
			if (shouldBatch) {
				return new BatchExtractor(basicExtractor, givenExtractStats, batchSize); 
			}
			else { // no cache, no batch
				return basicExtractor; 
			}
		}		
	}
	
	private static StatisticsCollector makeCollector(Kinds kind, StatisticsLogger logger, boolean verbose) {
		StatisticsCollector result = verbose ? new StatisticsCollector() : StatisticsCollector.mute();
		logger.putStatisticsCollector(kind, result);
		return result;
	}
	

	/**
	 * Registration test.
	 *
	 * @param cl the ConnectionLens instance
	 */
	private void registerDatasources(ConnectionLens cl) throws Exception {
		if (this.inputs.isEmpty()) {
			log.info("No inputs to register.");
			return;
		}
		long tRegStart = System.currentTimeMillis(); 
		log.info("Registration of inputs starts"); //: " + (tRegStart-start));
//		log.info(ValueAtomicities.valueOf(Config.getInstance().getProperty("value_atomicity")) +
//				" value creation, " +
//				EntityNodeCreationPolicy.valueOf(Config.getInstance().getProperty("entity_node_creation_policy")) +
//				" entity creation");
		
		List<URI> sources = Lists.newArrayList();
		List<URI> copyInput = Lists.newLinkedList();
		HashMap<URI, URI> originalURIMap = new HashMap<>();
		copyInput.addAll(this.inputs);

		this.convertDataSourcesBeforeLoading(copyInput, originalURIMap, cl);
		
		// Concatenate neo4j datasources into one datasource.
		copyInput.addAll(this.inputs);
		List<String> neo4jURIs = Lists.newArrayList();
		for (URI uri : copyInput) {	
			log.info("URI: " + uri); 
			if (!(uri.toString().startsWith("jdbc:"))) {// JDBC URIs don't convert to URL				
				if (uri.toURL().getPath().endsWith("njn") || uri.toURL().getPath().endsWith("nje")) {
					//log.info(uri.getPath());
					neo4jURIs.add(uri.getPath());
				}
			}
		}
		this.inputs.removeIf(s -> s.toString().endsWith("njn"));
		this.inputs.removeIf(s -> s.toString().endsWith("nje"));
		if(!neo4jURIs.isEmpty()) {
			String concatenatedFilenames = Joiner.on(":").join(neo4jURIs);
			URI concatenatedURI = new URI(concatenatedFilenames);
			//log.info(concatenatedURI);
			this.inputs.add(concatenatedURI);
		}
		copyInput.clear();


		this.inputs.forEach(s -> {
			sources.add(s);
		});

		cl.getOriginalURIMap(sources, this.originalUris).forEach(originalURIMap::putIfAbsent);

		if (sources.isEmpty()) {
			// log.info("No sources to register.");
			return;
		}
		List<DataSource> newSources = Lists.newLinkedList();
//		log.info("Going into source registration");
		cl.register(sources, originalURIMap).forEach(newSources::add);

//		log.info("Registration finished except possible index commit: " + (System.currentTimeMillis()-start) + " ms");
		
		if ((!this.late) || (this.late && this.lastLoad)) {
//			log.info("Committing index: " + (System.currentTimeMillis()-start));
			cl.graph().index().commit();
		}
		if (newSources.isEmpty() && this.forceSimilarity) {
			newSources.addAll(cl.catalog().getDataSources());
		}
		cl.extractStats().stopAll();

		if (Config.getInstance().getBooleanProperty("entity_disambiguation")) {
			log.info("Disambiguation index: " + (System.currentTimeMillis()- this.start));
			if ((!this.late) || (this.late && this.lastLoad)) {
				cl.graph().createDisambiguatedIndex();
				cl.graph().computeSimilarDisambiguatedEntities(cl.graph().openSession()::addEdge);
			}
		}
		
		if (Config.getInstance().getBooleanProperty("compare_nodes")) {
			log.info("Comparisons: " + (System.currentTimeMillis()- this.start));
			// June 24, 2021: create this index even if this is not the last run, because it needs
			// to be available for every batch of comparisons
			//cl.graph().createNodeLabelPrefixIndex(); // end of June 24, 2021 change
			cl.processSimilarities(cl.catalog().getDataSources(), newSources);
			log.info("Node comparisons finished");
			cl.simStats().stopAll();
		}
		
		// Calculate the mean and the standard deviation of the edges
		if ((!this.late) || (this.late && this.lastLoad)) {
//			log.info("Specificity computations: " + (System.currentTimeMillis()-start));
			this.graphStats.resume(StatisticsCollector.total());
			cl.graph().countEdgesForSpecificity();
			//log.info("Edges counted by rep, now transforming them in spec table: " + (System.currentTimeMillis()-start));;
			cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
//			log.info("Done with spec computations: " + (System.currentTimeMillis()-start));;
			this.graphStats.tick(StatisticsCollector.total(), fr.inria.cedar.connectionlens.util.StatisticsKeys.SPEC_T);
			
		}
		
		//Long stopSim = System.currentTimeMillis()-start;
		// log.info("FINISHED SIMILARITY IN: " + (stopSim - stopRegistration));
		if (Config.getInstance().getBooleanProperty("compare_nodes")) {
			cl.graphStats().put(StatisticsCollector.total(), WSA_NO, cl.graph().getWeakSameAs(null, 0.0).size());
		}
		
		long tStop = System.currentTimeMillis(); 
//		log.info("Only reporting and possible drawing left: " + (System.currentTimeMillis()-start));
		
		cl.graphStats().stopAll(); // stopAll sets TOTAL_T
		cl.graphStats().reportAll();
		cl.extractStats().stopAll();
		cl.extractStats().reportAll();
		cl.indexStats().stopAll();
		cl.indexStats().reportAll();
		if (Config.getInstance().getBooleanProperty("compare_nodes")) {
			cl.simStats().reportAll();
		}
		
		// at this point the graph has been fully computed

		if (this.json) {
			GraphExport2JSON graphExport2JSON = new GraphExport2JSON();
			graphExport2JSON.export(cl.graph(), cl.catalog().getDataSources());
		}
		if (this.print) {
			(new GraphPrintingByRepresentative()).printAllSources(cl);
		}
//		log.info("Actual total registration time: " + (tStop -tRegStart));
	}

	/** Converts 1 PDF data source into possibly a set of data sources.
	 *  Converts Word and Excel sources into HTML sources.
	 *
	 * @param copyInput
	 * @throws URISyntaxException
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void convertDataSourcesBeforeLoading(List<URI> copyInput, HashMap<URI, URI> originalURIMap, ConnectionLens cl) throws MalformedURLException, IOException, URISyntaxException {
		for (URI uri : copyInput) {
			if (!(uri.toString().startsWith("jdbc:"))) {// JDBC URIs don't convert to URL
				if (DataSource.isPDFFileURI(uri)) {
					log.info("PDF file: " + uri.getPath());
					List<URI> uriExtracted = cl.extractPDFContent(uri);
					if (uriExtracted != null) {
						uriExtracted.forEach(uri1 -> originalURIMap.put(uri1,uri));
						this.inputs.addAll(uriExtracted);
					}
				}
				if (DataSource.isOfficeFileURI(uri)) {
					log.info("Office file: " + uri.getPath());
					List<URI> uriExtracted = cl.extractOfficeContent(uri);
					if (uriExtracted != null) {
						uriExtracted.forEach(uri1 -> originalURIMap.put(uri1,uri));
						this.inputs.addAll(uriExtracted);
					}
				}

			}
		}
		copyInput.clear();
		this.inputs.removeIf(s -> (DataSource.needsConversionBeforeLoading(s)));
	}

	/**
	 * Query answering test.
	 *
	 * @param cl the ConnectionLens instance
	 */
	private void queryAnswering(ConnectionLens cl, StatisticsCollector stats) {
		if (this.interactive || !isNullOrEmpty(this.query) || this.queryFile != null) {
			if (!isNullOrEmpty(this.query)) {
				this.runQuery(this.query, stats, cl);
			}
			if (this.queryFile != null && Files.exists(this.queryFile)) {
				this.readInputFile(cl, stats, this.queryFile);
			}
			if (this.interactive) {
				this.readStdInput(cl, stats);
			}
			stats.reportAll();
		}
	}

	/**
	 * Builds an instance of QuerySearch using external parameters.
	 *
	 * @param cl the ConnectionLens instance
	 * @param f  the scoring function
	 * @return a fresh instance of QuerySearch
	 */
	private static QuerySearch buildGlobalSearch(ConnectionLens cl, ScoringFunction f, StatisticsCollector stats) {
		GlobalSearchAlgorithms algo = GlobalSearchAlgorithms
				.valueOf(Config.getInstance().getProperty("global_search_algorithm"));
		boolean queryStrategy = Config.getInstance().getBooleanProperty("query_only_specific_edge");
		switch (algo) {
		case SQL_PATH:
			return new SQLSearch(cl.graph().index(), cl.graph(), f, stats);
		case BFS_G:
			return new BFSSearch(cl.graph().index(), 
					(Config.getInstance().getBooleanProperty("prefetch_graph_in_memory")?
						(new InMemoryGraph(cl.graph())):cl.graph()), 
					f, stats, queryStrategy);
		case BFS_M:
			return new BFSMSearch(cl.graph().index(), 
					(Config.getInstance().getBooleanProperty("prefetch_graph_in_memory")?
						(new InMemoryGraph(cl.graph())):cl.graph()), 
					f, stats, queryStrategy, false);
		case BFS_AM:
			return new BFSMSearch(cl.graph().index(), 
					(Config.getInstance().getBooleanProperty("prefetch_graph_in_memory")?
						(new InMemoryGraph(cl.graph())):cl.graph()), 
					f, stats, queryStrategy, true);
		case GAM_E:
			return new GAMSearchExhaustive(cl.graph().index(), 
					(Config.getInstance().getBooleanProperty("prefetch_graph_in_memory")?
						(new InMemoryGraph(cl.graph())):cl.graph()), 
					f, stats, queryStrategy);
		default:
			return new GAMSearch(cl.graph().index(), 
					(Config.getInstance().getBooleanProperty("prefetch_graph_in_memory")?
						(new InMemoryGraph(cl.graph())):cl.graph()), 
					f, stats, queryStrategy);
		}

	}

	/**
	 * Reads the default input stream for query to run interactively.
	 * 
	 * @param cl    the ConnectionLens instance
	 * @param stats the statistics collector
	 */
	private void readInputFile(ConnectionLens cl, StatisticsCollector stats, Path p) {
		try (FileReader fr = new FileReader(p.toFile()); Scanner scanner = new Scanner(fr)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				int commentStart = line.indexOf(COMMENT_PREFIX);
				if (commentStart >= 0) {
					line = line.substring(0, commentStart);
				}
				if (!line.trim().isEmpty()) {
					this.runQuery(line, stats, cl);
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Reads the default input stream for query to run interactively.
	 * 
	 * @param cl    the ConnectionLens instance
	 * @param stats the statistics collector
	 */
	private void readStdInput(ConnectionLens cl, StatisticsCollector stats) {
		try (Scanner scanner = new Scanner(new CloseShieldInputStream(System.in))) {
			System.out.print("\nquery> ");
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				int commentStart = line.indexOf(COMMENT_PREFIX);
				if (commentStart >= 0) {
					line = line.substring(0, commentStart);
				}
				if (!line.trim().isEmpty()) {
					this.runQuery(line, stats, cl);
				}
                System.out.print("\nquery> ");
			}
            System.out.print("\nBye.");
		}
	}

	/**
	 * Runs a query search.
	 *
	 * @param queryString the query string
	 * @param stats       the statistics collector
	 * @param cl          the ConnectionLens instance
	 */
	private void runQuery(String queryString, StatisticsCollector stats, ConnectionLens cl) {
		final Query q = Query.parse(queryString);
		final long walltime = Config.getInstance().getLongProperty("search_stopper_timeout");
		final long gracePeriod = 1000;
		final SearchPhase phase = new SearchPhase(buildGlobalSearch(cl, cl.resolveScoringFunction(this.config.getProperty("scoring_function"), q), stats), q,
				stats);
		try {
			if (walltime > 0) {
				ExecutorService executor = Executors.newSingleThreadExecutor();
				try {
					executor.invokeAll(Arrays.asList(phase), walltime + gracePeriod, MILLISECONDS);
				} catch (InterruptedException e) {
					log.warn("Search interrupted:" + q);
					stats.put(q, STOP, "thread interrupted");
				} finally {
					executor.shutdown();
				}
			} else {
				phase.call();
			}
		} catch (Exception e) {
			stats.put(q, STOP, "Exception " + e.getClass().getSimpleName() + e);
		}
		phase.collectStats();
	}

	private class SearchPhase implements Callable<Integer> {

		private final StatisticsCollector stats;
		private final Query query;
		private final QuerySearch search;

		SearchPhase(QuerySearch s, Query q, StatisticsCollector st) {
			this.search = s;
			this.query = q;
			this.stats = st;
		}

		public void collectStats() {
			this.search.collectStats();
		}

		@Override
		public Integer call() {
			this.stats.start(this.query);
			long start = System.currentTimeMillis();
			final AtomicBoolean firstFound = new AtomicBoolean(false);

			/* Was originally:
			final SortedSet<AnswerTree> resultSet = new ConcurrentSkipListSet<>(
					comparing(at -> ((AnswerTree) at).score(this.search.scoring())).reversed()
							.thenComparing(Object::toString));
			Changed by MM on 10/11/2021 to have a more efficient Comparator
			and avoid concurrency overheads due to ConcurrentSkipListSet. */
			final SortedSet<AnswerTree> resultSet = new TreeSet<AnswerTree>(new AnswerTree.Comparator(this.search.scoring()));

			try {
				this.search.run(this.query, a -> {
					resultSet.add(a); 
					if (!firstFound.get()) {
						this.stats.put(this.query, "TIME_1", System.currentTimeMillis() - start);
						firstFound.set(true);
					}
					if (Experiment.this.verbose) {
							//output.println(a.toString());
							// IM, 11/11/11: for performance, replace with:
						Experiment.this.output.println(a.toJSONString(this.search.scoring()));
							// output.println(a.shortLabel()); //IM: to compare with CLMem
					}
					this.stats.put(this.query, "T_LAST", System.currentTimeMillis() - start);
				});
			} catch (ConditionReachedException e) {
				this.stats.put(this.query, STOP, e.getMessage());
				if (Experiment.this.verbose || Experiment.this.interactive) {
					Experiment.this.output.println("Stop condition reached.");
				}
			} catch (EvaluationException e) {
				this.stats.put(this.query, STOP, e.getClass().getSimpleName());
				if (Experiment.this.verbose || Experiment.this.interactive) {
					Experiment.this.output.println("Problem occurred in the evaluation. Check for consistency issues in search.");
					Experiment.this.output.println(e.getMessage());
				}
			} 
			catch(Exception e) {
				e.printStackTrace();
			}
			finally {
				Experiment.this.output.println(this.query + " " + resultSet.size() + " results.");
				this.stats.put(this.query, ANSW_NO, resultSet.size());
			}
			return resultSet.size();
		}
	}

	/**
	 * Parses the command line.
	 *
	 * @param args the command line arguments
	 */
	private Properties parseCommandLine(String... args) {
		JCommander jc = new JCommander(this);
		jc.setProgramName(Experiment.PROGRAM_NAME);
		Properties result = new Properties();
		try {
			jc.parse(args);
			for (ParameterDescription d : jc.getParameters()) {
				Object o = d.getParameterized().get(this);
				result.put(d.getLongestName(), o == null ? "" : o);
				//log.info("On " + d.getLongestName() + " got: " + (o == null ? "" : o));
				if (d.getLongestName().equals("--input")) {
					ArrayList<URI> inputURIs = (ArrayList<URI>)(o);
					ArrayList<URI> fixedInputURIs = new ArrayList<>();
					for (URI u: inputURIs) {
						//System.out.println("== URI: " + u); 
						if (u.toString().startsWith("file:") && u.toString().contains("jdbc:postgresql")) {
							String s2 = u.getPath().substring(u.getPath().indexOf("jdbc"));
							s2 = s2.replace(":/localhost", "://localhost"); 
							try{
								URI u2 = new URI(s2);
								fixedInputURIs.add(u2);
								//System.out.println("== New URI: " + u2); 
							}
							catch(URISyntaxException use) {
								log.error("Could not make an URI out of " + s2); 
							}
						}
						else {
							fixedInputURIs.add(u); 
						}
					}
					this.inputs = fixedInputURIs;
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Use option '-h' for help.");
			System.exit(-1);
		}
		if (this.isHelp()) {
			jc.usage();
			System.exit(0);
		}
		if (this.config == null) {
			this.config = Config.getInstance();
		}
		if (!this.params.isEmpty()) {
			this.config.override(this.params); // IM, 24/1/2021 params is not explicitly filled
			// anywhere in the class, the above call is the one that actually takes the DB name specified with
			// -DRDBMSDBName=bla in the command line, and applies it. 
			// "params" is probably a pre-defined name from beust.
			this.locale = requireNonNull(Locale.forLanguageTag(this.config.getProperty("default_locale")));
		}
		this.config.setProperty("RDBMS_DBName", String.valueOf(this.config.getProperty("RDBMS_DBName")).toLowerCase());
		this.validateRDBMSName();
		if (this.config.getBooleanProperty("drawing.draw")) {
			this.print = true;
			this.config.setProperty("drawing.draw", "true");
		}

		this.config.setProperty("late", "false");
		if (this.late) {
			this.config.setProperty("late", "true");
		}

		this.config.setProperty("last", "false");
		if (this.lastLoad) {
			this.config.setProperty("last", "true");
		}
		// config.setProperty("drawing.draw", String.valueOf(this.print));
		return result;
	}

	private void validateRDBMSName() {
		String s = this.config.getProperty("RDBMS_DBName");
		char c = s.charAt(0);
		if ( (!('a'<=c)) || (!(c<='z')) || s.contains("-")){
			throw new IllegalStateException("RDBMS_DBName " + s + " should start with a letter and not contain -"); 
		}
	}
	/**
	 * Checks the help flag is set.
	 *
	 * @return true if the line command asked for help.
	 */
	public boolean isHelp() {
		return this.help;
	}

	/**
	 * @return true, if verbose mode is set
	 */
	public boolean isVerbose() {
		return this.verbose;
	}

	/**
	 * @return the entity extractor
	 */
	public EntityExtractor getExtractor() {
		return this.extractor;
	}

	/**
	 * @return true, if interactive mode is set
	 */
	public boolean isInteractive() {
		return this.interactive;
	}

	/**
	 * @return true, if the ConnectionLens instance should be reset upon start-up
	 */
	public boolean isResetFirst() {
		return this.noResetFirst;
	}

	/**
	 * @return the input file
	 */
	public List<URI> getInputs() {
		return this.inputs;
	}

	/**
	 * @return the program's output stream
	 */
	public PrintStream getOutput() {
		return this.output;
	}

	/**
	 * @return the query passed to the command line, if any.
	 */
	public String getQuery() {
		return this.query;
	}

	/**
	 * @return the name of the scoring function to use
	 */
	public String getScoring() {
		return this.config.getProperty("scoring_function");
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return this.locale;
	}

	/**
	 * Instantiates and runs.
	 *
	 * @param args the command line arguments
	 */
	@SuppressWarnings("unused")
	public static void main(String... args) {
		new Experiment(args);
	}

	/**
	 * Checks if an input path is a proper directory.
	 */
	public static class DirectoryValidator implements IParameterValidator {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IParameterValidator#validate(java.lang.String,
		 *      java.lang.String)
		 */
		@Override
		public void validate(String name, String value) throws ParameterException {
			try {
				File f = new File(value);
				if (f.exists() && !f.isDirectory()) {
					throw new ParameterException(name + " already exists and is not a directory.");
				}
			} catch (Exception e) {
				throw new ParameterException(name + " must be a valid directory .");
			}
		}
	}

	/**
	 * Checks if an input path is a proper directory.
	 */
	public static class ExistingDirectoryValidator implements IParameterValidator {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IParameterValidator#validate(java.lang.String,
		 *      java.lang.String)
		 */
		@Override
		public void validate(String name, String value) throws ParameterException {
			try {
				File f = new File(value);
				if (!(f.exists() && f.isDirectory())) {
					throw new ParameterException(name + " must be a valid directory.");
				}
			} catch (Exception e) {
				throw new ParameterException(name + " must be a valid directory.");
			}
		}
	}

	/**
	 * Checks if a path is a proper file or directory.
	 */
	public static class ExistingFileValidator implements IParameterValidator {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IParameterValidator#validate(java.lang.String,
		 *      java.lang.String)
		 */
		@Override
		public void validate(String name, String value) throws ParameterException {
			try {
				File f = new File(value);
				if (!f.exists()) {
					throw new ParameterException(name + " must be a valid file or directory.");
				}
			} catch (Exception e) {
				throw new ParameterException(name + " must be a valid file or directory.");
			}
		}
	}

	/**
	 * Returns the configuration whose external file is pointed by the input string.
	 */
	public static class ConfigConverter implements IStringConverter<Config> {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public Config convert(String value) {
			File file = new File(value);
			try {
				return Config.getInstance(value);
			} catch (Exception e) {
				if (!file.exists()) {
					log.warn("Input file '" + value + "' does not exists. ");
				}
				if (file.isDirectory()) {
					log.warn("Input path '" + value + "' is a directory. ");
				}
				if (!file.canRead()) {
					log.warn("Input file '" + value + "' cannot be read. ");
				}
				throw new ParameterException(value + " is not a valid input file.");
			}
		}
	}


	/**
	 * Returns the entity extractor implementation named after the parameter.
	 */
	public static class ExtractorConverter implements IStringConverter<EntityExtractor> {

		/**
		 * {@inheritDoc}
	 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public EntityExtractor convert(String value) {
			Locale l = Locale.forLanguageTag(Config.getInstance().getProperty("default_locale"));
			try {
				switch (Extractors.valueOf(value)) {
				case SNER:
					return new StanfordNERExtractor(l);
				case FLAIR_NER:
					return new FlairNERExtractor(l);
				case NONE:
					log.info("NO extractor");
					return new TopLevelExtractor();
				default:
					throw new ParameterException(value + " is not a valid extractor type.");
				}
			} catch (Exception e) {
				throw new ParameterException("Something went wrong when creating extractor " + value + ". Maybe it didn't find its models?");
				
			}
		}
	}

	public static EntityExtractor buildExtractor(StatisticsCollector givenExtractStats) {
		Locale l = Locale.forLanguageTag(Config.getInstance().getProperty("default_locale"));
		String value = Config.getInstance().getStringProperty("extractor", "SNER");
		EntityExtractor extractor = null; 
		try {
			switch (Extractors.valueOf(value)) {
			case SNER:
				extractor =  new StanfordNERExtractor(l); break; 
			case FLAIR_NER:
				extractor = new FlairNERExtractor(l); break; 
			case NONE:
				extractor = new TopLevelExtractor(l); break; 
			default:
				throw new ParameterException(value + " is not a valid extractor type.");
			}
		} catch (Exception e) {
			throw new IllegalStateException("Something went wrong when creating extractor " + value + ". Maybe it didn't find its models?");
		}
		extractor.setStats(givenExtractStats);
		return extractor; 
	}

	/**
	 * Returns a list of URI recursively extracted from the file hierarchy possibly
	 * starting at the given root.
	 */
	public static class URIOrFileHierachyConverter implements IStringConverter<List<URI>> {
		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public List<URI> convert(String value) {
			try {
				List<URI> result = Lists.newLinkedList();
				for (String f : value.split(",")) {
					result.addAll(recursiveFileAppend(f));
				}
				return result;
			} catch (IOException e) {
				throw new ParameterException(value + " is not a valid input file hierarchy.");
			}
		}
	}

	/**
	 * Returns a list of URI recursively extracted starting at the given root.
	 */
	public static class URIListConverter implements IStringConverter<List<URI>> {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public List<URI> convert(String value) {
			try {
				List<URI> result = Lists.newLinkedList();
				for (String url : value.split(",")) {
					result.add(new URI(url));
				}
				return result;
			} catch (URISyntaxException e) {
				throw new ParameterException(value + " is not a valid input file hierarchy.");
			}
		}
	}

	/**
	 * Returns an instance of Locale matching the given name.
	 */
	public static class LocaleConverter implements IStringConverter<Locale> {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public Locale convert(String value) {
			Locale result = Locale.forLanguageTag(value);
			if (!supportedLocales.contains(result)) {
				throw new ParameterException("Locale '" + value + "' is not currently supported.");
			}
			return result;
		}
	}

	/**
	 * Recursively append files from the given root.
	 *
	 * @param file the file to recursively traverse
	 * @return the lists of input file's descendants
	 * @throws IOException signaling that an I/O exception has occurred.
	 */
	static List<URI> recursiveFileAppend(String file) throws IOException {
		List<URI> result = Lists.newLinkedList();
		File root = new File(file);
		if (!root.isDirectory()) {
			result.add(root.toURI());
			return result;
		}
		for (File f : root.listFiles()) {
			result.addAll(recursiveFileAppend(f.getPath()));
		}
		return result;
	}

	/**
	 * Input stream over a file with a name or prefix.
	 */
	public static class QualifiedFileInputStream extends FileInputStream {

		/**
		 * The file path.
		 */
		private final String path;

		/**
		 * Instantiates a new qualified file input stream.
		 *
		 * @param n the qualified name
		 * @param f the file
		 * @throws FileNotFoundException
		 */
		QualifiedFileInputStream(String n, File f) throws FileNotFoundException {
			super(f);
			this.path = n;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return this.path;
		}
	}

	/**
	 * Converts the output String to a PrintStream if it corresponds to a valid file
	 * name. Otherwise, reverts to STDOUT.
	 */
	public static class OutputStreamConverter implements IStringConverter<PrintStream> {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.IStringConverter#convert(java.lang.String)
		 */
		@Override
		public PrintStream convert(String value) {
			File file = new File(value);
			try {
				return new PrintStream(file);
			} catch (FileNotFoundException e) {
				if (!file.exists()) {
					log.warn("Input file '" + value + "' does not exists. ");
				}
				if (file.isDirectory()) {
					log.warn("Input path '" + value + "' is a directory. ");
				}
				if (!file.canWrite()) {
					log.warn("Input file '" + value + "' cannot be written. ");
				}
				log.warn("Reverting to STDOUT");
				return System.out;
			}
		}
	}

	/**
	 * Breaks a comma-separated list of in a String into a List<String>
	 */
	public static class StringSplitter implements IParameterSplitter {

		/**
		 * {@inheritDoc}
		 *
		 * @see com.beust.jcommander.converters.IParameterSplitter#split(java.lang.String)
		 */
		@Override
		public List<String> split(String value) {
			return Arrays.asList(value.split(","));
		}
	}

	/**
	 * @return the version of the builders code, as given by Maven
	 */
	public static Properties getBuildProperties() {
		Properties result = new Properties();
		String path = "/version";
		try (InputStream stream = Experiment.class.getResourceAsStream(path)) {
			result.load(stream);
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
		}
		return result;
	}
}

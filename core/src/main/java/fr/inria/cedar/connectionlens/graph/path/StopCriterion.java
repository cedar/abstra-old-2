/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph.path;

/**
 * This class supervises the heuristic selected by <code>P2BComputation</code>. 
 * It must be derived to subclasses that implement different mecanisms of 
 * heuristic themselves.
 * 
 * @author Mihu
 *
 */
public abstract class StopCriterion  {

	/** A constructor */
	public StopCriterion() {}
	
	/**
	 * The method that tells the objects being supervised if the stop criterion has been reached
	 * @return <code>true</code> if the stop criterion has been met, <code>false</code> otherwise
	 */
	public abstract boolean canStop();
	
	/** Reset the current state observed by the stop criterion */
	public abstract void reset();
	
	/** Update the current state observed by the stop criterion. 
	 * This differs for each type of heuristic, e.g. in case of TimeOutCriterion, 
	 * the current duration is incremented by 1, or in case of MaxEdgesCriterion, 
	 * the current number of edges being discovered 
	 */
	public abstract void update();
}

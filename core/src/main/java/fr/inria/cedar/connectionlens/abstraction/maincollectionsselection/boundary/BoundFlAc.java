package fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.boundary;

import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.BoundaryNode;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionBoundary;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph;
import fr.inria.cedar.connectionlens.abstraction.collectionsidentification.Path;
import fr.inria.cedar.connectionlens.abstraction.maincollectionsselection.Configuration;
import org.apache.log4j.Logger;

import java.util.ArrayList;

import static fr.inria.cedar.connectionlens.abstraction.collectionsidentification.CollectionGraph.ReportStatus.REPORTED;

public class BoundFlAc extends BoundaryMethod {
    public static final Logger log = Logger.getLogger(BoundFlAc.class);

    public BoundFlAc(int cstar, Configuration configuration) {
        super(cstar, configuration);
    }

    // When using PRodw (Section 4.2.2), the global weight transfers do not clearly suggest a boundary detection method.
    // On one hand, we can use the computeBoundaryWithBooleanLowerDAGProp method above.
    // Another method is to traverse the graph edges starting from the given collection Ci,
    // and include in Ci's boundary its neighbor node Cj if and only if:
    //   (i) the edge Ci -> Cj has a transfer factor of at least etfMin, or
    //   (ii) each node from Ci has at most one child in Cj.
    // The intuition for (ii) is that such a child Cj resembles an attribute of Ci, rather than being independent Ci.
    // We call this the flooding boundary (fBound) of Ci.
    @Override
    public CollectionBoundary compute() {
        log.debug("COMPUTE STRONG FLOODING BOUNDARY OF C" + this.cstar);
        CollectionGraph.getWorkingInstance().setStatusOfCollection(this.cstar, REPORTED);
        CollectionGraph.getInstance().setStatusOfCollection(this.cstar, REPORTED);

        this.alreadyVisitedCollections = new ArrayList<>();
        CollectionBoundary collectionBoundary = new CollectionBoundary();
        BoundaryNode bn = new BoundaryNode(this.cstar, collectionBoundary);
        collectionBoundary.setMainCollectionRoot(bn);
        log.debug(CollectionGraph.getWorkingInstance().getOutgoingEdges(collectionBoundary.getMainCollectionRoot().getId()));

        // NEW WAY: each collection that transfers some of its weight to the reported collection is part of the boundary
        // we still need to use recursion because the boundary that we want should reflect the DAG of the nodes. Therefore, we cannot just do for(collId) { if(ptf>0) { ... } }
        log.debug(CollectionGraph.getWorkingInstance().getCollectionsInvolvedInACycle());
        log.info("buildBoundaryRecursive(" + this.cstar + ", " + bn + ", " + collectionBoundary + ", " + new Path() + ", " + -1 + ", " + -1);
        this.buildBoundaryRecursive(this.cstar, bn, collectionBoundary, new Path(), -1, -1);

        return collectionBoundary;
    }
}

package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

//import com.alibaba.fastjson.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class CollectionBoundary implements Iterable<BoundaryNode> {
    public static final Logger log = Logger.getLogger(CollectionBoundary.class);
    private BoundaryNode mainCollectionRoot;
    private ArrayList<Integer> collectionsInBoundary = new ArrayList<>();
    private ArrayList<String> collectionsInBoundaryString = new ArrayList<>();

    public CollectionBoundary() {
        this.mainCollectionRoot = null;
        this.collectionsInBoundary = new ArrayList<>();
        this.collectionsInBoundaryString = new ArrayList<>();
    }

    public CollectionBoundary(int mainCollectionRoot) {
        this.mainCollectionRoot = new BoundaryNode(mainCollectionRoot, this);
        this.collectionsInBoundary = new ArrayList<>();
        this.collectionsInBoundaryString = new ArrayList<>();
        this.addCollectionToList(mainCollectionRoot);
    }

    public CollectionBoundary(BoundaryNode bn) {
        this.mainCollectionRoot = bn;
        this.collectionsInBoundary = new ArrayList<>();
        this.collectionsInBoundaryString = new ArrayList<>();
    }

    public void addCollectionToList(int collectionId) {
        if(!this.collectionsInBoundary.contains(collectionId)) {
            this.collectionsInBoundary.add(collectionId);
            String collectionLabel = CollectionGraph.getInstance().getCollectionLabel(collectionId);
            this.collectionsInBoundaryString.add(collectionLabel);
        }
    }

    public void addCollectionToListString(String collectionLabel) {
        if(!this.collectionsInBoundaryString.contains(collectionLabel)) {
            this.collectionsInBoundaryString.add(collectionLabel);
        }
    }

    public void setMainCollectionRoot(BoundaryNode mainCollectionRoot) {
        this.mainCollectionRoot = mainCollectionRoot;
    }

    public BoundaryNode getMainCollectionRoot() {
        return this.mainCollectionRoot;
    }

    public ArrayList<Integer> getCollectionsInBoundary() {
        return this.collectionsInBoundary;
    }

    public ArrayList<String> getCollectionsInBoundaryStrings() {
        return this.collectionsInBoundaryString;
    }

    public ArrayList<Integer> getNonLeafCollectionsInBoundary() {
        ArrayList<Integer> nonLeafCollectionsInBoundary = new ArrayList<>();
        for(int collectionId : this.collectionsInBoundary) {
            if(!CollectionGraph.getInstance().getCollectionLabel(collectionId).endsWith("#val")) {
                nonLeafCollectionsInBoundary.add(collectionId);
            }
        }
        return nonLeafCollectionsInBoundary;
    }

    public ArrayList<String> getNonLeafCollectionsInBoundaryStrings() {
        ArrayList<String> nonLeafCollectionsInBoundaryStrings = new ArrayList<>();
        for(String collectionLabel : this.collectionsInBoundaryString) {
            if(!collectionLabel.endsWith("#val")) {
                nonLeafCollectionsInBoundaryStrings.add(collectionLabel);
            }
        }
        return nonLeafCollectionsInBoundaryStrings;
    }

    // we assume that the entity is convertible as a HashMap
    // this method converts the JSON entity into a CollectionBoundaryObject
    public static CollectionBoundary getCollectionBoundaryFromEntityObjectMain(Object entity, HashMap<String, ArrayList<String>> referenceRelationships) {
        CollectionBoundary collectionBoundary = new CollectionBoundary();
        JSONObject jsonEntity = (JSONObject) entity;
        log.debug("in jsonEntity: "+jsonEntity);
        log.debug(referenceRelationships.values());

        // get the property label (if this is the first call, then this is the main entity label, else this is the property label)
        if(!jsonEntity.keySet().isEmpty()) {
            String currentElementLabel = (String) jsonEntity.keySet().toArray()[0];
            log.debug("currentElementLabel: " + currentElementLabel);
            if (!referenceRelationships.values().contains(currentElementLabel)) {
                BoundaryNode bn = new BoundaryNode(currentElementLabel, collectionBoundary);
                collectionBoundary.setMainCollectionRoot(bn);
                log.debug("collection boundary before : " + collectionBoundary.getCollectionsInBoundaryStrings());
                collectionBoundary.getCollectionsInBoundaryStrings().add(currentElementLabel);
                log.debug("collection boundary after : " + collectionBoundary.getCollectionsInBoundaryStrings());


                // get the children
                JSONArray children = (JSONArray) jsonEntity.get(currentElementLabel);

                // iterate over children of root property label to recursively compute boundary node
                for (Object child : children) {
                    JSONObject childObject = (JSONObject) child;
                    if (!childObject.keySet().isEmpty()) {
                        getCollectionBoundaryFromEntityObject(childObject, bn, collectionBoundary, referenceRelationships);
                    }
                }
            }
        }

        return collectionBoundary;
    }

    private static void getCollectionBoundaryFromEntityObject(JSONObject entity, BoundaryNode bn, CollectionBoundary collectionBoundary, HashMap<String, ArrayList<String>> referenceRelationships) {
        JSONObject jsonEntity = (JSONObject) entity;
//        log.debug("in jsonEntity: "+jsonEntity);
//        log.debug("collection boundary before: "+collectionBoundary.getCollectionsInBoundaryStrings());
        log.debug(referenceRelationships.values());

        // get the property label (if this is the first call, then this is the main entity label, else this is the property label)
        if(!jsonEntity.keySet().isEmpty()) {
            String currentElementLabel = (String) jsonEntity.keySet().toArray()[0];
            log.debug("currentElementLabel: " + currentElementLabel);
            if (!referenceRelationships.values().contains(currentElementLabel)) {
                BoundaryNode bn1 = new BoundaryNode(currentElementLabel, collectionBoundary);
                log.debug("collection boundary before: " + collectionBoundary.getCollectionsInBoundaryStrings());
                bn.addChildString(bn1);
                log.debug("collection boundary after: " + collectionBoundary.getCollectionsInBoundaryStrings());


                // get the children
                JSONArray children = (JSONArray) jsonEntity.get(currentElementLabel);

                // iterate over children of root property label to recursively compute boundary node
                for (Object child : children) {
                    JSONObject childObject = (JSONObject) child;
                    if (!childObject.keySet().isEmpty()) {
                        getCollectionBoundaryFromEntityObject(childObject, bn1, collectionBoundary, referenceRelationships);
                    }
                }
            }
        }
    }

    public void sortByFrequency(HashMap<Integer, HashMap<Integer, Double>> frequencies) {
        this.mainCollectionRoot.sortByFrequency(frequencies);
    }

    @Override
    public String toString() {
//        return "CollectionBoundary{" +
//                "mainCollectionRoot=" + this.mainCollectionRoot +
//                ", collectionsInBoundary=" + this.collectionsInBoundary +
//                ", collectionsInBoundaryString=" + this.collectionsInBoundaryString +
//                '}';
        return this.mainCollectionRoot.toString();
    }

    @Override
    public Iterator<BoundaryNode> iterator() {
        return new BoundaryNodeIterator(this);
    }
}
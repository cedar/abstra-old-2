/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.graph;

import com.google.common.cache.Cache;
import com.google.common.collect.Sets;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.extraction.EntityType;
import fr.inria.cedar.connectionlens.extraction.ExtractionPolicy;
import fr.inria.cedar.connectionlens.graph.sim.NodePairSelector;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionlens.util.Session;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import org.apache.commons.lang3.tuple.Pair;

import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;

import java.net.URI;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Interface for virtual graphs.
 */
public interface Graph {

	/**
	 * Enumeration of graph types.
	 */
	enum GraphType {
		ORIGINAL_GRAPH,
		ABSTRACT_GRAPH,
		NORMALIZED_GRAPH,
		SUMMARY_GRAPH,
		CLASSIFIED_GRAPH
	}

	/** The regular expression used to parse an edge's URI */
	static final Pattern regexp = Pattern.compile("->\\{([^{}]+)\\}\\{([^{}]+)\\}");

	/** Showing node context (in the GUI) requires accessing incoming edges,
	 * of which there can be too many in large graphs.  */
	public static final Integer maxEdgesReadForContext = 100; 

	/**
	 * Reset.
	 */
	void reset();

	void createNodeLabelPrefixIndex();

	void createDisambiguatedIndex();


	/**
	 * Close.
	 */
	void close();

	/**
	 * @param id the node ID
	 * @return an instance of the node with this ID, possibly retrieving it from the node cache
	 */
	Node resolveNode(NodeID id);

	/**
	 * @param id the edge ID
	 * @return an instance of the edge with this ID, possibly retrieving it from the edge cache
	 */
	Edge resolveEdge(EdgeID id);

	Node resolveDataSource(int id);

	int nextID();

	void setIDCounter (Boolean reset);
	
	

	/**
	 * @param n1 the source node
	 * @param n2 the target node
	 * @param label the edge label
	 * @return the given edge's specificity
	 */
	Specificity resolveSpecificity(Node n1, Node n2, String label);

	/**
	 * @return the node cache
	 */
	Cache<NodeID, Node> nodeCache();

	Cache<NodeID, Set<Edge>> sameAsCache();

	int sameAsCacheSize();

	/**
	 * @return the edge cache
	 */
	Cache<EdgeID, Edge> edgeCache();

	/**
	 * @return in-degree cache
	 */
	Cache<Pair<Node, String>, Specificity> inDegreeCache();

	/**
	 * @return out-degree cache
	 */
	Cache<Pair<Node, String>, Specificity> outDegreeCache();

	/**
	 * @return the node cache size
	 */
	int nodeCacheSize();

	/**
	 * @return the edge cache size
	 */
	int edgeCacheSize();

	/**
	 * Fetch an item from the given cache, iff the cache size is greater than 0.
	 *
	 * @param <I> the type of the input item
	 * @param <O> the type of the output item
	 * @param input the input item
	 * @param cache the item cache 
	 * @param size the cache size
	 * @param c the function to call in case of cache miss.
	 * @return the item found in (or possibly added to) the cache 
	 */
	default <I, O> O maybeUseCache(I input, Cache<I, O> cache, int size, Callable<O> c) {
		try {
			if (size > 0) {
				return cache.get(input, c);
			}
			return c.call();
		} catch (Exception e) {
			System.out.println(size);
			System.out.println(input);
			System.out.println(cache);
			System.out.println(c);
			// e.printStackTrace();
			throw new IllegalStateException(e);
		}
	}

	/**
	 * @param n the node for which the context is requested
	 * @return a short string describing what the node is and what it comes from 
	 * This handles a few cases that occur in any kind of data source, 
	 * then it leaves the rest to other sources
	 */
	default String getContext(Node n) {
		if(this.isNotOriginal()){
			return "";
		}
		// data source: 
		if(n.getType().equals (Node.Types.DATA_SOURCE)){
			return ("Data source <b>" + n.getDataSource().getLocalURI() + "</b>");
		}
		// original URI, local URI, or URI found in other places:
		if (n.getType() == RDF_URI) {
			StringBuffer sb = new StringBuffer();
			DataSource ds = n.getDataSource();
			Collection<Edge> incoming = getKIncomingEdgesPostLoading(n, maxEdgesReadForContext);
			for (Edge e: incoming) {
				if (e.getEdgeType() == Edge.Types.ORIGINAL_URL) {
					sb.append("Original URL <b>" + n.getLabel() + "</b> </i> of" +  
						ds.getLocalURI()+"</i>");
					return new String(sb);
				}
				else {
					if (e.getEdgeType() == Edge.Types.LOCAL_URL) {
						sb.append("Data source address in local file system when loaded: <b>" + 
								n.getLabel() +
								"</b>");
						return new String(sb);
					}
				}
			}	
			return ("URI <b>" + n.getLabel() + "</b> encountered in " + n.getDataSource().getType() +
					" data source <i>" + n.getDataSource().getLocalURI() + "</i>"); 
		}
		// extracted entities: 
		if (EntityType.isEntityType(n.getType())){
			StringBuffer sb = new StringBuffer();
			sb.append( 
					Node.decodeType(n.getType()) + " node <b> "
							+ n.getLabel()  
							+ "</b> extracted from its parent"); 
			Collection<Edge> incoming = getKIncomingEdgesPostLoading(n, maxEdgesReadForContext);
			TreeSet<URI> docURIs = new TreeSet<URI>();
			for (Edge e: incoming) {
				docURIs.add(e.getSourceNode().getDataSource().getLocalURI());
			}
			//System.out.println("Incoming: " + incoming.size() + " sources: " + docURIs.size()); 
			if (incoming.size()>1) {
				sb.append("s");
			}
			sb.append(" from " + (docURIs.size()>1?(docURIs.size() + " "):"") + 			
					"source" + (docURIs.size()>1?"s":"") +
					": <i>");
			int i = 0; 
			for (URI u: docURIs) {
				sb.append(u); 
				i++;
				if (i != docURIs.size()) {
					sb.append(", "); 
				}
			}
			// signal if the parent list has been truncated
			if (incoming.size() == maxEdgesReadForContext) {
				sb.append(" (and more)"); 
			}
			sb.append("</i>"); 
			return new String(sb); 
		}
		// anything else: 
		return n.getDataSource().getContext(this, n);
	}

	/**
	 *
	 * @param n A node
	 * @param k The desired maximum number of adjacent edgs
	 * @return The k most specific incoming edges, plus the k most specific outgoing edges 
	 */
	default Set<Edge> getSpecificEdges(Node n, int k){
		return Sets.union(getKOutgoingEdgesPostLoading(n, k), getKIncomingEdgesPostLoading(n, k)); 
	}

	/**
	 *
	 * @param oldNodes
	 * @param newNodes
	 * @return all Edges between two collections of odes: old and new
	 * this function will return:
	 * all edges from any old nodes to any new nodes.
	 * all edges from any new nodes to any old nodes.
	 * all edges from any new nodes to any new nodes.
	 */
	Set<Edge> getEdgesBetweenSets(Collection<Node> oldNodes, Collection<Node>newNodes);


	/**
	 * @param label some node label
	 * @return the set of all nodes having the given label 
	 */
	Set<Node> getNodes(String label, Item.Types... types);

	/**
	 * @param ds some data source
	 * @return the set of all nodes in the given data source.
	 */
	Set<Node> getNodes(DataSource ds, Item.Types... types);

	/**
	 * @param ds some data source
	 * @param label some label
	 * @return the set of all nodes from the given data source having the given label.
	 */
	Set<Node> getNodes(DataSource ds, String label, Item.Types... types);
	
	/**
	 * @param n1 some source node
	 * @param n2 some target node
	 * @return the set of all edges with the given source and target nodes.
	 */
	Set<Edge> getEdges(Node n1, Node n2, Item.Types... types);

	/**
	 * @param label some label
	 * @return the set of all edges having the given label
	 */
	Set<Edge> getEdges(String label, Item.Types... types);

	/**
	 * @param ds the data source
	 * @return the set of all edges from the given data source.
	 */
	Set<Edge> getEdges(DataSource ds, Item.Types... types);
	
	/**
	 * @param ds some data source
	 * @param label some label
	 * @return the set of all edges from the given data source having the given label.
	 */
	Set<Edge> getEdges(DataSource ds, String label, Item.Types... types);

	/**
	 * Updates the value of the node's type in the database
	 * @param id the id of the node involved
	 * @param t the new value for the type
	 */
	void setType(NodeID id, Node.Types t);


	/**
	 @param ds
	 @return all nodes disambiguated using Ambiverse.
	 */
	Set<Node> getDisambiguatedNodes(DataSource ds);

	/**
	 @param label
	 @return the ID (wiki url for the corresponding label)
	 */
	String getDisambiguatedID(String label);

	/**
	 @param node
	 @return the ID (wiki url for the corresponding node)
	 */
	String getDisambiguatedForTheNode(Node node);
	
	Set<Node> getNodes(Types nodeType);


	/**
	 * @param n some node
	 * @return the set of outgoing edges from n
	 */
	Set<Edge> getOutgoingEdges(Node n, Item.Types... types);
	
	/**
	 * @param n some node
	 * @param labels the allowed labels on the outgoing edges
	 * @return the set of outgoing edges from {@code n} labelled {@code labels}.
	 */
	Set<Edge> getOutgoingEdges(Node n, String[] labels, Item.Types... types);
	
	Node resolveEntityNode(String entityNodeLabel); 

	// IM, 11/6/2020: returns just one, not to be used unless we are sure there is only one.
	Node resolveNodeByLabel(String nodeLabel);


	Boolean computeSimilarDisambiguatedEntities(Consumer<Edge> processor);


	/** @return index of the graph **/
	IndexAndProcessNodes index();

	/**
	 * @param n some node
	 * @param ds some data source
	 * @return the set of outgoing edges from whose target node belongs to ds
	 */
	default Set<Edge> getOutgoingEdges(Node n, DataSource ds, Item.Types... types) {
		return Sets.filter(getOutgoingEdges(n, types), e->e.getTargetNode().getDataSource().equals(ds));
	}

	/**
	 * @param n some node
	 * @param exclude nodes to exclude
	 * @return the set of outgoing edges excluding this in exclude input set.
	 */
	default Set<Edge> getOutgoingEdges(Node n, Set<Node> exclude, Item.Types... types) {
		return Sets.filter(getOutgoingEdges(n, types), e->!exclude.contains(e.getTargetNode()));
	}

	/**
	 * @param n some node
	 * @param ds some data source 
	 * @param exclude nodes to exclude
	 * @return the set of outgoing edges from whose target node belongs to ds, and excluding this
	 * in exclude input set.
	 */
	default Set<Edge> getOutgoingEdges(Node n, DataSource ds, Set<Node> exclude, Item.Types... types) {
		return Sets.filter(getOutgoingEdges(n, types), e->e.getTargetNode().getDataSource().equals(ds)
				&& !exclude.contains(e.getTargetNode()));
	}

	/**
	 * @param n some node
	 * @return the set of incoming edges to n
	 */
	Set<Edge> getIncomingEdges(Node n, Item.Types... types);
	
	/**
	 * @param n some node
	 * @param labels the permitted labels on the incoming edges
	 * @return the set of incoming edges to {@code n} labelled {@code labels}.
	 */
	Set<Edge> getIncomingEdges(Node n, String[] labels, Item.Types... types);

	/**
	 * @param n some node
	 * @param ds some data source
	 * @return the set of incoming edges to b belonging to ds
	 */
	default Set<Edge> getIncomingEdges(Node n, DataSource ds, Item.Types... types) {
		return Sets.filter(getIncomingEdges(n, types), e->e.getSourceNode().getDataSource().equals(ds));
	}

	/**
	 * @param n some node
	 * @param exclude nodes to exclude
	 * @return the set of incoming edges to b excluding those in the exclude input set.
	 */
	default Set<Edge> getIncomingEdges(Node n, Set<Node> exclude, Item.Types... types) {
		return Sets.filter(getIncomingEdges(n, types), e->!exclude.contains(e.getSourceNode()));
	}

	/**
	 * @param n some node
	 * @param ds some data source
	 * @param exclude nodes to exclude
	 * @return the set of incoming edges to b belonging to ds excluding those in the exclude input 
	 * set.
	 */
	default Set<Edge> getIncomingEdges(Node n, DataSource ds, Set<Node> exclude, Item.Types... types) {
		return Sets.filter(getIncomingEdges(n, types), e->e.getSourceNode().getDataSource().equals(ds)
				&& !exclude.contains(e.getSourceNode()));
	}

	/**
	 * @param n some node
	 * @return the set of edges adjacent to n
	 */
	default Set<Edge> getAdjacentEdges(Node n) {
		return Sets.union(getOutgoingEdges(n), getIncomingEdges(n));
	}
	
	/**
	 * @param n some node
	 * @param labels the allowed labels on the edges
	 * @return the set of edges adjacent to n labelled labels.
	 */
	default Set<Edge> getAdjacentEdges(Node n, String[] labels) {
		return Sets.union(getOutgoingEdges(n, labels), getIncomingEdges(n, labels));
	}

	/**
	 * @param n some node
	 * @return the set of edges adjacent to n
	 */
	default Set<Edge> getAdjacentEdges(Node n, Item.Types... types) {
		return Sets.union(getOutgoingEdges(n, types), getIncomingEdges(n, types));
	}

	/**
	 * @param n some node
	 * @param ds some data source
	 * @return the set of edges adjacent to b within ds.
	 */
	default Set<Edge> getAdjacentEdges(Node n, DataSource ds, Item.Types... types) {
		return Sets.union(
				getOutgoingEdges(n, ds, types),
				getIncomingEdges(n, ds, types));
	}

	/**
	 * @param from a given node
	 * @return the set of same-as edges whose whose source  is that node, and whose confidence
	 * value is above the given threshold.
	 * Similarity is in general symmetric, 
	 * thus the graph should ensure that edges are returned also when this is asked with the target node. 
	 */
	default Set<Edge> getWeakSameAs(Node from) {
		return maybeUseCache(from.getId(), sameAsCache(), sameAsCacheSize(), () -> {
			return getWeakSameAs(from, 0.0);
		});
	}

	/**
	 * @param from a given node
	 * @return the set of same-as edges whose whose source  is that node, and whose confidence
	 * value is above the given threshold.
	 * Similarity is in general symmetric, 
	 * thus the graph should ensure that edges are returned also when this is asked with the target node. 
	 */
	Set<Edge> getWeakSameAs(Node from, double threshold);

	/**
	 * @param from some node
	 * @param to some data source
	 * @param threshold some threshold
	 * @return the set of same-as edges stemming from the given data source whose confidence
	 * value is above the given threshold.
	 */
	Set<Edge> getWeakSameAs(Node from, DataSource to, double threshold);

	/**
	 * @param from some data source
	 * @param threshold some threshold
	 * @return the set of same-as edges stemming from the given data source whose confidence
	 * value is above the given threshold.
	 */
	Set<Edge> getSameAs(Node from, double threshold);

	/**
	 * @param from some source data source
	 * @param to some target data source
	 * @param threshold some threshold
	 * @return the set of same-as edges between the given data sources whose confidence
	 * value is above the given threshold.
	 */
	Set<Edge> getSameAs(Node from, DataSource to, double threshold);

	/**
	 * @param threshold some threshold
	 * @return the set of same-as edges whose confidence value is above the given threshold.
	 */
	Set<Edge> getSameAs(double threshold);

	/**
	 * get the mean specificity of all edges along with the standard deviation
	 */
	Pair<Double, Double> getSpecificityStat();

	/** Should be used only after the graph has been completely loaded. */
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, Double threshold, Item.Types... types);
	
	/** Should be used only after the graph has been completely loaded. */

	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, Double threshold, Item.Types... types);
	
	/** Should be used only after the graph has been completely loaded. */
	public Set<Edge> getSpecificOutgoingEdgesPostLoading(Node n, String[] label, Double threshold, Item.Types... types);
	
	/** Should be used only after the graph has been completely loaded. */

	public Set<Edge> getSpecificIncomingEdgesPostLoading(Node n, String[] label, Double threshold, Item.Types... types);
	
	/** Should be used only after the graph has been completely loaded. */
	public Set<Edge> getKOutgoingEdgesPostLoading(Node n, Integer k, Item.Types... types);

	/** Should be used only after the graph has been completely loaded. */
	public Set<Edge> getKIncomingEdgesPostLoading(Node n, Integer k, Item.Types... types);
	
	/**
	 * @param from some node
	 * @return the set of (strong or weak) same-as edges starting from the given node.
	 */
	default Set<Edge> getSameAs(Node from) {
		return maybeUseCache(from.getId(), sameAsCache(), sameAsCacheSize(), () -> {
			return getSameAs(from, 0.0);
		});
	}

	/** strong and weak same-as edges */
	default Set<Edge> getSameAs(Node from, int limit) {
		return new HashSet<> ((new ArrayList<> (getSameAs(from))).subList(0, limit));
	}
	
	default Collection<Node> getAllNodes(DataSourceCatalog catalog){
		Set<Node> nodes = new HashSet<>();
		catalog.getDataSources().forEach(ds -> {
			nodes.addAll(getNodes(ds));
		});
		return nodes;
	}
	default Collection<Edge> getAllEdges(DataSourceCatalog catalog){
		Set<Edge> edges = new HashSet<>();
		catalog.getDataSources().forEach(ds -> {
			edges.addAll(getEdges(ds));
		});
		return edges;
	}
	/**
	 * @param types
	 * @return the set of all edges of the given type(s)
	 */
	default Collection<Edge> getEdges(DataSourceCatalog catalog, Item.Types... types){
		Set<Edge> edges = new HashSet<>();
		catalog.getDataSources().forEach(ds -> {
			edges.addAll(getEdges(ds, types));
		});
		return edges;
	}

	/**
	 * @param from some node
	 * @param to some data source
	 * @return the set of same-as edges starting from the given node and ending in the given data
	 * source.
	 */
	default Set<Edge> getSameAs(Node from, DataSource to) {
		return getSameAs(from, to, 0.0);
	}

	/**
	 * @return the all known same-as edges
	 */
	default Set<Edge> getSameAs() {
		return getSameAs(0.0);
	}


	/**
	 * @param oldNodes
	 * @param newNodes
	 * @param threshold
	 * @return same_as Edges between two collections of odes: old and new
	 * this function will return:
	 * same_as edges from any old nodes to any new nodes with score above threshold.
	 * same_as edges from any new nodes to any old nodes with score above threshold.
	 * same_as edges from any new nodes to any new nodes with score above threshold.
	 *
	 * Used only from the GUI. No performance issue here (stateless, supposes everything is in memory)
	 */
	default Set<Edge> getSameAsBetweenSets(Collection<Node> oldNodes, Collection<Node>newNodes, double threshold){
		// From new nodes
		Set<Edge> fromNew = newNodes.stream()
				.map(n -> this.getSameAs(n, threshold).stream().map(e -> e.toPair(n.getId()))
						.filter(p -> newNodes.contains(p.second()) || oldNodes.contains(p.second())).map(p -> p.first())
						.collect(Collectors.toSet()))
						.reduce(new HashSet<Edge>(), (set1, set2) -> Sets.union(set1, set2));

		// From old nodes
		Set<Edge> fromOld = oldNodes.stream()
				.map(n -> this.getSameAs(n, threshold).stream().map(e -> e.toPair(n.getId()))
						.filter(p -> newNodes.contains(p.second())).map(p -> p.first()).collect(Collectors.toSet()))
						.reduce(new HashSet<Edge>(), (set1, set2) -> Sets.union(set1, set2));
		return Sets.union(fromNew, fromOld);
	}

	/**
	 * 
	 * @return true if the current graph is of type ABSTRACT, false else.
	 */
	boolean isAbstract();

	/**
	 *
	 * @return true if the current graph is of type SUMMARY, false else.
	 */
	boolean isSummary();

	/**
	 *
	 * @return true if the current graph  is of type CLASSIFIED, false else.
	 */
	boolean isClassified();

	/**
	 *
	 * @return true if the current graph is not of type ORIGINAL, false else.
	 */
	boolean isNotOriginal();

	/**
	 *
	 * @return true if the current graph is of type NORMALIZED, false else.
	 */
	boolean isNormalized();

	/**
	 *
	 * @return true if the current graph is of type ORIGINAL, false else.
	 */
	boolean isOriginal();

	/**
	 * 
	 * @return the abstract graph (created if not set previously)
	 */
	Graph getAbstractGraph();

	/**
	 *
	 * @return the normalized graph (created if not set previously)
	 */
	Graph getNormalizedGraph();

	/**
	 *
	 * @return the classified graph (created if not set previously)
	 */
	Graph getClassifiedGraph();

	/**
	 *
	 * @return the summary graph (created if not set previously)
	 */
	Graph getSummaryGraph();

	GraphType getGraphType();

	/**
	 * Updates the value of the abstract graph.
	 * @param g the new abstract graph
	 */

	/**
	 * Updates the value of the normalized graph
	 * @param g the new normalized graph
	 */
	void updateNormalizedGraph(RelationalGraph g);

	void updateSummaryGraph(RelationalGraph g);

	void updateAbstractGraph(RelationalGraph g);

	void updateClassifiedGraph(RelationalGraph g);


	/**
	 * Update the graph considering its type
	 * @param g the new graph
	 * @param graphType the type of the new graph
	 */
	void updateGraph(RelationalGraph g, GraphType graphType);
	
	/**
	 * Enumerates pairs of nodes having the some chance to be deemed  by the similarity
	 * function. Each such pair leads to a same-as edge between the two nodes, which is
	 * processed using the given processor Consumer.
	 *
	 * @param processor a processor for the same-as edge generated but the method
	 */
	void enumeratePairs(Collection<DataSource> from, Collection<DataSource> to,
						NodePairSelector selector, Consumer<Edge> processor);

	/**
	 * Generate the specificity table for all the edges in the graph
	 * @param statisticsCollector 
	 */
	boolean generateEdgeSpecificityComputeMeanStdDev(StatisticsCollector statisticsCollector);

	/**
	 * @param ds some data source.
	 * @return the number of edges within the given data source, with any of the given type.
	 */
	int countEdges(DataSource ds, Item.Types... types);

	NodeID getOriginalNodeID(Node n);

	/**
	 * @param ds some data source.
	 * @return the number of same-as edge within the given data source.
	 */
	int countSameAs(DataSource ds);

	/**
	 * @param ds some data source.
	 * @return the number of nodes within the given data source.
	 */
	int countNodes(DataSource ds, Item.Types... types);

	ItemID.Factory getIDFactory();
	
	/**
	 * Open a session allowing to modifying the graph.
	 *
	 * @return a new graph session
	 */
	default GraphSession openSession() {
		return openSession(-1);
	}

	/**
	 * Open a session allowing to modifying the graph, with a batch size determine the size of 
	 * batches to send to the underlying graph representation.
	 *
	 * @param batchSize the batch size
	 * @return a new graph session with the given batch size.
	 */
	GraphSession openSession(int batchSize); 
	/**
	 * Open a session allowing to modifying the graph, with a batch size determine the size of 
	 * batches to send to the underlying graph representation.
	 *
	 * @param batchSize
	 * @return a new graph session with the given batch size.
	 */
	GraphSession openSession (int batchSize, Function<String, int[]> f, StatisticsCollector graphStats, StatisticsCollector extractStats, StatisticsCollector simStats);


	/**
	 * GraphSession interface
	 */
	public static interface GraphSession extends Session {

		/**
		 * @param n some node
		 * @return true, iff the given node has already been processed within this session.
		 */
		boolean hasProcessed(Node n);

		/**
		 * @param e some edge
		 * @return true, iff the given edge has already been processed within this session.
		 */
		boolean hasProcessed(Edge e);

		/**
		 * Adds the given node to the graph.
		 *
		 * @param n some node 
		 * @param processor some node processor to use directly after the node is successfully 
		 * added to the graph.
		 * @return true, iff the graph was modified as a result of this method.
		 */
		boolean addNode(Node n, Consumer<Node> processor);

		/**
		 * Adds the given edge to the graph.
		 *
		 * @param edge some edge
		 * @return true, iff the graph was modified as a result of this method.
		 */
		default boolean addEdge(Edge edge) {
			return addEdge(edge, e->{/*noop*/});
		}

		/**
		 * Adds the given edge to the graph.
		 *
		 * @param e some edge
		 * @param processor some edge processor to use directly after the edge is successfully 
		 * added to the graph.
		 * @return true, iff the graph was modified as a result of this method.
		 */
		boolean addEdge(Edge e, Consumer<Edge> processor);
	}

	GraphSession openSession(int batch, StatisticsCollector graphStats, StatisticsCollector extractStats,
			StatisticsCollector simStats);

	HashMap<NodeID, String> getDisambiguatedEntitiesNode();


	public Node getNodeFromCacheOrStorage(String sLabel);
	public void addToNodeCache(String oLabel, Node objectNode);
	public boolean isOnDisk(NodeID nodeID);
	public void setOnDisk(NodeID nodeID);
	public void addToDisambiguatedEntities (NodeID nodeID, String url);

	public ExtractionPolicy extractPolicy();

	public void countEdgesForSpecificity();

	/** sets the catalog for the graph and for the index */
	void setCatalog(DataSourceCatalog catalog);

	DataSourceCatalog getCatalog(); 
	

}

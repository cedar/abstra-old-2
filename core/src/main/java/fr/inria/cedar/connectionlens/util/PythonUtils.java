/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.util;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.FlairNERExtractor;

/**
 * @author Tayeb Merabti
 */
public class PythonUtils {

	public static final Logger log = Logger.getLogger(PythonUtils.class);

	private String script_location = Config.getInstance().getProperty("python_script_location");

	private PythonUtils() {
	}

	private static PythonUtils INSTANCE = null;
	private Process flairProcess = null;
	private Process smartExtractProcess = null; 
	private Process pdfScrapperProcess = null; 
	
	/** Point d'accès pour l'instance unique du singleton */
	public static synchronized PythonUtils getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PythonUtils();
		}
		return INSTANCE;
	}

	/**
	 * start Flask Flair Server
	 *
	 * @throws IOException
	 * @return
	 */
	public Process runFlairExtractor(String language) throws IOException {
		String pythonPath = Config.getInstance().getProperty("python_path");
		

		// hack IM 26/1/22:
		if (!Config.getInstance().getProperty("smart_extraction_profile").isEmpty()) {
			smartExtractProcess = runSmartExtractor();
		}
		
		String extractorBatchSizeFlair = Config.getInstance().getProperty("extractor_batch_size_flair");
		ProcessBuilder processBuilder;
		log.info("Python installation path: " + pythonPath);
		log.info("Running the script: " + Paths.get(script_location, "Flair_NER_tool", "flaskFlairNER.py").toString());

		if (language.equals("en")) {
			processBuilder = new ProcessBuilder(pythonPath,
					Paths.get(script_location, "Flair_NER_tool", "flaskFlairNER.py").toString(), "-l", "english", "-d",
					script_location, "-bs", extractorBatchSizeFlair);
			log.info("Starting english flairNER service...");
		} else {
			processBuilder = new ProcessBuilder(pythonPath,
					Paths.get(script_location, "Flair_NER_tool", "flaskFlairNER.py").toString(), "-l", "french", "-d",
					script_location, "-bs", extractorBatchSizeFlair);
			log.info("Starting french flairNER service...");
		}
		flairProcess = processBuilder.start();

		// Wait until the process start.
		// Choose max of 80 seconds.
		/*
		 * long timeoutExpiredMs = System.currentTimeMillis() + 150000; // max 2 minutes
		 * while(!FlairNERExtractor.isFlairInstalledAndRunning()){ long waitMillis =
		 * timeoutExpiredMs - System.currentTimeMillis(); if (waitMillis <= 0) {
		 * log.error("Time out: Flask fails to start."); System.exit(1); } }
		 */

		int sleepMs = 10000;
		while (sleepMs < 200000) {
			try {
				Thread.sleep(10000);
				// Thread.sleep(20000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (sleepMs % 10000 == 0) {
				if (FlairNERExtractor.isFlairInstalledAndRunning()) {
					return flairProcess;

				} else {
					sleepMs = sleepMs + 10000;
				}
			}

		}

		if (!FlairNERExtractor.isFlairInstalledAndRunning()) {
			log.error("Time out: Flask fails to start.");
			System.exit(1);
		}
		
		return flairProcess;
	}

	public Process runSmartExtractor() throws IOException {
		String pythonPath = Config.getInstance().getProperty("python_path");
		log.info("Python installation path: " + pythonPath);
		log.info("Running the script: "
				+ Paths.get(script_location, "smartExtract_module", "smartExtract.py").toString());
		ProcessBuilder processBuilder;
		processBuilder = new ProcessBuilder(pythonPath,
				Paths.get(script_location, "smartExtract_module", "smartExtract.py").toString());
		log.info("Starting smart extractor...");

		smartExtractProcess = processBuilder.start();

		int sleepMs = 10000;
		while (sleepMs < 50000) {
			try {
				System.out.println("Sleeping 10s");
				Thread.sleep(10000);
				// Thread.sleep(20000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (sleepMs % 10000 == 0) {
				if (smartExtractorIsRunning()) {
					System.out.println("Started smart extractor!");
					return smartExtractProcess;

				} else {
					System.out.println("Still no smart extractor, sleeping 10s more");
					sleepMs = sleepMs + 10000;
				}
			}

		}

		if (!smartExtractorIsRunning()) {
			log.error("Time out: smart extractor fails to start.");
			System.exit(1);
		}
		System.out.println("Started smart extractor!");
		return smartExtractProcess;
	}

	public static boolean smartExtractorIsRunning() {
		final Socket socket;
		try {
			URL url = new URL("http://127.0.0.1:4001/");
			// log.info(url);
			socket = new Socket(url.getHost(), url.getPort());
		} catch (IOException e) {
			ExceptionTools.prettyPrintException(e, 10); 
			return false;
		}
		try {
			socket.close();
		} catch (IOException e) {
		}
		return true;
	}

	/**
	 * Stop Flask Flair Server
	 *
	 * @throws IOException
	 */
	public void stopFlairExtractor() throws IOException {
		log.info("Stopping Flair extractor...");
		String pythonPath = Config.getInstance().getProperty("python_path");
		ProcessBuilder processBuilder;
		processBuilder = new ProcessBuilder(pythonPath,
				Paths.get(script_location, "Flair_NER_tool", "killFlaskProcess.py").toString());
		Process stopScriptProcess = processBuilder.start();
		try {
			while (FlairNERExtractor.isFlairInstalledAndRunning()) {
				Thread.sleep(100);
			}
			stopScriptProcess.destroy();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void stopSmartExtractor() throws IOException {
		log.info("Stopping Smart extractor...");
		String pythonPath = Config.getInstance().getProperty("python_path");
		ProcessBuilder processBuilder;
		processBuilder = new ProcessBuilder(pythonPath,
				Paths.get(script_location, "smartExtract_module", "killSmartExtract.py").toString());
		Process stopScriptProcess = processBuilder.start();
		try {
			while (FlairNERExtractor.isFlairInstalledAndRunning()) {
				Thread.sleep(100);
			}
			stopScriptProcess.destroy();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Run python PDF scrapper.  
	 * IM, 20012022: this used the same process as the Flask process, I've separated them.
	 * IM, 20012022: there no method here for stopping this process. 
	 * This may be because this process is called synchronously (waited-for in the caller).  
	 * @throws IOException
	 * @return
	 */
	public Process runPDFScrapper(String pdfFile) throws IOException {
		// log.info("Working Directory = " + System.getProperty("user.dir"));

		String pythonPath = Config.getInstance().getProperty("python_path");
		log.info("pythonPath = " + pythonPath);
		String flavor = Config.getInstance().getProperty("flavor");
		// String type_pdf = Config.getInstance().getProperty("type_pdf");
		log.info("PDF extractor-flavor value: " + flavor);
		// log.info("PDF extractor-type_pdf value: "+type_pdf);
		if (pdfFile.startsWith("file:")) {
			pdfFile = pdfFile.replaceFirst("file:", "");
		}
		String scrapdfFile = Paths.get(script_location, "pdf_scripts", "scrapdf.py").toString();
		// System.out.println("scrapdfFile=" + scrapdfFile);
		ProcessBuilder processBuilder = new ProcessBuilder(pythonPath, scrapdfFile, "--path_pdf", pdfFile, "--flavor",
				flavor);
		log.info("Starting scraping the file: " + pdfFile);
		pdfScrapperProcess = processBuilder.start();
		return pdfScrapperProcess;
	}
}

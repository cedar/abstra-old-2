package fr.inria.cedar.connectionlens.abstraction.collectionsidentification;

import java.util.ArrayList;

public interface PathAggregator {
    double aggregate(ArrayList<Path> pathsToAggregate, boolean workingGraph);
}

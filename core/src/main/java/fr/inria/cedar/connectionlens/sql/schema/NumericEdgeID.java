/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.sql.schema;

import fr.inria.cedar.connectionlens.graph.ItemID;
import fr.inria.cedar.connectionlens.graph.ItemID.EdgeID;

public class NumericEdgeID extends NumericItemID implements EdgeID {

	private static final long serialVersionUID = -5356743912784174403L;


	@Override
	public String toString() {
		return "NumericEdgeID(" + this.value.toString() + ")";
	}


	public NumericEdgeID(Integer v) {
		super(v);
	}


	/**
	 * @return the key of the reverse edge for the given key
	 */
	@Override
	public EdgeID reverse() {
		return new NumericEdgeID(-this.value());
	}

	@Override
	public Integer value() {
		return value;
	}

	@Override
	public int compareTo(ItemID id) {
		if (!getClass().isInstance(id)) {
			throw new IllegalStateException("Comparing incomparable objects"); 
		}
		NumericItemID i2 = (NumericItemID) id;
		if (this.value == i2.value()) {
			return 0;
		}
		else {
			return this.value.compareTo(i2.value); 
		}
	}
}

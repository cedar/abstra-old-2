/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionlens.extraction;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import fr.inria.cedar.connectionlens.Config;
import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.nio.file.Paths;

/**
 * TreeTagger implementation of MorphoSyntacticAnalyser.
 */
public class TreeTagger implements MorphoSyntacticAnalyser {
	
	/** A logger */
	static Logger log = Logger.getLogger(TreeTagger.class);

	/** A wrapper to call TreeTagger with Java */
	private TreeTaggerWrapper<String> tagger;

	/** The language model. */
	private final String languageModel;

	/** The potential entity POSs. */
	private final Set<String> potentialEntityPOSs;
	
	/** The indexable POSs. */
	final Set<String> indexablePOSs;
	
	final Locale locale;

	/**
	 * Instantiates a new tree tagger.
	 *
	 * @param locale the locale
	 */
	public TreeTagger(Locale locale) {
		this(Paths.get(Config.getInstance().getStringProperty("treetagger_home")).toString(), locale);

	}
	
	/**
	 * Instantiates a new tree tagger.
	 *
	 * @param pathToTreeTagger the installation location
	 * @param locale the locale
	 */
	public TreeTagger(String pathToTreeTagger, Locale locale) {
		this.locale = locale;
		ResourceBundle bundle = ResourceBundle.getBundle("treetagger",  locale);
		this.languageModel = bundle.getString("model_location");
		this.potentialEntityPOSs = ImmutableSet.copyOf(bundle.getString("entity_pos").split(","));
		this.indexablePOSs = ImmutableSet.copyOf(bundle.getString("indexable_pos").split(","));
		System.setProperty("treetagger.home", pathToTreeTagger);
		this.tagger = new TreeTaggerWrapper<>();
		try {
			this.tagger.setModel(this.languageModel);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public Set<String> potentialEntityPOS() {
		return this.potentialEntityPOSs;
	}

	@Override
	public boolean isIndexablePOS(PartOfSpeech pos) {
		return indexablePOSs.contains(pos.getTag());
	}

    @Override
    public Locale getLocale() {
	    return locale;
    }

    /**
	 * Process a string (from a keyword) with TreeTagger in a way that: 
	 * the entire string is tokenized, tokens of type "NAM", "NOM", "ABR" are kept 
	 * @param input the string to process
	 */
	@Override
	public Multimap<String, PartOfSpeech> process(String input) {
		// setup TreeTagger
		LinkedListMultimap<String, PartOfSpeech> result = LinkedListMultimap.create();
		org.apache.commons.lang3.mutable.MutableInt offset = new org.apache.commons.lang3.mutable.MutableInt(0);
		tagger.setHandler(new TokenHandler<String>() {
			@Override
			public void token(String token, String pos, String lemma) {
				// only consider NAM, NOM, ABR
				if (indexablePOSs.contains(pos)) {
					String[] lemmas = lemma.split("\\|");
					int os = input.indexOf(token, offset.intValue());
					result.get(pos).add(new PartOfSpeech(
							lemmas.length > 1 ? lemmas[0]: lemma,
							token.replaceAll("[^a-zA-Z0-9'_\u00C0-\u017F\\-\\s]", ""),
							pos, os));
					offset.setValue(os);
					return;
				}
			}
		});
		
		// split the text by space or punctuations & invoke TreeTagger
		String[] tokens = input.split(" |\\, |\\. |\\... |\\? |\\! |\\.|\\...|\\?|\\!");
		try {
			tagger.process(tokens);
		} catch (IOException | TreeTaggerException e) {
			log.error("Cannot invoke TreeTagger on this text!");
			e.printStackTrace();
		}
		return result;
	}	

	public Set<String> getLemma(String input) {
		Set<String> result = new HashSet<>();
		tagger.setHandler(new TokenHandler<String>() {
			@Override
			public void token(String token, String pos, String lemma) {
				// only consider NAM, NOM, ABR
				String[] lemmas = lemma.split("\\|");
				for (String l:lemmas){
					result.add(l);	
				}
				return;
				
			}
		});
		
		// split the text by space or punctuations & invoke TreeTagger
		String[] tokens = input.split(" |\\, |\\. |\\... |\\? |\\! |\\.|\\...|\\?|\\!");
		try {
			tagger.process(tokens);
		} catch (IOException | TreeTaggerException e) {
			log.error("Cannot invoke TreeTagger on this text!");
			e.printStackTrace();
		}
		return result;
	}
}

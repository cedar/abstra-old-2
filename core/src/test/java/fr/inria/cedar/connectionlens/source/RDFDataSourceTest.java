/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_LITERAL;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;

public class RDFDataSourceTest extends ConnectionLensTest {

	static EntityExtractor extractor;
	static ConnectionLens cl;
	static MorphoSyntacticAnalyser analyzer;
	static SimilarPairProcessor pp;

	@BeforeClass
	public static void setupClass() {
		extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
		analyzer = new TreeTagger(FRENCH);
		pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	}

	@Test
	public void testAtomicity() throws SQLException {
		log.info("testAtomicity()");
		setUpDB(); 
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		Config.getInstance().setProperty("value_atomicity", "PER_GRAPH");
		DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_atomicity_1.nt").toString());
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_atomicity_2.nt").toString());
		assertEquals(1, cl.graph().getNodes("http://inria.fr/book").size());
		assertEquals(1, cl.graph().getNodes("\"Author2\"^^<http://www.w3.org/2001/XMLSchema#string>").size());
		
		cl.close();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		Config.getInstance().setProperty("value_atomicity", "PER_DATASET");
		DataSource ds3 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_atomicity_1.nt").toString());
		DataSource ds4 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_atomicity_2.nt").toString());
		assertEquals(2, cl.graph().getNodes("http://inria.fr/book").size());
		assertEquals(2, cl.graph().getNodes("\"Author2\"^^<http://www.w3.org/2001/XMLSchema#string>").size());
		cl.close(); 
		tearDownDB(); 
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");

	}
	
	@Test
	public void testNewFlow() throws SQLException {		
		log.info("testNewFlow()");
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		RDFDataSource ds = (RDFDataSource) cl.register(Paths.get("data", "generated", "generated-100.nt").toString(), null);
		assertEquals(101, cl.graph().countEdges(ds, EDGE));
		assertEquals(0, cl.graph().countEdges(ds, EXTRACTION_EDGE));
		assertEquals(185, cl.graph().countNodes(ds));
		cl.close();
		tearDownDB();	
	}

	@Ignore // TODO: see if this test brings something or not, align connection management
	public void testRegisterTwoDatasets() throws SQLException {
		Config.getInstance().setProperty("update_batch_size", "1");
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString(), null);
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString(), null);
		Node n1 = ds1.buildLabelNodeOfType("http://inria.fr/book", RDF_URI);
		Node n2 = ds1.buildLabelNodeOfType("Sartwell, Crispin", RDF_LITERAL);
		Edge e1 = n1.buildEdge(n2, "http://dbpedia.org/property/author");

		Node m1 = ds2.buildLabelNodeOfType("http://inria.fr/book", RDF_URI);
		Node m2 = ds2.buildLabelNodeOfType("Sartwell, Crispin", RDF_LITERAL);
		Node m3 = ds2.buildLabelNodeOfType("http://inria.fr/doi2", RDF_URI);
		Edge f1 = m1.buildEdge(m2, "http://dbpedia.org/property/author");
		Edge f2 = m1.buildEdge(m3, "http://dbpedia.org/property/url");

		assertTrue(cl.graph().getNodes(ds1).containsAll(Sets.newHashSet(n1, n2)));
		assertFalse(cl.graph().getNodes(ds1).contains(Sets.newHashSet(m1)));
		assertTrue(cl.graph().getEdges(ds1).containsAll(Sets.newHashSet(e1)));
		assertFalse(cl.graph().getEdges(ds1).contains(Sets.newHashSet(f1)));
		assertEquals(Sets.newHashSet(n2), cl.graph().getNodes(ds1, "Sartwell, Crispin"));
		assertEquals(Sets.newHashSet(), cl.graph().getNodes(ds1, "http://inria.fr/doi2"));
		assertTrue(cl.graph().getEdges(ds1, "http://dbpedia.org/property/author").contains(e1));
		assertEquals(Sets.newHashSet(), cl.graph().getEdges(ds1, "http://dbpedia.org/property/url"));
		assertEquals(Sets.newHashSet(n2, m2), cl.graph().getNodes("Sartwell, Crispin"));
		assertEquals(Sets.newHashSet(m3), cl.graph().getNodes("http://inria.fr/doi2"));
		assertEquals(Sets.newHashSet(), cl.graph().getNodes("Jean-Paul Sartre"));
		assertEquals(Sets.newHashSet(), cl.graph().getEdges("nonsense"));
		assertEquals(12, cl.graph().countNodes(ds1));
		assertEquals(5, cl.graph().countEdges(ds1, EDGE));
		assertEquals(0, cl.graph().countEdges(ds1, EXTRACTION_EDGE));
		assertEquals(18, cl.graph().countNodes(ds2));
		assertEquals(14, cl.graph().countEdges(ds2, EDGE));
		assertEquals(0, cl.graph().countEdges(ds2, EXTRACTION_EDGE));
		cl.close();
		tearDownDB(); 
	}
	
}

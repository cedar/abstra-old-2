/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static fr.inria.cedar.connectionlens.graph.Node.Types.TEXT_VALUE;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.LUCENE;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static fr.inria.cedar.connectionlens.source.DataSource.ValueAtomicities.PER_INSTANCE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.PartOfSpeech;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class LuceneBasedIndexTest extends ConnectionLensTest {

	String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();
	String path3 = Paths.get("file:data", "text", "input", "no_entity.txt").toString();
	String n1ID = "1|1|offset542|length16|ENTITY_PERSON";
	String m1ID = "2|2|offset837|length16|ENTITY_PERSON";
	String m2ID = "2|2|offset3156|length6|ENTITY_LOCATION";

	SimilarPairProcessor sim = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	EntityExtractor dummyExtractor = new TopLevelExtractor();
	EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	MorphoSyntacticAnalyser dummyAnalyzer = Mockito.mock(MorphoSyntacticAnalyser.class);

	{
		String mockPos = "MIAM";
		when(dummyAnalyzer.potentialEntityPOS()).thenReturn(ImmutableSet.of(mockPos));
		when(dummyAnalyzer.process(Matchers.anyString())).thenAnswer(new Answer<Multimap<String, PartOfSpeech>>() {
			@Override
			public Multimap<String, PartOfSpeech> answer(InvocationOnMock invocation) throws Throwable {
				String[] tokens = ((String) invocation.getArguments()[0])
						.split(" |\\, |\\. |\\... |\\? |\\! |\\.|\\...|\\?|\\!");
				Multimap<String, PartOfSpeech> result = LinkedHashMultimap.create();
				int offset = 0;
				for (String token : tokens) {
					result.put(mockPos, new PartOfSpeech(token, token, mockPos, offset));
					offset += token.length() + 1;
				}
				return result;
			}
		});
	}
	
	public LuceneBasedIndexTest() {
		Config.getInstance().setProperty("update_batch_size", "10");
		Config.getInstance().setProperty("value_atomicity", PER_INSTANCE.name());
		Config.getInstance().setProperty("indexing_model", LUCENE.name());
	}

	@Test
	public void textIndexEntities() throws SQLException {
		setUpDB();
		try (ConnectionLens cl = new ConnectionLens(extractor, analyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "json_04.json").toString());
			IndexAndProcessNodes index = cl.graph().index();
			log.info("Index class is: " + index.getClass().getName()); 
			assertEquals(2, index.getNodeMatches(new AtomicKeyword("paris")).size());
			assertEquals(1, filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("paris"))).size());
			assertEquals(2, index.getNodeMatches(new AtomicKeyword("londres")).size());
			assertEquals(1, filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("londres"))).size());
			assertEquals(index.getNodeMatches(new AtomicKeyword("rencontre")),
					index.getNodeMatches(new AtomicKeyword("l'ombre")));
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
		tearDownDB();
	}

	@Test
	public void testRegisterTwoJSONDatasets() throws SQLException {
		setUpDB();
		try (ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());
	
			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(2, filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("sartre"))).size());
			assertEquals(1, index.getEdgeMatches(new AtomicKeyword("age")).size());
			assertEquals(2, index.getEdgeMatches(new AtomicKeyword("auteur")).size());
			assertEquals(2, filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("sartre"))).size());
			assertEquals(1, index.getItemMatches(new AtomicKeyword("age")).size());
			assertEquals(2, index.getItemMatches(new AtomicKeyword("auteur")).size());
			assertEquals(0, filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("claude"))).size());
			assertEquals(0, index.getEdgeMatches(new AtomicKeyword("size")).size());
			assertEquals(0, index.getItemMatches(new AtomicKeyword("size")).size());
			assertEquals(0, index.getDataSourceMatches(new AtomicKeyword("size")).size());
			assertEquals(2, index.getDataSourceMatches(new AtomicKeyword("sartre")).size());
			assertEquals(1, index.getDataSourceMatches(new AtomicKeyword("age")).size());
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
		tearDownDB();
	}

	@Test
	public void testRegisterTwoJSONDatasetsQueryComponents() throws SQLException {
		setUpDB();
		try (ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());
		
			//(new GraphPrintingByRepresentative()).printAllSources(cl);

			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(1, index.getNodeMatches(Conjunction.of("huis", "clos")).size());
			assertEquals(1, index.getNodeMatches(Conjunction.of("clos", "huis")).size());
			assertEquals(2,
					filterNonEntityMatches(index.getNodeMatches(Conjunction.of("jean-paul", "sartre"))).size());
			assertEquals(0, index.getNodeMatches(Conjunction.of("clos", "sartre")).size());
			assertEquals(0, index.getNodeMatches(NGram.of("clos", "huis")).size());
			assertEquals(1, index.getNodeMatches(NGram.of("huis", "clos")).size());
			assertEquals(0, index.getNodeMatches(NGram.of("clos", "sartre")).size());
			assertEquals(1, index.getNodeMatches(Disjunction.of("huis", "clos")).size());
			assertEquals(3,
					filterNonEntityMatches(index.getNodeMatches(Disjunction.of("huis", "sartre"))).size());
			assertEquals(2,
					filterNonEntityMatches(index.getNodeMatches(Disjunction.of("lion", "sartre"))).size());
			assertEquals(0, index.getNodeMatches(Disjunction.of("lion", "merle")).size());

			assertEquals(1, index.getItemMatches(Conjunction.of("huis", "clos")).size());
			assertEquals(1, index.getItemMatches(Conjunction.of("clos", "huis")).size());
			assertEquals(0, index.getItemMatches(Conjunction.of("clos", "auteur")).size());
			assertEquals(0, index.getItemMatches(NGram.of("clos", "huis")).size());
			assertEquals(1, index.getItemMatches(NGram.of("huis", "clos")).size());
			assertEquals(0, index.getItemMatches(NGram.of("clos", "auteur")).size());
			assertEquals(1, index.getItemMatches(Disjunction.of("huis", "clos")).size());

			assertEquals(0, index.getItemMatches(Disjunction.of("lion", "merle")).size());

			assertEquals(1, index.getItemMatches(ds2, Conjunction.of("huis", "clos")).size());
			assertEquals(1, index.getItemMatches(ds2, Conjunction.of("clos", "huis")).size());
			assertEquals(0, index.getItemMatches(ds1, Conjunction.of("clos", "huis")).size());
			assertEquals(0, index.getItemMatches(ds2, Conjunction.of("clos", "auteur")).size());
			assertEquals(0, index.getItemMatches(ds2, NGram.of("clos", "huis")).size());
			assertEquals(1, index.getItemMatches(ds2, NGram.of("huis", "clos")).size());
			assertEquals(0, index.getItemMatches(ds1, NGram.of("huis", "clos")).size());
			assertEquals(0, index.getItemMatches(ds2, NGram.of("clos", "auteur")).size());
			assertEquals(1, index.getItemMatches(ds2, Disjunction.of("huis", "clos")).size());
			assertEquals(0, index.getItemMatches(ds1, Disjunction.of("huis", "clos")).size());
			assertEquals(0, index.getItemMatches(ds2, Disjunction.of("lion", "merle")).size());


			assertEquals(0, index.getEdgeMatches(Disjunction.of("lion", "merle")).size());

			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(Conjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(ds1, ds2), index.getDataSourceMatches(Disjunction.of("age", "auteur")));
			assertEquals(Sets.newHashSet(ds1), index.getDataSourceMatches(Disjunction.of("age", "lion")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(Disjunction.of("lion", "merle")));


			assertEquals(3, index.getItemMatches(Disjunction.of("huis", "auteur")).size());
			assertEquals(2, index.getItemMatches(Disjunction.of("lion", "auteur")).size());
			assertEquals(3, index.getEdgeMatches(Disjunction.of("age", "auteur")).size());
			assertEquals(1, index.getEdgeMatches(Disjunction.of("age", "lion")).size());

			assertEquals(2, index.getItemMatches(ds2, Disjunction.of("huis", "auteur")).size());
			assertEquals(1, index.getItemMatches(ds2, Disjunction.of("lion", "auteur")).size());
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
		tearDownDB();
	}

	@Test
	public void testRegisterTwoRDFDatasets() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("update_batch_size", "10");
		try (ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
			DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

			Node n1 = cl.graph().resolveNode (new NumericNodeID(5));
			Node n2 = cl.graph().resolveNode (new NumericNodeID(6));
			Edge e1 = cl.graph().resolveEdge (new NumericEdgeID(7));


			Node m1 = cl.graph().resolveNode (new NumericNodeID(19));
			Node m2 = cl.graph().resolveNode (new NumericNodeID(20));

			Edge f1 = cl.graph().resolveEdge (new NumericEdgeID(21)); 

			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getNodeMatches(new AtomicKeyword("sartwell")));
			assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("http://inria.fr/book"))
					.containsAll(Sets.newHashSet(n1, m1)));
			assertTrue(cl.graph().index().getEdgeMatches(new AtomicKeyword("http://dbpedia.org/property/author"))
					.containsAll(Sets.newHashSet(e1, f1)));
		
			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getItemMatches(new AtomicKeyword("sartwell")));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("http://inria.fr/book"))
					.containsAll(Sets.newHashSet(n1, m1)));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("http://dbpedia.org/property/author"))
					.containsAll(Sets.newHashSet(e1, f1)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("claude")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("sartwell")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("pr")));
		
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
		tearDownDB();
	}

	@Test
	public void testRegisterTwoRelationalDatasets() throws SQLException {
		setUpDB("schema1", "schema2");
		Config.getInstance().setProperty("update_batch_size", "10");
		try (ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
					Statement s1 = c1.createStatement()) {
				s1.executeUpdate("CREATE TABLE schema1.test1      (name TEXT,         a TEXT)");
				s1.executeUpdate("INSERT INTO schema1.test1 VALUES('foo',             'bar')");
				s1.executeUpdate("INSERT INTO schema1.test1 VALUES('Emmanuel Macron', 'doe')");
				
				s1.executeUpdate("CREATE TABLE schema2.test2      (b CHAR, name TEXT)");
				s1.executeUpdate("INSERT INTO schema2.test2 VALUES(1,      'Emmanuel Macron')");
				s1.executeUpdate("INSERT INTO schema2.test2 VALUES(2,      'Emmanuel Macron')");
			}
			try (RelationalDataSource ds1 = (RelationalDataSource) cl
					.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword);
					RelationalDataSource ds2 = (RelationalDataSource) cl.register(
							"jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword)) {

				//(new GraphPrintingByRepresentative()).printAllSources(cl);

				Node n2 = cl.graph().resolveNode (new NumericNodeID(18)); // Macron node
				Node m3 = cl.graph().resolveNode (new NumericNodeID(37)); // Macron node 
				
				Edge e2 = cl.graph().resolveEdge (new NumericEdgeID(19)); // name edge
				Edge f2 = cl.graph().resolveEdge (new NumericEdgeID(38)); // name edge

				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getEdgeMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("macron")));
				assertEquals(Sets.newHashSet(ds1), cl.graph().index().getDataSourceMatches(new AtomicKeyword("doe")));
			}
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
		tearDownDB();
	}

	@Ignore
	public void testRegisterTwoTextDatasets() throws SQLException {
		Config.getInstance().setProperty("update_batch_size", "1");
		setUpDB();
		try (ConnectionLens cl = new ConnectionLens(extractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(path1);
			DataSource ds2 = cl.register(path2);

			String textp1 = IOUtils.slurpURL(path1);
			String textp2 = IOUtils.slurpURL(path2);

			Node ntx1 = ds1.buildLabelNodeOfType(textp1, TEXT_VALUE);

			Node mtx1 = ds2.buildLabelNodeOfType(textp2, TEXT_VALUE);
			Properties p1 = new Properties(), p2 = new Properties(), p3 = new Properties();
			p1.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "542");
			p1.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node n1 = ds1.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p1);
			p2.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "837");
			p2.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node m1 = ds2.buildChildNode(mtx1.getId(), "Edouard Philippe", ENTITY_PERSON, p2);
			p3.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "3156");
			p3.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "6");
			Node m2 = ds2.buildChildNode(mtx1.getId(), "Nantes", ENTITY_LOCATION, p3);

			if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_OCCURRENCE")){
				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1, m1)));
				assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("edouard")));
			}
			if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_GRAPH")){
				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1)));
				assertEquals(Sets.newHashSet(ds1), cl.graph().index().getDataSourceMatches(new AtomicKeyword("edouard")));
			}

			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("")));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("nantes")).containsAll(Sets.newHashSet(m2)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nantes")));
			cl.close();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}

		tearDownDB();
	}

	@AfterClass
	public static void tearDown() {
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		log.info("Restored " + POSTGRES_FULLTEXT.name() + " indexing"); 
		
	}
}
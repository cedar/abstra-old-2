/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.*;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;


import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Arrays.asList;
import static java.util.Locale.FRENCH;

public class BufferedGraphUpdateTest extends ConnectionLensTest {

	static StatisticsCollector stats = StatisticsCollector.mute();
	static EntityExtractor dummyExtractor = new TopLevelExtractor();
	static EntityExtractor cacheExtractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	static MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	
	// two hard-coded URIs for relational data sources
	final String db1 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password="
			+ defaultPassword;
	final String db2 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password="
			+ defaultPassword;
	final String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	final String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();

	public static ConnectionLens initWithDB(String dbName, int batchSize) throws SQLException {
		String standardDBName = Config.getInstance().getProperty("RDBMS_DBName");
		Config.getInstance().setProperty("RDBMS_DBName", dbName);
		Config.getInstance().setProperty("update_batch_size", String.valueOf(batchSize));
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		ConnectionLens cl =  new ConnectionLens(cacheExtractor, analyzer, pp, true);
		Config.getInstance().setProperty("RDBMS_DBName", standardDBName);
		return cl; 
	}
	

	@Test
	public void testBufferedRegisterTwoJSONDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString(), null);
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example2.json").toString(), null);
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString(), null);
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example2.json").toString(), null);

		sameGraph(cl1, cl2, true);

		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");

	}

	@Test
	public void testBufferedRegisterTwoDatasetsSpecificity() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example2.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example2.json").toString());

		sameGraph(cl1, cl2, true);

		cl1.processSimilarities(Arrays.asList(ds11, ds12));
		cl2.processSimilarities(Arrays.asList(ds21, ds22));

		sameGraph(cl1, cl2, true);

		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");

	}

	@Test
	public void testBufferedRegisterTwoRDFDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);

		DataSource ds11 = cl1.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

		sameGraph(cl1, cl2, true);

		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}

	@Test
	public void testBufferedRegisterTwoRelationalDatasets() throws SQLException, IOException {
		try {
			setUpDB("schema1", "schema2"); 
			// load relational data into junit.schema1 and junit.schema2
			Connection cRds = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword,
					"");
			Statement s1 = cRds.createStatement();
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");

			// load the two relational databases in each of two ConnectionLens instances,
			// with buffer size 1 respectively 10
			ConnectionLens cl1 = initWithDB("junit1", 1);
			RelationalDataSource ds11 = (RelationalDataSource) cl1.register(db1);
			RelationalDataSource ds12 = (RelationalDataSource) cl1.register(db2);
			
			ConnectionLens cl2 = initWithDB("junit2", 10); //new ConnectionLens(dummyExtractor, analyzer, pp, true);
			RelationalDataSource ds21 = (RelationalDataSource) cl2.register(db1);
			RelationalDataSource ds22 = (RelationalDataSource) cl2.register(db2);

			sameGraph(cl1, cl2, true);

			cl1.close();
			cl2.close();
			Config.getInstance().setProperty("RDBMS.DBName", "junit");
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		tearDownDB(); // the junit DB
		tearDownDB("junit1", "junit2"); 
	}

	@Test
	public void testBufferedRegisterTwoTextDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(path1);
		DataSource ds12 = cl1.register(path2);
		
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(path1);
		DataSource ds22 = cl2.register(path2);
		sameGraph(cl1, cl2, true);
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}

	@Test
	public void testBufferedRegisterTwoJSONDatasetsSameAs() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example3.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example5.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example3.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example5.json").toString());

		sameGraph(cl1, cl2, true);

		Config.getInstance().setProperty("update_batch_size", "1");
		cl1.processSimilarities(asList(ds11, ds12));
		Config.getInstance().setProperty("update_batch_size", "10");
		cl2.processSimilarities(asList(ds21, ds22));

		sameGraph(cl1, cl2, true);

		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}

	@Test
	@Ignore // Tayeb: batch=1 update specificity miss (erase the specificity for some
			// relations) Ioana 7/3/2021: en effet
	public void testBufferedRegisterTwoRDFDatasetsSameAs() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds22 = cl2.register(Paths.get("data" , "rdf", "rdf_02", "test_02.nt").toString());

		sameGraph(cl1, cl2, true);

		Config.getInstance().setProperty("update_batch_size", "1");
		cl1.processSimilarities(asList(ds11, ds12));

		Config.getInstance().setProperty("update_batch_size", "10");
		cl2.processSimilarities(asList(ds21, ds22));

		sameGraph(cl1, cl2, true);

	}

	@Test
	public void testBufferedRegisterTwoRelationalDatasetsSameAs() throws SQLException, IOException {
		try {
			setUpDB("schema1", "schema2"); // db: junit
			Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword,
					"");
			Statement s1 = c1.createStatement();
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");

			ConnectionLens cl1 = initWithDB("junit1", 1); // db: junit1
			ConnectionLens cl2 = initWithDB("junit2", 10); // db: junit2
			RelationalDataSource ds11 = (RelationalDataSource) cl1.register(db1);
			RelationalDataSource ds12 = (RelationalDataSource) cl1.register(db2);
			RelationalDataSource ds21 = (RelationalDataSource) cl2.register(db1);
			RelationalDataSource ds22 = (RelationalDataSource) cl2.register(db2);

			sameGraph(cl1, cl2, true);

			Config.getInstance().setProperty("update_batch_size", "1");
			cl1.processSimilarities(asList(ds11, ds12));
			Config.getInstance().setProperty("update_batch_size", "10");
			cl2.processSimilarities(asList(ds21, ds22));

			sameGraph(cl1, cl2, true);
			cl1.close();
			cl2.close();
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		tearDownDB(); // db: junit
		tearDownDB("junit1", "junit2");
	}

	@Test
	public void testBufferedRegisterTwoTextDatasetsSameAs() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(path1);
		DataSource ds12 = cl1.register(path2);
		(new GraphPrintingByRepresentative()).printAllSources(cl1); 
		
		ConnectionLens cl2 = initWithDB("junit2", 10);
		DataSource ds21 = cl2.register(path1);
		DataSource ds22 = cl2.register(path2);
		
		sameGraph(cl1, cl2, true);

		Config.getInstance().setProperty("update_batch_size", "1");
		cl1.processSimilarities(asList(ds11, ds12));
		Config.getInstance().setProperty("update_batch_size", "10");
		cl2.processSimilarities(asList(ds21, ds22));

		(new GraphPrintingByRepresentative()).printAllSources(cl1); 
		(new GraphPrintingByRepresentative()).printAllSources(cl2); 
		
		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB();
		tearDownDB("junit1", "junit2");
	}
	

	@AfterClass
	public static void restoreDefaults() {
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH"); 
	}
}

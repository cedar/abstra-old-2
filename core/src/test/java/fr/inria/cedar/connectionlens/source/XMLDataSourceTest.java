/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;

import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

/**
 * @author Ioana Manolescu, Tayeb Merabti
 */
public class XMLDataSourceTest extends ConnectionLensTest {

	EntityExtractor extractor = new TopLevelExtractor();
	MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	
	@Test
	public void testAdjacentEdges() throws SQLException {
		setUpDB();
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		//Config.getInstance().setProperty("drawing.draw", "true");

		DataSource ds = cl.register(Paths.get("data", "xml", "example1.xml").toString(), "");

		(new GraphPrintingByRepresentative()).printAllSources(cl);

		Node n1 = cl.graph().resolveNode(new NumericNodeID(4));
		Set<Edge> adj = cl.graph().getAdjacentEdges(n1);
		Edge e0 = cl.graph().resolveEdge(new NumericEdgeID(10));
		Edge e1 = cl.graph().resolveEdge(new NumericEdgeID(36));
		Edge e2 = cl.graph().resolveEdge(new NumericEdgeID(40));

		assertTrue(adj.contains(e0));
		assertTrue(adj.contains(e1));
		assertTrue(adj.contains(e2));
		assertEquals(9, adj.size());
		cl.close(); 
		tearDownDB();
	}
	
	/** Checks that XML attribute are correctly modeled 
	 * @throws SQLException */
	@Test
	public void testAttributes() throws SQLException {
		//log.info("XML attributes test"); 
		setUpDB();
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		Config.getInstance().setProperty("extractor", "NONE");
		cl.register(Paths.get("data", "xml", "example3.xml").toString());
		// we do have 3 (attribute) nodes labeled alpha
		Collection<Node> alphas = cl.graph().getNodes("alpha"); 
		assertEquals(3, alphas.size());
		for (Node nAlpha: alphas) {
			assertEquals(1, cl.graph().getIncomingEdges(nAlpha).size()); 
			for (Edge e: cl.graph().getIncomingEdges(nAlpha)) {
				assertEquals("rating", e.getLabel()); 
			}
		}
		cl.close(); 
		tearDownDB();
	}

	@Test
	public void testAtomicity1() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_PATH");
		Config.getInstance().setProperty("extractor", "NONE");
		
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		cl.register(Paths.get("data", "xml", "atomicity_xml_1.xml").toString());
		cl.register(Paths.get("data", "xml", "atomicity_xml_2.xml").toString());

		//(new GraphPrintingByRepresentative()).printAllSources(cl);
		
		assertEquals(2, cl.graph().getNodes("Macron").size());
		assertEquals(1, cl.graph().getNodes("2").size());
		cl.close(); 
		tearDownDB();
	}
	@Test
	public void testAtomicity2() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_DATASET");
		Config.getInstance().setProperty("extractor", "NONE");
		
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
	
		DataSource ds1 = cl.register(Paths.get("data", "xml", "atomicity_xml_1.xml").toString());
		DataSource ds2 = cl.register(Paths.get("data", "xml", "atomicity_xml_2.xml").toString());

		assertEquals(2, cl.graph().getNodes("Macron").size());
		assertEquals(1, cl.graph().getNodes(ds1, "Macron").size());
		assertEquals(1, cl.graph().getNodes(ds2, "Macron").size());
		cl.close(); 
		tearDownDB();
	}
	@Test
	public void testAtomicity3() throws SQLException {
		
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_GRAPH");
		Config.getInstance().setProperty("extractor", "NONE");
	
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource dsg1 = cl.register(Paths.get("data", "xml", "atomicity_xml_1.xml").toString());
		DataSource dsg2 = cl.register(Paths.get("data", "xml", "atomicity_xml_2.xml").toString());
		//System.out.println("DS1: " + cl.graph().getEdges(dsg1));
		//System.out.println("DS2: " + cl.graph().getEdges(dsg2)); 
		//(new GraphPrintingByRepresentative()).printAllSources(cl);
		assertEquals(1, cl.graph().getNodes("Macron").size());
		assertEquals(1, cl.graph().getNodes("Poutine").size());
		cl.close(); 
		tearDownDB();
	}
	@Test
	public void testAtomicity4() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("extractor", "NONE");
	
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "xml", "atomicity_xml_1.xml").toString());
		cl.register(Paths.get("data", "xml", "atomicity_xml_2.xml").toString());
		assertEquals(3, cl.graph().getNodes("Macron").size());
		assertEquals(3, cl.graph().getNodes("Poutine").size());
		cl.close(); 
		tearDownDB();
	}

}

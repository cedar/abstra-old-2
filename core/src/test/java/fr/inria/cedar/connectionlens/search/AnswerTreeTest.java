/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.util.StatisticsCollector.mute;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Item;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.JaroPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.source.DataSource;

public class AnswerTreeTest extends ConnectionLensTest {

	@Test
	public void testEquality() throws SQLException {
		setUpDB();
		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		EntityExtractor extractor = new TopLevelExtractor();
		SimilarPairProcessor pp = new JaroPairProcessor(mute(), 0.);
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString(), "");

		HashSet<Integer> components1 = new HashSet<Integer>();
		components1.add(new AtomicKeyword("x").hashCode());
		AnswerTree at1 = new AnswerTree(ds1.getRoot(), components1);
		HashSet<Integer> components2 = new HashSet<Integer>();
		components2.add(new AtomicKeyword("x").hashCode());
		AnswerTree at2 = new AnswerTree(ds1.getRoot(), components2);
		HashSet<Integer> components3 = new HashSet<Integer>();
		components3.add(new AtomicKeyword("y").hashCode());
		AnswerTree at3 = new AnswerTree(ds1.getRoot(), components3);

		assertEquals(at1, at1);
		assertEquals(at1, at2);
		assertNotEquals(at1, at3);

		AtomicInteger count = new AtomicInteger(0);
		Random random = new Random(0);
		SetMultimap<Integer, Item> matches = HashMultimap.create();
		Set<Node> nodes = new LinkedHashSet<>();
		Set<Edge> edges = new LinkedHashSet<>();
		for (Node n : cl.graph().getNodes(ds1)) {
			if (random.nextBoolean()) {
				nodes.add(n);
				if (random.nextBoolean()) {
					matches.put(new AtomicKeyword(String.valueOf(count.getAndIncrement())).hashCode(), n);
				}
			}
		}
		for (Edge e : cl.graph().getEdges(ds1)) {
			if (random.nextBoolean()) {
				edges.add(e);
				if (random.nextBoolean()) {
					matches.put(new AtomicKeyword(String.valueOf(count.getAndIncrement())).hashCode(), e);
				}
			}
		}
		AnswerTree at4 = new AnswerTree(ds1.getRoot(), nodes, edges,  matches);
		AnswerTree at5 = new AnswerTree(ds1.getRoot(), nodes, edges,  matches);
		// TODO: the empty hashsets are the matches of the root node.  
		assertEquals(at4, at4);
		assertEquals(at4, at5);
		cl.close();
		tearDownDB();
	}

	@Test
	public void testHashCode() throws SQLException {
		setUpDB();
		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		EntityExtractor extractor = new TopLevelExtractor();
		SimilarPairProcessor pp = new JaroPairProcessor(mute(), 0.);
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString(), "");
		HashSet<Integer> components1 = new HashSet<Integer>();
		components1.add(new AtomicKeyword("x").hashCode());
		AnswerTree at1 = new AnswerTree(ds1.getRoot(), components1);
		HashSet<Integer> components2 = new HashSet<Integer>();
		components2.add(new AtomicKeyword("x").hashCode());
		AnswerTree at2 = new AnswerTree(ds1.getRoot(), components2);
		HashSet<Integer> components3 = new HashSet<Integer>();
		components3.add(new AtomicKeyword("y").hashCode());
		AnswerTree at3 = new AnswerTree(ds1.getRoot(), components3);

		assertEquals(at1.hashCode(), at1.hashCode());
		assertEquals(at1.hashCode(), at2.hashCode());
		assertNotEquals(at1.hashCode(), at3.hashCode());

		AtomicInteger count = new AtomicInteger(0);
		Random random = new Random(0);
		SetMultimap<Integer, Item> matches = HashMultimap.create();
		Set<Node> nodes = new LinkedHashSet<>();
		Set<Edge> edges = new LinkedHashSet<>();
		Set<Edge> redges = new LinkedHashSet<>();
		for (Node n : cl.graph().getNodes(ds1)) {
			if (random.nextBoolean()) {
				nodes.add(n);
				if (random.nextBoolean()) {
					matches.put(new AtomicKeyword(String.valueOf(count.getAndIncrement())).hashCode(), n);
				}
			}
		}
		for (Edge e : cl.graph().getEdges(ds1)) {
			// log.info(e.getId()+"--"+e.reverse());
			if (random.nextBoolean()) {
				edges.add(e);
				redges.add(e.reverse());
				if (random.nextBoolean()) {
					matches.put(new AtomicKeyword(String.valueOf(count.getAndIncrement())).hashCode(), e);
				}
			}
		}
		AnswerTree at4 = new AnswerTree(ds1.getRoot(), nodes, edges, matches);
		AnswerTree at5 = new AnswerTree(ds1.getRoot(), nodes, edges, matches);
		AnswerTree at6 = new AnswerTree(ds1.getRoot(), nodes, edges, matches);
		assertEquals(at4.hashCode(), at4.hashCode());
		assertEquals(at4.hashCode(), at5.hashCode());
		assertEquals(at4.hashCode(), at6.hashCode());
		cl.close();
		tearDownDB();
	}

}

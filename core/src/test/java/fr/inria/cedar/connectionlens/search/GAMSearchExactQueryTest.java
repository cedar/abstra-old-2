package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.score.ScoringFunction;

/**
 * @author Ioana Manolescu, Tayeb Merabti
 * 
 * General principle: we run a specific query on a specific graph and then
 * check that it has the expected number of results
 *  
 */
public class GAMSearchExactQueryTest extends SearchTest{

	static ScoringFunction sf;
	private static final Logger log = Logger.getLogger(GAMSearchExactQueryTest.class);
	QuerySearch gamSearch;
	
	@Test
	public void testExactQuerySyntax() throws IOException, URISyntaxException, SQLException {
		
		setUpDB();
		// redoing set-up to ensure no extraction, for clarity of this test
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH");
		
		extractor = new TopLevelExtractor(); // we need to redefine it here
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		inputs.clear();
		log.info("Inputs:"+inputs);
		inputs.add(new URI(Paths.get("file:data", "xml", "exact_test.xml").toString()));
		cl.completeDataSourceSetRegistration(inputs);
		
		//QuerySearch gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);

		String query = "Alice";
		Query q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		QuerySearch gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 4);
		
		query = "\"Alice\"";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 4);
		
		query = "exact:Alice";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 1);
		
		query = "exact:Alice+Bob";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 1);
		
		query = "exact:Bob+Alice";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 1);
		
		query = "Alice Bob";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 2);
		
		query = "exact:Alice Bob exact:Carole";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(getResults(gamSearch, q).size(), 2);
		
		query = "Alice Bob Carole";

		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(2, getResults(gamSearch, q).size());
		
		query = "\"Alice Bob\" Carole";
		q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		//log.info("Size:"+getResults(gamSearch, q).size());
		assertEquals(1, getResults(gamSearch, q).size());

		cl.close();
		tearDownDB();
	}

}

/*
 * Copyright(C) 2021 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.ENGLISH;
import static org.junit.Assert.*;

import java.nio.file.Paths;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.SilentStatsCollector;

/**
 * Test for {@link Neo4jDataSource}.
 * 
 * @author Madhulika Mohanty
 *
 */
public class Neo4jDataSourceTest extends ConnectionLensTest {
	
	static EntityExtractor extractor;
	static MorphoSyntacticAnalyser analyzer;
	static SimilarPairProcessor pp;

	@BeforeClass
	public static void setupClass() {
		Config.getInstance().setProperty("default_locale", "en"); 
		extractor = new CacheExtractor(new StanfordNERExtractor(ENGLISH), new SilentStatsCollector(), 0, 0);
		analyzer = new TreeTagger(ENGLISH);
		pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	}
	
	@Test
	public void test() throws SQLException {	
		setUpDB();
		ConnectionLens cl =  new ConnectionLens(extractor, analyzer, pp, true);
		//log.info("Language locale is: " + Config.getInstance().getProperty("default_locale")); 
		cl.register(Paths.get("data", "neo4j", "nodes-address.njn").toString() + ":" 
		+ Paths.get("data", "neo4j", "edges.nje").toString() + ":" + Paths.get("data", "neo4j", "nodes-entity.njn").toString());
		Graph graph = cl.graph();
		assertEquals(1,graph.getNodes(Types.DATA_SOURCE).size());
		assertEquals(1,graph.getNodes(Types.RDF_URI).size());
		assertEquals(5,graph.getNodes(Types.ENTITY_PERSON).size());
		assertEquals(16,graph.getNodes(Types.ENTITY_LOCATION).size());
		assertEquals(14,graph.getNodes(Types.ENTITY_ORGANIZATION).size());
		assertEquals(9,graph.getNodes(Types.NEO4J_ENTITY).size());
		assertEquals(232,graph.getNodes(Types.NEO4J_VALUE).size());
		assertEquals(5,graph.getNodes(Types.NEO4J_EDGE).size());
		cl.close();
		tearDownDB();
	}
	
	@AfterClass
	public static void afterClass() {
		Config.getInstance().setProperty("default_locale", "fr");
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Arrays.asList;
import static java.util.Locale.FRENCH;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

public class BulkGraphUpdateTest extends ConnectionLensTest {

	static StatisticsCollector stats = StatisticsCollector.mute();
	static EntityExtractor dummyExtractor = new TopLevelExtractor();
	static EntityExtractor cacheExtractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	static MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	final String db1 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword;
	final String db2 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword;
	final String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	final String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();
	
	public static ConnectionLens initWithDB(String dbName, int batchSize) throws SQLException {
		String standardDBName = Config.getInstance().getProperty("RDBMS_DBName");
		Config.getInstance().setProperty("RDBMS_DBName", dbName);
		Config.getInstance().setProperty("update_batch_size", String.valueOf(batchSize));
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		ConnectionLens cl =  new ConnectionLens(dummyExtractor, analyzer, pp, true);
		Config.getInstance().setProperty("RDBMS_DBName", standardDBName);
		return cl; 
	}
	@Test
	public void testBulkRegisterTwoJSONDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example2.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example2.json").toString());

		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 
	}

	@Test
	public void testBulkRegisterTwoRDFDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		
		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 
	}

	@Test
	public void testBulkRegisterTwoRelationalDatasets() throws SQLException, IOException {
		setUpDB("schema1", "schema2"); // within junit
		try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
			 Statement s1 = c1.createStatement()) {
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
		}
		// two more databases
		ConnectionLens cl1 = initWithDB("junit1", 1);
		ConnectionLens cl2 = initWithDB("junit2", -1);
		try (RelationalDataSource ds11 = (RelationalDataSource) cl1.register(db1);
			 RelationalDataSource ds12 = (RelationalDataSource) cl1.register(db2);
			 RelationalDataSource ds21 = (RelationalDataSource) cl2.register(db1);
			 RelationalDataSource ds22 = (RelationalDataSource) cl2.register(db2)) {

			sameGraph(cl1, cl2, true); 
			
		}
		cl1.close();
		cl2.close();
		tearDownDB();
		tearDownDB("junit1", "junit2");
	}

	@Test
	public void testBulkRegisterTwoTextDatasets() throws IOException, SQLException {
		setUpDB();
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(path1);
		DataSource ds12 = cl1.register(path2);
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(path1);
		DataSource ds22 = cl2.register(path2);

		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 
	}

	@Test
	public void testBulkRegisterTwoDatasetsSpecificity() throws IOException, SQLException {

		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example2.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", 15);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example2.json").toString());

		sameGraph(cl1, cl2, true); 
		cl1.processSimilarities(asList(ds11, ds12));
		cl2.processSimilarities(asList(ds21, ds22));

		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 

	}

	@Test
	@Ignore //@Tayeb: batch=1 update specificity miss (erase the specificity for some relations)
	public void testBulkRegisterTwoJSONDatasetsSameAs() throws IOException, SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example3.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "json", "test_example5.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example3.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "json", "test_example5.json").toString());

		sameGraph(cl1, cl2, true);
		
		Config.getInstance().setProperty ("update_batch_size","1");
		cl1.processSimilarities(asList(ds11, ds12));
		Config.getInstance().setProperty ("update_batch_size","-1");
		cl2.processSimilarities(asList(ds21, ds22));

		//(new GraphPrintingByRepresentative()).printAllSources(cl1);
		//(new GraphPrintingByRepresentative()).printAllSources(cl2);

		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 

	}

	@Test
	@Ignore //@Tayeb: batch=1 update specificity miss (erase the specificity for some relations)
	public void testBulkRegisterTwoRDFDatasetsSameAs() throws IOException, SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

		sameGraph(cl1, cl2, true); 
		Config.getInstance().setProperty ("update_batch_size","1");
		cl1.processSimilarities(asList(ds11, ds12));
		Config.getInstance().setProperty ("update_batch_size","-1");
		cl2.processSimilarities(asList(ds21, ds22));
		
		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2"); 


	}

	@Test
	@Ignore //@Tayeb: batch=1 update specificity miss (erase the specificity for some relations)
	public void testBulkRegisterTwoRelationalDatasetsSameAs() throws SQLException, IOException {
		setUpDB("schema1", "schema2");
		try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
			 Statement s1 = c1.createStatement()) {
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
		}
		ConnectionLens cl1 = initWithDB("junit1", 1);
		ConnectionLens cl2 = initWithDB("junit2", -1);
		try (RelationalDataSource ds11 = (RelationalDataSource) cl1.register(db1);
			 RelationalDataSource ds12 = (RelationalDataSource) cl1.register(db2);
			 RelationalDataSource ds21 = (RelationalDataSource) cl2.register(db1);
			 RelationalDataSource ds22 = (RelationalDataSource) cl2.register(db2)) {

			sameGraph(cl1, cl2, true);
			
			Config.getInstance().setProperty ("update_batch_size","1");
			cl1.processSimilarities(asList(ds11, ds12));
			Config.getInstance().setProperty ("update_batch_size","-1");
			cl2.processSimilarities(asList(ds21, ds22));

			sameGraph(cl1, cl2, true); 
		}
		cl1.close();
		cl2.close();
		tearDownDB();
	}

	@Test
	@Ignore //@Tayeb: batch=1 update specificity miss (erase the specificity for some relations)
	public void testBulkRegisterTwoTextDatasetsSameAs() throws IOException, SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		DataSource ds11 = cl1.register(path1);
		DataSource ds12 = cl1.register(path2);
		ConnectionLens cl2 = initWithDB("junit2", -1);
		DataSource ds21 = cl2.register(path1);
		DataSource ds22 = cl2.register(path2);

		sameGraph(cl1, cl2, true); 

		Config.getInstance().setProperty ("update_batch_size","1");
		cl1.processSimilarities(asList(ds11, ds12));
		Config.getInstance().setProperty ("update_batch_size","-1");
		cl2.processSimilarities(asList(ds21, ds22));
		
		sameGraph(cl1, cl2, true); 
		cl1.close();
		cl2.close(); 
		tearDownDB();
	}


}

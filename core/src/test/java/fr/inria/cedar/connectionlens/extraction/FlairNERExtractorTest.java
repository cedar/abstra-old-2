/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.extraction;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static java.util.Locale.ENGLISH;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.PythonUtils;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
@Ignore
public class FlairNERExtractorTest extends ConnectionLensTest {

	private static final Logger log = Logger.getLogger(FlairNERExtractorTest.class);

	@Test
	public void testExtractEntitiesInFrenchLeMonde() throws IOException, SQLException {
		boolean failed = false;
		Config.getInstance().setProperty("entity_disambiguation", "true");
		EntityExtractor extractorFrench = new FlairNERExtractor(FRENCH);
		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
		if (pythonVersionExists()) {
			setUpDB();
			ConnectionLens cl1 = new ConnectionLens(extractorFrench, analyzer, pp, true);
			Graph graph = cl1.graph();
			if (FlairNERExtractor.isFlairInstalledAndRunning()) {
				log.info("Starting Flask with French Flair model...");
				DataSource ds = cl1.register("data/text/input/lemonde.txt", "http://lemonde.fr");
				assertEquals(9, graph.countNodes(ds, ENTITY_PERSON));
				assertEquals(4, graph.countNodes(ds, ENTITY_LOCATION));
				assertEquals(2, graph.countNodes(ds, ENTITY_ORGANIZATION));
				assertEquals(10, graph.getDisambiguatedNodes(ds).size());
				assertEquals("http://www.wikidata.org/entity/Q3052772", graph.getDisambiguatedID("Macron"));
				assertEquals("http://www.wikidata.org/entity/Q30974", graph.getDisambiguatedID("Rouen"));
				PythonUtils.getInstance().stopFlairExtractor();
				cl1.close();
			} else {
				log.info("Flair fails to start");
				failed = true;
			}
		} else {
			log.info("Python version mentioned in Properties not installed...");
			failed = true;
		}
		// tear down DB before failing
		tearDownDB();
		if (failed) {
			assert (false);
		}
	}

	@Test
	public void testExtractEntitiesInFrenchHTML() throws IOException, SQLException {
		boolean failed = false;
		Config.getInstance().setProperty("entity_disambiguation", "true");
		EntityExtractor extractorFrench = new FlairNERExtractor(FRENCH);
		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());

		if (pythonVersionExists()) {
			setUpDB();
			if (FlairNERExtractor.isFlairInstalledAndRunning()) {

				ConnectionLens cl2 = new ConnectionLens(extractorFrench, analyzer, pp, true);
				DataSource ds = cl2.register("data/html/testHTML.html",
						"https://www.mediapart.fr/journal/fil-dactualites/071016/henin-beaumont-le-fn-vote-une-motion-anti-migrants");
				Graph graph = cl2.graph();

				assertEquals(2, graph.countNodes(ds, ENTITY_PERSON));
				assertEquals(3, graph.countNodes(ds, ENTITY_LOCATION));
				assertEquals(3, graph.countNodes(ds, ENTITY_ORGANIZATION));
				assertEquals(5, graph.getDisambiguatedNodes(ds).size());
				assertEquals("http://www.wikidata.org/entity/Q3497810", graph.getDisambiguatedID("Steeve Briois"));
				assertEquals("http://www.wikidata.org/entity/Q240858", graph.getDisambiguatedID("Hénin-Beaumont"));
				PythonUtils.getInstance().stopFlairExtractor();
				cl2.close();
			} else {
				log.info("Flair fails to start");
				failed = true;
			}
		} else {
			log.info("Python version mentioned in Properties not installed...");
			failed = true;
		}
		// tear down DB before failing
		tearDownDB();
		if (failed) {
			assert (false);
		}
	}

	@Test
	public void testExtractEntitiesInEnglishSample() throws IOException, SQLException {
		boolean failed = false;
		if (pythonVersionExists()) {
			Config.getInstance().setProperty("entity_disambiguation", "true");
			Config.getInstance().setProperty("default_locale", "en");
			  
			StatisticsCollector stats = new StatisticsCollector.SilentStatsCollector();
			EntityExtractor extractorEnglish = new FlairNERExtractor(ENGLISH);
			MorphoSyntacticAnalyser analyzer = new TreeTagger(ENGLISH);
			SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
			setUpDB();
			ConnectionLens cl = new ConnectionLens(extractorEnglish, analyzer, pp, true);
			Graph graph = cl.graph();
			if (FlairNERExtractor.isFlairInstalledAndRunning()) {
				log.info("Starting Flask with English Flair model...");
				DataSource ds = cl.register("data/text/input/english/sample.txt", "");
				assertEquals(2, graph.countNodes(ds, ENTITY_PERSON));
				assertEquals(5, graph.countNodes(ds, ENTITY_ORGANIZATION));

				assertEquals("http://www.wikidata.org/entity/Q115860",
						graph.getDisambiguatedID("Federal Reserve Bank of New York"));
				assertEquals("http://www.wikidata.org/entity/Q648666", graph.getDisambiguatedID("Treasury"));
				PythonUtils.getInstance().stopFlairExtractor();
				cl.close();

			} else {
				log.info("Flair fails to start");
				failed = true;
			}
		} else {
			log.info("Python version mentioned in Properties not installed");
			failed = true;
		}
		tearDownDB();
		  
		if (failed) {
			assert (false);
		}
	}

	@Test
	public void testExtractEntitiesInEnglishObama() throws IOException, SQLException {
		boolean failed = false;
		Config.getInstance().setProperty("entity_disambiguation", "true");
		Config.getInstance().setProperty("default_locale", "en");
		
		EntityExtractor extractorEnglish = new FlairNERExtractor(ENGLISH);
		MorphoSyntacticAnalyser analyzer = new TreeTagger(ENGLISH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());

		if (pythonVersionExists()) {
			setUpDB();
			if (FlairNERExtractor.isFlairInstalledAndRunning()) {
				log.info("Starting Flask with English Flair model...");
				Config.getInstance().setProperty("entity_disambiguation", "false");

				ConnectionLens cl = new ConnectionLens(extractorEnglish, analyzer, pp, true);
				DataSource ds1 = cl.register("data/xml/example1.xml", "");
				Graph graph = cl.graph();

				assertEquals(3, graph.countNodes(ds1, ENTITY_PERSON));
				assertEquals(6, graph.countNodes(ds1, ENTITY_ORGANIZATION));
				Collection<Entity> extractedEntities = extractorEnglish.run(ds1,
						"Barack Obama was born on August 4, 1961 in Honolulu, Hawaii which was 4 days ago.",
						Integer.MAX_VALUE);

				Entity obamaEntity = extractedEntities.stream()
						.filter(c -> c.type().equals(ENTITY_PERSON) && c.value().equals("Barack Obama")).findFirst()
						.get();
				assertNotNull(obamaEntity);
				assertEquals("http://www.wikidata.org/entity/Q76", obamaEntity.getAmbiID());

				Entity hawaiEntity = extractedEntities.stream()
						.filter(c -> c.type().equals(ENTITY_LOCATION) && c.value().equals("Hawaii")).findFirst().get();

				assertNotNull(hawaiEntity);
				assertEquals("http://www.wikidata.org/entity/Q18094", hawaiEntity.getAmbiID());

				Config.getInstance().setProperty("entity_disambiguation", "false");
				PythonUtils.getInstance().stopFlairExtractor();
				cl.close();

			} else {
				log.info("Flair fails to start");
				failed = true;
			}
		} else {
			log.info("Python version mentioned in Properties not installed");
			failed = true;
		}
		tearDownDB();
	
		if (failed) {
			assert (false);
		}
	}

	private void printEntityList(Collection<Entity> entities) {
		for (Entity ent : entities) {
			log.info("entity(" + ent.value() + ", " + ent.offset() + ", " + ent.length() + ", " + ent.type() + ", "
					+ ent.confidence() + ")");
		}
	}

	@AfterClass
	public static void tearDown() {
		// log.info("AfterClass");
		Config.getInstance().setProperty("entity_disambiguation", "false");
		Config.getInstance().setProperty("default_locale", "fr");
	}

}
/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_LITERAL;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RDF_URI;
import static fr.inria.cedar.connectionlens.graph.Node.Types.TEXT_VALUE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Arrays.asList;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;

public class SpecificityTest extends ConnectionLensTest {

	static ConnectionLens cl;
	EntityExtractor extractor = new TopLevelExtractor();
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());

	@Test
	public void testRegisterTwoDatasets() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_02.nt").toString());
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_03.nt").toString());

		Node n1 = ds1.buildLabelNodeOfType("http://inria.fr/book", RDF_URI);
		Node n2 = ds1.buildLabelNodeOfType("Author 1", RDF_LITERAL);
		Edge e1 = ds1.buildLabelledEdge(n1, n2, "http://dbpedia.org/property/author");

		Node m1 = ds2.buildLabelNodeOfType("http://inria.fr/book2", RDF_URI);
		Node m2 = ds2.buildLabelNodeOfType("Author 1", RDF_LITERAL);
		Edge e2 = ds2.buildLabelledEdge(m1, m2, "http://dbpedia.org/property/title");

		assertEquals(1., e1.getSpecificityValue(), .0);
		assertEquals(1., e2.getSpecificityValue(), .0);

		cl.processSimilarities(asList(ds1, ds2));
		for (Edge e : cl.graph().getEdges(ds1, "http://dbpedia.org/property/author")) {
			if (e.getSourceNode().equals(n1) && e.getTargetNode().equals(n2)) {
				assertEquals(.4, e.getSpecificityValue(), .0);
				break;
			}
		}
		for (Edge e : cl.graph().getEdges(ds2, "http://dbpedia.org/property/title")) {
			if (e.getSourceNode().equals(m1) && e.getTargetNode().equals(m2)) {
				assertEquals(.5, e.getSpecificityValue(), .0);
				break;
			}
		}
		cl.close(); 
		tearDownDB();
	}

	@Test
	public void testSpecificEdgesTraversing() throws SQLException{
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);

		DataSource ds = cl.register(Paths.get("data", "rdf", "low-specificity-test.nt").toString());
		cl.graph().countEdgesForSpecificity();
		cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
		
		Pair<Double, Double> p = cl.graph().getSpecificityStat();
		
		//Node n = ds.buildNode("http://graph.cl/line1","http://graph.cl/line1", RDF_URI);

		Node n = cl.graph().resolveNode (new NumericNodeID(5));
		Set<Edge> o = cl.graph().getOutgoingEdges(n); 
		Set<Edge> os = cl.graph().getSpecificOutgoingEdgesPostLoading(n,p.getLeft() - 3 * p.getRight()); // mean - 3 * sd
		assertNotEquals(o.size(),os.size());
		cl.close(); 
		tearDownDB();
	}

	@Ignore
	public void testInitialSpecificitiesRDF() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_05", "model-03_17-10.nt").toString());
		cl.graph().countEdgesForSpecificity();
		cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
		
		Node n1 = ds1.buildLabelNodeOfType(
				"http://contentcheck.fr/schemas/TwitterAccount", RDF_URI);
		cl.processSimilarities(asList(ds1));
		for (Edge e : cl.graph().getEdges(ds1, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
			if (e.getTargetNode().equals(n1)) {
				assertEquals(.6, e.getSpecificityValue(), .1);
			}
		}
		for (Edge e : cl.graph().getEdges(ds1, "http://contentcheck.fr/schemas/name")) {
			if (e.getTargetNode().equals(n1)) {
				assertEquals(.5, e.getSpecificityValue(), .0);
			}
		}
		for (Edge e : cl.graph().getEdges(ds1, "http://contentcheck.fr/schemas/party")) {
			if (e.getTargetNode().equals(n1)) {
				assertEquals(1., e.getSpecificityValue(), .0);
			}
		}
		cl.close(); 
		tearDownDB();
	}

	@Test
	public void testInitialSpecificitiesRelational() throws SQLException {
		setUpDB("schema1");
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
				Statement s1 = c1.createStatement()) {
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'smith')");
		}
		try (RelationalDataSource ds = (RelationalDataSource) cl
				.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword)) {
			cl.graph().countEdgesForSpecificity();
			cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
		
			Graph graph = cl.graph();
			Node n0 = graph.resolveNode(new NumericNodeID(4));
			Node n1 = graph.resolveNode(new NumericNodeID(6));
			Node n2 = graph.resolveNode(new NumericNodeID(8));

			graph.getEdges(n0,n1).stream()
					.forEach (s->assertEquals(1.0, s.getSpecificityValue(), .0));

			graph.getEdges(n1,n2).stream()
					.forEach (s->assertEquals(.5, s.getSpecificityValue(), .0));

			graph.getEdges("name").stream()
					.forEach (s->assertEquals(1.0, s.getSpecificityValue(), .0));

		}
		cl.close(); 
		tearDownDB();
	}

	@Test
	public void testInitialSpecificitiesJSON() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "json", "test_example13.json").toString());
		cl.graph().countEdgesForSpecificity();
		cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
	
		Graph graph = cl.graph();

		Node n0 = graph.resolveNode(new NumericNodeID(4));
		Node n1 = graph.resolveNode(new NumericNodeID(5));


        graph.getEdges(n0,n1).stream()
				.forEach (s->assertEquals(.5, s.getSpecificityValue(), .0));

		graph.getEdges("auteur").stream()
				.forEach (s->assertEquals(1.0, s.getSpecificityValue(), .0));

		graph.getEdges("ouvrages").stream()
				.forEach (s->assertEquals(1.0, s.getSpecificityValue(), .0));


		Node n3 = graph.resolveNode(new NumericNodeID(6));
		Node n4 = graph.resolveNode(new NumericNodeID(7));
		Node n5 = graph.resolveNode(new NumericNodeID(9));

		graph.getEdges(n3,n4).stream()
				.forEach (s->assertEquals(0.4, s.getSpecificityValue(), .0));


		graph.getEdges(n3,n5).stream()
				.forEach (s->assertEquals(0.4, s.getSpecificityValue(), .0));
		cl.close(); 
		tearDownDB();
	}

	@Ignore
	public void testInitialSpecificitiesText() throws Exception {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		DataSource ds = cl.register(Paths.get("data", "text", "input", "lemonde.txt").toString());

		String textp1 = IOUtils.slurpURL(Paths.get("file:data", "text", "input", "lemonde.txt").toString());

		Node ntx1 = ds.buildLabelNodeOfType(textp1, TEXT_VALUE);


		Properties p1 = new Properties(), p2 = new Properties(), p3 = new Properties(), p4 = new Properties();
		Node n0 = ds.getRoot();
		p1.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "247");
		p1.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "5");
		Node n1 = ds.buildChildNode(ntx1.getId(), "Havre", ENTITY_LOCATION, p1);
		p2.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "542");
		p2.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
		Node n2 = ds.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p2);
		p3.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "1638");
		p3.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "6");
		Node n3 = ds.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p3);
		p4.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "1740");
		p4.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "5");
		Node n4 = ds.buildChildNode(ntx1.getId(), "Rouen", ENTITY_LOCATION, p4);

		Graph graph = cl.graph();
		Edge e1 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n1, 1.0);
		assertEquals(.33, graph.resolveEdge(e1.getId()).getSpecificityValue(), .01);
		Edge e2 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n2, 1.0);
		assertEquals(.25, graph.resolveEdge(e2.getId()).getSpecificityValue(), .01);
		Edge e3 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n3, 1.0);
		assertEquals(.25, graph.resolveEdge(e3.getId()).getSpecificityValue(), .01);
		Edge e4 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n4, 1.0);
		assertEquals(.33, graph.resolveEdge(e4.getId()).getSpecificityValue(), .01);
		cl.close(); 
		tearDownDB();
	}
}

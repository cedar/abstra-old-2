/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.SAME_AS_EDGE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_ORGANIZATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static fr.inria.cedar.connectionlens.graph.Node.Types.TEXT_VALUE;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;

import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Sets;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class TextDataSourceTest extends ConnectionLensTest {

	String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();
	String path3 = Paths.get("file:data", "text", "input", "no_entity.txt").toString();
	String n1ID = "1|1|offset542|length16|ENTITY_PERSON";
	String n2ID = "1|1|offset247|length5|ENTITY_LOCATION";
	String m1ID = "2|2|offset837|length16|ENTITY_PERSON";
	String m2ID = "2|2|offset3156|length6|ENTITY_LOCATION";

	EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	ConnectionLens cl; 

	@Test
	public void testNormalRegistration() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "generated", "generated-10.txt").toString());
		(new GraphPrintingByRepresentative()).printAllSources(cl);
		assertEquals(21, cl.graph().countNodes(ds));
		assertEquals(1, cl.graph().countEdges(ds, EDGE, EXTRACTION_EDGE));
		cl.close(); 
		tearDownDB();
	}

	@Ignore
	public void testRegisterTwoDatasets() throws Exception {
		setUpDB();
		Config.getInstance().setProperty("update_batch_size", "1");
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		DataSource ds1 = cl.register(path1);
		DataSource ds2 = cl.register(path2);

		String textp1 = IOUtils.slurpURL(path1);
		String textp2 = IOUtils.slurpURL(path2);

		Node ntx1 = ds1.buildLabelNodeOfType(textp1, TEXT_VALUE);

		Node mtx1 = ds2.buildLabelNodeOfType(textp2, TEXT_VALUE);

		Properties p11 = new Properties(), p12 = new Properties(), p21 = new Properties(), p22 = new Properties();
		p11.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "542");
		p11.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
		Node n1 = ds1.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p11);
		p12.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "247");
		p12.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "5");
		Node n2 = ds1.buildChildNode(ntx1.getId(), "Havre", ENTITY_LOCATION, p12);

		Edge e1 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n1, 1.0);
		Edge e2 = ntx1.getDataSource().buildNERExtractionEdge(ntx1, n2, 1.0);

		p21.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "837");
		p21.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
		Node m1 = ds2.buildChildNode(mtx1.getId(), "Edouard Philippe", ENTITY_PERSON, p21);
		p22.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "3156");
		p22.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "6");
		Node m2 = ds2.buildChildNode(mtx1.getId(), "Nantes", ENTITY_LOCATION, p22);

		Edge f1 = mtx1.getDataSource().buildNERExtractionEdge(mtx1, m1, 1.0);
	
		assertTrue(cl.graph().getNodes(ds1).contains(n1));
		assertTrue(cl.graph().getNodes(ds1).contains(n2));
		assertFalse(cl.graph().getNodes(ds1).contains(m1));
		assertTrue(cl.graph().getEdges(ds1).contains(e1));
		assertTrue(cl.graph().getEdges(ds1).contains(e2));
		assertFalse(cl.graph().getEdges(ds1).contains(f1));
		assertEquals(Sets.newHashSet(n1), cl.graph().getNodes(ds1, "Edouard Philippe"));
		assertEquals(Sets.newHashSet(), cl.graph().getNodes(ds1, "Nantes"));
		assertEquals(Sets.newHashSet(), cl.graph().getEdges(ds1, "nonsense"));
		if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_OCCURRENCE")){
			assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword ("edouard")).containsAll(Sets.newHashSet(n1, m1)));
		}
		if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_GRAPH")){
			assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1)));
		}

		//assertTrue(cl.graph().getNodes("Edouard Philippe").containsAll(Sets.newHashSet(n1, m1)));
		assertTrue(cl.graph().getNodes("Nantes").contains(m2));
		assertEquals(Sets.newHashSet(), cl.graph().getNodes("Jean-Paul Sartre"));
		assertEquals(Sets.newHashSet(), cl.graph().getEdges("nonsense"));
		assertEquals(34, cl.graph().countEdges(ds1));
		assertEquals(1, cl.graph().countEdges(ds1, EDGE));
		assertEquals(0, cl.graph().countEdges(ds1, EXTRACTION_EDGE));
		assertEquals(98, cl.graph().countEdges(ds2));
		assertEquals(1, cl.graph().countEdges(ds2, EDGE));
		assertEquals(0, cl.graph().countEdges(ds1, EXTRACTION_EDGE));
		assertEquals(0, cl.graph().countEdges(ds2, SAME_AS_EDGE));
		assertEquals(19, cl.graph().countNodes(ds1));
		assertEquals(51, cl.graph().countNodes(ds2));
		assertEquals(31, cl.graph().countNodes(ds2, ENTITY_PERSON, ENTITY_LOCATION));
		assertEquals(17, cl.graph().countNodes(ds2, ENTITY_ORGANIZATION));
		cl.close();
		tearDownDB();
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.SAME_AS_EDGE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static fr.inria.cedar.connectionlens.util.StatisticsCollector.mute;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.CachedSimilarPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.JaroPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

public class SameAsTest extends ConnectionLensTest {
	
	// TODO: test getBorders() / getSameAs([Node n, DataSource ds, ] double threshold)

//	String path3 = Paths.get("file:data", "text", "input", "no_entity.txt").toString();
//	String n1ID = "1|1|offset542|length16|ENTITY_PERSON";
//	String n2ID = "1|1|offset249|length5|ENTITY_LOCATION";
//	String m1ID = "2|2|offset837|length16|ENTITY_PERSON";
//	String m2ID = "2|2|offset3096|length6|ENTITY_LOCATION";

	String db1 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword;
	String db2 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword;
	String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	String path2 =  Paths.get("file:data", "text", "input", "piste.txt").toString();
	String path3 =  Paths.get("file:data", "json", "json_06.json").toString();
	
	static JaroPairProcessor pp;
	static EntityExtractor lazyExtractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	static EntityExtractor dummyExtractor = new TopLevelExtractor();

	@BeforeClass
	public static void setup() {
		Config.getInstance().setProperty("similarity_threshold_jaro", ".8");
	}
	
	private ConnectionLens init(EntityExtractor extractor) {
		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		pp = new JaroPairProcessor(mute(), 0.);
		return new ConnectionLens(extractor, analyzer, pp, true);
	}

	@Ignore @Test 
	// [IM, 8/2/20] The test fails if the addition of weak same-as edges is asymetric
	// (this is done in CachedSimilarityPairProcessor.process).
	// It makes sense not to add a weak similarity twice.
	// However, I'm not sure how to fix this test, nor what it means.
	public void testRegisterTwoJSONDatasets() {
		ConnectionLens cl = init(dummyExtractor);
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds2 = cl.register(Paths.get("data", "json", "test_example5.json").toString());
		Set<Edge> sameAsEdges = new LinkedHashSet<>();
		pp.process(cl.graph(), singleton(ds1), asList(ds1, ds2), e->{
			if (e.getType() == SAME_AS_EDGE) {
				log.info("Same-as edge: " + e + " " + e.confidence());
				sameAsEdges.add(e);
			}
		});
		log.info("After " + pp.getClass().getSimpleName() + " processing there are " + sameAsEdges.size() + " edges");
		cl.processSimilarities(asList(ds1, ds2));
		assertTrue(sameAsEdges.size() > 0);
		log.info("The graph has "  + cl.graph().getSameAs().size() + " sameAs edges");
		for (Edge e: cl.graph().getSameAs()) {
			log.info("Graph same-as edge: " + e);
		}
		assertEquals(sameAsEdges, cl.graph().getSameAs());
		assertEquals(sameAsEdges.size(), cl.graph().countSameAs(ds1) + cl.graph().countSameAs(ds2));
	}

	// [IM, 8/2/20] Same comment as first test in this class
	@Ignore @Test public void testRegisterTwoRDFDatasets() {
		ConnectionLens cl = init(dummyExtractor);
		DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		Set<Edge> sameAsEdge = new LinkedHashSet<>();
		pp.process(cl.graph(), singleton(ds1), asList(ds1, ds2), e->{
			if (e.getType() == SAME_AS_EDGE) {
				sameAsEdge.add(e);
			}
		});
		cl.processSimilarities(asList(ds1, ds2));
		assertTrue(sameAsEdge.size() > 0);
		assertEquals(sameAsEdge, cl.graph().getSameAs());
		assertEquals(sameAsEdge.size(), cl.graph().countSameAs(ds1) + cl.graph().countSameAs(ds2));
	}

	// [IM, 8/2/20] Same comment as first test in this class
	@Ignore @Test public void testRegisterTwoRelationalDatasets()  throws SQLException {
		ConnectionLens cl = init(dummyExtractor);
		setUpDB("schema1", "schema2");
		try(Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
			Statement s1 = c1.createStatement()) {
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
		}
		try(RelationalDataSource ds1 = (RelationalDataSource) cl.register( 
				"jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword);
			RelationalDataSource ds2 = (RelationalDataSource) cl.register(  
				"jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword)) {
			Set<Edge> sameAsEdge = new LinkedHashSet<>();
			pp.process(cl.graph(), singleton(ds1), asList(ds1, ds2), e->{
				if (e.getType() == SAME_AS_EDGE) {
					sameAsEdge.add(e);
				}
			});
			cl.processSimilarities(asList(ds1, ds2));
			assertTrue(sameAsEdge.size() > 0);
			assertEquals(sameAsEdge, cl.graph().getSameAs());
			assertEquals(sameAsEdge.size(), cl.graph().countSameAs(ds1) + cl.graph().countSameAs(ds2));
		}
		tearDownDB();
	}

	// [IM, 8/2/20] Same comment as first test in this class
	@Ignore @Test public void testRegisterTwoTextDatasets() {
		ConnectionLens cl = init(lazyExtractor);
		DataSource ds1 = cl.register(path1);
		DataSource ds2 = cl.register(path2);
		Set<Edge> sameAsEdges = new LinkedHashSet<>();
		pp.process(cl.graph(), singleton(ds1), asList(ds1, ds2), e->{
			if (e.getType() == SAME_AS_EDGE) {
				log.info("Adding edge: " + e);
				sameAsEdges.add(e);
			}
		});
		cl.processSimilarities(asList(ds1, ds2));
		assertTrue(sameAsEdges.size() > 0);
		assertEquals(sameAsEdges, cl.graph().getSameAs());
		assertEquals(sameAsEdges.size(), cl.graph().countSameAs(ds1) + cl.graph().countSameAs(ds2));
	}



	
}

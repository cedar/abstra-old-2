package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Locale;

import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import org.apache.log4j.Logger;
import org.jodconverter.local.office.utils.Lo;
import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.score.ScoringFunction;

/**
 * @author Madhulika Mohanty
 * 
 *  This class specifically tests GAMSearch on a chain graph with 3-keyword query.
 *  It also checks whether merges are proper or not.
 *  
 */
public class GAMSearchChainQueryTest extends SearchTest {

	static ScoringFunction sf;
	private static final Logger log = Logger.getLogger(GAMSearchChainQueryTest.class);
	QuerySearch gamSearch;
	

	/**
	 * Test specifically to check GAMSearch for correctness with a 3-keyword query.
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws SQLException
	 */
	@Test
	public void test3KwdTriangleQuery() throws IOException, URISyntaxException, SQLException {
		setUpDB();
		// redoing set-up to ensure no extraction, for clarity of this test
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH");
		
		extractor = new TopLevelExtractor(); // we need to redefine it here
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		inputs.clear();
		inputs.add(new URI(Paths.get("file:data", "poc", "5", "triangle.nt").toString()));
		cl.completeDataSourceSetRegistration(inputs);
		
		String query = "Alice Bob Carole";

		Query q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(1, getResults(gamSearch, q).size());
		
		cl.close();
		tearDownDB();
	}
	
	@Test
	public void test3KwdChainQuery() throws IOException, URISyntaxException, SQLException {
		setUpDB();
		// redoing set-up to ensure no extraction, for clarity of this test
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH");
		
		extractor = new TopLevelExtractor(); // we need to redefine it here
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		inputs.clear();
		inputs.add(new URI(Paths.get("file:data", "poc", "5", "chain.nt").toString()));
		cl.completeDataSourceSetRegistration(inputs);
		
		String query = "n1 n2 n3";

		Query q = Query.parse(query);
		sf = cl.resolveScoringFunction("", q);
		gamSearch = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		assertEquals(1, getResults(gamSearch, q).size());
		
		cl.close();
		tearDownDB();
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		extractor = new StanfordNERExtractor(Locale.FRENCH); // we need to redefine it here
	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class RelationalDataSourceTest extends ConnectionLensTest {

	static EntityExtractor extractor = new TopLevelExtractor();
	static MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);

	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());

	public static ConnectionLens initWithDB(String dbName, int batchSize) throws SQLException {
		String standardDBName = Config.getInstance().getProperty("RDBMS_DBName");
		Config.getInstance().setProperty("RDBMS_DBName", dbName);
		Config.getInstance().setProperty("update_batch_size", String.valueOf(batchSize));
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		ConnectionLens cl =  new ConnectionLens(extractor, analyzer, pp, true);
		Config.getInstance().setProperty("RDBMS_DBName", standardDBName);
		return cl; 
	}

	@Test
	public void testPKFK() throws SQLException {
		setUpDB("schema1");
		ConnectionLens cl = initWithDB("junit", 10); 
		Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword,
				"schema1");
		Statement s1 = c1.createStatement();
		s1.executeUpdate("CREATE TABLE schema1.person(id INT PRIMARY KEY, name VARCHAR)");
		s1.executeUpdate("INSERT INTO schema1.person VALUES (1, 'alice')");
		s1.executeUpdate("INSERT INTO schema1.person VALUES (2, 'bob')");
		s1.executeUpdate("CREATE TABLE schema1.company(name VARCHAR, ceo INT, FOREIGN KEY(ceo) REFERENCES schema1.person(id))");
		s1.executeUpdate("INSERT INTO schema1.company VALUES ('ibm', 1)");
		s1.executeUpdate("INSERT INTO schema1.company VALUES ('abc', 2)");
		
		RelationalDataSource ds1 = (RelationalDataSource) cl
				.register("jdbc:postgresql://localhost:" + defaultPort + 
						"/junit?currentSchema=schema1&user=" +	defaultUser + 
						"&password=" + defaultPassword,  "");
		(new GraphPrintingByRepresentative()).printAllSources(cl);
		
		// Nodes 18 and 24 are the two person tuples. 
		// They need to have *two* incoming edges each (one from the table, one for the fk)
		// and 2 outgoing edges.
		Node n1 = cl.graph().resolveNode(new NumericNodeID(18));
		assertEquals(2, cl.graph().getIncomingEdges(n1).size());
		assertEquals(2, cl.graph().getOutgoingEdges(n1).size());
		
		Node n2 = cl.graph().resolveNode(new NumericNodeID(24));
		assertEquals(2, cl.graph().getIncomingEdges(n2).size());
		assertEquals(2, cl.graph().getOutgoingEdges(n2).size());
		
		cl.close();
		tearDownDB(); 
	
	}
	
	@Test
	public void testMultiAttributeKey() throws SQLException {
		setUpDB("schema1");
		ConnectionLens cl = initWithDB("junit", 10); 
		Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword,
				"schema1");
		Statement s1 = c1.createStatement();
		s1.executeUpdate("create table employee(first varchar, last varchar, age int);");
		s1.executeUpdate("alter table employee add constraint flk primary key (first, last);");
		s1.executeUpdate("insert into employee values ('Alice', 'Jones', 24);");
		s1.executeUpdate("insert into employee values ('David', 'Bo', 44);");
		s1.executeUpdate("create table enterprise(name varchar, ceof varchar, ceol varchar);");
		s1.executeUpdate("alter table enterprise add constraint flfk foreign key (ceof, ceol) references employee (first, last);");
		s1.executeUpdate("insert into enterprise values ('IBM', 'Alice', 'Jones');");		
		s1.executeUpdate("insert into enterprise values ('Oracle', 'Alice', 'Jones');"); 
		s1.executeUpdate("insert into enterprise values ('Toyota', 'David', 'Bo');");
		
		RelationalDataSource ds1 = (RelationalDataSource) cl
				.register("jdbc:postgresql://localhost:" + defaultPort + 
						"/junit?currentSchema=schema1&user=" +	defaultUser + 
						"&password=" + defaultPassword,  "");
		//(new GraphPrintingByRepresentative()).printAllSources(cl);
		
		// In this database, 3 enterprises have FKs into 2 people.
		// Check the presence and label of the FK edges: 
		assertEquals(3, cl.graph().getEdges("ceof,ceol").size()); 
		// Check that they point to 2 nodes:
		HashSet<Node> ceos = new HashSet<Node>();
		for (Edge e: cl.graph().getEdges("ceof,ceol")) {
			ceos.add(e.getTargetNode()); 
		}
		assertEquals(2, ceos.size()); 
		
		cl.close();
		tearDownDB(); 
	
	}
	
	// TODO need to refactor this test with all possible atomicities.
	@Test
	public void testRegisterTwoDatasets() {
		try {
			setUpDB("schema1", "schema2");
			SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
			Config.getInstance().setProperty("update_batch_size", "1");
			ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
			Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword,
					"schema1");
			Statement s1 = c1.createStatement();
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
			RelationalDataSource ds1 = (RelationalDataSource) cl
					.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser
							+ "&password=" + defaultPassword, null);
			RelationalDataSource ds2 = (RelationalDataSource) cl
					.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser
							+ "&password=" + defaultPassword, null);
			//(new GraphPrintingByRepresentative()).printAllSources(cl);
			// System.out.println("DS1: " + cl.graph().getEdges(ds1));
			// System.out.println("DS2: " + cl.graph().getEdges(ds1));

			Node n1 = cl.graph().resolveNode(new NumericNodeID(14));
			Node n2 = cl.graph().resolveNode(new NumericNodeID(18)); // E. Macron
			Edge e1 = cl.graph().resolveEdge(new NumericEdgeID(5));
			Edge e2 = cl.graph().resolveEdge(new NumericEdgeID(17));

			Node m1 = cl.graph().resolveNode(new NumericNodeID(29));
			Node m2 = cl.graph().resolveNode(new NumericNodeID(31)); // E. Macron
			Node m3 = cl.graph().resolveNode(new NumericNodeID(37)); // E. Macron
			Edge f1 = cl.graph().resolveEdge(new NumericEdgeID(30));
			Edge f2 = cl.graph().resolveEdge(new NumericEdgeID(34));

			assertTrue(cl.graph().getNodes(ds1).containsAll(Sets.newHashSet(n1, n2)));
			assertFalse(cl.graph().getNodes(ds1).contains(Sets.newHashSet(m1)));
			assertTrue(cl.graph().getEdges(ds1).containsAll(Sets.newHashSet(e1, e2)));
			assertFalse(cl.graph().getEdges(ds1).contains(Sets.newHashSet(f1)));
			assertTrue(cl.graph().getNodes(ds1, "Emmanuel Macron").contains(n2));
			assertEquals(Sets.newHashSet(), cl.graph().getNodes(ds1, "1"));
			assertTrue(cl.graph().getEdges(ds2, "b").containsAll(Sets.newHashSet(f1)));
			assertEquals(Sets.newHashSet(), cl.graph().getEdges(ds1, "b"));
			assertTrue(cl.graph().getNodes("Emmanuel Macron").equals(Sets.newHashSet(n2, m2, m3)));
			assertTrue(cl.graph().getNodes("1").contains(m1));
			assertEquals(Sets.newHashSet(), cl.graph().getNodes("Jean-Paul Sartre"));
			assertEquals(Sets.newHashSet(), cl.graph().getEdges("nonsense"));
			assertEquals(10, cl.graph().countNodes(ds1));
			assertEquals(8, cl.graph().countEdges(ds1, EDGE));
			assertEquals(0, cl.graph().countEdges(ds1, EXTRACTION_EDGE));
			assertEquals(10, cl.graph().countNodes(ds2));
			assertEquals(8, cl.graph().countEdges(ds2, EDGE));
			assertEquals(0, cl.graph().countEdges(ds2, EXTRACTION_EDGE));
			cl.close();
			tearDownDB();
		} catch (Exception e) {
			e.printStackTrace();
			assert (false);
		}
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_LOCATION;
import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static fr.inria.cedar.connectionlens.graph.Node.Types.TEXT_VALUE;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static fr.inria.cedar.connectionlens.source.DataSource.ValueAtomicities.PER_INSTANCE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.PartOfSpeech;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class FulltextIndexTest extends ConnectionLensTest {

	protected static final Logger log = Logger.getLogger(FulltextIndexTest.class);

	String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();
	String path3 = Paths.get("file:data", "text", "input", "no_entity.txt").toString();
	String n1ID = "1|1|offset542|length16|ENTITY_PERSON";
	String m1ID = "2|2|offset837|length16|ENTITY_PERSON";
	String m2ID = "2|2|offset3156|length6|ENTITY_LOCATION";
	
	SimilarPairProcessor sim = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	EntityExtractor dummyExtractor = new TopLevelExtractor();
	EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	MorphoSyntacticAnalyser dummyAnalyzer = Mockito.mock(MorphoSyntacticAnalyser.class);

	{
		String mockPos = "MIAM";
		when(dummyAnalyzer.potentialEntityPOS()).thenReturn(ImmutableSet.of(mockPos));
		when(dummyAnalyzer.process(Matchers.anyString())).thenAnswer(new Answer<Multimap<String, PartOfSpeech>>() {
		    @Override
		    public Multimap<String, PartOfSpeech> answer(InvocationOnMock invocation) throws Throwable {
		      String[] tokens = ((String) invocation.getArguments()[0]).split(" |\\, |\\. |\\... |\\? |\\! |\\.|\\...|\\?|\\!");
		      Multimap<String, PartOfSpeech> result = LinkedHashMultimap.create();
		      int offset = 0;
		      for (String token: tokens) {
		    	  result.put(mockPos, new PartOfSpeech(token, token, mockPos, offset));
		    	  offset += token.length() + 1;
		      }
		      return result;
		    }
		  });
	}

	public FulltextIndexTest() {
		Config.getInstance().setProperty("update_batch_size", "1");
		Config.getInstance().setProperty("value_atomicity", PER_INSTANCE.name());
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
	}
	
	@Test public void textIndexEntities() throws SQLException {
		setUpDB();
		try(ConnectionLens cl = new ConnectionLens(extractor, analyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "json_04.json").toString());
			IndexAndProcessNodes index = cl.graph().index();
			Set<Node> nodes1 = index.getNodeMatches(new AtomicKeyword("paris"));
			assertEquals(2, nodes1.size()); 
			assertTrue(filterNonEntityMatches(nodes1).size()==1);
			Set<Node> nodes2 = index.getNodeMatches(new AtomicKeyword("londres"));
			assertEquals(2, nodes2.size());
			assertTrue(filterNonEntityMatches(nodes2).size()==1);
			assertEquals(index.getNodeMatches(new AtomicKeyword("rencontre")), index.getNodeMatches(new AtomicKeyword("l''ombre")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Test @Ignore public void testRegisterTwoJSONDatasets() {
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());

			//(new GraphPrintingByRepresentative()).printAllSources(cl);

			Edge ageEdge = cl.graph().resolveEdge (new NumericEdgeID(6)); 
			Edge authorEdge1 = cl.graph().resolveEdge (new NumericEdgeID(8)); 
			Edge authorEdge2 = cl.graph().resolveEdge (new NumericEdgeID(21)); 
			Node sartre1 = cl.graph().resolveNode (new NumericNodeID(7)); 
			Node sartre2 = cl.graph().resolveNode (new NumericNodeID(20)); 

			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(Sets.newHashSet(sartre1, sartre2), filterNonEntityMatches(index.getNodeMatches(new AtomicKeyword("sartre"))));
			assertEquals(Sets.newHashSet(ageEdge), 	index.getEdgeMatches(new AtomicKeyword("age")));
			assertEquals(Sets.newHashSet(authorEdge1, authorEdge2), index.getEdgeMatches(new AtomicKeyword("auteur")));
			assertEquals(Sets.newHashSet(authorEdge1, authorEdge2), index.getItemMatches(new AtomicKeyword("auteur")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(new AtomicKeyword("claude")));
			assertEquals(Sets.newHashSet(), index.getEdgeMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(ds1, ds2), index.getDataSourceMatches(new AtomicKeyword("sartre")));
			assertEquals(Sets.newHashSet(ds1), index.getDataSourceMatches(new AtomicKeyword("age")));
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace(); 
		} 
	}

	@Ignore @Test public void testRegisterTwoJSONDatasetsQueryComponents() throws SQLException {
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());

			Node n2 = cl.graph().resolveNode (new NumericNodeID(7));
			Node m1 = cl.graph().resolveNode (new NumericNodeID(22));
			Node m2 = cl.graph().resolveNode (new NumericNodeID(17));
			Edge e1 = cl.graph().resolveEdge (new NumericEdgeID(6));
			Edge e2 = cl.graph().resolveEdge (new NumericEdgeID(8));
			Edge f1 = cl.graph().resolveEdge (new NumericEdgeID(23));



			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(n2, m1), filterNonEntityMatches(index.getNodeMatches(Conjunction.of("jean-paul", "sartre"))));
			assertEquals(Sets.newHashSet(),   index.getNodeMatches(Conjunction.of("clos", "sartre")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(NGram.of("clos", "sartre")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, n2, m1), filterNonEntityMatches(index.getNodeMatches(Disjunction.of("huis", "sartre"))));
			assertEquals(Sets.newHashSet(n2, m1), filterNonEntityMatches(index.getNodeMatches(Disjunction.of("lion", "sartre"))));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(Disjunction.of("lion", "merle")));

			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getItemMatches(Conjunction.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, e2, f1), index.getItemMatches(Disjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(e2, f1), index.getItemMatches(Disjunction.of("lion", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(Disjunction.of("lion", "merle")));

			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getItemMatches(ds2, Conjunction.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, f1), index.getItemMatches(ds2, Disjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(f1), index.getItemMatches(ds2, Disjunction.of("lion", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, Disjunction.of("lion", "merle")));
			
			assertEquals(Sets.newHashSet(e1, e2, f1), index.getEdgeMatches(Disjunction.of("age", "auteur")));
			assertEquals(Sets.newHashSet(e1), index.getEdgeMatches(Disjunction.of("age", "lion")));
			assertEquals(Sets.newHashSet(), index.getEdgeMatches(Disjunction.of("lion", "merle")));
			
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getDataSourceMatches(Conjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(ds1, ds2), index.getDataSourceMatches(Disjunction.of("age", "auteur")));
			assertEquals(Sets.newHashSet(ds1), index.getDataSourceMatches(Disjunction.of("age", "lion")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(Disjunction.of("lion", "merle")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}


	@Test
	public void testRegisterTwoRDFDatasets() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("update_batch_size", "1");
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
			DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
			Node n2 = cl.graph().resolveNode (new NumericNodeID(6));
			Node m2 = cl.graph().resolveNode (new NumericNodeID(20));

			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getNodeMatches(new AtomicKeyword("sartwell")));
			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getItemMatches(new AtomicKeyword("sartwell")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("claude")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("sartwell")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("pr")));
			Config.getInstance().setProperty ("extract_from_uris","true");
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace(); 
		}
	}

	@Test public void testRegisterTwoRelationalDatasets()  throws SQLException {
		setUpDB("schema1", "schema2");
		Config.getInstance().setProperty("update_batch_size", "1");
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
					Statement s1 = c1.createStatement()) {
					s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
					s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
					s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
					s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
					s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
					s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
				}
			try(RelationalDataSource ds1 = (RelationalDataSource) cl.register( 
					"jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword);
				RelationalDataSource ds2 = (RelationalDataSource) cl.register(  
					"jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword)) {

				Node n2 = cl.graph().resolveNode (new NumericNodeID(18));
				Edge e2 = cl.graph().resolveEdge (new NumericEdgeID(32));
				Node m3 = cl.graph().resolveNode (new NumericNodeID(37));
				Edge f2 = cl.graph().resolveEdge (new NumericEdgeID(38));


				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getEdgeMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("macron")));
				assertEquals(Sets.newHashSet(ds1), cl.graph().index().getDataSourceMatches(new AtomicKeyword("doe")));
				cl.close();
				tearDownDB();
			}
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Ignore public void testRegisterTwoTextDatasets() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("update_batch_size", "1");
		try(ConnectionLens cl = new ConnectionLens(extractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(path1);
			DataSource ds2 = cl.register(path2);

			String textp1 = IOUtils.slurpURL(path1);
			String textp2 = IOUtils.slurpURL(path2);

			Node ntx1 = ds1.buildLabelNodeOfType(textp1, TEXT_VALUE);

			Node mtx1 = ds2.buildLabelNodeOfType(textp2, TEXT_VALUE);

			Properties p1 = new Properties(), p2 = new Properties(), p3 = new Properties();
			p1.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "542");
			p1.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node n1 = ds1.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p1);
			p2.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "837");
			p2.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node m1 = ds2.buildChildNode(mtx1.getId(), "Edouard Philippe", ENTITY_PERSON, p2);
			p3.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "3156");
			p3.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "6");
			Node m2 = ds2.buildChildNode(mtx1.getId(), "Nantes", ENTITY_LOCATION, p3);

			if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_OCCURRENCE")){
				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1, m1)));
			}
			if(Config.getInstance().getProperty ("entity_node_creation_policy").equals ("PER_GRAPH")){
				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1)));
			}
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("edouard")));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("nantes")).containsAll(Sets.newHashSet(m2)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("edouard")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nantes")));
			cl.close();
			tearDownDB();
		} catch (Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}

	}
}
/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.LUCENE;
import static java.util.Locale.FRENCH;

import java.sql.SQLException;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.indexing.IndexAndProcessNodes.IndexAndProcessNodesSession;
import fr.inria.cedar.connectionlens.sql.schema.PostgresFullTextIndexSession;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

public class IndexSessionTest extends ConnectionLensTest {
	static Config config = Config.getInstance();
	static EntityExtractor extractor = new TopLevelExtractor();
	static MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(config);

	@Test
	public void testPostgresFullTextIndexSessionNumericIDs() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("indexing_model", "POSTGRES_FULLTEXT");
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		Graph g = cl.graph();
		Assert.assertTrue(cl.graph().index().openSession(-1000, g) instanceof PostgresFullTextIndexSession);
		Assert.assertTrue(cl.graph().index().openSession(-1, g) instanceof PostgresFullTextIndexSession);
		Assert.assertTrue(cl.graph().index().openSession(0, g) instanceof PostgresFullTextIndexSession);
		Assert.assertTrue(cl.graph().index().openSession(1, g) instanceof PostgresFullTextIndexSession);
		Assert.assertTrue(cl.graph().index().openSession(2, g) instanceof PostgresFullTextIndexSession);
		Assert.assertTrue(cl.graph().index().openSession(1000, g) instanceof PostgresFullTextIndexSession);
		cl.close();
		tearDownDB();
	}

	@Test
	public void testLuceneSessionNumericIDs() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("indexing_model", LUCENE.name());
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		Graph g = cl.graph();
		try (IndexAndProcessNodesSession is1 = cl.graph().index().openSession(-1000, g)) {
			Assert.assertTrue(is1 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		try (IndexAndProcessNodesSession is2 = cl.graph().index().openSession(-1, g)) {
			Assert.assertTrue(is2 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		try (IndexAndProcessNodesSession is3 = cl.graph().index().openSession(0, g)) {
			Assert.assertTrue(is3 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		try (IndexAndProcessNodesSession is4 = cl.graph().index().openSession(1, g)) {
			Assert.assertTrue(is4 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		try (IndexAndProcessNodesSession is5 = cl.graph().index().openSession(2, g)) {
			Assert.assertTrue(is5 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		try (IndexAndProcessNodesSession is6 = cl.graph().index().openSession(1000, g)) {
			Assert.assertTrue(is6 instanceof fr.inria.cedar.connectionlens.indexing.lucene.LuceneBasedIndexSession);
		}
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		cl.close();
		tearDownDB();
	}
	@AfterClass
	public static void tearDown() {
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		log.info("Restored " + POSTGRES_FULLTEXT.name() + " indexing"); 
		
	}
}

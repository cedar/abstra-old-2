/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens;

import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ExperimentTest extends ConnectionLensTest {


	@Test public void testParameterInteractive() {
		Assert.assertTrue(new Experiment("-a").isInteractive());
		Assert.assertFalse(new Experiment().isInteractive());
	}

//	@Test public void testParameterResetBetweenDatabases() {
//		Assert.assertTrue(new Experiment("-r").isRBD());
//		Assert.assertFalse(new Experiment().isRBD());
//	}

	@Test public void testParameterResetFirst() {
		Assert.assertTrue(new Experiment().isResetFirst());
		Assert.assertFalse(new Experiment("-R").isResetFirst());
	}

	@Test public void testParameterVerbose() {
		Assert.assertTrue(new Experiment("-v").isVerbose());
		Assert.assertFalse(new Experiment().isVerbose());
	}

	@Test public void testParameterEmptyInputList() {
		Assert.assertEquals(ImmutableList.of(), new Experiment("-i").getInputs());
	}

	@Test public void testParameterSingleInputList() {
		Assert.assertEquals(ImmutableList.of("file1"), new Experiment("-i", "filepath").getInputs());
	}


	@Test public void testParameterDoubleInputList() {
		Assert.assertEquals(ImmutableList.of("file1", "file2"), new Experiment("-i", "file1,file2").getInputs());
	}
}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import edu.stanford.nlp.io.IOUtils;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.*;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.EntityOccurrenceNode;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import static fr.inria.cedar.connectionlens.extraction.EntityType.isEntityType;
import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@Ignore
// this test was implemented for the SQL Index model, for now only the POSTGRE
public class RelationalIndexTest extends ConnectionLensTest {

	String path1 = Paths.get("file:data", "text", "input", "lemonde.txt").toString();
	String path2 = Paths.get("file:data", "text", "input", "piste.txt").toString();
	String path3 = Paths.get("file:data", "text", "input", "no_entity.txt").toString();
	String n1ID = "1|1|offset542|length16|ENTITY_PERSON";
	String m1ID = "2|2|offset837|length16|ENTITY_PERSON";
	String m2ID = "2|2|offset3156|length6|ENTITY_LOCATION";
	
	SimilarPairProcessor sim = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	EntityExtractor dummyExtractor = new TopLevelExtractor();
	EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	MorphoSyntacticAnalyser dummyAnalyzer = Mockito.mock(MorphoSyntacticAnalyser.class);
	{
		String mockPos = "MIAM";
		when(dummyAnalyzer.potentialEntityPOS()).thenReturn(ImmutableSet.of(mockPos));
		when(dummyAnalyzer.process(Matchers.anyString())).thenAnswer(new Answer<Multimap<String, PartOfSpeech>>() {
		    @Override
		    public Multimap<String, PartOfSpeech> answer(InvocationOnMock invocation) throws Throwable {
		      String[] tokens = ((String) invocation.getArguments()[0]).split(" |\\, |\\. |\\... |\\? |\\! |\\.|\\...|\\?|\\!");
		      Multimap<String, PartOfSpeech> result = LinkedHashMultimap.create();
		      int offset = 0;
		      for (String token: tokens) {
		    	  result.put(mockPos, new PartOfSpeech(token, token, mockPos, offset));
		    	  offset += token.length() + 1;
		      }
		      return result;
		    }
		  });
	}

	@BeforeClass
	public static void setup() {
		Config.getInstance().setProperty("update_batch_size", "1");
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
	}

	@Test public void textIndexEntities() {
		try(ConnectionLens cl = new ConnectionLens(extractor, analyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "json_04.json").toString());
			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(2, index.getNodeMatches(new AtomicKeyword("paris")).size());
			assertTrue(isEntityType(index.getNodeMatches(new AtomicKeyword("paris")).iterator().next().getType()));
			assertEquals(1, index.getNodeMatches(new AtomicKeyword("londres")).size());
			assertTrue(isEntityType(index.getNodeMatches(new AtomicKeyword("londres")).iterator().next().getType()));
			assertEquals(index.getNodeMatches(new AtomicKeyword("rencontre")), index.getNodeMatches(new AtomicKeyword("l'ombre")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Test public void testRegisterTwoJSONDatasets() {
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {

			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());
			Node n0 = ds1.buildEmptyLabelNodeOfType(JSON_STRUCT);
			Node n1 = ds1.buildLabelNodeOfType("45", JSON_VALUE);
			Node n2 = ds1.buildLabelNodeOfType("Jean-Paul Sartre", JSON_VALUE);
			Node m0 = ds2.buildEmptyLabelNodeOfType(JSON_STRUCT);
			Node m1 = ds2.buildLabelNodeOfType("Jean-Paul Sartre", JSON_VALUE);
			Edge e0 = ds1.getRoot().buildEdge(n0);
			Edge e1 = n0.buildEdge(n1, "age");
			Edge e2 = n0.buildEdge(n2, "auteur");
			Edge f1 = m0.buildEdge(m1, "auteur");
			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(Sets.newHashSet(n2, m1), index.getNodeMatches(new AtomicKeyword("sartre")));
			assertEquals(Sets.newHashSet(e1), 	index.getEdgeMatches(new AtomicKeyword("age")));
			assertEquals(Sets.newHashSet(e2, f1), index.getEdgeMatches(new AtomicKeyword("auteur")));
			assertEquals(Sets.newHashSet(n2, m1), index.getItemMatches(new AtomicKeyword("sartre")));
			assertEquals(Sets.newHashSet(e1), index.getItemMatches(new AtomicKeyword("age")));
			assertEquals(Sets.newHashSet(e2, f1), index.getItemMatches(new AtomicKeyword("auteur")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(new AtomicKeyword("claude")));
			assertEquals(Sets.newHashSet(), index.getEdgeMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(ds1, ds2), index.getDataSourceMatches(new AtomicKeyword("sartre")));
			assertEquals(Sets.newHashSet(ds1), index.getDataSourceMatches(new AtomicKeyword("age")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Test public void testRegisterTwoJSONDatasetsQueryComponents() {
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
			DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());
			Node n0 = ds1.buildEmptyLabelNodeOfType(JSON_STRUCT);
			Node n1 = ds1.buildLabelNodeOfType("45", JSON_VALUE);
			Node n2 = ds1.buildLabelNodeOfType("Jean-Paul Sartre", JSON_VALUE);
			Node m0 = ds2.buildEmptyLabelNodeOfType(JSON_STRUCT);
			Node m1 = ds2.buildLabelNodeOfType("Jean-Paul Sartre", JSON_VALUE);
			Node m2 = ds2.buildLabelNodeOfType("Huis Clos", JSON_VALUE);
			Edge e0 = ds1.getRoot().buildEdge(n0);
			Edge e1 = n0.buildEdge(n1, "age");
			Edge e2 = n0.buildEdge(n2, "auteur");
			Edge f1 = m0.buildEdge(m1, "auteur");
			IndexAndProcessNodes index = cl.graph().index();
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(n2, m1),   index.getNodeMatches(Conjunction.of("jean-paul", "sartre")));
			assertEquals(Sets.newHashSet(),   index.getNodeMatches(Conjunction.of("clos", "sartre")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(NGram.of("clos", "sartre")));
			assertEquals(Sets.newHashSet(m2), index.getNodeMatches(Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, n2, m1), index.getNodeMatches(Disjunction.of("huis", "sartre")));
			assertEquals(Sets.newHashSet(n2, m1), index.getNodeMatches(Disjunction.of("lion", "sartre")));
			assertEquals(Sets.newHashSet(), index.getNodeMatches(Disjunction.of("lion", "merle")));
	
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getItemMatches(Conjunction.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, e2, f1), index.getItemMatches(Disjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(e2, f1), index.getItemMatches(Disjunction.of("lion", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(Disjunction.of("lion", "merle")));
	
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getItemMatches(ds2, Conjunction.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(m2), index.getItemMatches(ds2, Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds1, Disjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(m2, f1), index.getItemMatches(ds2, Disjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(f1), index.getItemMatches(ds2, Disjunction.of("lion", "auteur")));
			assertEquals(Sets.newHashSet(), index.getItemMatches(ds2, Disjunction.of("lion", "merle")));
			
			assertEquals(Sets.newHashSet(e1, e2, f1), index.getEdgeMatches(Disjunction.of("age", "auteur")));
			assertEquals(Sets.newHashSet(e1), index.getEdgeMatches(Disjunction.of("age", "lion")));
			assertEquals(Sets.newHashSet(), index.getEdgeMatches(Disjunction.of("lion", "merle")));
			
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("huis", "clos")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(Conjunction.of("clos", "huis")));
			assertEquals(Sets.newHashSet(),   index.getDataSourceMatches(Conjunction.of("huis", "auteur")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "huis")));
			assertEquals(Sets.newHashSet(ds2), index.getDataSourceMatches(NGram.of("huis", "clos")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(NGram.of("clos", "auteur")));
			assertEquals(Sets.newHashSet(ds1, ds2), index.getDataSourceMatches(Disjunction.of("age", "auteur")));
			assertEquals(Sets.newHashSet(ds1), index.getDataSourceMatches(Disjunction.of("age", "lion")));
			assertEquals(Sets.newHashSet(), index.getDataSourceMatches(Disjunction.of("lion", "merle")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Test
	public void testRegisterTwoRDFDatasets() {
		Config.getInstance().setProperty("update_batch_size", "1");
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
			DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
			Node n1 = ds1.buildLabelNodeOfType("http://inria.fr/book", RDF_URI);
			Node n2 = ds1.buildLabelNodeOfType( "Sartwell, Crispin", RDF_LITERAL);
			Edge e1 = n1.buildEdge(n2, "http://dbpedia.org/property/author");

			Node m1 = ds2.buildLabelNodeOfType("http://inria.fr/book", RDF_URI);
			Node m2 = ds2.buildLabelNodeOfType("Sartwell, Crispin", RDF_LITERAL);
			Node m3 = ds2.buildLabelNodeOfType("http://inria.fr/doi2", RDF_URI);
			Edge f1 = m1.buildEdge(m2, "http://dbpedia.org/property/author");
			Edge f2 = m1.buildEdge(m3, "http://dbpedia.org/property/url");

			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getNodeMatches(new AtomicKeyword("sartwell")));
			assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("http://inria.fr/book")).containsAll(Sets.newHashSet(n1, m1)));
			assertTrue(cl.graph().index().getEdgeMatches(new AtomicKeyword("http://dbpedia.org/property/author")).containsAll(Sets.newHashSet(e1, f1)));
			assertEquals(Sets.newHashSet(n2, m2), cl.graph().index().getItemMatches(new AtomicKeyword("sartwell")));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("http://inria.fr/book")).containsAll(Sets.newHashSet(n1, m1)));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("http://dbpedia.org/property/author")).containsAll(Sets.newHashSet(e1, f1)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("claude")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("size")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("sartwell")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("pr")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Test public void testRegisterTwoRelationalDatasets()  throws SQLException {
		setUpDB("schema1", "schema2");
		Config.getInstance().setProperty("update_batch_size", "1");
		try(ConnectionLens cl = new ConnectionLens(dummyExtractor, dummyAnalyzer, sim, true)) {
			try(Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
				Statement s1 = c1.createStatement()) {
				s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
				s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
				s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
				s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
				s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
				s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
			}
			try(RelationalDataSource ds1 = (RelationalDataSource) cl.register( 
					"jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword);
				RelationalDataSource ds2 = (RelationalDataSource) cl.register(  
					"jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword)) {
				Node n0 = ds1.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
				Node n1 = ds1.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
				Node n2 = ds1.buildLabelNodeOfType("Emmanuel Macron", RELATIONAL_VALUE);
				Edge e1 = ds1.getRoot().buildEdge(n0);
				Edge e2 = n1.buildEdge(n2, "name");

				Node m1 = ds2.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
				Node m2 = ds2.buildLabelNodeOfType("1", RELATIONAL_VALUE);
				Node m3 = ds2.buildLabelNodeOfType("Emmanuel Macron", RELATIONAL_VALUE);
				Edge f1 = m1.buildEdge(m2);
				Edge f2 = m1.buildEdge(m3, "name");
				
				assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getEdgeMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("macron")).containsAll(Sets.newHashSet(n2, m3)));
				assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("name")).containsAll(Sets.newHashSet(e2, f2)));
				assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
				assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("macron")));
				assertEquals(Sets.newHashSet(ds1), cl.graph().index().getDataSourceMatches(new AtomicKeyword("doe")));
				cl.close();
				tearDownDB();
			}
			catch(Exception e) {
				log.error(e.getMessage()); 
				e.printStackTrace();
			}
		}
	}

	@Test public void testRegisterTwoTextDatasets() {
		Config.getInstance().setProperty("update_batch_size", "1");
		//@Tayeb: I update the indexing_model to Postgres since the SQL generate an exception on `edouard` name.
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		try(ConnectionLens cl = new ConnectionLens(extractor, dummyAnalyzer, sim, true)) {
			DataSource ds1 = cl.register(path1);
			DataSource ds2 = cl.register(path2);
			String textp1 = IOUtils.slurpURL(path1);
			String textp2 = IOUtils.slurpURL(path2);
			Node ntx1 = ds1.buildLabelNodeOfType(textp1, TEXT_VALUE);

			Node mtx1 = ds2.buildLabelNodeOfType(textp2, TEXT_VALUE);

			Properties p1 = new Properties(), p2 = new Properties(), p3 = new Properties(); 
			p1.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "542");
			p1.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node n1 = ds1.buildChildNode(ntx1.getId(), "Edouard Philippe", ENTITY_PERSON, p1);
			p2.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "837");
			p2.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "16");
			Node m1 = ds2.buildChildNode(mtx1.getId(), "Edouard Philippe", ENTITY_PERSON, p2);
			p3.setProperty(EntityOccurrenceNode.Metadata.OFFSET.toString(), "3156");
			p3.setProperty(EntityOccurrenceNode.Metadata.LENGTH.toString(), "6");
			Node m2 = ds2.buildChildNode(mtx1.getId(), "Nantes", ENTITY_LOCATION, p3);
			assertTrue(cl.graph().index().getNodeMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1, m1)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("")));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("edouard")).containsAll(Sets.newHashSet(n1, m1)));
			assertTrue(cl.graph().index().getItemMatches(new AtomicKeyword("nantes")).containsAll(Sets.newHashSet(m2)));
			assertEquals(Sets.newHashSet(), cl.graph().index().getNodeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getEdgeMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getItemMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nonsense")));
			assertEquals(Sets.newHashSet(ds1, ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("edouard")));
			assertEquals(Sets.newHashSet(ds2), cl.graph().index().getDataSourceMatches(new AtomicKeyword("nantes")));
			cl.close();
			tearDownDB();
		}
		catch(Exception e) {
			log.error(e.getMessage()); 
			e.printStackTrace();
		}
	}
}
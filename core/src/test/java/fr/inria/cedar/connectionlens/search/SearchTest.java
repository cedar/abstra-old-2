/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import static fr.inria.cedar.connectionlens.util.StatisticsKeys.ANSW_NO;
import static fr.inria.cedar.connectionlens.util.StatisticsKeys.STOP;
import static java.util.Locale.FRENCH;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

import org.junit.AfterClass;

import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * @author Tayeb Merabti
 */
public abstract class SearchTest extends ConnectionLensTest {

    static EntityExtractor extractor = new StanfordNERExtractor(FRENCH);
    static MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
    static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
    StatisticsCollector stats = StatisticsCollector.mute();
    static List<URI> inputs = new ArrayList<> ();
    static HashMap<URI,URI> originalURIs = new HashMap<> ();
    static ConnectionLens cl; 
	

	/**
	 *
	 * @param querySearch
	 * @param query
	 * @return return the Set of answerTrees
	 */
	protected Set<AnswerTree> getResults(QuerySearch querySearch, Query query){//, ScoringFunction sf) {
		final SortedSet<AnswerTree> resultSet = new ConcurrentSkipListSet<>(
				new AnswerTree.Comparator(querySearch.scoring()));
		try {
			querySearch.run(query, a -> { 
				resultSet.add(a);
				//log.info(a);
				//log.info(resultSet.size()); 
				} 
			); 
		} catch (ConditionReachedException e) {
			stats.put(query, STOP, e.getMessage());

		} catch (EvaluationException e) {
			stats.put(query, STOP, e.getClass().getSimpleName());
		} finally {
			stats.put(query, ANSW_NO, resultSet.size());
		}
		return resultSet;
	}
	
}

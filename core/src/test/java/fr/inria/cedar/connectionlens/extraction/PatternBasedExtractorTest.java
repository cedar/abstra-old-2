package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.junit.*;


import static fr.inria.cedar.connectionlens.graph.Node.Types.*;
import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;
import java.sql.SQLException;

public class PatternBasedExtractorTest extends ConnectionLensTest {

	
    @Test
    public void testExtractMentions() throws SQLException {
    	setUpDB();
        PatternBasedExtractor extractor = new MentionExtractor(ENGLISH);
        SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
        MorphoSyntacticAnalyser analyzer = new TreeTagger(extractor.getLocale());
        ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
        Graph graph = cl.graph();

        DataSource ds = cl.register(Paths.get("data", "text", "input", "tweet.txt").toString());
        assertEquals(1, graph.countNodes(ds, MENTION));
        cl.close();
        tearDownDB();
    }

    @Test
    public void testExtractHashtags() throws SQLException {
    	setUpDB();
        PatternBasedExtractor extractor = new HashtagExtractor(ENGLISH);
        SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
        MorphoSyntacticAnalyser analyzer = new TreeTagger(extractor.getLocale());
        ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
        Graph graph = cl.graph();

        DataSource ds1 = cl.register(Paths.get("data", "text", "input", "tweet.txt").toString());
        assertEquals(1, graph.countNodes(ds1, HASHTAG));
        
        DataSource ds2 = cl.register(Paths.get("data", "rdf", "chain-Alice-Bob-10.nt").toString());
        assertEquals(0, graph.countNodes(ds2, HASHTAG)); // hashtags in RDF URI should not be extracted
        cl.close();
        tearDownDB();
    }

    @Test 
    public void testExtractEmails() throws SQLException {
    	setUpDB();
        PatternBasedExtractor extractor = new EmailExtractor(ENGLISH);
        SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
        MorphoSyntacticAnalyser analyzer = new TreeTagger(extractor.getLocale());
        ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
        Graph graph = cl.graph();

        DataSource ds = cl.register(Paths.get("data", "text", "input", "mail.txt").toString());
        
        assertEquals(6, graph.countNodes(ds, EMAIL));
        cl.close();
        tearDownDB();
    }
    
	
}

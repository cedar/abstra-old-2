/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Node.Types.HTML_NODE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.HTML_VALUE;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class HTMLDataSourceTest extends ConnectionLensTest {

	static EntityExtractor extractor = new TopLevelExtractor();
	static MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	static ConnectionLens cl;

	@BeforeClass
	public static void setupClass() throws SQLException {
		Config.getInstance().setProperty("update_batch_size", "1");
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
	}

	// tests registerDataset with the simplest example
	@Ignore
	@Test
	public void testRegisterTwoDatasets() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		DataSource ds1 = cl.register(Paths.get("data", "html", "example_meta1.html").toString());
		DataSource ds2 = cl.register(Paths.get("data", "html", "example_meta2.html").toString());
		Node n0 = ds1.buildEmptyLabelNodeOfType(HTML_NODE); // ds
		Node n1 = ds1.buildEmptyLabelNodeOfType(HTML_NODE); // head
		Node n2 = ds1.buildLabelNodeOfType("", HTML_NODE); // meta
		// Node n3 = ds1.buildNode("1.1.2@1", "author", HTML_VALUE); // name
		Node n3 = ds1.buildLabelNodeOfType("La Rédaction De Mediapart", HTML_VALUE); // content

		Node m0 = ds2.buildEmptyLabelNodeOfType(HTML_NODE);
		Node m1 = ds2.buildEmptyLabelNodeOfType(HTML_NODE);
		Node m2 = ds2.buildLabelNodeOfType("description", HTML_VALUE);

		Edge e0 = ds1.getRoot().buildEdge(n0, Edge.ROOT_LABEL);
		Edge e1 = n0.buildEdge(n1, "head");
		Edge e2 = n1.buildEdge(n2, "meta");
		Edge e3 = n2.buildEdge(n3, "author");

		Graph graph = cl.graph();
		assertTrue(graph.getNodes(ds1).containsAll(Sets.newHashSet(ds1.getRoot(), n0, n1, n2)));
		assertTrue(graph.getEdges(ds1).containsAll(Sets.newHashSet(e0, e1, e2, e3)));
		assertTrue(graph.getNodes(ds1, "La Rédaction De Mediapart").containsAll(Sets.newHashSet(n3)));
		assertEquals(Sets.newHashSet(n3), graph.getNodes(ds1, "La Rédaction De Mediapart"));
		assertEquals(Sets.newHashSet(e1), graph.getEdges(ds1, "head"));
		assertEquals(23, graph.countEdges(ds1));
		assertEquals(24, graph.countEdges(ds2));
		assertEquals(24, graph.countNodes(ds1));
		assertEquals(25, graph.countNodes(ds2));
		cl.close();
		tearDownDB();
	}

	@Test
	public void testAdjacentEdges() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		//Config.getInstance().setProperty("drawing.draw", "true");
		DataSource ds = cl.register(Paths.get("data", "html", "example_meta1.html").toString(), "");
		//(new GraphPrintingByRepresentative()).printAllSources(cl);

		Node n1 = cl.graph().resolveNode(new NumericNodeID(14));
		Edge e0 = cl.graph().resolveEdge(new NumericEdgeID(17));
		Edge e1 = cl.graph().resolveEdge(new NumericEdgeID(16));

		Set<Edge> adj = cl.graph().getAdjacentEdges(n1);

		assertTrue(adj.contains(e0));
		assertTrue(adj.contains(e1));
		assertEquals(2, adj.size());
		
		cl.close();
		tearDownDB();
	}

	@Test
	public void testAtomicity1() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_GRAPH");
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "html", "atomicity1.html").toString());
		cl.register(Paths.get("data", "html", "atomicity2.html").toString());

		assertEquals(1, cl.graph().getNodes("rapport").size());
		assertEquals(1, cl.graph().getNodes("rapport periode1").size());

		cl.close();
		tearDownDB();
	}
	@Test
	public void testAtomicity2() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_PATH");
		cl = new ConnectionLens(extractor, analyzer, pp, true);

		cl.register(Paths.get("data", "html", "atomicity1.html").toString());
		cl.register(Paths.get("data", "html", "atomicity2.html").toString());
		assertEquals(2, cl.graph().getNodes("rapport").size());

		cl.close();
		tearDownDB();
	}
	@Test
	public void testAtomicity3() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_DATASET");
		cl = new ConnectionLens(extractor, analyzer, pp, true);

		DataSource ds5 = cl.register(Paths.get("data", "html", "atomicity1.html").toString());
		DataSource ds6 = cl.register(Paths.get("data", "html", "atomicity2.html").toString());

		assertEquals(2, cl.graph().getNodes("rapport").size());

		assertEquals(2, cl.graph().getNodes("rapport periode1").size());
		assertEquals(1, cl.graph().getNodes(ds5, "rapport").size());
		assertEquals(1, cl.graph().getNodes(ds6, "rapport").size());
		cl.close();
		tearDownDB();
	}
	
	@Test
		public void testAtomicity4() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		cl = new ConnectionLens(extractor, analyzer, pp, true);

		cl.register(Paths.get("data", "html", "atomicity1.html").toString());
		cl.register(Paths.get("data", "html", "atomicity2.html").toString());

		assertEquals(4, cl.graph().getNodes("rapport").size());
		cl.close();
		tearDownDB();
	}
	// afterclass, we have the tearDownDB of the parent class
	@AfterClass
	public static void restoreDefaults() {
		Config.getInstance().setProperty("value_atomicity", "PER_OCCURRENCE");
		Config.getInstance().setProperty("extractor", "SNER");
	}
}

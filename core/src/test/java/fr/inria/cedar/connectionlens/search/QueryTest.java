/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.indexing.AtomicKeyword;
import fr.inria.cedar.connectionlens.indexing.NGram;
import org.junit.Assert;
import org.junit.Test;

public class QueryTest extends ConnectionLensTest {

	@Test
	public void testQueryPreprocess1() {
		Query q = Query.parse("\"Jean-Paul Sartre\" \"Emmanuel Macron\" \"Huis Clos\" 45 \"03 Avril 2018\""
				+ "http://books.google.com/books?id=bk-aaMVGKO0C");
		Assert.assertEquals(4l, q.getComponents().stream().filter(NGram.class::isInstance).count());
		Assert.assertEquals(2l, q.getComponents().stream().filter(AtomicKeyword.class::isInstance).count());
	}

	@Test
	public void testQueryPreprocess2() {
		Query q = Query.parse("Clos Nausée 45");
		Assert.assertEquals(0l, q.getComponents().stream().filter(NGram.class::isInstance).count());
		Assert.assertEquals(3l, q.getComponents().stream().filter(AtomicKeyword.class::isInstance).count());
	}

	@Test
	public void testQueryPreprocess3() {
		Query q = Query.parse("\"Jean d'Ormesson\"");
		Assert.assertEquals(1l, q.getComponents().stream().filter(NGram.class::isInstance).count());
		Assert.assertEquals(0l, q.getComponents().stream().filter(AtomicKeyword.class::isInstance).count());
		Assert.assertEquals(3l, ((NGram) q.getComponents().get(0)).size());
	}
}

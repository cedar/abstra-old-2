/*
 * Copyright(C) 2021 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.ENGLISH;
import static org.junit.Assert.*;

import java.nio.file.Paths;
import java.sql.SQLException;

import fr.inria.cedar.connectionlens.graph.Edge;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.SilentStatsCollector;

/**
 * Test for {@link CSVDataSource}.
 *
 * @author Prajna Upadhyay
 *
 */
public class CSVDataSourceTest extends ConnectionLensTest {

    static EntityExtractor extractor;
    static MorphoSyntacticAnalyser analyzer;
    static SimilarPairProcessor pp;

    @BeforeClass
    public static void setupClass() {
        Config.getInstance().setProperty("default_locale", "en");
        extractor = new CacheExtractor(new StanfordNERExtractor(ENGLISH), new SilentStatsCollector(), 0, 0);
        analyzer = new TreeTagger(ENGLISH);
        pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
    }

    @Test
    public void test() throws SQLException {
        setUpDB();
        ConnectionLens cl =  new ConnectionLens(extractor, analyzer, pp, true);
        //log.info("Language locale is: " + Config.getInstance().getProperty("default_locale"));
        cl.register(Paths.get("data", "csv", "deputes2018-10.csv").toString());
        Graph graph = cl.graph();

        assertEquals(1,graph.getNodes(Types.DATA_SOURCE).size());
        assertEquals(9,graph.getEdges(Edge.TUPLE_EDGE_LABEL).size());
        assertEquals(245,graph.getNodes(Types.RELATIONAL_VALUE).size());
        cl.close();
        tearDownDB();
    }

    @AfterClass
    public static void afterClass() {
        Config.getInstance().setProperty("default_locale", "fr");
    }

}

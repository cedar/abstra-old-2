/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;

/**
 * @author Tayeb Merabti Test the pdf extractions scripts developed by @Youssr.
 *         The original project is located here:
 *         https://gitlab.inria.fr/cedar/pdf-integration/ Python scripts are
 *         executed into CL via `runScrapingPDF` from PythonUtils class.
 */
public class PDFDataSourceTest extends ConnectionLensTest {

	EntityExtractor extractor = new TopLevelExtractor();
	MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(ENGLISH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());

	@Test
	public void generic() throws SQLException {
		setUpDB();
		boolean failed = false;
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);

		List<DataSource> dataSources = Lists.newLinkedList();
		Config.getInstance().setProperty("type_pdf", "generic");
		try {
			cl.registerPDF(Paths.get("data", "pdf", "test_files", "generic.pdf").toString()).forEach(dataSources::add);
			cl.graph().countEdgesForSpecificity();
			cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
			assertEquals(1, dataSources.size());

			// The next two blocks print the counts for all types of nodes and edges
//		for (Node.Types t: Node.Types.values()) {
//			System.out.println(t + ": " + Node.decodeType(t) + " " + cl.graph().getNodes(t).size());
//		}
//		for (Edge.Types t: Edge.Types.values()) {
//			System.out.println(t + ": " + Edge.decodeType(t) + " " + cl.graph().getEdges(dataSources.get(0), t).size());
//		}
			// the next asserts based on all the non-0 counts as given by the code on
			// 26/3/21
			assertEquals(cl.graph().getNodes(Node.Types.DATA_SOURCE).size(), 1); // DATA_SOURCE: data source 1
			assertEquals(cl.graph().getNodes(Node.Types.JSON_STRUCT).size(), 2); // JSON_STRUCT: JSON struct 2
			assertEquals(cl.graph().getNodes(Node.Types.JSON_VALUE).size(), 59); // JSON_VALUE: JSON value 59
			assertEquals(cl.graph().getNodes(Node.Types.RDF_URI).size(), 2); // RDF_URI: URI 2
			assertEquals(cl.graph().getEdges(cl.catalog(), Edge.Types.EDGE).size(), 62); // EDGE: edge 62
			assertEquals(cl.graph().getEdges(cl.catalog(), Edge.Types.LOCAL_URL).size(), 1); // LOCAL_URL: cl:local_uri
		} catch (Exception e) {
			e.printStackTrace();
			failed = true;
		}
		cl.close();
		tearDownDB();
		if (failed) {
			assert (false);
		}
	}

	// @Ignore
	// @IM, 8/3/21: the last two tests here rely on graph serialization and we
	// should no longer use this method for tests.
	// The tests are no longer here, but it's not clear if they are in the Python
	// project?
	// TM: 08/12/2020
	// Ignored since only generic parameter is loaded.
//	public void eos_extraction() throws Exception {
//		cl.reset();
//		List<DataSource> dataSources = Lists.newLinkedList();
//		Config.getInstance().setProperty("type_pdf", "cois_EFSA");
//		cl.registerPDF("data/pdf/test_files/cois_efsa.pdf").forEach(dataSources::add);
//		assertEquals(2, dataSources.size());
//		DataSource ds1 = dataSources.get(0);
//		assertEquals(49, graph.getNodes("%table0%").size());
//		assertEquals(0, graph.getNodes("%table1%").size());
//		assertEquals(1, graph.countNodes(ds1, ENTITY_PERSON));
//		assertEquals(1, graph.countNodes(ds1, ENTITY_LOCATION));
//		assertEquals(8, graph.countNodes(ds1, ENTITY_ORGANIZATION));
//	}

	// @Ignore
	// @IM, 8/3/21: the last two tests here rely on graph serialization and we
	// should no longer use this method for tests.
	// The tests are no longer here, but it's not clear if they are in the Python
	// project?
	// @TM: 16/09/2020
	// I ignored this test for now too long to be executed, @Youssr/
	// or me work on a optimization of loading using this parameter
	// It needs a connection to Pubmed Server and the connection
	// 08/12/2020: this type of script will be included onto the pubmed pipeline.
//	public void paper_extraction() throws Exception {
//		cl.reset();
//		List<DataSource> dataSources = Lists.newLinkedList();
//		Config.getInstance().setProperty("type_pdf", "paper");
//		cl.registerPDF("data/pdf/test_files/paper.pdf").forEach(dataSources::add);
//		assertEquals(1, dataSources.size());
//		File tempFolder = testFolder.newFolder("test_pdf_generic");
//		String expectedGraph = exportAndGetContent(cl, tempFolder.getAbsolutePath(), "graph3", false, false);
//		String referenceGraph = getReferenceGraphFile("loading/pdf_tests", "paper");
//		assertEqual(referenceGraph, expectedGraph);
//	}

}

/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import java.net.URI;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.junit.Test;

public class DataSourceCatalogTest extends ConnectionLensTest {

	EntityExtractor extractor = new TopLevelExtractor();
	MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	ConnectionLens cl;

	@Test public void testAddEntry() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		assertEquals(ds, cl.catalog().getEntry(URI.create(Paths.get("file:data", "json", "test_example1.json").toString())));
		assertEquals(ds, cl.catalog().getEntry(ds.getLocalURI()));
		assertEquals(ds, cl.catalog().getEntry(ds.getID()));
		assertNull(cl.catalog().getEntry(URI.create("file:dummy")));
		assertNull(cl.catalog().getEntry((short) -1));
		cl.close();
		tearDownDB();
	}

	@Test public void testReset() throws SQLException {
		setUpDB();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		
		DataSource ds = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		assertEquals(ds, cl.catalog().getEntry(URI.create(Paths.get("file:data", "json", "test_example1.json").toString())));
		cl.close();
		cl = new ConnectionLens(extractor, analyzer, pp, true);
		assertNull(cl.catalog().getEntry(URI.create(Paths.get("file:data", "json", "test_example1.json").toString())));
		cl.close();
		tearDownDB();
	}

}
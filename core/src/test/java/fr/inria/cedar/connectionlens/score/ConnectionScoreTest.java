/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.score;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import com.google.common.collect.Sets;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Edge.Specificity;
import org.junit.Assert;
import org.junit.Test;

public class ConnectionScoreTest {

	Specificity s1 = mock(Specificity.class);
	Specificity s2 = mock(Specificity.class);
	Specificity s3 = mock(Specificity.class);
	Specificity s4 = mock(Specificity.class);
	Edge e1 = mock(Edge.class);
	Edge e2 = mock(Edge.class);
	Edge e3 = mock(Edge.class);
	Edge e4 = mock(Edge.class);
	Scorable s = mock(Scorable.class);
	
	@Test public void testAverageConfidence1() {
		when(s1.get()).thenReturn(1.);
		when(s2.get()).thenReturn(.8);
		when(s3.get()).thenReturn(.4);
		when(s4.get()).thenReturn(.2);
		when(e1.confidence()).thenReturn(1.);
		when(e2.confidence()).thenReturn(1.);
		when(e3.confidence()).thenReturn(1.);
		when(e4.confidence()).thenReturn(1.);
		when(s.edges()).thenReturn(Sets.newHashSet(e1, e2, e3, e4));
		Assert.assertEquals(1.0, new ConnectionScore(1.0).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(1,  new ConnectionScore(0.004).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(1.0, new ConnectionScore(.5).compute(s).value().doubleValue(), .0001);
	}
	
	@Test public void testAverageConfidence2() {
		when(s1.get()).thenReturn(1.);
		when(s2.get()).thenReturn(1.);
		when(s3.get()).thenReturn(1.);
		when(s4.get()).thenReturn(1.);
		when(e1.confidence()).thenReturn(1.);
		when(e2.confidence()).thenReturn(.8);
		when(e3.confidence()).thenReturn(.4);
		when(e4.confidence()).thenReturn(.2);
		when(s.edges()).thenReturn(Sets.newHashSet(e1, e2, e3, e4));
		Assert.assertEquals(0.064, new ConnectionScore(0).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.064,  new ConnectionScore(1).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.064, new ConnectionScore(.5).compute(s).value().doubleValue(), .0001);
	}
	
	@Test public void testAverageConfidence3() {
		when(s1.get()).thenReturn(1.);
		when(s2.get()).thenReturn(.8);
		when(s3.get()).thenReturn(.4);
		when(s4.get()).thenReturn(.2);
		when(e1.confidence()).thenReturn(1.);
		when(e2.confidence()).thenReturn(.8);
		when(e3.confidence()).thenReturn(.4);
		when(e4.confidence()).thenReturn(.2);
		when(s.edges()).thenReturn(Sets.newHashSet(e1, e2, e3, e4));
		Assert.assertEquals(.064, new ConnectionScore(0).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.064,  new ConnectionScore(1).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.064, new ConnectionScore(.5).compute(s).value().doubleValue(), .0001);
	}
	
	@Test public void testAverageConfidenceConfidence() {
		when(s1.get()).thenReturn(1.);
		when(s2.get()).thenReturn(.8);
		when(s3.get()).thenReturn(.4);
		when(s4.get()).thenReturn(.2);
		when(e1.confidence()).thenReturn(0.);
		when(e2.confidence()).thenReturn(.8);
		when(e3.confidence()).thenReturn(.4);
		when(e4.confidence()).thenReturn(.2);
		when(s.edges()).thenReturn(Sets.newHashSet(e1, e2, e3, e4));
		Assert.assertEquals(.0, new ConnectionScore(0).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.0,  new ConnectionScore(1).compute(s).value().doubleValue(), .0001);
		Assert.assertEquals(.0, new ConnectionScore(.5).compute(s).value().doubleValue(), .0001);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAverageConfidenceSpecificityNegativeBeta() {
		new ConnectionScore(-.1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAverageConfidenceSpecificityBetaOverLimit() {
		new ConnectionScore(1.1);
	}
}

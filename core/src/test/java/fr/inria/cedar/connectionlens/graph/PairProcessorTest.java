/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import static fr.inria.cedar.connectionlens.graph.Node.Types.ENTITY_PERSON;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.DatePairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.ExactPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.HammingPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.JaccardPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.JaroPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.LevenshteinPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.LongStringPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.NumberPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.ShortStringPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.graph.sim.URIPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;
import fr.inria.cedar.connectionlens.util.StatisticsCollector.SilentStatsCollector;

public class PairProcessorTest extends ConnectionLensTest {

	StatisticsCollector stats = new SilentStatsCollector();
	private static final Logger log = Logger.getLogger(PairProcessorTest.class);

	String n1 = "1";
	String n2 = "289";
	String n3 = "1.0";
	String n4 = "2";
	String t1 = "XYZ";
	String t2 = "SDIPnlAPSIDASDLQWElkasd";
	String t3 = "SDIPnlAPSIDASDLQWElkasd";
	String t4 = "Emmanuel";
	String t5 = "Emanuelle";
	String t6 = "r";
	String t7 = "Emmanuel se rend à Davos";
	String t8 = "Emanuelle se rendra à Davos";
	String t9 = "w";
	String u1 = "http://www.inria.fr/en/";
	String u2 = "http://www.inria.fr/fr/";
	String u3 = "http://team.inria.fr/cedar";
	String u4 = "ftp://aist.go.jp/";
	String d1 = "Sat Jun 30 22:21:57 +0000 2018";
	String d2 = "Sat Jun 30 22:21:58 +0000 2018";
	String d3 = "Sat Jun 30 23:21:57 +0100 2018";
	String d4 = "Sun Jul 1 10:45:32 +0001 2015";

	@Test
	public void testJaccard() {
		Assert.assertEquals(1., new JaccardPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new JaccardPairProcessor(stats, 0).apply(t3, t2), .0);
		Assert.assertEquals(3. / 7., new JaccardPairProcessor(stats, 0).apply(t7, t8), .0);
		Assert.assertEquals(0., new JaccardPairProcessor(stats, 0).apply(t4, t5), .0);
	}

	@Test
	public void testExact() {
		Assert.assertEquals(1., new ExactPairProcessor(stats).apply(t2, t2), .0);
		Assert.assertEquals(1., new ExactPairProcessor(stats).apply(t2, t3), .0);
		Assert.assertEquals(0., new ExactPairProcessor(stats).apply(t4, t5), .0);
	}

	@Test
	public void testJaro() {
		Assert.assertEquals(1., new JaroPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new JaroPairProcessor(stats, 0).apply(t2, t3), .0);
		Assert.assertEquals(.88, new JaroPairProcessor(stats, 0).apply(t4, t5), .01);
		Assert.assertEquals(0., new JaroPairProcessor(stats, 0).apply(t1, t5), .0);
	}

	@Test
	public void testLevenstein() {
		Assert.assertEquals(1., new LevenshteinPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new LevenshteinPairProcessor(stats, 0).apply(t2, t3), .0);
		Assert.assertEquals(1. - (3. / 9.), new LevenshteinPairProcessor(stats, 0).apply(t4, t5), .0);
		Assert.assertEquals(0., new LevenshteinPairProcessor(stats, 0).apply(t1, t5), .0);
	}

	@Test
	public void testHamming() {
		Assert.assertEquals(1., new HammingPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new HammingPairProcessor(stats, 0).apply(t2, t3), .0);
		Assert.assertEquals(1. - (6. / 9.), new HammingPairProcessor(stats, 0).apply(t4, t5), .0);
		Assert.assertEquals(0., new HammingPairProcessor(stats, 0).apply(t1, t5), .0);
	}

	@Test
	public void testURIs() {
		Assert.assertEquals(1., new URIPairProcessor(stats, 0).apply(u1, u1), .0);
		Assert.assertEquals(.0, new URIPairProcessor(stats, 0).apply(u1, u2), .0);
		Assert.assertEquals(.0, new URIPairProcessor(stats, 0).apply(u3, u2), .0);
		Assert.assertEquals(.0, new URIPairProcessor(stats, 0).apply(u1, u4), .0);
	}

	@Test
	public void testDates() {
		Assert.assertEquals(1., new DatePairProcessor(stats, 0).apply(d1, d1), .0);
		// IM, 4/6/20: The DatePairProcessor subclasses EqualityProcessor which is
		// normally applied only on nodes that have identical normalized labels (thanks
		// to the selector).
		// Therefore, I made its apply() method always return 1.0.
		// Thus, the comparisons below which require 0.0 similarity cannot work.
		// Could be switched back in the future.
		// Assert.assertEquals(.0, new DatePairProcessor(stats, 0).apply(d1, d2), .0);
		Assert.assertEquals(1., new DatePairProcessor(stats, 0).apply(d3, d1), .0);
		// Assert.assertEquals(.0, new DatePairProcessor(stats, 0).apply(d1, d4), .0);
	}

	@Test
	public void testNumbers() {
		Assert.assertEquals(1., new NumberPairProcessor(stats, 0).apply(n1, n1), .0);
		Assert.assertEquals(.0, new NumberPairProcessor(stats, 0).apply(n1, n2), .0);
		Assert.assertEquals(.0, new NumberPairProcessor(stats, 0).apply(n3, n2), .0);
		Assert.assertEquals(.0, new NumberPairProcessor(stats, 0).apply(n1, n4), .0);
	}

	@Test
	public void testShortStrings() {
		Assert.assertEquals(1., new ShortStringPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new ShortStringPairProcessor(stats, 0).apply(t2, t3), .0);
		Assert.assertEquals(1. - (3. / 9.), new ShortStringPairProcessor(stats, 0).apply(t4, t5), .0);
		Assert.assertEquals(0., new ShortStringPairProcessor(stats, 0).apply(t1, t5), .0);
	}

	@Test
	public void testLongStrings() {
		Assert.assertEquals(1., new LongStringPairProcessor(stats, 0).apply(t1, t1), .0);
		Assert.assertEquals(1., new LongStringPairProcessor(stats, 0).apply(t3, t2), .0);
		Assert.assertEquals(3. / 7., new LongStringPairProcessor(stats, 0).apply(t7, t8), .0);
		Assert.assertEquals(0., new LongStringPairProcessor(stats, 0).apply(t4, t5), .0);
	}

	@Test
    /** The purpose of this test is to check how person entities are compared with various spellings.
     *  The SNER extractor doesn't really find them. Flair finds 6 or so, but making this test
     *  depend on Flair is not a good idea.
     *
     *  So, we fake the Person identification using the policy-driven extractor, then compare
     *  the resulting entities.
     * @throws IOException
     * @throws SQLException
     */
	public void testPersonNormalization() throws IOException, SQLException {
		setUpDB();
		EntityExtractor extractor = new StanfordNERExtractor(FRENCH);

		Config.getInstance().setProperty("extract_policy",
				"test.persona Person, test.personb Person, test.personc Person, test.persond Person, " +
				"test.persone Person, test.personf Person, test.persong Person, test.personh Person, " +
				"test.personi Person"
				);
		Config.getInstance().setProperty("extract_from_uris", "false");
		// disable string comparisons to compare directly (only) entities
		Config.getInstance().setProperty("short_string_comparison", "NONE");
		Config.getInstance().setProperty("long_string_comparison", "NONE");

		MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
		SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "xml", "test.xml").toString(), "http://test.xml");

		cl.processSimilarities(cl.catalog().getDataSources());
		cl.graph().countEdgesForSpecificity();
		cl.graph().generateEdgeSpecificityComputeMeanStdDev(cl.graphStats());
		//(new GraphPrintingByRepresentative()).printAllSources(cl);

		Set<Node> personNodes = cl.graph().getNodes(ENTITY_PERSON);
		Assert.assertEquals(7, personNodes.size()); // 7 person nodes

		HashSet<Node> personRepresentatives = new HashSet<Node>();
		for (Node pn: personNodes) {
			personRepresentatives.add(pn.getRepresentative());
		}
		Assert.assertEquals(4, personRepresentatives.size()); // 4 Michetti person representatives (1 of them represents 2 others)

		HashSet<Edge> personSimEdges = new HashSet<Edge>();
		for (Node pn: personNodes) {
			personSimEdges.addAll(cl.graph().getWeakSameAs(pn));
		}
		Assert.assertEquals(9, personSimEdges.size()); // 9 similarity edges
		cl.close();
		tearDownDB();
		Config.getInstance().setProperty("extract_policy", "");
		Config.getInstance().setProperty("extract_from_uris", "true");
		Config.getInstance().setProperty("short_string_comparison", "PREFIX");
		Config.getInstance().setProperty("long_string_comparison", "PREFIX");

	}

	/**
	 * test if the json serialization of the graph is equal to the reference graph.
	 * 
	 * @throws IOException
	 */
	private void assertEqual(String referenceGraph, String graphExpected) throws IOException {
		try {
			JSONAssert.assertEquals(referenceGraph, graphExpected, JSONCompareMode.NON_EXTENSIBLE);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}

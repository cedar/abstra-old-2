/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static java.util.Locale.FRENCH;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Lists;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;

public class OrderIndependenceTest extends ConnectionLensTest {
	static EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	static MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	static ConnectionLens cl, cl0; 
	
	public static ConnectionLens init(String name, EntityExtractor extractor, int batchSize) {
		Config.getInstance().setProperty("RDBMS_DBName", name);
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		return new ConnectionLens(extractor, analyzer, pp, true);
	}	
	
	
	@Test
	@Ignore
	// this test is ignored since it take too long. It passed on 7/3/21.
	public void testRegisterAllDemo() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(Arrays.asList(new String[] { Paths.get("data", "poc", "2", "deputes.json").toString(),
				Paths.get("data", "poc", "2", "tweet-Ruffin.json").toString(), Paths.get("data", "poc", "2", "fb-etienne-chouard.txt").toString(), Paths.get("data", "poc", "2", "medias.txt").toString() }));

		assertAllPermutationsEqual(inputs);

	}

	@Test
	public void testRegisterJson() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(Arrays.asList(new String[] { Paths.get("data", "json", "test_example3.json").toString(),
				Paths.get("data", "json", "test_example5.json").toString(), Paths.get("data", "json", "test_example10.json").toString() }));
		assertAllPermutationsEqual(inputs);
	}

	@Test
	public void testRegisterTexts() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(Arrays.asList(new String[] { Paths.get("data", "poc", "2", "fb-etienne-chouard.txt").toString(),
				Paths.get("file:data", "text", "input", "lemonde.txt").toString(), Paths.get("file:data", "text", "input", "piste.txt").toString() }));
		assertAllPermutationsEqual(inputs);

	}

	@Test
	public void testRegisterRDF() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(Arrays.asList(new String[] { Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString(),
				Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString(), Paths.get("data", "rdf", "rdf_03", "test_03.nt").toString() }));
		assertAllPermutationsEqual(inputs);

	}

	@Test
	public void testRegisterJSONTEXTDemo() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(
				Arrays.asList(new String[] { Paths.get("data", "poc", "2", "deputes.json").toString(), Paths.get("data", "poc", "2", "fb-etienne-chouard.txt").toString() }));
		assertAllPermutationsEqual(inputs);

	}

	@Test
	public void testRegisterJSONDemo() throws IOException {
		ArrayList<String> inputs = new ArrayList<>(
				Arrays.asList(new String[] { Paths.get("data", "poc", "2", "deputes.json").toString(), Paths.get("data", "poc", "2", "tweet-Ruffin.json").toString() }));
		assertAllPermutationsEqual(inputs);

	}

	/**
	 * Tests if the graphs obtained from any permutation are equal to the graph 
	 * obtained from the first permutation
	 * 
	 * @param inputs
	 * @throws IOException
	 */
	private void assertAllPermutationsEqual(ArrayList<String> inputs) throws IOException {
		LinkedList<String[]> allPermutations = new LinkedList<String[]>();
		getDSPermutations(inputs.size(), inputs.toArray(new String[0]), allPermutations);
		cl = init("junit1", extractor, 1);
		
		// load that in a cl object
		cl0 = init("junit1k", extractor, 1);
		String[] permK = allPermutations.get(0);
		ArrayList<String> sourcesK = new ArrayList<>(Arrays.asList(permK));
		registerSources(sourcesK, cl0);

		for (int i = 1; i < allPermutations.size(); i++) {
			String[] permutation = allPermutations.get(i);
			ArrayList<String> sourcesI = new ArrayList<>(Arrays.asList(permutation));
			registerSources(sourcesI, cl);

			sameGraph(cl, cl0, false); 
		}

	}

	/**
	 * Registers a list of sources (in this order!) and returns a map source URI -->
	 * registration record
	 *
	 * @param URIs
	 * @return a map associating to each URI its corresponding registration record
	 */
	private HashMap<String, DataSourceRegistrationRecord> registerSources(ArrayList<String> URIs, ConnectionLens clParam) {
		clParam.reset();
		clParam.graph().getCatalog().reset();
		HashMap<String, DataSourceRegistrationRecord> res = new HashMap<String, DataSourceRegistrationRecord>();
		HashMap<String, DataSource> sources = new HashMap<String, DataSource>();
		List<DataSource> newSources = Lists.newLinkedList();

		for (String URI : URIs) { // register them in order
			DataSource ds = clParam.register(URI);
			sources.put(URI, ds);
			newSources.add(ds);
		}

		clParam.processSimilarities(clParam.catalog().getDataSources());
		return res;
	}

	/**
	 * generate all permutations and put it in allPermutations Lists;
	 *
	 * @param n
	 * @param inputDS
	 */
	private void getDSPermutations(int n, String[] inputDS, LinkedList<String[]> allPermutations) {
		if (n == 1) {
			allPermutations.add(inputDS.clone());
		} else {
			for (int i = 0; i < n - 1; i++) {
				getDSPermutations(n - 1, inputDS, allPermutations);
				if (n % 2 == 0) {
					swap(inputDS, i, n - 1);
				} else {
					swap(inputDS, 0, n - 1);
				}
			}
			getDSPermutations(n - 1, inputDS, allPermutations);
		}
	}

	@AfterClass
	public static void cleanUp() throws SQLException {
		cl.close();
		cl0.close();
		tearDownDB("junit1", "junit1k"); 
		Config.getInstance().setProperty("RDBMS_DBName", "junit");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH");
		
	}
	/**
	 * swap helper
	 *
	 * @param inputs
	 * @param i
	 * @param j
	 */
	private void swap(String[] inputs, int i, int j) {
		String tmp = inputs[i];
		inputs[i] = inputs[j];
		inputs[j] = tmp;
	}

	/**
	 *
	 * @return get Stats for this Data source.
	 */

	class DataSourceRegistrationRecord {
		int nodesNo;
		int edgesNo;
		int sameAsEdgesNo;
		int extractionEdgesNo;
		int globalSameAsNo;

		DataSourceRegistrationRecord(int nodesNo, int edgesNo, int sameAsEdgesNo, int extractionEdgesNo,
				int globalSameAsNo) {
			this.nodesNo = nodesNo;
			this.edgesNo = edgesNo;
			this.sameAsEdgesNo = sameAsEdgesNo;
			this.extractionEdgesNo = extractionEdgesNo;
			this.globalSameAsNo = globalSameAsNo;
		}

		public boolean equals(Object o) {
			if (o instanceof DataSourceRegistrationRecord) {
				DataSourceRegistrationRecord o2 = (DataSourceRegistrationRecord) o;
				if (nodesNo != o2.nodesNo) {
					return false;
				}
				if (edgesNo != o2.edgesNo) {
					return false;
				}
				if (sameAsEdgesNo != o2.sameAsEdgesNo) {
					return false;
				}
				if (extractionEdgesNo != o2.extractionEdgesNo) {
					return false;
				}
				if (globalSameAsNo != o2.globalSameAsNo) {
					return false;
				}
				return true;
			} else {
				return false;
			}
		}
	}
}

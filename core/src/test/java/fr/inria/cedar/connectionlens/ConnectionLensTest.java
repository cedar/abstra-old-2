/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens;

import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.Node.Types;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.GraphExport2JSON;

public class ConnectionLensTest {

	protected static final Logger log = Logger.getLogger(ConnectionLensTest.class);

	// We should not use the config both here and in setUp(), because the call in setUp
	// forces reload, which destroys parameters set in the lines below.
	protected static String defaultUser = ""; 
	protected static String defaultPassword = ""; 
	protected static String defaultPort = ""; 
	
	@BeforeClass
	public static void setUp() {
		defaultUser = Config.getInstance(true).getProperty("RDBMS_user");
		defaultPassword = Config.getInstance().getProperty("RDBMS_password");
		defaultPort = Config.getInstance().getProperty("RDBMS_port"); 
		Config.getInstance().setProperty("cache_location", ".junit");
		Config.getInstance().setProperty("extract_dates", "false");
		Config.getInstance().setProperty("language", "fr");
		Config.getInstance().setProperty("compare_nodes", "true");
		Config.getInstance().setProperty("extract_from_uris", "true");
	}

	public static void setUpDB(String... schemas) throws SQLException {
		// first, we recreate the database junit if needed (below, we will need to *connect to it*
		// so we cannot drop a database through a connection to it!
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL); Statement statement = c.createStatement()) {
			statement.executeUpdate("DROP DATABASE IF EXISTS junit");
			statement.executeUpdate("CREATE DATABASE junit");
		}
		// then open a connection to this database
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL, "junit");
				Statement statement = c.createStatement()) {
			// and if required, create schemas in it. 
			for (String schema : schemas) {
				statement.executeUpdate("CREATE SCHEMA " + schema);
				//log.info("CREATE SCHEMA " + schema); 
			}
		}
		//log.info("=== Opened connections to junit, RDBMS_DBName was: " + 
		//	Config.getInstance().getProperty("RDBMS_DBName")); 
		Config.getInstance().setProperty("RDBMS_DBName", "junit"); // last hope
		//log.info("=== Now RDBMS_DBName is: " + 
		//		Config.getInstance().getProperty("RDBMS_DBName")); 			
		//log.info("=== setupDB finished"); 
	}

	public static void tearDownDB() {
		//log.info("=== DROP DATABASE junit");
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL); Statement statement = c.createStatement()) {
			statement.executeUpdate("DROP DATABASE junit"); 
		}
		catch(Exception e) {
			log.info(e.getMessage()); 
		}
	}
	public static void tearDownDB(String... dbNames) {
		try (Connection c = ConnectionManager.getConnection(POSTGRESQL); Statement statement = c.createStatement()) {
			for (String dbName: dbNames) {
				statement.executeUpdate("DROP DATABASE " + dbName); 
			}
		}
		catch(Exception e) {
			log.info(e.getMessage()); 
		}
	}
	
	// any safety check on what the a class test leaves behind should be AfterClass

	protected HashSet<Node> filterNonEntityMatches(Set<Node> nodeMatches) {
		HashSet<Node> res = new HashSet<Node>();
		for (Node node : nodeMatches) {
			if ((node.getType() != Types.ENTITY_PERSON)
					&& (node.getType() != Types.ENTITY_ORGANIZATION) && (node.getType() != Types.ENTITY_LOCATION)) {
				res.add(node);
			}
		}
		return res;
	}

	protected String exportAndGetContent(ConnectionLens cl, String pathToFile, String fileName,
			Boolean includeSimilarEdges, Boolean localId) {

		GraphExport2JSON graphExport1 = new GraphExport2JSON(pathToFile, fileName);

		graphExport1.setIncludeSameAs(includeSimilarEdges);
		graphExport1.setLocalId(localId);

		graphExport1.export(cl.graph(), cl.catalog().getDataSources());
		String graph = getFileContent(pathToFile, fileName, "json");

		return graph;
	}

	protected String getFileContent(String pathToFile, String fileName, String fileType) {
		String fileContent = null;

		String location = Paths.get(pathToFile, fileName + "." + fileType).toString();
		// log.info("Reading file at: " + location);
		try {
			fileContent = new String(Files.readAllBytes(Paths.get(location)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return fileContent;
	}

	/**
	 *
	 * @return the reference json file.
	 * @throws IOException
	 */
	protected String getReferenceGraphFile(String globalTestName, String testName) {
		String reference = null;

		log.info("Looking for the file: " + Paths.get(globalTestName, testName + ".json").toString());
		try {
			reference = IOUtils.toString(this.getClass().getResourceAsStream(
					Paths.get(globalTestName, testName + ".json").toString()),
					"UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return reference;
	}

	/**
	 * This checks that two graphs have the exact same number of nodes and edges of each type.
	 * It is necessary, if not sufficient, to ensure graph equality.
	 * @param cl1
	 * @param cl2
	 */
	public void sameGraph(ConnectionLens cl1, ConnectionLens cl2, boolean testSameAs) {
		for (Node.Types t: Node.Types.values()) {
			assertEquals(cl1.graph().getNodes(t).size(), cl2.graph().getNodes(t).size());
		}
		for (Edge.Types t: Edge.Types.values()) {
			assertEquals(cl1.graph().getEdges(cl1.catalog(), t).size(), cl2.graph().getEdges(cl2.catalog(), t).size());
		}
		
		if (testSameAs) {
			assertEquals(cl1.graph().getSameAs().size(), cl2.graph().getSameAs().size()); 
		}
		
	}
	protected Boolean pythonVersionExists() {
		File pythonVersion = new File(Config.getInstance().getProperty("python_path"));
		return pythonVersion.exists();

	}
	
}

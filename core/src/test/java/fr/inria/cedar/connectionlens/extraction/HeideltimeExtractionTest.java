package fr.inria.cedar.connectionlens.extraction;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static fr.inria.cedar.connectionlens.graph.Node.Types.DATE;
import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;
import java.sql.SQLException;

public class HeideltimeExtractionTest extends ConnectionLensTest {

    @BeforeClass
    /** IM, 12/4/2021: this class really needs its specific setup */
    public static void setup() {
    	Config.getInstance().setProperty("extract_dates", "true"); // set true only for tests, set to false after
    	Config.getInstance().setProperty("default_locale", "en"); 
    }

    @Test
    public void testExtractDates() throws SQLException {
    	setUpDB();

    	HeideltimeExtractor extractor = new HeideltimeExtractor(ENGLISH);
    	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
    	MorphoSyntacticAnalyser analyzer = new TreeTagger(extractor.getLocale());
    	ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
    	Graph graph = cl.graph();

        DataSource ds = cl.register(Paths.get("data", "text", "input", "mail.txt").toString());

    	int nDates = graph.countNodes(ds, DATE);
    	//for (Node n: graph.getNodes(ds, DATE)) {
    	//	log.info("DATE: " + n); 
    	//}

    	cl.close();
    	tearDownDB();

    	assertEquals(7, nDates); // moved the assert last to ensure DB closed and erased

    }

    @AfterClass
    public static void tearDown() {
        Config.getInstance().setProperty("extract_dates", "false");
        Config.getInstance().setProperty("default_locale", "fr");
    }
}

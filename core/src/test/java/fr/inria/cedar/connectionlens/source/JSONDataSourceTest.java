/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Sets;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.schema.NumericEdgeID;
import fr.inria.cedar.connectionlens.sql.schema.NumericNodeID;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class JSONDataSourceTest extends ConnectionLensTest {

	EntityExtractor extractor = new TopLevelExtractor();
	MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
	SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	
	@BeforeClass
	public static void setupClass() throws SQLException {
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
	}

	// tests registerDataset with the simplest example
	// simple json object
	@Test
	public void testRegisterTwoDatasets() throws SQLException {
		setUpDB(); 
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds2 = cl.register(Paths.get("data", "json", "test_example2.json").toString());
		Graph graph = cl.graph();
		//(new GraphPrintingByRepresentative()).printAllSources(cl);

		Node n0 = cl.graph().resolveNode(new NumericNodeID(4));
		Node n1 = cl.graph().resolveNode(new NumericNodeID(5));
		Node n2 = cl.graph().resolveNode(new NumericNodeID(7));

		Node m1 = cl.graph().resolveNode(new NumericNodeID(20));

		Edge e0 = cl.graph().resolveEdge(new NumericEdgeID(9));
		Edge e1 = cl.graph().resolveEdge(new NumericEdgeID(6));
		Edge e2 = cl.graph().resolveEdge(new NumericEdgeID(8));

		assertTrue(graph.getNodes(ds1).containsAll(Sets.newHashSet(ds1.getRoot(), n0, n1, n2)));
		assertTrue(graph.getEdges(ds1).containsAll(Sets.newHashSet(e0, e1, e2)));
		assertTrue(graph.getNodes(ds1, "Jean-Paul Sartre").containsAll(Sets.newHashSet(n2)));
		assertEquals(Sets.newHashSet(n1), graph.getNodes(ds1, "45"));
		assertEquals(Sets.newHashSet(), graph.getNodes(ds1, "Jean-Claude Sartre"));
		assertEquals(Sets.newHashSet(e1), graph.getEdges(ds1, "age"));
		assertEquals(Sets.newHashSet(), graph.getEdges(ds1, "size"));
		assertEquals(Sets.newHashSet(), graph.getNodes(ds2, "45"));
		assertEquals(Sets.newHashSet(), graph.getEdges(ds2, "age"));
		assertTrue(graph.getNodes("Jean-Paul Sartre").containsAll(Sets.newHashSet(n2, m1)));
		assertEquals(Sets.newHashSet(n1), graph.getNodes("45"));
		assertEquals(Sets.newHashSet(), graph.getNodes("Jean-Claude Sartre"));
		assertEquals(Sets.newHashSet(e1), graph.getEdges("age"));
		assertEquals(Sets.newHashSet(), graph.getEdges("size"));
		assertEquals(3, graph.countEdges(ds1, EDGE, EXTRACTION_EDGE));
		assertEquals(5, graph.countEdges(ds2, EDGE, EXTRACTION_EDGE));
		assertEquals(5, graph.countNodes(ds1));
		assertEquals(7, graph.countNodes(ds2));
		cl.close(); 
		tearDownDB();
	}

	@Test
	public void testAdjacentEdges() throws SQLException {
		setUpDB();
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "json", "test_example5.json").toString());
		//(new GraphPrintingByRepresentative()).printAllSources(cl);

		Node n1 = cl.graph().resolveNode(new NumericNodeID(4));

		Set<Edge> adj = cl.graph().getAdjacentEdges(n1);

		Edge e0 = cl.graph().resolveEdge(new NumericEdgeID(18));
		Edge e1 = cl.graph().resolveEdge(new NumericEdgeID(20));
		Edge e2 = cl.graph().resolveEdge(new NumericEdgeID(21));

		assertTrue(adj.contains(e0));
		assertTrue(adj.contains(e1));
		assertTrue(adj.contains(e2));
		assertEquals(3, adj.size());
		cl.close();
		tearDownDB();
	}
	
	@Test
	/** Ensures that URIs are recognized from this tweet (cast from Strings) */
	public void testCastToURI() throws SQLException {
		setUpDB();
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		DataSource ds = cl.register(Paths.get("data", "poc", "2", "tweet-Ruffin.json").toString());
		//(new GraphPrintingByRepresentative()).printAllSources(cl);

		assertEquals(11, cl.graph().getNodes(ds, Node.Types.RDF_URI).size()); 
		cl.close();
		tearDownDB();
	}

	@Test
	public void testAtomicity1() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE"); // default
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "json", "issue-100-sample.json").toString());
		cl.register(Paths.get("data", "json", "issue-100-sample2.json").toString());
		assertEquals(4, cl.graph().getNodes("1").size());
		assertEquals(2, cl.graph().getNodes("2").size()); 
		cl.close();
		tearDownDB();
	}
	@Test
	public void testAtomicity2() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_PATH");
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "json", "issue-100-sample.json").toString());
		cl.register(Paths.get("data", "json", "issue-100-sample2.json").toString());
		assertEquals(3, cl.graph().getNodes("1").size());
		assertEquals(2, cl.graph().getNodes("2").size()); 
		cl.close();
		tearDownDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");	
	}
	@Test
	public void testAtomicity3() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_DATASET");
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "json", "issue-100-sample.json").toString());
		cl.register(Paths.get("data", "json", "issue-100-sample2.json").toString());
		assertEquals(2, cl.graph().getNodes("1").size());
		assertEquals(2, cl.graph().getNodes("2").size()); 
		cl.close();
		tearDownDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");	
	}
	@Test
	public void testAtomicity4() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("value_atomicity", "PER_GRAPH");
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true);
		cl.register(Paths.get("data", "json", "issue-100-sample.json").toString());
		cl.register(Paths.get("data", "json", "issue-100-sample2.json").toString());
		assertEquals(1, cl.graph().getNodes("1").size());
		assertEquals(1, cl.graph().getNodes("2").size()); 
		cl.close();
		tearDownDB();
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");	
	}
	@AfterClass
	public static void resetDefaults() {
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_GRAPH");
	}

}

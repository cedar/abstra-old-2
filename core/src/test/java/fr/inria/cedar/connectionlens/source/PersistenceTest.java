/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.graph.Edge.Types.EDGE;
import static fr.inria.cedar.connectionlens.graph.Edge.Types.EXTRACTION_EDGE;
import static fr.inria.cedar.connectionlens.graph.Node.Types.RELATIONAL_STRUCT;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.LUCENE;
import static fr.inria.cedar.connectionlens.indexing.IndexingModels.POSTGRES_FULLTEXT;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Maps;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.GraphPrintingByRepresentative;

public class PersistenceTest extends ConnectionLensTest {

	EntityExtractor extractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	MorphoSyntacticAnalyser analyzer = new fr.inria.cedar.connectionlens.extraction.TreeTagger(FRENCH);
	SimilarPairProcessor sim = SimilarPairProcessor.buildPairFinder(Config.getInstance());

	@Test
	public void testIDCoherenceUponReload() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("RDBMS_DBName", "junit1");
		// used to set index to POSTGRES (the default) 
		ConnectionLens cl1 = new ConnectionLens(extractor, analyzer, sim, true);
		Graph g1 = cl1.graph();
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds13 = cl1.register(Paths.get("data", "text", "input", "lemonde.txt").toString());
		URI u1 = ds11.getLocalURI();
		URI u2 = ds12.getLocalURI();
		URI u3 = ds13.getLocalURI();
		Config.getInstance().setProperty("RDBMS_DBName", "junit2");
		ConnectionLens cl2 = new ConnectionLens(extractor, analyzer, sim, true);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds23 = cl2.register(Paths.get("data", "text", "input", "lemonde.txt").toString());
		assertEquals(cl1.graph().getNodes(ds11), cl2.graph().getNodes(ds21));
		assertEquals(cl1.graph().getNodes(ds12), cl2.graph().getNodes(ds22));
		assertEquals(cl1.graph().getNodes(ds13), cl2.graph().getNodes(ds23));
		assertEquals(cl1.graph().getEdges(ds11), cl2.graph().getEdges(ds21));
		assertEquals(cl1.graph().getEdges(ds12), cl2.graph().getEdges(ds22));
		assertEquals(cl1.graph().getEdges(ds13), cl2.graph().getEdges(ds23));
		ConnectionLens cl3 = new ConnectionLens(extractor, analyzer, sim, false);
		DataSource ds31 = cl3.catalog().getEntry(u1);
		DataSource ds32 = cl3.catalog().getEntry(u2);
		DataSource ds33 = cl3.catalog().getEntry(u3);
		Graph g3 = cl3.graph();
		assertEquals(g1.getNodes(ds11), g3.getNodes(ds31));
		assertEquals(g1.getNodes(ds12), g3.getNodes(ds32));
		assertEquals(g1.getNodes(ds13), g3.getNodes(ds33));
		assertEquals(g1.getEdges(ds11).toString(), g3.getEdges(ds31).toString());
		assertEquals(g1.getEdges(ds12).toString(), g3.getEdges(ds32).toString());
		assertEquals(g1.getEdges(ds13).toString(), g3.getEdges(ds33).toString());
		cl1.close();
		cl2.close();
		cl3.close();
		tearDownDB("junit1", "junit2");
		Config.getInstance().setProperty("RDBMS_DBName", "junit");
	}

	@Test
	public void testReRegisterFourDatasets() throws SQLException {
		int nodes0, nodes1, nodes2, nodes3;
		int edges0, edges1, edges2, edges3;

		setUpDB("schema1");

		// log.info("Creating relational data source");
		Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
		Statement s1 = c1.createStatement();
		s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
		log.info("Loading this and three other sources (json, RDF, txt)");

		// in junit
		ConnectionLens cl1 = new ConnectionLens(new TopLevelExtractor(), analyzer, sim, true);
		RelationalDataSource ds0 = (RelationalDataSource) cl1
				.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser
						+ "&password=" + defaultPassword);
		Node n0 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n1 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n2 = ds0.buildLabelNodeOfType("Emmanuel Macron", RELATIONAL_STRUCT);
		DataSource ds1 = cl1.register(Paths.get("data" , "json", "test_example1.json").toString());
		DataSource ds2 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds3 = cl1.register(Paths.get("data", "text", "input", "lemonde.txt").toString());

		Graph graph = cl1.graph();
		nodes0 = graph.countNodes(ds0);
		edges0 = graph.countEdges(ds0, EDGE);
		nodes1 = graph.countNodes(ds1);
		edges1 = graph.countEdges(ds1, EDGE);
		nodes2 = graph.countNodes(ds2);
		edges2 = graph.countEdges(ds2, EDGE);
		nodes3 = graph.countNodes(ds3);
		edges3 = graph.countEdges(ds3, EDGE);
		// log.info("Nodes0: " + nodes0 + " nodes1: " + nodes1 + " nodes2: " + nodes2 +
		// " nodes3: " + nodes3);
		// log.info("Edges0: " + edges0 + " edges1: " + edges1 + " edges2: " + edges2 +
		// " edges3: " + edges3);
		cl1.close();

		// in junit, no reset
		ConnectionLens cl2 = new ConnectionLens(new TopLevelExtractor(), analyzer, sim, false);
		Graph graph2 = cl2.graph();
		Map<String, DataSource> dss = Maps.uniqueIndex(cl2.catalog().getDataSources(),
				ds -> ds.getLocalURI().toString());
		RelationalDataSource rds = (RelationalDataSource) dss
				.get("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password="
						+ defaultPassword);
		int cNodes0 = graph2.countNodes(rds);
		int cEdges0 = graph2.countEdges(rds, EDGE);
		int cNodes1 = graph2.countNodes(dss.get(Paths.get("file:data", "json", "test_example1.json").toString()));
		int cEdges1 = graph2.countEdges(dss.get(Paths.get("file:data", "json", "test_example1.json").toString()), EDGE);
		int cNodes2 = graph2.countNodes(dss.get(Paths.get("file:data", "rdf/", "rdf_02", "test_02.nt").toString()));
		int cEdges2 = graph2.countEdges(dss.get(Paths.get("file:data", "rdf", "rdf_02", "test_02.nt").toString()), EDGE);
		int cNodes3 = graph2.countNodes(dss.get(Paths.get("file:data", "text", "input", "lemonde.txt").toString()));
		int cEdges3 = graph2.countEdges(dss.get(Paths.get("file:data", "text", "input", "lemonde.txt").toString()), EDGE);
		// log.info("cNodes0: " + cNodes0 + " cNodes1: " + cNodes1 + " cNodes2: " +
		// cNodes2 + " cNodes3: " + cNodes3);
		// log.info("cEdges0: " + cEdges0 + " cEdges1: " + cEdges1 + " cEdges2: " +
		// cEdges2 + " cEdges3: " + cEdges3);
		assert (cNodes0 > 0);
		assertEquals(nodes0, cNodes0);
		assertEquals(edges0, cEdges0);
		assertEquals(nodes1, cNodes1);
		assertEquals(edges1, cEdges1);
		assertEquals(nodes2, cNodes2);
		assertEquals(edges2, cEdges2);
		assertEquals(nodes3, cNodes3);
		assertEquals(edges3, cEdges3);
		cl2.close();
		tearDownDB();
	}

	@Test
	public void testIDCoherenceUponReloadPostgresFullText() throws SQLException {
		setUpDB();
		// same place, reset
		ConnectionLens cl1 = new ConnectionLens(extractor, analyzer, sim, true);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds13 = cl1.register(Paths.get("data", "text", "input", "lemonde.txt").toString());
		URI u1 = ds11.getLocalURI();
		URI u2 = ds12.getLocalURI();
		URI u3 = ds13.getLocalURI();

		// same place, reset
		ConnectionLens cl2 = new ConnectionLens(extractor, analyzer, sim, true);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds23 = cl2.register(Paths.get("data", "text", "input", "lemonde.txt").toString());
		assertEquals(cl1.graph().getNodes(ds11), cl2.graph().getNodes(ds21));
		assertEquals(cl1.graph().getNodes(ds12), cl2.graph().getNodes(ds22));
		assertEquals(cl1.graph().getNodes(ds13), cl2.graph().getNodes(ds23));
		assertEquals(cl1.graph().getEdges(ds11), cl2.graph().getEdges(ds21));
		assertEquals(cl1.graph().getEdges(ds12), cl2.graph().getEdges(ds22));
		assertEquals(cl1.graph().getEdges(ds13), cl2.graph().getEdges(ds23));

		// same place, don't reset
		ConnectionLens cl3 = new ConnectionLens(extractor, analyzer, sim, false);
		DataSource ds31 = cl3.catalog().getEntry(u1);
		DataSource ds32 = cl3.catalog().getEntry(u2);
		DataSource ds33 = cl3.catalog().getEntry(u3);
		assertEquals(cl1.graph().getNodes(ds11), cl3.graph().getNodes(ds31));
		assertEquals(cl1.graph().getNodes(ds12), cl3.graph().getNodes(ds32));
		assertEquals(cl1.graph().getNodes(ds13), cl3.graph().getNodes(ds33));
		assertEquals(cl1.graph().getEdges(ds11).toString(), cl3.graph().getEdges(ds31).toString());
		assertEquals(cl1.graph().getEdges(ds12).toString(), cl3.graph().getEdges(ds32).toString());
		assertEquals(cl1.graph().getEdges(ds13).toString(), cl3.graph().getEdges(ds33).toString());

		cl1.close();
		cl1.close();
		cl3.close();
		tearDownDB();
	}

	@Test
	public void testReRegisterThreeDatasetsPostgresFullText() throws SQLException {
		setUpDB("schema1");
		// this is in junit
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, sim, true);

		// create in junit.schema1 a tiny relational dataset (table test1)
		Config.getInstance().setProperty("bulk_inserts", "false");
		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
		Statement s1 = c1.createStatement();
		s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");

		// load this tiny dataset in cl
		RelationalDataSource ds0 = (RelationalDataSource) cl
				.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser
						+ "&password=" + defaultPassword);
		// manualy build a few nodes (note: they have already been built in cl)
		Node n0 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n1 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n2 = ds0.buildLabelNodeOfType("Emmanuel Macron", RELATIONAL_STRUCT);

		// load two more datasets
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

		// chck the structure of the graph
		Graph graph = cl.graph();
		assertEquals(8, graph.countEdges(ds0, EDGE, EXTRACTION_EDGE));
		assertEquals(3, graph.countEdges(ds1, EDGE, EXTRACTION_EDGE));
		assertEquals(14, graph.countEdges(ds2, EDGE, EXTRACTION_EDGE));
		assertEquals(11, graph.countNodes(ds0));
		assertEquals(6, graph.countNodes(ds1));
		assertEquals(20, graph.countNodes(ds2));
		cl.close();

		// re-opening a CL on the same DB:
		ConnectionLens cl2 = new ConnectionLens(extractor, analyzer, sim, false);
		Graph graph2 = cl2.graph();
		Map<String, DataSource> dss = Maps.uniqueIndex(cl2.catalog().getDataSources(),
				ds -> ds.getLocalURI().toString());
		RelationalDataSource rds = (RelationalDataSource) dss
				.get("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password="
						+ defaultPassword);
		assertEquals(8, graph2.countEdges(rds, EDGE, EXTRACTION_EDGE));
		assertEquals(3, graph2.countEdges(dss.get(Paths.get("file:data", "json", "test_example1.json").toString()), EDGE, EXTRACTION_EDGE));
		assertEquals(14, graph2.countEdges(dss.get(Paths.get("file:data", "rdf", "rdf_02", "test_02.nt").toString()), EDGE, EXTRACTION_EDGE));
		assertEquals(11, graph2.countNodes(dss.get("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user="
				+ defaultUser + "&password=" + defaultPassword)));
		assertEquals(6, graph2.countNodes(dss.get(Paths.get("file:data", "json", "test_example1.json").toString())));
		assertEquals(20, graph2.countNodes(dss.get(Paths.get("file:data", "rdf", "rdf_02", "test_02.nt").toString())));
		cl.close();
		tearDownDB();
	}

	@Test
	public void testIDCoherenceUponReloadLucene() throws SQLException {
		setUpDB();
		Config.getInstance().setProperty("indexing_model", LUCENE.name());
		Config.getInstance().setProperty("RDBMS_DBName", "junit1");
		ConnectionLens cl1 = new ConnectionLens(extractor, analyzer, sim, true);
		DataSource ds11 = cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds12 = cl1.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds13 = cl1.register(Paths.get("data", "text", "input", "lemonde.txt").toString());
		URI u1 = ds11.getLocalURI();
		URI u2 = ds12.getLocalURI();
		URI u3 = ds13.getLocalURI();
		Config.getInstance().setProperty("RDBMS_DBName", "junit2");
		ConnectionLens cl2 = new ConnectionLens(extractor, analyzer, sim, true);
		DataSource ds21 = cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds22 = cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		DataSource ds23 = cl2.register(Paths.get("data","text", "input", "lemonde.txt").toString());
		assertEquals(cl1.graph().getNodes(ds11), cl2.graph().getNodes(ds21));
		assertEquals(cl1.graph().getNodes(ds12), cl2.graph().getNodes(ds22));
		assertEquals(cl1.graph().getNodes(ds13), cl2.graph().getNodes(ds23));
		assertEquals(cl1.graph().getEdges(ds11), cl2.graph().getEdges(ds21));
		assertEquals(cl1.graph().getEdges(ds12), cl2.graph().getEdges(ds22));
		assertEquals(cl1.graph().getEdges(ds13), cl2.graph().getEdges(ds23));
		ConnectionLens cl3 = new ConnectionLens(extractor, analyzer, sim, false);
		DataSource ds31 = cl3.catalog().getEntry(u1);
		DataSource ds32 = cl3.catalog().getEntry(u2);
		DataSource ds33 = cl3.catalog().getEntry(u3);
		assertEquals(cl1.graph().getNodes(ds11), cl3.graph().getNodes(ds31));
		assertEquals(cl1.graph().getNodes(ds12), cl3.graph().getNodes(ds32));
		assertEquals(cl1.graph().getNodes(ds13), cl3.graph().getNodes(ds33));
		assertEquals(cl1.graph().getEdges(ds11).toString(), cl3.graph().getEdges(ds31).toString());
		assertEquals(cl1.graph().getEdges(ds12).toString(), cl3.graph().getEdges(ds32).toString());
		assertEquals(cl1.graph().getEdges(ds13).toString(), cl3.graph().getEdges(ds33).toString());
		cl1.close();
		cl2.close();
		cl3.close();
		tearDownDB("junit1", "junit2");
		// make sure the sequel of the tests is back to the default settings
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
		Config.getInstance().setProperty("RDBMS_DBName", "junit");

	}

	@Test
	public void testReRegisterFourDatasetsLucene() throws SQLException {
		setUpDB("schema1");
		Config.getInstance().setProperty("indexing_model", LUCENE.name());

		Config.getInstance().setProperty("value_atomicity", "PER_INSTANCE");
		
		// this should use junit
		ConnectionLens cl = new ConnectionLens(extractor, analyzer, sim, true);
	
		// for the relational data, using junit.schema1:
		Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
		Statement s1 = c1.createStatement();
		s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
		s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
		RelationalDataSource ds0 = (RelationalDataSource) cl
				.register("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser
						+ "&password=" + defaultPassword);
		Node n0 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n1 = ds0.buildEmptyLabelNodeOfType(RELATIONAL_STRUCT);
		Node n2 = ds0.buildLabelNodeOfType("Emmanuel Macron", RELATIONAL_STRUCT);
		Edge e1 = ds0.getRoot().buildEdge(n0);
		Edge e2 = n1.buildEdge(n2);
		DataSource ds1 = cl.register(Paths.get("data", "json", "test_example1.json").toString());
		DataSource ds2 = cl.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());
		//(new GraphPrintingByRepresentative()).printAllSources(cl);
	
		Graph graph = cl.graph();
		assertEquals(8, graph.countEdges(ds0, EDGE, EXTRACTION_EDGE));
		assertEquals(3, graph.countEdges(ds1, EDGE, EXTRACTION_EDGE));
		assertEquals(14, graph.countEdges(ds2, EDGE, EXTRACTION_EDGE));
		assertEquals(11, graph.countNodes(ds0));
		assertEquals(6, graph.countNodes(ds1));
		assertEquals(20, graph.countNodes(ds2));

		ConnectionLens cl2 = new ConnectionLens(extractor, analyzer, sim, false);
		Graph graph2 = cl2.graph();
//		(new GraphPrintingByRepresentative()).printAllSources(cl2);
		Map<String, DataSource> dss = Maps.uniqueIndex(cl.catalog().getDataSources(),
				ds -> ds.getLocalURI().toString());
		RelationalDataSource rds = (RelationalDataSource) dss
				.get("jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password="
						+ defaultPassword);
		assertEquals(8, graph2.countEdges(rds, EDGE, EXTRACTION_EDGE));
		DataSource newDS1 = dss.get(Paths.get("file:data", "json", "test_example1.json").toString()); 
		assertEquals(3, graph2.countEdges(newDS1, EDGE, EXTRACTION_EDGE));
		DataSource newDS2 = dss.get(Paths.get("file:data", "rdf", "rdf_02", "test_02.nt").toString()); 
		assertEquals(14, graph2.countEdges(newDS2, EDGE, EXTRACTION_EDGE));
		assertEquals(11, graph2.countNodes(rds));
		assertEquals(6, graph2.countNodes(newDS1));
		assertEquals(20, graph2.countNodes(newDS2));
		cl.close();
		cl2.close();
		tearDownDB();
		// make sure the sequel of the tests is back to the default settings
		Config.getInstance().setProperty("indexing_model", POSTGRES_FULLTEXT.name());
	}
}

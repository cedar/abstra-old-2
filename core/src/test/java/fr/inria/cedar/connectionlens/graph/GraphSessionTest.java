/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.graph;

import static java.util.Locale.FRENCH;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraphSession.*;

public class GraphSessionTest extends ConnectionLensTest {
	static Config config = Config.getInstance();
	static EntityExtractor extractor = new TopLevelExtractor();
	static MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(config);
	
	@Test public void testGraphSessionNumericIDs() {
		try{
			setUpDB();
			ConnectionLens cl = new ConnectionLens(extractor, analyzer, pp, true); 
			Assert.assertTrue(cl.graph().openSession(-1000) instanceof BatchSession);
			Assert.assertTrue(cl.graph().openSession(-1) instanceof BatchSession);
			Assert.assertTrue(cl.graph().openSession(0) instanceof BatchSession);
			Assert.assertTrue(cl.graph().openSession(1) instanceof SequentialSession);
			Assert.assertTrue(cl.graph().openSession(2) instanceof BufferedSession);
			Assert.assertTrue(cl.graph().openSession(1000) instanceof BufferedSession);
			cl.close(); 
			tearDownDB();	
		}
		catch(Exception e) {
			assert(false); 
		}
	}
}

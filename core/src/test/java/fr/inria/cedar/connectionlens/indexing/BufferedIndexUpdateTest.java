/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.indexing;

import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static java.util.Locale.FRENCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.CacheExtractor;
import fr.inria.cedar.connectionlens.extraction.MorphoSyntacticAnalyser;
import fr.inria.cedar.connectionlens.extraction.StanfordNERExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.RelationalDataSource;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Ignore;
import org.junit.Test;

public class BufferedIndexUpdateTest extends ConnectionLensTest {

	static StatisticsCollector stats;
	static EntityExtractor dummyExtractor = new TopLevelExtractor();
	static EntityExtractor cacheExtractor = new CacheExtractor(new StanfordNERExtractor(FRENCH));
	static MorphoSyntacticAnalyser analyzer = new TreeTagger(FRENCH);
	static SimilarPairProcessor pp = SimilarPairProcessor.buildPairFinder(Config.getInstance());
	// the two below were static
	final String db1 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema1&user=" + defaultUser + "&password=" + defaultPassword;
	final String db2 = "jdbc:postgresql://localhost:5432/junit?currentSchema=schema2&user=" + defaultUser + "&password=" + defaultPassword;
	static String path1 = Paths.get("data", "text", "input", "lemonde.txt").toString();
	static String path2 = Paths.get("data", "text", "input", "piste.txt").toString();

	private static ConnectionLens init(String name, EntityExtractor extractor, int batchSize) {
		Config.getInstance().setProperty("RDBMS_DBName", name);
		Config.getInstance().setProperty("update_batch_size", String.valueOf(batchSize));
		return new ConnectionLens(extractor, analyzer, pp, true);
	}
	public static ConnectionLens initWithDB(String dbName, int batchSize) throws SQLException {
		String standardDBName = Config.getInstance().getProperty("RDBMS_DBName");
		Config.getInstance().setProperty("RDBMS_DBName", dbName);
		Config.getInstance().setProperty("update_batch_size", String.valueOf(batchSize));
		Config.getInstance().setProperty("entity_node_creation_policy", "PER_OCCURRENCE");
		ConnectionLens cl =  new ConnectionLens(dummyExtractor, analyzer, pp, true);
		Config.getInstance().setProperty("RDBMS_DBName", standardDBName);
		return cl; 
	}
	/** This tests that the batch size does not influence the outcome. 
	 *  Thus, it needs two different databases.
	 *  Thus, it should take care to destroy them both.
	 *  
	 *  As we play the same trick several times, the DBs need to be
	 *  dropped at the end of each test method. 
	 *  
	 * @throws SQLException
	 */
	@Test public void testBufferedRegisterTwoJSONDatasets() throws SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		cl1.register(Paths.get("data", "json", "test_example1.json").toString());
		cl1.register(Paths.get("data", "json", "test_example2.json").toString());
		ConnectionLens cl2 = initWithDB("junit2", 10);
		cl2.register(Paths.get("data", "json", "test_example1.json").toString());
		cl2.register(Paths.get("data", "json", "test_example2.json").toString());

		QueryComponent nodeMatch = new AtomicKeyword("sartre");
		QueryComponent edgeMatch = new AtomicKeyword("auteur");
		QueryComponent bothMatch = Disjunction.of("sartre", "auteur");

		assertTrue(cl1.graph().index().getDataSourceMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(nodeMatch), cl2.graph().index().getDataSourceMatches(nodeMatch));
		assertTrue(cl1.graph().index().getNodeMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(nodeMatch), cl2.graph().index().getNodeMatches(nodeMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(nodeMatch).size() == 0);
		assertEquals(cl1.graph().index().getEdgeMatches(nodeMatch), cl2.graph().index().getEdgeMatches(nodeMatch));
		assertTrue(cl1.graph().index().getItemMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(nodeMatch), cl2.graph().index().getItemMatches(nodeMatch));

		assertTrue(cl1.graph().index().getDataSourceMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(edgeMatch), cl2.graph().index().getDataSourceMatches(edgeMatch));
		assertTrue(cl1.graph().index().getNodeMatches(edgeMatch).size() == 0);
		assertEquals(cl1.graph().index().getNodeMatches(edgeMatch), cl2.graph().index().getNodeMatches(edgeMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getEdgeMatches(edgeMatch), cl2.graph().index().getEdgeMatches(edgeMatch));
		assertTrue(cl1.graph().index().getItemMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(edgeMatch), cl2.graph().index().getItemMatches(edgeMatch));

		assertTrue(cl1.graph().index().getDataSourceMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(bothMatch), cl2.graph().index().getDataSourceMatches(bothMatch));
		assertTrue(cl1.graph().index().getNodeMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(bothMatch), cl2.graph().index().getNodeMatches(bothMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getEdgeMatches(bothMatch), cl2.graph().index().getEdgeMatches(bothMatch));
		assertTrue(cl1.graph().index().getItemMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(bothMatch), cl2.graph().index().getItemMatches(bothMatch));
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}

	@Test
	public void testBufferedRegisterTwoRDFDatasets() throws SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		cl1.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		cl1.register(Paths.get("data","rdf", "rdf_02", "test_02.nt").toString());
		ConnectionLens cl2 = initWithDB("junit2", 10);
		cl2.register(Paths.get("data", "rdf", "rdf_01", "test_01.nt").toString());
		cl2.register(Paths.get("data", "rdf", "rdf_02", "test_02.nt").toString());

		QueryComponent nodeMatch = new AtomicKeyword("sartwell");
		QueryComponent edgeMatch = new AtomicKeyword("http://dbpedia.org/property/author");
		QueryComponent bothMatch = Disjunction.of("sartwell", "http://dbpedia.org/property/author");

		assertTrue(cl1.graph().index().getDataSourceMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(nodeMatch), cl2.graph().index().getDataSourceMatches(nodeMatch));
		assertTrue(cl1.graph().index().getNodeMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(nodeMatch), cl2.graph().index().getNodeMatches(nodeMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(nodeMatch).size() == 0);
		assertEquals(cl1.graph().index().getEdgeMatches(nodeMatch), cl2.graph().index().getEdgeMatches(nodeMatch));
		assertTrue(cl1.graph().index().getItemMatches(nodeMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(nodeMatch), cl2.graph().index().getItemMatches(nodeMatch));

		assertTrue(cl1.graph().index().getDataSourceMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(edgeMatch), cl2.graph().index().getDataSourceMatches(edgeMatch));
		assertTrue(cl1.graph().index().getNodeMatches(edgeMatch).size() == 0);
		assertEquals(cl1.graph().index().getNodeMatches(edgeMatch), cl2.graph().index().getNodeMatches(edgeMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getEdgeMatches(edgeMatch), cl2.graph().index().getEdgeMatches(edgeMatch));
		assertTrue(cl1.graph().index().getItemMatches(edgeMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(edgeMatch), cl2.graph().index().getItemMatches(edgeMatch));

		assertTrue(cl1.graph().index().getDataSourceMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(bothMatch), cl2.graph().index().getDataSourceMatches(bothMatch));
		assertTrue(cl1.graph().index().getNodeMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(bothMatch), cl2.graph().index().getNodeMatches(bothMatch));
		assertTrue(cl1.graph().index().getEdgeMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getEdgeMatches(bothMatch), cl2.graph().index().getEdgeMatches(bothMatch));
		assertTrue(cl1.graph().index().getItemMatches(bothMatch).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(bothMatch), cl2.graph().index().getItemMatches(bothMatch));
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}

	@Test public void testBufferedRegisterTwoRelationalDatasets()  throws SQLException {
		setUpDB("schema1", "schema2");
		try (Connection c1 = ConnectionManager.getConnection(POSTGRESQL, "junit", defaultUser, defaultPassword, "schema1");
				Statement s1 = c1.createStatement()) {
			s1.executeUpdate("CREATE TABLE schema1.test1(name TEXT, a TEXT)");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('foo', 'bar')");
			s1.executeUpdate("INSERT INTO schema1.test1 VALUES ('Emmanuel Macron', 'doe')");
			s1.executeUpdate("CREATE TABLE schema2.test2(b CHAR, name TEXT)");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (1, 'Emmanuel Macron')");
			s1.executeUpdate("INSERT INTO schema2.test2 VALUES (2, 'Emmanuel Macron')");
		}
		ConnectionLens cl1 = initWithDB("junit1", 1);
		ConnectionLens cl2 = initWithDB("junit2", 10);

		QueryComponent nodeMatch = new AtomicKeyword("doe");
		QueryComponent edgeMatch = new AtomicKeyword("name");
		QueryComponent bothMatch = Disjunction.of("doe", "name");

		try(RelationalDataSource ds11 = (RelationalDataSource) cl1.register(db1);
				RelationalDataSource ds12 = (RelationalDataSource) cl1.register(db2);
				RelationalDataSource ds21 = (RelationalDataSource) cl2.register(db1);
				RelationalDataSource ds22 = (RelationalDataSource) cl2.register(db2)) {

			assertTrue(cl1.graph().index().getDataSourceMatches(nodeMatch).size() > 0);
			assertEquals(cl1.graph().index().getDataSourceMatches(nodeMatch), cl2.graph().index().getDataSourceMatches(nodeMatch));
			assertTrue(cl1.graph().index().getNodeMatches(nodeMatch).size() > 0);
			assertEquals(cl1.graph().index().getNodeMatches(nodeMatch), cl2.graph().index().getNodeMatches(nodeMatch));
			assertTrue(cl1.graph().index().getEdgeMatches(nodeMatch).size() == 0);
			assertEquals(cl1.graph().index().getEdgeMatches(nodeMatch), cl2.graph().index().getEdgeMatches(nodeMatch));
			assertTrue(cl1.graph().index().getItemMatches(nodeMatch).size() > 0);
			assertEquals(cl1.graph().index().getItemMatches(nodeMatch), cl2.graph().index().getItemMatches(nodeMatch));

			assertTrue(cl1.graph().index().getDataSourceMatches(edgeMatch).size() > 0);
			assertEquals(cl1.graph().index().getDataSourceMatches(edgeMatch), cl2.graph().index().getDataSourceMatches(edgeMatch));
			assertTrue(cl1.graph().index().getNodeMatches(edgeMatch).size() == 0);
			assertEquals(cl1.graph().index().getNodeMatches(edgeMatch), cl2.graph().index().getNodeMatches(edgeMatch));
			assertTrue(cl1.graph().index().getEdgeMatches(edgeMatch).size() > 0);
			assertEquals(cl1.graph().index().getEdgeMatches(edgeMatch), cl2.graph().index().getEdgeMatches(edgeMatch));
			assertTrue(cl1.graph().index().getItemMatches(edgeMatch).size() > 0);
			assertEquals(cl1.graph().index().getItemMatches(edgeMatch), cl2.graph().index().getItemMatches(edgeMatch));

			assertTrue(cl1.graph().index().getDataSourceMatches(bothMatch).size() > 0);
			assertEquals(cl1.graph().index().getDataSourceMatches(bothMatch), cl2.graph().index().getDataSourceMatches(bothMatch));
			assertTrue(cl1.graph().index().getNodeMatches(bothMatch).size() > 0);
			assertEquals(cl1.graph().index().getNodeMatches(bothMatch), cl2.graph().index().getNodeMatches(bothMatch));
			assertTrue(cl1.graph().index().getEdgeMatches(bothMatch).size() > 0);
			assertEquals(cl1.graph().index().getEdgeMatches(bothMatch), cl2.graph().index().getEdgeMatches(bothMatch));
			assertTrue(cl1.graph().index().getItemMatches(bothMatch).size() > 0);
			assertEquals(cl1.graph().index().getItemMatches(bothMatch), cl2.graph().index().getItemMatches(bothMatch));
		}
		cl1.close();
		cl2.close();
		tearDownDB(); 
		tearDownDB("junit1", "junit2");
	}

	@Test public void testBufferedRegisterTwoTextDatasets() throws SQLException {
		ConnectionLens cl1 = initWithDB("junit1", 1);
		cl1.register(path1);
		cl1.register(path2);
		ConnectionLens cl2 = initWithDB("junit2", 10);
		cl2.register(path1);
		cl2.register(path2);

		QueryComponent match1 = new AtomicKeyword("edouard");
		QueryComponent match2 = new AtomicKeyword("nantes");
		QueryComponent match3 = Disjunction.of("edouard", "nantes");

		assertTrue(cl1.graph().index().getDataSourceMatches(match1).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(match1), cl2.graph().index().getDataSourceMatches(match1));
		assertTrue(cl1.graph().index().getNodeMatches(match1).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(match1), cl2.graph().index().getNodeMatches(match1));
		assertTrue(cl1.graph().index().getEdgeMatches(match1).size() == 0);
		assertEquals(cl1.graph().index().getEdgeMatches(match1), cl2.graph().index().getEdgeMatches(match1));
		assertTrue(cl1.graph().index().getItemMatches(match1).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(match1), cl2.graph().index().getItemMatches(match1));

		assertTrue(cl1.graph().index().getDataSourceMatches(match2).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(match2), cl2.graph().index().getDataSourceMatches(match2));
		assertTrue(cl1.graph().index().getNodeMatches(match2).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(match2), cl2.graph().index().getNodeMatches(match2));
		assertTrue(cl1.graph().index().getEdgeMatches(match2).size() == 0);
		assertEquals(cl1.graph().index().getEdgeMatches(match2), cl2.graph().index().getEdgeMatches(match2));
		assertTrue(cl1.graph().index().getItemMatches(match2).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(match2), cl2.graph().index().getItemMatches(match2));

		assertTrue(cl1.graph().index().getDataSourceMatches(match3).size() > 0);
		assertEquals(cl1.graph().index().getDataSourceMatches(match3), cl2.graph().index().getDataSourceMatches(match3));
		assertTrue(cl1.graph().index().getNodeMatches(match3).size() > 0);
		assertEquals(cl1.graph().index().getNodeMatches(match3), cl2.graph().index().getNodeMatches(match3));
		assertTrue(cl1.graph().index().getEdgeMatches(match3).size() == 0);
		assertEquals(cl1.graph().index().getEdgeMatches(match3), cl2.graph().index().getEdgeMatches(match3));
		assertTrue(cl1.graph().index().getItemMatches(match3).size() > 0);
		assertEquals(cl1.graph().index().getItemMatches(match3), cl2.graph().index().getItemMatches(match3));
		cl1.close();
		cl2.close();
		tearDownDB("junit1", "junit2");
	}
	
}

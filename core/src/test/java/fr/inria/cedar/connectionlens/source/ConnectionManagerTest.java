/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.source;

import static fr.inria.cedar.connectionlens.sql.ConnectionManager.makeURL;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.DatabaseTypes.POSTGRESQL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLensTest;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Ignore;
import org.junit.Test;

public class ConnectionManagerTest extends ConnectionLensTest {

	@Test
	public void testGetConnection() throws SQLException {
		// Don't autoclose master as it is use across the application and tests.
		Connection conn1 = ConnectionManager.getAdminConnection();
		try (Connection conn2 = ConnectionManager.getConnection(POSTGRESQL, "template1")) {
			assertSame(conn1, ConnectionManager.getAdminConnection());
			assertNotSame(conn2, ConnectionManager.getAdminConnection());
		}
	}

	@Test
	public void testMakeURLPostgreSQL() {
		String defaultUser = Config.getInstance().getProperty("RDBMS_user");
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?currentSchema=mySchema&user=user1&password=p4$$w0rd",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, "user1", "p4$$w0rd", "mySchema"));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?currentSchema=mySchema&user=user1",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, "user1", null, "mySchema"));
		assertEquals(
				"jdbc:postgresql://myHost:12123/MyDB?currentSchema=mySchema&user=" + defaultUser + "&password=p4$$w0rd",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, null, "p4$$w0rd", "mySchema"));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?currentSchema=mySchema&user=" + defaultUser,
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, null, null, "mySchema"));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?user=user1&password=p4$$w0rd",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, "user1", "p4$$w0rd", null));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?user=user1",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, "user1", null, null));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?user=" + defaultUser + "&password=p4$$w0rd",
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, null, "p4$$w0rd", null));
		assertEquals("jdbc:postgresql://myHost:12123/MyDB?user=" + defaultUser,
				makeURL(POSTGRESQL, "MyDB", "myHost", 12123, null, null, null));
	}
}

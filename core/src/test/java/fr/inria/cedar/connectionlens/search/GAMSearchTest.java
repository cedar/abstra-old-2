/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.search;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Set;

import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.score.ScoringFunction;

/**
 * @author Ioana Manolescu, Tayeb Merabti
 * 
 * General principle: we run a specific query on a specific graph and then
 * check that:
 * - it has the expected number of results
 * - the minimum and maximum size of these results is as expected
 * - the minimum and maximum score among these results is at less than 0.001 
 * distance from an expected value.
 * 
 *  To modify a test, uncomment the logging line in assertEqualSearch,
 *  to find out the actual sizes and score bounds for each query. Then,
 *  edit the call to assertEqualSearch to adjust the test's expectations. 
 */
@Ignore
public class GAMSearchTest extends SearchTest {

	static ScoringFunction sf;
	private static final Logger log = Logger.getLogger(GAMSearchTest.class);
	QuerySearch gamSearch;
	
	@BeforeClass
	public static void setupClass() throws URISyntaxException, SQLException {
		setUpDB();
		Config.getInstance().setProperty("update_batch_size", "1");
		Config.getInstance().setProperty("extract_dates", "false");

		cl = new ConnectionLens(extractor, analyzer, pp, true);
		inputs.clear();
		//sf = cl.resolveScoringFunction(""); //Madhu -- cannot set it up here as the scoring class requires the query as input.
		log.info("Inputs:"+inputs);
		inputs.add(new URI(Paths.get("file:data", "poc", "2", "deputes.json").toString()));
		inputs.add(new URI(Paths.get("file:data", "poc", "2", "fb-etienne-chouard.txt").toString()));
		inputs.add(new URI(Paths.get("file:data", "poc", "2", "medias.txt").toString()));
		inputs.add(new URI(Paths.get("file:data", "poc", "2", "tweet-Ruffin.json").toString()));
		cl.completeDataSourceSetRegistration(inputs);
		
	}
	
	private QuerySearch buildSearcher(long timeout, long maxSolutions, long stationarity, Query q) {
		sf = cl.resolveScoringFunction("", q);
		GAMSearch searcher = new GAMSearch(cl.graph().index(), cl.graph(), sf, stats);
		searcher.resetStoppers(timeout, maxSolutions, stationarity);
		return searcher; 
	}

	@Test
	public void testDemoQuery_Briand() throws IOException {
		Query q = Query.parse("Briand");
		QuerySearch searcher = buildSearcher(10000, (-1), (-1), q); 
		assertEqualSearch(searcher, q, 1, 0, 0, 0.883, 0.884);
	}

	@Test
	public void testDemoQuery_MerciPatron() throws IOException {
		Query q = Query.parse("MerciPatron");
		QuerySearch searcher = buildSearcher(10000, (-1), (-1), q); 
		assertEqualSearch(searcher, q, 2, 0, 0, 0.983, 1.0);
	}

	@Test
	public void testDemoQuery_Ruffin() throws IOException {
		Query q = Query.parse("Ruffin");
		QuerySearch searcher = buildSearcher(-1, 8, (-1), q); 
		assertEqualSearch(searcher, q, 8, 0, 0, 0.827, 1.0);
	}

	@Test
	public void testDemoQuery_Briand_Tonolli() throws IOException {
		Query q = Query.parse("Briand Tonolli");
		QuerySearch searcher = buildSearcher(10000, (-1), (-1), q); 
		assertEqualSearch(searcher, q, 1, 6, 6, 0.284, 0.285);
	}

	@Test
	public void testDemoQuery_Fakir_MerciPatron() throws IOException {
		Query q = Query.parse("Fakir MerciPatron");
		QuerySearch searcher = buildSearcher(10000, (-1), (-1), q); 
		assertEqualSearch(searcher, q, 1, 0, 0, 1.0, 1.0);
	}

	@Test 
	public void testDemoQuery_Briand_Pigasse() throws IOException {
		Query q = Query.parse("Briand Pigasse");
		QuerySearch searcher = buildSearcher(20000, 20, (-1), q); 
		// this query has hundreds of answers, and the first 20 are not
		// fully deterministically the same. That is why this test is only counting
		// how many solutions were found.
		assertEqualSearch(searcher, q, 20, -1, -1, -1, -1);
	}

	@Test
	public void testDemoQuery_Niel_Pigasse_Prisa() throws IOException {
		Query q = Query.parse("Niel Pigasse Prisa");
		QuerySearch searcher = buildSearcher(10000, 1, (-1), q); 
		//String query = "Niel Pigasse Prisa";
		assertEqualSearch(searcher, q, 1, 2, 2, 0.537, 0.537);
	}

	@Test
	public void testDemoQuery_Briand_Halluin_Tonolli() throws IOException {
		Query q = Query.parse("Briand Halluin Tonolli");
		QuerySearch searcher = buildSearcher(10000, 1, (-1), q); 
		assertEqualSearch(searcher, q, 1, 9, 9, 0.232, 0.232);
	}

	@Test
	public void testDemoQuery_BADDOU_Halluin_Fakir() throws IOException {
		Query q = Query.parse("BADDOU Halluin Fakir");
		QuerySearch searcher = buildSearcher(-1, 1, (-1), q); 
		assertEqualSearch(searcher, q, 1, -1, -1, -1, -1);
	}

	
	/**
	 * Test the result query produced by each query, with respect to expected minimum and maximum AT size and score.
	 */
	public void assertEqualSearch(QuerySearch gamSearch, Query parseQuery, int expectedNumberOfResults, int minExpectedSize, 
			int maxExpectedSize, double minExpectedScore, double maxExpectedScore) {
		//Query parseQuery = Query.parse(query); // query
		
		Set<AnswerTree> allAnswerTrees = getResults(gamSearch, parseQuery);
		assertEquals(expectedNumberOfResults, allAnswerTrees.size());
		
		int minSize = Integer.MAX_VALUE;  
		int maxSize = 0;
		double minScore = Double.MAX_VALUE;
		double maxScore = 0.0; 
		int i = 0; 
		for (AnswerTree at: allAnswerTrees) {
			double atScore = ((Double)sf.compute(at).value()).doubleValue(); 
			// To find the size and score of a result, uncomment this line: 
			//log.info("At " + i + "/" + allAnswerTrees.size() + ": AT of size " + at.edges().size() + " and score " + atScore);
			i ++; 
			if (at.edges.size() < minSize) { minSize = at.edges.size(); }
			if (at.edges.size() > maxSize) { maxSize = at.edges.size(); }
			if (atScore < minScore) { minScore = atScore; }
			if (atScore > maxScore) { maxScore = atScore; }
		}
		if (minExpectedSize > 0) { // if negative, ignore
			assertEquals(minExpectedSize, minSize);
		}
		if (maxExpectedSize > 0) { // if negative, ignore
			assertEquals(maxExpectedSize, maxSize);
		}
		log.info("MinScore: " + minScore);
		log.info("MaxScore: " + maxScore);
		if (minExpectedScore > 0.0) { // if negative, ignore
			assert(Math.abs(minScore - minExpectedScore) < 0.001); 
		}
		if (maxExpectedScore > 0.0) { // if negative, ignore
			assert(Math.abs(maxScore - maxExpectedScore) < 0.001); 
		}
	}

	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		cl.close();
		tearDownDB();
	}
}

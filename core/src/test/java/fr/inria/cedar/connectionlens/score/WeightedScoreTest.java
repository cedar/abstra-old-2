/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

package fr.inria.cedar.connectionlens.score;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import com.google.common.collect.ImmutableList;
import fr.inria.cedar.connectionlens.score.Score.DoubleScore;
import java.util.Random;
import org.junit.Test;

public class WeightedScoreTest {

	@Test public void testEmptyScores() {
		ScoringFunction f = new WeightedScore(new ScoringFunction[0], new Double[0]);
		Score s = f.compute(null);
		assertEquals(0., ((DoubleScore) s).value(), .0);
	}

	@Test public void testRandomScores() {
		Random random = new Random();
		Scorable s = mock(Scorable.class);
		ImmutableList.Builder<ScoringFunction> f = ImmutableList.builder();
		ImmutableList.Builder<Double> w = ImmutableList.builder();
		double expected = 0.0;
		int l = random.nextInt(100);
		for (int i = 0; i < l; i++) {
			ScoringFunction sf = mock(ScoringFunction.class);
			double ss = random.nextDouble();
			double ww = random.nextDouble();
			when(s.score(sf)).thenReturn(new DoubleScore(ss));
			f.add(sf);
			w.add(ww);
			expected += ww * ss;
		}
		ScoringFunction sf = new WeightedScore(
				f.build().toArray(new ScoringFunction[l]), w.build().toArray(new Double[l]));
		assertEquals(expected, ((DoubleScore) sf.compute(s)).value(), .0);
	}
}

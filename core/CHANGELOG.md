August 2, 2018
==============

Architecture Changes
--------------------

# Overview
The new code base does not strictly map the classes to the table in the database, rather, it focuses on the function we need.
They are not two high-level interfaces `Graph` and `Index`, which deal respectively with the graph matter (node/edge/path)-related stuffs,
and retrieve nodes and edges based on the content of their labels. Things like same-as links and path-2-borders are dealt with inside the graph interface.
The postgres implementation can we swap with something else, for either or both (i.e. there is no dependency between the two).
The database has only slightly changed for now, table `index` has been renamed `keyword_index`, `labels` has been split into `nodes` and `edges` and `wnc` is now `weak_same_as`.
Due to the way things were coded initially, a lot of the work had to be done for both nodes and edges, while it is often preferable to work at the level of node OR edge. Many methods had to pass on large number of objects to deal with database access. Now, there is less back-and-forth between the database and memory. Many tests were not testing anything, or redundant with some other tests. I did a overhaul, and started a bunch of new tests making sure the most crucial features are working at all time (which does not mean we cannot do more, of course).
Another outcome of these architecture changes is that registering, finding same-as, and path computation, can more easily be done in parallel and/or asynchronously now. Although, this still needs to be tested.

# Graph & Index
The (virtual) `Graph` interface provides `read` access to methods for retrieving nodes/edges/paths based on their DS or label.
Every `write` access to the graph go through a `GraphSession` interface, which allows to control how we want to spill changes to the database. The `Index` follows the same patterns, where the interface is used for `read` access and the `IndexSession` is used for `write` access.
For instance, when set to `true`, `bulk_inserts` boolean parameters perform all update in memory until the session closes, then the database is actually updated. Otherwise, every update it applied on the database on-you-go.
The sessions also make it more easy to control concurrency if we would to have different threaded write to the graph or index in parallel.

# Extractors
The way `LazyOpenCalaisExtractor` used for work has been generalized to any extractor. It is controlled with the command line parameters `"-L", "--lazy-extractor"`, i.e. if set, whichever extractor is used will cache a copy of its results to disk which will be used in subsequent calls.
The new indexing, in which extracted entities are stored as a child of the node they were extracted from, is part of this update.

# Nodes/Edges
The new code attempts to minimize the access to database to load and off-load nodes and edges.
One important thing to note is that implicit nodes/edges (e.g. structural ones) are not stored in the database, but keep in memory are the level of the datasource. The params `node_cache` and `edge_cache` can be adjusted to control how many nodes/edges should be kept in memory.

# Same As
The same-as exploration is in two phase, asking the graphs to potential candidates pair (this is provided by the graph interface),
and actually evaluating the similarity, which is now encapsulated in the SimilarityEstimator class.
The `similarity_cache` params controls how many pair comparisons to keep in cache.
The `weak_same_as` table was (renamed from `wnc`) now has a `signature` column of type integer[]. In there we can store numeric features of the labels, such as the length or the output of some LHS function, to get all the candidate pairs in a single SQL query. This is currently used to get pair whose label length is within a margin given by the threshold (a test that was already part of the similarity function).
A major bottleneck we currently have is same_as comparisons, which is quadratic, further affects the path computation.
It is disproportionately bad for JSON and RELATIONAL, because we treat every value node as unique. So some values appearing many time is a column will give way to many redundant comparisons.
We have introduce two new optimizations.
Parameter `entities_only`, limits the comparison to entity of the same type. The two motivation for this are: (i) it is not desirable to compare "Chatillon":LOCATION with "Chatillon":PERSON, (ii) things there were not detected as entities are of lesser interest.
Parameter `value_atomicity` determines how to we treat the atomicity of value nodes (in JSON and RELATIONAL). PER_INSTANCE is was we have been doing so far, i.e. every instance of string is unique; PER_VALUE means that all node sharing the same label coincide with the same data source; PER_TYPE is something in between, whereby every node with a given label on a given path coincide. E.g. the "Chatillon" on two distinct tuples of a given column coincide, but `myschema.employee.lastname:Chatillon` and `myschema.employee.city:Chatillon` do not.
Note that semantically, these do not impact the results, since structural node remains the same.
Preliminary results on this are quite encouraging.

# Paths 2 Borders
Huong's P2BComputation has been plugged in, without the need to build map in the first place. However, when testing this, I could not get correct results for the exhaustive case (there were no such tests, so I created a new one). To be sure, I coded a default exhaustive path search from scratch, which is obviously expensive, but works. Running P2BComputation with heuristic produces results but, currently, numbers indicate P2B is slower in the new code and in what Huong's reported earlier. We yet have to understand where the time is lost.

# Logging
With a few exceptions, the logging is not mostly done in one place (the ConnectionLens class) to minimise logging mistakes.

# What remains
 - Query answering, local search in mostly working (need better test), but the global search still need to be plugged in.
- On the demo side, the change are minor. But we should be able to able an import feature where the registration is done live (usual quick) and the sameas/path computation is done in the background.
 - Finally, the new architecture should be it easier to switch to the all numeric IDs approach we have been discussing for a while.




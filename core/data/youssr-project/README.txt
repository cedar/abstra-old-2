The .nt and .json files are those produced by Youssr's code as of her defense on April 30.

attempt.txt is an attempt to transform some parts of the JSON in natural language, hoping to improve the extraction results.
This transformation could be trivially coded. 

attempt-fixed.txt is the result of a manual pass over attempt.txt, trying to smooth out some syntactic quirks that may confuse extraction.
These fixes would be harder to code (more human knowledge involved).

Italy’s Health Care System Groans Under Coronavirus — a Warning to the World
By Jason Horowitz
15-19 minutes
In less than three weeks, the virus has overloaded hospitals in northern Italy, offering a glimpse of what countries face if they cannot slow the contagion.
A makeshift emergency unit  at the Brescia hospital, in northern Italy, on Thursday.
A makeshift emergency unit  at the Brescia hospital, in northern Italy, on Thursday.Credit...Luca Bruno/Associated Press
Jason Horowitz
Paris is capital of France.
    Published March 12, 2020Updated March 13, 2020, 12:46 a.m. ET
ROME — The mayor of one town complained that doctors were forced to decide not to treat the very old, leaving them to die. In another town, patients with coronavirus-caused pneumonia were being sent home. Elsewhere, a nurse collapsed with her mask on, her photograph becoming a symbol of overwhelmed medical staff.
In less than three weeks, the coronavirus has overloaded the health care system all over northern Italy. It has turned the hard hit Lombardy region into a grim glimpse of what awaits countries if they cannot slow the spread of the virus and ‘‘flatten the curve’’ of new cases — allowing the sick to be treated without swamping the capacity of hospitals.
If not, even hospitals in developed countries with the world’s best health care risk becoming triage wards, forcing ordinary doctors and nurses to make extraordinary decisions about who may live and who may die. Wealthy northern Italy is facing a version of that nightmare already.
“This is a war,” said Massimo Puoti, the head of infectious medicine at Milan’s Niguarda hospital, one of the largest in Lombardy, the northern Italian region at the heart of the country’s coronavirus epidemic.
He said the goal was to limit infections, stave off the epidemic and learn more about the nature of the enemy. “We need time.”
This week Italy put in place draconian measures — restricting movement and closing all stores except for pharmacies, groceries and other essential services. But they did not come in time to prevent the surge of cases that has deeply taxed the capacity even of a well-regarded health care system.
Italy’s experience has now underscored the need to act decisively — quickly and early — well before case numbers even appear to reach crisis levels. By that point, it may already be too late to prevent a spike in cases that stretches systems beyond their limits.
With Italy having appeared to pass that threshold, its doctors are finding themselves in an extraordinary position largely unseen by developed European nations with public health care systems since the Second World War.
Regular doctors are suddenly shifting to wartime footing. They face questions of triage as surgeries are canceled, respirators become rare resources, and officials propose converting abandoned exposition spaces into vast intensive care wards.
Hospitals are erecting inflatable, sealed-off infectious disease tents on their grounds. In Brescia, patients are crowded into hallways.
“We live in a system in which we guarantee health and the right of everyone to be cured,” Prime Minister Giuseppe Conte said on Monday as he announced the measures to keep Italians in their homes.
Video
Video player loading
A dozen ambulances waited to transfer patients to the emergency room of a hospital in Merate, in northern Italy, on March 10.CreditCredit...Merate Online, via Youtube
“It’s a foundation, a pillar, and I’d say a characteristic of our system of civilization,” he said. “And thus we can’t allow ourselves to let our guard down.”
For now, Italian public health experts argue that the system, while deeply challenged, is holding, and that all the thousands of people receiving tests, emergency room visits and intensive care, are getting it for free, keeping a central principle of Italian democracy intact.
But before the region of Lombardy centralized its communication on Thursday and seemed to muzzle doctors and nurses who spoke out about the conditions, there emerged troubling pictures of life inside the trenches against the infection.
A photo of one nurse, Elena Pagliarini, who collapsed face down with her mask on in a hospital in the northern town of Cremona after 10 straight hours of work, became a symbol of an overwhelmed system.
“We are on our last legs, physically and physiologically,” Francesca Mangiatordi, a colleague who took the picture said on Italian television on Wednesday, urging people to protect themselves to avoid spreading the virus. “Otherwise the situation will collapse, provided it hasn’t already.”
A doctor in a hospital in Bergamo this week posted on social media a graphic account of the stress on the health system by the overwhelming number of patients.
“The war has literally exploded and battles are uninterrupted day and night,” the doctor, Daniele Macchini wrote, calling the situation an “epidemiological disaster” that has “overwhelmed” the doctors.
Fabiano Di Marco, head of pulmonology at the Papa Giovanni XXIII hospital in Bergamo, where he has taken to sleeping in his office, said Thursday that doctors literally “draw a line on the ground to divide the clean part of the hospital from the dirty one,” where anything they touch is considered contagious.
Giorgo Gori, the mayor of Bergamo, said that in some cases in Lombardy the gap between resources and the enormous influx of patients “forced the doctors to decide not to intubate some very old patients,” essentially leaving them to die.
“Were there more intensive care units,” he added, “it would have been possible to save more lives.”
Dr. Di Marco disputed the claim of his mayor, saying that everyone received care, though he added, “it is evident that in this moment, in some cases, it could happen that we have a comparative evaluation between patients.”
On Thursday, Flavia Petrini, the president of the Italian College of Anesthesia, Analgesia, Resuscitation and Intensive Care, said her group had issued guidelines on what to do in a period that bordered on wartime “catastrophe medicine.”
“In a context of grave shortage of health resources,” the guidelines say, intensive care should be given to “patients with the best chance of success” and those with the “best hope of life” should be prioritized.
The guidelines also say that in “in the interests of maximizing benefits for the largest number,” limits could be put on intensive care units to reserve scarce resources to those who have, first, “greater likelihood of survival and secondly who have more potential years of life.”
“No one is getting kicked out, but we’re offering criteria of priority,” Dr. Petrini said. “These choices are made in normal times, but what’s not normal is when you have to assist 600 people all at once.”
Giulio Gallera, the Lombardy official leading the emergency response, said on Thursday that he hoped the guidelines never needed to be applied.
He also said the region was working with Italy’s civil protection agency to study the possibility of using an exhibition space abandoned by canceled conventions as a 500-bed intensive care ward.
But, he said, the region needed doctors, and respirators.
“The outbreak has put hospitals under a stress that has no precedents since the Second World War,” said Massimo Galli, the director of infectious diseases at Milan’s Sacco University hospital, which is treating many of the coronavirus patients. “If the tide continues to rise, attempts to build dams to retain it will become increasingly difficult.”
Dr. Galli pointed out that while the government’s emergency decrees had sought to boost the hiring of thousands of doctors and health workers — including medical residents in their last years of medical school — it took time to train new doctors, even those transferred from other departments, who had little experience with infectious diseases. Doctors are also highly exposed to contagion.
Matteo Stocco, the director of the San Paolo and San Carlo hospitals in Milan, said 13 members of his staff were home after testing positive for the virus. One of his primary emergency room doctors was also infected, he said, “after three weeks of continuous work, day and night on the field.”
Dr. Puoti, of Niguarda hospital, said the doctors kept distance from one another in the cafeteria, wore masks during staff meetings and avoided gathering in small rooms. Still, he said, some had been infected, which created the risk of greater personnel shortages.
“We’re trying to keep a humanly sustainable level of work,” he said. “Because this thing is going to last.”
He said the hospital was trying to buy more respirators and preparing for the possibility that patients would come not only from the surrounding towns, but because of a wave of infections in Milan.
Dr. Stocco said that moment had already arrived.
Fifty people showed up in the emergency room on Thursday afternoon with respiratory problems, he said. The hospital had already canceled surgeries and diverted beds and respirators to coronavirus patients, and doubled its intensive care capacity.
“The infection is here,” he said.
Carlo Palermo, president of the association representing Italy’s public hospital doctors, said the system had so far held up, despite years of budget cuts. It also helped, he said, that it was a public system. Had it been an insurance-based system, there would have been a “fragmented” response, he said.
He said that since about 50 percent of the people who tested positive for the virus required some form of hospitalization, there was an obvious stress on the system. But the 10 percent needing intensive care, which requires between two and three weeks in the hospital, “can saturate the capacity of response.”
Many experts have noted that if the wealthy and sophisticated northern Italian health care system cannot bear the brunt of the outbreak, it is highly unlikely that the poorer south would be able to cope.
If the virus spread south at the same rate, Dr. Palermo said, “the system won’t hold up, and we won’t be able to assure care.”
Video
Video player loading
Police warn residents of Cutro, southern Italy, against unnecessary movements.CreditCredit...Annarachele, via Twitter
Many experts have warned that Italy is about 10 days ahead of other European countries in the development of its outbreak. Chancellor Angela Merkel of Germany has raised the alarm that about 70 percent of Germans could get the virus.
And reports of the overwhelmed Italian system have resonated in the United States, where President Trump closed flights to foreigners coming from Europe on Wednesday night.
“The Italian disease is becoming a European disease and Trump, with his decision, is trying to avoid that this becomes an American disease,” said Romano Prodi, a former Italian prime minister and president of the European Union commission.
“In any case I think that coronavirus is already also an American problem,” he said, adding that, because of the difference in the health care system, “it may be more serious than the European one.”
Elisabetta Povoledo contributed reporting from Rome, Emma Bubola from Verona, Anna Momigliano from Milan, and Barbara Marcolini and Haley Willis from New York.
    Updated March 12, 2020
        What is a coronavirus?
        It is a novel virus named for the crownlike spikes that protrude from its surface. The coronavirus can infect both animals and people and can cause a range of respiratory illnesses from the common cold to lung lesions and pneumonia.
        How contagious is the virus?
        It seems to spread very easily from person to person, especially in homes, hospitals and other confined spaces. The pathogen can travel through the air, enveloped in tiny respiratory droplets that are produced when a sick person breathes, talks, coughs or sneezes.
        Where has the virus spread?
        The virus, which originated in Wuhan, China, has sickened more than 132,300 in at least 111 countries and more than 4,900 have died. The spread has slowed in China but is gaining speed in Europe and the United States. World Health Organization officials said the outbreak qualifies as a pandemic.
        What symptoms should I look out for?
        Symptoms, which can take between two to 14 days to appear, include fever, a dry cough, fatigue and difficulty breathing or shortness of breath. Milder cases may resemble the flu or a bad cold, but people may be able to pass on the virus even before they develop symptoms.
        How do I keep myself and others safe?
        Washing your hands frequently is the most important thing you can do, along with staying at home when you’re sick and avoiding touching your face.
        How can I prepare for a possible outbreak?
        Keep a 30-day supply of essential medicines. Get a flu shot. Have essential household items on hand. Have a support system in place for elderly family members.
        What if I’m traveling?
        The C.D.C. has advised against all non-essential travel to South Korea, China, Italy and Iran. And the agency has warned older and at-risk travelers to avoid Japan.The State Department has advised Americans against traveling on cruise ships.
        How long will it take to develop a treatment or vaccine?
        Several drugs are being tested, and some initial findings are expected soon. A vaccine to stop the spread is still at least a year away.
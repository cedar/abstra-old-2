Une piste de sortie pour Macron dans le dossier de Notre-Dame-des-Landes
6-7 minutes

L’évacuation de la ZAD, occupée par plusieurs centaines d’opposants à l’aéroport, reste, quel que soit le scénario retenu, une opération délicate à mener.

LE MONDE | 14.12.2017 à 10h26 • Mis à jour le 14.12.2017 à 10h30 | Par Rémi Barroux
A Notre-Dame-des-Landes, en juillet. JEAN-FRANCOIS MONIER / AFP

Emmanuel Macron et le gouvernement vont peut-être trouver la voie de sortie, dans ce dossier vieux d’une cinquantaine d’années de l’aéroport de Notre-Dame-des-Landes. En proposant deux scénarios, la réalisation du projet du nouvel aéroport dans le bocage, au nord de Nantes, et le réaménagement de l’actuelle plate-forme aéroportuaire de Nantes-Atlantique, les trois médiateurs, qui ont remis leur rapport au premier ministre, Edouard Philippe, mercredi 13 décembre, ont tendu une perche au président de la République, qui annoncera son choix avant la fin du mois de janvier.

Celui-ci peut se prévaloir de nouvelles informations, crédibilisant le scénario, défendu depuis toujours par les opposants à la construction de Notre-Dame-des-Landes, d’une modernisation de l’aéroport existant.

Alors que ce projet est encalminé depuis son origine, dans les années 1970, il va enfin trouver sa conclusion. Emmanuel Macron l’a confirmé dans un entretien au Monde (13 décembre), jugeant que l’information sur ce dossier avait été, jusqu’alors, « insuffisante ».

Lire aussi :   Le réaménagement de l’aéroport de Nantes Atlantique devient crédible

Ce faisant, le chef de l’Etat répond au principal argument des partisans du transfert de l’actuel aéroport nantais : le déni de démocratie, avec la négation du résultat du scrutin départemental du 26 juin 2016, qui avait apporté une nette majorité de réponses favorables à l’unique question posée : « Etes-vous favorable au projet de transfert de l’aéroport de Nantes Atlantique sur la commune de Notre-Dame-des-Landes ? »

En avançant qu’« on prend toujours les meilleures décisions en acceptant le débat démocratique, mais instruit et informé de manière indépendante », Emmanuel Macron prépare le terrain. Le report de sa décision, annoncée à l’origine avant la fin de l’année, qui ne sera rendue qu’en janvier, laisse le temps pour d’éventuelles précisions sur le scénario de réaménagement de l’actuel aéroport et, surtout, offre un délai afin de tenter d’amadouer les élus locaux, dont l’immense majorité, quel qu’en soit le bord politique, sont attachés à ce projet de transfert. Edouard Philippe, le premier ministre, lors de la remise du rapport, a d’ailleurs rappelé la nécessité de « faire de la pédagogie ». Il a aussi confirmé qu’il y aurait bien une « décision claire et assumée par le gouvernement ».

Jusqu’alors, aucune autorité, de gauche comme de droite, n’a été capable de trancher. Que ce soit François Fillon, favorable au projet de nouvel aéroport, quand il était premier ministre, lorsque le décret de déclaration d’utilité publique était publié, en 2008. Ou Jean-Marc Ayrault, ardent défenseur du projet, comme maire de Nantes ou président de la région des Pays de la Loire, et premier ministre socialiste de 2012 à 2014. Ce dernier a même tenté, à l’automne 2012, avec Manuel Valls comme ministre de l’intérieur, l’opération « César » d’évacuation de la ZAD, la zone à défendre occupée par les opposants, qui s’est soldée par un échec retentissant.
Le syndrome de Sivens

Aucun gouvernement ni chef de l’Etat n’a donc conclu, laissant au suivant le soin de décider, et la situation s’enliser. Emmanuel Macron hérite de la « patate chaude » et va conclure, au côté de son ministre de la transition écologique et solidaire, Nicolas Hulot, icône de l’écologie et opposé au projet. Au sein du gouvernement, les avis sont partagés. Le ministre des affaires étrangères, le socialiste Jean-Yves Le Drian, ancien président de la région Bretagne, est favorable à la construction de Notre-Dame-des-Landes.

Le chef de l’Etat n’a pas le choix, car la déclaration d’utilité publique (DUP) arrive à échéance le 8 février 2018, sa durée étant de dix ans. Pour ménager l’avenir, le gouvernement pourrait déposer devant le Conseil d’Etat un décret de prorogation de cette DUP. Côté judiciaire, les recours déposés par les opposants sur les décrets préfectoraux « loi sur l’eau » et « protection des espèces protégées » font encore l’objet d’un pourvoi en cassation devant le Conseil d’Etat.

Quel que soit le choix du gouvernement, il reste le très délicat problème de la ZAD et de ses quelque 300 occupants. Si la décision de la construction à Notre-Dame-des-Landes était prise – le scénario le moins probable – l’opération d’évacuation susciterait de nouveau une très forte résistance, soutenue par un mouvement national de solidarité, à même de mobiliser des dizaines de milliers de personnes sur le site. Dans l’hypothèse de l’abandon du projet, le problème demeure pour le gouvernement. Les médiateurs ont été clairs sur ce point. La réalisation du nouveau scénario « ne pourrait s’accompagner du maintien d’une zone de non-droit sur le site de Notre-Dame-des-Landes ».

L’opération d’évacuation, même si le mouvement de solidarité sera moins virulent du fait de l’abandon du projet de nouvel aéroport, reste délicate. De 2 000 à 3 000 gendarmes et policier seraient nécessaires et les risques d’affrontements violents, avec de possibles victimes, réels.

Le syndrome de Sivens, dans le Tarn, où Rémi Fraisse, un jeune manifestant opposé au projet de barrage, avait été tué par une grenade lancée par un gendarme, le 26 octobre 2014, hante l’Elysée. « Quel serait alors l’intérêt de prendre un tel risque, d’engager de telles dépenses si on ne fait pas l’aéroport ? », notent certains partisans du transfert. Malgré l’éclairage des médiateurs, la conclusion du dossier Notre-Dame-des-Landes reste périlleuse pour M. Macron.

Je demande à tous mes accusateurs (et à leur média), Messieurs Apathie, Joffrin, Baddou, Schneiderman et les autres, de me permettre de me défendre loyalement, en direct et en face-à-face, à la fois le RIC pour les gilets jaunes et mon travail pour le bien commun.

Si on est bien en « démocratie » et s’ils sont honnêtes, comme ils le prétendent tous, ça devrait pouvoir se faire.

Ecrit par Etienne Chouard.

20 décembre 2018.

Mais leur problème principal, c’est que, quoi qu’ils décident à propos de mon droit de réponse, en l’occurrence tuer le messager ne suffira plus, car il y a désormais dans le pays des centaines d’autres messagers tout aussi compétents et bons orateurs (c’est-à-dire respectueux des autres) pour prendre ma place : ce qui est né en France, avec les Gilets jaunes et le RIC comme première marche intellectuelle vers une aspiration populaire à un processus constituant populaire (débarrassé des professionnels de la politique), c’est une prise de conscience contagieuse, à la base de la société, que le niveau constituant est le meilleur pour engager les luttes sociales : le niveau législatif est mauvais pour nous émanciper car 1) il nous enferme dans une impuissance politique qui est verrouillée à un niveau supérieur, inaccessible, et 2) il nous empêche de fraterniser contre l’oppression des riches du moment par des disputes sans fin sur des sujets secondaires.

Le niveau législatif, celui où l’on décide quelles sont les lois qu’il nous faut aujourd’hui, thème par thème, est celui des disputes sans fin, alors que le niveau constituant, celui où l’on décide comment l’on va mettre au point les lois, qui va nous représenter, avec quel mandat, sous quels contrôles et avec quelles possibilités de révocation, ce niveau constituant est celui de la concorde facile car la plupart de nos aspirations y convergent, que nous soyons de droite, de gauche ou d’autre chose : tout le monde comprend à toute vitesse que notre cause commune, la cause commune des 99% partout sur terre, c’est : « Nous voulons instituer nous-mêmes notre propre puissance ».

Il me semble que ce pourrait être la devise des Gilets Jaunes du monde entier.

Cette mutation universellement contagieuse va tout changer : les électeurs enfants sont en train d’aspirer à devenir citoyens constituants, et ils n’auront pas à le demander à leurs maîtres : il leur suffira de le vouloir vraiment, ensemble. Étienne de la Boétie l’avait excellemment prédit : soyez résolus de ne plus servir, et vous voilà libres.

Je peux me tromper, bien sûr, et je passe mon temps à chercher des contradicteurs pour trouver mes erreurs et progresser, mais il est extravagant de me faire passer pour un tyran qui avancerait masqué. Extravagant.

Je vais reproduire à la fin de ce billet un message important que j’avais rédigé en novembre 2014 (il y a 4 ans déjà), où je faisais le point sur les accusations à propos de Soral. Vous jugerez. Je souligne simplement que, personnellement, je ne parle jamais de Soral, absolument jamais, et que, par contre, tous ceux qui m’accusent de le fréquenter (ce qui n’est pas vrai), eux, en parlent tout le temps… comme si c’était ces imprécateurs eux-mêmes qui étaient chargés de la promotion quotidienne du personnage qu’ils prétendent combattre.

Bref, devant ce torrent de haine recuite, tournant en boucle et auto-entretenue, de la part des « grands » éditorialistes du pays, je me dis que, finalement, être ainsi craint par ces gens-là, c’est un peu comme une Légion d’honneur, une marque de vraie résistance : il semble donc que je ne sois pas, moi, une opposition contrôlée (Cf. 1984 d’Orwell : une opposition dont le pouvoir n’a rien à craindre).

Finalement, il est assez logique que je sois détesté par cette bande de détestables : je rappelle que tous ces « journalistes » (ce sont les mêmes propagandistes qui défendaient tous le Oui pour le référendum contre l’anticonstitution européenne en 2005 et qui étiquetaient déjà « extrême droite » tous leurs adversaires défenseurs du Non, pour ne pas avoir à leur répondre sur le fond), tous ces « journalistes » qui me traitent de « facho », donc, défendent ardemment, depuis 40 ans (depuis le début des années 1980), à la fois le fléau du néo-libéralisme et celui du libre-échange, la catastrophe absolue qu’est la libre circulation des capitaux et donc l’évasion fiscale, les délocalisations et la désindustrialisation du pays, la dérégulation financière et la dépossession des États du pouvoir de création monétaire, le transfert scandaleux de la souveraineté nationale (qui ne leur appartenait pourtant pas) à des institutions supranationales tyranniques hors contrôle et corrompues jusqu’à la moelle, la flexibilité et l’austérité, la désindexation des salaires et des retraites, la rigueur et les coups de ceinture pour les pauvres, les cadeaux somptueux et obscènes pour les plus riches, insatiables pompes à fric, véritables siphons à pognon privant la société des signes monétaires nécessaires à la prospérité, l’asphyxie financière des services publics pour en faire à terme des centres de profit privé, la vente à vil prix des biens publics rentables (autoroutes, péages, aéroports, barrages, industries stratégiques…) aux parrains maffieux qui les ont mis en place à leurs micros, et j’en passe… Il est assez logique que ces défenseurs du capitalisme déchaîné (et de son principal outil, la prétendue Union européenne) ne nous aiment pas et qu’ils nous craignent, moi et les Gilets jaunes devenant constituants.

Mille mercis à tous ceux qui me défendent comme ils peuvent, sur les réseaux sociaux et dans les conversations, il est facile de comprendre combien pour moi c’est émouvant.

Et notamment merci à ceux qu’on appelle les gentils virus démocratiques, dont j’observe tous les jours le dévouement au bien commun et à la démocratie qui vient.

Merci aussi à RussiaToday,  la chaîne de télé russe qui assume désormais quasiment seule en France le service public de Résistance à l’oppression, en donnant la parole à tout le monde et en permettant de bons débats de fond sur des sujets importants. J’ai rencontré leurs équipes et j’ai été frappé par leur professionnalisme et leur rigueur. Probablement parce qu’ils n’ont aucun droit à l’erreur (le gouvernement et ses complices « journalistes » les traquent depuis leur création), et aussi peut-être parce qu’ils ne suivent pas, eux, un idéal lié au profit ou à la domination, les articles et vidéos publiés par RT semblent les plus fiables du pays.

Merci aussi à François Ruffin, pour son courage. Ce qu’il a fait ne m’étonne pas de lui : il est profondément honnête. C’est sans doute l’homme politique que j’admire le plus dans mon pays (malgré quelques profonds désaccords, notamment sur la très nécessaire sortie de l’UE). J’espère que ses amis (qui sont aussi les miens, dans ma tête en tout cas) ne le martyriseront pas pour ce qu’il a dit de moi (qui n’est quand même pas si grave).

Bon, les Gilets jaunes, on continue d’apprendre à constituer, sur les péages et les ronds-points ? On se fout de ces voleurs de pouvoir, on ne leur demandera pas la permission pour s’émanciper de leur domination. Il faut par contre qu’on s’entraîne, hein ? Allez, à tout à l’heure !
(ce soir jeudi à Bordeaux, vendredi à Périgueux, samedi dans le Lot, dimanche à Brignoles, ou à Toulon je ne sais plus 🙂 )

Amitiés à tous (vraiment à tous).

Étienne.

#GiletsJaunesConstituant

#CeNestPasAuxHommesAuPouvoirDÉcrireLesRèglesDuPouvoir

#PasDeDémocratieSansCitoyensConstituants

Quand le message est trop fort, attaquer le messager…

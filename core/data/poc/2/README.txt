Scenarios tried on April 5, 2019, for Florence Parly:
  java -jar target/connection-lens-full-0.5.0-SNAPSHOT-bugfix-93-*.jar  -i data/poc/2/deputes.json,data/poc/2/tweet-Ruffin.json,data/poc/2/fb-etienne-chouard.txt,data/poc/2/medias.txt -Dcandidate_pairs_variant=COMMON_WORD -Dp2b_stopper_timeout_per_border=-1 -DRDBMSDBName=cl_demo
     
     Query: "François Ruffin" "Etienne Chouard" - prendre résultat de 1 source (3 noeuds), puis de 2 sources
     Query: "Russie" "Ruffin" "Assemblée Nationale" - prendre dans le 1er paquet (11 noeuds) le 1er résultat 
     

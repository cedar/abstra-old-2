===============================
Contributing to Connection Lens
===============================

About
=====

This document provides a set of best practices for contributions to this project -
bug reports, code submissions / pull requests, etc.

Submitting issues
=================

Issue tracker
-------------
Our issue tracker currently has the following labels:
 - **bug/critical**: Crashes the program or introduces inconsistencies
 - **bug/major**: Blocks development
 - **bug/minor**: Non-blocking problem
 - **feature request**: Non-trivial enhancements
 - **discussion**: General help, architecture changes, optimizations and other enhancements

Typically, bugs and feature requests lead to branches named accordingly (see "Version Control"
section). It is a good idea to log an issue even if you are the one assigned to it. This makes
it easier for other to track what other onto.

The discussion tags are means to topics that aims to clarify or reached agreement on things before
there are turned into something else. The outcome of discussion may be the introduction of a new
bug or feature request as well as a new entry in the wiki.  

Reporting bugs
--------------



* Before submitting a bug, please perform **basic due dilligence ** steps:

    * **Make sure you're on the latest version.** If you're not on the most
      recent version, your problem may have been solved already! Upgrading is
      always the best first step.
    * **Try older versions.** If you're already *on* the latest release, try
      rolling back a few minor versions (e.g. if on 1.7, try 1.5 or 1.6) and
      see if the problem goes away. This will help the devs narrow down when
      the problem first arose in the commit log.
    * **Try switching up dependency versions.** If the software in question has
      dependencies (other libraries, etc) try upgrading/downgrading those as
      well.
    * **Search the project's bug/issue tracker** to make sure it's not a known issue.
  
* Describe the issue in **sufficient details** so others can reproduce the problem.
  This includes specifying the **branch(es), version(s)** where you know the issue occurs.

* Providing a **minimal reproducible example (MRE)** is often good enough.
  You may use the template available in the issue tracker to get inspiration.
  

Version control branching
=========================

* The branch structure is loosely inspired from the following.

  <img src="https://nvie.com/img/git-model@2x.png" alt="Git Model" width="600"/>
  
* Our working branch is named `develop`. This is the default branch to fork from when working
  on new features. It should have stable code (i.e. that builds and passes the test suite)
  at any time.
  
* The `master` branch always contains the most recent stable version of the project at any time.
  It is only ever get merged from the `develop` branch, when we judge the latter to be stable 
  enough.
  
* We do not have release branches for now. Releases are published through tags on `master`.

* You are free to name **local branches** however you like, but branches pushed to the common 
  repository should be prefixed with either `bugfix-*` or `feature-*`, followed but a short clear 
  description, possibly including an issue number.

* Always **make a new branch** for your work, no matter how small. This makes
  it easy for others to take just that set of changes from your repository,
  in case you have multiple unrelated changes floating around.

    * A corollary: **don't submit unrelated changes in the same branch/pull
      request**! The maintainer shouldn't have to reject your awesome bugfix
      because the feature you put in with it needs more review.

* **Base your new branch off of the appropriate branch** on the main repository:

    * **Bug fixes** should be based on the branch named after the **oldest changeset** the bug
      affects.

    * **New features** should branch off of **`develop`** branch.

        * Note that depending on how long it takes for the dev team to merge
          your patch, the copy of ``master`` you worked off of may get out of
          date! If you find yourself 'bumping' a pull request that's been
          sidelined for a while, **make sure you rebase or merge to latest
          master** to ensure a speedier resolution.

* **Never push unstable code on `develop` or `master`**, and refrain from pushed unstable code to 
  the common repo. 
  If you have too, prefix the commit message with **"WIP:" (Work In Progress)**, so others
  known what to expect.
  
* When a sequence of multiple commits have to pushed for a single change, it is a good idea to
  [squash](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#_squashing) them in
  a single commit. This makes it easier for those who will merge your code. 


Code formatting
===============

* We use Eclipse's default code formatter for the java code, with no particular group on imports.
  Be sure your code is formatted properly before committing as improper formatting can 
  significantly complicate merges and code reviews. 

Documentation isn't optional
----------------------------

By "documentation" we mean:

* **Javadoc** (API-doc-friendly comments for each method non-local variable)
  (This step is optional for some bugfixes.)

* Large submissions should have a **changelog entry** crediting the contributor
  and/or any individuals instrumental in identifying the problem.

Tests aren't optional
---------------------

Any bugfix that doesn't include a test proving the existence of the bug being
fixed, may be suspect.  Ditto for new features that can't prove they actually
work.

We've found that test-first development really helps make features better
architected and identifies potential edge cases earlier instead of later.
Writing tests before the implementation is strongly encouraged.

Full example
------------

Here's an example workflow for a project ``theproject`` hosted on Github, which
is currently in version 1.3.x. Your username is ``yourname`` and you're
submitting a basic bugfix. (This workflow only changes slightly if the project
is hosted at Bitbucket, self-hosted, or etc.)

Preparing your Fork
^^^^^^^^^^^^^^^^^^^

1. Click 'Fork' on Github, creating e.g. ``yourname/theproject``.
2. Clone your project: ``git clone git@github.com:yourname/theproject``.
3. ``cd theproject``
4. Install the development requirements: ``mvn install``.
5. Create a branch: ``git checkout -b foo-the-bars``.

Making your Changes
^^^^^^^^^^^^^^^^^^^

1. Write tests expecting the correct/fixed functionality; make sure they fail.
2. Code.
3. Run tests again, making sure they pass.
4. Commit your changes: ``git commit -m "Foo the bars"``

Creating Merge Requests
^^^^^^^^^^^^^^^^^^^^^^

1. Push your commit to get it back up to your fork: ``git push origin HEAD``
2. On Gitlab, create a new "Merge request" from the branch you just pushed to the destination
   branch, typically the one you forked from.
3. In the description field, write down issue number (if submitting code fixing
   an existing issue) or describe the issue + your fix (if submitting a wholly
   new bugfix).
4. Except rare cases, merge requests should be handle by some one else than the requester.

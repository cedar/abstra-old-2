
#!/bin/bash

ABSTRA_DIR=.
CACHE_DIR=cache
TMP_DIR=tmp
TT_DIR=treetagger
TT_BIN=""
HEIDELTIME_DIR=heideltime
PYTHON_SCRIPTS_DIR=scripts
CLASSIFICATION_DIR=classification
REPORTING=reporting

# get OS to get the relevant executables for treetagger
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux OS
  TT_BIN=binLinux;
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  TT_BIN=binMacos
else
  # by default, we will try with Linux
  TT_BIN=binLinux
fi

echo "$OSTYPE"

usage() {
  echo "Usage: ${0} -d <path/to/Abstra/working/directory> -t <path/to/deployed/app/tin/tomcat>"
  echo ""
  echo "    -d: <path/to/Abstra/working/directory>. The path to the directory that we will used as a working directory for Abstra (copying models and writing some temporary files)."
  echo "    -t: <path/to/deployed/app/tin/tomcat>. The absolute path of the deployed Abstra GUI in Tomcat. Can be set to the same value as -d if no deployed app. "
  exit 0
}

realpath() {
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
}

while getopts "d:t:" opt; do
  case "${opt}" in
    d)
      ABSTRA_DIR=${OPTARG}
      ;;
    t)
      ABSOLUTEPATH_TOMCAT=${OPTARG}
      ;;
    *)
      usage
      ;;
  esac
done
shift "$(($OPTIND -1))"

[ ! -d $ABSTRA_DIR ] && mkdir -p $ABSTRA_DIR

echo "ABSTRA_DIR is :'$ABSTRA_DIR'"

# create folders
mkdir -p $ABSTRA_DIR/$CACHE_DIR
mkdir -p $ABSTRA_DIR/$TMP_DIR
mkdir -p $ABSTRA_DIR/$TT_DIR/bin
mkdir -p $ABSTRA_DIR/$TT_DIR/cmd
mkdir -p $ABSTRA_DIR/$TT_DIR/lib
mkdir -p $ABSTRA_DIR/$TT_DIR/models
mkdir -p $ABSTRA_DIR/$HEIDELTIME_DIR
mkdir -p $ABSTRA_DIR/$PYTHON_SCRIPTS_DIR
mkdir -p $ABSTRA_DIR/$CLASSIFICATION_DIR
mkdir -p $ABSTRA_DIR/$REPORTING

# configure Python venv
# Mac OSX or Linux OS
python3.6 -m venv $ABSTRA_DIR/abstra_env
source $ABSTRA_DIR/abstra_env/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
deactivate

# copy necessary files
cp -R core/lib/treetagger/$TT_BIN/. $ABSTRA_DIR/$TT_DIR/bin/.
cp core/lib/treetagger/cmd/utf8-tokenize.perl $ABSTRA_DIR/$TT_DIR/cmd/.
cp -R core/lib/treetagger/lib/. $ABSTRA_DIR/$TT_DIR/lib/.
cp -R core/lib/treetagger/models/. $ABSTRA_DIR/$TT_DIR/models/.
cp -R core/lib/classification/. $ABSTRA_DIR/$CLASSIFICATION_DIR/.
cp -R core/lib/rdf-quotient.jar $ABSTRA_DIR/.
cp -R core/lib/reporting/. $ABSTRA_DIR/$REPORTING/.

cp core/lib/heideltime/config-heideltime-no-treetagger.props $ABSTRA_DIR/$HEIDELTIME_DIR/config-heideltime.props

cp -R core/scripts/. $ABSTRA_DIR/$PYTHON_SCRIPTS_DIR/.

cp core/src/main/resources/parameter.settings $ABSTRA_DIR/local.settings


# customise the local.settings using absolute path (from $ABSTRA_DIR)
ABSOLUTEPATH_MODELS=$(realpath $ABSTRA_DIR/$TT_DIR/models)
ABSOLUTEPATH_TMP=$(realpath $ABSTRA_DIR/$TMP_DIR)
ABSOLUTEPATH_CACHE=$(realpath $ABSTRA_DIR/$CACHE_DIR)
ABSOLUTEPATH_PYTHON=$(realpath $ABSTRA_DIR/abstra_env)
ABSOLUTEPATH_TT=$(realpath $ABSTRA_DIR/$TT_DIR)
ABSOLUTEPATH_HEIDELTIME=$(realpath $ABSTRA_DIR/$HEIDELTIME_DIR)
ABSOLUTEPATH_PYTHON_SCRIPTS=$(realpath $ABSTRA_DIR/$PYTHON_SCRIPTS_DIR)
ABSOLUTEPATH_CLASSIFICATION=$(realpath $ABSTRA_DIR/$CLASSIFICATION_DIR)

# add to local.settings the  absolute path to models
printf "\n\n\n#### GENERATED PARAMETERS\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute path to tmp directory
printf "# Temporary directory for Abstra\n" >> $ABSTRA_DIR/local.settings
printf "temp_dir=${ABSOLUTEPATH_TMP}\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute path to tmp directory
printf "# Tomcat directory for Abstra GUI\n" >> $ABSTRA_DIR/local.settings
printf "tomcat_dir=${ABSOLUTEPATH_TOMCAT}\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute path to the cache
printf "# Abstra cache location\n" >> $ABSTRA_DIR/local.settings
printf "cache_location=${ABSOLUTEPATH_CACHE}\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute path to python
printf "# Python location\n" >> $ABSTRA_DIR/local.settings
printf "python_path=${ABSOLUTEPATH_PYTHON}/bin/python\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute path to python scripts
printf "# Path to python scripts (FLAIR extraction)\n" >> $ABSTRA_DIR/local.settings
printf "python_script_location=${ABSOLUTEPATH_PYTHON_SCRIPTS}\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the absolute paths to TreeTagger
printf "# Treetagger location\n" >> $ABSTRA_DIR/local.settings
printf "treetagger_home=${ABSOLUTEPATH_TT}\n\n" >> $ABSTRA_DIR/local.settings

printf "# Stanford models location\n" >> $ABSTRA_DIR/local.settings
printf "stanford_models=${ABSOLUTEPATH_MODELS}\n\n" >> $ABSTRA_DIR/local.settings

# add to local.settings the path to heideltime's settings
printf "# Configuration file used by HeidelTime (date extractor)\n" >> $ABSTRA_DIR/local.settings
printf "config_heideltime=${ABSOLUTEPATH_HEIDELTIME}/config-heideltime.props\n\n" >> $ABSTRA_DIR/local.settings

# Path to word2vec model (used for classification)
printf "# Word2Vec model location (used for classification during abstraction)\n" >> $ABSTRA_DIR/local.settings
printf "word_embedding_model_path=${ABSOLUTEPATH_CLASSIFICATION}/word2vec.bin\n" >> $ABSTRA_DIR/local.settings
printf "stop_words_english=${ABSOLUTEPATH_CLASSIFICATION}/stop_words_english.txt\n" >> $ABSTRA_DIR/local.settings
printf "stop_words_french=${ABSOLUTEPATH_CLASSIFICATION}/stop_words_french.txt\n\n" >> $ABSTRA_DIR/local.settings

# set of semantic resources for the classification
printf "# set of manually-defined semantic properties\n" >> $ABSTRA_DIR/local.settings
printf "set_of_semantic_properties_manual=${ABSOLUTEPATH_CLASSIFICATION}/set-of-semantic-properties-manual.json\n\n" >> $ABSTRA_DIR/local.settings

printf "# set of semantic properties extended with Yago and DBPedia triples\n" >> $ABSTRA_DIR/local.settings
printf "set_of_semantic_properties_extended=${ABSOLUTEPATH_CLASSIFICATION}/set-of-semantic-properties-extended.json\n\n" >> $ABSTRA_DIR/local.settings

printf "# set of classes (domain/range) extracted from GitTables semantic properties\n" >> $ABSTRA_DIR/local.settings
printf "gitTables_classes=${ABSOLUTEPATH_CLASSIFICATION}/GitTables-classes.txt\n\n" >> $ABSTRA_DIR/local.settings

printf "# set of semantic properties provided by GitTables\n" >> $ABSTRA_DIR/local.settings
printf "gitTables_semantic_properties=${ABSOLUTEPATH_CLASSIFICATION}/GitTables-semantic-properties.json\n\n" >> $ABSTRA_DIR/local.settings

printf "# class hierarchy for Schema.org classes\n" >> $ABSTRA_DIR/local.settings
printf "schemaorg_class_hierarchy=${ABSOLUTEPATH_CLASSIFICATION}/class-hierarchy-schemaorg-cleaned.json\n\n" >> $ABSTRA_DIR/local.settings

printf "# synonyms for name,designation,denomination file\n" >> $ABSTRA_DIR/local.settings
printf "synonyms_denomination_file=${ABSOLUTEPATH_CLASSIFICATION}/words-related-to-denomination.txt\n\n" >> $ABSTRA_DIR/local.settings

printf "# mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person)\n" >> $ABSTRA_DIR/local.settings
printf "mapping_classes_to_categories=${ABSOLUTEPATH_CLASSIFICATION}/mapping-classes-to-categories.json\n\n" >> $ABSTRA_DIR/local.settings

# rdf quotient (used to summarize rdf inputs)
printf "# rdf quotient (used to summarize rdf inputs)\n" >> $ABSTRA_DIR/local.settings
printf "rdf_quotient=${ABSTRA_DIR}/rdf-quotient.jar\n\n" >> $ABSTRA_DIR/local.settings


# add to config-heideltime.props the absolute path to TreeTagger
printf "" >> $ABSTRA_DIR/$HEIDELTIME_DIR/config-heideltime.props
printf "#### GENERATED PARAMETERS\n" >> $ABSTRA_DIR/$HEIDELTIME_DIR/config-heideltime.props
printf "treeTaggerHome=${ABSOLUTEPATH_TT}\n\n" >> $ABSTRA_DIR/$HEIDELTIME_DIR/config-heideltime.props

cp $ABSTRA_DIR/local.settings core/src/main/resources
cp $ABSTRA_DIR/local.settings gui/WebContent/WEB-INF